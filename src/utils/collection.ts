import { SyntaxError } from "../base/customError";

export class Collection {
    protected arr;
    constructor(arr) {
        if (!Array.isArray(arr)) {
            throw new SyntaxError('Must Array');
        }
        this.arr = arr;
    }

    //pagination
    pagination(page, limit) {
        page = parseInt(page);
        limit = parseInt(limit);
        const totalRecord = this.arr.length;
        const totalPage = (totalRecord / limit >> 0) + (totalRecord % limit ? 1 : 0);
        if (page < 1) page = 1;

        const indexMin = (page - 1) * limit;
        const indexMax = Math.min((page * limit) - 1, totalRecord - 1);

        let data: any = [];
        if (page <= totalPage) {
            data = this.arr.slice(indexMin, indexMax + 1);
        }

        let paginateRes = {
            limit: limit,
            total_page: totalPage,
            page: page,
            total_record: totalRecord
        }
        return { data: data, paginate: paginateRes };
    }

    //filter
    filter(callback) {
        let items = this.arr;
        if (typeof callback == 'function') {
            items = this.arr.filter(callback)
        }
        return new Collection(items);
    }

    //filter
    search(key, value) {
        return this.filter((e) => {
            return e[key] !== undefined
                && typeof e[key] == 'string'
                && typeof value == 'string'
                && e[key].toLowerCase().includes(
                    value.toLowerCase()
                );
        });

    }

    // get
    get() {
        return this.arr;
    }

    sort(callback) {
        this.arr.sort(callback);
    }

    // public  orderBy(callback, desc = 'DESC')
    // {
    //     let descs = explode(" ", desc);
    //     let descendings = [];
    //     for (let value of descs) {
    //         descendings.push( mb_strtoupper(descs) == 'DESC' ? true : false;_
    //     }
    //     if (!descendings.length) descendings.push(true);
    //     if (typeof callback == 'function')) {
    //         $array_key = explode(" ", $callback);
    //         return $this->sort(function ($item1, $item2) use ($array_key, $descendings) {
    //             $compare = 0;
    //             $key_desc = 0;
    //             $length_desc = count($descendings);
    //             foreach ($array_key as $value) {
    //                 $array_key_child = explode(";", $value);
    //                 $data_tmp1 = $item1;
    //                 $data_tmp2 = $item2;
    //                 foreach ($array_key_child as $value_child) {
    //                     $data_tmp1 = $data_tmp1[$value_child];
    //                     $data_tmp2 = $data_tmp2[$value_child];
    //                 }

    //                 if ((is_numeric($data_tmp1) && is_numeric($data_tmp2))
    //                     || $data_tmp1 === null ||  $data_tmp2 === null
    //                 ) {
    //                     $compare = (float)$data_tmp1 > (float)$data_tmp2 ? 1
    //                         : ((float)$data_tmp1 < (float)$data_tmp2 ? -1 : 0);
    //                 } else {
    //                     $compare = strcmp(convert_unicode_utf8($data_tmp1), convert_unicode_utf8($data_tmp2));
    //                 }
    //                 $descending = $key_desc < $length_desc
    //                     ? $descendings[$key_desc] : $descendings[$length_desc - 1];

    //                 if ($compare) {
    //                     return $descending ? -$compare : $compare;
    //                 }

    //             }

    //             return is_array($descending) ? $descending[0] ? -$compare : $compare :  -$compare;
    //         });
    //     }

    //     return $this->sortBy($callback, SORT_NATURAL | SORT_FLAG_CASE, $descendings[0]);
    // }
}