import moment from "moment"

export const getTimeNow = () => {
    return new Date()
}
export const getTimeAfterNow = (mins) => {
    let after = getTimeNow()
    after.setMinutes(after.getMinutes() + mins)
    return new Date(after)
}
const serverTimeZoneOffset = new Date().getTimezoneOffset() / 60;
const changeOffset = serverTimeZoneOffset + 7; // VN timezone
// export const setCurrentDate = (selectedDate) => {
//   const currentDate = new Date(selectedDate * 1);
//   currentDate.setHours(currentDate.getHours() + changeOffset);
//   currentDate.setHours(0, 0, 0, 0);
//   return currentDate;
// }
// export const setNetDate = (selectedDate) => {
//   const netDate = new Date(selectedDate * 1);
//   netDate.setHours(0, 0, 0, 0);
//   netDate.setDate(netDate.getDate() + 1);
//   return netDate;
// }
export const setFirstDateOfMonth = () => {
    let date = new Date()
    return new Date(date.getFullYear(), date.getMonth(), 1)
}
export const setLastDateOfMonth = () => {
    let date = new Date()
    return new Date(date.getFullYear(), date.getMonth() + 1, 0)
}
export const getListMonthFromYear = (year) => {
    let arr: string[] = []
    for (let i = 1; i <= 12; i++) {
        let va = i < 10 ? `${year}-0${i}` : `${year}-${i}`;
        arr.push(va);
    }
    return arr
}
export const getFirstDayOfCurrentYear = (year) => {
    let date = new Date(year.toString())
    return new Date(date.getFullYear(), 0, 1)
}
export const getFirstDayOfNextYear = (year) => {
    let date = new Date(year.toString())
    return new Date(date.getFullYear() + 1, 0, 1)
}
export const getFirstDayOfMonthInCurrentYear = (month) => {
    let date = new Date(month)
    return new Date(date.getFullYear(), date.getMonth(), 1)
}
export const getNextDayOfMonthInCurrentYear = (month) => {
    let date = new Date(month)
    return new Date(date.getFullYear(), date.getMonth() + 1, 0)
}
export const getMonthInCurrentYear = () => {
    let date = new Date()
    return new Date(date.getFullYear(), date.getMonth())
}
export const getLastMonthInCurrentYear = (month) => {
    let date = new Date(month.toString())
    return new Date(date.getFullYear(), date.getMonth() - 1)
}
export const setCurrentDate = (date = new Date()) => {
    // let date = new Date()
    date.setHours(date.getHours() + changeOffset);
    date.setHours(0, 0, 0, 0)
    return new Date(date)
}
export const setNetDate = (netDate = new Date()) => {
    // const netDate = new Date()
    netDate.setHours(0, 0, 0, 0)
    netDate.setDate(netDate.getDate() + 1)
    return netDate
}
export const setTomorrowDate = () => {
    const tomorrowDate = new Date()
    tomorrowDate.setHours(0, 0, 0, 0)
    tomorrowDate.setDate(tomorrowDate.getDate() - 1)
    return tomorrowDate
}
export const set7DayAgoDate = (date = new Date()) => {
    date.setHours(0, 0, 0, 0)
    date.setDate(date.getDate() - 7)
    return date
}
export const set30DayAgoDate = (date = new Date()) => {
    date.setHours(0, 0, 0, 0)
    date.setDate(date.getDate() - 30)
    return date
}
export const setSomeDayAgoDate = (date = new Date(), dayAgo) => {
    date.setHours(0, 0, 0, 0)
    date.setDate(date.getDate() - parseInt(dayAgo))
    return date
}
export const setStartDateOfYear = (date = new Date()) => {
    date.setHours(0, 0, 0, 0)
    return new Date(date.getFullYear(), 0, 1)
}
export const setEndDateOfYear = (date = new Date()) => {
    date.setHours(0, 0, 0, 0)
    return new Date(date.getFullYear() + 1, 0, 0)
}
export const getDateStringFromDate = (date = new Date()) => {
    let mm = date.getMonth() + 1
    let dd = date.getDate()
    return [date.getFullYear(), mm < 10 ? '0' + mm : mm, dd < 10 ? '0' + dd : dd].join('-')
}
export const getDaysArrayBetween = (start, end) => {
    for (var arr: string[] = [], dt = new Date(start); dt <= end; dt.setDate(dt.getDate() + 1)) {
        arr.push(getDateStringFromDate(dt))
    }
    return arr
}

export const convertTimestampToDate = (timestamp) => {
    let date = new Date(0);
    date.setUTCSeconds(timestamp);
    return date;
}

function get2Digit(value) {
    value += "";
    if (value.length == 1) return "0" + "" + value;
    else return value;
}

export const generateIdFromDate = () => {
    const now = new Date();
    const month = get2Digit(now.getMonth());
    const date = get2Digit(now.getDate());
    const hours = get2Digit(now.getHours());
    const random = Math.floor(1000 + Math.random() * 9000);
    const dateValue = month + date + hours + random;
    return dateValue;
}

export const yearDiff = (dateA: Date, dateB: Date) => {
    return Math.abs(dateB.getFullYear() - dateA.getFullYear());
}

export const yearOld = (birthDay: Date) => {
    return yearDiff(birthDay, new Date());
}

/**
 * check timestamp (seconds)
 * @param timeA 
 * @param timeB 
 */
export const isSameDateTimestamp = (timeA: number, timeB: number) => {
    const a = new Date(timeA * 1000);
    const b = new Date(timeB * 1000);
    return a.getFullYear() == b.getFullYear()
        && a.getMonth() == b.getMonth()
        && a.getDate() == b.getDate();

}

export const timeToString = (date) => {
    let time = date
        .getHours()
        .toString()
        .padStart(2, "0") +
        ":" +
        date
            .getMinutes()
            .toString()
            .padStart(2, "0")
    return time;
}
export const dateToString = (date, isYear = true) => {
    let str =
        date
            .getDate()
            .toString()
            .padStart(2, "0") +
        "/" +
        (date.getMonth() + 1).toString().padStart(2, "0")
    if (isYear)
        str += "/" + date.getFullYear();
    return str
}

export const dateTimeToString = (date) => {
    return dateToString(date) + " " + timeToString(date);
}

export const toDateTime = (time, formatDate = 'DD/MM/YYYY') => {
    if (time) {
        return moment(time).format(formatDate)
    } else {
        return null
    }
}