import { arrayRemoveDuplicates } from "./utils"
const crypto = require("crypto");

export const vietnameseStringToUnicode = (str: string) => {
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a")
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e")
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i")
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o")
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u")
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y")
  str = str.replace(/đ/g, "d")
  str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "a")
  str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "e")
  str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "i")
  str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "o")
  str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "u")
  str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "y")
  str = str.replace(/Đ/g, "d")
  str = str.toLowerCase()
  return str
}

export const getHashtagFromString = (str: string) => {
  let found: any[] = []

  if (str === null) {
    return found
  }
  let patent = /#((\w|[\u00A1-\uFFFF])+)/gmi
  let hashtags = str.toLowerCase().match(patent)
  if (hashtags != null) {
    let newHashtag = hashtags.toString().replace(/[#]+/g, "").split(',').filter((element) => element !== '' && element)
    if (newHashtag.length > 0) {
      found = newHashtag.filter((element) => element !== '' && element)
    }
  }
  let result = arrayRemoveDuplicates(found)
  return result
}

export const getValueFromStr = (str: string) => {
  try {
    let res = str.match(/\${([^)]+)\}/)?.pop();
    return res;
  } catch (error) {
    return null
  }
}

export const addLink = (link_base: string, link_target: string) => link_target && link_target.includes('http') ? link_target : (link_base + link_target)

export const randomId = () => crypto.randomBytes(8).toString("hex");

export const randomShortId = (n) => crypto.randomBytes(n).toString("hex");

export const buildQuery = (queryParams) => new URLSearchParams(queryParams);
export const removeObjectReplacementCharacter = (str: string) => str.replace(/[\ufff0-\uffff]+/g, '').trim().replace(/  +/g, ' ');
