export const removeExtensionFromFile = (file: string) => {
  return file.split('.').slice(0, -1).join('.').toString()
}

export const buildErrObject = (code: number, message: string) => {
  return { code, message }
}

export const arrayRemoveDuplicates = (arr: Array<any>) => {
  let s = new Set(arr);
  return [...s]
}
export const urlify = (str: string): string => { return str.toLowerCase().replace(/[^a-z0-9]+/g, "-").replace(/^-+|-+$/g, "-").replace(/^-+|-+$/g, '') };

export const randomNumber = (length) => {
  return Math.floor(
    Math.pow(10, length - 1) +
      Math.random() * (Math.pow(10, length) - Math.pow(10, length - 1) - 1)
  ).toString();
}

export const generateOrderNumber = () => {
    let now = Date.now().toString();

    // pad with additional random digits
    if (now.length < 14) {
      const pad = 14 - now.length;
      now += randomNumber(pad);
    }
    // split into xxxx-xxxxxx-xxxx format
    return [now.slice(0, 4), now.slice(4, 10), now.slice(10, 14)].join('-');
}

export const makeShortenUrl = (length, prefix) => {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for ( var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return `/${prefix}/${result}`;
}

export const randomStringWithLength = (length) => {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for ( var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result
}

export const percentRound = (ipt, precision) => {
  if (!precision) {
      precision = 0;
  }
  if (!Array.isArray(ipt)) {
      throw new Error('percentRound input should be an Array');
  }
  const iptPercents = ipt.slice();
  const length = ipt.length;
  const out = new Array(length);

  let total = 0;
  for (let i = length - 1; i >= 0; i--) {
      if (typeof iptPercents[i] === "string") {
          iptPercents[i] = Number.parseFloat(iptPercents[i]);
      }
      total += iptPercents[i] * 1;
  }
  if (isNaN(total)) {
      throw new Error('percentRound invalid input');
  }

  if (total === 0) {
      out.fill(0);
  } else {
      const powPrecision = Math.pow(10, precision);
      const pow100 = 100 * powPrecision;
      let check100 = 0;
      for (let i = length - 1; i >= 0; i--) {
          iptPercents[i] = 100 * iptPercents[i] / total;
          check100 += out[i] = Math.round(iptPercents[i] * powPrecision);
      }

      if (check100 !== pow100) {
          const totalDiff = (check100 - pow100) ;
          const roundGrain = 1;
          let grainCount = Math.abs(totalDiff);
          const diffs = new Array(length);

          for (let i = 0; i < length; i++) {
              diffs[i] = Math.abs(out[i] - iptPercents[i] * powPrecision);
          }

          while (grainCount > 0) {
              let idx = 0;
              let maxDiff = diffs[0];
              for (let i = 1; i < length; i++) {
                  if (maxDiff < diffs[i]) {
                      idx = i;
                      maxDiff = diffs[i];
                  }
              }
              if (check100 > pow100) {
                  out[idx] -= roundGrain;
              } else {
                  out[idx] += roundGrain;
              }
              diffs[idx] -= roundGrain;
              grainCount--;
          }
      }

      if (powPrecision > 1) {
          for (let i = 0; i < length; i++) {
              out[i] = out[i] / powPrecision;
          }
      }
  }

  return out;
}


export const getBodyShapeMark = (value, bodyShapes) => {
    const bodyShapeItem = bodyShapes.find(item => item.bodyTypeName.toUpperCase() === value.toUpperCase())
    const bodyShapeMark = {}
    const nameMark = {}
    if(!bodyShapeItem) {
        bodyShapes.forEach(item => {
            bodyShapeMark[item._id.toString()] = 0
            nameMark[item.bodyTypeName] = 0
        })
    }
    else {
        bodyShapes.forEach(item => {
            bodyShapeMark[item._id.toString()] = 5 - Math.abs(bodyShapeItem.priority - item.priority)
            nameMark[item.bodyTypeName.toString()] = 5 - Math.abs(bodyShapeItem.priority - item.priority)
        });
    }

    return bodyShapeMark
}

export const bookmarkByUserAndFilterSoldOut = (listNewestProducts, listFavoriteProducts) => {
    return listNewestProducts.map((item: any) => {
        item.is_bookmarked = listFavoriteProducts.includes(item?._id) ? true : false;
        item.sold = item?.sold < 10 ? 0 : item.sold;
        return item;
    })
}