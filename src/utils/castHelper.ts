export default class CastHelper {

  /**
   * 
   * @param date Date|null
   * @return Int
   */
  static dateToTimestamp(date) {
    if (date instanceof Date)
      return Math.floor(date.getTime() / 1000)
    return null;
  }
}