import mongoose from "mongoose";
import { KOREA, VIET_NAM } from "../base/variable";
export const cloneObj = (obj) => {
    return JSON.parse(JSON.stringify(obj));
}

export function randomInt(min = 1_000_000, max: any = null) {
    if (max == null)
        return Math.floor(Math.random() * min) + min;
    return Math.floor(Math.random() * (max - min)) + min;
}

export function randomFloat(min = 1, max: any = null) {
    if (max == null)
        return parseFloat(((Math.random() * min) + min).toFixed(1));
    return parseFloat(((Math.random() * (max - min)) + min).toFixed(1));
}

export function sortObject(object) {
    var sorted: any = {},
        key: any, array: any = [];
    for (key in object) {
        if (object.hasOwnProperty(key)) {
            array.push(key);
        }
    }
    array.sort();
    for (key = 0; key < array.length; key++) {
        sorted[array[key]] = object[array[key]];
    }
    return sorted;
}

export function exclude(objects, excludeIds) {
    const filterExcludeIds = excludeIds?.filter(e => e)
    return objects?.filter((object => !filterExcludeIds.includes(object?.id) && !filterExcludeIds.includes(object?._id)))
}

export const ObjectId = mongoose.Types.ObjectId;

// Determine the country of Korea or Vietnam by phone number
export const countryByPhoneNumber = (phoneNumber) => {
    let phone_number_vn_regex: any;

    if (phoneNumber.charAt(0) == '+') {
        phone_number_vn_regex = /((849|843|847|848|845)+([0-9]{8})\b)/g;
        phoneNumber = phoneNumber.slice(1);
    } else {
        phone_number_vn_regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
    }

    if( phone_number_vn_regex.test(phoneNumber) == true) {
        return VIET_NAM;
    } else {
        return KOREA;
    }
}

export function isMappable(array: object[]): boolean {
    if (Array.isArray(array)) return array.length > 0;
    return false;
  }

export const phoneNormalize = (phone) => {
    if (phone.charAt(0) == "+") {
        if (/((849|843|847|848|845)+([0-9]{8})\b)/g.test(phone)) {
            return phone;
        }
    } else {
        if (/((09|03|07|08|05)+([0-9]{8})\b)/g.test(phone)) {
            return `+84${phone.substring(1)}`;
        }

        if (/((849|843|847|848|845)+([0-9]{8})\b)/g.test(phone)) {
            return `+${phone}`
        }
    }
};

export const roundNumberPercent = (num, scale) => {
    if (!('' + num).includes('e')) {
        return +(Math.round(+(num + 'e+' + scale)) + 'e-' + scale);
    } else {
        var arr = ('' + num).split('e');
        var sig = '';
        if (+arr[1] + scale > 0) {
            sig = '+';
        }
        return +(
            Math.round(+(+arr[0] + 'e' + sig + (+arr[1] + scale))) +
            'e-' +
            scale
        );
    }
};
export const timeOutOfPromise = (timeInMiliSecond, returnData) => new Promise(resolve =>
    setTimeout(() => resolve(returnData), timeInMiliSecond)
)

export const convertCamelCaseToNormal = (name: string) => {
    return name.replace(/([A-Z])/g, ' $1').replace(/^./, (str) => str.toUpperCase())
} 

export function getReplaceString (number: number) {
    let str = ""
    for(let i=0;i<number;i++){
        str += "?"
    }
    return str;
}

export const formatDiscountPercentOrPriceProduct = (number: number) => {
    let convertNumber = number.toString()
    const priceLength = convertNumber.length
    let replaceString = convertNumber.slice(1,(priceLength-4))
    const replaceLength = replaceString.length
    const firstChar = convertNumber[0] || ""
    switch (priceLength-3) {
        case -1:
        case 0:
        case 1:
        case 2:
            convertNumber = convertNumber.replace(firstChar, '?');
            break;
        case 3:
            convertNumber = convertNumber.replace(replaceString, getReplaceString(replaceLength));
            break;
        case 4:
            convertNumber = convertNumber.replace(replaceString, getReplaceString(replaceLength));
            break;
        case 5:
            convertNumber = convertNumber.replace(replaceString, getReplaceString(replaceLength));
            break;
        default:
            replaceString = convertNumber.slice(1,(priceLength-3)) 
            convertNumber = convertNumber.replace(replaceString, getReplaceString(replaceLength));
            break;
    }
    const regex = /(\w+)(\d\d\d)/gm;
    const subst = `$1\.$2`;
    convertNumber = convertNumber.replace(regex,subst)
    return convertNumber
}