const fs = require('fs')

export const moveFile = (oldPath, newPath) => {
    return new Promise((resolve, reject) => {
        fs.rename(oldPath, newPath, function (err) {
            if (err) {
                console.log(err)
                resolve(false)
            }
            resolve(true)
        })
    })

}