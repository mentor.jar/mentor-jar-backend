import { SyntaxError } from "../base/customError";

export class Pagination {
    protected arr;
    constructor(arr) {
        if (!Array.isArray(arr)) {
            throw new SyntaxError('Must Array');
        }
        this.arr = arr;
    }

    //pagination
    pagination(page: Number, limit: any, totalRecord: any) {
        const paginateRes = {
            limit: limit,
            total_page: Math.ceil(totalRecord/limit),
            page: page,
            total_record: totalRecord
        }
        return { data: this.arr, paginate: paginateRes };
    }

}