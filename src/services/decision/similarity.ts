import { ObjectId } from "../../utils/helper";
import { ProductQueryHelper } from '../../repositories/ProductRepo'

export default class SimilarProductStategy {


    /**
     * By category relation on DB
     * @param systemCategory 
     * @param shopCategory 
     * @param userId 
     * @returns 
     */
    static byCategoryBiasQuery(systemCategory, shopCategory, userId): Array<any> {
        const query: any = [];
        const orCondition: any = [];
        if (systemCategory)
            orCondition.push({
                category_id: ObjectId(systemCategory)
            })

        if (shopCategory && shopCategory?.length > 0) {
            query.push(ProductQueryHelper.relations.category);
            const listObjectId = shopCategory.map(e => ObjectId(e));
            orCondition.push({
                product_categories: {
                    $elemMatch: { category_id: { $in: listObjectId } }
                }
            })
        }

        query.push({
            $match: {
                $or: orCondition
            }
        })

        return query

    }
}