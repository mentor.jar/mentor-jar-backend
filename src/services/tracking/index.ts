import TrackingRepo from '../../repositories/TrackingRepo';
import { ObjectId } from 'mongoose';
import { TrackingType } from './definitions';
import { firestore } from '../../base/connection/firebase';

export default class TrackingService {

    private static _instance: TrackingService;

    static get _() {
        if (!this._instance) {
            this._instance = new TrackingService();
        }
        return this._instance;
    }

    /**
     * Always save new event
     * @param event 
     * @returns 
     */
    async recordNew(event) {
        return TrackingRepo.getInstance().create(event);
    }


    /**
     * Save to a existed record
     * @param event 
     * @param incrementCount 
     * @returns 
     */
    async recordContinue(event, incrementCount = 1) {
        const { target_type, target_id, actor_type, actor_id, action_type, action_info } = event;

        const activity = await firestore.collection('trackingactivities')
            .where('target_type', '==', target_type)
            .where('target_id', '==', target_id)
            .where('actor_type', '==', actor_type)
            .where('actor_id', '==', actor_id)
            .where('action_type', '==', action_type)
            .get();
        
        if(!activity.empty && activity.size < 2) {
            activity.forEach(act => {
                const {visited_ats, count_number} = act.data();

                return new Promise((resolve, reject) => {
                    resolve(firestore.collection('trackingactivities').doc(act.id).update({
                        count_number: count_number + incrementCount,
                        visited_ats: [...visited_ats, new Date()],
                    }))
                });
            })
            return;
        }

        return new Promise((resolve, reject) => {
            resolve(firestore.collection('trackingactivities').doc().set(event))
        });
    }


    /**
     * prebuild method
     */

    /**
     * 
     * @param userId 
     * @param productId 
     * @returns 
     */
    async userViewProduct(userId: ObjectId, productId: ObjectId) {
        return this.recordContinue(
            new TrackingBuilder()
                .setTarget('Product', productId)
                .setActor('User', userId)
                .setActionType(TrackingType.ViewProduct)
                .get()
        )
    }

    /**
     * 
     * @param userId 
     * @param productId 
     * @returns 
     */
    async addProductToCartInStream(userId: ObjectId, productId: ObjectId) {
        return this.recordContinue(
            new TrackingBuilder()
                .setTarget('Product', productId)
                .setActor('User', userId)
                .setActionType(TrackingType.AddProductToCartStream)
                .get()
        )
    }

    // working
    /**
     * 
     * @param userId 
     * @param productId 
     * @returns 
     */
    async addProductToCart(userId: ObjectId, productId: ObjectId) {
        return this.recordContinue(
            new TrackingBuilder()
                .setTarget('Product', productId)
                .setActor('User', userId)
                .setActionType(TrackingType.AddProductToCart)
                .get()
        )
    }

    /**
     * @param userId 
     * @param productId 
     * @returns 
     */
    async buyProduct(userId: ObjectId, productId: ObjectId) {
        return this.recordContinue(
            new TrackingBuilder()
                .setTarget('Product', productId)
                .setActor('User', userId)
                .setActionType(TrackingType.BuyProduct)
                .get()
        )
    }

    /**
     * 
     * @param userId 
     * @param productId 
     * @returns 
     */
    async buyProductInStream(userId: ObjectId, productId: ObjectId) {
        return this.recordContinue(
            new TrackingBuilder()
                .setTarget('Product', productId)
                .setActor('User', userId)
                .setActionType(TrackingType.BuyProductInStream)
                .get()
        )
    }

    /**
     * 
     * @param userId 
     * @param productId 
     * @returns 
     */

    async addProductToFavorite(userId: ObjectId, productId: ObjectId) {
        return this.recordContinue(
            new TrackingBuilder()
                .setTarget('Product', productId)
                .setActor('User', userId)
                .setActionType(TrackingType.AddProductToFavorite)
                .get()
        )
    }

    async userInteractiveProduct(userId: ObjectId, productId: ObjectId, action_type: string) {
        let trackings;
        switch (action_type) {
            case TrackingType.ViewProduct:
                trackings = await this.userViewProduct(userId, productId);
                break;
            case TrackingType.AddProductToCartStream:
                trackings = await this.addProductToCartInStream(userId, productId);
                break;
            case TrackingType.AddProductToCart:
                trackings = await this.addProductToCart(userId, productId);
                break;
            case TrackingType.BuyProduct:
                trackings = await this.buyProduct(userId, productId);
                break;
            case TrackingType.BuyProductInStream:
                trackings = await this.buyProductInStream(userId, productId);
                break;
            case TrackingType.AddProductToFavorite:
                trackings = await this.addProductToFavorite(userId, productId);
                break;
        }
        return trackings;
    }
}

export class TrackingBuilder {

    private target_id;
    private target_type;
    private actor_id;
    private actor_type;
    private action_type;
    private action_info = {};
    private count_number = 1;

    setTarget(target_type, target_id) {
        this.target_id = target_id;
        this.target_type = target_type;
        return this;
    }

    setActor(actor_type, actor_id) {
        this.actor_type = actor_type;
        this.actor_id = actor_id;
        return this;
    }

    setActionType(type: TrackingType) {
        this.action_type = type;
        return this;
    }

    setNumberCount(count) {
        this.count_number = count;
        return this;
    }


    setActionInfo(action_info) {
        this.action_info = action_info;
        return this;
    }

    get() {
        const instance = {
            target_id: this.target_id,
            target_type: this.target_type,
            actor_id: this.actor_id,
            actor_type: this.actor_type,
            action_type: this.action_type,
            action_info: this.action_info,
            count_number: this.count_number,
            created_at: new Date(),
            updated_at: new Date(),
            visited_ats: [new Date()]
        }
        return instance;
    }
}