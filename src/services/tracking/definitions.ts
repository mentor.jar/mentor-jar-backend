export enum TrackingType {
    ViewPost = 'view_post',
    SharePost = 'share_post',
    ViewAds = 'view_ads',

    ViewStream = 'view_stream',

    ViewProduct = 'view_product',
    AddProductToCart = 'add_cart',
    BuyProduct = 'buy_product',

    BuyProductInStream = 'buy_product_in_stream',
    AddProductToCartStream = 'add_cart_in_stream',
    AddProductToFavorite = 'add_product_to_favorite',

}

export enum TrackingRate {
    ViewProduct = 1,
    AddProductToCart = 2,
    BuyProduct = 3,

    BuyProductInStream = 3,
    AddProductToCartStream = 2,
    AddProductToFavorite = 1
}