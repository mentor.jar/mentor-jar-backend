import { ChatMessage } from "../sockets/definitions";
import {
  CampaignServiceInt, ChangeNotificationStatusRequest,
  CreateNotifyCampaignRequest, GetNotificationsRequest,
  PushMessageRequest, PushServiceInt, StorageServiceInt
} from "./interface";
import { NOTIFY_TYPE, DEFAULT_TITLE, SOUND_EFFECT, ANDROID_CHANNEL, DEEPLINK_PREFIX, NOTIFY_SYSTEM_TYPE } from "./constants";
import NotifyRequestBuilder from './module/builder';
import { InMemoryMessageForRoomStore } from "../../SocketStores/MessageStore";
import { NotificationHelper } from "./module/helper";
import NotificationRepo from "../../repositories/NotificationRepo";
import BannerRepo from "../../repositories/BannerRepo";
import { ShopRepo }  from "../../repositories/ShopRepo";

const RoomStore = InMemoryMessageForRoomStore.getInstance();
const bannerRepo = BannerRepo.getInstance();
const shopRepo = ShopRepo.getInstance();
export default class NotificationCore {

  private pushService: PushServiceInt;
  private storageService: StorageServiceInt;
  private campaignService: CampaignServiceInt;

  constructor(pushService: PushServiceInt, storageService: StorageServiceInt, campaignService: CampaignServiceInt) {
    this.pushService = pushService;
    this.storageService = storageService;
    this.campaignService = campaignService;
  }

  /**
   * Send notification and save
   * @param request 
   * @param options 
   */
  public async saveAndSend(
    request: PushMessageRequest,
    options: any = {
      sendSync: true
    }
  ) {
    if (!request.receiverId || request.receiverId?.length <= 0) return;
    const notifySaved = await this.storageService.saveNotification(request);
    if (options.sendSync)
      await this.pushService.pushMessageBatch(request, notifySaved);
    else this.pushService.pushMessageBatch(request, notifySaved);

    return notifySaved;
  }

  /**
   * only send notification to device
   * @param request 
   */
  public async send(request: PushMessageRequest) {
    const result = await this.pushService.pushMessageBatch(request, null);
    return result;
  }

  /**
   * Send and push campaign
   * @param request 
   * @returns 
   */
  public async saveAndPushCampaign(request: CreateNotifyCampaignRequest) {
    const data = this.campaignService.createNotifyCampaign(request);

    let deeplink = DEEPLINK_PREFIX + request.type.toLowerCase();
    switch (request.type) {
      case NOTIFY_SYSTEM_TYPE.SHOP:
      case NOTIFY_SYSTEM_TYPE.PROFILE:
        request.context.deeplink = `${deeplink}/${request.context.user_id}`; 
        break;
      case NOTIFY_SYSTEM_TYPE.BANNER:
        const banner = await bannerRepo.getBannerById(request.context.banner_id);

        if(banner.classify === 'none' || banner.classify === 'link') break;
        
        deeplink += `/${banner.classify}`;
        if (banner.classify === 'category_view') {
          deeplink += `/${banner.advance_actions.category}`;
        } else if (banner.classify === 'shop_view'){
          const shop = await shopRepo.findShopByShopId(banner.advance_actions.shop);
          deeplink += `/${shop.user_id}`;
        } else {
          deeplink += `/${request.context.banner_id}`;
        }

        request.context.deeplink = deeplink;
        break;
      case NOTIFY_SYSTEM_TYPE.PRODUCT:
        request.context.deeplink = `${deeplink}/${request.context.product_id}`;
      default:
        break;
    }

    switch (request.schedule) {
      case 'now':
        this._saveAndPushCampaign(request);
        break;
      case 'remind':
        this._remindPushCampaign(request);
        break;
      default:
        throw new Error("Không hỗ trợ.")
    }
    return data;
  }

  /**
   * Save remind campaign
   * @param request 
   * @returns 
   */
  public async _remindPushCampaign(request: CreateNotifyCampaignRequest) {
    // do something
    return true;
  }

  /**
   * Save and push campaign
   * @param request 
   * @returns 
   */
   public _saveAndPushCampaign(request: CreateNotifyCampaignRequest) {
    const pushRequest = new NotifyRequestBuilder()
      .setType(request.type)
      .setContentType(request.type)
      .setContent(request.content)
      .setTitle(request.title)
      .setReceiverId(request.receivers || '')
      .setImage(request.image)
      .setData(request.data)
      .setContext(request.context)
      .get()

    this.pushService.pushMessageBatch(pushRequest, null);
    return true;
  }

  /**
   * Send chat notification
   * @param room 
   * @param message 
   * @param receiverIds 
   * @returns 
   */
  public async sendChatNotification(room, message: ChatMessage, receiverIds) {
    // remove user not active
    // remove user hide notification
    // remove user turn off notification chat room
    let content_notify = message.type == 'text' ? message.content : undefined
    let image_notify = undefined
    if (message.type == 'customize') {
      let object = JSON.parse(message.content);
      image_notify = object?.image ? object?.image : undefined
    }
    if (message.type == 'image') {
      image_notify = message.content
    }
    return this.pushService.pushMessageIndividual(
      new NotifyRequestBuilder()
        .setReceiverId(receiverIds)
        .setType(NOTIFY_TYPE.CHAT_MESSAGE)
        .setTitle(message?.user?.userName)
        .setContent(content_notify)
        .setSound(SOUND_EFFECT.CHAT_SOUND)
        .setAndroidChannel(ANDROID_CHANNEL.CHAT)
        .setData({
          roomId: room._id.toString(),
          messageId: message.id.toString(),
          type: NOTIFY_TYPE.CHAT_MESSAGE,
          content: message.content,
          content_type: message.type,
          senderId: message.user_id,
          senderName: message?.user?.userName,
          senderAvatar: message?.user?.avatar
        })
        .setImage(image_notify)
        .get()
    )
  }

  public async getNotification(request: GetNotificationsRequest) {
    // return this.storageService.getNotifications(request);
    const notificationRepo = NotificationRepo.getInstance();
    const notifications = await notificationRepo.getNotifications(request);
    return notifications;
  }

  public getLatestNotification(userId: String, type: String) {
    return this.storageService.getLatestNotification(userId, type);
  }

  public countUnreadNotification() {
    return this.storageService.countNotification();
  }

  public changeStatusNotification(request: ChangeNotificationStatusRequest) {
    return this.storageService.changeReadStatus(request);
  }

  public getNotifyCampaigns(request) {
    return this.campaignService.getNotifyCampaigns(request)
  }

  public getNotifyCampaignDetail(id) {
    return this.campaignService.getNotifyCampaignDetail(id)
  }

  public async getUnreadMessage(userId) {
    return NotificationHelper.getUnreadMessage(userId);
  }
}