export interface Notifiable { }

export interface StorageServiceInt {

  saveNotification(request: PushMessageRequest);

  getNotifications(request: GetNotificationsRequest);

  getLatestNotification(userId: String, type: String)

  countNotification();

  changeReadStatus(request: ChangeNotificationStatusRequest);

  removeNotification();

  unreadNotification(userId);
}

export interface CampaignServiceInt {

  createNotifyCampaign(request: CreateNotifyCampaignRequest, isCreate?: boolean);

  getNotifyCampaigns(request);

  getNotifyCampaignDetail(id);

  removeNotifyCampaign();

}

export interface PushServiceInt {
  pushMessageBatch(request: PushMessageRequest, notifySaved: any);
  pushMessageIndividual(request: PushMessageRequest);
}

export interface PushMessageRequest {

  type?: String,
  contentType?: String,
  receiverId: String | Array<String>,

  creatorId?: String,
  title?: String,
  content?: Object,
  data?: Object,
  context?: any,
  targetType?: String,
  os?: String,
  image?: String,
  groupId?: String,
  sound?: String,
  androidChannel?: String,
  campaignId?: String,
  badge?: Number
}


export interface CreateNotifyCampaignRequest {
  type: String,
  title: String,
  content: String,
  receivers?: Array<String>,
  topic?: Array<String>,
  description?: String,
  createdBy?: String,
  data?: Object,
  os?: String,
  image?: String,
  sound?: String,
  schedule?: String,
  timeWillPush?: Date,
  context?: any,
}

export interface ChangeNotificationStatusRequest {
  status: Boolean,
  notifyIds?: Array<String>,
  userId?: String,
  maskAll?: Boolean
}

export interface GetNotificationsRequest {
  userId?: String,
  status?: Boolean,
  type: String | Array<String>,
  page: Number,
  limit: Number,
}