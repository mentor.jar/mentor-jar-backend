import { NOTIFY_TYPE, NOTIFY_CONTENT_TYPE, DEFAULT_LANG } from "./constants";
import vi from "./lang/vi";
import en from "./lang/en";

const lang = { vi, en }

const message = lang[DEFAULT_LANG];

const configMessage = {
  [NOTIFY_TYPE.CHATS]: {
    [NOTIFY_CONTENT_TYPE.NEW_MESSAGE]: [message.new_stream_title, message.new_stream_content],
  },
  [NOTIFY_TYPE.EVENTS]: {

  },
  [NOTIFY_TYPE.ORDERS]: {
    [NOTIFY_CONTENT_TYPE.ORDER]: [message.order_payment_title, message.order_payment_content],
  },
  [NOTIFY_TYPE.PRIZES]: {

  },
  [NOTIFY_TYPE.PROMOTIONS]: {
    [NOTIFY_CONTENT_TYPE.ORDER]: [message.order_payment_title, message.order_payment_content],
  },
}

export const trans = (name, locale = DEFAULT_LANG) => {
  if (
    !lang[locale] ||
    lang[locale][name] == null ||
    lang[locale][name] != undefined
  ) locale = DEFAULT_LANG;
  return lang[locale][name];
}

export const buildMessageContent = (type, contentType, context) => {
  let [title, content] = configMessage[type]?.[contentType]
    ?? [null, null];

  if (title) title = compileText(title, context);
  if (content) content = compileText(content, context);

  return [title, content]
}

const compileText = (formatText, data) => {
  const needed_params = formatText?.match(/\{.[^}]*}/gi)?.map(e => e.replace(/{|}/gi, ""));
  let result = formatText;

  if (data)
    needed_params?.forEach(key => {
      if (data[key] != undefined && data != null)
        result = result.replace(new RegExp(`{${key}}`, 'gi'), data[key].toString())
    });

  return formatText;
}

