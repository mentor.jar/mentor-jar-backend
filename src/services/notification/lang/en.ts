const en = {
  "new_stream_title": "Live stream",
  "new_stream_content": "Xem live stream của <bold>{shop_name}</bold>: <live_stream_title>",
  "order_payment_title": "Xác nhận thanh toán",
  "order_payment_content": "Thanh toán cho đơn hàng <blue>{order_id}</blue> thành công! Vui lòng kiểm tra thời gian nhận hàng dự kiến trong phần chi tiết đơn hàng nhé.",
  "order_shipping_title": "Đang vận chuyển",
  "order_shipping_content": "Kiện hàng của đơn hàng <green>{order_id}</green> đã được người bán <green>{seller_name}</green> giao cho <green>{shipping_method_name}</green>",
  "order_received_title": "Xác nhận đã nhận hàng",
  "order_received_content": "Đơn hàng <green>{order_id}</green> đã được xác nhận giao hàng thành công.",
  "order_cancel_success_title": "Huỷ đơn hàng thành công",
  "order_cancel_success_content": "Đơn hàng <green>{order_id}</green> đã được người bán <green>{seller_name}</green> xác nhận huỷ đơn hàng thành công."
}

export default en;