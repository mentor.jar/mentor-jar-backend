import NotificationRepo from "../../../repositories/NotificationRepo";
import { InMemoryMessageForRoomStore } from "../../../SocketStores/MessageStore";
import { SHARDING_KEY } from "../constants";

const storageService = NotificationRepo.getInstance();
const RoomStore = InMemoryMessageForRoomStore.getInstance();

export class NotificationHelper {

    public static async getUnreadNumberBatch(usersId) {
        let unreadResult: any = {};
        try {
            if (typeof usersId == 'string' && usersId != '') {
                unreadResult[usersId] = await NotificationHelper.getUnreadMessage(usersId);
            } else {
                if (Array.isArray(usersId) && usersId?.length) {
                    await Promise.all(
                        usersId.map(async userId => {
                            unreadResult[userId] = await NotificationHelper.getUnreadMessage(userId)
                        })
                    )

                }
            }
            return unreadResult;
        } catch (error) {
            console.log(error);
        }
    }

    public static async getUnreadMessage(userId) {
        let systemCount = 0;
        let chatCount = 0;
        try {
            const systemUnreadNotify = await storageService.unreadNotification(userId);
            systemCount = systemUnreadNotify?.length || 0;
        } catch (error) {
            console.log(error);
        }
        // try {
        //     const userRooms: any = await RoomStore.fetchAllRoomByUser(userId.toString());
        //     userRooms.forEach(room => {
        //         const user = room.users.find(user => user._id == userId);
        //         if (user) {
        //             const last_seen_index = user.seen_last_message_index != null ? user.seen_last_message_index : -1;
        //             const unread = room.messages?.length - (last_seen_index + 1);
        //             chatCount += Math.max(unread, 0);
        //         }
        //     });
        // } catch (error) {
        //     console.log(error);
        // }
        return systemCount + chatCount;
    }

    public static async getUnreadMessageByApp(userId) {
        let community = 0;
        let commerce = 0;
        try {
            const systemUnreadNotify = await storageService.unreadNotification(userId);
            commerce = systemUnreadNotify.filter(e => e.shardingKey == SHARDING_KEY)?.length;
            community = systemUnreadNotify?.length - commerce;
        } catch (error) {
            console.log(error);
        }
        return {
            community,
            commerce
        };
    }

}