import { DEFAULT_TITLE, NOTIFY_TYPE, DEFAULT_CONTENT, ANDROID_CHANNEL } from '../constants';
import { PushMessageRequest } from '../interface';

export default class NotifyRequestBuilder {
    _receiverId: String | Array<String> | undefined;
    _creatorId: String | undefined;
    _data: any | undefined;
    _title: String | undefined;
    _content: String | undefined;
    _contentType: String | undefined;
    _type: String | undefined;
    _image: String | undefined;
    _context: any | undefined;
    _sound: any | undefined;
    _androidChannel: any | undefined;
    _targetType: String | undefined;

    get receiverId() {
        if (!this._receiverId) return [];
        if (Array.isArray(this._receiverId))
            return this._receiverId.map(item => item.toString());
        return this._receiverId?.toString()
    }

    setReceiverId(receiverId: String | Array<String>) {
        this._receiverId = receiverId;
        return this;
    }


    get creatorId() {
        return this._creatorId;
    }

    setCreatorId(creatorId: any | undefined) {
        this._creatorId = creatorId;
        return this;
    }

    get data() {
        const data = { ...this._data }
        if (!data?.type)
            data.type = this.type;
        if (this.targetType)
            data.targetType = this.targetType;
        return data;
    }

    setData(data: any | undefined) {
        this._data = data;
        return this;
    }

    get title() {
        return this._title || DEFAULT_TITLE;
    }

    setTitle(title: any | undefined) {
        this._title = title;
        return this;
    }

    get content() {
        return this._content || DEFAULT_CONTENT;
    }

    setContent(content: any | undefined) {
        this._content = content;
        return this;
    }

    get contentType() {
        return this._contentType;
    }

    setContentType(contentType: any | undefined) {
        this._contentType = contentType;
        return this;
    }

    get type() {
        return this._type;
    }

    setType(type: any | undefined) {
        this._type = type;
        return this;
    }

    get image() {
        return this._image;
    }

    setImage(image: any | undefined) {
        this._image = image;
        return this;
    }

    get context() {
        return this._context;
    }

    setContext(context: any | undefined) {
        this._context = context;
        return this;
    }


    get androidChannel() {
        return this._androidChannel || ANDROID_CHANNEL.DEFAULT;
    }

    setAndroidChannel(channelName: any | undefined) {
        this._androidChannel = channelName;
        return this;
    }

    get sound() {
        return this._sound || null;
    }

    setSound(soundEffect: any | undefined) {
        this._sound = soundEffect;
        return this;
    }

    get targetType() {
        return this._targetType;
    }

    setTargetType(targetType: any | undefined) {
        this._targetType = targetType;
        return this;
    }

    get(): PushMessageRequest {
        if (!this.type) throw new Error("Expect required property: type")

        const request: PushMessageRequest = {
            receiverId: this.receiverId,
            data: this.data,
            title: this.title,
            content: this.content,
            contentType: this.contentType,
            creatorId: this.creatorId,
            type: this.type,
            image: this.image,
            context: this.context,
            targetType: this.targetType,
            sound: this.sound,
            androidChannel: this.androidChannel
        }

        return request;
    }

}