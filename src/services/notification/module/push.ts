import * as admin from 'firebase-admin';
import { groupBy } from 'lodash';
import AppUserRepo from '../../../repositories/AppUserRepo';
import { ObjectId } from "../../../utils/helper";
import { DEFAULT_TITLE, OS, DEFAULT_CONTENT, RESERVED_KEYWORD } from "../constants";
import { PushMessageRequest, PushServiceInt } from "../interface";
import { NotificationHelper } from './helper';
import { notifyLogChannel } from '../../../base/log';

/**
 * Firebase and self management key push service 
 */
export default class FCMPushService implements PushServiceInt {

  private appUserRepo = AppUserRepo.getInstance();
  private pushEach = 500;
  /**
   * Export method
   */

  /**
   * Push message by batch
   * @param request 
   * @param notifySaved 
   * @returns 
   */
  public async pushMessageBatch(request: PushMessageRequest, notifySaved) {
    const app_users = await this.appUserLastActive(request.receiverId);
    if (!app_users.length) return;
    const unreadList = await NotificationHelper.getUnreadNumberBatch(request.receiverId);
    const messages = await this.buildMessageBundle(app_users, request, notifySaved, unreadList);

    for (let index = 0; index < messages.length; index += this.pushEach ) {
      const pushMessages = messages.slice(index, index + this.pushEach);
      await this.sendAll(pushMessages);
    }
    return;
  }

  /**
   * Push to each device
   * @param request 
   */
  public async pushMessageIndividual(request: PushMessageRequest) {
    try {
      const app_users = await this.appUserAll(request.receiverId);
      notifyLogChannel.info("Debug:", app_users)
      const unreadList = await NotificationHelper.getUnreadNumberBatch(request.receiverId);
      const promises = app_users.map(app_user => {
        if (app_user.firebaseToken) {
          const { payload, options } = this.buildPayload(app_user, request, unreadList[app_user.userId.toString()])
          return this.sendToDevice(app_user.firebaseToken, payload, options);
        }
      })
      const response = await Promise.all(promises)
    } catch (error) {
      notifyLogChannel.info('Request: ', request)
      this.handlePushResponse(null, error, null)
    }
  }

  /**
   * Send ways
   */

  /**
   * Start send notification by batch
   * @param messages 
   */
  private sendAll(messages) {
    return new Promise((resolve, reject) => {
      admin
        .messaging()
        .sendAll(messages)
        .then((response) => {
          this.handlePushResponse(response, null, messages)
          return resolve(response);
        })
        .catch((error) => {
          this.handlePushResponse(null, error, messages)
          return resolve(null);
        });
    })
  }

  /**
   * Send to each device each time
   * @param token 
   * @param payload 
   * @param options 
   * @returns 
   */
  private sendToDevice(token, payload, options = {}) {
    return new Promise((resolve, reject) => {
      admin
        .messaging()
        .sendToDevice(token, payload, {
          priority: "high",
          contentAvailable: true,
          ...options
        })
        .then(response => {
          this.handlePushResponse(response, null, { token, payload })
          return resolve(response);
        })
        .catch(error => {
          this.handlePushResponse(null, error, { token, payload })
          return resolve(null);
        });
    });
  }

  /**
   * Builder
   */

  /**
   * https://firebase.google.com/docs/cloud-messaging/send-message#send-a-batch-of-messages
   * @param app_users 
   * @param request 
   * @param notifySaved 
   */
  private async buildMessageBundle(app_users, request: PushMessageRequest, notifySaved: any, unreadList: any) {
    const messageBundle = app_users.map(app_user => {
      const token = app_user.firebaseToken;
      let { payload, options } = this.buildPayload(app_user, request, unreadList[app_user.userId.toString()]);
      payload.token = token;
      if (notifySaved) {
        const notifyMatch = notifySaved?.find(e => e.targetedBy.toString() == app_user.userId)
        if (notifyMatch) payload.data.notificationId = notifyMatch._id.toString();
      }
      if (options?.apns?.payload?.aps)
        options.apns.payload.aps.badge = parseInt(payload?.notification?.badge);
      delete payload?.notification?.badge;

      if (payload?.notification?.sound) {
        if (app_user.os == OS.ANDROID) {
          options.android = options.android || { notification: {} };
          options.android.notification.sound = payload?.notification?.sound;
        }
        if (app_user.os == OS.iOS) {
          options.apns = options.apns || { payload: { aps: {} } };
          options.apns.payload.aps.sound = payload?.notification?.sound;
        }
        delete payload?.notification?.sound;
      }

      if (payload?.notification?.android_channel_id) {
        options.android = options.android || { notification: {} };
        options.android.notification.channelId = payload?.notification?.android_channel_id;
        delete payload?.notification?.android_channel_id;
      }

      payload = { ...payload, ...options };
      return payload;
    })

    return messageBundle;
  }

  private buildPayload(app_user, request: PushMessageRequest, badgeNumber = 0): { payload: any, options: any } {
    const title = request.title ?? DEFAULT_TITLE;
    const content = request.content ?? DEFAULT_CONTENT;
    const payload: any = {}
    const options: any = {}
    const badge = badgeNumber >= 0 && !isNaN(badgeNumber) ? badgeNumber : 0;
    let notification: any = {
      title: title,
      body: content,
      badge: badge + ""
    }

    if (request.sound) notification.sound = request.sound;

    if (app_user.os == OS.ANDROID) {
      if (request.image)
        notification = { ...notification, image: request.image }
      if (request.androidChannel)
        notification.android_channel_id = request.androidChannel;
    }

    if (app_user.os == OS.iOS) {
      options.apns = {
        payload: {
          aps: {
            "mutable-content": 1
          },
        },
        headers: {
          "apns-priority": "5"
        }
      }
      if (request.image) {
        options.apns.fcm_options = {
          image: request.image
        }
      }
    }

    let data = {};
    if (request.data)
      for (const [key, value] of Object.entries(request.data)) {
        if (!RESERVED_KEYWORD.includes(key))
          data[key] = value;
      }

    payload.data = {...data, ...request.context} ;
    payload.notification = notification;
    return { payload, options };
  }

  /**
   * Get token to send
   */

  /**
   * Get app user with userId(s)
   * filter the latest item have same userId
   * @param usersId 
   */
  private async appUserLastActive(usersId: String | Array<String>) {
    let data: any = [];
    if (typeof usersId == 'string' && usersId != '') {
      data = await this.appUserRepo
        .getModel()
        .findOne({
          userId: ObjectId(usersId.toString()),
          // shardingKey: SHARDING_KEY
        }, null, {
          sort: { updatedAt: -1 }
        })
        .exec();
      if (!data) return [];
      data = [{ ...data.toJSON(), userId: data.userId.toString() }];
    } else {
      const condition: any = {
        // shardingKey: SHARDING_KEY
      };
      if (Array.isArray(usersId) && usersId?.length) condition.userId = { $in: usersId };
      data = await this.appUserRepo.find(condition);

      data = data.map(item => ({ ...item.toJSON(), userId: item.userId.toString() }))
      data = groupBy(data, 'userId');
      data = Object.values(data).map((groups: any) => {
        return groups.sort((a, b) => b.updatedAt - a.updatedAt)?.[0];
      })
    }

    return data;
  }

  /**
   * Get all app user of a user, not the lastest
   * @param usersId 
   */
  private async appUserAll(usersId: String | Array<String>) {

    let data: any = [];
    if (typeof usersId == 'string' && usersId != '') {
      data = await this.appUserRepo
        .getModel()
        .find({
          userId: ObjectId(usersId.toString()),
          // shardingKey: SHARDING_KEY
        }, null, {
          sort: { updatedAt: -1 }
        })
        .exec();
      data = [{ ...data.toJSON(), userId: data.userId.toString() }];
    } else {
      const condition: any = {
        // shardingKey: SHARDING_KEY
      };
      if (Array.isArray(usersId) && usersId?.length)
        condition.userId = { $in: usersId.map(id => ObjectId(id.toString())) };
      data = await this.appUserRepo.find(condition);
    }

    const dataCleaned: any = [];

    data.forEach(appUser => {
      if (!dataCleaned.find((existedItem: any) => existedItem.firebaseToken == appUser.firebaseToken))
        dataCleaned.push(appUser);
    });

    return dataCleaned;
  }

  /*
    Handle edge
  */

  /**
   * 
   * @param response 
   * @param error 
   * @param message 
   */
  private handlePushResponse(response, error: any = null, message: any = null) {
    if (response) {
      if (Array.isArray(response.responses)) {
        response.responses.forEach((result, index) => {
          if (result.success) {
            notifyLogChannel.info("-----*----")
            notifyLogChannel.info('Send success with payload')
            notifyLogChannel.info("Payload: ", message?.[index])
            notifyLogChannel.info("-----*----")
          } else {
            notifyLogChannel.info("-----x----")
            notifyLogChannel.info("Send notification error at", new Date())
            notifyLogChannel.info("Payload: ", message?.[index])
            notifyLogChannel.info("Error: ", result.error.errorInfo)
            notifyLogChannel.info("-----x----")
          }
        })

      } else if (Array.isArray(response.results)) {
        response.results.forEach(result => {
          if (result.messageId) {
            notifyLogChannel.info("-----*----")
            notifyLogChannel.info('Send success with payload')
            notifyLogChannel.info("Payload: ", message)
            notifyLogChannel.info("-----*----")
          } else if (result.error) {
            notifyLogChannel.info("-----x----")
            notifyLogChannel.info("Send notification error at", new Date())
            notifyLogChannel.info("Payload: ", message)
            notifyLogChannel.info("Error: ", result.error.errorInfo)
            notifyLogChannel.info("-----x----")
          }
        });
      }
    }

    if (error) {
      notifyLogChannel.info("-----x----")
      notifyLogChannel.info("!Invalid send content")
      notifyLogChannel.info("Send notification error at", new Date())
      notifyLogChannel.info("Payload: ", message)
      notifyLogChannel.info(error)
      notifyLogChannel.info("-----x----")
    }
  }

}