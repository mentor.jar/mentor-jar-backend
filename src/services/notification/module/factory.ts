import { ANDROID_CHANNEL, DEFAULT_TITLE, NOTIFY_CONTENT_TYPE, NOTIFY_TYPE, SOUND_EFFECT } from '../constants';
import { PushMessageRequest } from '../interface';
import NotifyRequestBuilder from './builder';

export default class NotifyRequestFactory {

    public static generate(type, targetType, dataBundle): PushMessageRequest {
        switch (type) {
            case NOTIFY_TYPE.EVENTS_LIVE_STREAM_REMIND:
                return this.liveStreamRemind(dataBundle, targetType);
            case NOTIFY_TYPE.ORDERS_ACCEPT:
                return this.orderAccept(dataBundle, targetType);
            case NOTIFY_TYPE.ORDERS_CREATED:
                return this.orderCreated(dataBundle, targetType);
            case NOTIFY_TYPE.ORDERS_SHIPPED:
                return this.orderShipped(dataBundle, targetType);
            case NOTIFY_TYPE.ORDERS_CANCELED:
                return this.orderCanceled(dataBundle, targetType);
            case NOTIFY_TYPE.ORDERS_CANCELING:
                return this.orderCanceling(dataBundle, targetType);
            case NOTIFY_TYPE.ORDERS_GHTK:
                return this.orderChangeStatusShipping(dataBundle, targetType);
            case NOTIFY_TYPE.FEEDBACK_PRODUCT_CREATED:
                return this.feedbackProductCreated(dataBundle, targetType);
            case NOTIFY_TYPE.FEEDBACK_USER_CREATED:
                return this.feedbackUserCreated(dataBundle, targetType);
            case NOTIFY_TYPE.START_LIVESTREAM:
                return this.startLivestream(dataBundle, targetType);
            case NOTIFY_TYPE.REWARD_VOUCHER:
                return this.voucherReward(dataBundle, targetType);
            case NOTIFY_TYPE.REFUND_REQUEST_SENT:
                return this.refundRequestSent(dataBundle, targetType);
            case NOTIFY_TYPE.REFUND_SHOP_APPROVED:
                return this.refundShopApproved(dataBundle, targetType);
            case NOTIFY_TYPE.REFUND_SHOP_REJECTED:
                return this.refundShopRejected(dataBundle, targetType);
            case NOTIFY_TYPE.REFUND_SHOP_RECEIVED:
                return this.refundShopReceived(dataBundle, targetType);
            case NOTIFY_TYPE.REFUND_ADMIN_APPROVED:
                return this.refundAdminApproved(dataBundle, targetType);
            case NOTIFY_TYPE.REFUND_ADMIN_REJECTED:
                return this.refundAdminRejected(dataBundle, targetType);
            case NOTIFY_TYPE.REFUND_REQUEST_CANCELED:
                return this.refundRequestCanceled(dataBundle, targetType);
            case NOTIFY_TYPE.REFUND_ADMIN_COMPLETED:
                return this.refundAdminCompleted(dataBundle, targetType);
            case NOTIFY_TYPE.CREATE_SHOP_APPROVED:
                return this.createShopApproved(dataBundle, targetType);
            case NOTIFY_TYPE.CREATE_SHOP_REJECTED:
                return this.createShopRejected(dataBundle, targetType);
            case NOTIFY_TYPE.ORDERS_REMIND_FEEDBACK:
                return this.orderRemindFeedback(dataBundle, targetType);
            case NOTIFY_TYPE.ASSIGN_CASH_BACK:
                return this.assignCashBackToUser(dataBundle, targetType)
            case NOTIFY_TYPE.ORDER_INVALID:
                return this.invalidOrder(dataBundle, targetType)
            case NOTIFY_TYPE.PAYMENT_MOMO_PAID:
                return this.paymentMomoPaid(dataBundle, targetType)
            case NOTIFY_TYPE.GB_ROOM_CREATED:
                return this.groupBuyRoomCreated(dataBundle, targetType)
            case NOTIFY_TYPE.GB_JOIN:
                return this.groupBuyJoined(dataBundle, targetType)
            case NOTIFY_TYPE.GB_NEED_INVITE:
                return this.groupBuyNeedInvitePerson(dataBundle, targetType)
            case NOTIFY_TYPE.GB_HOST_PAID:
                return this.groupBuyHostPaid(dataBundle, targetType)
            case NOTIFY_TYPE.GB_HOST_MISSING_1_PER:
                return this.groupBuyHostMissing1Person(dataBundle, targetType)
            case NOTIFY_TYPE.GB_CANCELED:
                return this.groupBuyCanceled(dataBundle, targetType)
            case NOTIFY_TYPE.GB_MEMBER_MISSING_1_PER:
                return this.groupBuyMissing1Person(dataBundle, targetType)
            case NOTIFY_TYPE.GB_MEMBER_PAID:
                return this.groupBuyMemberPaid(dataBundle, targetType)
            case NOTIFY_TYPE.GB_MEMBER_INVITED:
                return this.groupBuyMemberInvited(dataBundle, targetType)
            // ... continue at type here
            default: throw new Error("Unsupport factory type");
        }
    }

    private static invalidOrder(dataBundle, targetType) {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.ORDER_INVALID)
            .setContentType(NOTIFY_CONTENT_TYPE.ORDER)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setTargetType(targetType)
            .setContent(dataBundle.content || `Bạn có 1 đơn hàng không thành công`)
            .setReceiverId(dataBundle.receiverId)
            .setSound(SOUND_EFFECT.ORDER_SOUND)
            .setAndroidChannel(ANDROID_CHANNEL.ORDER)
            .setData({
                userId: dataBundle.userId,
            })
            .setContext({
                user_id: dataBundle.userId
            })
            .get()
    }

    private static assignCashBackToUser(dataBundle, targetType) {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.ASSIGN_CASH_BACK)
            .setContentType(NOTIFY_CONTENT_TYPE.ORDER)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setTargetType(targetType)
            .setContent(dataBundle.content || `Bạn vừa nhận được voucher giảm giá từ đơn hàng ${dataBundle.orderNumber}. Vui lòng kiểm tra ví voucher để xem chi tiết`)
            .setReceiverId(dataBundle.receiverId)
            .setSound(SOUND_EFFECT.ORDER_SOUND)
            .setAndroidChannel(ANDROID_CHANNEL.ORDER)
            .setData({
                orderId: dataBundle.orderId,
                userId: dataBundle.userId,
                orderNumber: dataBundle.orderNumber
            })
            .setContext({
                order_id: dataBundle.orderId,
                order_number: dataBundle.orderNumber
            })
            .get()
    }

    // We can use raw object here
    private static liveStreamRemind(dataBundle: { receiverID, streamId, streamName }, targetType): PushMessageRequest {
        return {
            receiverId: dataBundle.receiverID,
            type: NOTIFY_TYPE.EVENTS_LIVE_STREAM_REMIND,
            contentType: NOTIFY_CONTENT_TYPE.LIVESTREAM,
            targetType: targetType,
            data: {
                streamId: dataBundle.streamId,
                streamName: dataBundle.streamName,
            }
        }
    }

    // Or use NotifyRequestBuilder
    private static orderAccept(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.ORDERS_ACCEPT)
            .setContentType(NOTIFY_CONTENT_TYPE.ORDER)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setContent(dataBundle.content || 'Đơn hàng ' + dataBundle.orderNumber + ' đã tiếp nhận.')
            .setReceiverId(dataBundle.receiverId)
            .setTargetType(targetType)
            .setSound(SOUND_EFFECT.ORDER_SOUND)
            .setAndroidChannel(ANDROID_CHANNEL.ORDER)
            .setData({
                orderId: dataBundle.orderId,
                userId: dataBundle.userId,
                orderNumber: dataBundle.orderNumber
            })
            .setContext({
                userId: dataBundle.userId,
                order_id: dataBundle.orderId,
                order_number: dataBundle.orderNumber
            })
            .get()
    }

    private static orderCreated(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.ORDERS_CREATED)
            .setContentType(NOTIFY_CONTENT_TYPE.ORDER)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setTargetType(targetType)
            .setContent(dataBundle.content || 'Bạn vừa nhận được đơn hàng ' + dataBundle.orderNumber + ' .')
            .setReceiverId(dataBundle.receiverId)
            .setSound(SOUND_EFFECT.ORDER_SOUND)
            .setAndroidChannel(ANDROID_CHANNEL.ORDER)
            .setData({
                orderId: dataBundle.orderId,
                userId: dataBundle.userId,
                orderNumber: dataBundle.orderNumber
            })
            .setContext({
                order_id: dataBundle.orderId,
                order_number: dataBundle.orderNumber
            })
            .get()
    }

    private static orderShipped(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.ORDERS_SHIPPED)
            .setContentType(NOTIFY_CONTENT_TYPE.ORDER)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setTargetType(targetType)
            .setContent(dataBundle.content || 'Đơn hàng ' + dataBundle.orderNumber + ' đã được nhận.')
            .setReceiverId(dataBundle.receiverId)
            .setSound(SOUND_EFFECT.ORDER_SOUND)
            .setAndroidChannel(ANDROID_CHANNEL.ORDER)
            .setData({
                orderId: dataBundle.orderId,
                userId: dataBundle.userId,
                orderNumber: dataBundle.orderNumber
            })
            .setContext({
                order_id: dataBundle.orderId,
                order_number: dataBundle.orderNumber
            })
            .get()
    }

    private static orderCanceling(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.ORDERS_CANCELING)
            .setContentType(NOTIFY_CONTENT_TYPE.ORDER)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setTargetType(targetType)
            .setContent(dataBundle.content || 'Đơn hàng ' + dataBundle.orderNumber + ' đã yêu cầu hủy. Xem lý do hủy đơn.')
            .setReceiverId(dataBundle.receiverId)
            .setSound(SOUND_EFFECT.ORDER_SOUND)
            .setAndroidChannel(ANDROID_CHANNEL.ORDER)
            .setData({
                orderId: dataBundle.orderId,
                userId: dataBundle.userId,
                orderNumber: dataBundle.orderNumber
            })
            .setContext({
                order_id: dataBundle.orderId,
                order_number: dataBundle.orderNumber
            })
            .get()
    }

    private static orderCanceled(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.ORDERS_CANCELED)
            .setContentType(NOTIFY_CONTENT_TYPE.ORDER)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setTargetType(targetType)
            .setContent(dataBundle.content || `Đơn hàng ${dataBundle.orderNumber} đã bị huỷ.`)
            .setReceiverId(dataBundle.receiverId)
            .setSound(SOUND_EFFECT.ORDER_SOUND)
            .setAndroidChannel(ANDROID_CHANNEL.ORDER)
            .setData({
                orderId: dataBundle.orderId,
                userId: dataBundle.userId,
                orderNumber: dataBundle.orderNumber
            })
            .setContext({
                order_id: dataBundle.orderId,
                order_number: dataBundle.orderNumber
            })
            .get()
    }

    private static orderChangeStatusShipping(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.ORDERS_GHTK)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setContent(dataBundle.content || 'Đơn hàng ' + dataBundle.orderNumber + ' ' + dataBundle.shipping_status + '.')
            .setReceiverId(dataBundle.receiverId)
            .setContentType(NOTIFY_CONTENT_TYPE.ORDER)
            .setTargetType(targetType)
            .setSound(SOUND_EFFECT.ORDER_SOUND)
            .setAndroidChannel(ANDROID_CHANNEL.ORDER)
            .setData({
                orderId: dataBundle.orderId,
                userId: dataBundle.userId,
                orderNumber: dataBundle.orderNumber
            })
            .setContext({
                order_id: dataBundle.orderId,
                order_number: dataBundle.orderNumber,
                status_id: dataBundle.status_id,
                shipping_status: dataBundle.shipping_status,
            })
            .get()
    }

    private static feedbackProductCreated(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.FEEDBACK_PRODUCT_CREATED)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setContent(dataBundle.content || `Đơn hàng ${dataBundle.orderNumber} đã được người mua đánh giá.`)
            .setReceiverId(dataBundle.receiverId)
            .setContentType(NOTIFY_CONTENT_TYPE.FEEDBACK)
            .setTargetType(targetType)
            .setData({
                userName: dataBundle.userName,
                productId: dataBundle.productId,
                productName: dataBundle.productName,
                orderId: dataBundle.orderId,
                userId: dataBundle.userId,
                feedbackId: dataBundle.feedbackId,
                orderNumber: dataBundle.orderNumber,
            })
            .setContext({
                product_id: dataBundle.productId,
                user_id: dataBundle.userId,
                order_id: dataBundle.orderId,
                feedback_id: dataBundle.feedbackId,
                order_number: dataBundle.orderNumber,
            })
            .get()
    }

    private static feedbackUserCreated(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.FEEDBACK_USER_CREATED)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setContent(dataBundle.content || `Đơn hàng ${dataBundle.orderNumber} đã được người bán đánh giá.`)
            .setReceiverId(dataBundle.receiverId)
            .setContentType(NOTIFY_CONTENT_TYPE.FEEDBACK)
            .setTargetType(targetType)
            .setData({
                userName: dataBundle.userName,
                shopId: dataBundle.shopId,
                userId: dataBundle.userId,
                orderId: dataBundle.orderId,
                feedbackId: dataBundle.feedbackId,
                orderNumber: dataBundle.orderNumber
            })
            .setContext({
                shop_id: dataBundle.shopId,
                user_id: dataBundle.userId,
                order_id: dataBundle.orderId,
                feedback_id: dataBundle.feedbackId,
                order_number: dataBundle.orderNumber
            })
            .get()
    }

    private static startLivestream(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.START_LIVESTREAM)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setContent(dataBundle.content || `Người dùng ${dataBundle.userName} đang phát luồng trực tuyến`)
            .setReceiverId(dataBundle.receiverId)
            .setContentType(NOTIFY_CONTENT_TYPE.LIVESTREAM)
            .setTargetType(targetType)
            .setData({
                followerId: dataBundle.receiverId,
                userId: dataBundle.userId,
                userName: dataBundle.userName,
                streamSessionId: dataBundle.streamSessionId
            })
            .setContext({
                follower_id: dataBundle.receiverId,
                user_id: dataBundle.userId,
                user_name: dataBundle.userName,
                stream_session_id: dataBundle.streamSessionId
            })
            .get()
    }

    private static voucherReward(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.REWARD_VOUCHER)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setContent(dataBundle.content)
            .setReceiverId(dataBundle.receiverId)
            .setContentType(NOTIFY_CONTENT_TYPE.VOUCHER)
            .setTargetType(targetType)
            .setData({
                userId: dataBundle.receiverId,
                rewardType: dataBundle.type
            })
            .setContext({
                user_id: dataBundle.receiverId,
                reward_type: dataBundle.type,
                count: dataBundle.count
            })
            .get()
    }

    private static refundRequestSent(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.REFUND_REQUEST_SENT)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setContent(dataBundle.content || `Đơn hàng ${dataBundle.orderNumber} có yêu cầu hoàn trả sản phẩm ${dataBundle.itemName}`)
            .setReceiverId(dataBundle.receiverId)
            .setContentType(NOTIFY_CONTENT_TYPE.REFUND)
            .setTargetType(targetType)
            .setSound(SOUND_EFFECT.ORDER_SOUND)
            .setAndroidChannel(ANDROID_CHANNEL.ORDER)
            .setData({
                orderId: dataBundle.orderId,
                userId: dataBundle.userId,
                orderNumber: dataBundle.orderNumber,
                itemId: dataBundle.itemId
            })
            .setContext({
                order_id: dataBundle.orderId,
                user_id: dataBundle.userId,
                order_number: dataBundle.orderNumber,
                item_id: dataBundle.itemId
            })
            .get()
    }

    private static refundShopApproved(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.REFUND_SHOP_APPROVED)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setContent(dataBundle.content || `Yêu cầu hoàn trả sản phẩm ${dataBundle.itemName} của đơn hàng ${dataBundle.orderNumber} đã được cửa hàng chấp nhận`)
            .setReceiverId(dataBundle.receiverId)
            .setContentType(NOTIFY_CONTENT_TYPE.REFUND)
            .setTargetType(targetType)
            .setSound(SOUND_EFFECT.ORDER_SOUND)
            .setAndroidChannel(ANDROID_CHANNEL.ORDER)
            .setData({
                orderId: dataBundle.orderId,
                userId: dataBundle.userId,
                orderNumber: dataBundle.orderNumber,
                itemId: dataBundle.itemId
            })
            .setContext({
                order_id: dataBundle.orderId,
                user_id: dataBundle.userId,
                order_number: dataBundle.orderNumber,
                item_id: dataBundle.itemId
            })
            .get()
    }

    private static refundShopRejected(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.REFUND_SHOP_REJECTED)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setContent(dataBundle.content || `Yêu cầu hoàn trả sản phẩm ${dataBundle.itemName} của đơn hàng ${dataBundle.orderNumber} đã bị cửa hàng từ chối`)
            .setReceiverId(dataBundle.receiverId)
            .setContentType(NOTIFY_CONTENT_TYPE.REFUND)
            .setTargetType(targetType)
            .setSound(SOUND_EFFECT.ORDER_SOUND)
            .setAndroidChannel(ANDROID_CHANNEL.ORDER)
            .setData({
                orderId: dataBundle.orderId,
                userId: dataBundle.userId,
                orderNumber: dataBundle.orderNumber,
                itemId: dataBundle.itemId
            })
            .setContext({
                order_id: dataBundle.orderId,
                user_id: dataBundle.userId,
                order_number: dataBundle.orderNumber,
                item_id: dataBundle.itemId
            })
            .get()
    }

    private static refundShopReceived(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.REFUND_SHOP_REJECTED)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setContent(dataBundle.content || `Sản phẩm hoàn trả ${dataBundle.itemName} của đơn hàng ${dataBundle.orderNumber} đã được cửa hàng nhận`)
            .setReceiverId(dataBundle.receiverId)
            .setContentType(NOTIFY_CONTENT_TYPE.REFUND)
            .setTargetType(targetType)
            .setSound(SOUND_EFFECT.ORDER_SOUND)
            .setAndroidChannel(ANDROID_CHANNEL.ORDER)
            .setData({
                orderId: dataBundle.orderId,
                userId: dataBundle.userId,
                orderNumber: dataBundle.orderNumber,
                itemId: dataBundle.itemId
            })
            .setContext({
                order_id: dataBundle.orderId,
                user_id: dataBundle.userId,
                order_number: dataBundle.orderNumber,
                item_id: dataBundle.itemId
            })
            .get()
    }

    private static refundAdminApproved(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.REFUND_ADMIN_APPROVED)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setContent(dataBundle.content || `Yêu cầu hoàn trả sản phẩm ${dataBundle.itemName} của đơn hàng ${dataBundle.orderNumber} đã được Admin chấp nhận`)
            .setReceiverId(dataBundle.receiverId)
            .setContentType(NOTIFY_CONTENT_TYPE.REFUND)
            .setTargetType(targetType)
            .setSound(SOUND_EFFECT.ORDER_SOUND)
            .setAndroidChannel(ANDROID_CHANNEL.ORDER)
            .setData({
                orderId: dataBundle.orderId,
                userId: dataBundle.userId,
                orderNumber: dataBundle.orderNumber,
                itemId: dataBundle.itemId
            })
            .setContext({
                order_id: dataBundle.orderId,
                user_id: dataBundle.userId,
                order_number: dataBundle.orderNumber,
                item_id: dataBundle.itemId
            })
            .get()
    }

    private static refundAdminRejected(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.REFUND_ADMIN_REJECTED)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setContent(dataBundle.content || `Yêu cầu hoàn trả sản phẩm ${dataBundle.itemName} của đơn hàng ${dataBundle.orderNumber} đã bị Admin từ chối`)
            .setReceiverId(dataBundle.receiverId)
            .setContentType(NOTIFY_CONTENT_TYPE.REFUND)
            .setTargetType(targetType)
            .setSound(SOUND_EFFECT.ORDER_SOUND)
            .setAndroidChannel(ANDROID_CHANNEL.ORDER)
            .setData({
                orderId: dataBundle.orderId,
                userId: dataBundle.userId,
                orderNumber: dataBundle.orderNumber,
                itemId: dataBundle.itemId
            })
            .setContext({
                order_id: dataBundle.orderId,
                user_id: dataBundle.userId,
                order_number: dataBundle.orderNumber,
                item_id: dataBundle.itemId
            })
            .get()
    }

    private static refundRequestCanceled(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.REFUND_REQUEST_CANCELED)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setContent(dataBundle.content || `Yêu cầu hoàn trả sản phẩm ${dataBundle.itemName} của đơn hàng ${dataBundle.orderNumber} đã huỷ bỏ`)
            .setReceiverId(dataBundle.receiverId)
            .setContentType(NOTIFY_CONTENT_TYPE.REFUND)
            .setTargetType(targetType)
            .setSound(SOUND_EFFECT.ORDER_SOUND)
            .setAndroidChannel(ANDROID_CHANNEL.ORDER)
            .setData({
                orderId: dataBundle.orderId,
                userId: dataBundle.userId,
                orderNumber: dataBundle.orderNumber,
                itemId: dataBundle.itemId
            })
            .setContext({
                order_id: dataBundle.orderId,
                user_id: dataBundle.userId,
                order_number: dataBundle.orderNumber,
                item_id: dataBundle.itemId
            })
            .get()
    }

    private static refundAdminCompleted(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.REFUND_ADMIN_COMPLETED)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setContent(dataBundle.content || `Yêu cầu hoàn trả sản phẩm ${dataBundle.itemName} của đơn hàng ${dataBundle.orderNumber} đã được hoàn tiền`)
            .setReceiverId(dataBundle.receiverId)
            .setContentType(NOTIFY_CONTENT_TYPE.REFUND)
            .setTargetType(targetType)
            .setSound(SOUND_EFFECT.ORDER_SOUND)
            .setAndroidChannel(ANDROID_CHANNEL.ORDER)
            .setData({
                orderId: dataBundle.orderId,
                userId: dataBundle.userId,
                orderNumber: dataBundle.orderNumber,
                itemId: dataBundle.itemId
            })
            .setContext({
                order_id: dataBundle.orderId,
                user_id: dataBundle.userId,
                order_number: dataBundle.orderNumber,
                item_id: dataBundle.itemId
            })
            .get()
    }

    private static createShopApproved(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.CREATE_SHOP_APPROVED)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setContent(dataBundle.content || `Yêu cầu tạo cửa hàng của bạn đã được duyệt`)
            .setReceiverId(dataBundle.receiverId)
            .setContentType(NOTIFY_CONTENT_TYPE.SHOP)
            .setTargetType(targetType)
            .setSound(SOUND_EFFECT.DEFAULT)
            .setAndroidChannel(ANDROID_CHANNEL.DEFAULT)
            .setData({
                shopId: dataBundle.shopId,
                userId: dataBundle.userId
            })
            .setContext({
                shop_id: dataBundle.shopId,
                user_id: dataBundle.userId
            })
            .get()
    }

    private static createShopRejected(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.CREATE_SHOP_REJECTED)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setContent(dataBundle.content || `Yêu cầu tạo cửa hàng của bạn đã bị từ chối`)
            .setReceiverId(dataBundle.receiverId)
            .setContentType(NOTIFY_CONTENT_TYPE.SHOP)
            .setTargetType(targetType)
            .setSound(SOUND_EFFECT.DEFAULT)
            .setAndroidChannel(ANDROID_CHANNEL.DEFAULT)
            .setData({
                shopId: dataBundle.shopId,
                userId: dataBundle.userId
            })
            .setContext({
                shop_id: dataBundle.shopId,
                user_id: dataBundle.userId
            })
            .get()
    }
    
    private static orderRemindFeedback(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.ORDERS_REMIND_FEEDBACK)
            .setContentType(NOTIFY_CONTENT_TYPE.ORDER)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setTargetType(targetType)
            .setContent(dataBundle.content || 'Vui lòng đánh giá sản phẩm cho đơn hàng ' + dataBundle.orderNumber)
            .setReceiverId(dataBundle.receiverId)
            .setSound(SOUND_EFFECT.ORDER_SOUND)
            .setAndroidChannel(ANDROID_CHANNEL.ORDER)
            .setData({
                orderId: dataBundle.orderId,
                userId: dataBundle.userId,
                orderNumber: dataBundle.orderNumber
            })
            .setContext({
                order_id: dataBundle.orderId,
                order_number: dataBundle.orderNumber
            })
            .get()
    }
    
    private static groupBuyRoomCreated(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.GB_ROOM_CREATED)
            .setContentType(NOTIFY_CONTENT_TYPE.GROUP_BUY)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setTargetType(targetType)
            .setContent(dataBundle.content || 'Bạn đã tạo nhóm mua chung thành công. Vui lòng mời thêm bạn để mua được giá ưu đãi.')
            .setReceiverId(dataBundle.receiverId)
            .setSound(SOUND_EFFECT.ORDER_SOUND)
            .setAndroidChannel(ANDROID_CHANNEL.ORDER)
            .setData({
                orderId: dataBundle.orderId,
                userId: dataBundle.userId,
                orderNumber: dataBundle.orderNumber,
                orderRoomId: dataBundle.orderRoomId,
                refLink: dataBundle.refLink
            })
            .setContext({
                order_id: dataBundle.orderId,
                order_number: dataBundle.orderNumber,
                order_room_id: dataBundle.orderRoomId,
                ref_link: dataBundle.refLink,
                product_id: dataBundle.productId
            })
            .get()
    }
    
    private static groupBuyJoined(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.GB_JOIN)
            .setContentType(NOTIFY_CONTENT_TYPE.GROUP_BUY)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setTargetType(targetType)
            .setContent(dataBundle.content || dataBundle.userName + ' đã tham gia nhóm mua chung của bạn.')
            .setReceiverId(dataBundle.receiverId)
            .setSound(SOUND_EFFECT.ORDER_SOUND)
            .setAndroidChannel(ANDROID_CHANNEL.ORDER)
            .setData({
                orderId: dataBundle.orderId,
                userId: dataBundle.userId,
                orderNumber: dataBundle.orderNumber,
                orderRoomId: dataBundle.orderRoomId,
                refLink: dataBundle.refLink
            })
            .setContext({
                user_id: dataBundle.userId,
                order_id: dataBundle.orderId,
                order_number: dataBundle.orderNumber,
                order_room_id: dataBundle.orderRoomId,
                ref_link: dataBundle.refLink,
                product_id: dataBundle.productId
            })
            .get()
    }

    private static groupBuyNeedInvitePerson(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.GB_NEED_INVITE)
            .setContentType(NOTIFY_CONTENT_TYPE.GROUP_BUY)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setTargetType(targetType)
            .setContent(dataBundle.content || 'Nhóm thanh toán chưa đủ người. Mời bạn bè mua chung để được giá tốt.')
            .setReceiverId(dataBundle.receiverId)
            .setSound(SOUND_EFFECT.ORDER_SOUND)
            .setAndroidChannel(ANDROID_CHANNEL.ORDER)
            .setData({
                orderId: dataBundle.orderId,
                userId: dataBundle.userId,
                orderNumber: dataBundle.orderNumber,
                orderRoomId: dataBundle.orderRoomId,
                refLink: dataBundle.refLink
            })
            .setContext({
                order_id: dataBundle.orderId,
                order_number: dataBundle.orderNumber,
                order_room_id: dataBundle.groupRoomId,
                ref_link: dataBundle.refLink,
                product_id: dataBundle.productId
            })
            .get()
    }

    private static groupBuyHostPaid(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.GB_HOST_PAID)
            .setContentType(NOTIFY_CONTENT_TYPE.GROUP_BUY)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setTargetType(targetType)
            .setContent(dataBundle.content || 'Yay!! Nhóm của bạn đã thanh toán thành công. Đơn hàng đang được chuẩn bị. Theo dõi đơn hàng tại đây.')
            .setReceiverId(dataBundle.receiverId)
            .setSound(SOUND_EFFECT.ORDER_SOUND)
            .setAndroidChannel(ANDROID_CHANNEL.ORDER)
            .setData({
                orderId: dataBundle.orderId,
                userId: dataBundle.userId,
                orderNumber: dataBundle.orderNumber,
                orderRoomId: dataBundle.orderRoomId,
                refLink: dataBundle.refLink
            })
            .setContext({
                user_id: dataBundle.userId,
                order_id: dataBundle.orderId,
                order_number: dataBundle.orderNumber,
                order_room_id: dataBundle.orderRoomId,
                ref_link: dataBundle.refLink,
                product_id: dataBundle.productId
            })
            .get()
    }

    private static groupBuyHostMissing1Person(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.GB_HOST_MISSING_1_PER)
            .setContentType(NOTIFY_CONTENT_TYPE.GROUP_BUY)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setTargetType(targetType)
            .setContent(dataBundle.content || 'Nhóm mua chung của bạn còn thiếu 1 người tham gia. Vui lòng mời thêm để mua được giá ưu đãi.')
            .setReceiverId(dataBundle.receiverId)
            .setSound(SOUND_EFFECT.ORDER_SOUND)
            .setAndroidChannel(ANDROID_CHANNEL.ORDER)
            .setData({
                orderId: dataBundle.orderId,
                userId: dataBundle.userId,
                orderNumber: dataBundle.orderNumber,
                orderRoomId: dataBundle.orderRoomId,
                refLink: dataBundle.refLink
            })
            .setContext({
                user_id: dataBundle.userId,
                order_id: dataBundle.orderId,
                order_number: dataBundle.orderNumber,
                order_room_id: dataBundle.orderRoomId,
                ref_link: dataBundle.refLink,
                product_id: dataBundle.productId
            })
            .get()
    }

    private static groupBuyCanceled(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.GB_CANCELED)
            .setContentType(NOTIFY_CONTENT_TYPE.GROUP_BUY)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setTargetType(targetType)
            .setContent(dataBundle.content || 'Đơn hàng ' + dataBundle.orderNumber + ' đã bị hủy. Tiền sẽ hoàn vào tài khoản của bạn trong vòng 48 tiếng. Vui lòng tạo nhóm mới tại đây.')
            .setReceiverId(dataBundle.receiverId)
            .setSound(SOUND_EFFECT.ORDER_SOUND)
            .setAndroidChannel(ANDROID_CHANNEL.ORDER)
            .setData({
                orderId: dataBundle.orderId,
                userId: dataBundle.userId,
                orderNumber: dataBundle.orderNumber,
                orderRoomId: dataBundle.orderRoomId,
                refLink: dataBundle.refLink
            })
            .setContext({
                user_id: dataBundle.userId,
                order_id: dataBundle.orderId,
                order_number: dataBundle.orderNumber,
                order_room_id: dataBundle.orderRoomId,
                ref_link: dataBundle.refLink,
                product_id: dataBundle.productId
            })
            .get()
    }
   
    private static groupBuyMissing1Person(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.GB_MEMBER_MISSING_1_PER)
            .setContentType(NOTIFY_CONTENT_TYPE.GROUP_BUY)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setTargetType(targetType)
            .setContent(dataBundle.content || 'Nhóm thanh toán đang còn thiếu 1 người. Mời bạn bè mua chung để được giá tốt.')
            .setReceiverId(dataBundle.receiverId)
            .setSound(SOUND_EFFECT.ORDER_SOUND)
            .setAndroidChannel(ANDROID_CHANNEL.ORDER)
            .setData({
                orderId: dataBundle.orderId,
                userId: dataBundle.userId,
                orderNumber: dataBundle.orderNumber,
                orderRoomId: dataBundle.orderRoomId,
                refLink: dataBundle.refLink,
                product_id: dataBundle.productId
            })
            .setContext({
                user_id: dataBundle.userId,
                order_id: dataBundle.orderId,
                order_number: dataBundle.orderNumber,
                order_room_id: dataBundle.orderRoomId,
                ref_link: dataBundle.refLink,
                product_id: dataBundle.productId
            })
            .get()
    }
   
    private static groupBuyMemberPaid(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.GB_MEMBER_PAID)
            .setContentType(NOTIFY_CONTENT_TYPE.GROUP_BUY)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setTargetType(targetType)
            .setContent(dataBundle.content || 'Nhóm của bạn đã thanh toán thành công. Đơn hàng đang được chuẩn bị. Theo dõi đơn hàng tại đây.')
            .setReceiverId(dataBundle.receiverId)
            .setSound(SOUND_EFFECT.ORDER_SOUND)
            .setAndroidChannel(ANDROID_CHANNEL.ORDER)
            .setData({
                orderId: dataBundle.orderId,
                userId: dataBundle.userId,
                orderNumber: dataBundle.orderNumber,
                orderRoomId: dataBundle.orderRoomId,
                refLink: dataBundle.refLink,
                product_id: dataBundle.productId
            })
            .setContext({
                user_id: dataBundle.userId,
                order_id: dataBundle.orderId,
                order_number: dataBundle.orderNumber,
                order_room_id: dataBundle.orderRoomId,
                ref_link: dataBundle.refLink,
                product_id: dataBundle.productId
            })
            .get()
    }
    
    private static groupBuyMemberInvited(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.GB_MEMBER_INVITED)
            .setContentType(NOTIFY_CONTENT_TYPE.GROUP_BUY)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setTargetType(targetType)
            .setContent(dataBundle.content || 'Bạn nhận được lời mời mua chung từ ' + dataBundle.userName + '.')
            .setReceiverId(dataBundle.receiverId)
            .setSound(SOUND_EFFECT.ORDER_SOUND)
            .setAndroidChannel(ANDROID_CHANNEL.ORDER)
            .setData({
                userId: dataBundle.userId,
                orderRoomId: dataBundle.orderRoomId,
                refLink: dataBundle.refLink
            })
            .setContext({
                user_id: dataBundle.userId,
                order_room_id: dataBundle.orderRoomId,
                ref_link: dataBundle.refLink,
                product_id: dataBundle.productId
            })
            .get()
    }

    private static paymentMomoPaid(dataBundle, targetType): PushMessageRequest {
        return new NotifyRequestBuilder()
            .setType(NOTIFY_TYPE.PAYMENT_MOMO_PAID)
            .setContentType(NOTIFY_CONTENT_TYPE.PAYMENT)
            .setTitle(dataBundle.title || DEFAULT_TITLE)
            .setTargetType(targetType)
            .setContent(dataBundle.content || 'Đơn hàng ' + dataBundle.orderNumber +' đã thanh toán thành công qua ví Momo.')
            .setReceiverId(dataBundle.receiverId)
            .setSound(SOUND_EFFECT.ORDER_SOUND)
            .setAndroidChannel(ANDROID_CHANNEL.ORDER)
            .setData({
                orderId: dataBundle.orderId,
                userId: dataBundle.userId,
                orderNumber: dataBundle.orderNumber
            })
            .setContext({
                order_id: dataBundle.orderId,
                order_number: dataBundle.orderNumber,
            })
            .get()
    }

    // ... continue define here
}