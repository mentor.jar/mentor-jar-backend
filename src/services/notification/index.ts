import NotificationRepo from '../../repositories/NotificationRepo';
import NotifyCampaignRepo from '../../repositories/NotifyCampaignRepo';
import * as NotifyType from './constants';
import NotificationCore from './core';
import * as NotifyInterface from './interface';
import FCMPushService from './module/push';
import NotifyRequestBuilder from './module/builder';
import NotifyRequestFactory from './module/factory';
import { NotificationHelper } from './module/helper';

const storageService = NotificationRepo.getInstance();
const campaignService = NotifyCampaignRepo.getInstance();
const pushService = new FCMPushService();
const notificationService = new NotificationCore(pushService, storageService, campaignService);

export default notificationService;
export {
    NotifyType,
    NotifyInterface,
    NotifyRequestBuilder,
    NotifyRequestFactory,
    NotificationHelper
};

