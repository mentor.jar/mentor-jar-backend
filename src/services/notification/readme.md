### Notification service

Notification integration instruction

### 1. Basic usage

To send a notification, we need import notification service, and its additional resource like:   
- NotifyType: Define type supported
- NotifyInterface: Export interface
- NotifyRequestBuilder: To create request easier
- NotifyRequestFactory: To make create request in one place

```
import notificationService, { NotifyType, NotifyInterface, NotifyRequestBuilder, NotifyRequestFactory } from "../../../../services/notification";
```
In notificationService we export main method for push notification

- ```saveAndSend```: send and save notification to db
- ```send```: just send notification, not save to db

Both method receive first argument is a **PushMessageRequest** instance
You can import it from **NotifyInterface** or directly from ```interface.ts``` 

Let focus to **PushMessageRequest**
```
interface PushMessageRequest {
  type?: String,
  contentType?: String,
  receiverId: String | Array<String>,
  creatorId?: String,
  title?: String,
  content?: Object,
  data?: Object,
  context?: any,
  os?: String,
  image?: String,
  groupId?: String,
  sound?: String,
  campaignId?: String
}
```
Explain

- type: Notification type, which defined in ```NOTIFY_TYPE``` of ```constants.ts``` file
- contentType: for auto generator content, rarely used
- receiverId: can be a String or an array of String, the id of user who recieved
- creatorId: the create notification id
- title: the push title, default is ```DEFAULT_TITLE``` of ```constants.ts``` file
- content: the push body, default is ```DEFAULT_CONTENT``` of ```constants.ts``` file
- data: ***the data payload which will be read by mobile***, that correspond to [data field in document](https://bithumb.atlassian.net/wiki/spaces/BF/pages/793182209/Notification+Document#b.-data-field-convention)
- context: save additional data like: order_id, shipping_method_id,...
- os: specify a os to send
- image: image attached, should be a image URL
- groupId: not use yet
- sound: not use yet
- campaignId: specify a campaign id for marketing

Example of a push with PushMessageRequest

```
import notificationService, { NotifyType, NotifyInterface, NotifyRequestBuilder, NotifyRequestFactory } from "../../../../services/notification";

...

// send remind livestream notification for each stream session
const sendBatch = streamSessions.map((session) => notificationService.saveAndSend({
    receiverId: session.user_id.toString(),
    content: 'Bạn có lịch cho livestream' + session.name + ' trong 30 phút sắp tới',
    title: 'Bidu Livestream',
    data:{
        type:NotifyType.NOTIFY_TYPE.EVENTS,
        streamId: session.id,
        name: session.name
    }
    type: NotifyType.NOTIFY_TYPE.EVENTS,
    contentType: NotifyType.NOTIFY_CONTENT_TYPE.LIVE_STREAM_REMIND,
    image: session.image
}))

await Promise.all(sendBatch);

// or send single request

await notificationService.saveAndSend({
    receiverId: request.receiverId,
    contentType: request.contentType,
    type: NotifyType.NOTIFY_TYPE.EVENTS,
    creatorId: userId,
    content: 'Xin chào',
    title: 'Bidu Livestream',
    data:{
        type:NotifyType.NOTIFY_TYPE.EVENTS,
        streamId: session.id,
        name: session.name
    }
    type: NotifyType.NOTIFY_TYPE.EVENTS,
})

```

### 2. With Builder and Factory Helper

We provide the notification builder with builder stype **NotifyRequestBuilder**

```
notificationService.saveAndSend(
    new NotifyRequestBuilder()
        .setReceiverId(session.user_id.toString())
        .setType(NotifyType.NOTIFY_TYPE.EVENTS_LIVE_STREAM_REMIND)
        .setContent('Bạn có lịch cho livestream ' + session.name + ' trong 30 phút sắp tới')
        .setImage(session.image)
        .setData({
            streamId: session.id,
            name: session.name
        })
        .get()
)
```

An also provider a one place define is **NotifyRequestFactory**. We show how we use it following

1. Define in switch case of ```generate(type, dataBundle)``` function

```
switch (type) {
    case NOTIFY_TYPE.EVENTS_LIVE_STREAM_REMIND:
        return this.liveStreamRemind(dataBundle);
    case NOTIFY_TYPE.ORDERS_ACCEPT:
        return this.orderAccept(dataBundle);
    // ... continue at type here
    default: throw new Error("Unsupport factory type");
}
```
2. define a private static method conrespond
```
// dataBundle is an object have anything we want
// We can use raw object here
private static liveStreamRemind(dataBundle): PushMessageRequest {
    return {
        receiverId: dataBundle.receiverID,
        type: NOTIFY_TYPE.EVENTS_LIVE_STREAM_REMIND,
        data: {
            streamId: dataBundle.streamId,
            streamName: dataBundle.streamName,
        }
    }
}

// Or use NotifyRequestBuilder
// with defined structure of data { receiverID, streamId, streamName }
private static orderAccept(dataBundle: { receiverID, streamId, streamName }): PushMessageRequest {
    return new NotifyRequestBuilder()
        .setType(NOTIFY_TYPE.ORDERS_ACCEPT)
        .setReceiverId(dataBundle.receiverID)
        .setData({
            orderId: dataBundle.orderId,
            userId: dataBundle.userId,
        })
        .get()
}
```
3. Use it in anywhere you want
```
import notificationService, { NotifyType, NotifyInterface, NotifyRequestBuilder, NotifyRequestFactory } from "../../../../services/notification";

notificationService.saveAndSend(
    NotifyRequestFactory.generate(
        NotifyType.ORDERS_ACCEPT,
        {
            receiverID : 'the_receiverid'
            orderId: 'the_order_id',
            userId: 'the_user_id'
        }
    ))

```

### 3. Note

#### a. Data reverse word
Some keyword can not be able to use in data and will cause of run time error [read more](https://firebase.google.com/docs/cloud-messaging/http-server-ref#downstream-http-messages-json)
To keep program stable, we will ignore those field when it's exist in ```data``` object
It be defined following
```
export const RESERVED_KEYWORD = [
  'from',
  'message_type',
  'registration_id',
  'collapse_key',
  'time_to_live',
  'restricted_package_name',
  'dry_run'
];

```


#### b. App user

Execpt the chat notification, all notification in all follow rule
***only send to the last active device*** that mean when you have already logined but your account is logined from any where, notification will send to the latest login device.

#### c. Documentation 

The offcial document is pushished to [confluence link](https://bithumb.atlassian.net/wiki/spaces/BF/pages/793182209/Notification+Document)
When add some new notification type, please update that document for other developer know how to integrate it.