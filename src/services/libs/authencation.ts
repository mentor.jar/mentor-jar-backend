
import bcrypt from 'bcryptjs'
import jwt from 'jsonwebtoken'
import keys from '../../config/env/keys'
export const expireTime = 365
const expiresIn = `${expireTime}d`
export const comparePassword = (passwordAttempt, user) => {
    return new Promise((resolve, reject) => {
        bcrypt.compare(passwordAttempt, user.password, (err, isMatch) => err ? reject(err) : resolve(isMatch))
    })
}

export function getAccessToken(user) {
    const token = jwt.sign({ id: user._id }, keys.jwtSecret, { expiresIn: expiresIn })
    return token
}