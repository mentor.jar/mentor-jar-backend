import redis from "redis";

export const client = redis.createClient({
    port: process.env.REDIS_PORT || 6379,
    host: process.env.REDIS_HOST || '103.69.193.161',
    password: process.env.REDIS_PASSWORD
});
// export const client = redis.createClient();

client.on("error", function (err) {
    console.log("Error " + err);
});