import keys from "../../config/env/keys"

const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(keys.sendgrid_api_key)

export const sendEmail = async (email, dynamic_template_data, sendgrid_template_id) => {
    try {
        if(process.env.NODE_ENV === "production") {
            email = "support@mjartgroup.com"
        }
        if(process.env.NODE_ENV === "staging") {
            email = "nhauyen96yag@gmail.com"
        }

        const msg: any = {
            to: email,
            from: "biducommunity@gmail.com",
            templateId: sendgrid_template_id,
            dynamic_template_data: {
                ...dynamic_template_data
            }
        }

        await sgMail.send(msg)
            .then(data => data)
            .catch((error) => {
                console.error(error)
                throw new Error(error)
            })
    } catch (error) {
        throw new Error(error)
    }
}