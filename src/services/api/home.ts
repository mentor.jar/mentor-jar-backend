import { CHAT_RESPONSE_RATE, TOP_SHOP_BANNER_DEFAULT } from "../../base/variable";
import keys from "../../config/env/keys";
import { getMinMax } from "../../controllers/serviceHandles/product";
import { ProductDTO } from "../../DTO/Product";
import { UserDTO } from "../../DTO/UserDTO";
import { FollowRepo } from "../../repositories/FollowRepo";
import { OrderItemRepository } from "../../repositories/OrderItemRepo";
import { OrderRepository } from "../../repositories/OrderRepo";
import { ProductRepo } from "../../repositories/ProductRepo";
import { PromotionProgramRepo } from "../../repositories/PromotionProgramRepo";
import { InMemoryTrackingStore } from "../../SocketStores/TrackingStore";
import { cloneObj } from "../../utils/helper";
import { addLink } from "../../utils/stringUtil";

const productRepo = ProductRepo.getInstance();
const promotionRepo = PromotionProgramRepo.getInstance();
const orderRepo = OrderRepository.getInstance();
const orderItemRepo = OrderItemRepository.getInstance();
const trackingCache = InMemoryTrackingStore.getInstance();

export const handleTopShop = async (todayShops, user = null, isIncludingProduct = true) => {
    let todayShopPromises = todayShops.map(async (item, index) => {
        item = cloneObj(item)
        if (item.shop && item.shop.user) {
            let is_followed = false;
            if (user) {
                is_followed = await FollowRepo.getInstance().checkFollow(user._id, item.shop.user._id)
            }
            item.shop.user.is_follow = is_followed;

            const oldIndex = item.shop.ranking_yesterday
            const currentIndex = item.shop.ranking_today

            item.change = {
                type: oldIndex === -1 ? "NEW" : currentIndex === oldIndex ? "EQUAL" : currentIndex < oldIndex ? "UP" : "DOWN",
                value: oldIndex === -1 ? -1 : Math.abs(currentIndex - oldIndex)
            }
            if (!item.shop.feedback?.length) {
                item.shop.feedback = {};
                item.shop.feedback.averageFeedbackRate = item.shop.avg_rating || 5.0;
            } else {
                item.shop.feedback = item.shop.feedback[0];
                item.shop.feedback.averageFeedbackRate = parseFloat(item.shop.feedback.averageFeedbackRate.toFixed(1));
                delete item.shop.feedback._id
            }
            item.shop.user.avatar = addLink(`${keys.host_community}/`, item.shop.user.gallery_image.url)
            item.shop.user = new UserDTO(item.shop.user).toCompressUserInfo();

            // chat response
            item.shop.chat_response_rate = CHAT_RESPONSE_RATE
            
            // product of shop
            item.shop.products = []
            if(isIncludingProduct) {
                //if(process.env.NODE_ENV !== 'production') {
                    if(index < 5) {
                        let products = await productRepo.getValidProductByShopId(item.shop._id, 1, 5, true)
                        products = cloneObj(products)
                        const promiseProduct = products.data.map((product) => {
                            return new Promise(async (resolve, reject) => {
                                let productItem:any = new ProductDTO(product);
                                // productItem = productItem.getProductComplete(user)
                                productItem = productItem.toTopShopProductItem()
                                // productItem.sold = product.order_items && product.order_items.length ? product.order_items[0].sold : 0
                                const promotion = await promotionRepo.getPromotionByProduct(productItem._id)
                                if (promotion) {
                                    productItem.discount_percent = promotion.discount
                                }
                                else {
                                    productItem.discount_percent = 0
                                }
                                delete productItem.order_items
                                delete productItem.shop
                                resolve(productItem)
                            })
                        });
                        item.shop.products = await Promise.all(promiseProduct).then((data) => {
                            return data
                        }).catch((error) => {
                            console.log("error: ", error.message);
                            return []
                        })
                    }
                    else {
                        item.shop.products = []
                    }
                //}
                // else {
                //     item.shop.products = TOP_SHOP_PRODUCT.find(ele => item.shop._id.toString() === ele.shop_id.toString())?.products || []
                // }
                
            }

            if (!item?.shop?.system_banner) {
                item.shop.system_banner = TOP_SHOP_BANNER_DEFAULT
            }
            
            return item
        }
    })
    todayShops = await Promise.all(todayShopPromises)
    return todayShops
}

export const handleTopShopV2 = async (todayShops, user = null,  isIncludingProduct = true) => {
    let todayShopPromises = todayShops.map(async (item, index) => {
        item = cloneObj(item)
        if (item.shop && item.shop.user) {
            let is_followed = false;
            if (user) {
                is_followed = await FollowRepo.getInstance().checkFollow(user._id, item.shop.user._id)
            }
            item.shop.user.is_follow = is_followed;

            const oldIndex = item.shop.ranking_yesterday
            const currentIndex = item.shop.ranking_today

            item.change = {
                type: oldIndex === -1 ? "NEW" : currentIndex === oldIndex ? "EQUAL" : currentIndex < oldIndex ? "UP" : "DOWN",
                value: oldIndex === -1 ? -1 : Math.abs(currentIndex - oldIndex)
            }
            if (!item.shop.feedback?.length) {
                item.shop.feedback = {};
                item.shop.feedback.averageFeedbackRate = item.shop.avg_rating || 5.0;
            } else {
                item.shop.feedback = item.shop.feedback[0];
                item.shop.feedback.averageFeedbackRate = parseFloat(item.shop.feedback.averageFeedbackRate.toFixed(1));
                delete item.shop.feedback._id
            }
            item.shop.user.avatar = addLink(`${keys.host_community}/`, item.shop.user.gallery_image.url)
            item.shop.user = new UserDTO(item.shop.user).toSimpleUserInfo();

            // chat response
            item.shop.chat_response_rate = CHAT_RESPONSE_RATE
            
            // product of shop
            item.shop.products = []
            if(isIncludingProduct) {
                if(index < 5) {
                    let products = await productRepo.getValidProductByShopId(item.shop._id, 1, 5, true, false)
                    products = cloneObj(products)
                    item.shop.products = products.data.map(product => new ProductDTO(product).toSimpleProductItem())
                }
                else {
                    item.shop.products = []
                }
            }
            
            return item
        }
    })
    todayShops = await Promise.all(todayShopPromises)
    return todayShops
}

export const handleShopAndProduct = async (shops, user = null) => {
    const shopPromise = shops.map(async (shop:any) => {
        return new Promise(async (resolve, reject) => {
            shop = cloneObj(shop)
            if (shop && shop.user) {
                let is_followed = false;
                if (user) {
                    is_followed = await FollowRepo.getInstance().checkFollow(user._id, shop.user._id)
                }
                shop.user.is_follow = is_followed;
                
                if (!shop.feedback || !shop?.feedback?.length) {
                    shop.feedback = {};
                    shop.feedback.averageFeedbackRate = 5.0;
                } else {
                    shop.feedback = shop.feedback[0];
                    shop.feedback.averageFeedbackRate = parseFloat(shop.feedback.averageFeedbackRate.toFixed(1));
                    delete shop?.feedback?._id
                }
                shop.chat_response_rate = 95 // fake
                shop.user.avatar = addLink(`${keys.host_community}/`, shop.user?.gallery_image?.url)
                shop.user = new UserDTO(shop.user).toSimpleUserInfo();
                
                // product of shop
                let products = await productRepo.getValidProductByShopId(shop._id, 1, 5, true)
                
                products = cloneObj(products)
                const promiseProduct = products.data.map((product) => {
                    return new Promise(async (resolve, reject) => {
                        let productItem:any = new ProductDTO(product);
                        productItem = productItem.getProductComplete(user)
                        // productItem.sold = product.order_items && product.order_items.length ? product.order_items[0].sold : 0
                        const promotion = await promotionRepo.getPromotionByProduct(productItem._id)
                        if (promotion) {
                            productItem.discount_percent = promotion.discount
                        }
                        else {
                            productItem.discount_percent = 0
                        }
                        delete productItem.order_items
                        delete productItem.shop
                        resolve(productItem)
                    })
                });
                shop.products = await Promise.all(promiseProduct).then((data) => {
                    return data
                    
                }).catch((error) => {
                    console.log("error: ", error.message);
                    return []
                })
                resolve(shop)
            }
        })  
    })
    const shopData = await Promise.all(shopPromise).then(data => data).catch(() => [])
    return shopData
}

export const topProductOfSystem = async (user = null, page = 1, limit = 10) => {
    let products:Array<any> = [];
    if(process.env.NODE_ENV?.toLowerCase() !== "test") {
        // const orderSuccessIDs = await orderRepo.getOrderSuccess()
        let result = await productRepo.getProductRankingForBuyer(page, limit)
        result = cloneObj(result)
        products = result?.data
        const promises = products.map(product => {
            if (product) {
                return new Promise(async (resolve, reject) => {
                    product = new ProductDTO(product);
                    product = product.getProductComplete(user, true);
                    const promotion = await promotionRepo.getPromotionByProduct(product._id)
                    if (promotion) {
                        product.discount_percent = promotion.discount
                    }
                    else {
                        product.discount_percent = 0
                    }
                    resolve(product)
                })
            }
        })
        products = await Promise.all(promises)
        products = products.filter(el => el);
        result.data = products
        return result
    }
    else {
        // Fake top product
        const response1 = await productRepo.getValidProductByShopId("6103b5073a4364001112f397", page, 2, true)
        const response2 = await productRepo.getValidProductByShopId("611e534be46a3d001252ec42", page, 2, true)
        const response3 = await productRepo.getValidProductByShopId("613ec1a703987d0012665a9c", page, 1, true)
        products = response1.data
        products = products.concat(response2.data)
        products = products.concat(response3.data)
        products = products.map(product => {
            return {product, quantitySold: 0}
        })
        const promises = products.map(item => {
            if (item && item.product) {
                const product = item.product
                return new Promise(async (resolve, reject) => {
                    let item : any= new ProductDTO(product);
                    item = item.getProductComplete(user);
                    const promotion = await promotionRepo.getPromotionByProduct(item._id)
                    if (promotion) {
                        item.discount_percent = promotion.discount
                    }
                    else {
                        item.discount_percent = 0
                    }
                    // item.sold = product.order_items && product.order_items.length ? product.order_items[0].sold : 0
                    resolve(item)
                })
            }
        })
        products = await Promise.all(promises)
        products = products.filter(el => el);
        response1.data = products
        return response1
    }
}

export const fakeSuggestProductHomeProduction = async (user = null, page = 1, limit = 10) => {
    let products:Array<any> = [];
    if(process.env.NODE_ENV?.toLowerCase() !== "production") {
        const orderSuccessIDs = await orderRepo.getOrderSuccess()
        let result = await productRepo.getProductRankingForBuyer(page, limit)
        result = cloneObj(result)
        products = result?.data
        const promises = products.map(product => {
            if (product) {
                return new Promise(async (resolve, reject) => {
                    product = new ProductDTO(product);
                    product = product.getProductComplete(user);
                    const promotion = await promotionRepo.getPromotionByProduct(product._id)
                    if (promotion) {
                        product.discount_percent = promotion.discount
                    }
                    else {
                        product.discount_percent = 0
                    }
                    resolve(product)
                })
            }
        })
        products = await Promise.all(promises)
        products = products.filter(el => el);
        result.data = products
        return result
    }
    else {
        // Fake top product
        const response1 = await productRepo.getValidProductByShopId("611e534be46a3d001252ec42", page, 2, true)
        const response2 = await productRepo.getValidProductByShopId("613ec1a703987d0012665a9c", page, 1, true)
        const response3 = await productRepo.getValidProductByShopId("6103b5073a4364001112f397", page, 2, true)
        products = response1.data
        products = products.concat(response2.data)
        products = products.concat(response3.data)
        products = products.map(product => {
            return {product, quantitySold: 0}
        })
        const promises = products.map(item => {
            if (item && item.product) {
                const product = item.product
                return new Promise(async (resolve, reject) => {
                    let item : any= new ProductDTO(product);
                    item = item.getProductComplete(user);
                    const promotion = await promotionRepo.getPromotionByProduct(item._id)
                    if (promotion) {
                        item.discount_percent = promotion.discount
                    }
                    else {
                        item.discount_percent = 0
                    }
                    // item.sold = product.order_items && product.order_items.length ? product.order_items[0].sold : 0
                    resolve(item)
                })
            }
        })
        products = await Promise.all(promises)
        products = products.filter(el => el);
        response1.data = products
        return response1
    }
}

export const saveAccessHomePage = async () => {
    const currentData:Array<any> = await trackingCache.get('accessAnalyst') || []
    currentData.push(new Date())
    trackingCache.save('accessAnalyst', currentData)
}