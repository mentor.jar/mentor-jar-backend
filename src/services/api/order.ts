import { resolve } from "path"
import { KEY } from "../../constants/cache"
import { updateQuantityCancelOrder } from "../../controllers/api/mobile_app/orders"
import UserService from "../../controllers/serviceHandles/user"
import { CancelType, ShippingStatus } from "../../models/enums/order"
import { TargetType, AdHoc } from "../../models/enums/TrackingObject"
import { AddressRepo } from "../../repositories/AddressRepo"
import { CartItemRepo } from "../../repositories/CartItemRepo"
import { GroupOrderRepo } from "../../repositories/GroupOrderRepo"
import { OrderItemRepository } from "../../repositories/OrderItemRepo"
import { OrderRepository } from "../../repositories/OrderRepo"
import { OrderShippingRepo } from "../../repositories/OrderShippingRepo"
import { ProductRepo } from "../../repositories/ProductRepo"
import { ShopRepo } from "../../repositories/ShopRepo"
import { TrackingObjectRepo } from "../../repositories/TrackingObjectRepo"
import { UserRepo } from "../../repositories/UserRepo"
import { VariantRepo } from "../../repositories/Variant/VariantRepo"
import { VoucherRepository } from "../../repositories/VoucherRepository"
import { InMemoryGBOrderPaymentStore } from "../../SocketStores/GroupBuyOrderPaymentStore"
import { InMemoryTrackingStore } from "../../SocketStores/TrackingStore"
import { InMemoryVoucherStore } from "../../SocketStores/VoucherStore"
import { cloneObj, isMappable, ObjectId, timeOutOfPromise } from "../../utils/helper"
import { generateOrderNumber, makeShortenUrl } from "../../utils/utils"
import { getUserLanguage, getTranslation } from "../i18nCustom"
import notificationService, { NotifyRequestFactory } from "../notification"
import { NOTIFY_TYPE, NOTIFY_TARGET_TYPE } from "../notification/constants"

const orderRepo = OrderRepository.getInstance()
const userRepo = UserRepo.getInstance()
const voucherRepo = VoucherRepository.getInstance()
const groupOrderRepo = GroupOrderRepo.getInstance();
const trackingObjectRepo = TrackingObjectRepo.getInstance();
const shopRepo = ShopRepo.getInstance();
const addressRepo = AddressRepo.getInstance();
const variantRepo = VariantRepo.getInstance();
const productRepo = ProductRepo.getInstance();
const orderItemRepo = OrderItemRepository.getInstance();
const cartItemRepo = CartItemRepo.getInstance();
const orderShippingRepo = OrderShippingRepo.getInstance();
const trackingCache = InMemoryTrackingStore.getInstance();
const voucherAndOrderCache = InMemoryVoucherStore.getInstance();
const gbOrderPaymentStore = InMemoryGBOrderPaymentStore.getInstance();

export const handleUserAfterCancelOrder = async (userID) => {
    const orderQuantity = await orderRepo.getOrderNotCanceledByUser(userID)
    const user = await userRepo.findOne({_id: ObjectId(userID)})
    
    if(user) {
        await userRepo.update(userID, {
            is_newbie: orderQuantity.length === 0
        })
    }
}

export const setIsNewbieForOldUser = async () => {
    let users = await userRepo.find({})
    users = cloneObj(users)
    const promises = users.map(user => {
        return new Promise(async (resolve, reject) => {
            await handleUserAfterCancelOrder(user._id)
        })
    })
}

export const checkOrderNumber = async (orderNumber) => {
    console.log("Running check order number: ", orderNumber); // keep it for debugging
    const getOrderNumberPromises = [
        timeOutOfPromise(3000, {
            result: []
        }),
        (async () => {
            const data = await voucherAndOrderCache.get("orderNumberUseds")
            return {
                result: data
            }
        })()
    ]
    let orderNumberList:any = await Promise.race(getOrderNumberPromises).then((data:any) => {
        return data.result
    })
    if(orderNumberList.length) {
        console.log("Find in cache"); // keep it for debugging
        return orderNumberList.find(ele => orderNumber === ele)
    }
    else {
        console.log("Find in database"); // keep it for debugging
        const order = await orderRepo.findOne({ order_number: orderNumber })
        return order ? true : false
    }
}

export const reCheckSomeOrderCondition = async (data) => {
    console.time("reCheck")
    const { userID, voucherLists } = data;
    const promises = []
    voucherLists.forEach(voucher => {
        if(voucher?.conditions?.limit_per_user) {
            const promiseItem = new Promise(async (resolve, reject) => {
                const orders = await orderRepo.find({
                    user_id: ObjectId(userID),
                    "vouchers._id": voucher._id.toString()
                })
                const groupOrder = []
                orders.forEach(order => {
                    const id = order.group_order_id.toString()
                    if(!groupOrder.includes(id)) {
                        groupOrder.push(id)
                    }
                })
                resolve({
                    voucher_id: voucher._id,
                    limit: voucher?.conditions?.limit_per_user,
                    used: groupOrder.length
                })
            })
            promises.push(promiseItem)
        }
    })
    let voucherFromCache = await Promise.all(promises).then(data => data).catch(err => {
        console.log("Error re-check voucher before create order");
        return []
    })

    const isValid = voucherFromCache.some(item => item.used < item.limit)
    console.timeEnd("reCheck")
    return isValid
}

/**
 * Run in queue
 * @param voucherLists 
 * @param userID 
 */
export const handleVoucherAfterOrderCreated = async (job, done) => {
    const { voucherLists, userID } = job.data
    const voucherUsed: Array<any> = []
    for(let it = 0; it < voucherLists.length; it++) {
        const voucher = voucherLists[it]
        if(!voucherUsed.includes(voucher._id.toString())) {
            let voucherOrder: any = null
            voucherOrder = voucherLists.find(ele => ele._id.toString() === voucher._id.toString())
            if(!voucherOrder) {
                voucherOrder = await voucherRepo.findOne({ _id: ObjectId(voucher._id) })
            }
            voucherOrder = cloneObj(voucherOrder)
            let usedBy = voucherOrder.used_by
            if (usedBy) {
                usedBy[userID] ? usedBy[userID] += 1 : usedBy[userID] = 1
            }
            else {
                usedBy = {}
                usedBy[userID] = 1
            }
            if (voucherOrder) {
                const newVoucher = {
                    $inc: { available_quantity: -1 },
                    used_by: usedBy || {}
                }
                voucherOrder = await voucherRepo.update(voucherOrder._id, newVoucher)
                voucherUsed.push(voucherOrder._id.toString())
            }
        }
    }
    done()
}

export const createOrderQueue = async (job, done) => {
    console.log("Running")
    let request = job.data

    request = cloneObj(request)

    const { orderInfo, shops, payment_method_id, system_voucher, total_order_items, userID, 
        groupOrderID, voucherLists, dataItems, order_room_id, ref_link, ref_from_id, avatar_url, group_id } = request
    
    
    
    const isGroupBuyOrder = order_room_id ? true : false
    console.log("new data: ", order_room_id, ref_link, isGroupBuyOrder);

    const isValidOrder = reCheckSomeOrderCondition({ userID, voucherLists })
    if(!isValidOrder) {
        const language = await getUserLanguage(userID);
        notificationService.saveAndSend(
            NotifyRequestFactory.generate(
                NOTIFY_TYPE.ORDER_INVALID,
                NOTIFY_TARGET_TYPE.BUYER,
                {
                    title: getTranslation(language, 'notification.bidu_notification'),
                    content: getTranslation(language, 'notification.order_invalid'),
                    receiverId: userID.toString(),
                    userId: userID.toString()
                }
            ))
        done()
        return;
    }
    
    const getOrderNumberPromises = [
        timeOutOfPromise(3000, {
            result: []
        }),
        (async () => {
            const data = await voucherAndOrderCache.get("orderNumberUseds")
            return {
                result: data
            }
        })()
    ]
    let orderNumberList:any = await Promise.race(getOrderNumberPromises).then((data:any) => {
        return data.result
    })
    
    // update voucher
    const voucherUsed: Array<any> = []
    for(let it = 0; it < voucherLists.length; it++) {
        const voucher = voucherLists[it]
        if(!voucherUsed.includes(voucher._id.toString())) {
            let voucherOrder: any = null
            voucherOrder = voucherLists.find(ele => ele._id.toString() === voucher._id.toString())
            if(!voucherOrder) {
                voucherOrder = await voucherRepo.findOne({ _id: ObjectId(voucher._id) })
            }
            voucherOrder = cloneObj(voucherOrder)
            let usedBy = voucherOrder.used_by
            if (usedBy) {
                usedBy[userID] ? usedBy[userID] += 1 : usedBy[userID] = 1
            }
            else {
                usedBy = {}
                usedBy[userID] = 1
            }
            if (voucherOrder) {
                const newVoucher = {
                    $inc: { available_quantity: -1 },
                    used_by: usedBy || {}
                }
                if(voucherOrder.classify === 'cash_back') {
                    newVoucher[`using_by.${userID}`] = 1
                }
                else if(voucherOrder.classify === 'cash_back_discount') {
                    const updateCashBackRef = {}
                    const cashBackID = voucherOrder?.cash_back_ref?.cash_back_id
                    updateCashBackRef[`using_by.${userID}`] = 0
                    await voucherRepo.update(cashBackID, updateCashBackRef)
                }
                voucherOrder = await voucherRepo.update(voucherOrder._id, newVoucher)
                voucherUsed.push(voucherOrder._id.toString())
            }
        }
    }

    const { address } = orderInfo
    const order = {
        payment_status: 'pending',
        shipping_status: 'pending',
        vouchers: [],
        total_price: 0,
        payment_method_id,
        user_id: userID,
        handle_by: null,
        shop_id: null,
        address,
        total_value_items: 0,
        shipping_fee: 0,
        shipping_discount: 0,
        voucher_discount: 0,
        note: {},
        order_service: null,
        is_group_buy_order: isGroupBuyOrder
    }
    let discountVoucher: any = null
    let shippingVoucher: any = null
    let cashBackVoucher: any = null
    let cashBackDiscountVoucher: any = null
    if (system_voucher) {
        discountVoucher = system_voucher.filter(voucher => voucher.classify === 'discount')[0] || null
        shippingVoucher = system_voucher.filter(voucher => voucher.classify === 'free_shipping')[0] || null
        cashBackVoucher = system_voucher.filter(voucher => voucher.classify === 'cash_back')[0] || null
        cashBackDiscountVoucher = system_voucher.filter(voucher => voucher.classify === 'cash_back_discount')[0] || null
    }

    const newOrderNumbers = []
    const promises = await shops.map(shop => {
        return new Promise(async (resolve, reject) => {
            const { shop_id, shipping_method_id, items, vouchers, shipping_fee, shipping_discount, note, order_service } = shop
            let { total_price, voucher_discount, total_value_items } = shop
            total_value_items = +total_value_items
            let voucherForOrder = vouchers || []
            const shopRatioOrderPrice: any = (total_value_items / total_order_items) > 1 ? 1.00 : (total_value_items / total_order_items).toFixed(2)
            if (discountVoucher) {
                const shopDiscount = {...discountVoucher}
                const discountValue: any = (+discountVoucher.discount * shopRatioOrderPrice).toFixed(0)
                shopDiscount.discount = +discountValue
                voucherForOrder = voucherForOrder.concat([shopDiscount])
            }
            if (shippingVoucher) {
                voucherForOrder = voucherForOrder.concat([shippingVoucher])
            }
            if (cashBackVoucher) {
                voucherForOrder = voucherForOrder.concat([cashBackVoucher])
            }
            if (cashBackDiscountVoucher) {
                const cashBackShopDiscount = {...cashBackDiscountVoucher}
                const discountForShop: any = (+cashBackDiscountVoucher.discount * shopRatioOrderPrice).toFixed(0)
                cashBackShopDiscount.discount = +discountForShop
                voucherForOrder = voucherForOrder.concat([cashBackShopDiscount])
            }
            voucher_discount = voucherForOrder
                .filter(item => !item.classify || (item.classify !== "free_shipping" && item.classify !== "cash_back"))
                .reduce((sum, item) => sum += item.discount, 0) || 0

            // Make sure voucher_discount can't be bigger than total value items
            voucher_discount = Math.min(total_value_items, voucher_discount)
            voucher_discount = voucher_discount.toFixed(0)

            // Get pick address default of shop
            let shopInfo: any = await shopRepo.findOne({ _id: ObjectId(shop_id) })
            let pick_address = await addressRepo.findOne({ accessible_id: shopInfo.user_id.toString(), is_pick_address_default: true })
            pick_address = cloneObj(pick_address)

            // Order number
            let order_number = generateOrderNumber()
            while (await checkOrderNumber(order_number)) {
                order_number = generateOrderNumber();
            }

            // Make sure total_price always including shipping fee if has
            total_price = (total_value_items - voucher_discount) > 0 ? (total_value_items - voucher_discount).toFixed(0) : 0
            total_price = +total_price + +shipping_fee - Math.min(+shipping_fee, +shipping_discount)
            total_price = total_price.toFixed(0)

            const shopOrder = {
                ...order,
                shop_id,
                shipping_method_id,
                vouchers: voucherForOrder,
                total_price,
                total_value_items,
                shipping_fee,
                shipping_discount,
                voucher_discount,
                note,
                pick_address,
                group_order_id: ObjectId(groupOrderID),
                order_number,
                order_service: order_service
            }

            // Create order
            let orderCreated: any = await orderRepo.create(shopOrder)
            orderCreated = cloneObj(orderCreated)
            orderCreated.items = []
            if (orderCreated) {
                newOrderNumbers.push(order_number)
                // Create order_item
                for(let i = 0; i < items.length; i++) {
                    // const now = new Date()
                    const item = items[i]
                    const { product_id, variant_id, quantity } = item
                    let variant: any = null
                    /// Get variant and product info
                    if (variant_id) {
                        variant = dataItems.find(ele => ele.variant?._id.toString() === variant_id)?.variant
                        if(!variant) {
                            console.log("Phải tìm biến thể ở db thôi");
                            variant = await variantRepo.findOne({ _id: ObjectId(variant_id) })
                        }
                        variant = cloneObj(variant)
                    }

                    let product: any = null
                    product = dataItems.find(ele => ele.product?._id.toString() === product_id)?.product
                    if(!product) {
                        // console.log("Phải tìm sản phẩm ở db thôi");
                        product = await productRepo.findOne({ _id: ObjectId(product_id) })
                    }
                    
                    product = cloneObj(product)

                    // Update variant and product quantity and create order items
                    const [newVariant, newProduct, orderItem] = await Promise.all([
                        (async () => {
                            if(variant) {
                                const result = await variantRepo.update(variant._id, {
                                    $inc: { quantity: -quantity }
                                })
                                return result
                            }
                        })(),
                        (async () => {
                            let newProduct:any = {
                                $inc: { quantity: -quantity },
                            }
                            const salePriceAvailable = product.sale_price_order_available && product.sale_price_order_available > 0
                                ? product.sale_price_order_available - quantity : product.sale_price_order_available
                            if(salePriceAvailable) {
                                newProduct = {
                                    $inc: { quantity: -quantity, sale_price_order_available: -quantity },
                                }
                            }
                            if (order_room_id) {
                                newProduct = {
                                    $inc: { quantity: -quantity, "group_buy.quantity": -quantity, "group_buy.sold": quantity },
                                }
                                if (variant_id) {
                                    product.group_buy.groups = product.group_buy.groups.map(value => {
                                        value.variants.map(item => {
                                            if (item.variant_id.toString() === variant_id.toString()) {
                                                item.quantity -= quantity
                                                item.sold += quantity
                                            }
                                        })
                                        return value
                                    })
                                    newProduct = {
                                        ...newProduct,
                                        "group_buy.groups": product.group_buy.groups
                                    }
                                }
                            }
                            const result = await productRepo.update(product._id, newProduct)
                            return result
                        })(),
                        (async () => {
                            const itemOrder = {
                                order_id: orderCreated._id,
                                product_id,
                                variant_id,
                                quantity,
                                variant: variant,
                                product: product,
                                images: product.images
                            }
                            const itemCreated = await orderItemRepo.create(itemOrder)
                            return itemCreated
                        })()
                    ])
                    
                    
                    if (orderItem) {
                        orderCreated.items.push(orderItem)
                    }
                    resolve(orderCreated)
                }


                // delete cartItem
                const cartIDsToBeDeleted = items.map(item => ObjectId(item._id))
                await cartItemRepo.deleteManyByID(cartIDsToBeDeleted);

                //create orderShipping
                const orderShipping = {
                    order_id: orderCreated._id,
                    user_id: orderCreated.user_id,
                    shop_id: orderCreated.shop_id
                }
                orderShippingRepo.create(orderShipping)
            }
        })
    });

    const orders: any = await Promise.all(promises).then(async (data) => {
        if(orderNumberList.length) {
            const newList = [...orderNumberList, ...newOrderNumbers]
            await voucherAndOrderCache.save("orderNumberUseds", newList) // must have await to make sure this process is completed before next process
        }
        userRepo.update(userID, {
            is_newbie: false
        })
        return data
    })

    // update groupOrder
    groupOrderRepo.update(groupOrderID, {
        orders,
        status: "completed",
        is_group_buy_order: isGroupBuyOrder
    })

    // just save data to redis with GroupBuy order
    if (isGroupBuyOrder) {
        gbOrderPaymentStore.set(KEY.STORE_GROUP_ORDER_OF_GB, {
            group_order_id: groupOrderID,
            ref_link,
            order_room_id,
            group_id,
            avatar_url,
            ref_from_id,
        })
    }
    //send notification to shop
    const sendBatch = orders.map((order) => {
        return new Promise(async (resolve, reject) => {
            try {
                const userShop = await userRepo.getUserByShopId(order.shop_id);
                const language = await getUserLanguage(userShop._id);
                await notificationService.saveAndSend(
                    NotifyRequestFactory.generate(
                        NOTIFY_TYPE.ORDERS_CREATED,
                        NOTIFY_TARGET_TYPE.SELLER,
                        {
                            title: getTranslation(language, 'notification.bidu_notification'),
                            content: getTranslation(language, 'notification.seller_order.order_created', order.order_number),
                            receiverId: userShop._id.toString(),
                            orderId: order._id.toString(),
                            userId: order.user_id.toString(),
                            orderNumber: order.order_number,
                        }
                    ))

                // Insert Table TrackingObject Seller
                // const currentTrackingCache = await trackingCache.get('trackings') || []
                // currentTrackingCache.push({
                //     user_id: userShop._id,
                //     target_type: TargetType.ORDER,
                //     target_id: order._id,
                //     ad_hoc: AdHoc.ORDER_MANAGE
                // }, {
                //     user_id: order.user_id,
                //     target_type: TargetType.ORDER,
                //     target_id: order._id,
                //     ad_hoc: AdHoc.ORDER_MANAGE
                // })
                // trackingCache.save('trackings', currentTrackingCache)
                // trackingObjectRepo.createOrUpdateTrackingObject(userShop._id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);
                // Insert Table TrackingObject Buyer
                // trackingObjectRepo.createOrUpdateTrackingObject(order.user_id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);
                resolve(true);
            } catch (error) {
                reject(error)
            }
        })
    });
    Promise.all(sendBatch);

    // To make sure some user conditions will be reseted in cache
    UserService._.clearUserInfoInCache(userID.toString())

    done()
    job.log("Done");
}

export const approveRejectOrderQueue = async (job, done) => {
    const { orderID, is_approved, paymentIds } = job.data
    let order: any = await orderRepo.findOne({ _id: orderID });
    const { _id: orderId, vouchers, group_order_id: groupOrderId } = order;

    // Get Language
    const language = await getUserLanguage(order.user_id);

    let listVoucherSystem = [];
    if (isMappable(vouchers)) {
        listVoucherSystem = cloneObj(vouchers.filter((item) => item.classify && item.classify !== null))
    }

    let cancellableOrderList = [];
    if (isMappable(listVoucherSystem)) {
        let groupOrder = await groupOrderRepo.findById(groupOrderId.toString());
        groupOrder = cloneObj(groupOrder);

        const listOtherOrderBelongToGroupOrder = groupOrder?.orders?.filter(
            (item) => item._id.toString() !== orderId.toString()
        );
        
        const listOrderCanceledTogether = await Promise.all(
            listOtherOrderBelongToGroupOrder?.map(async (item: any) => 
                orderRepo.findOne({ _id: item._id })
            )
        )

        cancellableOrderList = await Promise.all(
            listOrderCanceledTogether.map(async (item: any) => {
                const orderShipping: any = await orderShippingRepo.findOne({ order_id: item._id });

                if (orderShipping && orderShipping.shipping_info && orderShipping.shipping_info.label) {
                    return 0;
                } else {
                    return item;
                }
            })
        )

        cancellableOrderList = cancellableOrderList.filter((item) => item !== 0);     
    } 

    if (is_approved == true) {
        // send notification
        await notificationService.saveAndSend(
            NotifyRequestFactory.generate(
                NOTIFY_TYPE.ORDERS_ACCEPT,
                NOTIFY_TARGET_TYPE.BUYER,
                {
                    title: getTranslation(
                        language,
                        'notification.bidu_notification'
                    ),
                    content: getTranslation(
                        language,
                        'notification.buyer_order.order_accepted',
                        order.order_number
                    ),
                    receiverId: order.user_id.toString(),
                    orderId: order._id.toString(),
                    userId: order.user_id.toString(),
                    orderNumber: order.order_number,
                }
            )
        );

        const languageSeller = await getUserLanguage(order.pick_address.accessible_id);
        await notificationService.saveAndSend(
            NotifyRequestFactory.generate(
                NOTIFY_TYPE.ORDERS_ACCEPT,
                NOTIFY_TARGET_TYPE.SELLER,
                {
                    title: getTranslation(
                        languageSeller,
                        'notification.bidu_notification'
                    ),
                    content: getTranslation(
                        languageSeller,
                        'notification.seller_order.order_accepted',
                        order.order_number
                    ),
                    receiverId: order.pick_address.accessible_id,
                    orderId: order._id.toString(),
                    userId: order.pick_address.accessible_id,
                    orderNumber: order.order_number,
                }
            )
        );

        await orderRepo.update(orderID, {
            shipping_status: ShippingStatus.WAIT_TO_PICK,
            approved_time: new Date(),
        });
    } else {
        await handleCanceledOrder(order, language);

        if (isMappable(listVoucherSystem) && isMappable(cancellableOrderList)) {
            Promise.all(
                cancellableOrderList.map(async(order) => {
                    handleCanceledOrder(order, language);
                })
            )
        }
    }

    done()
}

const handleCanceledOrder = async (order, language) => {
    await notificationService.saveAndSend(
        NotifyRequestFactory.generate(
            NOTIFY_TYPE.ORDERS_CANCELED,
            NOTIFY_TARGET_TYPE.BUYER,
            {
                title: getTranslation(
                    language,
                    'notification.bidu_notification'
                ),
                content: getTranslation(
                    language,
                    'notification.buyer_order.order_canceled',
                    order.order_number
                ),
                receiverId: order.user_id.toString(),
                orderId: order._id.toString(),
                userId: order.user_id.toString(),
                orderNumber: order.order_number,
            }
        )
    );

    await orderRepo.update(order._id, {
        shipping_status: ShippingStatus.CANCELED,
        cancel_reason: 'Admin từ chối đơn hàng',
        cancel_type: CancelType.SYSTEM,
        cancel_time: Date.now(),
    });

    updateQuantityCancelOrder(order._id);

    handleUserAfterCancelOrder(order.user_id);
};

export const generateOrderRoomReferralByUser = async () => {
    try {
        const getReferralLinkUsed = [
            timeOutOfPromise(3000, {
                result: []
            }),
            (async () => {
                const data = await voucherAndOrderCache.get("orderRoomReferralLinkUsed")
                return {
                    result: data
                }
            })()
        ]
        let orderRoomReferralLinkUsed:any = await Promise.race(getReferralLinkUsed).then((data:any) => {
            return data.result
        })
        let shortenLink = makeShortenUrl(10, "gb")
        if(orderRoomReferralLinkUsed && orderRoomReferralLinkUsed.length) {
            while (await orderRoomReferralLinkUsed.includes(shortenLink)) {
                shortenLink = makeShortenUrl(10, "gb")
            }
            orderRoomReferralLinkUsed.push(shortenLink)
            voucherAndOrderCache.save("orderRoomReferralLinkUsed", orderRoomReferralLinkUsed)
        }
        else {
            const newLinkList = [shortenLink]
            voucherAndOrderCache.save("orderRoomReferralLinkUsed", newLinkList)
        }
        
        return shortenLink
    } catch (error) {
        throw new Error(error)
    }
}

/**
 * 
 * @param param0 - items: get order items buy order id - if isGroupBuyOrder - item = items[0]
 * @param param1 - data_cache: from momo ipn response
 */

export const handlerAddRoomGroupBuyOrder = async ({
    order,
    data_cache
}) => {
    if (!order.is_group_buy_order) return false; // just add room with order.is_group_buy_order = true
    const orderItems = await orderItemRepo.find({ order_id: ObjectId(order._id) });
    let { ref_link, order_room_id,
        group_id, avatar_url,
        ref_from_id } = data_cache;

    const now = new Date(), userID = order.user_id.toString()
    const item = orderItems[0];
    const { product_id, variant, quantity }: any = item
    /// Get product info

    let product: any = await productRepo.findOne({ _id: ObjectId(product_id) })
    product = cloneObj(product)

    // handle join order room || create a room with GB order
    if (order_room_id) {
        let room = product.order_rooms?.find(r => r._id.toString() === order_room_id.toString())
        console.log("room: ", room)
        if (!room) {
            // Tao moi room
            console.log("tao moi room")
            const newRoom = {
                owner_id: userID,
                is_valid: true,
                status: 'pending',
                members: [
                    {
                        _id: userID, role: 'owner', referal_link: ref_link,
                        payment_status: 'paid', avatar: avatar_url,
                        invited_time: now, invited_by: null, joined_time: now,
                        order_info: {
                            order_id: order._id.toString(),
                            order_number: order.order_number,
                            group_order_id: order.group_order_id.toString()
                        }
                    }
                ],
                group_id: ObjectId(group_id),
                variant_id: variant?._id ? ObjectId(variant._id) : null,
                created_at: now, updated_at: now
            }
            if (product.order_rooms && product.order_rooms.length) {
                console.log("Da ton tai it nhat 1 order room");

                product.order_rooms.push(newRoom)
            }
            else {
                console.log('Chua ton tai order room nao');

                product.order_rooms = [newRoom]
            }
        }
        else {
            console.log("Add room")
            room = cloneObj(room)
            const members = room.members || []
            const groupOfRoom = product.group_buy?.groups?.find(group => group._id.toString() === room.group_id.toString())
            if (members.length < groupOfRoom.number_of_member) {
                members.push({
                    _id: userID, role: 'joined', referal_link: ref_link,
                    payment_status: 'paid', avatar: avatar_url,
                    invited_time: now, invited_by: ref_from_id, joined_time: now,
                    order_info: {
                        order_id: order._id.toString(),
                        order_number: order.order_number,
                        group_order_id: order.group_order_id.toString()
                    }
                })
            }

            const numberOfValidUserInRoom = members.filter(mem => mem.role === 'joined' || mem.role === 'owner')

            let status;
            if (groupOfRoom.number_of_member > numberOfValidUserInRoom.length)
                status = 'pending'
            else status = 'completed'

            room.status = status
            room.members = members
            product.order_rooms = product.order_rooms.filter(roomEle => roomEle._id.toString() !== room._id.toString())
            product.order_rooms.push(room)
        }
    }

    // Update product order_rooms
    let  newProductQuery: any = {
        order_rooms: product.order_rooms
    }
    let productUpdate = await productRepo.update(product._id, newProductQuery);

    pushNotificationGroupBuy({ buyer_id: userID, order_room_id, product: productUpdate, order})
    return productUpdate;
}

const pushNotificationGroupBuy = async ({
    buyer_id, order_room_id, product, order
}) => {
    // người mua đầu tiên - host
    const orderRoom = product.order_rooms.length === 1 ? product.order_rooms[0] : product.order_rooms.find(room => room._id.toString() === order_room_id);
    const groupBuy = product.group_buy.groups.filter(group => orderRoom?.group_id?.toString() === group._id.toString());

    if (!orderRoom || !groupBuy.length) return;
    // mess: tạo nhóm thành công
    if (orderRoom.status === "completed") {
        let fetchPromise = orderRoom.members?.map(async (member) => {
            if (!member) return;
            let language = await getUserLanguage(member._id);
            let content = getTranslation(language, 'notification.order.group_buy_member_paid'),
                notifyType = NOTIFY_TYPE.GB_MEMBER_PAID,
                title = getTranslation(language, 'notification.bidu_notification');

            if (member._id.toString() === orderRoom.owner_id.toString()) {
                // mess to host: nhóm thanh toán thành công
                content = getTranslation(language, 'notification.order.group_buy_host_paid');
                notifyType = NOTIFY_TYPE.GB_HOST_PAID;
            } else {
                // mess to member: nhóm thanh toán thành công
                content = getTranslation(language, 'notification.order.group_buy_member_paid');
                notifyType = NOTIFY_TYPE.GB_MEMBER_PAID;
            }
            return notificationService.saveAndSend(
                NotifyRequestFactory.generate(
                    notifyType,
                    NOTIFY_TARGET_TYPE.BUYER,
                    {
                        title: title,
                        content: content,
                        receiverId: member._id.toString(),
                        userId: member._id.toString(),
                        refLink: member.referal_link,
                        productId: product._id.toString(),
                        orderNumber: member?.order_info.order_number,
                        orderId: member.order_info.order_id.toString(),
                        orderRoomId: orderRoom._id.toString()
                    }
                ))
        })
        Promise.all(fetchPromise);
    } else if (orderRoom.status === "pending") {
        if (orderRoom.members.length === 1) {
            let language = await getUserLanguage(orderRoom.owner_id.toString());
            return notificationService.saveAndSend(
                NotifyRequestFactory.generate(
                    NOTIFY_TYPE.GB_ROOM_CREATED,
                    NOTIFY_TARGET_TYPE.BUYER,
                    {
                        title: getTranslation(language, 'notification.bidu_notification'),
                        content: getTranslation(language, 'notification.order.group_buy_room_created'),
                        receiverId: orderRoom.owner_id.toString(),
                        userId: orderRoom.owner_id.toString(),
                        refLink: orderRoom.members[0]?.referal_link,
                        productId: product._id.toString(),
                        orderNumber: orderRoom.member[0]?.order_info.order_number,
                        orderId: orderRoom.member[0].order_info.order_id.toString(),
                        orderRoomId: orderRoom._id.toString()
                    }
                ))
        } else if (groupBuy.number_of_member - orderRoom.members.length === 1) {
            let fetchPromise = orderRoom.members?.map(async (member) => {
                if (!member) return;
                let language = await getUserLanguage(member._id);
                let content = getTranslation(language, 'notification.order.group_buy_missing_1_person'),
                    notifyType = NOTIFY_TYPE.GB_MEMBER_MISSING_1_PER,
                    title = getTranslation(language, 'notification.bidu_notification');

                if (member._id.toString() === orderRoom.owner_id.toString()) {
                    // mess to host: thiếu 1 người
                    content = getTranslation(language, 'notification.order.group_buy_host_missing_1_person');
                    notifyType = NOTIFY_TYPE.GB_HOST_MISSING_1_PER;
                } else {
                    // mess to member: thiếu 1 người
                    content = getTranslation(language, 'notification.order.group_buy_missing_1_person');
                    notifyType = NOTIFY_TYPE.GB_MEMBER_MISSING_1_PER;
                }
                return notificationService.saveAndSend(
                    NotifyRequestFactory.generate(
                        notifyType,
                        NOTIFY_TARGET_TYPE.BUYER,
                        {
                            title: title,
                            content: content,
                            receiverId: member._id.toString(),
                            userId: member._id.toString(),
                            refLink: member.referal_link,
                            productId: product._id.toString(),
                            orderNumber: member?.order_info.order_number,
                            orderId: member.order_info.order_id.toString(),
                            orderRoomId: orderRoom._id.toString()
                        }
                    ))
            })
            Promise.all(fetchPromise);
        }
    }
    else {
        return;
    }

    // join group
    const membersExceptLastBuyer = orderRoom.members?.filter(member => member._id.toString() != buyer_id);
    const buyer: any = await userRepo.getUserWithGallery(buyer_id);
    const joinGroupFetch = membersExceptLastBuyer.map(async (member) => {
        let language = await getUserLanguage(member._id);
        let content = getTranslation(language, 'notification.order.group_buy_join', buyer[0].nameOrganizer.userName),
            notifyType = NOTIFY_TYPE.GB_JOIN,
            title = getTranslation(language, 'notification.bidu_notification');

        return notificationService.saveAndSend(
            NotifyRequestFactory.generate(
                notifyType,
                NOTIFY_TARGET_TYPE.BUYER,
                {
                    title: title,
                    content: content,
                    receiverId: member._id.toString(),
                    userId: buyer[0]._id.toString(),
                    refLink: member.referal_link,
                    productId: product._id.toString(),
                    orderNumber: member?.order_info.order_number,
                    orderId: member?.order_info.order_id.toString(),
                    orderRoomId: orderRoom._id.toString()
                }
            ))
    })

    Promise.all(joinGroupFetch);
}