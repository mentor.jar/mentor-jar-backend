let suffix = ""

if (process.env.NODE_ENV === 'production') {
    suffix = "-prd"
}

if (process.env.NODE_ENV === 'staging' || process.env.NODE_ENV === "development") {
    suffix = "-stg"
}

export const TableName = {
    PAYMENT_LOGS: 'payment_logs' + `${suffix}`,
    GB_JOB_CANCEL_48H: 'group_buy_job_cancel_48h' + `${suffix}`
}
