import { firestore } from '../../base/connection/firebase';

export default class FirebaseStoreService {
    private static _instance: FirebaseStoreService;

    static _() {
        if (!this._instance) {
            this._instance = new FirebaseStoreService();
        }
        return this._instance;
    }
    /**
     * data: data want to store in firebase store, type must is object { key: value ... }
     * tableName: name of table store in firebase store
     */
    save(tableName: string, data: any) {
        return firestore.collection(tableName).doc().set(data);
    }
}
