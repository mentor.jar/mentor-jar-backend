function setCacheHeaders(res, time = 5) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    // Request methods you wish to allow
    res.setHeader(
        'Access-Control-Allow-Methods',
        'GET, POST, OPTIONS, PUT, PATCH, DELETE'
    );
    res.setHeader('Cache-Control', `public, max-age=${time}`);
    res.setHeader('db', Date.now());
}

export default setCacheHeaders;