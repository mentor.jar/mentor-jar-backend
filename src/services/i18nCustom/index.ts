import * as viJSON from '../../locales/vi.json';
import * as enJSON from '../../locales/en.json';
import * as koJSON from '../../locales/ko.json';
import { ObjectId } from '../../utils/helper';
import AppUserRepo from '../../repositories/AppUserRepo';

const appUserRepo = AppUserRepo.getInstance();

export const getTranslation = (language, link, ...variables) => {
    let value: any;
    const keys = link.split('.');
    switch (language) {
        case 'vi':
            value = translate(keys, viJSON, variables);
            break;
        case 'ko':
            value = translate(keys, koJSON, variables);
            break;
        case 'en':
            value = translate(keys, enJSON, variables);
            break;
        default:
            value = translate(keys, viJSON, variables);
            break;
    }
    return value.toString();
}

export const translate = (keys, json, variables) => {
    let value = json;
    while (keys?.length) {
        let key = keys.shift();
        value = value[key];
    }
    if (variables.length)
        variables.forEach(element => {
            value = value.replace('%s', element);
        });
    return value;
}

export const getUserLanguage = async (userId) => {
    const appUser = await appUserRepo
        .getModel()
        .findOne({
            userId: ObjectId(userId)
        }, null, {
            sort: { updatedAt: -1 }
        })
        .exec();
    const language = appUser?.language || 'vi';
    return language
}
