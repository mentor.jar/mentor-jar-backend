import excel from "exceljs";
import { toDateTime } from "../../utils/dateUtil";
import { convertCamelCaseToNormal } from "../../utils/helper";

class ExportExcelService {

    async common(res, nameColum, data) {
        try {
            let workbook = new excel.Workbook();
            Object.keys(data).forEach((value) => {
                const sheet = workbook.addWorksheet(convertCamelCaseToNormal(value));
        
                sheet.columns = nameColum;
        
                sheet.addRows(data[value]);
        
                sheet.getRow(1).font = {
                    size: 12,
                    bold: true
                };
            })
    
            res.setHeader(
                "Content-Type",
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            );
            await workbook.xlsx.write(res);
            res.end();
        } catch (error) {
            res.error(error.name, error.message, error.statusCode);
        }
    }

    async transaction(res, nameColumByShippingStatus, data) {
        try {
            let workbook = new excel.Workbook();
            Object.keys(data).forEach((value) => {
                const sheet = workbook.addWorksheet(convertCamelCaseToNormal(value));
        
                if (data[value][0]?.shipping_status) {
                    sheet.columns = nameColumByShippingStatus(data[value][0]?.shipping_status);
                } else {
                    sheet.columns = nameColumByShippingStatus();
                }

                sheet.addRows(data[value]);

                sheet.getRow(1).font = {
                    size: 12,
                    bold: true
                };
            })
    
            res.setHeader(
                "Content-Type",
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            );
            await workbook.xlsx.write(res);
            res.end();
        } catch (error) {
            res.error(error.name, error.message, error.statusCode);
        }
    }

    async revenue(res, nameColum, data) {
        try {
            let workbook = new excel.Workbook();
            Object.keys(data).forEach((value) => {
                const sheet = workbook.addWorksheet(convertCamelCaseToNormal(value));
        
                sheet.columns = nameColum;
        
                sheet.addRows(data[value]);
        
                sheet.getRow(1).font = {
                    size: 12,
                    bold: true
                };
            })
    
            res.setHeader(
                "Content-Type",
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            );
            await workbook.xlsx.write(res);
            res.end();
        } catch (error) {
            res.error(error.name, error.message, error.statusCode);
        }
    }

    async transactionAccountant(res, nameColumByShippingStatus, data, dateStart, dateEnd) {
        try {
            const start = toDateTime(dateStart, 'dd_mm_yy');
            const end = toDateTime(dateEnd, 'dd_mm_yy');
            let workbook = new excel.Workbook();
            Object.keys(data).forEach((value) => {
                const sheet = workbook.addWorksheet(value);
        
                sheet.columns = nameColumByShippingStatus();

                sheet.addRows(data[value]);

                sheet.getRow(1).font = {
                    size: 12,
                    bold: true
                };
            })
    
            res.setHeader(
                "Content-Type",
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            );
            await workbook.xlsx.write(res);
            res.end();
        } catch (error) {
            res.error(error.name, error.message, error.statusCode);
        }
    }

};

const exportExcelService = new ExportExcelService();
export default exportExcelService;