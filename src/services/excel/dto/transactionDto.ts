interface ITransactionDto {
    shop_id: string;
    shop_name: string;
    order_number: string;
    order_shipping_id: string;
    created_at: string;
    buyer_id: string;
    buyer_name: string;
    total_price_product: number;
    shipping_fee: number;
    system_voucher_discount: number;
    system_voucher_shipping_fee: number;
    shop_voucher_discount: number;
    total_voucher_discount: number;
    total_voucher_cashback?: number;
    total_price: number;
    delivery_date: string;
    shipping_status: string;
    checked_date: string;
    shipping_name: string;
}

export class TransactionDto {
    private _shopId: string;
    private _shopName: string;
    private _orderNumber: string;
    private _orderShippingId: string;
    private _createdAt: string;
    private _buyerId: string;
    private _buyerName: string;
    private _totalPriceProduct: number;
    private _shippingFee: number;
    private _systemVoucherDiscount: number;
    private _systemVoucherShippingFee: number;
    private _shopVoucherDiscount: number;
    private _totalVoucherDiscount: number;
    private _totalVoucherCashback: number;
    private _totalPrice: number;
    private _deliveryDate: string;
    private _shippingStatus: string;
    private _checked: string;
    private _shippingName: string;

    get shopId() {
        return this._shopId;
    }

    getShortId() {
        return this._shopId.toString().slice(-10);
    }

    setShopId(shopId: string) {
        this._shopId = shopId ?? 'N/A';
        return this;
    }

    get shopName() {
        return this._shopName;
    }

    setShopName(shopName: string) {
        this._shopName = shopName ?? 'N/A';
        return this;
    }

    get orderNumber() {
        return this._orderNumber;
    }

    setOrderNumber(orderNumber: string) {
        this._orderNumber = orderNumber ?? 'N/A';
        return this;
    }

    get orderShippingId() {
        return this._orderShippingId;
    }

    setOrderShippingId(orderShippingId: string) {
        this._orderShippingId = orderShippingId ?? 'N/A';
        return this;
    }

    get createdAt() {
        return this._createdAt;
    }

    setCreatedAt(createdAt: string) {
        this._createdAt = createdAt ?? 'N/A';
        return this;
    }

    get buyerId() {
        return this._buyerId;
    }

    setBuyerId(buyerId: string) {
        this._buyerId = buyerId ?? 'N/A';
        return this;
    }

    get buyerName() {
        return this._buyerName;
    }

    setBuyerName(buyerName: string) {
        this._buyerName = buyerName ?? 'N/A';
        return this;
    }

    get totalPriceProduct() {
        return this._totalPriceProduct;
    }

    setTotalPriceProduct(totalPriceProduct: number) {
        this._totalPriceProduct = totalPriceProduct ?? 0;
        return this;
    }

    get shippingFee() {
        return this._shippingFee;
    }

    setShippingFee(shippingFee: number) {
        this._shippingFee = shippingFee ?? 0;
        return this;
    }

    get systemVoucherDiscount() {
        return this._systemVoucherDiscount;
    }

    setSystemVoucherDiscount(systemVoucherDiscount: number) {
        this._systemVoucherDiscount = systemVoucherDiscount ?? 0;
        return this;
    }

    get systemVoucherShippingFee() {
        return this._systemVoucherShippingFee;
    }

    setSystemVoucherShippingFee(systemVoucherShippingFee: number) {
        this._systemVoucherShippingFee = systemVoucherShippingFee ?? 0;
        return this;
    }

    get shopVoucherDiscount() {
        return this._shopVoucherDiscount;
    }

    setShopVoucherDiscount(shopVoucherDiscount: number) {
        this._shopVoucherDiscount = shopVoucherDiscount ?? 0;
        return this;
    }

    get totalVoucherDiscount() {
        return this._totalVoucherDiscount;
    }

    setTotalVoucherDiscount(totalVoucherDiscount: number) {
        this._totalVoucherDiscount = totalVoucherDiscount ?? 0;
        return this;
    }

    get totalVoucherCashback() {
        return this._totalVoucherCashback;
    }

    setTotalVoucherCashback(totalVoucherCashback: number) {
        this._totalVoucherCashback = totalVoucherCashback ?? 0;
        return this;
    }

    get totalPrice() {
        return this._totalPrice;
    }

    setTotalPrice(totalPrice: number) {
        this._totalPrice = totalPrice ?? 0;
        return this;
    }

    get deliveryDate() {
        return this._deliveryDate;
    }

    setDeliveryDate(deliveryDate: string) {
        this._deliveryDate = deliveryDate ?? 'N/A';
        return this;
    }

    get shippingStatus() {
        return this._shippingStatus;
    }

    setShippingStatus(shippingStatus: string) {
        this._shippingStatus = shippingStatus ?? 'N/A';
        return this;
    }
   
    get checked() {
        return this._checked;
    }

    setCheckedTime(checked: string) {
        this._checked = checked ?? 'N/A';
        return this;
    }
   
    get shippingName() {
        return this._shippingName;
    }

    setShippingName(shippingName: string) {
        this._shippingName = shippingName;
        return this;
    }

    get(): ITransactionDto {
        const response: ITransactionDto = {
            shop_id: this.getShortId(),
            shop_name: this.shopName,
            order_number: this.orderNumber,
            order_shipping_id: this.orderShippingId,
            created_at: this.createdAt,
            buyer_id: this.buyerId,
            buyer_name: this.buyerName,
            total_price_product: this.totalPriceProduct,
            shipping_fee: this.shippingFee,
            system_voucher_discount: this.systemVoucherDiscount,
            system_voucher_shipping_fee: this.systemVoucherShippingFee,
            shop_voucher_discount: this.shopVoucherDiscount,
            total_voucher_discount: this.totalVoucherDiscount,
            // total_voucher_cashback: this.totalVoucherCashback,
            total_price: this.totalPrice,
            delivery_date: this.deliveryDate,
            checked_date: this.checked,
            shipping_name: this.shippingName,
            shipping_status: this.shippingStatus,
        };

        return response;
    }
}