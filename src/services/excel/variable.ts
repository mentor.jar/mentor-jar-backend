import { ShippingStatus } from "../../models/enums/order";

export const COLUMN_SHOP = [
    { header: "ID", key: "_id", width: 25 },
    { header: "Name shop", key: "nameShop", width: 25 },
    { header: "Email", key: "email", width: 25 },
    { header: "Phone Number", key: "phoneNumber", width: 15 },
    { header: "Country", key: "country", width: 10,  },
    { header: "Birthday", key: "birthday", width: 10 },
    { header: "Created at", key: "createdAt", width: 20,},
];

export const COLUMN_RATING_SHOP = [
    { header: "ID", key: "_id", width: 25 },
    { header: "Content", key: "content", width: 75 },
    { header: "Medias", key: "medias", width: 75 },
    { header: "ID Shop", key: "shop_id", width: 25 },
    { header: "ID User", key: "user_id", width: 25,  },
    { header: "ID Order", key: "order_id", width: 25 },
    { header: "Created at", key: "created_at", width: 20,},
];

export const COLUMN_TRANSACTION = (status: ShippingStatus = ShippingStatus.PENDING) => {

    let headers = [
        { header: "Order Number", key: "order_number", width: 18 },
        { header: "Code Shipping", key: "code_shipping", width: 32 },
        { header: "Order Creation Date", key: "created_at", width: 18,},
        { header: "ID Shop", key: "shop_id", width: 25 },
        { header: "Name Shop", key: "shop_name", width: 25 },
        { header: "ID User", key: "user_id", width: 25,  },
        { header: "User Name", key: "user_name", width: 25 },
        { header: "Total Item Value", key: "total_value_items", width: 15 },
        { header: "Shipping Fee", key: "shipping_fee", width: 12 },
        { header: "Shipping Discount", key: "shipping_discount", width: 18 },
        { header: "Voucher System Discount", key: "voucher_system_discount", width: 23 },
        { header: "Voucher Shop Discount", key: "voucher_shop_discount", width: 23 },
        { header: "Voucher Cashback Discount", key: "voucher_cashback_discount", width: 23 },
        { header: "Total Voucher Discount", key: "total_voucher_discount", width: 20 },
        { header: "Total Price", key: "total_price", width: 10 },
        { header: "Shipping Status", key: "shipping_status", width: 14,},
    ];

    switch (status) {
        case ShippingStatus.PENDING:
        case ShippingStatus.WAIT_TO_PICK:
        case ShippingStatus.SHIPPING:
        case ShippingStatus.CANCELING:
        case ShippingStatus.CANCELED:
        case ShippingStatus.RETURN:
            return headers;

        case ShippingStatus.SHIPPED:
            headers.push({ header: "Delivery Date", key: "date_by_shipping_status", width: 18,})
            return headers;
    
        default:
            return headers;
    }
    
}

export const COLUMN_TRANSACTION_ACCOUNTANT = () => {
    
    let headers = [
        { header: "Mã shop", key: "shop_id", width: 18 },
        { header: "Tên shop", key: "shop_name", width: 32 },
        { header: "Mã đơn hàng", key: "order_number", width: 18,},
        { header: "Mã vận đơn", key: "order_shipping_id", width: 25 },
        { header: "Ngày tạo đơn hàng (trên BIDU)", key: "created_at", width: 32 },
        { header: "Id người mua", key: "buyer_id", width: 25,  },
        { header: "Tên người mua", key: "buyer_name", width: 25 },
        { header: "Tổng giá tiền đơn hàng", key: "total_price_product", width: 15 },
        { header: "Tiền vận chuyển", key: "shipping_fee", width: 12 },
        { header: "Tiền giảm giá hệ thống(voucher)", key: "system_voucher_discount", width: 18 },
        { header: "Tiền giảm giá vận chuyển hệ thống(voucher)", key: "system_voucher_shipping_fee", width: 23 },
        { header: "Tiền giảm giá của shop(voucher)", key: "shop_voucher_discount", width: 23 },
        { header: "Tổng giảm giá voucher", key: "total_voucher_discount", width: 23 },
        // { header: "Tổng giảm giá voucher cashback", key: "total_voucher_cashback", width: 23 },
        { header: "Tổng tiền thanh toán", key: "total_price", width: 20 },
        { header: "Ngày giao hàng", key: "delivery_date", width: 10 },
        { header: "Ngày đối soát", key: "checked_date", width: 10 },
        { header: "Trạng thái đơn hàng", key: "shipping_status", width: 14,},
        { header: "Đơn vị vận chuyển", key: "shipping_name", width: 14,},
    ];

    return headers;
}