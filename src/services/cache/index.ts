const NodeCache = require("node-cache");
import { TTL } from "../../SocketStores/constants";

export default class CacheService {

    private static _instance: CacheService;
    private nodeCache = new NodeCache({
        stdTTL: 60 * 5 // ttl = 5 minutes
    });

    static get _() {
        if (!this._instance) {
            this._instance = new CacheService();
        }
        return this._instance;
    }

    get(key) {
        return this.nodeCache.get(key);
    }

    getMultiple(keys) {
        return this.nodeCache.mget(keys);
    }

    set(key, obj) {
        return this.nodeCache.set(key, obj);
    }

    setTTL(key, obj, ttl: TTL) {
        return this.nodeCache.set(key, obj, ttl);
    }

    setUnlimited(key, obj) {
        return this.nodeCache.set(key, obj, 0);
    }

    take(key) {
        return this.nodeCache.take(key)
    }

    delete(key) {
        return this.nodeCache.del(key);
    }

    keys() {
        return this.nodeCache.keys();
    }

    has(key) {
        return this.nodeCache.has(key)
    }
}

