import keys from "../../../config/env/keys";
import NodeRSA from 'node-rsa';

const publicKey = '-----BEGIN PUBLIC KEY-----' + keys.momo_payment.public_key + '-----END PUBLIC KEY-----';

export class RSAHashing {
    private static _instances: RSAHashing = null;

    constructor() {
    }

    static getInstance() {
        if (!this._instances) {
            this._instances = new RSAHashing();
        }
        return this._instances
    }

    public encrypt(data) {
        const key = new NodeRSA(publicKey, { encryptionScheme: 'pkcs1' });
        return key.encrypt(JSON.stringify(data), 'base64');
    }

}