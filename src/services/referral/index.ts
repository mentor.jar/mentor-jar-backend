import { WrongReferralCodeError } from "../../base/customError";
import ReferralUsage from "../../models/Referral_Usage";
import { UserRepo } from "../../repositories/UserRepo";
import { ReferralUsageRepo } from '../../repositories/ReferralUsageRepo';
import { ObjectId } from "../../utils/helper";
import { randomShortId } from '../../utils/stringUtil';
import { VoucherRepository } from '../../repositories/VoucherRepository';
import notificationService, { NotifyRequestFactory } from '../notification/index';
import { NOTIFY_TARGET_TYPE, NOTIFY_TYPE } from "../notification/constants";
import { getTranslation, getUserLanguage } from "../i18nCustom";

class ReferralService {

    static APPLIED_NUMBER = 5
    /**
     * Storage db service
     */
    storageService;
    notificationService;

    constructor(storageService, notificationService) {
        this.storageService = storageService;
        this.notificationService = notificationService;
    }

    /**
     * Generate user code
     * @param user_id 
     * @returns 
     */
    async generateUserCode(user_id) {
        const user = await UserRepo.getInstance().findById(user_id);
        if (user.referral_code) return user.referral_code;
        user.referral_code = (user_id.toString().slice(-2) + randomShortId(3)).toUpperCase();
        await user.save()
        return user.referral_code;
    }

    /**
     * Get (or generate if not exist) referral code 
     * @param user_id 
     */
    async getUserWithCode(code) {
        const user = await UserRepo.getInstance().find({
            referral_code: code
        });
        if (user?.[0]) return user?.[0];
        return null
    }

    /**
     * Use a code of other
     * @param user_id user use code
     * @param referral_code referral code
     * @returns 
     */
    async useCode(user_id, referral_code) {
        const userHaveCode = await this.getUserWithCode(referral_code);
        if (!userHaveCode)
            throw new WrongReferralCodeError("wrong_referral_code");

        if (userHaveCode._id.toString() == user_id.toString())
            throw new WrongReferralCodeError("not_use_same_user");

        const existedUsage = await ReferralUsageRepo.getInstance().find({
            user_id: ObjectId(user_id),
        })

        if (existedUsage.length > 0) throw new WrongReferralCodeError("usage_existed");

        const referralUsing = new ReferralUsage({
            user_id: ObjectId(user_id),
            referred_id: userHaveCode._id,
            code: referral_code
        })

        await referralUsing.save()
        await this.checkApplyBounty(referralUsing);
        return referralUsing;
    }

    /**
     * Apply voucher to user
     * max is 5 time for introducer and
     * apply immediately for first introduced user
     * @param newReferralUsage 
     */
    async checkApplyBounty(newReferralUsage) {

        const introducedUserId = newReferralUsage.user_id;
        const introduceUserId = newReferralUsage.referred_id.toString();

        const appliedNumber = await ReferralUsageRepo.getInstance().find({
            referred_id: ObjectId(newReferralUsage.referred_id),
        })

        // who share referral code
        if (appliedNumber.length < ReferralService.APPLIED_NUMBER) {
            await this.notifyReferral('introduce', introduceUserId, appliedNumber.length);
        }
        if (appliedNumber.length === ReferralService.APPLIED_NUMBER) {
            const introduceVouchers: any = await await VoucherRepository.getInstance().find({
                target: 'introduce',
                end_time: { '$gte': new Date() },
            });
            if (introduceVouchers.length) {
                await Promise.all(introduceVouchers.map((voucher) => this.addUserToVoucher(voucher, introduceUserId)))
                await this.notifyReferral('introduce', introduceUserId, appliedNumber.length);
            }
        }

        // who use referral code
        const introducedVouchers: any = await VoucherRepository.getInstance().find({
            target: 'introduced',
            end_time: { '$gte': new Date() },
        });

        if (introducedVouchers.length) {
            await Promise.all(introducedVouchers.map((voucher) => this.addUserToVoucher(voucher, introducedUserId)))
            await this.notifyReferral('introduced', introducedUserId)
        }
    }

    async addUserToVoucher(voucher, userId) {
        let usersInVoucher = voucher.users;
        usersInVoucher = usersInVoucher.filter(userIds => userIds.toString() == userId)
        if (usersInVoucher.length > 0) return Promise.resolve(false);
        voucher.users.push(ObjectId(userId));
        return voucher.save();
    }

    async notifyReferral(type, user_id, count?) {
        const language = await getUserLanguage(user_id);
        await notificationService.saveAndSend(
            NotifyRequestFactory.generate(
                NOTIFY_TYPE.REWARD_VOUCHER,
                NOTIFY_TARGET_TYPE.BUYER,
                {
                    title: getTranslation(language, 'notification.bidu_notification'),
                    content: this._getTranslationContent({ type, count, language }),
                    count: count,
                    receiverId: user_id,
                    type: type
                }
            ))
    }

    _getTranslationContent({ type, language, count }) {
        let result = null;
        switch (type) {
            case 'introduce':
                result = (count < ReferralService.APPLIED_NUMBER) ? 
                    getTranslation(language, 'referral.introduce_reward_v2', count) : 
                    getTranslation(language, 'referral.introduce_reward_v2' , count) + ' ' + getTranslation(language, 'referral.introduce_reward_voucher' , count);
                break;
        
            case 'introduced':
                result = getTranslation(language, 'referral.introduced_reward');
                break;
        }
        return result;
    }
}

const referralService = new ReferralService(null, null)
export default referralService;