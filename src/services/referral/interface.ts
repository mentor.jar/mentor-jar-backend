interface ReferralStorage {
    saveReferralCode(user_id);
    getReferralCode(user_id);
    saveUsageReferralCode(user_id, code);
}

interface ReferralNotification {
    sendReferralCodeApply();
}