### Referral service

#### User story

- Each user have their a referral code -> save to db -> display on UI -> copy text or copy share link
- User can share their referral code -> display on opengraph -> web preview
- click on link -> navigate to signup with same referral code
- signup screen also have input referral code 
- in profile screen also have button to navigate to referral code enter screen
- after referred user enter referral code success, the user who have referral code get bounty

#### Suppose we have
A --- share code ---> B 

#### Bounty maybe
 - A user ranking
 - A & B got voucher

#### Rule
 - referral code is unique, easy to enter
 - each user should be enter referral code 1 onetime