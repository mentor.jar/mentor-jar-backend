import { REDIS_HOST, REDIS_PORT } from "../../base/variable";

const kue = require('kue')

export default class QueueService {

    static _queue = kue.createQueue(
        {
            prefix: 'jobs',
            redis: {
                port: REDIS_PORT,
                host: REDIS_HOST,
                auth: process.env.REDIS_PASSWORD,
            }
        }
    );

    public register(name: String, jobData: Object, runner: Function, priority = "high") {
        let start = new Date().getTime();
        const job = QueueService._queue.create(name, jobData).priority(priority)
            .save(function (err) {
                if (!err) {
                    console.log(job.id);
                }
                else {
                    console.log(err);
                }
            });
        QueueService._queue.process(name, function (job, done) {
            console.log(name);
            runner(job, done);
        });

        job.on('enqueue', function (result) {
            console.log("enqueue: ", result);
            
        }).on('start', function (result) {
            console.log("start: ", result);
            
        }).on('complete', function (result) {
            let end = new Date().getTime();

            console.log(`Time: ${end - start},Job completed with data `, result);

        }).on('failed attempt', function (errorMessage, doneAttempts) {
            console.log('Job failed');

        }).on('failed', function (errorMessage) {
            console.log('Job failed');

        }).on('progress', function (progress, data) {
            console.log('\r  job #' + job.id + ' ' + progress + '% complete with data ', data);

        });
    }

}
