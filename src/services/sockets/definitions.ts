export enum ChatType {
    LIVE_STREAM = 'LIVE STREAM',
    CHAT = 'CHAT',
}

export enum ChatEventType {
    RoomChat = "room chat",
    pushUserOnline = "push user online",
    pushUserOffline = "push user offline",
    getUser = "get user",
    ReadLastMessage = "read last message",
    UserRead = "user read",
    DeleteMessage = "delete message",
    PrivateMessage = "private message",
    ErrorHaveResponse = "error response",
    Error = "error",
    Sucess = "success"
}

export enum LiveStreamEventType {
    RoomLiveStream = "room livestream",
    AmountView = "amount view",
    AmountHeart = "amount heart",
    LeftRoom = "left room livestream",
    SendHeart = "send heart",
    Comment = "comment",
    Mute = "mute",
    UnMute = "unmute",
    PinMessage = "pin message",
    GetStream = "get stream",
    UpdateProduct = "update product stream",
    Error = "error",
    Sucess = "success"
}

export enum PublicEventType {
    LoadMoreMess = 'load more message',
    LoadNewMess = 'load new message'
}
export enum ResponseSocketSussess {
    PushMessage = "push message success",
    ReadLastMessage = "read last message success",
    PushUserOnline = "push user online success",
    PushUserOffline = "push user offline success",
}
export interface ChatMessage {
    id: number,
    content: string,
    type: string,
    user_id: string,
    user: any
}
