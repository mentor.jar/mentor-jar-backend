import { InMemoryMessageForRoomStore } from '../../SocketStores/MessageStore';
import { RoomChatRepo } from '../../repositories/RoomChatRepo';
import { text } from 'body-parser';
import { cloneObj } from '../../utils/helper';
import { AuthencationError, QueryDBError } from '../../base/customError';
import { ChatEventType, ChatType, ResponseSocketSussess } from './definitions';
import { RoomChatDTO } from '../../DTO/RoomChatDTO';
import notificationService from '../notification/index';
import { UserRepo } from '../../repositories/UserRepo';
const probe = require('probe-image-size');

const RoomStore = InMemoryMessageForRoomStore.getInstance();
const roomChatRepo = RoomChatRepo.getInstance();

export const socketChat = socket => {

    // fetch message of room exist and join room chat
    let paramGetRoom = {
        userId: socket.user._id,
        type: ChatType.CHAT,
        limit: 10000,
        page: 1,
    };

    const validUserInRoom = room => {
        return (
            room.users.findIndex(
                user => user._id.toString() == socket.user._id.toString()
            ) > -1
        );
    };

    const saveRoomWhenRedisClear = async roomId => {
        const isRoomApi = await RoomStore.isRoomApi(roomId);
        if (!isRoomApi) {
            let room = await roomChatRepo.getRoomById(roomId);
            if (!room) {
                socket.emit(
                    ChatEventType.Error,
                    'Room not found'
                );
                return;
            }
            RoomStore.saveCheckRoomApi(roomId);
            await RoomStore.saveRoom(room);
        }
    };

    const sendNotification = async (roomId, message) => {
        try {
            console.log("----send-noti-debug----" + roomId + "-------" + message.id);
            const room = await RoomStore.getRoom(roomId);
            if (!room) throw new Error('Can not find room');
            const senderId = message.user_id;
            const receiver = room?.users?.filter(user => user._id != senderId);
            const receiverIds = receiver?.map(user => user._id);

            notificationService.sendChatNotification(
                room,
                message,
                receiverIds
            );
        } catch (error) {
            console.log('sockerChat@sendNotification', error);
            console.log('sockerChat@debug', roomId, message);
        }
    };

    roomChatRepo.getRoom(paramGetRoom).then(listRoomOfUser => {
        listRoomOfUser = cloneObj(listRoomOfUser);
        listRoomOfUser.data.map(room => {
            socket.join(room._id);
        });
    });

    socket.on(ChatEventType.RoomChat, async ({ roomId }) => {
        try {
            await saveRoomWhenRedisClear(roomId);
            const roomEmit = await RoomStore.getRoom(roomId);
            if (!validUserInRoom(roomEmit)) {
                throw new AuthencationError(
                    'Đây không phải phòng chat của bạn'
                );
            }

            roomChatRepo.userJoinRoom(roomId, socket.user);
            await RoomStore.userJoinRoom(roomId, socket.user);

            socket.join(roomId);

            let dtoChat = new RoomChatDTO(roomEmit);
            roomEmit.users.map(user => {
                if (user._id.toString() != socket.user._id.toString()) {
                    socket.join(user._id.toString())
                }
            })
            roomEmit.messages = dtoChat.getEndMoreMessage();
            socket.emit(roomId, roomEmit);
        } catch (error) {
            socket.emit(ChatEventType.Error, error.message);
        }
    });

    socket.on(ChatEventType.ReadLastMessage, async ({ roomId }) => {
        try {
            // check and add room to store when user disconnect
            await saveRoomWhenRedisClear(roomId);
            await roomChatRepo.readLastMessage(roomId, socket.user);
            await RoomStore.readLastMessage(roomId, socket.user);
            socket.to(roomId).emit(ChatEventType.UserRead, {
                user: socket.user,
                room_id: roomId,
            });
        } catch (error) {
            socket.emit(ChatEventType.Error, error.message);
        }
    });

    socket.on(ChatEventType.DeleteMessage, async ({ roomId, messageId }) => {
        try {
            // check and add room to store when user disconnect
            await saveRoomWhenRedisClear(roomId);
            let isDelete = await RoomStore.deleteMessage(
                roomId,
                messageId,
                socket.user
            );
            if (isDelete) {
                socket
                    .to(roomId)
                    .emit(ChatEventType.DeleteMessage, { roomId, messageId });
            } else {
                socket.emit(
                    ChatEventType.Error,
                    'Message not found'
                );
            } socket
        } catch (error) {
            socket.emit(ChatEventType.Error, error.message);
        }
    });

    // event listen and push private message
    socket.on(
        ChatEventType.PrivateMessage,
        async ({ content, idMessage, type, roomId }) => {
            // check and add room to store when user disconnect
            try {
                await saveRoomWhenRedisClear(roomId);
                const roomEmit = await RoomStore.getRoom(roomId);

                if (!validUserInRoom(roomEmit)) {
                    throw new AuthencationError(
                        'Đây không phải phòng chat của bạn'
                    );
                }
                let message: any = {
                    id: idMessage ? idMessage : Math.floor(Date.now() / 1000),
                    content,
                    type: type,
                    user_id: socket.user._id,
                    user: socket.user,
                };
                if (type == 'image') {
                    try {
                        let result_image = await probe(content);
                        message.height = result_image.height
                        message.width = result_image.width
                        // console.log(result_image);
                    } catch (error) {
                        message.height = 160
                        message.width = 240
                    }
                }

                // save message into memory Room and update DB
                // await roomChatRepo.addMessage(roomId, message);
                // send to room to get and add message
                let save_mess = await RoomStore.saveMessage(roomId, message);
                if (save_mess) {
                    socket
                        .to(roomId)
                        .emit(ChatEventType.PrivateMessage, { message, roomId });
                    sendNotification(roomId, message);
                    socket.emit(ChatEventType.Sucess, {
                        type: ResponseSocketSussess.PushMessage,
                        message
                    })
                } else {
                    console.log('------------------------chat-ko gui duoc-' + (new Date()) + '---------------')
                    console.log(message)
                    console.log('------------------------chat-----------------')
                    socket.emit(ChatEventType.ErrorHaveResponse, {
                        type: ResponseSocketSussess.PushMessage,
                        message
                    });
                }
            } catch (error) {
                console.log(error);
                socket.emit(ChatEventType.Error, error.message);
            }
        }
    );

    socket.on(ChatEventType.pushUserOnline, async ({ userId }) => {
        try {
            let user = await UserRepo.getInstance().getInfoUserByRoom(userId)
            if (user) {
                socket.to(userId.toString()).emit(userId.toString(), { user })
            }
        } catch (error) {
            socket.emit(ChatEventType.Error, error.message);
        }
    })

    socket.on(ChatEventType.pushUserOffline, async ({ userId }) => {
        try {
            let user = await UserRepo.getInstance().getInfoUserByRoom(userId)
            if (user) {
                socket.to(userId.toString()).emit(userId.toString(), { user })
            }
        } catch (error) {
            socket.emit(ChatEventType.Error, error.message);
        }
    })


    socket.on(ChatEventType.getUser, async ({ userId }) => {
        try {
            let user = await UserRepo.getInstance().getInfoUserByRoom(userId)
            if (user) {
                socket.emit(userId.toString(), { user })
            }
        } catch (error) {
            socket.emit(ChatEventType.Error, error.message);
        }
    })
};
