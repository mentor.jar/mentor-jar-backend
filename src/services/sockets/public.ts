import { InMemoryMessageForRoomStore } from '../../SocketStores/MessageStore';
import { RoomChatRepo } from '../../repositories/RoomChatRepo';
import { PublicEventType } from './definitions';
import { RoomChatDTO } from '../../DTO/RoomChatDTO';
const RoomStore = InMemoryMessageForRoomStore.getInstance();
const roomChatRepo = RoomChatRepo.getInstance();

export const socketPublic = socket => {
    // fetch message of room exist and join room chat
    socket.on(PublicEventType.LoadMoreMess, async ({ roomId, indexLoadMore }) => {
        const roomEmit = await RoomStore.getRoom(roomId);

        if (roomEmit) {
            let roomMerge = await roomChatRepo.mergeMessage(roomEmit)
            let dtoChat = new RoomChatDTO(roomMerge);
            socket.emit(PublicEventType.LoadMoreMess, {
                data: dtoChat.getEndMoreMessage(indexLoadMore),
                roomId,
            });
        }
    });

    // fetch message of room exist and join room chat
    socket.on(PublicEventType.LoadNewMess, async ({ roomId, idMessage }) => {
        const roomEmit = await RoomStore.getRoom(roomId);
        if (roomEmit) {
            let dtoChat = new RoomChatDTO(roomEmit);
            socket.emit(PublicEventType.LoadNewMess, {
                data: dtoChat.getNewMessage(idMessage),
                roomId,
            });
        }
    });
};
