

import { InMemoryMessageForRoomStore } from "../../SocketStores/MessageStore";
import { RoomChatRepo } from "../../repositories/RoomChatRepo";
import { text } from "body-parser";
import { QueryDBError } from "../../base/customError";
import { InMemoryStreamForStore } from "../../SocketStores/StreamStore";
import { RoomChatDTO } from "../../DTO/RoomChatDTO";
import { LiveStreamEventType } from "./definitions";
import { randomInt } from "../../utils/helper";
const probe = require('probe-image-size');
const RoomStore = InMemoryMessageForRoomStore.getInstance();
const roomChatRepo = RoomChatRepo.getInstance();
const StreamStore = InMemoryStreamForStore.getInstance();
export const socketLivestream = (socket) => {

    // fetch message of room exist and join room chat
    const saveRoomWhenRedisClear = async (roomId) => {
        const isRoomApi = await RoomStore.isRoomApi(roomId);
        if (!isRoomApi) {
            let room = await roomChatRepo.getRoomById(roomId);
            if (!room) {
                socket.emit(LiveStreamEventType.Error, 'Room not found')
                return;
            };
            RoomStore.saveCheckRoomApi(roomId);
            await RoomStore.saveRoom(room);
        }
    }

    socket.on(LiveStreamEventType.RoomLiveStream, async ({ roomId }) => {
        try {
            await saveRoomWhenRedisClear(roomId);
            socket.join(roomId);
            roomChatRepo.userJoinRoom(roomId, socket.user)
            await RoomStore.userJoinRoom(roomId, socket.user)

            const roomEmit = await RoomStore.getRoom(roomId)
            // console.log(room);
            let dtoChat = new RoomChatDTO(roomEmit);
            roomEmit.messages = dtoChat.getEndMoreMessage();
            socket.emit(roomId, roomEmit);
            let view = await RoomStore.getView(roomId);
            let stream = await StreamStore.getStream(roomEmit.stream_id)
            if (!view && !stream.active) view = randomInt(2, 10)
            socket.to(roomId).emit(LiveStreamEventType.AmountView, {
                view: view,
                streamId: roomEmit.stream_id,
                user: socket.user
            });
        } catch (error) {
            socket.emit(LiveStreamEventType.Error, error.message)
        }
    });

    // left livestream and leave room
    socket.on(LiveStreamEventType.LeftRoom, async ({ roomId }) => {
        try {
            await saveRoomWhenRedisClear(roomId);

            roomChatRepo.userLeftRoom(roomId, socket.user)
            await RoomStore.userLeftRoom(roomId, socket.user)
            const roomEmit = await RoomStore.getRoom(roomId)
            socket.leave(roomId);
            let view = await RoomStore.getView(roomId);
            if (!view) view = randomInt(2, 10);
            socket.to(roomId).emit(LiveStreamEventType.AmountView, {
                view: view,
                streamId: roomEmit.stream_id,
                user: null
            });
        } catch (error) {
            socket.emit(LiveStreamEventType.Error, error.message)
        }


    })

    // send heart when user click icon heart
    socket.on(LiveStreamEventType.SendHeart, async ({ streamId }) => {
        try {
            let stream = await StreamStore.getStream(streamId)
            if (stream) {
                socket.to(stream.room_chat_id).emit(LiveStreamEventType.AmountHeart, {
                    heart: stream.heart + 1,
                    // user: socket.user,
                    streamId: streamId
                });
                await StreamStore.updateHeart(streamId);
            }
        } catch (error) {

        }

    })

    // event listen and comment livestream
    socket.on(LiveStreamEventType.Comment, async ({ content, idMessage, type, roomId }) => {
        try {
            await saveRoomWhenRedisClear(roomId);

            const user = await RoomStore.findUser(roomId, socket.user._id)

            if (user.is_muted == true) {
                socket.emit(LiveStreamEventType.Error, new Error('Bạn đã bị cấm chat'))
                return;
            }
            const message: any = {
                id: idMessage ? idMessage : Math.floor(Date.now() / 1000),
                content,
                type: type == 'image' ? type : 'text',
                user_id: socket.user._id,
                user: socket.user
            };
            if (type == 'image') {
                try {
                    let result_image = await probe(content);
                    message.height = result_image.height
                    message.width = result_image.width
                    console.log(result_image);
                } catch (error) {
                    message.height = 160
                    message.width = 240
                }
            }
            const roomEmit = await RoomStore.getRoom(roomId)
            socket.to(roomId).emit(LiveStreamEventType.Comment, {
                message,
                roomId,
                streamId: roomEmit.stream_id
            });

            RoomStore.saveMessage(roomId, message);
            roomChatRepo.addMessage(roomId, message);
        } catch (error) {
            socket.emit(LiveStreamEventType.Error, error.message)
        }

    });

    // mute user 
    socket.on(LiveStreamEventType.PinMessage, async ({ streamId, message }) => {
        try {

            let stream = await StreamStore.getStream(streamId)
            if (stream) {
                socket.to(stream.room_chat_id).emit(LiveStreamEventType.PinMessage, {
                    streamId,
                    message
                });
                await StreamStore.pinMessage(streamId, message);
            } else {
                socket.emit(LiveStreamEventType.Error, "Stream không tồn tại")
                return;
            }
        } catch (error) {
            socket.emit(LiveStreamEventType.Error, error.message)
        }
    });

    // pin message user 
    socket.on(LiveStreamEventType.Mute, async ({ creatorId, userId, roomId }) => {
        try {
            await saveRoomWhenRedisClear(roomId);

            if (socket.user._id != creatorId) {
                socket.emit(LiveStreamEventType.Error, 'Chỉ có chủ phòng mới được mute')
                return;
            }
            if (userId == creatorId) {
                socket.emit(LiveStreamEventType.Error, 'Không được mute chính mình')
                return;
            }
            //update CSDL
            await RoomStore.saveMute(roomId, userId);
            await roomChatRepo.saveMute(roomId, userId);
            const room = await RoomStore.getRoom(roomId);
            //const usersMuted = room.users.filter(user => user.is_muted == true);
            socket.to(roomId).emit(LiveStreamEventType.Mute, room.users);
        } catch (error) {
            socket.emit(LiveStreamEventType.Error, error.message)
        }
    });

    // unmute user 
    socket.on(LiveStreamEventType.UnMute, async ({ userId, roomId }) => {
        try {
            await saveRoomWhenRedisClear(roomId);

            //update CSDL
            await RoomStore.saveUnmute(roomId, userId);
            await roomChatRepo.saveUnmute(roomId, userId);
            const room = await RoomStore.getRoom(roomId);
            socket.to(roomId).emit(LiveStreamEventType.UnMute, room.users);
        } catch (error) {
            socket.emit(LiveStreamEventType.Error, error.message)
        }
    });

    // update list product
    socket.on(LiveStreamEventType.UpdateProduct, async ({ streamId, productIds }) => {
        try {
            let stream = await StreamStore.updateProducts(streamId, productIds)

            socket.nsp.to(stream.room_chat_id).emit(LiveStreamEventType.GetStream, { stream: stream });
            // socket.emit("get stream", { stream: stream });
        } catch (error) {
            socket.emit(LiveStreamEventType.Error, "không thể chỉnh sửa sản phẩm")
        }


    });
}
