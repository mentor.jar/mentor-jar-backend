import { ListItemProduct } from "./ListItemProduct";

export class CreateOrder {
    private _orderNumber: string;
    private _groupAddressId: number;
    private _cusId: number;
    private _deliveryDate: string;
    private _senderFullName: string;
    private _senderAddress: string;
    private _senderPhone: string;
    private _senderEmail: string;
    private _senderWard: number;
    private _senderDistrict: number;
    private _senderProvince: number;
    private _senderLatitude: number;
    private _senderLongtitude: number;
    private _receiverFullName: string;
    private _receiverAddress: string;
    private _receiverPhone: string;
    private _receiverEmail: string;
    private _receiverWard: number;
    private _receiverDistrict: number;
    private _receiverProvince: number;
    private _receiverLatitude: number;
    private _receiverLongtitude: number;
    private _productName: string;
    private _productDescription: string;
    private _productQuantity: number;
    private _productPrice: number;
    private _productWeight: number;
    private _productLength: number;
    private _productWidth: number;
    private _productHeight: number;
    private _productType: string;
    private _orderPayment: number;
    private _orderService: string;
    private _orderServiceAdd: string;
    private _orderVoucher: string;
    private _orderNote: string;
    private _moneyCollection: number;
    private _moneyTotalFee: number;
    private _moneyFeeCod: number;
    private _moneyFeeVas: number;
    private _moneyFeeInsurrance: number;
    private _moneyFee: number;
    private _moneyFeeOther: number;
    private _moneyTotalVAT: number;
    private _moneyTotal: number;
    private _listItem: any[];

    get orderNumber() {
        return this._orderNumber;
    }
    setOrderNumber(orderNumber: string) {
        this._orderNumber = orderNumber;
        return this;
    }
    get groupAddressId() {
        return this._groupAddressId;
    }
    setGroupAddressId(groupAddressId: number) {
        this._groupAddressId = groupAddressId;
        return this;
    }
    get cusId() {
        return this._cusId;
    }
    setCusId(cusId: number) {
        this._cusId = cusId;
        return this;
    }
    get deliveryDate() {
        return this._deliveryDate;
    }
    setDeliveryDate(deliveryDate: string) {
        this._deliveryDate = deliveryDate;
        return this;
    }
    get senderFullName() {
        return this._senderFullName;
    }
    setSenderFullName(senderFullName: string) {
        this._senderFullName = senderFullName;
        return this;
    }
    get senderAddress() {
        return this._senderAddress;
    }
    setSenderAddress(senderAddress: string) {
        this._senderAddress = senderAddress;
        return this;
    }
    get senderPhone() {
        return this._senderPhone;
    }
    setSenderPhone(senderPhone: string) {
        this._senderPhone = senderPhone;
        return this;
    }
    get senderEmail() {
        return this._senderEmail;
    }
    setSenderEmail(senderEmail: string) {
        this._senderEmail = senderEmail;
        return this;
    }
    get senderWard() {
        return this._senderWard;
    }
    setSenderWard(senderWard: number) {
        this._senderWard = senderWard;
        return this;
    }
    get senderDistrict() {
        return this._senderDistrict;
    }
    setSenderDistrict(senderDistrict: number) {
        this._senderDistrict = senderDistrict;
        return this;
    }
    get senderProvince() {
        return this._senderProvince;
    }
    setSenderProvince(senderProvince: number) {
        this._senderProvince = senderProvince;
        return this;
    }
    get senderLatitude() {
        return this._senderLatitude;
    }
    setSenderLatitude(senderLatitude: number) {
        this._senderLatitude = senderLatitude;
        return this;
    }
    get senderLongtitude() {
        return this._senderLongtitude;
    }
    setSenderLongtitude(senderLongtitude: number) {
        this._senderLongtitude = senderLongtitude;
        return this;
    }
    get receiverFullName() {
        return this._receiverFullName;
    }
    setReceiverFullName(receiverFullName: string) {
        this._receiverFullName = receiverFullName;
        return this;
    }
    get receiverAddress() {
        return this._receiverAddress;
    }
    setReceiverAddress(receiverAddress: string) {
        this._receiverAddress = receiverAddress;
        return this;
    }
    get receiverPhone() {
        return this._receiverPhone;
    }
    setReceiverPhone(receiverPhone: string) {
        this._receiverPhone = receiverPhone;
        return this;
    }
    get receiverEmail() {
        return this._receiverEmail;
    }
    setReceiverEmail(receiverEmail: string) {
        this._receiverEmail = receiverEmail;
        return this;
    }
    get receiverWard() {
        return this._receiverWard;
    }
    setReceiverWard(receiverWard: number) {
        this._receiverWard = receiverWard;
        return this;
    }
    get receiverDistrict() {
        return this._receiverDistrict;
    }
    setReceiverDistrict(receiverDistrict: number) {
        this._receiverDistrict = receiverDistrict;
        return this;
    }
    get receiverProvince() {
        return this._receiverProvince;
    }
    setReceiverProvince(receiverProvince: number) {
        this._receiverProvince = receiverProvince;
        return this;
    }
    get receiverLatitude() {
        return this._receiverLatitude;
    }
    setReceiverLatitude(receiverLatitude: number) {
        this._receiverLatitude = receiverLatitude;
        return this;
    }
    get receiverLongtitude() {
        return this._receiverLongtitude;
    }
    setReceiverLongtitude(receiverLongtitude: number) {
        this._receiverLongtitude = receiverLongtitude;
        return this;
    }
    get productName() {
        return this._productName;
    }
    setProductName(productName: string) {
        this._productName = productName;
        return this;
    }
    get productDescription() {
        return this._productDescription;
    }
    setProductDescription(productDescription: string) {
        this._productDescription = productDescription;
        return this;
    }
    get productQuantity() {
        return this._productQuantity;
    }
    setProductQuantity(productQuantity: number) {
        this._productQuantity = productQuantity;
        return this;
    }
    get productPrice() {
        return this._productPrice;
    }
    setProductPrice(productPrice: number) {
        this._productPrice = productPrice;
        return this;
    }
    get productWeight() {
        return this._productWeight;
    }
    setProductWeight(productWeight: number) {
        this._productWeight = productWeight;
        return this;
    }
    get productLength() {
        return this._productLength;
    }
    setProductLength(productLength: number) {
        this._productLength = productLength;
        return this;
    }
    get productWidth() {
        return this._productWidth;
    }
    setProductWidth(productWidth: number) {
        this._productWidth = productWidth;
        return this;
    }
    get productHeight() {
        return this._productHeight;
    }
    setProductHeight(productHeight: number) {
        this._productHeight = productHeight;
        return this;
    }
    get productType() {
        return this._productType;
    }
    setProductType(productType: string) {
        this._productType = productType; // HH - TH (only set with HH) // hàng hóa
        return this;
    }
    get orderPayment() {
        return this._orderPayment;
    }
    setOrderPayment(orderPayment: number) {
        this._orderPayment = orderPayment;
        return this;
    }
    get orderService() {
        return this._orderService;
    }
    setOrderService(orderService: string) {
        this._orderService = orderService;
        return this;
    }
    get orderServiceAdd() {
        return this._orderServiceAdd;
    }
    setOrderServiceAdd(orderServiceAdd: string) {
        this._orderServiceAdd = orderServiceAdd;
        return this;
    }
    get orderVoucher() {
        return this._orderVoucher;
    }
    setOrderVoucher(orderVoucher: string) {
        this._orderVoucher = orderVoucher;
        return this;
    }
    get orderNote() {
        return this._orderNote;
    }
    setOrderNote(orderNote: string) {
        this._orderNote = orderNote;
        return this;
    }
    get moneyCollection() {
        return this._moneyCollection;
    }
    setMoneyCollection(moneyCollection: number) {
        // Phí thu hộ = total order
        this._moneyCollection = moneyCollection;
        return this;
    }
    get moneyTotalFee() {
        return this._moneyTotalFee;
    }
    setMoneyTotalFee(moneyTotalFee: number) {
        // Phí Cước chính = 0
        this._moneyTotalFee = moneyTotalFee;
        return this;
    }
    get moneyFeeCod() {
        return this._moneyFeeCod;
    }
    setMoneyFeeCod(moneyFeeCod: number) {
        // Phụ phí thu hộ = 0
        this._moneyFeeCod = moneyFeeCod;
        return this;
    }
    get moneyFeeVas() {
        return this._moneyFeeVas;
    }
    setMoneyFeeVas(moneyFeeVas: number) {
        // Phí gia tăng ( các dịch vụ cộng thêm khác có phát sinh) = 0
        this._moneyFeeVas = moneyFeeVas;
        return this;
    }
    get moneyFeeInsurrance() {
        return this._moneyFeeInsurrance;
    }
    setMoneyFeeInsurrance(moneyFeeInsurrance: number) {
        // 	Phí bảo hiểm = 0
        this._moneyFeeInsurrance = moneyFeeInsurrance;
        return this;
    }
    get moneyFee() {
        return this._moneyFee;
    }
    setMoneyFee(moneyFee: number) {
        // 	Phụ phí = 0
        this._moneyFee = moneyFee;
        return this;
    }
    get moneyFeeOther() {
        return this._moneyFeeOther;
    }
    setMoneyFeeOther(moneyFeeOther: number) {
        // 	Phụ phí khác = 0
        this._moneyFeeOther = moneyFeeOther;
        return this;
    }
    get moneyTotalVAT() {
        return this._moneyTotalVAT;
    }
    setMoneyTotalVAT(moneyTotalVAT: number) {
        // 	Phụ phí vat = 0
        this._moneyTotalVAT = moneyTotalVAT;
        return this;
    }
    get moneyTotal() {
        return this._moneyTotal;
    }
    setMoneyTotal(moneyTotal: number) {
        // 	Tổng tiền bao gồm VAT = 0
        this._moneyTotal = moneyTotal;
        return this;
    }
    get listItem() {
        const listItemDto = this._listItem.map((item) =>
            new ListItemProduct()
                .setProductName(item.product.name)
                .setProductPrice(item.sale_price)
                .setProductQuantity(item.quantity)
                .setProductWeight(item.product.weight)
                .get()
        );
        return listItemDto;
    }
    setListItem(listItem: any[]) {
        this._listItem = listItem;
        return this;
    }

    get(): any {
        const request = {
            ORDER_NUMBER: this.orderNumber,
            GROUPADDRESS_ID: this.groupAddressId,
            CUS_ID: this.cusId,
            DELIVERY_DATE: this.deliveryDate, // '11/10/2018 15:09:52'

            SENDER_FULLNAME: this.senderFullName,
            SENDER_ADDRESS: this.senderAddress,
            SENDER_PHONE: this.senderPhone,
            SENDER_EMAIL: this.senderEmail,
            SENDER_WARD: this.senderWard,
            SENDER_DISTRICT: this.senderDistrict,
            SENDER_PROVINCE: this.senderProvince,
            SENDER_LATITUDE: 0,
            SENDER_LONGITUDE: 0,

            RECEIVER_FULLNAME: this.receiverFullName,
            RECEIVER_ADDRESS: this.receiverAddress,
            RECEIVER_PHONE: this.receiverPhone,
            RECEIVER_EMAIL: this.receiverEmail,
            RECEIVER_WARD: this.receiverWard,
            RECEIVER_DISTRICT: this.receiverDistrict,
            RECEIVER_PROVINCE: this.receiverProvince,
            RECEIVER_LATITUDE: 0,
            RECEIVER_LONGITUDE: 0,

            PRODUCT_NAME: this.productName,
            PRODUCT_DESCRIPTION: this.productDescription,
            PRODUCT_QUANTITY: this.productQuantity,
            PRODUCT_PRICE: this.productPrice,
            PRODUCT_WEIGHT: this.productWeight,
            PRODUCT_LENGTH: this.productLength,
            PRODUCT_WIDTH: this.productWidth,
            PRODUCT_HEIGHT: this.productHeight,
            PRODUCT_TYPE: this.productType,
            ORDER_PAYMENT: this.orderPayment,
            ORDER_SERVICE: this.orderService,
            ORDER_SERVICE_ADD: this.orderServiceAdd, // ''
            ORDER_VOUCHER: this.orderVoucher, // ''
            ORDER_NOTE: this.orderNote,
            MONEY_COLLECTION: this.moneyCollection,
            MONEY_TOTALFEE: this.moneyTotalFee, // 0
            MONEY_FEECOD: this.moneyFeeCod, // 0
            MONEY_FEEVAS: this.moneyFeeVas, // 0
            MONEY_FEEINSURRANCE: this.moneyFeeInsurrance, // 0
            MONEY_FEE: this.moneyFee, // 0
            MONEY_FEEOTHER: this.moneyFeeOther, // 0
            MONEY_TOTALVAT: this.moneyTotalVAT, // 0
            MONEY_TOTAL: this.moneyTotal, // 0
            LIST_ITEM: this.listItem,
        };

        return request;
    }
}
