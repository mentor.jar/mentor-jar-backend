export class PricingServiceResponse {
    private _serviceCode: string;
    private _serviceName: string;
    private _serviceFee: number;
    private _serviceTime: string;
    private _exchangeWeight: number;

    get serviceCode() {
        return this._serviceCode;
    }
    setServiceCode(serviceCode: string) {
        this._serviceCode = serviceCode;
        return this;
    }
    get serviceName() {
        return this._serviceName;
    }
    setServiceName(serviceName: string) {
        this._serviceName = serviceName;
        return this;
    }
    get serviceFee() {
        return this._serviceFee;
    }
    setServiceFee(serviceFee: number) {
        this._serviceFee = serviceFee;
        return this;
    }
    get serviceTime() {
        return this._serviceTime;
    }
    setServiceTime(serviceTime: string) {
        this._serviceTime = serviceTime;
        return this;
    }
    get exchangeWeight() {
        return this._exchangeWeight;
    }
    setExchangeWeight(exchangeWeight: number) {
        this._exchangeWeight = exchangeWeight;
        return this;
    }
    get(): any {
        const request = {
            order_service: this.serviceCode,
            fee: this.serviceFee,
            service_name: this.serviceName,
            service_time: this.serviceTime,
            exchange_weight: this.exchangeWeight,
        };

        return request;
    }

}
