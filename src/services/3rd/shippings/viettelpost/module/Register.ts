import { IRegister } from '../interface';

export default class LoginResponseBuilder {
    private _name: string;
    private _email: string;
    private _phone: string;
    private _address: string;
    private _wardsId: number;

    get name() {
        return this._name;
    }

    setName(name: string) {
        this._name = name;
        return this;
    }

    get email() {
        return this._email;
    }

    setEmail(email: string) {
        this._email = email;
        return this;
    }

    get address() {
        return this._address;
    }

    setAddress(address: string) {
        this._address = address;
        return this;
    }

    get phone() {
        return this._phone;
    }

    setPhone(phone: string) {
        this._phone = phone;
        return this;
    }

    get wardsId() {
        return this._wardsId;
    }

    setWardsId(wardsId: number) {
        this._wardsId = wardsId;
        return this;
    }

    get(): IRegister {
        const request: IRegister = {
            EMAIL: this.email,
            PHONE: this.phone,
            NAME: this.name,
            ADDRESS: this.address,
            WARDS_ID: this.wardsId,
        };

        return request;
    }
}
