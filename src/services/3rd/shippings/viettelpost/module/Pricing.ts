export default class GetPricingBuilder {
    private _productWeight: number;
    private _productPrice: number;
    private _moneyCollection: number;

    private _orderServiceAdd: string;
    private _orderService: string;

    private _senderProvince: number;
    private _senderDistrict: number;

    private _receiverProvince: number;
    private _receiverDistrict: number;

    private _productType: string;
    private _nationalType: number;

    private _productWidth: number;
    private _productHeight: number;
    private _productLength: number;

    get productWeight() {
        return this._productWeight;
    }

    setProductWeight(product_weight: number) {
        this._productWeight = product_weight;
        return this;
    }

    get productPrice() {
        return this._productPrice;
    }

    setProductPrice(product_price: number) {
        this._productPrice = product_price;
        return this;
    }

    get moneyCollection() {
        return this._moneyCollection;
    }

    setMoneyCollection(moneyCollection: number) {
        this._moneyCollection = moneyCollection;
        return this;
    }

    get orderServiceAdd() {
        return this._orderServiceAdd;
    }

    setOrderServiceAdd(orderServiceAdd: string) {
        this._orderServiceAdd = orderServiceAdd;
        return this;
    }

    get orderService() {
        return this._orderService;
    }

    setOrderService(orderService: string) {
        this._orderService = orderService;
        return this;
    }

    get senderProvince() {
        return this._senderProvince;
    }

    setSenderProvince(senderProvince: number) {
        this._senderProvince = senderProvince;
        return this;
    }

    get senderDistrict() {
        return this._senderDistrict;
    }

    setSenderDistrict(senderDistrict: number) {
        this._senderDistrict = senderDistrict;
        return this;
    }

    get receiverProvince() {
        return this._receiverProvince;
    }

    setReceiverProvince(receiverProvince: number) {
        this._receiverProvince = receiverProvince;
        return this;
    }

    get receiverDistrict() {
        return this._receiverDistrict;
    }

    setReceiverDistrict(receiverDistrict: number) {
        this._receiverDistrict = receiverDistrict;
        return this;
    }

    get productType() {
        return this._productType;
    }

    setProductType(productType: string) {
        this._productType = productType;
        return this;
    }

    get nationalType() {
        return this._nationalType;
    }

    setNationalType(nationalType: number) {
        this._nationalType = nationalType;
        return this;
    }

    get productWidth() {
        return this._productWidth;
    }

    setProductWidth(productWidth: number) {
        this._productWidth = productWidth;
        return this;
    }

    get productHeight() {
        return this._productHeight;
    }

    setProductHeight(productHeight: number) {
        this._productHeight = productHeight;
        return this;
    }

    get productLength() {
        return this._productLength;
    }

    setProductLength(productLength: number) {
        this._productLength = productLength;
        return this;
    }

    get(): any {
        const request: any = {
            PRODUCT_WEIGHT: this.productWeight, // Product weight
            PRODUCT_PRICE: this.productPrice, //Product price
            MONEY_COLLECTION: this.moneyCollection, // collection money (that customers want VTP to collect from receivers)
            ORDER_SERVICE_ADD: this.orderServiceAdd, // Additional Services (contract), additional services are separated by “,”
            ORDER_SERVICE: this.orderService, // Services (contract)
            SENDER_PROVINCE: this.senderProvince, // Province/city ID send
            SENDER_DISTRICT: this.senderDistrict, // District ID send
            RECEIVER_PROVINCE: this.receiverProvince, // Province/city ID receive
            RECEIVER_DISTRICT: this.receiverDistrict, // District ID receive
            PRODUCT_TYPE: this.productType, // Product type: TH: Thư/ Envelope || HH: Hàng hóa/ Goods
            NATIONAL_TYPE: this.nationalType,
        };

        return request;
    }
}
