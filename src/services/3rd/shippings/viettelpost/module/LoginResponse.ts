import { ILoginResponse } from '../interface';

export default class LoginResponseBuilder {
    private _userId: number;
    private _token: string;
    private _partner: number;
    private _phone: string;
    private _expired: string | 0;
    private _encrypted: string | null;
    private _source: number;
    private _status: number;

    private _type: string; // partner or owner_connect

    get type() {
        return this._type;
    }

    setType(type: string) {
        this._type = type;
        return this;
    }
    
    get status() {
        return this._status;
    }

    setStatus(status: number) {
        this._status = status;
        return this;
    }

    get userId() {
        return this._userId;
    }

    setUserId(user_id: number) {
        this._userId = user_id;
        return this;
    }

    get token() {
        return this._token;
    }

    setToken(token: string) {
        this._token = token;
        return this;
    }

    get partner() {
        return this._partner;
    }

    setPartner(partner: number) {
        this._partner = partner;
        return this;
    }

    get phone() {
        return this._phone;
    }

    setPhone(phone: string) {
        this._phone = phone;
        return this;
    }

    get expried() {
        return this._expired;
    }

    setExpired(expried: string) {
        this._expired = expried;
        return this;
    }

    get encrypted() {
        return this._encrypted;
    }

    setEncrypted(encrypted: string) {
        this._encrypted = encrypted;
        return this;
    }

    get source() {
        return this._source;
    }

    setSource(source: number) {
        this._source = source;
        return this;
    }

    get(): ILoginResponse {
        let request: ILoginResponse = {
            userId: this.userId,
            token: this.token,
            partner: this.partner,
            phone: this.phone,
            expired: this.expried,
            encrypted: this.encrypted,
            source: this.source,
        };

        if (this.type == 'owner_connect') {
            request.status = this.status;
        }

        return request;
    }
}
