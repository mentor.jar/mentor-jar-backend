
export default class PricingResponseBuilder {
    private _moneyTotalOld: number;
    private _moneyTotal: number;
    private _moneyTotalFee: number;
    private _moneyFee: number;
    private _moneyCollectionFee: number;
    private _moneyOtherFee: number;
    private _moneyVas: number;
    private _moneyVat: number;
    private _kpiHp: number;
    private _orderService: string;

    get moneyTotalOld() {
        return this._moneyTotalOld;
    }

    setMoneyTotalOld(moneyTotalOld: number) {
        this._moneyTotalOld = moneyTotalOld;
        return this;
    }

    get moneyTotal() {
        return this._moneyTotal;
    }

    setMoneyTotal(moneyTotal: number) {
        this._moneyTotal = moneyTotal;
        return this;
    }

    get moneyTotalFee() {
        return this._moneyTotalFee;
    }

    setMoneyTotalFee(moneyTotalFee: number) {
        this._moneyTotalFee = moneyTotalFee;
        return this;
    }

    get moneyFee() {
        return this._moneyFee;
    }

    setMoneyFee(moneyFee: number) {
        this._moneyFee = moneyFee;
        return this;
    }
   
    get moneyCollectionFee() {
        return this._moneyCollectionFee;
    }

    setMoneyCollectionFee(moneyCollectionFee: number) {
        this._moneyCollectionFee = moneyCollectionFee;
        return this;
    }

    get moneyVas() {
        return this._moneyVas;
    }

    setMoneyVas(moneyVas: number) {
        this._moneyVas = moneyVas;
        return this;
    }
    
    get moneyVat() {
        return this._moneyVat;
    }

    setMoneyVat(moneyVat: number) {
        this._moneyVat = moneyVat;
        return this;
    }
    
    get kpiHp() {
        return this._kpiHp;
    }

    setkpiHp(kpiHp: number) {
        this._kpiHp = kpiHp;
        return this;
    }
    
    get orderService() {
        return this._orderService;
    }

    setOrderService(orderService: string) {
        this._orderService = orderService;
        return this;
    }

    
    get(): any {
        const request: any = {
            fee: this.moneyTotal,
            order_service: this.orderService
        };

        return request;
    }
}
