export class ListItemProduct {
    private _productName: string;
    private _productPrice: number;
    private _productWeight: number;
    private _productQuantity: number;

    get productName() {
        return this._productName;
    }
    setProductName(productName: string) {
        this._productName = productName;
        return this;
    }
    get productPrice() {
        return this._productPrice;
    }
    setProductPrice(productPrice: number) {
        this._productPrice = productPrice;
        return this;
    }
    get productWeight() {
        return this._productWeight;
    }
    setProductWeight(productWeight: number) {
        this._productWeight = productWeight;
        return this;
    }
    get productQuantity() {
        return this._productQuantity;
    }
    setProductQuantity(productQuantity: number) {
        this._productQuantity = productQuantity;
        return this;
    }
    get(): any {
        const request = {
            PRODUCT_NAME: this.productName,
            PRODUCT_PRICE: this.productPrice,
            PRODUCT_WEIGHT: this.productWeight,
            PRODUCT_QUANTITY: this.productQuantity,
        };

        return request;
    }

}
