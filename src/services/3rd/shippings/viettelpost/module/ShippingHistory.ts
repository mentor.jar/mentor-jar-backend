import moment from 'moment';
import { IShippingHistory } from '../interface';

export default class ShippingHistoryBuilder {
    private _partnerId: string;
    private _labelId: string;
    private _shippingStatus: string;
    private _statusId: string;
    private _statusName: string;

    private _fee: number;
    private _pickMoney: number;
    private _feeCod: number;
    private _returnPartPackage: number;
    private _weight: number;
    private _reason: string;
    private _reasonCode: string;
    private _reasonCodeShipping: string;
    private _note: string;
    private _actionTime: string;

    private _orderService: string;
    private _expectedDelivery: string;

    get partnerId() {
        return this._partnerId;
    }

    setPartnerId(partnerId: string) {
        this._partnerId = partnerId;
        return this;
    }

    get labelId() {
        return this._labelId;
    }

    setLabelId(labelId: string) {
        this._labelId = labelId;
        return this;
    }

    get statusId() {
        return this._statusId;
    }

    setStatusId(statusId: string) {
        this._statusId = statusId;
        return this;
    }

    get statusName() {
        return this._statusName;
    }

    setStatusName(statusName: string) {
        this._statusName = statusName;
        return this;
    }

    get fee() {
        return this._fee;
    }

    setFee(fee: number) {
        this._fee = fee;
        return this;
    }

    get feeCod() {
        return this._feeCod;
    }

    setFeeCod(feeCod: number) {
        this._feeCod = feeCod;
        return this;
    }

    get returnPartPackage() {
        return this._returnPartPackage;
    }

    setReturnPartPackage(returnPartPackage: number) {
        this._returnPartPackage = returnPartPackage;
        return this;
    }

    get reason() {
        return this._reason;
    }

    setReason(reason: string) {
        this._reason = reason;
        return this;
    }

    get reasonCode() {
        return this._reasonCode;
    }

    setReasonCode(reasonCode: string) {
        this._reasonCode = reasonCode;
        return this;
    }

    get reasonCodeShipping() {
        return this._reasonCodeShipping;
    }

    setReasonCodeShipping(reasonCodeShipping: string) {
        this._reasonCodeShipping = reasonCodeShipping;
        return this;
    }

    get note() {
        return this._note;
    }

    setNote(note: string) {
        this._note = note;
        return this;
    }

    get actionTime() {
        return this._actionTime;
    }

    setActionTime(actionTime: string) {
        const date = moment(actionTime, "DD/MM/YYYY HH:mm:ss");
        this._actionTime = date.toString();
        return this;
    }

    get orderService() {
        return this._orderService;
    }

    setOrderService(orderService: string) {
        this._orderService = orderService;
        return this;
    }

    get shippingStatus() {
        return this._shippingStatus;
    }

    setShippingStatus(shippingStatus: string) {
        this._shippingStatus = shippingStatus;
        return this;
    }

    get pickMoney() {
        return this._pickMoney;
    }

    setPickMoney(pickMoney: number) {
        this._pickMoney = pickMoney;
        return this;
    }

    get expectedDelivery() {
        return this._expectedDelivery;
    }

    setExpectedDelivery(expectedDelivery: string) {
        this._expectedDelivery = expectedDelivery;
        return this;
    }

    get weight() {
        return this._weight;
    }

    setWeight(weight: number) {
        this._weight = weight;
        return this;
    }

    get(): IShippingHistory {
        const request: IShippingHistory = {
            partner_id: this.partnerId,
            label_id: this.labelId,
            shipping_status: this.shippingStatus,

            status_id: this.statusId,
            status_name: this.statusName,
            fee: this.fee,
            fee_cod: this.feeCod,
            pick_money: this.pickMoney,
            return_part_package: this.returnPartPackage,
            weight: this.weight,
            reason: this.reason,
            reason_code: this.reasonCode,
            reason_code_shipping: this.reasonCodeShipping,
            action_time: this.actionTime,
            order_service: this.orderService,
            expected_delivery: this.expectedDelivery,
        };

        return request;
    }
}
