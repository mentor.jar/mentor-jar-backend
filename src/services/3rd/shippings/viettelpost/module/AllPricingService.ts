export default class AllPricingService {
    private _productWeight: number;
    private _productPrice: number;
    private _moneyCollection: number;

    private _senderProvince: number;
    private _senderDistrict: number;

    private _receiverProvince: number;
    private _receiverDistrict: number;

    private _productType: string;
    private _type: number;

    private _productWidth: number;
    private _productHeight: number;

    get productWeight() {
        return this._productWeight;
    }

    setProductWeight(product_weight: number) {
        this._productWeight = product_weight;
        return this;
    }

    get productPrice() {
        return this._productPrice;
    }

    setProductPrice(product_price: number) {
        this._productPrice = product_price;
        return this;
    }

    get moneyCollection() {
        return this._moneyCollection;
    }

    setMoneyCollection(moneyCollection: number) {
        this._moneyCollection = moneyCollection;
        return this;
    }

    get senderProvince() {
        return this._senderProvince;
    }

    setSenderProvince(senderProvince: number) {
        this._senderProvince = senderProvince;
        return this;
    }

    get senderDistrict() {
        return this._senderDistrict;
    }

    setSenderDistrict(senderDistrict: number) {
        this._senderDistrict = senderDistrict;
        return this;
    }

    get receiverProvince() {
        return this._receiverProvince;
    }

    setReceiverProvince(receiverProvince: number) {
        this._receiverProvince = receiverProvince;
        return this;
    }

    get receiverDistrict() {
        return this._receiverDistrict;
    }

    setReceiverDistrict(receiverDistrict: number) {
        this._receiverDistrict = receiverDistrict;
        return this;
    }

    get productType() {
        return this._productType;
    }

    setProductType(productType: string) {
        this._productType = productType;
        return this;
    }

    get type() {
        return this._type;
    }

    setType(type: number) {
        this._type = type;
        return this;
    }

    get productWidth() {
        return this._productWidth;
    }

    setProductWidth(productWidth: number) {
        this._productWidth = productWidth;
        return this;
    }

    get productHeight() {
        return this._productHeight;
    }

    setProductHeight(productHeight: number) {
        this._productHeight = productHeight;
        return this;
    }

    get(): any {
        const request: any = {
            PRODUCT_WEIGHT: this.productWeight, // Product weight
            PRODUCT_PRICE: this.productPrice, //Product price
            MONEY_COLLECTION: this.moneyCollection, // collection money (that customers want VTP to collect from receivers)
            SENDER_PROVINCE: this.senderProvince, // Province/city ID send
            SENDER_DISTRICT: this.senderDistrict, // District ID send
            RECEIVER_PROVINCE: this.receiverProvince, // Province/city ID receive
            RECEIVER_DISTRICT: this.receiverDistrict, // District ID receive
            PRODUCT_TYPE: this.productType, // Product type: TH: Thư/ Envelope || HH: Hàng hóa/ Goods
            TYPE: this.type,
        };

        return request;
    }
}
