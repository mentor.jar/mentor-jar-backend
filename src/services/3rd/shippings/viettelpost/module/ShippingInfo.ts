import { IShippingInfo } from '../interface';

export default class ShippingInfoBuilder {
    private _partnerId: string;
    private _label: string;
    private _statusId: string;

    private _area: string;
    private _fee: number;
    private _insuranceFee: number;
    private _estimatedPickTime: string;
    private _estimatedDeliverTime: string;
    private _products: any[];
    private _trackingId: string;
    private _sortingCode: string;
    private _isXfast: string;
    private _shippingStatus: string;

    private _pickMoney: number;
    private _moneyOtherFee: number;
    private _moneyFee: number;
    private _moneyCollectionFee: number;
    private _moneyFeeVat: number;
    private _moneyTotalFee: number;
    private _exchangeWeight: number;

    get partnerId() {
        return this._partnerId;
    }

    setPartnerId(partnerId: string) {
        this._partnerId = partnerId;
        return this;
    }

    get label() {
        return this._label;
    }

    setLabel(label: string) {
        this._label = label;
        return this;
    }

    get statusId() {
        return this._statusId;
    }

    setStatusId(statusId: string) {
        this._statusId = statusId;
        return this;
    }

    get area() {
        return this._area;
    }

    setArea(area: string) {
        this._area = area;
        return this;
    }

    get fee() {
        return this._fee;
    }

    setFee(fee: number) {
        this._fee = fee;
        return this;
    }

    get insuranceFee() {
        return this._insuranceFee;
    }

    setInsuranceFee(insuranceFee: number) {
        this._insuranceFee = insuranceFee;
        return this;
    }

    get estimatedPickTime() {
        return this._estimatedPickTime;
    }

    setEstimatedPickTime(estimatedPickTime: string) {
        this._estimatedPickTime = estimatedPickTime;
        return this;
    }

    get estimatedDeliverTime() {
        return this._estimatedDeliverTime;
    }

    setEstimatedDeliverTime(estimatedDeliverTime: string) {
        this._estimatedDeliverTime = estimatedDeliverTime;
        return this;
    }

    get products() {
        return this._products;
    }

    setProducts(products: any[]) {
        this._products = products;
        return this;
    }

    get trackingId() {
        return this._trackingId;
    }

    setTrackingId(trackingId: string) {
        this._trackingId = trackingId;
        return this;
    }

    get sortingCode() {
        return this._sortingCode;
    }

    setSortingCode(sortingCode: string) {
        this._sortingCode = sortingCode;
        return this;
    }

    get isXfast() {
        return this._isXfast;
    }

    setIsXfast(isXfast: string) {
        this._isXfast = isXfast;
        return this;
    }

    get shippingStatus() {
        return this._shippingStatus;
    }

    setShippingStatus(shippingStatus: string) {
        this._shippingStatus = shippingStatus;
        return this;
    }

    get pickMoney() {
        return this._pickMoney;
    }

    setPickMoney(pickMoney: number) {
        this._pickMoney = pickMoney;
        return this;
    }

    get moneyOtherFee() {
        return this._moneyOtherFee;
    }

    setMoneyOrtherFee(moneyOtherFee: number) {
        this._moneyOtherFee = moneyOtherFee;
        return this;
    }

    get moneyFee() {
        return this._moneyFee;
    }

    setMoneyFee(moneyFee: number) {
        this._moneyFee = moneyFee;
        return this;
    }

    get moneyCollectionFee() {
        return this._moneyCollectionFee;
    }

    setMoneyCollectionFee(moneyCollectionFee: number) {
        this._moneyCollectionFee = moneyCollectionFee;
        return this;
    }

    get moneyTotalFee() {
        return this._moneyTotalFee;
    }

    setMoneyTotalFee(moneyTotalFee: number) {
        this._moneyTotalFee = moneyTotalFee;
        return this;
    }

    get moneyFeeVat() {
        return this._moneyFeeVat;
    }

    setMoneyFeeVat(moneyFeeVat: number) {
        this._moneyFeeVat = moneyFeeVat;
        return this;
    }

    get exchangeWeight() {
        return this._exchangeWeight;
    }

    setExchangeWeight(exchangeWeight: number) {
        this._exchangeWeight = exchangeWeight;
        return this;
    }

    get(): IShippingInfo {
        const request: IShippingInfo = {
            partner_id: this.partnerId,
            label: this.label,
            shipping_status: this.shippingStatus,

            status_id: this.statusId,

            fee: this.fee,
            insurance_fee: this.insuranceFee,

            area: this.area,
            estimated_pick_time: this.estimatedPickTime,
            estimated_deliver_time: this.estimatedPickTime,
            products: this.products,
            tracking_id: this.trackingId,
            sorting_code: this.sortingCode,
            is_xfast: this.isXfast,

            pick_money: this.pickMoney,
            money_other_fee: this.moneyOtherFee,
            money_fee: this.moneyFee,
            money_collection_fee: this.moneyCollectionFee,
            money_fee_vat: this.moneyFeeVat,
            money_total_fee: this.moneyTotalFee,
            exchange_weight: this.exchangeWeight,
        };

        return request;
    }
}
