import { ILogin } from '../interface';

export default class LoginBuilder {
    private _userName: string;
    private _password: string;

    get userName() {
        return this._userName;
    }

    setUserName(user_name: string) {
        this._userName = user_name;
        return this;
    }

    get password() {
        return this._password;
    }

    setPassword(password: string) {
        this._password = password;
        return this;
    }

    get(): ILogin {
        const request: ILogin = {
            USER_NAME: this.userName,
            PASSWORD: this.password,
        };

        return request;
    }
}
