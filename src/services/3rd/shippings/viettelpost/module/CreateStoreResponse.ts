import keys from "../../../../../config/env/keys";
import { NAME_QUERY_VTP } from "../definitions";

export default class CreateStoreResponse {
    private _groupaddressId: number;
    private _cusId: string;
    private _phone: string;
    private _name: string;
    private _address: string;
    private _wardId: number;
    private _provinceId: number;
    private _districtId: number;

    private _token: string;

    get groupaddressId() {
        return this._groupaddressId;
    }

    setGroupaddressId(groupaddressId: number) {
        this._groupaddressId = groupaddressId;
        return this;
    }

    get cusId() {
        return this._cusId;
    }

    setCusId(cusId: string) {
        this._cusId = cusId;
        return this;
    }
    get phone() {
        return this._phone;
    }

    setPhone(phone: string) {
        this._phone = phone;
        return this;
    }

    get name() {
        return this._name;
    }

    setName(name: string) {
        this._name = name;
        return this;
    }

    get address() {
        return this._address;
    }

    setAddress(address: string) {
        this._address = address;
        return this;
    }

    get wardId() {
        return this._wardId;
    }

    setWardId(wardId: number) {
        this._wardId = wardId;
        return this;
    }
    get districtId() {
        return this._districtId;
    }

    setDistrictId(districtId: number) {
        this._districtId = districtId;
        return this;
    }
    get provinceId() {
        return this._provinceId;
    }

    setProvinceId(provinceId: number) {
        this._provinceId = provinceId;
        return this;
    }
    
    get token() {
        return this._token;
    }

    setToken(token: string) {
        this._token = token;
        return this;
    }

    get(): any {
        const request: any = {
            groupaddress_id: this.groupaddressId,
            // cus_id: this.cusId,
            code: this.cusId,
            name_store: this.name,
            phone: this.phone,
            address: this.address,
            province_id: this.provinceId,
            district_id: this.districtId,
            wards_id: this.wardId,
            name_query: NAME_QUERY_VTP,
            token: this.token
        };

        return request;
    }

    getDefault(): any {
        const request: any = {
            code: this.cusId,
            name_query: NAME_QUERY_VTP,
            token: this.token
        };

        return request;
    }
}
