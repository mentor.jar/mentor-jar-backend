import { ICreateNewStore } from '../interface';

export default class CreateNewStoreBuilder {
    private _phone: string;
    private _name: string;
    private _address: string;
    private _wardId: number;

    get phone() {
        return this._phone;
    }

    setPhone(phone: string) {
        this._phone = phone;
        return this;
    }

    get name() {
        return this._name;
    }

    setName(name: string) {
        this._name = name;
        return this;
    }

    get address() {
        return this._address;
    }

    setAddress(address: string) {
        this._address = address;
        return this;
    }

    get wardId() {
        return this._wardId;
    }

    setWardId(wardId: number) {
        this._wardId = wardId;
        return this;
    }

    get(): ICreateNewStore {
        const request: ICreateNewStore = {
            PHONE: this.phone,
            NAME: this.name,
            ADDRESS: this.address,
            WARDS_ID: this.wardId,
        };

        return request;
    }
}

