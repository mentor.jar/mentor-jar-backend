export const NAME_QUERY_VTP = 'VIETTELPOST';

export enum VIETTEL_POST_HOST {
    HOST_DEV = 'https://partner.viettelpost.vn',
    HOST_PRD = 'https://partner.viettelpost.vn',
}

export enum VIETTEL_POST_PATH {
    LOGIN = '/v2/user/Login',
    CATEGORIES = '/v2/categories/listProvinceById',
    DISTRICTS = '/v2/categories/listDistrict',
    WARDS = '/v2/categories/listWards',
    CREATE_NEW_STORE = '/v2/user/registerInventory',
    LIST_STORE = '/v2/user/listInventory',
    GET_PRICE = '/v2/order/getPrice',
    GET_ALL_PRICE = '/v2/order/getPriceAll',
    CREATE_ORDER = '/v2/order/createOrder',
}
export enum ORDER_STATUS {
    CONFIRM_ORDER = 1,
    CONFIRM_RETURN_SHIPPING = 2,
    DELIVERY_AGAIN = 3,
    CANCEL_ORDER = 4,
    RE_ORDER = 5,
    DELETE_ORDER = 11,
}

export enum ORDER_PAYMENT {
    UNCOLLECT_MONEY = 1, // 1: Không thu tiền
    COLLECT_FEE_AND_PRICE = 2, // 2: Thu hộ tiền cước và tiền hàng
    COLLECT_FEE_PRICE = 3, // 3: Thu hộ tiền hàng
    COLLECT_FEE = 4, // 4: Thu hộ tiền cước
}
export enum PRODUCT_TYPE_SHIPPING {
    HH = 'HH', // Hàng hóa
    TH = 'TH', // Thư
}

export enum NATIONAL_TYPE_SHIPPING {
    INLAND = 1, // trong nước
    INTERNATIONAL = 0, // quốc tế
}
