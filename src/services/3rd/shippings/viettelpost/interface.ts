export interface ILogin {
    USER_NAME: string;
    PASSWORD: string;
}

export interface ILoginResponse {
    userId: number;
    token: string;
    partner: number;
    phone: string;
    expired: string | 0;
    encrypted: null | string;
    source: number;
    status?: number;
}

export interface IRegister {
    EMAIL: string;
    PHONE: string;
    NAME: string;
    ADDRESS: string;
    WARDS_ID: number;
}

export interface IRegisterResponse {
    status: number;
    userId: number;
    token: string;
    partner: number;
    phone: string;
    expired: string | 0;
    encrypted: null | string;
    source: number;
}

export interface IShippingInfo {
    partner_id: string;
    label: string;
    shipping_status: string;
    status_id: string;
    fee: number;
    insurance_fee: number;
    area: string;
    estimated_pick_time: string;
    estimated_deliver_time: string;
    products: any[];
    tracking_id: string;
    sorting_code: string;
    is_xfast: string;
    pick_money: number;
    money_other_fee: number;
    money_fee: number;
    money_collection_fee: number;
    money_fee_vat: number;
    money_total_fee: number;
    exchange_weight: number;
}

export interface IShippingHistory {
    partner_id: string;
    label_id: string;
    shipping_status: string;
    status_id: string;
    status_name: string;
    fee: number;
    fee_cod: number;
    pick_money: number;
    return_part_package: number;
    weight: number;
    reason: string;
    reason_code: string;
    reason_code_shipping: string;
    action_time: string;
    order_service: string;
    expected_delivery: string;
}

export interface ICreateNewStore {
    PHONE: string;
    NAME: string;
    ADDRESS: string;
    WARDS_ID: number;
}
