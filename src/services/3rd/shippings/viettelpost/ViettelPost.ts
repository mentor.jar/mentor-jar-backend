import axios from 'axios';
import keys from '../../../../config/env/keys';
import { BIDUStatusAction, ShippingStatus } from '../../../../models/enums/order';
import { OrderItemRepository } from '../../../../repositories/OrderItemRepo';
import { buildQuery } from '../../../../utils/stringUtil';
import {
    ORDER_PAYMENT,
    PRODUCT_TYPE_SHIPPING,
    VIETTEL_POST_HOST,
    VIETTEL_POST_PATH,
} from './definitions';
import AllPricingService from './module/AllPricingService';
import CreateNewStoreBuilder from './module/CreateNewStore';
import { CreateOrder } from './module/CreateOrder';
import CreateStoreResponse from './module/CreateStoreResponse';
import LoginBuilder from './module/Login';
import LoginResponseBuilder from './module/LoginResponse';
import GetPricingBuilder from './module/Pricing';
import PricingResponseBuilder from './module/PricingResponse';
import { PricingServiceResponse } from './module/PricingServiceResponse';
import ShippingHistoryBuilder from './module/ShippingHistory';
import ShippingInfoBuilder from './module/ShippingInfo';
import { viettelPostLogChannel } from '../../../../base/log';

const tokenAPI = keys.VIETTELPOST.token;
const emailRoot = keys.VIETTELPOST.email;
const passwordRoot = keys.VIETTELPOST.password;

const orderItemRepo = OrderItemRepository.getInstance();
class ViettelPostService {
    private static instance: ViettelPostService;

    static get _(): ViettelPostService {
        if (!this.instance) {
            this.instance = new ViettelPostService();
        }
        return this.instance;
    }

    getResponse(response) {
        this.viettelPostLogResponseInfo(response.data);
        if (!response.data) return Promise.reject(new Error('API error'));
        return response.data;
    }

    getWebhookHashCode = () => {
        const hash = 'biduecommerce';
        return hash;
    }

    /**
     *
     * @param email: email login
     * @param password: password login
     * @return LoginResponse
     */

    private async login({ email = emailRoot, password = passwordRoot }) {
        try {
            let url = `${VIETTEL_POST_HOST.HOST_PRD}${VIETTEL_POST_PATH.LOGIN}`;
            const loginBuilder = new LoginBuilder()
                .setUserName(email)
                .setPassword(password)
                .get();

            const config = {
                headers: {},
            };
            const response = await axios.post(url, loginBuilder, config);

            const {
                userId,
                token,
                partner,
                phone,
                expired,
                encrypted,
                source,
            } = this.getResponse(response.data);

            const loginResponse = new LoginResponseBuilder()
                .setUserId(userId)
                .setToken(token)
                .setPartner(partner)
                .setPhone(phone)
                .setExpired(expired)
                .setEncrypted(encrypted)
                .setSource(source)
                .get();

            return loginResponse;
        } catch (error) {
            this.viettelPostLogErrorInfo(error.message);
            throw new Error(error.message);
        }
    }

    private async connectClient() {}

    /**
     * Get all province
     * @return allProvince
     */

    private async allProvince() {
        try {
            let url = `${VIETTEL_POST_HOST.HOST_PRD}${
                VIETTEL_POST_PATH.CATEGORIES
            }${buildQuery({ provinceId: -1 })}`;

            const response = await axios.get(url);
            return this.getResponse(response.data);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    /**
     * Get all district by province id
     * @param provinceId
     * @return allProvince
     */

    private async allDistrictByProvince(provinceId) {
        try {
            let url = `${VIETTEL_POST_HOST.HOST_PRD}${
                VIETTEL_POST_PATH.DISTRICTS
            }${buildQuery({ provinceId: provinceId })}`;

            const response = await axios.get(url);
            return this.getResponse(response.data);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    /**
     * Get all ward by district id
     * @param districtId
     * @return allWards
     */

    private async allWardByDistrict(districtId) {
        try {
            let url = `${VIETTEL_POST_HOST.HOST_PRD}${
                VIETTEL_POST_PATH.WARDS
            }${buildQuery({ districtId: districtId })}`;

            const response = await axios.get(url);
            return this.getResponse(response.data);
        } catch (error) {
            throw new Error(error.message);
        }
    }

    /**
     * creat new store custommer
     * @param phone
     * @param nameStore
     * @param address
     * @param wardId
     * @param token
     * @return store info
     */

    async createNewStore({
        phone,
        nameStore,
        address,
        wardId,
        token = tokenAPI,
    }) {
        try {
            let url = `${VIETTEL_POST_HOST.HOST_PRD}${VIETTEL_POST_PATH.CREATE_NEW_STORE}`;
            const newStoreBuilder = new CreateNewStoreBuilder()
                .setAddress(address)
                .setPhone(phone)
                .setName(nameStore)
                .setWardId(wardId)
                .get();

            const config = {
                headers: {
                    Token: token,
                },
            };
            const response = await axios.post(url, newStoreBuilder, config);
            const result = this.getResponse(response.data);

            const newStore = result[0];
            const storeResponse = new CreateStoreResponse()
                .setAddress(newStore.address)
                .setCusId(newStore.cusId)
                .setGroupaddressId(newStore.groupaddressId)
                .setPhone(newStore.phone)
                .setDistrictId(newStore.districtId)
                .setProvinceId(newStore.provinceId)
                .setWardId(newStore.wardsId)
                .setToken(tokenAPI)
                .setName(newStore.name)
                .get();

            return storeResponse;
        } catch (error) {
            this.viettelPostLogErrorInfo(error.message);
            throw new Error(error.message);
        }
    }

    async setStoreDefault({
        token,
    }) {
        try {
            const storeResponse = new CreateStoreResponse()
                .setCusId('bidu-viettelpost') // default = 'bidu-viettelpost'
                .setGroupaddressId(0) // default = 0
                .setToken(token)
                .getDefault();

            return storeResponse;
        } catch (error) {
            this.viettelPostLogErrorInfo(error.message);
            throw new Error(error.message);
        }
    }

    async getAllStore({ token = tokenAPI }) {
        try {
            let url = `${VIETTEL_POST_HOST.HOST_PRD}${VIETTEL_POST_PATH.LIST_STORE}`;

            const config = {
                headers: {
                    Token: token,
                },
            };
            const response = await axios.get(url, config);
            return this.getResponse(response.data);
        } catch (error) {
            this.viettelPostLogErrorInfo(error.message);
            throw new Error(error.message);
        }
    }

    /**
     * creat new store custommer
     * @param token
     * @param productWeight
     * @param productPrice
     * @param moneyCollection
     * @param token
     * @param orderServiceAdd,
     * @param orderService,
     * @param senderProvince,
     * @param senderDistrict,
     * @param recieverProvince,
     * @param recieverDistrict,
     * @param productType,
     * @param nationalType,
     * @return store info
     */

    async pricing({
        token = tokenAPI,
        productWeight,
        productPrice,
        moneyCollection,
        orderServiceAdd,
        orderService,
        senderProvince,
        senderDistrict,
        receiverProvince,
        receiverDistrict,
        productType,
        nationalType = 1, // =1 : Trong nước/ inland ||  =0 : Quốc tế/ international
    }) {
        try {
            let url = `${VIETTEL_POST_HOST.HOST_PRD}${VIETTEL_POST_PATH.GET_PRICE}`;

            const config = {
                headers: {
                    Token: token,
                },
            };

            const pricingDto = new GetPricingBuilder()
                .setProductWeight(productWeight)
                .setProductPrice(productPrice)
                .setMoneyCollection(moneyCollection)

                .setSenderProvince(senderProvince)
                .setSenderDistrict(senderDistrict)

                .setReceiverProvince(receiverProvince)
                .setReceiverDistrict(receiverDistrict)
                .setProductType(productType || 'HH')
                .setNationalType(nationalType)
                .setOrderServiceAdd(orderServiceAdd)
                .setOrderService(orderService)
                .get();

            const response = await axios.post(url, pricingDto, config);
            const { MONEY_TOTAL } = this.getResponse(response.data);

            const fee = new PricingResponseBuilder()
                .setMoneyTotal(MONEY_TOTAL)
                .setOrderService(orderService)
                .get();

            return fee;
        } catch (error) {
            this.viettelPostLogErrorInfo(error.message);
            throw new Error(error.message);
        }
    }

    async allPricing({
        token = tokenAPI,
        productWeight,
        productPrice,
        moneyCollection,
        productType = 'HH',
        senderProvince,
        senderDistrict,
        recieverProvince,
        recieverDistrict,
        type = 1,
    }) {
        try {
            let url = `${VIETTEL_POST_HOST.HOST_PRD}${VIETTEL_POST_PATH.GET_ALL_PRICE}`;

            const config = {
                headers: {
                    Token: token,
                },
            };

            const pricingRequest = new AllPricingService()
                .setProductWeight(productWeight)
                .setProductPrice(productPrice)
                .setMoneyCollection(moneyCollection)

                .setSenderProvince(senderProvince)
                .setSenderDistrict(senderDistrict)

                .setReceiverProvince(recieverProvince)
                .setReceiverDistrict(recieverDistrict)
                .setProductType(productType || 'HH')
                .setType(type) // =1 : Trong nước/ inland ||  =0 : Quốc tế/ international
                .get();

            const response = await axios.post(url, pricingRequest, config);
            return response.data;
        } catch (error) {
            this.viettelPostLogErrorInfo(error.message);
            throw new Error(error.message);
        }
    }

    async bestOfServiceWithPricing({
        token = tokenAPI,
        productWeight,
        productPrice,
        moneyCollection,
        productType = 'HH',
        senderProvince,
        senderDistrict,
        recieverProvince,
        recieverDistrict,
        type = 1,
    }) {
        try {
            const allPricing = await this.allPricing({
                token,
                productWeight,
                productPrice,
                moneyCollection,
                productType,
                senderProvince,
                senderDistrict,
                recieverProvince,
                recieverDistrict,
                type: type,
            });
            if (!allPricing.length) {
                return new PricingServiceResponse()
                    .setServiceCode('bidu_default')
                    .setServiceFee(50000)
                    .get();
            }

            const minPricing = allPricing.reduce((prev, current) =>
                prev.GIA_CUOC > current.GIA_CUOC ? current : prev
            );

            return new PricingServiceResponse()
                .setServiceCode(minPricing.MA_DV_CHINH)
                .setServiceName(minPricing.TEN_DICHVU)
                .setServiceFee(minPricing.GIA_CUOC)
                .setServiceTime(minPricing.THOI_GIAN)
                .setExchangeWeight(minPricing.EXCHANGE_WEIGHT)
                .get();
        } catch (error) {
            this.viettelPostLogErrorInfo(error.message);
            throw new Error(error.message);
        }
    }

    async prepareOrder ({
        orderShop,
        addressShop
    }) {
        const orderItems = await orderItemRepo.findOrderItemVariantByOrder(orderShop._id);
        const orderDto = new CreateOrder()
        .setOrderNumber(orderShop.order_number)
        .setGroupAddressId(0) // only set groupaddress = 0
        .setCusId(0) // only set cusId = 0
        .setDeliveryDate('') 
        .setSenderFullName(addressShop.name)
        .setSenderAddress(addressShop.street)
        .setSenderPhone(addressShop.phone)
        .setSenderEmail('')
        .setSenderWard(addressShop.ward.id_vtp)
        .setSenderDistrict(addressShop.district.id_vtp)
        .setSenderProvince(addressShop.state.id_vtp)
        .setSenderLatitude(0)
        .setSenderLongtitude(0)
        .setReceiverFullName(orderShop.address.name)
        .setReceiverAddress(orderShop.address.street)
        .setReceiverPhone(orderShop.address.phone)
        .setReceiverEmail('')
        .setReceiverWard(orderShop.address.ward.id_vtp)
        .setReceiverDistrict(orderShop.address.district.id_vtp)
        .setReceiverProvince(orderShop.address.state.id_vtp)
        .setReceiverLatitude(0)
        .setReceiverLongtitude(0)
        .setProductName(orderItems[0].product.name)
        .setProductDescription('')
        .setProductQuantity(orderItems.map(item => item.quantity).reduce((prev, next) => prev + next, 0))
        .setProductPrice(orderShop.total_value_items)
        .setProductWeight(orderItems.map(item => item.product.weight*item.quantity).reduce((prev, next) => prev + next, 0))
        .setProductLength(0)
        .setProductWidth(0)
        .setProductHeight(0)
        .setProductType(PRODUCT_TYPE_SHIPPING.HH)
        .setOrderPayment(ORDER_PAYMENT.COLLECT_FEE_PRICE)
        .setOrderService(orderShop.order_service)
        .setOrderServiceAdd('')
        .setOrderVoucher('')
        .setOrderNote(orderShop.note)
        .setMoneyCollection(orderShop.total_price)
        .setMoneyTotalFee(0)
        .setMoneyFeeCod(0)
        .setMoneyFeeVas(0)
        .setMoneyFeeInsurrance(0)
        .setMoneyFee(0)
        .setMoneyFeeOther(0)
        .setMoneyTotalVAT(0)
        .setMoneyTotal(0)
        .setListItem(orderItems)
        .get()
        return orderDto;
    }

    async createBill({
        orderShop,
        addressShop
    }) {
        try {
            let url = `${VIETTEL_POST_HOST.HOST_PRD}${VIETTEL_POST_PATH.CREATE_ORDER}`;

            const config = {
                headers: {
                    Token: tokenAPI,
                },
            };

            const data = await this.prepareOrder({
                orderShop,
                addressShop
            })

            const response = await axios.post(url, data, config);

            const result = this.getResponse(response.data);
            const shippingInfoDto = new ShippingInfoBuilder()
                .setPartnerId(orderShop.order_number)
                .setLabel(result.ORDER_NUMBER)
                .setStatusId('1') // set default = Chưa tiếp nhận
                .setArea(null) //
                .setFee(result.MONEY_TOTAL)
                .setInsuranceFee(null)
                .setEstimatedPickTime(null)
                .setEstimatedDeliverTime(null)
                .setProducts([])
                .setTrackingId(null)
                .setSortingCode(null)
                .setIsXfast(null)
                .setShippingStatus('Chưa tiếp nhận')
                .setPickMoney(result.MONEY_COLLECTION)
                .setMoneyOrtherFee(result.MONEY_FEEOTHER)
                .setMoneyFee(result.MONEY_FEE)
                .setMoneyCollectionFee(result.MONEY_COLLECTION_FEE)
                .setMoneyTotalFee(result.MONEY_TOTAL_FEE)
                .setMoneyFeeVat(result.MONEY_VAT)
                .setExchangeWeight(result.EXCHANGE_WEIGHT)
                .get();

            return shippingInfoDto;
        } catch (error) {
            this.viettelPostLogErrorInfo(error.message);
            throw new Error(error.message);
        }
    }

    async updateStatusOrder({ token = tokenAPI, type, orderNumber, note }) {
        try {
            let url = `${VIETTEL_POST_HOST.HOST_PRD}${VIETTEL_POST_PATH.GET_ALL_PRICE}`;

            const config = {
                headers: {
                    Token: token,
                },
            };

            const data = {
                TYPE: 4,
                ORDER_NUMBER: '11506020148',
                NOTE: 'Ghi chú',
            };
            const response = await axios.post(url, data, config);
            return response.data;
        } catch (error) {
            this.viettelPostLogErrorInfo(error.message);
            throw new Error(error.message);
        }
    }

    /**
     * get shipping history from webhook viettel post
     * @param data data from API ViettelPost webhook
     * @return shipping history info
     */

    async webhookToShippingHistory(data) {
        try {
            const { DATA, TOKEN } = data;

            const orderStatusId = DATA.ORDER_STATUS;
            const orderStatus = this.webhookStatusToOrderStatus(orderStatusId.toString());

            const shippingHistory = new ShippingHistoryBuilder()
                .setLabelId(DATA.ORDER_NUMBER)
                .setPartnerId(DATA.ORDER_REFERENCE)
                .setStatusId(orderStatus.status_id)
                .setShippingStatus(orderStatus.message) // ORDER_STATUS -> to statsus_shipping
                .setStatusName(DATA?.STATUS_NAME)
                .setFee(DATA.MONEY_TOTAL)
                .setPickMoney(DATA.MONEY_COLLECTION)
                .setPickMoney(DATA.MONEY_COLLECTION)
                .setFeeCod(DATA.MONEY_FEECOD)
                .setReturnPartPackage(null)
                .setWeight(DATA.PRODUCT_WEIGHT)
                .setReason(DATA.NOTE)
                .setReasonCode(null)
                .setReasonCodeShipping(null)
                .setActionTime(DATA.ORDER_STATUSDATE)
                .setOrderService(DATA.ORDER_SERVICE)
                .setExpectedDelivery(DATA.EXPECTED_DELIVERY)
                .get();

            return {
                history: shippingHistory,
                order_status: orderStatus,
            };
        } catch (error) {
            this.viettelPostLogErrorInfo(error.message);
            throw new Error(error.message);
        }
    }

    webhookStatusToOrderStatus(code) {
        const result: any = {
            message: null,
            status: null,
            shipping_status: null,
            status_id: null
        };

        switch (code) {
            case '-100':
                result.message = `Đơn hàng mới tạo, chưa duyệt`;
                result.status = `Not approved`;
                result.shipping_status = ShippingStatus.WAIT_TO_PICK;
                result.status_id = BIDUStatusAction.NOT_RECEIVED;
                break;
            case '-108':
                result.message = `Đơn hàng gửi tại bưu cục`;
                result.status = `Approved`;
                result.shipping_status = ShippingStatus.WAIT_TO_PICK;
                result.status_id = BIDUStatusAction.ORDER_SENT_AT_POSTOFFICE;
                break;
            case '-109':
                result.message = `Đơn hàng đã gửi tại điểm thu gom`;
                result.status = `Sent at convenience store`;
                result.shipping_status = ShippingStatus.SHIPPING;
                break;
            case '-110':
                result.message = `Đơn hàng đang bàn giao qua bưu cục`;
                result.status = `Sent at convenience store`;
                result.shipping_status = ShippingStatus.SHIPPING;
                break;
            case '100':
                result.message = `Tiếp nhận đơn hàng từ đối tác "Viettelpost xử lý đơn hàng"`;
                result.status = `Approved`;
                result.shipping_status = ShippingStatus.WAIT_TO_PICK;
                break;
            case '101':
                result.message = `ViettelPost yêu cầu hủy đơn hàng`;
                result.shipping_status = ShippingStatus.CANCELED;
                break;
            case '102':
                result.message = `Đơn hàng chờ xử lý`;
                result.status = `Approved`;
                result.shipping_status = ShippingStatus.WAIT_TO_PICK;
                result.status_id = BIDUStatusAction.ON_PROCESSING;
                break;
            case '103':
                result.message = `Giao cho bưu cục "Viettelpost xử lý đơn hàng"`;
                result.status = `Approved`;
                result.shipping_status = ShippingStatus.WAIT_TO_PICK;
                result.status_id = BIDUStatusAction.ON_SHIPMENT_PROCESSING;
                break;
            case '104':
                result.message = `Giao cho Bưu tá đi nhận`;
                result.status = `Approved`;
                result.shipping_status = ShippingStatus.WAIT_TO_PICK;
                result.status_id = BIDUStatusAction.DELIVER_BY_RECEIVER_POSTMAN;
                break;
            case '105':
                result.message = `Bưu Tá đã nhận hàng`;
                result.status = `Has taken the goods`;
                result.shipping_status = ShippingStatus.SHIPPING;
                result.status_id = BIDUStatusAction.RECEIVED;
                break;
            case '106':
                result.message = `Đối tác yêu cầu lấy lại hàng`;
                break;
            case '107':
                result.message = `Đối tác yêu cầu hủy qua API`;
                result.status = `Canceled`;
                result.shipping_status = ShippingStatus.SHIPPING;
                result.status_id = BIDUStatusAction.CANCELED;
                break;
            case '200':
                result.message = `Nhận từ bưu tá - Bưu cục gốc`;
                result.status = `Being transported`;
                result.shipping_status = ShippingStatus.WAIT_TO_PICK;
                result.status_id = BIDUStatusAction.ASSIGN;
                break;
            case '201':
                result.message = `Hủy nhập phiếu gửi`;
                result.status = `Canceled`;
                result.shipping_status = ShippingStatus.CANCELED;
                result.status_id = BIDUStatusAction.CANCELED;
                break;
            case '202':
                result.message = `Sửa phiếu gửi`;
                result.status = `Being transported`;
                result.shipping_status = ShippingStatus.WAIT_TO_PICK;
                break;
            case '300':
                result.message = `Close delivery file`;
                result.status = `Being transported`;
                result.shipping_status = ShippingStatus.WAIT_TO_PICK;
                break;
            case '301':
                result.message = `Ðóng túi gói "Vận chuyển đi từ"`;
                break;
            case '302':
                result.message = `Đóng chuyến thư "Vận chuyển đi từ"`;
                break;
            case '303':
                result.message = `Đóng tuyến xe "Vận chuyển đi từ"`;
                break;
            case '320':
                result.message = ``;
                result.status = `Being transported`;
                result.shipping_status = ShippingStatus.WAIT_TO_PICK;
                break;
            case '400':
                result.message = `Nhận bảng kê đến "Nhận tại"`;
                result.status = `Being transported`;
                result.shipping_status = ShippingStatus.WAIT_TO_PICK;
                result.status_id = BIDUStatusAction.PICKED_UP;
                break;
            case '401':
                result.message = `Nhận Túi gói "Nhận tại"`;
                break;
            case '402':
                result.message = `Nhận chuyến thư "Nhận tại"`;
                break;
            case '403':
                result.message = `Nhận chuyến xe "Nhận tại"`;
                break;
            case '500':
                result.message = `Giao bưu tá đi phát`;
                result.status = `On delivery`;
                result.shipping_status = ShippingStatus.SHIPPING;
                result.status_id = BIDUStatusAction.DELIVERING;
                break;
            case '501':
                result.message = `Thành công - Phát thành công`;
                result.status = `Successful delivery`;
                result.shipping_status = ShippingStatus.SHIPPED;
                result.status_id = BIDUStatusAction.DELIVERIED;
                break;
            case '502':
                result.message = `Chuyển hoàn bưu cục gốc`;
                result.status = `Approval to return`;
                result.shipping_status = ShippingStatus.SHIPPING;
                result.status_id = BIDUStatusAction.RETURN_TO_SHOP;
                break;
            case '503':
                result.message = `Hủy - Theo yêu cầu khách hàng`;
                result.status = `Successful delivery destroyed`;
                result.shipping_status = ShippingStatus.CANCELED;
                break;
            case '504':
                result.message = `Thành công - Chuyển trả người gửi`;
                result.status = `Successful return`;
                result.shipping_status = ShippingStatus.CANCELED; // returned
                result.status_id = BIDUStatusAction.RETURNED_TO_SHOP;
                break;
            case '505':
                result.message = `Tồn - Thông báo chuyển hoàn bưu cục gốc`;
                result.status = `Wait for approval to return`;
                result.shipping_status = ShippingStatus.SHIPPING;
                result.status_id = BIDUStatusAction.CHECKED_AND_RETURNED;
                break;
            case '506':
                result.message = `Tồn - Khách hàng nghỉ, không có nhà`;
                result.status = `On delivery`;
                result.shipping_status = ShippingStatus.SHIPPING;
                result.status_id = BIDUStatusAction.DELAY_DELIVER;
                break;
            case '507':
                result.message = `Tồn - Khách hàng đến bưu cục nhận`;
                result.status = `Delivery failed`;
                result.shipping_status = ShippingStatus.SHIPPING;
                result.status_id = BIDUStatusAction.UNABLE_TO_DELIVER;
                break;
            case '508':
                result.message = `Phát tiếp`;
                result.status = `On delivery`;
                result.shipping_status = ShippingStatus.SHIPPING;
                result.status_id = BIDUStatusAction.ON_DELIVERING;
                break;
            case '509':
                result.message = `Chuyển tiếp bưu cục khác`;
                result.status = `On delivery`;
                result.shipping_status = ShippingStatus.SHIPPING;
                result.status_id = BIDUStatusAction.ON_DELIVERING_OTHER_POST;
                break;
            case '510':
                result.message = `Hủy phân công phát`;
                break;
            case '515':
                result.message = `Bưu cục phát duyệt hoàn`;
                result.status = `Approval to return`;
                result.shipping_status = ShippingStatus.SHIPPING;
                break;
            case '550':
                result.message = `Đơn Vị Yêu Cầu Phát Tiếp`;
                result.status = `On delivery`;
                result.shipping_status = ShippingStatus.SHIPPING;
                result.status_id = BIDUStatusAction.RE_SENT;
                break;
            default:
                break;
        }

        return result;
    }

    viettelPostLogErrorInfo = (error) => {
        viettelPostLogChannel.info("-----*----")
        viettelPostLogChannel.info("Error: ", error)
        viettelPostLogChannel.info("-----*----")
    }
    viettelPostLogResponseInfo = (response) => {
        viettelPostLogChannel.info("-----*----")
        viettelPostLogChannel.info("Response: ", response)
        viettelPostLogChannel.info("-----*----")
    }
}

export default ViettelPostService;
