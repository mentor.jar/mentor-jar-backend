import axios, { AxiosResponse } from 'axios';
import queryString from 'querystring'
import { ShippingError } from '../../../base/customError';
import keys from '../../../config/env/keys';
import { PaymentMethodQuery, ShippingMethodQuery } from '../../../models/enums/order';
import { IDeliveryAddressGHTKRepo, IOrderGHTKRepo, IOtherInfoGHTKRepo, IPickAddresGHTKRepo, IProductGHTKRepo } from '../../../repositories/interfaces/IOrderRepo';
import { OrderItemRepository } from '../../../repositories/OrderItemRepo';
import { PaymentMethodRepo } from '../../../repositories/PaymentMethodRepo';
import { ShopRepo } from '../../../repositories/ShopRepo';
import { GHTKAccount, GHTK_CODE } from './definitions';
import { randomNumber } from '../../../utils/utils'
import { ghtkLogChannel } from '../../../base/log';
import { OrderRepository } from '../../../repositories/OrderRepo';

const paymentMethodRepo = PaymentMethodRepo.getInstance();
const orderRepo = OrderRepository.getInstance();
const shopRepo = ShopRepo.getInstance();
class GHTKService {
    
    private static instance: GHTKService;

    static getInstance(): GHTKService {
        if (!this.instance) {
            this.instance = new GHTKService();
        }
        return this.instance;
    }

    getPickAddressOfShop = async (token: any) => {
        try {
            let url = `${keys.GHTK.ghtk_url}/services/shipment/list_pick_add`;
            let response = await axios.get(url, { headers: { Token: token } })

            return response.data.data
        } catch (error) {
            throw new ShippingError(error.message)
        }
    }

    createRequestGHTK = async (stringParams, token) => {
        let url = `${keys.GHTK.ghtk_url}/services/shipment/fee?${stringParams}`
        token = token ? token : keys.GHTK.GHTKb2cToken;
        return new Promise(async (resolve, reject) => {
            try {
                let response = await axios.get(url, { headers: { Token: token } });
                this.ghtkLogResponseInfo(response?.data);
                if (response.data.success) return resolve(response.data);
                return resolve(false);
            } catch (error) {
                this.ghtkLogErrorInfo(new ShippingError(error.message));
                return resolve(false);
                // throw new ShippingError(error.message)
            }
        })
    }

    getShippingFeeFromAddressCheckoutPage = async (params: any, token: any) => {
        const stringParams = queryString.stringify(params);
        let data: any = await this.createRequestGHTK(stringParams, token);
        if (data) return data.fee;
        //fake data
        return this.fakeShippingFee();
    }

    createAccountB2CForShop = async (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                let url = `${keys.GHTK.ghtk_url}/services/shops/add `;
                 
                const data  = {
                    "name": params.name,
                    "first_address": params.first_address,
                    "province": params.province,
                    "district": params.district,
                    "tel": params.tel,
                    "email": params.email
                };

                const config = {
                    headers: {
                        'Content-Type': 'application/json',
                        'Content-length': 351,
                        'Token': keys.GHTKb2cToken,
                    }
                };

                const response = await axios.post(url, data, config);
                this.ghtkLogResponseInfo(response.data);
                if (response.data.success) return resolve(response.data.data)
                return reject(new ShippingError(response.data.message))
            } catch (error) {
                this.ghtkLogErrorInfo(new ShippingError(error.message));
                reject(new ShippingError(error.message))
            }
        })
    }

    setAccountForShop = (): GHTKAccount => {
        let account: GHTKAccount = {
            code: GHTK_CODE.CODE,
            token: keys.GHTK.GHTKb2cToken
        }; 
        return account;
    }

    getStatusTextOrder = (statusId) => {
        if(!statusId) return false;

        let statusText;
        switch (parseInt(statusId)) {
            case -1:
                statusText = 'Huỷ đơn hàng';
                break;
            case 1:
                statusText = 'Chưa tiếp nhận';
                break;
            case 2:
                statusText = 'Đã tiếp nhận';
                break;
            case 3:
                statusText = 'Đã lấy hàng/Đã nhập kho';
                break;
            case 4:
                statusText = 'Đã điều phối giao hàng/Đang giao hàng';
                break;
            case 5:
                statusText = 'Đã giao hàng/Chưa đối soát';
                break;
            case 6:
                statusText = 'Đã đối soát';
                break;
            case 7:
                statusText = 'Không lấy được hàng';
                break;
            case 8:
                statusText = 'Hoãn lấy hàng';
                break;
            case 9:
                statusText = 'Không giao được hàng';
                break;
            case 10:
                statusText = 'Delay giao hàng';
                break;
            case 11:
                statusText = 'Đã đối soát công nợ trả hàng';
                break;
            case 12:
                statusText = 'Đã điều phối lấy hàng/Đang lấy hàng';
                break;
            case 13:
                statusText = 'Đơn hàng bồi hoàn';
                break;
            case 20:
                statusText = 'Đang trả hàng (COD cầm hàng đi trả)';
                break;
            case 21:
                statusText = 'Đã trả hàng (COD đã trả xong hàng)';
                break;
            case 123:
                statusText = 'Shipper báo đã lấy hàng';
                break;
            case 127:
                statusText = 'Shipper (nhân viên lấy/giao hàng) báo không lấy được hàng';
                break;
            case 128:
                statusText = 'Shipper báo delay lấy hàng';
                break;
            case 45:
                statusText = 'Shipper báo đã giao hàng';
                break;
            case 49:
                statusText = 'Shipper báo không giao được giao hàng';
                break;
            case 410:
                statusText = 'Shipper báo delay giao hàng';
                break;
            default :
                statusText = 'Không xác định';
                break;
        }

        return statusText;
    }

    getReasonTextOrder = (reseonCode) => {
        let reasonText = '';

        if(reseonCode == '') return reasonText;
        switch (parseInt(reseonCode)) {
            case 100:
                reasonText = 'Nhà cung cấp (NCC) hẹn lấy vào ca tiếp theo';
                break;
            case 101:
                reasonText = 'GHTK không liên lạc được với NCC';
                break;
            case 102:
                reasonText = 'NCC chưa có hàng';
                break;
            case 103:
                reasonText = 'NCC đổi địa chỉ';
                break;
            case 104:
                reasonText = 'NCC hẹn ngày lấy hàng';
                break;
            case 105:
                reasonText = 'GHTK quá tải, không lấy kịp';
                break;
            case 106:
                reasonText = 'Do điều kiện thời tiết, khách quan';
                break;
            case 110:
                reasonText = 'Địa chỉ ngoài vùng phục vụ';
                break;
            case 111:
                reasonText = 'Hàng không nhận vận chuyển';
                break;
            case 112:
                reasonText = 'NCC báo hủy';
                break;
            case 113:
                reasonText = '	NCC hoãn/không liên lạc được 3 lần';
                break;
            case 114:
                reasonText = 'Lý do khác';
                break;
            case 107:
                reasonText = 'Đối tác hủy đơn qua API';
                break;
            case 120:
                reasonText = 'GHTK quá tải, giao không kịp';
                break;
            case 121:
                reasonText = 'Người nhận hàng hẹn giao ca tiếp theo';
                break;
            case 122:
                reasonText = 'Không gọi được cho người nhận hàng';
                break;
            case 123:
                reasonText = 'Người nhận hàng hẹn ngày giao';
                break;
            case 124:
                reasonText = 'Người nhận hàng chuyển địa chỉ nhận mới';
                break;
            case 125:
                reasonText = '	Địa chỉ người nhận sai, cần NCC check lại';
                break;
            case 126:
                reasonText = 'Do điều kiện thời tiết, khách quan';
                break;
            case 127:
                reasonText = 'Lý do khác';
                break;
            case 128:
                reasonText = 'Đối tác hẹn thời gian giao hàng';
                break;
            case 129:
                reasonText = 'Không tìm thấy hàng';
                break;
            case 1200:
                reasonText = 'SĐT người nhận sai, cần NCC check lại';
                break;
            case 130:
                reasonText = 'Người nhận không đồng ý nhận sản phẩm';
                break;
            case 131:
                reasonText = 'Không liên lạc được với KH 3 lần';
                break;
            case 132:
                reasonText = 'KH hẹn giao lại quá 3 lần';
                break;
            case 133:
                reasonText = 'Shop báo hủy đơn hàng';
                break;
            case 134:
                reasonText = 'Lý do khác';
                break;
            case 135:
                reasonText = 'Đối tác hủy đơn qua API';
                break;
            case 140:
                reasonText = 'NCC hẹn trả ca sau';
                break;
            case 141:
                reasonText = 'Không liên lạc được với NCC';
                break;
            case 142:
                reasonText = 'NCC không có nhà';
                break;
            case 143:
                reasonText = 'NCC hẹn ngày trả';
                break;
            case 144:
                reasonText = 'Lý do khác';
                break;
            
            default :
            reasonText = 'Không xác định';
                break;
        }
        return reasonText;
    }

    getWebhookHashCode = () => {
        const hash = 'biduecommerce';
        return hash;
    }
    getTokenGHTKByShop = async (shop) => {
        return new Promise(async (resolve, reject) => {
            try {
                const shopRepo = ShopRepo.getInstance();
                let token = await shopRepo.getTokenByShop(shop.shipping_methods, ShippingMethodQuery.GIAOHANGTIETKIEM);
                if (!token) return reject(new ShippingError('Lỗi thiếu token. Shop chưa tạo tài khoản shop với GHTK'));
                return resolve(token);
            } catch (error) {
                reject(error)
            }
        })
        
    }

    prepareOrderGHTK = async (orderShop, addressShop) => {
        const arrayPayment = [ PaymentMethodQuery.VNPAY, PaymentMethodQuery.MOMO ]
        const orderItemRepo = await OrderItemRepository.getInstance();
        const productOder = await orderItemRepo.find({ order_id: orderShop._id });
        if(!productOder.length) throw new Error("Không tìm thấy sản phẩm trong đơn hàng.")

        // const filterPayment = await paymentMethodRepo.checkPaymentMethod(orderShop.payment_method_id);

        let products: any = [];
        let product: IProductGHTKRepo = <IProductGHTKRepo>{};
        let objOrder: IOrderGHTKRepo = <IOrderGHTKRepo>{};;
        let pickAddress: IPickAddresGHTKRepo = <IPickAddresGHTKRepo>{};
        let deliveryAddress: IDeliveryAddressGHTKRepo = <IDeliveryAddressGHTKRepo>{};
        let otherInfo: IOtherInfoGHTKRepo = <IOtherInfoGHTKRepo>{};
        
        objOrder.id = orderShop.order_number;

        // let pick_money = arrayPayment.includes(filterPayment.name_query) ? 0 : orderShop.total_price
        let pick_money = orderShop.total_price // set pick money = total_price;

        pickAddress.pick_name = addressShop.name;
        pickAddress.pick_address = addressShop.street;
        pickAddress.pick_province = addressShop.state.name;
        pickAddress.pick_district = addressShop.district.name;
        pickAddress.pick_ward = addressShop.ward.name;
        pickAddress.pick_tel = addressShop.phone;
        pickAddress.pick_money = pick_money; // if VNPAY, MOMO: pick_money = 0; else orderShop.total_price

        deliveryAddress.tel = orderShop.address.phone;
        deliveryAddress.name = orderShop.address.name;
        deliveryAddress.address = orderShop.address.street;
        deliveryAddress.province = orderShop.address.state.name;
        deliveryAddress.ward = orderShop.address.ward.name;
        deliveryAddress.district = orderShop.address.district.name;
        deliveryAddress.note = orderShop.note ? orderShop.note : '';
        deliveryAddress.hamlet = 'Khác';

        otherInfo.value = orderShop.total_value_items;
        otherInfo.is_freeship = 1; //Integer - Freeship cho người nhận hàng. Nếu bằng 1 COD sẽ chỉ thu người nhận hàng số tiền bằng pick_money, nếu bằng 0 COD sẽ thu tiền người nhận số tiền bằng pick_money + phí ship của đơn hàng, giá trị mặc định bằng 0
        otherInfo.transport = 'road'; // update transport default = road

        products = productOder.map((item) => {
            product.name = item.product.name;
            product.quantity = item.quantity;
            product.weight = parseFloat((item.product.weight/1000).toFixed(2));

            return product;
        })
        const result = { order: {...objOrder, ...pickAddress,...deliveryAddress,...otherInfo}, products}
        this.ghtkLogResponseInfo(result);
        return result;
    }

    createOrder = (orderShop, token, addressShop) =>{
        return new Promise(async (resolve, reject) => {
            try {

                const params = await this.prepareOrderGHTK(orderShop, addressShop);
                let url = `${keys.GHTK.ghtk_url}/services/shipment/order/?ver=1.5`;

                const data  = {
                    products: params.products,
                    order: params.order,
                };

                const config = {
                    headers: {
                        'Content-Type': 'application/json',
                        'Token': token,
                    }
                };

                const response = await axios.post(url, data, config);
                this.ghtkLogResponseInfo(response?.data) //logs
                if (response.data.success) return resolve(response.data.order);
                return reject(new ShippingError(response.data?.message));
            } catch (error) {
                this.ghtkLogErrorInfo(new ShippingError(error.message));
                reject(new ShippingError(error.message));
            }
        })
    }

    setReasonCancel = (order) => {
        let reason = order.reason_code_shipping ? order.reason_code_shipping : order.reason
        const result = !reason ? order.shipping_status : order.shipping_status + ' vì: ' + reason
       
        return result;
    }

    fakeDataCreateOrder = (order) => {
        let dateFake = new Date()
        let orderFake = {
            "partner_id": order.order_number,
            "label": this.randomLabel(),
            "area": "1",
            "fee": "30400",
            "insurance_fee": "15000",
            "estimated_pick_time": dateFake.toUTCString(),
            "estimated_deliver_time": dateFake.toUTCString(),
            "products": [],
            "status_id": 1
        }

        let dataFake = {
            success: true,
            order: orderFake,
        }
        
        let responseFake = {
            data: dataFake
        }
        return responseFake
    }
    randomLabel = () => {
        // Example: S19485292.FA.MB20.C2.1623548745
        let result = `S${randomNumber(8)}.FA.MB${randomNumber(2)}.C2.${randomNumber(10)}`;
        return result;
    }

    fakeShippingFee = () => {
        return { fee: 32500 };
    }

    setUrlCancelOrderWithOrderNumber = (orderNumber) => {
        return `${keys.GHTK.ghtk_url}/services/shipment/cancel/${orderNumber}`;
    }

    cancelOrderGHTK = async (orderNumber) => {
        return new Promise(async (resolve, reject) => {
            try {
                let url = this.setUrlCancelOrderWithOrderNumber(orderNumber);
                let token = await this.getTokenGHTKByOrder(orderNumber);
                if (!token) return resolve(false);

                let response = await axios.post(url, null, { headers: { Token: token } });
                this.ghtkLogResponseInfo(response?.data);
                return (response.data.success) ? resolve(true) : resolve(false);
            } catch (error) {
                this.ghtkLogErrorInfo(error.message);
                resolve(false);
            }
        })
    }

    getTokenGHTKByOrder = async (orderNumber) => {
        return new Promise(async (resolve, reject) => {
            try {
                const order = await orderRepo.findOrderByOrderNumber({ order_number: orderNumber });
                const shop = await shopRepo.findById(order.shop_id)
                const token = await this.getTokenGHTKByShop(shop);
                if (!token) return resolve(null);
                return resolve(token);
            } catch (error) {
                resolve(false)
            }
        })
    }

    ghtkLogErrorInfo = (error) => {
        ghtkLogChannel.info("-----*----")
        ghtkLogChannel.info("Error: ", error)
        ghtkLogChannel.info("-----*----")
    }
    ghtkLogResponseInfo = (response) => {
        ghtkLogChannel.info("-----*----")
        ghtkLogChannel.info("Response: ", response)
        ghtkLogChannel.info("-----*----")
    }

}

export default GHTKService;