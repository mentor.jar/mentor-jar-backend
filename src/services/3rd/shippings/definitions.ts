export enum  GHTK_HOST{
    HOST_DEV = 'https://services.ghtklab.com/',
    HOST_PRD = 'https://services.giaohangtietkiem.vn'
}

export enum  GHTK_CODE{
    CODE = 'S19420300'
}
export interface GHTKAccount {
    code: string,
    token: string
}