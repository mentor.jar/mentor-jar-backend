
import axios from 'axios';
import shajs from 'sha.js'
import { ConflictError, NotFoundError, PaymentError } from '../../../base/customError';
import keys from '../../../config/env/keys'
import { PaymentStatus, ShippingStatus } from '../../../models/enums/order';
import { GroupOrderRepo } from '../../../repositories/GroupOrderRepo';
import { OrderRepository } from '../../../repositories/OrderRepo';
import { PaymentRepo } from '../../../repositories/PaymentRepo';
import { OrderItemRepository } from '../../../repositories/OrderItemRepo';
import { cloneObj, ObjectId } from '../../../utils/helper';
import FirebaseStoreService from '../../firebase';
import { TableName } from '../../firebase/constant';
import { TYPE_REQUEST } from './definitions';
import { getTranslation, getUserLanguage } from '../../i18nCustom';
import notificationService, { NotifyRequestFactory } from '../../notification';
import { NOTIFY_TARGET_TYPE, NOTIFY_TYPE } from '../../notification/constants';
import { RSAHashing } from '../../hashsing/rsa/rsa';
import { InMemoryGBOrderPaymentStore } from '../../../SocketStores/GroupBuyOrderPaymentStore';
import { KEY } from '../../../constants/cache';
import { handlerAddRoomGroupBuyOrder } from '../../api/order';

const crypto = require("crypto");
const firestoreService = FirebaseStoreService._();
const paymentRepo = PaymentRepo.getInstance();
const hasingService = RSAHashing.getInstance();
const gbOrderPaymentStore = InMemoryGBOrderPaymentStore.getInstance();

interface IlogPayment {
    type?: string,
    param: any,
    message_error?: string
}
export class MomoPaymentService {

    private static instance: MomoPaymentService;

    public static getInstance(): MomoPaymentService {
        if (!MomoPaymentService.instance) {
            MomoPaymentService.instance = new MomoPaymentService();
        }
        return MomoPaymentService.instance;
    }

    public async createUrl(req: any) {
        let groupOrderRepo = GroupOrderRepo.getInstance();
        let orderRepo = OrderRepository.getInstance();
        let { order_id, type_id } = req.body;


        let partner_code = keys.momo_payment.partner_code
        let access_key = keys.momo_payment.access_key
        let serect_key = keys.momo_payment.sercret_key
        let order_info = `Thanh toan cho hoa don ${order_id}`;
        let redirect_url = keys.momo_payment.return_url;
        let ipn_url = keys.momo_payment.ipn_url;
        let extra_data = ""
        let requestType = "captureWallet"
        let request_id = Math.floor(Date.now() / 1000) + ""
        let amount;
        if (type_id == "ORDER") {
            const singleOrder: any = await orderRepo.findOne({ _id: order_id });
            order_id = order_id + "-" + request_id + "-ORDER"
            amount = singleOrder?.total_price

        } else {
            const groupOrder: any = await groupOrderRepo.findOne({ _id: order_id });
            order_id = order_id + "-" + request_id + "-GROUP"
            amount = groupOrder?.total_price
        }
        let rawHash = "accessKey=" + access_key + "&amount=" + amount + "&extraData=" + extra_data
            + "&ipnUrl=" + ipn_url + "&orderId=" + order_id + "&orderInfo=" + order_info
            + "&partnerCode=" + partner_code + "&redirectUrl=" + redirect_url +
            "&requestId=" + request_id + "&requestType=" + requestType

        let param = {
            partnerCode: partner_code,
            accessKey: access_key,
            requestId: request_id,
            amount: amount + "",
            orderId: order_id,
            orderInfo: order_info,
            redirectUrl: redirect_url,
            ipnUrl: ipn_url,
            extraData: extra_data,
            lang: "vi",
            requestType: requestType,
            signature: MomoPaymentService.createSignature(rawHash)
        }
        this.saveLogPayment({ type: TYPE_REQUEST.REQUEST_CREATED, param: param })
        let dataResult = await axios.post(keys.momo_payment.end_point, param);
        return dataResult.data;
    }

    public async createUrlV2(req: any) {
        let groupOrderRepo = GroupOrderRepo.getInstance();
        let orderRepo = OrderRepository.getInstance();
        let { order_id, type_id, ref_link, order_room_id, group_id, avatar_url } = req.body;

        let partner_code = keys.momo_payment.partner_code
        let access_key = keys.momo_payment.access_key
        let serect_key = keys.momo_payment.sercret_key
        let order_info = `Thanh toan cho hoa don ${order_id}`;
        let redirect_url = keys.momo_payment.return_url;
        let ipn_url = keys.momo_payment.ipn_url;
        let extra_data = ""
        let requestType = "captureWallet"
        let request_id = Math.floor(Date.now() / 1000) + ""
        let amount;

        if (type_id == "ORDER") {
            const singleOrder: any = await orderRepo.findOne({ _id: order_id });
            order_id = order_id + "-" + request_id + "-ORDER"
            amount = singleOrder?.total_price

        } else {
            const groupOrder: any = await groupOrderRepo.findOne({ _id: order_id });
            order_id = order_id + "-" + request_id + "-GROUP"
            amount = groupOrder?.total_price

        }

        let rawHash = "accessKey=" + access_key + "&amount=" + amount + "&extraData=" + extra_data
            + "&ipnUrl=" + ipn_url + "&orderId=" + order_id + "&orderInfo=" + order_info
            + "&partnerCode=" + partner_code + "&redirectUrl=" + redirect_url +
            "&requestId=" + request_id + "&requestType=" + requestType

        let param = {
            partnerCode: partner_code,
            accessKey: access_key,
            requestId: request_id,
            amount: amount + "",
            orderId: order_id,
            orderInfo: order_info,
            redirectUrl: redirect_url,
            ipnUrl: ipn_url,
            extraData: extra_data,
            lang: "vi",
            requestType: requestType,
            signature: MomoPaymentService.createSignature(rawHash)
        }
        this.saveLogPayment({ type: TYPE_REQUEST.REQUEST_CREATED, param: param })
        let dataResult = await axios.post(keys.momo_payment.end_point, param);
        return dataResult.data;
    }

    public static createSignature(rawHash) {
        return crypto.createHmac('sha256', keys.momo_payment.sercret_key)
            .update(rawHash)
            .digest('hex');
    }

    public async checkSumIpnAndUpdate(param) {
        //Checksum
        try {
            let accessKey = keys.momo_payment.access_key;
            let rawHash = "accessKey=" + accessKey + "&amount=" + param.amount
                + "&extraData=" + param.extraData + "&message=" + param.message
                + "&orderId=" + param.orderId + "&orderInfo=" + param.orderInfo
                + "&orderType=" + param.orderType + "&partnerCode=" + param.partnerCode
                + "&payType=" + param.payType + "&requestId=" + param.requestId
                + "&responseTime=" + param.responseTime + "&resultCode=" + param.resultCode
                + "&transId=" + param.transId;

            const checkType = param.orderId.split('-')[2];
            if (checkType == 'GROUP') {
                let m2signature = param.signature
                let partnerSignature = MomoPaymentService.createSignature(rawHash)
                let resultCode = param.resultCode
                let groupOrderId = param.orderId.split('-')[0];
                let orderRepo = OrderRepository.getInstance();
                let groupOrderRepo = GroupOrderRepo.getInstance();
                let paymentRepo = PaymentRepo.getInstance();
                let groupOrder: any = await groupOrderRepo.findOne({ _id: groupOrderId })
                if (!groupOrder) {
                    throw new NotFoundError("Đơn hàng không tồn tại");
                }
                if (groupOrder.total_price != param.amount) {
                    throw new ConflictError("Số tiền thanh toán không đúng với giá trị của đơn hàng.");
                };
                if (partnerSignature == m2signature) {
                    if (resultCode == '0') {
                        // update payment
                        groupOrder = cloneObj(groupOrder);
                        for (let i = 0; i < groupOrder.orders.length; i++) {
                            let order = groupOrder.orders[i];
                            if (order.payment_status != 'pending') {
                                throw new ConflictError("Đơn hàng đã được xác nhận");
                            }
                            let orderId = order._id
                            const payment = {
                                order_id: orderId,
                                user_id: order.user_id,
                                shop_id: order.shop_id,
                                price: order.total_price,
                                type: 'MOMO',
                                code: resultCode,
                                payment_info: param
                            }
                            const orderUpdate = {
                                payment_status: PaymentStatus.PAID,
                                shipping_status: ShippingStatus.WAIT_TO_PICK,
                                approved_time: new Date()
                            }
                            await paymentRepo.create(payment);
                            groupOrder.orders[i] = await orderRepo.update(orderId, orderUpdate);
                        }
                        const groupOrderUpdate = {
                            orders: groupOrder.orders,
                        }
                        await groupOrderRepo.update(groupOrderId, groupOrderUpdate);
                        groupOrderUpdate.orders.map(order => this.pushNotificationToOrderPaid(order));
                    } else {
                        throw new PaymentError(param.message);
                    }
                } else {
                    throw new PaymentError("Chữ ký không đúng");
                }
            } else {
                let m2signature = param.signature
                let partnerSignature = MomoPaymentService.createSignature(rawHash)
                let resultCode = param.resultCode
                let orderId = param.orderId.split('-')[0];
                let orderRepo = OrderRepository.getInstance();
                let groupOrderRepo = GroupOrderRepo.getInstance();
                let paymentRepo = PaymentRepo.getInstance();
                let order: any = await orderRepo.findOne({ _id: orderId })
                if (!order) {
                    throw new NotFoundError("Đơn hàng không tồn tại");
                }
                if (order.total_price != param.amount) {
                    throw new ConflictError("Số tiền thanh toán không đúng với giá trị của đơn hàng.");
                };
                if (partnerSignature == m2signature) {
                    if (resultCode == '0') {
                        // update payment
                        order = cloneObj(order);
                        if (order.payment_status != 'pending') {
                            throw new ConflictError("Đơn hàng đã được xác nhận");
                        }
                        let orderId = order._id
                        const payment = {
                            order_id: orderId,
                            user_id: order.user_id,
                            shop_id: order.shop_id,
                            price: order.total_price,
                            type: 'MOMO',
                            code: resultCode,
                            payment_info: param
                        }
                        const orderUpdate = {
                            payment_status: PaymentStatus.PAID,
                            shipping_status: ShippingStatus.WAIT_TO_PICK
                        }
                        await paymentRepo.create(payment);
                        await orderRepo.update(orderId, orderUpdate);
                        this.pushNotificationToOrderPaid(order)
                    } else {
                        throw new PaymentError(param.message);
                    }
                } else {
                    throw new PaymentError("Chữ ký không đúng");
                }
            }



        } catch (error) {
            throw new PaymentError(error.message);
        }


    }

    public async checkSumIpnAndUpdateV2(param) {
        //Checksum
        try {
            let accessKey = keys.momo_payment.access_key;
            let rawHash = "accessKey=" + accessKey + "&amount=" + param.amount
                + "&extraData=" + param.extraData + "&message=" + param.message
                + "&orderId=" + param.orderId + "&orderInfo=" + param.orderInfo
                + "&orderType=" + param.orderType + "&partnerCode=" + param.partnerCode
                + "&payType=" + param.payType + "&requestId=" + param.requestId
                + "&responseTime=" + param.responseTime + "&resultCode=" + param.resultCode
                + "&transId=" + param.transId;
            let dataCache: any = null;

            const checkType = param.orderId.split('-')[2];
            if (checkType == 'GROUP') {
                let m2signature = param.signature
                let partnerSignature = MomoPaymentService.createSignature(rawHash)
                let resultCode = param.resultCode
                let groupOrderId = param.orderId.split('-')[0];
                let orderRepo = OrderRepository.getInstance();
                let groupOrderRepo = GroupOrderRepo.getInstance();
                let paymentRepo = PaymentRepo.getInstance();
                let groupOrder: any = await groupOrderRepo.findOne({ _id: groupOrderId })
                if (!groupOrder) {
                    throw new NotFoundError("Đơn hàng không tồn tại");
                }
                if (groupOrder.total_price != param.amount) {
                    throw new ConflictError("Số tiền thanh toán không đúng với giá trị của đơn hàng.");
                };

                if (groupOrder.is_group_buy_order) {
                    dataCache = await this.getDataGroupOrderCache(groupOrder._id.toString());
                }
                if (partnerSignature == m2signature) {
                    if (resultCode == '0') {
                        // update payment
                        groupOrder = cloneObj(groupOrder);
                        for (let i = 0; i < groupOrder.orders.length; i++) {
                            let order = groupOrder.orders[i];
                            if (order.payment_status != 'pending') {
                                throw new ConflictError("Đơn hàng đã được xác nhận");
                            }
                            let orderId = order._id
                            const payment = {
                                order_id: orderId,
                                user_id: order.user_id,
                                shop_id: order.shop_id,
                                price: order.total_price,
                                type: 'MOMO',
                                code: resultCode,
                                payment_info: param
                            }
                            const orderUpdate = {
                                payment_status: PaymentStatus.PAID,
                                shipping_status: ShippingStatus.WAIT_TO_PICK,
                                approved_time: new Date()
                            }
                            await paymentRepo.create(payment);
                            groupOrder.orders[i] = await orderRepo.update(orderId, orderUpdate);
                        }
                        const groupOrderUpdate = {
                            orders: groupOrder.orders,
                        }
                        await Promise.all([
                            await groupOrderRepo.update(groupOrderId, groupOrderUpdate),
                            await handlerAddRoomGroupBuyOrder({ order: groupOrder.orders[0], data_cache: dataCache })
                        ])
                        this.removeDataGroupOrderCache(groupOrder._id.toString())
                        groupOrderUpdate.orders.map(order => this.pushNotificationToOrderPaid(order));
                    } else {
                        throw new PaymentError(param.message);
                    }
                } else {
                    throw new PaymentError("Chữ ký không đúng");
                }
            } else {
                let m2signature = param.signature
                let partnerSignature = MomoPaymentService.createSignature(rawHash)
                let resultCode = param.resultCode
                let orderId = param.orderId.split('-')[0];
                let orderRepo = OrderRepository.getInstance();
                let groupOrderRepo = GroupOrderRepo.getInstance();
                let paymentRepo = PaymentRepo.getInstance();
                let order: any = await orderRepo.findOne({ _id: orderId })
                if (!order) {
                    throw new NotFoundError("Đơn hàng không tồn tại");
                }
                if (order.total_price != param.amount) {
                    throw new ConflictError("Số tiền thanh toán không đúng với giá trị của đơn hàng.");
                };
                if (order.is_group_buy_order) {
                    dataCache = await this.getDataGroupOrderCache(order.group_order_id.toString());
                }
                if (partnerSignature == m2signature) {
                    if (resultCode == '0') {
                        // update payment
                        order = cloneObj(order);
                        if (order.payment_status != 'pending') {
                            throw new ConflictError("Đơn hàng đã được xác nhận");
                        }
                        let orderId = order._id
                        const payment = {
                            order_id: orderId,
                            user_id: order.user_id,
                            shop_id: order.shop_id,
                            price: order.total_price,
                            type: 'MOMO',
                            code: resultCode,
                            payment_info: param
                        }
                        const orderUpdate = {
                            payment_status: PaymentStatus.PAID,
                            shipping_status: ShippingStatus.WAIT_TO_PICK
                        }

                        await Promise.all([
                            await paymentRepo.create(payment),
                            await orderRepo.update(orderId, orderUpdate),
                            await handlerAddRoomGroupBuyOrder({ order, data_cache: dataCache })
                        ])
                        this.removeDataGroupOrderCache(order.group_order_id.toString())
                        this.pushNotificationToOrderPaid(order)
                    } else {
                        throw new PaymentError(param.message);
                    }
                } else {
                    throw new PaymentError("Chữ ký không đúng");
                }
            }
        } catch (error) {
            throw new PaymentError(error.message);
        }


    }

    public saveLogPayment({
        type = TYPE_REQUEST.REQUEST_CREATED,
        param,
        message_error
    }: IlogPayment) {
        param = {
            ...param,
            type,
            message_error
        }

        return firestoreService.save(TableName.PAYMENT_LOGS, param);
    }

    /**
    * partnerCode: partner code
    * orderId: Id của giao dịch hoàn tiền, không phải order id của đơn hàng
    * requestId: id request
    * amount: số tiền cần hoàn
    * transId: transaction id do momo cung cấp cho giao dịch thành công trước đó
    * lang: ngôn ngữ mess được trả về
    * description: mô tả (optional)
    * signature: signature hash bởi Hmac_SHA256
    */

    public async refundTransaction(param) {
        try {
            let { order_payment_id, description = "" } = param;

            const paymentInfo: any = await paymentRepo.findOrFail({ _id: ObjectId(order_payment_id) });

            let acessKey = keys.momo_payment.access_key
            let partnerCode = keys.momo_payment.partner_code
            let requestId = Math.floor(Date.now() / 1000) + "";

            let transId = paymentInfo.payment_info.transId;
            let orderId = order_payment_id;
            let amount = paymentInfo.price;

            let rawHash = "accessKey=" + acessKey + "&amount=" + amount + "&description=" +
                description + "&orderId=" + orderId + "&partnerCode=" + partnerCode +
                "&requestId=" + requestId + "&transId=" + transId;
            let partnerSignature = MomoPaymentService.createSignature(rawHash);

            let paramRequest = {
                "partnerCode": partnerCode,
                "orderId": orderId,
                "requestId": requestId,
                "amount": amount,
                "transId": transId,
                "lang": "vi",
                "description": description,
                "signature": partnerSignature
            }
            this.saveLogPayment({ type: TYPE_REQUEST.REQUEST_REFUND, param: paramRequest })
            let dataResult = await axios.post(keys.momo_payment.refund_end_point, paramRequest);
            return dataResult.data;

        } catch (error) {
            throw new PaymentError(error.response.data.message);
        }
    }

    async pushNotificationToOrderPaid(order) {
        const language = await getUserLanguage(order.user_id);
        await notificationService.saveAndSend(
            NotifyRequestFactory.generate(
                NOTIFY_TYPE.PAYMENT_MOMO_PAID,
                NOTIFY_TARGET_TYPE.BUYER,
                {
                    title: getTranslation(
                        language,
                        'notification.bidu_notification'
                    ),
                    content: getTranslation(
                        language,
                        'notification.payment.payment_momo_paid',
                        order.order_number
                    ),
                    receiverId: order.user_id,
                    orderId: order._id,
                    userId: order.user_id,
                    orderNumber: order.order_number,
                }
            )
        );
        return true;
    }

    async getDataGroupOrderCache(groupOrderId: string) {
        return await gbOrderPaymentStore.get(KEY.STORE_GROUP_ORDER_OF_GB, groupOrderId);
    }

    removeDataGroupOrderCache(groupOrderId: string) {
        return gbOrderPaymentStore.deleteById(groupOrderId)
    }
}
