import keys from '../../../config/env/keys';
import dateFormat from 'dateformat';
import shajs from 'sha.js';
import querystring from 'qs';
import { cloneObj, sortObject } from "../../../utils/helper";
import { OrderRepository } from '../../../repositories/OrderRepo';
import { VNPAY_LOCALE, VNPAY_CURRCODE, VNPAY_VERSION, VNPAY_COMMAND } from '../../../base/variable';
import { PaymentRepo } from '../../../repositories/PaymentRepo';
import { VnPayCode } from './definitions';
import { GroupOrderRepo } from '../../../repositories/GroupOrderRepo';
import { PaymentStatus, ShippingStatus } from '../../../models/enums/order';

class VnPayService {

    protected tmnCode;
    protected secretKey;
    private static _instance: VnPayService;

    static get _() {
        if (!this._instance) {
            this._instance = new VnPayService(keys.vnpay_payment.tmnCodeVnPay, keys.vnpay_payment.secretKeyVnPay);
        }
        return this._instance;
    }
    private constructor(tmnCode, secretKey) {
        this.tmnCode = tmnCode;
        this.secretKey = secretKey;
    }

    createPaymentUrl = async (req: any) => {
        let groupOrderRepo = GroupOrderRepo.getInstance();
        let orderRepo = OrderRepository.getInstance();
        const { order_id: orderId, type_id: typeId, is_web_request: isWebRequest } = req.body;
        let vnpUrl = keys.vnpay_payment.urlVnPay;

        const ipAddr = req.headers['x-forwarded-for'] ||
            req.connection.remoteAddress ||
            req.socket.remoteAddress ||
            req.connection.socket.remoteAddress;
        const returnUrl = isWebRequest ? keys.vnpay_payment.returnUrlWebVnPay : keys.vnpay_payment.returnUrlVnPay;
        const date = new Date();
        const createDate = dateFormat(date, 'yyyymmddHHmmss');
        const orderInfo = `Thanh toan cho don hang ${orderId}`;

        let vnp_Params = {};
        vnp_Params['vnp_Version'] = VNPAY_VERSION;
        vnp_Params['vnp_Command'] = VNPAY_COMMAND;
        vnp_Params['vnp_TmnCode'] = this.tmnCode;
        vnp_Params['vnp_Locale'] = VNPAY_LOCALE;
        vnp_Params['vnp_CurrCode'] = VNPAY_CURRCODE;
        if (typeId == "ORDER") {
            const singleOrder: any = await orderRepo.findOne({ _id: orderId });
            vnp_Params['vnp_TxnRef'] = orderId + "-" + Date.now() + "-ORDER";
            vnp_Params['vnp_Amount'] = singleOrder?.total_price * 100;
        } else {
            const orderGroupOrder: any = await groupOrderRepo.findOne({ _id: orderId });
            vnp_Params['vnp_TxnRef'] = orderId + "-" + Date.now() + "-GROUP";
            vnp_Params['vnp_Amount'] = orderGroupOrder?.total_price * 100;
        }

        vnp_Params['vnp_OrderInfo'] = orderInfo;
        vnp_Params['vnp_ReturnUrl'] = returnUrl;
        vnp_Params['vnp_IpAddr'] = ipAddr;
        vnp_Params['vnp_CreateDate'] = createDate;

        vnp_Params = sortObject(vnp_Params);

        // convert object to string
        let signData = this.secretKey + querystring.stringify(vnp_Params, { encode: false });

        // hashing signData
        let secureHash = shajs('sha256').update(signData).digest('hex');

        vnp_Params['vnp_SecureHashType'] = 'SHA256';
        vnp_Params['vnp_SecureHash'] = secureHash;

        // generate url
        vnpUrl += '?' + querystring.stringify(vnp_Params, { encode: true });
        // console.log(vnpUrl);

        return { RspCode: VnPayCode.SUCCESS, url: vnpUrl };

    }

    getReturnPaymentUrl = async (req: any) => {
        let vnp_Params = req.query;

        let secureHash = vnp_Params['vnp_SecureHash'];
        delete vnp_Params['vnp_SecureHash'];
        delete vnp_Params['vnp_SecureHashType'];

        vnp_Params = sortObject(vnp_Params);

        let signData = this.secretKey + querystring.stringify(vnp_Params, { encode: false });

        let checkSum = shajs('sha256').update(signData).digest('hex');

        return secureHash === checkSum ? (vnp_Params['vnp_ResponseCode'] == VnPayCode.SUCCESS ? 1 : 0) : 0;

    }

    getReturnPaymentIpn = async (req: any) => {
        try {
            let vnp_Params = req.query;

            let secureHash = vnp_Params['vnp_SecureHash'];
            delete vnp_Params['vnp_SecureHash'];
            delete vnp_Params['vnp_SecureHashType'];

            vnp_Params = sortObject(vnp_Params);

            let signData = this.secretKey + querystring.stringify(vnp_Params, { encode: false });

            let checkSum = shajs('sha256').update(signData).digest('hex');

            const checkType = vnp_Params['vnp_TxnRef'].split('-')[2];
            if (checkType == 'GROUP') {
                let groupOrderId = vnp_Params['vnp_TxnRef'].split('-')[0];
                let rspCode = vnp_Params['vnp_ResponseCode'];
                let price = parseInt(vnp_Params['vnp_Amount']) / 100;
                let orderRepo = OrderRepository.getInstance();
                let groupOrderRepo = GroupOrderRepo.getInstance();
                let paymentRepo = PaymentRepo.getInstance();
                let groupOrder: any = await groupOrderRepo.findOne({ _id: groupOrderId })

                if (!groupOrder) {
                    return { RspCode: VnPayCode.NOT_FOUND, Message: 'Không tìm thấy hoá đơn' }
                }

                if (groupOrder.total_price != price) {
                    return { RspCode: VnPayCode.PRICE_ERR, Message: 'Số tiền không giống với khởi tạo' }
                }
                if (secureHash !== checkSum) {
                    return { RspCode: VnPayCode.SIGNATURE_ERR, Message: 'Chữ ký không hợp lệ' }
                }
                groupOrder = cloneObj(groupOrder);
                for (let i = 0; i < groupOrder.orders.length; i++) {
                    let order = groupOrder.orders[i];
                    if (order.payment_status != 'pending') {
                        return { RspCode: VnPayCode.CONFIRMED, Message: 'Hoá đơn đã được xác nhận' }
                    }
                    let orderId = order._id
                    const payment = {
                        order_id: orderId,
                        user_id: order.user_id,
                        shop_id: order.shop_id,
                        price: order.total_price,
                        type: 'VNPAY',
                        code: rspCode,
                        payment_info: vnp_Params
                    }
                    await paymentRepo.create(payment);
                    if (rspCode == VnPayCode.SUCCESS) {
                        const orderUpdate = {
                            payment_status: PaymentStatus.PAID,
                            shipping_status: ShippingStatus.WAIT_TO_PICK,
                            approved_time: new Date()
                        }
                        groupOrder.orders[i] = await orderRepo.update(orderId, orderUpdate);
                    }
                }
                const groupOrderUpdate = {
                    orders: groupOrder.orders,
                }
                await groupOrderRepo.update(groupOrderId, groupOrderUpdate)
            } else {
                let orderId = vnp_Params['vnp_TxnRef'].split('-')[0];
                let rspCode = vnp_Params['vnp_ResponseCode'];
                let price = parseInt(vnp_Params['vnp_Amount']) / 100;
                let orderRepo = OrderRepository.getInstance();
                let paymentRepo = PaymentRepo.getInstance();
                let order: any = await orderRepo.findOne({ _id: orderId })

                if (!order) {
                    return { RspCode: VnPayCode.NOT_FOUND, Message: 'Không tìm thấy hoá đơn' }
                }

                if (order.total_price != price) {
                    return { RspCode: VnPayCode.PRICE_ERR, Message: 'Số tiền không giống với khởi tạo' }
                }
                if (secureHash !== checkSum) {
                    return { RspCode: VnPayCode.SIGNATURE_ERR, Message: 'Chữ ký không hợp lệ' }
                }
                if (order.payment_status != 'pending') {
                    return { RspCode: VnPayCode.CONFIRMED, Message: 'Hoá đơn đã được xác nhận' }
                }
                order = cloneObj(order);
                const payment = {
                    order_id: order._id,
                    user_id: order.user_id,
                    shop_id: order.shop_id,
                    price: order.total_price,
                    type: 'VNPAY',
                    code: rspCode,
                    payment_info: vnp_Params
                }
                await paymentRepo.create(payment);
                if (rspCode == VnPayCode.SUCCESS) {
                    const orderUpdate = {
                        payment_status: PaymentStatus.PAID,
                        shipping_status: ShippingStatus.WAIT_TO_PICK
                    }
                    await orderRepo.update(orderId, orderUpdate);
                }
            }
            return { RspCode: VnPayCode.SUCCESS, Message: 'Thanh toán thành công' }
        } catch (error) {
            return { RspCode: VnPayCode.UNKNOW_ERR, Message: 'Lỗi không xác định' }
        }

    }
}

export default VnPayService;
