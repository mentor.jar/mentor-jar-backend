export enum VnPayCode {
    SUCCESS = '00',
    CANCEL = '24',
    NOT_FOUND = '01',
    CONFIRMED = '02',
    PRICE_ERR = '03',
    SIGNATURE_ERR = '97',
    UNKNOW_ERR = '99'
}

export enum TYPE_REQUEST {
    REQUEST_CREATED = 'request_created',
    RESPONSE_CREATED = 'response_created',
    REQUEST_REFUND = 'request_refund',
    RESPONSE_REFUND = 'response_refund',
}