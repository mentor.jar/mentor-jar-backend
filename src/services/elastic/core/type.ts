import { TransportRequestOptions } from '@elastic/elasticsearch/lib/Transport';
import { Client } from '@elastic/elasticsearch';
import { nanoid } from 'nanoid'

interface QueryResultSets {
    total: any;
    record: any;
    aggregations?: any;
}

interface ElasticMapper {
    map(rawInstance: any)
}

class ElasticEvent {

    public index: string;
    public name: string;
    public id: string;
    public data: any;
    public traceId: string;
    public type: 'update' | 'delete';

    constructor({
        index,
        type,
        name,
        id,
        data
    }) {
        this.index = index
        this.type = type
        this.name = name
        this.id = id
        this.data = data
        this.traceId = nanoid()
    }

    toLog(){
        return `name:${this.name}, traceId:${this.traceId}, id:${this.id} `
    }
}

type ElasticEventListener = (event: ElasticEvent) => void

export {
    Client,
    QueryResultSets,
    TransportRequestOptions,
    ElasticEventListener,
    ElasticEvent,
    ElasticMapper,
}









