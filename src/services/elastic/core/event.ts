import EventEmitter from "events";
import { ElasticEventListener, ElasticEvent } from "./type";


export class ElasticEventBus {

    private static namespacePrefix = 'elasticEvent_';
    private static _instances: ElasticEventBus = null;

    private _eventRegisters: Map<string, ElasticEventListener>;
    private _emitter = new EventEmitter();

    static getInstance() {
        if (!this._instances) {
            this._instances = new ElasticEventBus();
        }
        return this._instances
    }

    private naming(eventName: string) {
        return ElasticEventBus.namespacePrefix + eventName;
    }

    public register(eventName: string, callback: ElasticEventListener) {
        const _eventName = this.naming(eventName);

        if (this._eventRegisters.get(_eventName)) {
            throw new Error(`Duplicate event register for ${eventName}`)
        }

        this._eventRegisters.set(_eventName, callback);
        this._emitter.on(_eventName, callback)
    }

    public unRegister(eventName: string) {
        const _eventName = this.naming(eventName);

        this._eventRegisters.delete(_eventName);
        this._emitter.removeAllListeners(_eventName);
    }

    public fire(event: ElasticEvent) {
        this._emitter.emit(event.name, event)
    }

}