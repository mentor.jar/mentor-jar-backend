import { ElasticClient } from './client';
import { TransportRequestOptions, Client } from './type';

export class ElasticCommand {

    private static _instances: ElasticCommand = null;

    constructor() {
    }

    get _client(): Client{
        return ElasticClient.getInstance();
    }

    static getInstance() {
        if (!this._instances) {
            this._instances = new ElasticCommand();
        }
        return this._instances
    }

    public async createIndex (index: string, mappings: object) {
        await this._client.indices.create({
            index,
            body: mappings
        })
    }

    public async putMapping (index: string, mappings: object) {
        await this._client.indices.putMapping({
            index,
            body: mappings
        })
    }

    public async update(
        index: string,
        query: any,
        data: any,
        options?: TransportRequestOptions
    ): Promise<any> {
        const result = await this._client.updateByQuery({
            index,
            q: query,
            body:  {
                doc: data, 
                doc_as_upsert: true
            }
        }, options)

        return result;
    }


    public async updateById(
        index: string,
        id: string,
        data: any,
        options?: TransportRequestOptions
    ): Promise<any> {
        const result = await this._client.update({
            index,
            id,
            body:  {
                doc: data, 
                doc_as_upsert: true
            }
        }, options)

        return result;
    }

    public async create(
        index: string,
        id: string,
        data: any,
        options?: TransportRequestOptions
    ): Promise<any> {
        const result = await this._client.create({
            index,
            id,
            body: data
        }, options)

        return result;
    }

    public async delete(
        index: string,
        query: any,
        options?: TransportRequestOptions
    ): Promise<any> {
        const result = await this._client.deleteByQuery({
            index,
            q: query,
            body: null
        }, options)

        return result;
    }

    public async deleteById(
        index: string,
        id: string,
        options?: TransportRequestOptions
    ): Promise<any> {
        const result = await this._client.delete({
            index,
            id,
        }, options)

        return result;
    }

    public async bulk(
        index: string,
        data: any,
        options?: TransportRequestOptions
    ): Promise<any> {
        const result = await this._client.helpers.bulk({
            datasource: data,
            onDocument(doc: any) {
                return [
                    { update: { _index: index, _id: doc._id } },
                    { doc_as_upsert: true }
                ]
            }
        }, options)

        return result;
    }

}