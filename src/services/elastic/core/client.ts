import { Client, ClientOptions } from "@elastic/elasticsearch";

export class ElasticClient {

    private static _connection: Client = null;


    public static connect(option: ClientOptions) {
        this._connection = new Client(option)
        this.ping();
    }

    public static getInstance() {
        return this._connection;
    }

    public static ping() {
        if (this._connection) {
            this._connection.ping({}, function (error) {
                if (error) {
                    console.log(error)
                    console.log('Elasticsearch is down!');
                } else {
                    console.log('Elasticsearch: All is well');
                }
            })
        }
    }
}