import { ElasticClient } from './client';
import { Client, QueryResultSets, TransportRequestOptions } from './type';

export class ElasticQuery {

    private static _instances: ElasticQuery = null;

    constructor() {
    }

    get _client(): Client{
        return ElasticClient.getInstance();
    }

    static getInstance() {
        if (!this._instances) {
            this._instances = new ElasticQuery();
        }
        return this._instances
    }

    public async search(
        index: string,
        query: any,
        paginate: any,
        sort?: any,
        options?: TransportRequestOptions,
    ): Promise<QueryResultSets> {
        const result = await this._client.search({
            index,
            body: {
                query,
                sort
            },
            from: paginate.from,
            size: paginate.size
        }, options)

        const { total, hits } = result.body.hits;

        if (!hits || !hits.map) {
            throw new Error('Search result is unable to iterate');
        }

        return {
            total,
            record: hits
        }

    }

    public async searchDistinct(
        index: string,
        query: any,
        aggs?: any,
        options?: TransportRequestOptions,
    ): Promise<any> {
        const result = await this._client.search({
            index,
            body: {
                query,
                size: 0,
                aggs,
            },
        }, options)

        const { buckets } = result.body.aggregations.list_category;

        if (!buckets) {
            throw new Error('Search result is unable to iterate');
        }

        return buckets;

    }

    public async getById(
        index: string,
        id: string,
        options?: TransportRequestOptions
    ): Promise<any> {
        const result = await this._client.get({
            id,
            index,
        }, options)

        return result.body;
    }

    public async count(
        index: string,
        query: any,
        options: TransportRequestOptions
    ): Promise<number> {

        const result = await this._client.count({
            index,
            body: {
                query
            }
        }, options)

        return (result.body.count as number);
    }
}

