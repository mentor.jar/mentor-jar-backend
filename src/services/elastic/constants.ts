let suffix = ""

if (process.env.NODE_ENV === 'production') {
    suffix = "-prd"
}

if (process.env.NODE_ENV === 'staging' || process.env.NODE_ENV === "development") {
    suffix = "-stg"
}

const elsIndexName = {
    product: `products${suffix}`,
    user: `users${suffix}`
};

const elsEventName = {
    updateAProduct: 'updateAProduct',
    createAProduct: 'createAProduct',
    deleteAProduct: 'deleteAProduct',
}

export {
    elsIndexName,
    elsEventName
}