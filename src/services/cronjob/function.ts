import { OrderRepository } from "../../repositories/OrderRepo";
import { PaymentMethodRepo } from "../../repositories/PaymentMethodRepo";
import { ProductRepo } from "../../repositories/ProductRepo";
import { VariantRepo } from "../../repositories/Variant/VariantRepo";
import { cloneObj, isMappable } from "../../utils/helper";

let orderRepository = OrderRepository.getInstance();
let productRepo = ProductRepo.getInstance();
let variantRepo = VariantRepo.getInstance();

export const updateProductQuantity = async (listOrderCancel) => {
    // Update variant
    const ordersWithItems = await orderRepository.getOrderByArrayId(listOrderCancel);
    // Create 2 object to count the quantity of each variant and product that we wanna change
    let quantity_variant_map_id: any = {};
    let quantity_product_map_id: any = {};
    let array_product_have_variant: any = [];
    ordersWithItems.forEach(order => {
        order.order_items.map(item => {
            // if Item have variant, then count on variant, else count on product which have no variant
            if (item.variant) {
                if (quantity_variant_map_id[item.variant._id.toString()]) {
                    quantity_variant_map_id[item.variant._id.toString()] += item.quantity
                } else {
                    quantity_variant_map_id[item.variant._id.toString()] = item.quantity
                }
                array_product_have_variant.push(item.product_id.toString());
            } else {
                if (quantity_product_map_id[item.product_id.toString()]) {
                    quantity_product_map_id[item.product_id.toString()] += item.quantity
                } else {
                    quantity_product_map_id[item.product_id.toString()] = item.quantity
                }
            }
        });
    });

    // update for product have no variant
    const arrayIdProductNoVariant = Object.keys(quantity_product_map_id);
    const productNoVariantPromises = arrayIdProductNoVariant.map(id => {
        return new Promise(async (resolve, reject) => {
            const product: any = await productRepo.findOne({ _id: id });
            const quantity = product.quantity + quantity_product_map_id[id];
            await productRepo.update(id, { quantity: quantity })
            resolve(true);
        })
    })
    await Promise.all(productNoVariantPromises);

    // update for variant
    const arrayIdVariant = Object.keys(quantity_variant_map_id);
    const variantPromises = arrayIdVariant.map(id => {
        return new Promise(async (resolve, reject) => {
            const variant: any = await variantRepo.findOne({ _id: id });
            if (variant) {
                const quantity = variant.quantity + quantity_variant_map_id[id];
                await variantRepo.update(id, { quantity: quantity })
            }
            resolve(true);
        })
    })
    await Promise.all(variantPromises);

    // update for products that have variant
    const listUniqueProduct: any = [...new Set(array_product_have_variant)];
    const productPromises = listUniqueProduct.map(productId => {
        return new Promise(async (resolve, reject) => {
            let totalQuantity: number = 0;
            const listVariant: any = await variantRepo.find({ product_id: productId });
            listVariant.forEach(variant => {
                totalQuantity += variant.quantity;
            });
            await productRepo.update(productId, { quantity: totalQuantity });
            resolve(productId);
        })
    })
    await Promise.all(productPromises);
}

export const updateQuantityProductGroupBuyPaymentFailed = async (listOrderCancel) => {
    let listProductHaveVariant: any = [];
    let quantity_variant_map_id: any = {};
    let quantity_product_map_id: any = {};

    // Get list order with order item
    const ordersWithItems = await orderRepository.getOrderByArrayId(listOrderCancel);

    ordersWithItems.forEach(order => {
        order.order_items.forEach(item => {
            // if Item have variant, then count on variant, else count on product which have no variant
            if (item.variant) {
                let productFound = listProductHaveVariant.find((product) => product.product_id === item.product_id.toString());
                if (productFound) {
                    productFound.quantity_sold += item.quantity;
                    productFound.variants[item.variant._id.toString()]
                        ? (productFound.variants[item.variant._id.toString()] += item.quantity)
                        : (productFound.variants[item.variant._id.toString()] = item.quantity);
                } else {
                    listProductHaveVariant.push({
                        product_id: item.product_id.toString(),
                        variants: {
                            [item.variant._id.toString()]: +item.quantity
                        },
                        quantity_sold: +item.quantity,
                    })
                }

                quantity_variant_map_id[item.variant._id.toString()]
                    ? (quantity_variant_map_id[item.variant._id.toString()] += item.quantity)
                    : (quantity_variant_map_id[item.variant._id.toString()] = item.quantity);
            } else {
                quantity_product_map_id[item.product_id.toString()]
                    ? quantity_product_map_id[item.product_id.toString()] += item.quantity
                    : quantity_product_map_id[item.product_id.toString()] = item.quantity
            }
        });
    });

    const arrayIdVariant: any = Object.keys(quantity_variant_map_id);
    if(isMappable(arrayIdVariant)) {
        const variantPromises = arrayIdVariant.map(id => {
            return new Promise(async (resolve, reject) => {
                const variant: any = await variantRepo.findOne({ _id: id });
                if (variant) {
                    const quantity = variant.quantity + quantity_variant_map_id[id];
                    await variantRepo.update(id, { quantity: quantity })
                }
                resolve(true);
            })
        })
        await Promise.all(variantPromises);
    }

    if(isMappable(listProductHaveVariant)) {
        await Promise.all(
            listProductHaveVariant.map(async(productHaveVariant) => {
                let product = await productRepo.findById(productHaveVariant.product_id);
                product = cloneObj(product);

                let { quantity, sold, group_buy: groupBuy } = product;
                const quantitySold: number = +productHaveVariant.quantity_sold;

                quantity += quantitySold;
                sold = sold - quantitySold > 0 ? sold - quantitySold : 0;

                groupBuy.quantity += quantitySold;
                groupBuy.sold = groupBuy?.sold - quantitySold > 0 ? groupBuy?.sold - quantitySold : 0;
                groupBuy.groups = groupBuy?.groups.map((group) => {
                    if(isMappable(group?.variants)){
                        group.variants = group?.variants.map((variant) => {
                            const quantitySoldVariant = +productHaveVariant.variants[variant?.variant_id];
                            if(Boolean(quantitySoldVariant)) {
                                variant.quantity += quantitySoldVariant;
                                variant.sold = variant?.sold - quantitySoldVariant > 0 ? variant?.sold - quantitySoldVariant : 0;
                            }
                            return variant;
                        })
                    }
                    return group;
                })

                await productRepo.update(productHaveVariant.product_id, {
                    quantity,
                    sold,
                    group_buy: groupBuy,
                });
            })
        )
    }

    const arrayIdProductNoVariant: any = Object.keys(quantity_product_map_id);
    if(isMappable(arrayIdProductNoVariant)) {
        await Promise.all(
            arrayIdProductNoVariant.map(async(productId) => {
                let product = await productRepo.findById(productId);
                product = cloneObj(product);

                let { quantity, sold, group_buy: groupBuy } = product;
                const quantitySold: number = +quantity_product_map_id[productId];

                quantity += quantitySold;
                sold = sold - quantitySold > 0 ? sold - quantitySold : 0;

                groupBuy.quantity += quantitySold;
                groupBuy.sold = groupBuy?.sold - quantitySold > 0 ? groupBuy?.sold - quantitySold : 0;
                
                await productRepo.update(productId, {
                    quantity,
                    sold,
                    group_buy: groupBuy,
                });
            })
        )
    }
}

export const updateQuantityProductGroupBuy = async (originProduct, orderRoomsRemove, listOrderCancel) => {
    const { _id: id, quantity, sold, order_rooms: orderRooms, group_buy: groupBuy } = originProduct;
    let totalQuantity: number = 0
    // Create 2 object to count the quantity of each variant and product that we wanna change
    let quantity_variant_map_id: any = {};
    let quantity_product_map_id: any = {};

    // Get list order with order item
    const ordersWithItems = await orderRepository.getOrderByArrayId(listOrderCancel);
    
    ordersWithItems.forEach(order => {
        order.order_items.forEach(item => {
            // if Item have variant, then count on variant, else count on product which have no variant
            if (item.variant) {
                quantity_variant_map_id[item.variant._id.toString()]
                    ? (quantity_variant_map_id[item.variant._id.toString()] += item.quantity)
                    : (quantity_variant_map_id[item.variant._id.toString()] = item.quantity);
            } else {
                quantity_product_map_id[item.product_id.toString()]
                    ? quantity_product_map_id[item.product_id.toString()] += item.quantity
                    : quantity_product_map_id[item.product_id.toString()] = item.quantity
            }
            totalQuantity += item.quantity;
        });
    });

    // Handler data update product
    const orderRoomsUpdate = orderRooms.filter(room => !orderRoomsRemove.includes(room))
    const quantityUpdate = quantity + totalQuantity; 
    const soldUpdate = sold - totalQuantity > 0 ? sold - totalQuantity : 0;
    let groupBuyUpdate = groupBuy;
    groupBuyUpdate.quantity = groupBuy.quantity + totalQuantity;
    groupBuyUpdate.sold = groupBuy.sold - totalQuantity > 0 ? groupBuy.sold - totalQuantity : 0;

    // // update for variant
    const arrayIdVariant: any = Object.keys(quantity_variant_map_id);
    if (isMappable(arrayIdVariant)) {
        const variantPromises = arrayIdVariant.map(id => {
            return new Promise(async (resolve, reject) => {
                const variant: any = await variantRepo.findOne({ _id: id });
                if (variant) {
                    const quantity = variant.quantity + quantity_variant_map_id[id];
                    await variantRepo.update(id, { quantity: quantity })
                }
                resolve(true);
            })
        })
        await Promise.all(variantPromises);

        groupBuyUpdate.groups = groupBuyUpdate?.groups?.map((group) => {
            const variantFound = group?.variants.find((variant) => variant?.variant_id.toString() === arrayIdVariant[0])
            if (variantFound) {
                const quantityRollBack = quantity_variant_map_id[arrayIdVariant[0]];
                variantFound.quantity += quantityRollBack
                variantFound.sold = variantFound.sold - quantityRollBack > 0 ? variantFound.sold - quantityRollBack : 0;
            }

            return group;
        })

    } 

    await productRepo.update(id, {
        quantity: quantityUpdate,
        sold: soldUpdate,
        order_rooms: orderRoomsUpdate,
        group_buy: groupBuyUpdate,
    });
}