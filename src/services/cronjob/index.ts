import approveOrder from './jobs/approveOrder';
import cancelOrder from './jobs/cancelOrder';
import cancelPromotion from './jobs/cancelPromotion';
import autoShippedOrderAfter3Days from './jobs/orderShipping';
import remindLivestream, { remindLivestreamRunner } from './jobs/remindLivestream';
import removeLivestream from './jobs/removeLivestream';
import taskEndStream from './jobs/taskEndStream';
import cancelOrderApproved from './jobs/cancelOrderApproved';
import removeUnuseLive from './jobs/removeUnuseLive';
import autoCancelAfterSellerPrepaired from './jobs/cancelOrderAfterPrepaired';
import startPromotion from './jobs/startPromotion';
import updateSold from './jobs/updateSold';
import cancelPreOrder from './jobs/cancelPreOrder';
import autoRankingShop from './jobs/rankingShop'
import reCalculatePopularMark from './jobs/popularProduct';
import recacheSuggestProduct from './jobs/updateSuggestProductInCache';
import recacheTopProductByCategory from './jobs/topProductByCategoryInCache';
import saveTrackingFromCache from './jobs/saveTrackingFromCache';
import autoClearAccessCache from './jobs/clearAccessCountCache';
import remindPushNotification from './jobs/remindPushNotification';
import mergeRedisToDB from './jobs/mergeRedisToDB';
import rankingShopByPolicy from './jobs/rankingShopByPolicy'
import saveTrackingFromCacheToFirestore from './jobs/saveTrackingFromCacheToFirestore';
import updateTopSellerJob from './jobs/topShop';
import autoGetUserAndShop from './jobs/getUserAndShop';
import syncProduct20Second from './jobs/elasticSearch/syncProduct'
import syncProductDaily from './jobs/elasticSearch/syncAllProduct';
import updatePrepareOrder from './jobs/cachePrepareOrder';
import autoReCalculateTotalAccess from './jobs/reCalculateTotalAccessLegal';
import clearVoucherHistoryByDayJob from './jobs/clearVoucherHistoryByDay';
import updateBiggestPriceShop from './jobs/elasticSearch/updateBiggestPriceShop';
import cancelGroupBuyOrderJob from './jobs/cancelGroupBuyOrder';

autoClearAccessCache.start()
//remindLivestream.start()
// taskEndStream.start()
approveOrder.start() // turn off for event 133
// saveTrackingFromCache.start() // turn off for event
saveTrackingFromCacheToFirestore.start() // turn off for event 133
// approveOrder.start() // turn off for event
cancelOrder.start() // turn off for event 133
cancelPromotion.start()
// removeLivestream.start()
autoShippedOrderAfter3Days.start() // turn off for event 133
cancelOrderApproved.start()// turn off for event 133
// removeUnuseLive.start()
// autoCancelAfterSellerPrepaired.start() turn off for TET holiday
startPromotion.start()
updateSold.start() // turn off for event 133
cancelPreOrder.start() // turn off for event 133
autoRankingShop.start()
rankingShopByPolicy.start()
reCalculatePopularMark.start()
recacheSuggestProduct.start() // turn off for event 133
recacheTopProductByCategory.start() // turn off for event 133
remindPushNotification.start() // can turn off for event 133
mergeRedisToDB.start()
updateTopSellerJob.start()
autoGetUserAndShop.start()
syncProduct20Second.start()
syncProductDaily.start();

updatePrepareOrder.start();
autoReCalculateTotalAccess.start()
clearVoucherHistoryByDayJob.start()
updateBiggestPriceShop.start()
cancelGroupBuyOrderJob.start()

export {
    // remindLivestreamRunner
}