import cron from "node-cron";
import { RoomChatDTO } from "../../../DTO/RoomChatDTO";
import { RoomChatRepo } from "../../../repositories/RoomChatRepo";
import { TrackingJobProcessRepo } from "../../../repositories/TrackingJobProcessRepo";
import { InMemoryMessageForRoomStore } from "../../../SocketStores/MessageStore";
import { Topic } from "../constants";

const roomChatRepo = RoomChatRepo.getInstance();
const jobProcessRepo = TrackingJobProcessRepo.getInstance();
const roomStore = InMemoryMessageForRoomStore.getInstance()
export const mergeRedisToDBRunner = async () => {
    try {

        if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') || !process.env.IS_SERVER_WOWZA) return;
        let flagDoing = await jobProcessRepo.checkProcessDoing("SYNC_MESSAGE_CHAT");
        if (flagDoing) {
            console.log("Process job sync chat doing")
            return;
        }
        let key_room_chats = await roomStore.getFullKeyRoom()
        // let key_room_chats = ['615bc4d6b7337c0012d645fa'];
        let rooms = await roomChatRepo.find({
            _id: { $in: key_room_chats },
        })
        console.log("start merge chat")
        let jobProcess: any = await jobProcessRepo.createProcess({
            target: rooms.length,
            doing: 0,
            name: "SYNC_MESSAGE_CHAT",
            data_detail: {
                list_room_id: rooms.map(e => e._id.toString())
            },
        })
        for (let i = 0; i < rooms.length; i++) {
            let room: any = rooms[i]
            let roomRedis = await roomStore.getRoom(room._id.toString())
            if (roomRedis) {
                let roomMerge = await roomChatRepo.mergeMessage(roomRedis)
                let dtoChat = new RoomChatDTO(roomMerge);
                await roomChatRepo.update(room._id.toString(), {
                    messages: roomMerge.messages,
                    users: roomMerge.users,
                    last_message: roomMerge.last_message
                })
                roomRedis.messages = dtoChat.getEndMoreMessage(0)
                await roomStore.saveRoom(roomRedis);
                let list_room_id = jobProcess?.data_detail?.list_room_id.filter(e => {
                    return e != room._id.toString()
                })
                await jobProcessRepo.updateProcessIncrease("SYNC_MESSAGE_CHAT", { list_room_id })
            }
        }

        console.log("Done")

    } catch (error) {
        console.log(error);
    }
}

const mergeRedisToDB = cron.schedule(
    Topic.everyDay0AM,
    mergeRedisToDBRunner
);
export default mergeRedisToDB;
