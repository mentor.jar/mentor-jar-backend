import cron from "node-cron";
import { ProductRepo } from "../../../repositories/ProductRepo";
import { Topic } from "../constants";

const productRepo = ProductRepo.getInstance();

const updateProductSoldRunner = async () => {
    try {
        if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') || process.env.IS_SERVER_WOWZA) return;
        const products: Array<any> = await productRepo.getProductWithSoldQuantity()
        const productHasSold = products.filter(item => item.order_items.length && item.order_items?.[0]?.sold !== item?.sold)
        
        Promise.all(
            productHasSold.map((product) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        productRepo.update(product._id, {
                            sold: product.order_items?.[0]?.sold || 0,
                        })
                        resolve(true)
                    } catch (error) {
                        console.log(error);
    
                    }
                })
            })
        )
    } catch (error) {
        console.log(error);
    }
}

const updateProductSold = cron.schedule(
    Topic.everyDay3AM,
    updateProductSoldRunner
);
export default updateProductSold;