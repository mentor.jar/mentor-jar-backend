import cron from "node-cron";
import { ProductRepo } from "../../../repositories/ProductRepo";
import { InMemoryProductStore } from "../../../SocketStores/ProductStore";
import { Topic } from "../constants";

const productRepo = ProductRepo.getInstance();
const productCache = InMemoryProductStore.getInstance();

const cacheSuggestProduct = async () => {
  try {
    if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') || process.env.IS_SERVER_WOWZA) return;
    // job 
    console.info("RUNNING UPDATE SUGGEST PRODUCT IN CACHE");
    console.info("[1/3]: Get from database")
    const productSuggests: any = await productRepo.getSuggestProductList(1, 400, null)
    console.info(`Found ${productSuggests?.data?.length} products`)
    console.info("[2/3]: Checking current cache")
    const oldData = await productCache.get("suggestProducts")
    console.log(oldData?.length);
    console.info("[3/3]: Save to redis cache")
    const data = await productCache.save("suggestProducts", productSuggests?.data)
    console.log(`Saved ${data?.length} products`);

    return;
  } catch (error) {
    console.log("Error when update suggest product cache: ", error.message);
  }
}

const recacheSuggestProduct = cron.schedule(
  Topic.everyFiveMinute,
  cacheSuggestProduct
);
export default recacheSuggestProduct;
