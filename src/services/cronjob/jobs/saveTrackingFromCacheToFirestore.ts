import cron from "node-cron";
import { InMemoryTrackingStore } from "../../../SocketStores/TrackingStore";
import TrackingService from "../../tracking";
import { Topic } from "../constants";

const trackingCache = InMemoryTrackingStore.getInstance();

const saveTrackingJob = async () => {
  try {
    if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') || process.env.IS_SERVER_WOWZA) return;
    // job 
    console.info("RUNNING SAVE TRACKING FROM CACHE TO FIRESTORE");
    console.info("[1/2]: Get from cache")
    let trackings = await trackingCache.get("interactiveProducts");
    const promises = trackings.map(item => {
      return new Promise(async (resolve, reject) => {
        try {
          const { user_id, product_id, action_type } = item
          const trackingItem: any = await TrackingService._.userInteractiveProduct(
            user_id,
            product_id,
            action_type
          );
          resolve(trackingItem)
        } catch (error) {
          reject(error)
        }
      })
    })
    console.info("[2/2]: Save to firestore")
    const remainingTracking = promises.reduce((tracks, promise, idx) => {
      promise.then().catch(() => {
        tracks.push(trackings[idx]);
      })
      return tracks;
    }, []);
    trackingCache.save('interactiveProducts', remainingTracking)
    return;
  } catch (error) {
    console.log("Error when update newest product cache: ", error.message);
  }
}

const saveTrackingFromCacheToFirestore = cron.schedule(
  Topic.everyOneHour,
  saveTrackingJob
);
export default saveTrackingFromCacheToFirestore;
