import cron from 'node-cron';
import { CancelType, ShippingStatus } from '../../../models/enums/order';
import { OrderItemRepository } from '../../../repositories/OrderItemRepo';
import { OrderRepository } from '../../../repositories/OrderRepo';
import { PaymentMethodRepo } from '../../../repositories/PaymentMethodRepo';
import { ProductRepo } from '../../../repositories/ProductRepo';
import { UserRepo } from '../../../repositories/UserRepo';
import { VariantRepo } from '../../../repositories/Variant/VariantRepo';
import { ObjectId } from '../../../utils/helper';
import { handleUserAfterCancelOrder } from '../../api/order';
import { getTranslation, getUserLanguage } from '../../i18nCustom';
import notificationService, { NotifyRequestFactory } from '../../notification';
import { NOTIFY_TARGET_TYPE, NOTIFY_TYPE } from '../../notification/constants';
import { Topic } from '../constants';
import { updateProductQuantity } from '../function';

let orderRepository = OrderRepository.getInstance();
let paymentMethodRepo = PaymentMethodRepo.getInstance();
let productRepo = ProductRepo.getInstance();
let variantRepo = VariantRepo.getInstance();

const cancelPreOrderRunner = async () => {
    try {
        if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') || process.env.IS_SERVER_WOWZA) return;
        const dateSevenCompare = new Date(Date.now() - 7 * 24 * 60 * 60 * 1000);
        const dateFifteenCompare = new Date(
            Date.now() - 15 * 24 * 60 * 60 * 1000
        );
        const orders = await orderRepository.getPreOrderWithOrderShipping();

        let orderSevenDays = [];
        let orderFifteenDays = [];
        let orderCancel = [];
        orders.forEach((order) => {
            if (order?.order_items?.length) {
                orderFifteenDays.push(order);
            } else {
                orderSevenDays.push(order);
            }
        });

        orderSevenDays.forEach((order) => {
            if (order.created_at < dateSevenCompare) {
                orderCancel.push(order);
            }
        });
        orderFifteenDays.forEach((order) => {
            if (order.created_at < dateFifteenCompare) {
                orderCancel.push(order);
            }
        });

        const orderUpdate = {
            shipping_status: ShippingStatus.CANCELED,
            cancel_reason: 'Hệ thống tự động huỷ do hết hạn chuẩn bị hàng',
            cancel_type: CancelType.SYSTEM,
            cancel_by: null,
            cancel_time: new Date(),
        };

        let listOrderCancel: any = [];
        let listPromise: any = orderCancel.map((order) => {
            return new Promise(async (resolve, reject) => {
                await orderRepository.update(order._id, orderUpdate);
                listOrderCancel.push(order._id.toString());

                handleUserAfterCancelOrder(order.user_id)

                // send notification
                const language = await getUserLanguage(order.user_id);
                await notificationService.saveAndSend(
                    NotifyRequestFactory.generate(
                        NOTIFY_TYPE.ORDERS_CANCELED,
                        NOTIFY_TARGET_TYPE.BUYER,
                        {
                            title: getTranslation(
                                language,
                                'notification.bidu_notification'
                            ),
                            content: getTranslation(
                                language,
                                'notification.buyer_order.order_canceled',
                                order.order_number
                            ),
                            receiverId: order.user_id.toString(),
                            orderId: order._id.toString(),
                            userId: order.user_id.toString(),
                            orderNumber: order.order_number,
                        }
                    )
                );
                resolve(true);
            });
        });
        await Promise.all(listPromise);

        await updateProductQuantity(listOrderCancel);
    } catch (error) {
        console.log(error);
    }
};

const cancelPreOrder = cron.schedule(Topic.everyMinute, cancelPreOrderRunner);
export default cancelPreOrder;
