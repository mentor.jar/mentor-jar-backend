import cron from "node-cron";
import { TrackingObjectRepo } from "../../../repositories/TrackingObjectRepo";
import { InMemoryTrackingStore } from "../../../SocketStores/TrackingStore";
import TrackingService from "../../tracking";
import { Topic } from "../constants";

const trackingRepo = TrackingObjectRepo.getInstance();
const trackingCache = InMemoryTrackingStore.getInstance();

const saveTrackingJob = async () => {
  try {
    if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') || process.env.IS_SERVER_WOWZA) return;
    // job 
    console.info("RUNNING SAVE TRACKING FROM CACHE TO DATABASE");
    console.info("[1/2]: Get from cache")
    const trackings = await trackingCache.get("interactiveProducts")
    const promises = trackings.map(item => {
      return new Promise(async (resolve, reject) => {
        try {
          const { user_id, product_id, action_type } = item
          const trackingItem: any = await TrackingService._.userInteractiveProduct(
            user_id,
            product_id,
            action_type
          );
          resolve(trackingItem)
        } catch (error) {
          reject(error)
        }
      })
    })
    console.info("[2/2]: Save to database")
    await Promise.all(promises)
    trackingCache.save('interactiveProducts', [])
    return;
  } catch (error) {
    console.log("Error when update newest product cache: ", error.message);
  }
}

const saveTrackingFromCache = cron.schedule(
  Topic.everyOneHour,
  saveTrackingJob
);
export default saveTrackingFromCache;
