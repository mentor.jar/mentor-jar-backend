import cron from "node-cron";
import { topProductByCategory } from "../../../controllers/serviceHandles/product";
import { CategoryRepo } from "../../../repositories/CategoryRepo";
import { ProductRepo } from "../../../repositories/ProductRepo";
import { InMemoryProductStore } from "../../../SocketStores/ProductStore";
import { Topic } from "../constants";

const productRepo = ProductRepo.getInstance();
const productCache = InMemoryProductStore.getInstance();
const categoryRepo = CategoryRepo.getInstance();

const cacheTopProductByCategory = async () => {
  try {
    if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') || process.env.IS_SERVER_WOWZA) return;
    // job 
    console.info("RUNNING UPDATE TOP PRODUCT BY CATEGORY IN CACHE");
    console.info("[1/2]: Get from database")
    const topProductLists = await topProductByCategory().catch(error => {
      throw new Error(error)
    })
    console.log("topProductLists: ", topProductLists);

    console.info("[2/2]: Save to redis cache")
    const promises = topProductLists.map((item: any) => {
      return new Promise(async (resolve, reject) => {
        try {
          const data = await productCache.save(`cate_${item.categoryID}`, item.topProducts)
          resolve(data)
        } catch (error) {
          reject(error)
        }
      })
    })
    await Promise.all(promises).catch(error => {
      throw new Error(error)
    })

    return;
  } catch (error) {
    console.log("Error when update top product in cache: ", error.message);
  }
}

const recacheTopProductByCategory = cron.schedule(
  Topic.everyDay0AMAnd0PM,
  cacheTopProductByCategory
);
export default recacheTopProductByCategory;
