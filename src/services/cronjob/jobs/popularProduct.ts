import cron from "node-cron";
import { updateProductBodyShapeMark } from "../../../controllers/serviceHandles/product";
import { BodyShapeRepo } from "../../../repositories/BodyShapeRepo";
import { CategoryInfoRepo } from "../../../repositories/CategoryInfoRepo";
import { ProductDetailInfoRepo } from "../../../repositories/ProductDetailInfo";
import { ProductRepo } from "../../../repositories/ProductRepo";
import { ShopRepo } from "../../../repositories/ShopRepo";
import { Topic } from "../constants";

const productRepo = ProductRepo.getInstance();
const categortInfoRepo = CategoryInfoRepo.getInstance();
const productDetailRepo = ProductDetailInfoRepo.getInstance();
const shopRepo = ShopRepo.getInstance();
const bodyShapeRepo = BodyShapeRepo.getInstance();

const popularMarkJob = async () => {
  try {
    if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') || process.env.IS_SERVER_WOWZA) return;
    // job 
    console.info("RUNNING UPDATE PRODUCT MARK");
    console.info("[1/2]: Update popular mark")
    await productRepo.updatePopularMark();
    console.info("[2/2]: Update product body shape mark");
    await updateProductBodyShapeMark();
    console.info("Completed");
    return;
  } catch (error) {
    console.log("Error when update shop ranking: ", error.message);
  }
}

const reCalculatePopularMark = cron.schedule(
  Topic.everyDay0AM,
  popularMarkJob
);
export default reCalculatePopularMark;
