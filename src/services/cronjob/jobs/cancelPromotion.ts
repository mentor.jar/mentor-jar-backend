import cron from "node-cron";
import { ProductRepo } from "../../../repositories/ProductRepo";
import { PromotionProgramRepo } from "../../../repositories/PromotionProgramRepo";
import { VariantRepo } from "../../../repositories/Variant/VariantRepo";
import { cloneObj, ObjectId } from "../../../utils/helper";
import { Topic } from "../constants";

let productRepo = ProductRepo.getInstance();
let variantRepo = VariantRepo.getInstance();
let promotionProgramRepo = PromotionProgramRepo.getInstance();

const handleExpirePromotion = async () => {
    const promotions = await promotionProgramRepo.getExpirePromotion();
    let listProductId: any = [];

    // Update Promotion
    let listPromisePromotion: any = promotions.map(promotion => {
        return new Promise(async (resolve, reject) => {
            listProductId = listProductId.concat(promotion.products);
            await promotionProgramRepo.update(promotion._id, { is_reset: true });
            resolve(true);
        })
    })
    await Promise.all(listPromisePromotion);

    const listUniqueProductId: any = [...new Set(listProductId)];

    // Update Product
    let listPromiseProduct: any = listUniqueProductId.map(id => {
        return new Promise(async (resolve, reject) => {
            const product = await productRepo.findById(id);
            const instanceUpdate = {
                sale_price: product.before_sale_price ?? 0,
                $unset: {limit_sale_price_order: 1, sale_price_order_available: 1}
            }
            await productRepo.update(id.toString(), instanceUpdate);
            resolve(true);
        })
    })
    await Promise.all(listPromiseProduct);

    // Update variant
    const listVariant: any = await variantRepo.find({ product_id: { $in: listUniqueProductId } });

    let listPromiseVariant: any = listVariant.map(variant => {
        return new Promise(async (resolve, reject) => {
            const instanceUpdate = {
                sale_price: variant.before_sale_price ?? 0,
            }
            await variantRepo.update(variant._id, instanceUpdate);
            resolve(true);
        })
    })
    await Promise.all(listPromiseVariant)
}

const handleProductOutOfPromotionStock = async () => {
    const products = await productRepo.find({
        $and: [
            {
                limit_sale_price_order: { $exists: true }
            },
            {
                limit_sale_price_order: { $gt: 0}
            }
        ],
        sale_price_order_available: 0
    })
    
    const promises = products.map(product => {
        return new Promise(async (resolve, reject) => {
            let promotion:any = await promotionProgramRepo.findOne({
                products: ObjectId(product._id),
                is_reset: false,
                start_time: {$lte: new Date()},
                end_time: {$gte: new Date()}
            })
            if(product.sale_price !== product.before_sale_price && promotion) {
                const variants:Array<any> = await variantRepo.find({
                    product_id: ObjectId(product._id)
                })
                const newProduct = await productRepo.update(product._id, {
                    sale_price: product.before_sale_price || 0,
                    $unset: {limit_sale_price_order: 1, sale_price_order_available: 1}
                })
                const variantPromises = variants.map(variant => {
                    return new Promise(async (resolve, reject) => {
                        try {
                            variant = cloneObj(variant)
                            variant.sale_price = variant.before_sale_price
                            variant = await variantRepo.update(variant._id, variant)
                            resolve(variant)
                        } catch (error) {
                            reject(error.message)
                        }
                    })
                })
                // update variant
                await Promise.all(variantPromises)
                if(promotion) {
                    promotion = cloneObj(promotion)
                    const productList = promotion.products
                    const productItem = productList.find(item => item.toString() === product._id.toString())
                    if(productItem) {
                        const indexOfItem = productList.indexOf(productItem)
                        let newProList = [
                            ...productList.slice(0, indexOfItem),
                            ...productList.slice(indexOfItem + 1)
                        ]
                        await promotionProgramRepo.update(promotion._id, {
                            products: newProList
                        })
                    }
                }
                resolve(newProduct)
            }
            resolve(null)
        })
    })

    await Promise.all(promises).catch(error => {
        console.log("Err: ", error.message);
    })
    return;
}

const cancelPromotionRunner = async () => {
    try {
        if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') || process.env.IS_SERVER_WOWZA) return;
        // handle expire promotion
        await handleExpirePromotion()

        // handle product out of promotion stock
        await handleProductOutOfPromotionStock()

    } catch (error) {
        console.log(error);
    }
}

const cancelPromotion = cron.schedule(
    Topic.everyMinute,
    cancelPromotionRunner
);
export default cancelPromotion;
