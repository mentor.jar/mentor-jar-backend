import cron from "node-cron";
import SellerRankingService from "../../../controllers/serviceHandles/shop/sellerRanking";
import { ShopRepo } from "../../../repositories/ShopRepo";
import { Topic } from "../constants";

const shopRepo = ShopRepo.getInstance();

const rankingShopPolicy = async () => {
  try {
    if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') || process.env.IS_SERVER_WOWZA) return;
    // job
    console.log("RUNNING UPDATE SHOP RANKING BY POLICY: ");
    const shops = await shopRepo.find({
      is_approved: true
    })
    const promises = shops.map(shop => {
      return new Promise(async (resolve, reject) => {
        const result: { name, data } = await new SellerRankingService(shop).checkRankOfSeller()
        await shopRepo.update(shop._id, {
          rank_policy: {
            name: result?.name,
            data: result?.data
          }
        })
        resolve({
          _id: shop.id,
          rank: result
        })
      })
    })
    const data: any = await Promise.all(promises).catch(error => {
      console.log("error: ", error.message);
      return []
    });
    console.log("Updated: ", data.length);
  } catch (error) {
    console.log("Error when update shop ranking by policy: ", error.message);
  }
}

const rankingShopByPolicy = cron.schedule(
  Topic.everyDay1AM,
  rankingShopPolicy
);
export default rankingShopByPolicy;
