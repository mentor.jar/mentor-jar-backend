import cron from "node-cron";
import { PromotionProgramRepo } from "../../../repositories/PromotionProgramRepo";
import { Topic } from "../constants";

let promotionProgramRepo = PromotionProgramRepo.getInstance();

const startPromotionRunner = async () => {
    try {
        if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') || process.env.IS_SERVER_WOWZA) return;
        const promotions = await promotionProgramRepo.getStartPromotion();
        let listPromisePromotion: any = promotions.map(promotion => {
            return new Promise(async (resolve, reject) => {
                promotionProgramRepo.updateProductList(promotion.products, promotion);
                // Update promotion data to avoid some unexpected errors
                // if (!productIdsUpdated.length) {
                //     const updatePromotion = {
                //         ...promotion,
                //         is_valid: false,
                //         products: []
                //     }
                //     await promotionProgramRepo.update(promotion._id, updatePromotion)

                // }
                // else {
                //     const updatePromotion = {
                //         ...promotion,
                //         products: productIdsUpdated
                //     }
                //     await promotionProgramRepo.update(promotion._id, updatePromotion)

                // }
                resolve(true);
            })
        })
        await Promise.all(listPromisePromotion);
    } catch (error) {
        console.log(error);
    }
}

const startPromotion = cron.schedule(
    Topic.everyMinute,
    startPromotionRunner
);
export default startPromotion;