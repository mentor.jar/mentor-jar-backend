import cron from 'node-cron';
import { KEY } from '../../../constants/cache';
import { OrderRepository } from '../../../repositories/OrderRepo';
import { InMemoryPrepareOrderStore } from '../../../SocketStores/PrepareOrderStore';
import { Topic } from '../constants';

const orderRepo = OrderRepository.getInstance()
const prepareOrderStore = InMemoryPrepareOrderStore.getInstance();

const refreshPrepareOrder = async () => {
    try {
        if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') ||  process.env.IS_SERVER_WOWZA) return;

        const listPrepareOrders = await prepareOrderStore.getAllKeys(KEY.STORE_PREPARE_ORDER);
        Promise.all(
            listPrepareOrders.map(async (item) => {
                const newPrepareOrder = await orderRepo.getTimePrepareOrder(item)
                prepareOrderStore.set(KEY.STORE_PREPARE_ORDER, item, newPrepareOrder);
            })
        );

        return;
    } catch (error) {
        console.log('Error when update cache prepare order: ', error.message);
    }
};

const updatePrepareOrder = cron.schedule(
    Topic.everyDay1AM,
    refreshPrepareOrder
);

export default updatePrepareOrder;

