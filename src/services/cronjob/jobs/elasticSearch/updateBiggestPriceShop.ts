import { Topic } from './../../constants';
import cron from "node-cron";
import { cloneObj } from './../../../../utils/helper';
import { ShopRepo } from './../../../../repositories/ShopRepo';
import ProductService from "../../../../controllers/serviceHandles/product/service";

const productService = ProductService._;
const shopRepo = ShopRepo.getInstance();

const updateBiggestPriceProductShop = async () => {
    try {
        if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') ||  process.env.IS_SERVER_WOWZA) return;
        let shop: any = await shopRepo.getAllShopActive()
        shop = cloneObj(shop)
        const data = shop.map(item => {
            return new Promise(async (resolve) => {
                const biggestPriceProduct = await productService.getBiggestPriceProductByShop(item._id);
                if (biggestPriceProduct &&
                    biggestPriceProduct?.sort_price &&
                    biggestPriceProduct?.sort_price > 0 &&
                    biggestPriceProduct?.sort_price !== item?.biggest_price
                ) {
                    shopRepo.update(item._id, {
                        biggest_price: biggestPriceProduct.sort_price,
                    });
                }
                resolve(true)
            })
        })
        await Promise.all(data)
        return;
    } catch (error) {
        console.log('Error when update biggest price shop: ', error.message);
    }
}

const updateBiggestPriceShopRunner = () => updateBiggestPriceProductShop()

const updateBiggestPriceShop = cron.schedule(
    Topic.everyOneHour,
    updateBiggestPriceShopRunner
);

export default updateBiggestPriceShop;