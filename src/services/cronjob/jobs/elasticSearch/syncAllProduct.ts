import cron from "node-cron";
import { ProductRepo } from "../../../../repositories/ProductRepo";
import { Topic } from "../../constants";
import { ElasticCommand } from "../../../elastic/core";
import { elsIndexName } from "../../../elastic";
import { SCAN_MODE } from "../../../../constants/elasticsearch";
import _ from 'lodash';
import { formatDiscountPercentOrPriceProduct, isMappable } from "../../../../utils/helper";

const productRepo = ProductRepo.getInstance();
const elsCommand = ElasticCommand.getInstance();

const syncProduct = async (scanMode: SCAN_MODE) => {
    try {
        if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') ||  process.env.IS_SERVER_WOWZA) return;
        console.time("prepare data")
        let  products :any = await productRepo.getALlProductSyncELS(scanMode);
        console.timeEnd("prepare data")

        console.log("products:", products.length);

        const chunks = _.chunk(products, 500);

        console.log("chunks:", chunks.length);

        let chunk_count = 1;
        for (let chunk of chunks) {
            console.log("start chunk -> ", chunk_count)
            let product_count = 1;
            for (let product of chunk) {
                const id = product._id;
                delete product._id;
                product.time_prepare_orders = product?.time_prepare_orders?.map((item) => {
                    item.value = item.value.toFixed(3);
                    return item;
                })

                const numberMember = product?.group_buy?.groups?.map((item) => item?.number_of_member);
                product.minimum_member_group = 0;
                if (numberMember && isMappable(numberMember)) {
                    product.minimum_member_group = Math.min(...product?.group_buy?.groups?.map((item) => item?.number_of_member));
                }

                if(product?.group_buy && (product?.option_types && isMappable(product.option_types))) {
                    let variantArr = product.group_buy.groups[0].variants.map((value) => value.variant_id)
                    let variant = product.variants.filter((value) => variantArr.includes(value._id))
                    if (variant && isMappable(variant)) {
                        let colorArr = variant.map((value) => value.option_values[0].name)
                        let sizeArr = variant.map((value) => value.option_values[1].name)
                        product.option_types_group_buy = product.option_types
                        product.option_types_group_buy[0].option_values =
                            product.option_types_group_buy[0].option_values.filter(
                                (value) => colorArr.includes(value.name)
                            );
                        product.option_types_group_buy[1].option_values =
                            product.option_types_group_buy[1].option_values.filter(
                                (value) => sizeArr.includes(value.name)
                            );
                    }
                }

                if(product?.group_buy && isMappable(product?.group_buy.groups)) {
                    product.group_buy.groups = product.group_buy.groups.map(value => {
                        if(value.group_buy_price) {
                            value.group_buy_price_hidden = formatDiscountPercentOrPriceProduct(value.group_buy_price)
                        } else {
                            value.variants = value.variants.map(variant => {
                                variant.group_buy_price_hidden = formatDiscountPercentOrPriceProduct(variant.group_buy_price)
                                return variant
                            })
                        }
                        return value
                    })
                }

                if(product?.order_rooms && isMappable(product?.order_rooms)) {
                    product.order_rooms = product.order_rooms.filter(value => value.is_valid === true)
                }

                product.sort_price = product?.price_min_max?.min ? product?.price_min_max?.min : product?.sale_price;
                product.stocking = product?.quantity > 0 ? true : false; 

                try {
                    await elsCommand.updateById(elsIndexName.product, id, product);
                    console.log("Chunk - " + chunk_count + " - product - " + product_count + " - index - " + id );
                } catch (error) {
                    console.log('Sync Product failed - '+ id + '  :'+ error)
                }
                product_count++;
            }
            chunk_count ++;
        }

        console.log("sync done !!!")
        return;
    } catch (error) {
        console.log(error);
    }
}

// const syncProduct20SecondsRunner = () => syncProduct(SCAN_MODE.FIVE_MINUTE)
// const syncProduct10MinutesRunner = () => syncProduct(SCAN_MODE.HOURLY)
const syncProductDailyRunner = () => syncProduct(SCAN_MODE.ALL)

const syncProductDaily = cron.schedule(
    Topic.everyDay2AM,
    syncProductDailyRunner
);

export default syncProductDaily;

