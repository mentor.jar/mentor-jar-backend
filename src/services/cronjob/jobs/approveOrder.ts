import cron from "node-cron";
import { AdHoc, TargetType } from "../../../models/enums/TrackingObject";
import { OrderRepository } from "../../../repositories/OrderRepo";
import { PaymentMethodRepo } from "../../../repositories/PaymentMethodRepo";
import { SystemSettingRepo } from "../../../repositories/SystemSettingRepo";
import { TrackingObjectRepo } from "../../../repositories/TrackingObjectRepo";
import { UserRepo } from "../../../repositories/UserRepo";
import { isMappable, ObjectId } from "../../../utils/helper";
import { getTranslation, getUserLanguage } from "../../i18nCustom";
import notificationService, { NotifyRequestFactory } from "../../notification";
import { NOTIFY_TARGET_TYPE, NOTIFY_TYPE } from "../../notification/constants";
import { Topic } from "../constants";

const userRepo = UserRepo.getInstance();
const orderRepository = OrderRepository.getInstance()
const systemSetting  = SystemSettingRepo.getInstance();
const paymentMethodRepo = PaymentMethodRepo.getInstance();
const trackingObjectRepo = TrackingObjectRepo.getInstance();

const approveOrderRunner = async () => {
    try {
        if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') || process.env.IS_SERVER_WOWZA) return;
        const settingApproveOrder: any = await systemSetting.getOrCreateSystemSetting();
        if (!settingApproveOrder?.approved_order) return;
        let paymentMethods: any = await paymentMethodRepo.findOne({ name: 'CASH' });
        const dateCompare: any = new Date(Date.now() - 24 * 60 * 60 * 1000);
        const orders = await orderRepository.find({
            payment_status: 'pending',
            shipping_status: 'pending',
            payment_method_id: ObjectId(paymentMethods?._id),
            created_at: { $gte: new Date(2021, 10, 18), $lte: dateCompare},
        });

        if (!isMappable(orders)) return;

        let listOrderApprove: any = [];
        let listPromise: any = orders.map(order => {
            return new Promise(async (resolve, reject) => {
                const orderUpdate = {
                    shipping_status: "wait_to_pick",
                    approved_time: new Date()
                }
                await orderRepository.update(order._id, orderUpdate);
                listOrderApprove.push(order._id.toString());
                // send notification
                const language = await getUserLanguage(order.user_id);
                const languageSeller = await getUserLanguage(order.pick_address.accessible_id);
                await notificationService.saveAndSend(
                    NotifyRequestFactory.generate(
                        NOTIFY_TYPE.ORDERS_ACCEPT,
                        NOTIFY_TARGET_TYPE.BUYER,
                        {
                            title: getTranslation(language, 'notification.bidu_notification'),
                            content: getTranslation(language, 'notification.order_accepted', order.order_number),
                            receiverId: order.user_id.toString(),
                            orderId: order._id.toString(),
                            userId: order.user_id.toString(),
                            orderNumber: order.order_number,
                        }
                    ))

                await notificationService.saveAndSend(
                    NotifyRequestFactory.generate(
                        NOTIFY_TYPE.ORDERS_ACCEPT,
                        NOTIFY_TARGET_TYPE.SELLER,
                        {
                            title: getTranslation(
                                languageSeller,
                                'notification.bidu_notification'
                            ),
                            content: getTranslation(
                                languageSeller,
                                'notification.seller_order.order_accepted',
                                order.order_number
                            ),
                            receiverId: order.pick_address.accessible_id,
                            orderId: order._id.toString(),
                            userId: order.pick_address.accessible_id,
                            orderNumber: order.order_number,
                        }
                    )
                );

                 // Insert Table TrackingObject Seller
                const userShop = await userRepo.getUserByShopId(order.shop_id);
                // await trackingObjectRepo.createOrUpdateTrackingObject(userShop._id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);
                    // Insert Table TrackingObject Buyer
                // await trackingObjectRepo.createOrUpdateTrackingObject(order.user_id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);

                resolve(true);
            })
        })
        await Promise.all(listPromise)

    } catch (error) {
        console.log(error);
    }
}

const approveOrder = cron.schedule(
    Topic.everyMinute,
    approveOrderRunner
);

export default approveOrder;

