import cron from "node-cron";
import { CancelType, ShippingStatus } from "../../../models/enums/order";
import { OrderRepository } from "../../../repositories/OrderRepo";
import { ProductRepo } from "../../../repositories/ProductRepo";
import { ObjectId } from "../../../utils/helper";
import FirebaseStoreService from "../../firebase";
import { TableName } from "../../firebase/constant";
import { getTranslation, getUserLanguage } from "../../i18nCustom";
import notificationService, { NotifyRequestFactory } from "../../notification";
import { NOTIFY_TARGET_TYPE, NOTIFY_TYPE } from "../../notification/constants";
import { Topic } from "../constants";
import { updateQuantityProductGroupBuy } from "../function";

let orderRepository = OrderRepository.getInstance();
let productRepo = ProductRepo.getInstance();
const firestoreService = FirebaseStoreService._();

const time_limit = 48 // 48h time config cancel order - need add to system setting

const filterRoomHasIncomplete = (product) => {
  return product.order_rooms.filter(room => room.status === 'pending' && room.members.length !== findNumberOfMember(product, room.group_id))
}

const findNumberOfMember = (product, group_id) => {
  if (!product.group_buy) return;
  return product.group_buy?.groups.find(group => group._id.toString() === group_id.toString())?.number_of_member;
}

const handlerCancelOrderWithRoom = async (product) => {
  if (!product) return;

  const orderRooms = filterRoomHasIncomplete(product);

  // cancel order in member
  let listOrders: any = await Promise.all(
      orderRooms.map(async (orderRoom) => 
          Promise.all(
              orderRoom.members.map(async (orderMember) =>
                  handlerCancelOrder(orderMember)
              )
          )
      )
  );

  listOrders = listOrders.flatMap((order) => order)

  // add orderRooms to firebase log
  saveToLogCancelOrder(product._id.toString(), orderRooms);

  // roll back quantity
  updateQuantityProductGroupBuy(product, orderRooms, listOrders);
}


const handlerCancelOrder = async (member) => {
  const orderId = member?.order_info?.order_id;
  const order: any = await orderRepository.findOne({
    _id: ObjectId(orderId)
  });

  if (!order) return;

  const orderUpdate = {
    shipping_status: ShippingStatus.CANCELED,
    cancel_reason: "Hệ thống tự động huỷ do nhóm thanh toán không đủ người.",
    cancel_type: CancelType.SYSTEM,
    cancel_by: null,
    cancel_time: new Date()
  }

  const language = await getUserLanguage(order.user_id);
  await notificationService.saveAndSend(
    NotifyRequestFactory.generate(
      NOTIFY_TYPE.ORDERS_CANCELED,
      NOTIFY_TARGET_TYPE.BUYER,
      {
        title: getTranslation(language, 'notification.bidu_notification'),
        content: getTranslation(language, 'notification.buyer_order.order_canceled', order.order_number),
        receiverId: order.user_id.toString(),
        orderId: order._id.toString(),
        userId: order.user_id.toString(),
        orderNumber: order.order_number,
      }
    ))

  await orderRepository.update(order._id, orderUpdate);

  return orderId;
}

const saveToLogCancelOrder = (productId, orderRooms) => {
  const data = {
  product_id: productId,
  order_rooms: JSON.stringify(orderRooms),
  created_at: new Date()
  
  };
  
  return firestoreService.save(TableName.GB_JOB_CANCEL_48H, data)
  
  }

const cancelOrderGroupBuyRunner = async () => {
  try {
    // if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') || process.env.SERVER_WOWZA === 'production') return;
    // find product groupbuy running
    const dateCompare = new Date(Date.now() - time_limit * 60 * 60 * 1000);
    const productGBs = await productRepo.findProductGroupbuy(dateCompare);

    // find roomcancel in product with: (host join gt 48h + not enough member)
    productGBs.forEach(product => handlerCancelOrderWithRoom(product))
  } catch (error) {
    console.log(error);
  }
}

const cancelGroupBuyOrder = cron.schedule(
  Topic.everyMinute,
  cancelOrderGroupBuyRunner
);
export default cancelGroupBuyOrder;
