import cron from "node-cron";
import JobHisoryRepo from '../../../repositories/JobHisoryRepo';
import NotifyCampaignRepo from "../../../repositories/NotifyCampaignRepo";
import { ObjectId } from "../../../utils/helper";
import notificationService from '../../notification/index';
import { Topic } from "../constants";

const advanceTimeLimit = 1 * 60; // seconds
const jobName = 'remindPushNotification'; // seconds

export const remindPushNotificationRunner = async () => {
    try {
        if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') || process.env.IS_SERVER_WOWZA) return;
        console.log('*** START ' + jobName)
        const jobHistoryRepo = JobHisoryRepo.getInstance();
        const notifyCampaignRepo = NotifyCampaignRepo.getInstance();
        const now = Date.now();
        const advanceTime = now + advanceTimeLimit * 1000;

        let notifyCampaigns = await notifyCampaignRepo.find({
            timeWillPush: {
                $gte: new Date(now),
                $lte: new Date(advanceTime)
            }
        });

        const ranJob: Array<any> = await jobHistoryRepo.find({
            jobName,
            objectType: 'notifycampaigns',
            objectIds: { $elemMatch: { $in: notifyCampaigns.map(e => ObjectId(e._id)) } }
        });

        const ranIds: Array<any> = ranJob.reduce((list, e) => [...list, ...e.objectIds.map(e => e.toString())], []);

        notifyCampaigns = notifyCampaigns.filter(notify => !ranIds.includes(notify._id.toString()))

        if (notifyCampaigns.length == 0) {
            console.log('*** STOP ' + jobName, ' because of empty data')
            return;
        }

        const sendBatch = notifyCampaigns.map((notify) => notificationService.send({
            receiverId: notify.receivers || "",
            title: notify.title,
            content: notify.content,
            type: notify.type,
            image: notify.image,
            data: notify.data
        }))

        await Promise.all(sendBatch);

        await jobHistoryRepo.create({
            jobName,
            objectType: 'notifycampaigns',
            objectIds: notifyCampaigns.map(notify => notify._id),
        })

        // console.log('*** RAN ' + jobName)
        return;
    } catch (error) {
        console.log(error);
    }
}

const remindPushNotification = cron.schedule(
    Topic.everyThirtySeconds,
    remindPushNotificationRunner
);

export default remindPushNotification;