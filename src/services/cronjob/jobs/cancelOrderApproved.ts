import cron from "node-cron";
import { CancelType, ShippingStatus } from "../../../models/enums/order";
import { OrderItemRepository } from "../../../repositories/OrderItemRepo";
import { OrderRepository } from "../../../repositories/OrderRepo";
import { getTranslation, getUserLanguage } from "../../i18nCustom";
import notificationService, { NotifyRequestFactory } from "../../notification";
import { NOTIFY_TARGET_TYPE, NOTIFY_TYPE } from "../../notification/constants";
import { Topic } from "../constants";
import { updateProductQuantity } from "../function";

let orderRepository = OrderRepository.getInstance();

const cancelOrderApprovedRunner = async () => {
    try {
        if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') || process.env.IS_SERVER_WOWZA) return;
        const dateCompare = new Date(Date.now() - (240 + 15) * 60 * 60 * 1000); // update 3days ->  10days
        const orders = await orderRepository.getOrderWithOrderShipping(dateCompare);

        const orderUpdate = {
            shipping_status: ShippingStatus.CANCELED,
            cancel_reason: "Hệ thống tự động huỷ do hết thời gian chuẩn bị hàng",
            cancel_type: CancelType.SYSTEM,
            cancel_by: null,
            cancel_time: new Date()
        }

        let listOrderCancel: any = [];
        let listPromise: any = orders.map(order => {
            return new Promise(async (resolve, reject) => {
                if (!order.order_shipping.assign_to_shipping_unit_time) {
                    await orderRepository.update(order._id, orderUpdate);
                    listOrderCancel.push(order._id.toString());
                    // send notification
                    const language = await getUserLanguage(order.user_id);
                    await notificationService.saveAndSend(
                        NotifyRequestFactory.generate(
                            NOTIFY_TYPE.ORDERS_CANCELED,
                            NOTIFY_TARGET_TYPE.BUYER,
                            {
                                title: getTranslation(language, 'notification.bidu_notification'),
                                content: getTranslation(language, 'notification.buyer_order.order_canceled', order.order_number),
                                receiverId: order.user_id.toString(),
                                orderId: order._id.toString(),
                                userId: order.user_id.toString(),
                                orderNumber: order.order_number,
                            }
                        ))
                }
                resolve(true);
            })
        })
        await Promise.all(listPromise)

        await updateProductQuantity(listOrderCancel);

    } catch (error) {
        console.log(error);
    }
}

const cancelOrderApproved = cron.schedule(
    Topic.everyDay15PM,
    cancelOrderApprovedRunner,
    {
        scheduled: true,
        timezone: "Asia/Ho_Chi_Minh"
    }
);
export default cancelOrderApproved;
