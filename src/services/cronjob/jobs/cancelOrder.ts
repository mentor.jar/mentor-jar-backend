import cron from "node-cron";
import { CancelType, ShippingStatus } from "../../../models/enums/order";
import { OrderRepository } from "../../../repositories/OrderRepo";
import { PaymentMethodRepo } from "../../../repositories/PaymentMethodRepo";
import { cloneObj, isMappable } from "../../../utils/helper";
import { handleUserAfterCancelOrder } from "../../api/order";
import { getTranslation, getUserLanguage } from "../../i18nCustom";
import notificationService, { NotifyRequestFactory } from "../../notification";
import { NOTIFY_TARGET_TYPE, NOTIFY_TYPE } from "../../notification/constants";
import { Topic } from "../constants";
import { updateProductQuantity, updateQuantityProductGroupBuyPaymentFailed } from "../function";

let orderRepository = OrderRepository.getInstance();
let paymentMethodRepo = PaymentMethodRepo.getInstance();

const cancelOrderRunner = async () => {
    try {
        if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') || process.env.IS_SERVER_WOWZA) return;
        const paymentMethods = await paymentMethodRepo.find({ name: { $in: ['VNPAY', 'MOMO'] } });
        const paymentMethodIds = paymentMethods.map(paymentMethod => paymentMethod._id);
        const dateCompare = new Date(Date.now() - 24 * 60 * 60 * 1000);
        let orders = await orderRepository.find(
            {
                $and: [
                    { payment_status: 'pending' },
                    { payment_method_id: { $in: paymentMethodIds } },
                    { shipping_status: { $ne: 'canceled' } },
                    { created_at: { $lte: dateCompare } }
                ]
            });
        orders = cloneObj(orders);
        const normalOrder = [];
        const groupBuyOrder = [];

        const orderUpdate = {
            shipping_status: ShippingStatus.CANCELED,
            cancel_reason: "Hệ thống tự động huỷ do hết hạn thanh toán",
            cancel_type: CancelType.SYSTEM,
            cancel_by: null,
            cancel_time: new Date()
        }

        let listPromise: any = orders.map(order => {
            return new Promise(async (resolve, reject) => {
                await orderRepository.update(order._id, orderUpdate);
                order?.is_group_buy_order
                    ? groupBuyOrder.push(order._id.toString())
                    : normalOrder.push(order._id.toString());
                
                handleUserAfterCancelOrder(order.user_id)
                
                // send notification
                const language = await getUserLanguage(order.user_id);
                await notificationService.saveAndSend(
                    NotifyRequestFactory.generate(
                        NOTIFY_TYPE.ORDERS_CANCELED,
                        NOTIFY_TARGET_TYPE.BUYER,
                        {
                            title: getTranslation(language, 'notification.bidu_notification'),
                            content: getTranslation(language, 'notification.buyer_order.order_canceled', order.order_number),
                            receiverId: order.user_id.toString(),
                            orderId: order._id.toString(),
                            userId: order.user_id.toString(),
                            orderNumber: order.order_number,
                        }
                    ))
                resolve(true);
            })
        })
        await Promise.all(listPromise);

        if (isMappable(normalOrder)) {
            await updateProductQuantity(normalOrder);
        }

        if (isMappable(groupBuyOrder)) {
            await updateQuantityProductGroupBuyPaymentFailed(groupBuyOrder);
        }  

    } catch (error) {
        console.log(error);
    }
}

const cancelOrder = cron.schedule(
    Topic.everyTenMinute,
    cancelOrderRunner
);
export default cancelOrder;
