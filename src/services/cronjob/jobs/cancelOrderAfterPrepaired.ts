import cron from "node-cron";
import { updateQuantityCancelOrder } from "../../../controllers/api/mobile_app/orders";
import { CancelType, ShippingStatus } from "../../../models/enums/order";
import { OrderRepository } from "../../../repositories/OrderRepo";
import { OrderShippingRepo } from "../../../repositories/OrderShippingRepo";
import GHTKService from "../../3rd/shippings/GHTK";
import { handleUserAfterCancelOrder } from "../../api/order";
import { getTranslation, getUserLanguage } from "../../i18nCustom";
import notificationService, { NotifyRequestFactory } from "../../notification";
import { NOTIFY_TARGET_TYPE, NOTIFY_TYPE } from "../../notification/constants";
import { Topic } from "../constants";

const orderShippingRepo = OrderShippingRepo.getInstance();
const orderRepo = OrderRepository.getInstance();

//Auto cancel order after 1 days at 15h from seller prepaired order if shipping unit not picked
const cancelOrderAfterSellerPrepaired = async () => {
  try {
    if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') || process.env.IS_SERVER_WOWZA) return;
    // job 
    const cronService = new CronCancelOrderService();
    const orders = await cronService.getOrderNotPickUp();
    const fetchPromises = orders.map(async (order) => {
      let orderNumber = order.shipping_info.partner_id;
      let cancelOrder = await cronService.cancelOrderNotPickUp(orderNumber);
      if (cancelOrder) {
        const result = await GHTKService.getInstance().cancelOrderGHTK(orderNumber);
        return;
      }
    })
    await Promise.all(fetchPromises)
    return;
  } catch (error) {
    console.log(error);
  }
}

class CronCancelOrderService {

  getOrderNotPickUp = async () => {
    let orderNotPickUp = await orderShippingRepo.getOrderGHTKCannotPickUp()
    return orderNotPickUp;
  }

  cancelOrderNotPickUp = async (orderNumber) => {
    return new Promise(async (resolve, reject) => {
      try {
        const order = await orderRepo.findOrderByOrderNumber({ order_number: orderNumber });

        if (!order || order?.shipping_status !== ShippingStatus.WAIT_TO_PICK) return resolve(false);
        // update cancel order
        order.shipping_status = ShippingStatus.CANCELED;
        order.cancel_reason = "Hệ thống tự động huỷ do đơn vị vận chuyển không lấy hàng";
        order.cancel_type = CancelType.SYSTEM;
        order.cancel_time = new Date();

        order.save();
        // update quantity cancel
        await updateQuantityCancelOrder(order._id);

        handleUserAfterCancelOrder(order.user_id)

        // send notification for buyer
        const languageBuyer = await getUserLanguage(order.user_id);
        await notificationService.saveAndSend(
          NotifyRequestFactory.generate(
            NOTIFY_TYPE.ORDERS_CANCELED,
            NOTIFY_TARGET_TYPE.BUYER,
            {
              title: getTranslation(languageBuyer, 'notification.bidu_notification'),
              content: getTranslation(languageBuyer, `notification.buyer_order.order_canceled`, order.order_number),
              receiverId: order.user_id.toString(),
              orderId: order._id.toString(),
              userId: order.user_id.toString(),
              orderNumber: order.order_number,
            }
          ))

        // send notification for seller
        const languageSeller = await getUserLanguage(order.pick_address.accessible_id);
        await notificationService.saveAndSend(
          NotifyRequestFactory.generate(
            NOTIFY_TYPE.ORDERS_CANCELED,
            NOTIFY_TARGET_TYPE.SELLER,
            {
              title: getTranslation(languageSeller, 'notification.bidu_notification'),
              content: getTranslation(languageSeller, `notification.seller_order.order_canceled`, order.order_number),
              receiverId: order.pick_address.accessible_id.toString(),
              orderId: order._id.toString(),
              userId: order.pick_address.accessible_id.toString(),
              orderNumber: order.order_number,
            }
          ))
        return resolve(true);
      } catch (error) {
        reject(error)
      }
    })
  }
}

const autoCancelAfterSellerPrepaired = cron.schedule(
  Topic.everyDay15PM,
  cancelOrderAfterSellerPrepaired,
  {
    scheduled: true,
    timezone: "Asia/Ho_Chi_Minh"
  }
);
export default autoCancelAfterSellerPrepaired;