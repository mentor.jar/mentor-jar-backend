import cron from "node-cron";
import { InMemoryTrackingStore } from "../../../SocketStores/TrackingStore";
import { Topic } from "../constants";

const trackingCache = InMemoryTrackingStore.getInstance();

const reCalculateTotalAccess = async () => {
  try {
    if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') || process.env.IS_SERVER_WOWZA) return;
    // job 
    const currentNumber = await trackingCache.get('accessTotal')
    const newAccess = Math.floor(Math.random() * (300 - 100 + 1) + 100);
    trackingCache.save('accessTotal', +currentNumber + newAccess)
    return;
  } catch (error) {
    console.log("Error when update shop ranking: ", error.message);
  }
}

const autoReCalculateTotalAccess = cron.schedule(
  Topic.everyDay0AM,
  reCalculateTotalAccess
);
export default autoReCalculateTotalAccess;
