import moment from "moment";
import cron from "node-cron";
import { InMemoryTrackingStore } from "../../../SocketStores/TrackingStore";
import { Topic } from "../constants";

const trackingCache = InMemoryTrackingStore.getInstance();

const clearAccessCache = async () => {
  try {
    if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') || process.env.IS_SERVER_WOWZA) return;
    // job 
    const last30Minutes = moment.utc(new Date()).add(-30, 'minute')
    const currentData: Array<any> = await trackingCache.get('accessAnalyst') || []
    const accessByTime = currentData.map(strTime => moment(strTime))
    const validAccess = accessByTime.filter(item => last30Minutes.isSameOrBefore(item))
    const newData = validAccess.map(item => item.toDate())
    trackingCache.save('accessAnalyst', newData)
    return;
  } catch (error) {
    console.log("Error when update shop ranking: ", error.message);
  }
}

const autoClearAccessCache = cron.schedule(
  Topic.everyOneHour,
  clearAccessCache
);
export default autoClearAccessCache;
