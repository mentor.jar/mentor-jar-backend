import cron from "node-cron";
import { Topic } from "../constants";
import _ from "lodash"
import { InMemoryVoucherStore } from "../../../SocketStores/VoucherStore";
import VoucherService from "../../../controllers/serviceHandles/voucher";

const voucherAndOrderCache = InMemoryVoucherStore.getInstance();

const clearVoucherHistoryByDay = async () => {
    try {
        if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') || process.env.IS_SERVER_WOWZA) return;
        const keys = await voucherAndOrderCache.keys('voucherHistoryDay_*')
        keys.map(key => {
            voucherAndOrderCache.delKey(key)
        })

        console.log("clearVoucherHistoryByDay done");

        // re-sync voucher to cache
        VoucherService._.syncVoucherSystem()
        console.log("clear & re-sync voucher system done");
        
    } catch (error) {
        console.log(error);
    }
}

const clearVoucherHistoryByDayJob = cron.schedule(
    Topic.everyDay0AM,
    clearVoucherHistoryByDay
);
export default clearVoucherHistoryByDayJob;