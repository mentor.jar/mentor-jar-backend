import moment from "moment";
import cron from "node-cron";
import { ShopRepo } from "../../../repositories/ShopRepo";
import { Topic } from "../constants";

const shopRepo = ShopRepo.getInstance();

const rankingShop = async () => {
  try {
    if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') || process.env.IS_SERVER_WOWZA) return;
    // job 
    const toDate = moment().set({ hour: 23, minute: 59, second: 59 }).toDate()
    const fromDate = moment().subtract(6, 'days').set({ hour: 0, minute: 0, second: 0 }).toDate()
    await shopRepo.updateShopRanking(fromDate, toDate);
    return;
  } catch (error) {
    console.log("Error when update shop ranking: ", error.message);
  }
}

const autoRankingShop = cron.schedule(
  Topic.every0AMonMonday,
  rankingShop
);
export default autoRankingShop;
