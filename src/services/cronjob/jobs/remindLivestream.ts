import cron from "node-cron";
import JobHisoryRepo from '../../../repositories/JobHisoryRepo';
import { StreamSessionRepository } from '../../../repositories/StreamSessionRepository';
import { ObjectId } from "../../../utils/helper";
import { NotifyType } from '../../notification';
import notificationService from '../../notification/index';
import { Topic } from "../constants";

const advanceTimeLimit = 30 * 60; // seconds
const jobName = 'remindLivestream'; // seconds

export const remindLivestreamRunner = async () => {
    try {
        if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') || process.env.IS_SERVER_WOWZA) return;
        // console.log('*** START ' + jobName)
        const jobHistoryRepo = JobHisoryRepo.getInstance();
        const streamSessionRepository = StreamSessionRepository.getInstance();
        const now = Date.now();
        const advanceTime = now + advanceTimeLimit * 1000;

        let streamSessions = await streamSessionRepository.find({
            time_will_start: {
                $gte: new Date(now),
                $lte: new Date(advanceTime)
            }
        });

        const ranJob: Array<any> = await jobHistoryRepo.find({
            jobName,
            objectType: 'streamsessions',
            objectIds: { $elemMatch: { $in: streamSessions.map(e => ObjectId(e._id)) } }
        });

        const ranIds: Array<any> = ranJob.reduce((list, e) => [...list, ...e.objectIds.map(e => e.toString())], []);

        streamSessions = streamSessions.filter(session => !ranIds.includes(session._id.toString()))

        if (streamSessions.length == 0) {
            console.log('*** STOP ' + jobName, ' because of empty data')
            return;
        }

        const sendBatch = streamSessions.map((session) => notificationService.saveAndSend({
            receiverId: session.user_id.toString(),
            content: 'Bạn có lịch cho livestream' + session.name + ' trong 30 phút sắp tới',
            title: 'Bidu Livestream',
            type: NotifyType.NOTIFY_TYPE.EVENTS,
            contentType: NotifyType.NOTIFY_CONTENT_TYPE.LIVESTREAM,
            image: session.image
        }))

        await Promise.all(sendBatch);

        await jobHistoryRepo.create({
            jobName,
            objectType: 'streamsessions',
            objectIds: streamSessions.map(session => session._id),
        })

        // console.log('*** RAN ' + jobName)
        return;
    } catch (error) {
        console.log(error);
    }
}

const remindLivestream = cron.schedule(
    Topic.everyFiveMinute,
    remindLivestreamRunner
);

export default remindLivestream;