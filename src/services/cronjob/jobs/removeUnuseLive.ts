import cron from "node-cron";
import { StreamSessionRepository } from "../../../repositories/StreamSessionRepository";
import { Topic } from "../constants";

let streamSessionRepo = StreamSessionRepository.getInstance();

const removeUnuseRunner = async () => {
    try {
        if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') || process.env.IS_SERVER_WOWZA) return;

        // 1 days
        const dateCompare = new Date(Date.now() - 1 * 24 * 60 * 60 * 1000);

        const listStream = await streamSessionRepo.find(
            { time_start: null, time_end: null, active: { $ne: true }, created_at: { $lte: dateCompare } }
        )
        let listPromise: any = listStream.map(live => {
            return new Promise(async (resolve, reject) => {
                await streamSessionRepo.getModel().deleteOne({ _id: live._id.toString() });
                resolve(true);
            })
        })

        await Promise.all(listPromise);
    } catch (error) {
        console.log(error);
    }
}

const removeUnuseLive = cron.schedule(
    Topic.everyDay15PM,
    removeUnuseRunner,
    {
        scheduled: true,
        timezone: "Asia/Ho_Chi_Minh"
    }
);
export default removeUnuseLive;
