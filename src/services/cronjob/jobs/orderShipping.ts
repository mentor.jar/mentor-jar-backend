import cron from "node-cron";
import { OrderShippingRepo } from "../../../repositories/OrderShippingRepo";
import { Topic } from "../constants";

const orderShippingRepo = OrderShippingRepo.getInstance();

const updateOrderAndShipping = async () => {
  try {
    if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') || process.env.IS_SERVER_WOWZA) return;
    // job 
    const job = await orderShippingRepo.updateOrderAndOrderShippedHistory();
    return;
  } catch (error) {
    console.log(error);
  }
}

const autoShippedOrderAfter3Days = cron.schedule(
  Topic.everyMinute,
  updateOrderAndShipping
);
export default autoShippedOrderAfter3Days;