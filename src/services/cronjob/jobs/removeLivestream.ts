import cron from "node-cron";
import { StreamSessionRepository } from "../../../repositories/StreamSessionRepository";
import { Topic } from "../constants";
import fs from "fs";
import streamSession from "../../../models/Stream_Session";
import rimraf from "rimraf";

let streamSessionRepo = StreamSessionRepository.getInstance();

const removeLivestreamRunner = async () => {
    try {
        if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') || process.env.IS_SERVER_WOWZA) return;
        const recentStreamSessions = await streamSessionRepo.getRecentStreamSessions();
        const streamSessionIds = recentStreamSessions.map(el => el._id.toString());
        // 7 days
        const dateCompare = new Date(Date.now() - 7 * 24 * 60 * 60 * 1000);

        const listStream = await streamSessionRepo.find(
            { _id: { $nin: streamSessionIds }, active: { $ne: true }, time_start: { $lte: dateCompare } }
        )
        const server = process.env.NODE_ENV === 'production' ? 'production' : 'staging';
        const dir = `uploads/video-live-streams/${server}`;
        let listPromise: any = listStream.map(live => {
            return new Promise(async (resolve, reject) => {
                const newDir = `${dir}/${live.user_id}/${live._id}`;
                rimraf.sync(newDir);
                await streamSessionRepo.getModel().deleteOne({ _id: live._id.toString() });
                resolve(true);
            })
        })
        await Promise.all(listPromise);
    } catch (error) {
        console.log(error);
    }
}

const removeLivestream = cron.schedule(
    Topic.everyTenMinute,
    removeLivestreamRunner
);
export default removeLivestream;
