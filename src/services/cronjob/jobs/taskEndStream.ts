import cron from "node-cron";
import { getDetailStreamSession } from "../../../controllers/serviceHandles/streams";
import { StreamSessionRepository } from "../../../repositories/StreamSessionRepository";
import { DisconnectTime, Topic } from "../constants";

let streamSessionRepository = StreamSessionRepository.getInstance();

const taskEndStreamRunner = async () => {
    try {
        if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') || process.env.IS_SERVER_WOWZA) return;
        const streamSessions = await streamSessionRepository.find({ active: true });
        let listPromise: any = streamSessions.map(session => {
            return new Promise((resolve, reject) => {
                getDetailStreamSession(`${session.user_id}.stream`).then(data => {
                    if (!data) {
                        if (session.check_disconnect == DisconnectTime.TwoTime) {
                            // console.log('*** Start stop stream of user ', session.user_id, ' due to inactive');
                            streamSessionRepository.stopStreamSession(session._id)
                                .finally(() => {
                                    // console.log('*** Stop done')
                                    resolve(true)
                                })
                        } else {
                            // console.log('One time');
                            const check = session.check_disconnect + 1;
                            streamSessionRepository.update(session._id, { check_disconnect: check })
                                .finally(() => {
                                    // console.log('check :', check);
                                    resolve(true)
                                })
                        }

                    } else {
                        // console.log('Did NOT find any streams');
                        resolve(true)
                    }
                })

            })
        })
        await Promise.all(listPromise)
    } catch (error) {
        console.log(error);
    }
}

const taskEndStream = cron.schedule(
    Topic.everyMinute,
    taskEndStreamRunner
);
export default taskEndStream;