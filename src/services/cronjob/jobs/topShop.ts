import cron from "node-cron";
import { ShopRepo } from "../../../repositories/ShopRepo";
import { InMemorySellerStore } from "../../../SocketStores/SellerStore";
import { Topic } from "../constants";
import _ from "lodash"
import { ShopDTO } from "../../../DTO/Shop";
import { handleTopShop } from "../../api/home";

const shopRepo = ShopRepo.getInstance();
const shopCache = InMemorySellerStore.getInstance();

const updateTopSeller = async () => {
    try {
        if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') || process.env.IS_SERVER_WOWZA) return;
        let todayShops = await shopRepo.getShopRanking(1, 500)
        todayShops = todayShops.map(item => {
            item.shop = new ShopDTO(item.shop).toTopShopJSON()
            return item
        })
        todayShops = await handleTopShop(todayShops, null, true)
        todayShops = todayShops.map(item => item.shop)
        const splitShops = _.chunk(todayShops, 20)
        const env = process.env.NODE_ENV === 'production' ? 'production' : 'staging'
        splitShops.forEach((shopList, index) => {
            shopCache.save(`topSellers_${env}_page_${index + 1}`, shopList)
        })
    } catch (error) {
        console.log(error);
    }
}

const updateTopSellerJob = cron.schedule(
    Topic.everyThirtyMinute,
    updateTopSeller
);
export default updateTopSellerJob;