import cron from "node-cron";
import { UserRepo } from "../../../repositories/UserRepo";
import { InMemoryUserStore } from "../../../SocketStores/UserStore";
import { Topic } from "../constants";

const userRepo = UserRepo.getInstance();
const userCache = InMemoryUserStore.getInstance();

const userAndShops = async () => {
    try {
        if ((process.env.NODE_ENV !== 'staging' && process.env.NODE_ENV !== 'production') || process.env.IS_SERVER_WOWZA) return;
        // job
        const userAndShops: any = await userRepo.userAndShops()
        await userCache.save("userAndShops", userAndShops)
    } catch (error) {
        console.log(error);
    }
}

const autoGetUserAndShop = cron.schedule(
    Topic.everyOneHour,
    userAndShops
)

export default autoGetUserAndShop;