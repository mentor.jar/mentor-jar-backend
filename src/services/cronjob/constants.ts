export enum Topic {
  everyMinute = '* * * * *',
  everyTwoMinute = '*/2 * * * *',
  everyThreeMinute = '*/3 * * * *',
  everyFiveMinute = '*/5 * * * *',
  everyTenMinute = '*/10 * * * *',
  everyThirtyMinute = '*/30 * * * *',

  everyTenSeconds = '*/10 * * * * *',
  everyTwentySeconds = '*/20 * * * * *',
  everyThirtySeconds = '*/30 * * * * *',

  everyDay15PM = '0 15 * * *',
  everyDay0AM = '0 0 * * *',
  everyDay1AM = '0 1 * * *',
  everyDay2AM = '0 2 * * *',
  everyDay3AM = '0 3 * * *',

  everyOneHour = '0 */1 * * *',

  everyDay0AMAnd0PM = '0 0,12 * * *',

  every0AMonMonday = '0 0 * * 1'

}

export enum DisconnectTime {
  OneTime = 1,
  TwoTime = 2,
  ThreeTime = 3
}

export enum Time {
  ThreeMinute = 3 * 60 * 1000,
  FiveMinute = 5 * 60 * 1000,
  OneHour = 60 * 60 * 1000,
}

export const ArrayOnlinePaymentMedthod = ['MOMO', 'VNPAY']
