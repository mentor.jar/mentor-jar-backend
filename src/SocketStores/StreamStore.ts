import { cloneObj } from '../utils/helper';
import { client } from '../services/redis/redis';
import { promisify } from 'util';
import { StreamSessionRepository } from '../repositories/StreamSessionRepository';
import { InMemoryAuthUserStore } from './AuthStore';
import { NotFoundError } from '../base/customError';
import { RoomChatDTO } from '../DTO/RoomChatDTO';

const setAsync = promisify(client.hset).bind(client);
const getAsync = promisify(client.hget).bind(client);
const getAll = promisify(client.hvals).bind(client);
export class InMemoryStreamForStore {
    private static instance: InMemoryStreamForStore;

    public static getInstance(): InMemoryStreamForStore {
        if (!InMemoryStreamForStore.instance) {
            InMemoryStreamForStore.instance = new InMemoryStreamForStore();
        }
        return InMemoryStreamForStore.instance;
    }

    async getStream(streamId) {
        let stream = await getAsync('streams', streamId);
        stream = JSON.parse(stream);
        return stream;
    }

    async saveStream(stream) {
        stream = cloneObj(stream);
        await setAsync('streams', stream._id, JSON.stringify(stream));
        return stream;
    }

    async updateView(streamId, view) {
        let stream = await this.getStream(streamId);
        stream.view = view;
        this.saveStream(stream);
    }

    async updateHeart(streamId) {
        let stream = await this.getStream(streamId);
        if (!stream) return 0;
        stream.heart += 1;
        this.saveStream(stream);
        StreamSessionRepository.getInstance().updateHeart(
            streamId,
            stream.heart
        );
        return stream.heart;
    }

    async findStreamByRoom(room_chat_id) {
        let allStream = await getAll('streams');
        let result = allStream.find(stream => {
            stream = JSON.parse(stream);
            return stream.room_chat_id == room_chat_id;
        });
        return result ? JSON.parse(result) : null;
    }

    async updateProducts(streamId, listIdProduct) {
        let stream = await this.getStream(streamId);
        if (!stream) {
            throw new NotFoundError('kênh stream không tồn tại');
        }
        await this.saveStream(stream);
        stream = await StreamSessionRepository.getInstance().updateProduct(
            streamId,
            listIdProduct
        );
        this.saveStream(stream);
        return stream;
    }

    async pinMessage(streamId, message) {
        let stream = await this.getStream(streamId);
        stream.pin_message = message;
        this.saveStream(stream);
        StreamSessionRepository.getInstance().pinMessage(
            streamId,
            message
        );
        return stream
    }
}
