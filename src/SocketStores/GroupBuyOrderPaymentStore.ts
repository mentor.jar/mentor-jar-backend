import { cloneObj } from '../utils/helper';
import { promisify } from 'util';
import { client } from '../services/redis/redis';
import { KEY } from '../constants/cache';

const setAsync = promisify(client.hset).bind(client);
const getAsync = promisify(client.hget).bind(client);
const getAllKeys = promisify(client.hkeys).bind(client);
const deleteAsync = promisify(client.hdel).bind(client);

export class InMemoryGBOrderPaymentStore {
    private static instance: InMemoryGBOrderPaymentStore;

    public static getInstance(): InMemoryGBOrderPaymentStore {
        if (!InMemoryGBOrderPaymentStore.instance) {
            InMemoryGBOrderPaymentStore.instance = new InMemoryGBOrderPaymentStore();
        }
        return InMemoryGBOrderPaymentStore.instance;
    }

    /**
     * 
     * @param key
     * @param groupOrderId: _id of grouporders || group_order_id of order
     * @returns 
     */

    async get(key: KEY, groupOrderId: String) {
        if (!client.ready) return undefined;
        let gbPayment = await getAsync(key, groupOrderId);
        gbPayment = JSON.parse(gbPayment);
        return gbPayment;
    }

    /**
     * 
     * @param key 
     * @param group_order_id: _id of grouporders || group_order_id of order
     * @param ref_link: ref_link of groupbuy
     * @param order_room_id: order_room_id of groupbuy
     * @param group_id: group_id of groupbuy
     * @param avatar_url: avatar_url of buyer
     * @param ref_from_id: ref_from_id
     * @returns 
     */

    async set(key: KEY, {
        group_order_id,
        ref_link,
        order_room_id,
        group_id,
        avatar_url,
        ref_from_id
    }: any) {
        if (!client.ready) return;
        let data = {
            ref_link,
            order_room_id,
            group_id,
            avatar_url,
            ref_from_id
        }
        await setAsync(key, group_order_id, JSON.stringify(data));
        return data;
    }

    async getAllKeys(key: KEY) {
        if (!client.ready) return undefined;
        let field = await getAllKeys(key)

        return field;
    }

    async deleteById(id: String) {
        if (!client.ready) return;
        await deleteAsync(KEY.STORE_GROUP_ORDER_OF_GB, id);
    }
}
