import { cloneObj } from '../utils/helper';
import { promisify } from 'util';
import { client } from '../services/redis/redis';
import { TTL } from './constants';

const setAsync = promisify(client.hset).bind(client);
const setExpire = promisify(client.expire).bind(client);
const getAsync = promisify(client.hget).bind(client);
const getWithFieldAsync = promisify(client.hmget).bind(client);
const hIncrByAsync = promisify(client.hincrby).bind(client);
const hgetAllAsync = promisify(client.hgetall).bind(client)
const delKeyAsync = promisify(client.del).bind(client)
const keysAsync = promisify(client.keys).bind(client)

export class InMemoryVoucherStore {
    private static instance: InMemoryVoucherStore;

    public static getInstance(): InMemoryVoucherStore {
        if (!InMemoryVoucherStore.instance) {
            InMemoryVoucherStore.instance = new InMemoryVoucherStore();
        }
        return InMemoryVoucherStore.instance;
    }

    async get(key) {
        const env = process.env.NODE_ENV === 'production' ? process.env.NODE_ENV : 'staging'
        let vouchers = await getAsync(key, env);
        vouchers = JSON.parse(vouchers);
        return vouchers;
    }

    async save(key, vouchers) {
        vouchers = cloneObj(vouchers)
        await setAsync(key, process.env.NODE_ENV === 'production' ? process.env.NODE_ENV : 'staging', JSON.stringify(vouchers));
        return vouchers
    }

    async exists(key) {
        const isExists = await client.exists(key)
        return isExists
    }

    async expire(key, TTL: TTL) {
        setExpire(key, TTL);
    }

    async saveUserUsingVoucher(key, field, value) {
        setAsync(key, field, value)
    }

    async checkVoucherUser(key) {
        const env = process.env.NODE_ENV === 'production' ? process.env.NODE_ENV : 'staging'
        const data = await getAsync(key, env)
        console.log("data: ", data);        
        return data?.length
    }

    async getByKeyAndField(key, field, isOnlyFirstItem = false) {
        const value = await getWithFieldAsync(key, field)
        return isOnlyFirstItem ? value?.[0] : value
    }

    async increaseValueFromKeyAndField(key, field, quantity) {
        console.log(key, field, quantity);
        
        hIncrByAsync(key, field, quantity)
    }

    async getAllByKey(key) {
        return hgetAllAsync(key)
    }

    async delKey(key) {
        return delKeyAsync(key)
    }

    async keys(key) {
        return keysAsync(key)
    }
}
