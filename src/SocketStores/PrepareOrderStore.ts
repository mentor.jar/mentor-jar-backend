import { cloneObj } from '../utils/helper';
import { promisify } from 'util';
import { client } from '../services/redis/redis';
import { KEY } from '../constants/cache';

const setAsync = promisify(client.hset).bind(client);
const getAsync = promisify(client.hget).bind(client);
const getAllKeys = promisify(client.hkeys).bind(client);
export class InMemoryPrepareOrderStore {
    private static instance: InMemoryPrepareOrderStore;

    public static getInstance(): InMemoryPrepareOrderStore {
        if (!InMemoryPrepareOrderStore.instance) {
            InMemoryPrepareOrderStore.instance = new InMemoryPrepareOrderStore();
        }
        return InMemoryPrepareOrderStore.instance;
    }

    async get(key: KEY, shopId: String) {
        if(!client.ready) return undefined;
        let rangeTime = await getAsync(key, shopId);
        rangeTime = JSON.parse(rangeTime);
        return rangeTime;
    }

    async set(key: KEY, shopId, rangeTime) {
        if(!client.ready) return;
        rangeTime = cloneObj(rangeTime);
        await setAsync(key, shopId, JSON.stringify(rangeTime));
        return rangeTime;
    }

    async getAllKeys(key: KEY) {
        if(!client.ready) return undefined;
        let prepareOrders = await getAllKeys(key)

        return prepareOrders;
    }
}
