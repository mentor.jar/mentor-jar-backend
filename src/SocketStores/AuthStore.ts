import { cloneObj } from '../utils/helper';
import { promisify } from 'util';
import { client } from '../services/redis/redis';
import { KEY } from '../constants/cache';

const setAsync = promisify(client.hset).bind(client);
const getAsync = promisify(client.hget).bind(client);
const delAsync = promisify(client.hdel).bind(client);
export class InMemoryAuthUserStore {
    private static instance: InMemoryAuthUserStore;

    public static getInstance(): InMemoryAuthUserStore {
        if (!InMemoryAuthUserStore.instance) {
            InMemoryAuthUserStore.instance = new InMemoryAuthUserStore();
        }
        return InMemoryAuthUserStore.instance;
    }

    async getUser(id) {
        let user = await getAsync('user', id);
        user = JSON.parse(user);
        return user;
    }

    async saveUser(user) {
        user = cloneObj(user);
        await setAsync('user', user._id, JSON.stringify(user));
        return user;
    }

    async get(id) {
        if(!client.ready) return null;
        let user = await getAsync(KEY.STORE_USER, id);
        user = JSON.parse(user);
        return user;
    }

    async set(key: KEY, user) {
        if(!client.ready) return;
        user = cloneObj(user);
        await setAsync(key, user._id, JSON.stringify(user));
        return user;
    }

    async updateShop(id, shop) {
        if(!client.ready) return null;
        let user = await getAsync(KEY.STORE_USER, id);
        if (user) {
            user = JSON.parse(user);
            user.shop = shop;
            setAsync(KEY.STORE_USER, user._id, JSON.stringify(user));
        }
    }

    async updateBookMark(id, favoriteProducts) {
        if(!client.ready) return null;
        let user = await getAsync(KEY.STORE_USER, id);
        if (user) {
            user = JSON.parse(user);
            user.favorite_products = favoriteProducts;
            setAsync(KEY.STORE_USER, user._id, JSON.stringify(user));
        }
    }

    async delAsync(key, field) {
        return client.hdel(key, field)
    }
}
