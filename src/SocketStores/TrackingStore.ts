import { cloneObj } from '../utils/helper';
import { promisify } from 'util';
import { client } from '../services/redis/redis';

const setAsync = promisify(client.hset).bind(client);
const getAsync = promisify(client.hget).bind(client);

export class InMemoryTrackingStore {
    private static instance: InMemoryTrackingStore;

    public static getInstance(): InMemoryTrackingStore {
        if (!InMemoryTrackingStore.instance) {
            InMemoryTrackingStore.instance = new InMemoryTrackingStore();
        }
        return InMemoryTrackingStore.instance;
    }

    async get(key) {
        const env = process.env.NODE_ENV === 'production' ? process.env.NODE_ENV : 'staging'
        let trackings = await getAsync(key, env);
        trackings = JSON.parse(trackings);
        return trackings;
    }

    async save(key, trackings) {
        trackings = cloneObj(trackings)
        await setAsync(key, process.env.NODE_ENV === 'production' ? process.env.NODE_ENV : 'staging', JSON.stringify(trackings));
        return trackings
    }

    async exists(key) {
        const isExists = await client.exists(key)
        return isExists
    }
}
