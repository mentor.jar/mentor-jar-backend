import { cloneObj } from '../utils/helper';
import { promisify } from 'util';
import { client } from '../services/redis/redis';

const setAsync = promisify(client.hset).bind(client);
const getAsync = promisify(client.hget).bind(client);

export class InMemoryUserStore {
    private static instance: InMemoryUserStore;

    public static getInstance(): InMemoryUserStore {
        if (!InMemoryUserStore.instance) {
            InMemoryUserStore.instance = new InMemoryUserStore();
        }
        return InMemoryUserStore.instance;
    }

    async get(key) {
        const env = process.env.NODE_ENV === 'production' ? process.env.NODE_ENV : 'staging'
        let userAndShops = await getAsync(key, env);
        userAndShops = JSON.parse(userAndShops);
        return userAndShops;
    }

    async save(key, userAndShops) {
        userAndShops = cloneObj(userAndShops)
        await setAsync(key, process.env.NODE_ENV === 'production' ? process.env.NODE_ENV : 'staging', JSON.stringify(userAndShops));
        return userAndShops
    }

    async exists(key) {
        const isExists = await client.exists(key)
        return isExists
    }
}