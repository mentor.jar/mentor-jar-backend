export enum TTL {
    tenSeconds = 10,
    thirtySeconds = 30,

    minute = 60,
    twoMinute = 120,
    threeMinute = 180,
    fiveMinute = 300,
    tenMinute = 600,
  
    oneHour = 3600,
}