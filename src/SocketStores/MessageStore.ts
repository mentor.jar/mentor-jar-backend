import { cloneObj } from '../utils/helper';

import { client } from '../services/redis/redis';
import { promisify } from 'util';
import { ChatType } from '../services/sockets/definitions';
import { StreamSessionRepository } from '../repositories/StreamSessionRepository';
import { InMemoryStreamForStore } from './StreamStore';
import { RoomChatRepo } from '../repositories/RoomChatRepo';

const setAsync = promisify(client.hset).bind(client);
const getAsync = promisify(client.hget).bind(client);
const getAll = promisify(client.hvals).bind(client);
export class InMemoryMessageForRoomStore {
    private rooms;
    private checkRoomApi;
    constructor() {
        this.rooms = new Map();
        this.checkRoomApi = new Map();
    }

    private static instance: InMemoryMessageForRoomStore;

    public static getInstance(): InMemoryMessageForRoomStore {
        if (!InMemoryMessageForRoomStore.instance) {
            InMemoryMessageForRoomStore.instance = new InMemoryMessageForRoomStore();
        }
        return InMemoryMessageForRoomStore.instance;
    }

    async getRoom(roomId) {
        let room = await getAsync('room', roomId);
        if (room) {
            room = JSON.parse(room);
            if (room.messages)
                room.messages.sort((a, b) => a.id - b.id)
            return room;
        }
        return room;
    }

    async saveRoom(room) {
        room = cloneObj(room);
        if (room.type == ChatType.LIVE_STREAM) {
            let stream = await StreamSessionRepository.getInstance().getStreamSessionByRoomChat(
                room._id
            );
            if (stream) {
                await InMemoryStreamForStore.getInstance().saveStream(stream);
                room.stream_id = stream._id;
            }

        }
        await setAsync('room', room._id, JSON.stringify(room));
        return room;
    }

    async isRoomApi(roomId) {
        let room = await getAsync('roomApi', roomId);
        room = JSON.parse(room);
        return room;
    }

    async saveCheckRoomApi(roomId) {
        roomId = cloneObj(roomId);
        await setAsync('roomApi', roomId, true);
        return roomId;
    }

    async fetchAllRoomByUser(userId = null) {
        let allRoom = await getAll('room');
        allRoom = allRoom.filter(room => {
            room = JSON.parse(room);
            return room.users.findIndex(user => user._id == userId) > -1;
        });
        return allRoom.map(room => JSON.parse(room))
    }

    async getFullKeyRoom() {
        let allRoom = await getAll('room')

        return allRoom.map(room => JSON.parse(room)._id)
    }

    async saveMessage(roomId, message) {
        try {
            let room = await this.getRoom(roomId);
            if (!room) return false;
            room.messages.push(message);
            room.messages.sort((a, b) => a.id - b.id)
            let sizeMess = room.messages.length;
            let users = room.users.map(user => {
                user.seen_last_message_index =
                    user._id.toString() == message.user_id.toString()
                        ? sizeMess - 1
                        : user.seen_last_message_index || null;
                user.seen_last_message_id = sizeMess && user._id.toString() == message.user_id.toString() ? room.messages[sizeMess - 1].id : null;
                return user;
            });
            room.users = users;
            room.last_object = !['image', 'text'].includes(message.type) ? message : room?.last_object
            await this.saveRoom(room);
            let check_room = await this.getRoom(roomId);
            if (check_room.messages.findIndex(e => e.id == message.id) == -1) {
                await this.saveRoom(room);
                check_room = await this.getRoom(roomId);
                if (check_room.messages.findIndex(e => e.id == message.id) == -1) {
                    await this.saveRoom(room);
                    check_room = await this.getRoom(roomId);
                    if (check_room.messages.findIndex(e => e.id == message.id) == -1) {
                        return false;
                    }
                }
            }

        } catch (error) {
            console.log('------------------------chat--' + (new Date()) + '---------------')
            console.log(error)
            console.log('------------------------chat-----------------')
            return false
        }
        return true
    }

    async saveMute(roomId, userId) {
        let room = await this.getRoom(roomId);
        if (!room) return;
        room.users = room.users.map(user => {
            if (user._id == userId) user.is_muted = true;
            return user;
        });
        await this.saveRoom(room);
    }

    async saveUnmute(roomId, userId) {
        let room = await this.getRoom(roomId);
        if (!room) return;
        room.users = room.users.map(user => {
            if (user._id == userId) user.is_muted = false;
            return user;
        });
        await this.saveRoom(room);
    }

    async userJoinRoom(roomId, userJoin) {
        let room = await this.getRoom(roomId);
        if (!room) return;
        if (room.type == ChatType.LIVE_STREAM) {
            userJoin.is_muted = false;
        }
        let index = room.users.findIndex(user => {
            return user._id + '' == userJoin._id + '';
        });
        // check and update online
        if (index == -1) {
            if (room.type == ChatType.LIVE_STREAM) {
                userJoin.seen_last_message_index = room.messages.length - 1;
                userJoin.is_online = true;
                room.users.push(userJoin);
            }
        } else {
            room.users[index].is_online = true;
            room.users[index].userName = userJoin.userName;
            room.users[index].avatar = userJoin.avatar;
            room.users[index].gender = userJoin.gender;
        }
        // save room
        await this.saveRoom(room);
        await this.saveViewer(roomId);
    }

    async userLeftRoom(roomId, userJoin) {
        let room = await this.getRoom(roomId);
        if (!room) return;
        if (room.type == ChatType.LIVE_STREAM) {
            userJoin.is_muted = false;
        }
        let index = room.users.findIndex(user => {
            return user._id + '' == userJoin._id + '';
        });
        // check and update online
        if (index >= 0) {
            room.users[index].is_online = false;
        }
        // save room
        await this.saveRoom(room);
        await this.saveViewer(roomId);
    }

    async saveViewer(roomId) {
        // update view
        let view = await this.getView(roomId);
        let memoryStream = InMemoryStreamForStore.getInstance();
        let streamRedis = await memoryStream.findStreamByRoom(roomId);
        if (streamRedis) {
            // update viewer in api
            StreamSessionRepository.getInstance().updateView(
                streamRedis._id,
                view
            );
            // // update viewer in redis
            await memoryStream.updateView(streamRedis._id, view);
        }
    }

    async getView(roomId) {
        let room = await this.getRoom(roomId);
        let user_onlines = room.users.filter(user => {
            return user.is_online;
        });
        return user_onlines.length - 1 >= 0 ? user_onlines.length - 1 : 1;
    }

    async readLastMessage(roomId, userRead) {
        let room = await this.getRoom(roomId);
        let sizeMess = room.messages.length;
        let users = room.users.map(user => {
            user.seen_last_message_index =
                user._id == userRead._id
                    ? sizeMess - 1
                    : user.seen_last_message_index || null;
            user.red_dot =
                sizeMess -
                (user.seen_last_message_index == null
                    ? 0
                    : user.seen_last_message_index + 1);
            user.seen_last_message_id = sizeMess ? room.messages[sizeMess - 1].id : null;
            return user;
        });
        room.users = users;
        await this.saveRoom(room);
    }

    async findUser(roomId, userId) {
        const room = await this.getRoom(roomId);
        const user = room.users.find(user => user._id == userId);
        return user;
    }

    async deleteMessage(roomId, messageId, user) {
        let room = await this.getRoom(roomId);
        if (!room) return false;
        let check_delete = false;
        let indexMessDel = -1;
        let messages = room.messages.map((message, index) => {
            if (message.id == messageId && user._id == message.user_id) {
                message.deleted_at = Math.floor(Date.now() / 1000);
                check_delete = true;
                indexMessDel = index;
            }
            return message;
        });
        if (indexMessDel > 0) {
            room.users = room.users.map(user => {
                if (
                    user.seen_last_message_index &&
                    user.seen_last_message_index >= indexMessDel
                ) {
                    user.seen_last_message_index -= 1;
                }
                return user;
            });
        }
        room.messages = messages;
        await this.saveRoom(room);
        RoomChatRepo.getInstance().deleteMessage(roomId, messageId, user);
        return check_delete;
    }
}
