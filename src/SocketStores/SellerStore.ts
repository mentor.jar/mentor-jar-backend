import { cloneObj } from '../utils/helper';
import { promisify } from 'util';
import { client } from '../services/redis/redis';

const setAsync = promisify(client.set).bind(client);
const getAsync = promisify(client.get).bind(client);

export class InMemorySellerStore {
    private static instance: InMemorySellerStore;

    public static getInstance(): InMemorySellerStore {
        if (!InMemorySellerStore.instance) {
            InMemorySellerStore.instance = new InMemorySellerStore();
        }
        return InMemorySellerStore.instance;
    }

    async get(key) {
        const env = process.env.NODE_ENV === 'production' ? process.env.NODE_ENV : 'staging'
        let sellers = await getAsync(key);
        sellers = JSON.parse(sellers);
        return sellers;
    }

    async save(key, sellers) {
        sellers = cloneObj(sellers)
        await setAsync(key, JSON.stringify(sellers));
        return sellers
    }

    async exists(key) {
        const isExists = await client.exists(key)
        return isExists
    }

    async saveWithKeyAndField(key, field, value) {
        setAsync(key, field, value)
    }

}
