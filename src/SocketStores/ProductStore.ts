import { cloneObj } from '../utils/helper';
import { promisify } from 'util';
import { client } from '../services/redis/redis';
import { TTL } from './constants';
import { KEY } from '../constants/cache';

const setAsync = promisify(client.hset).bind(client);
const setManyAsync = promisify(client.hmset).bind(client);
const setExpire = promisify(client.expire).bind(client);
const getAsync = promisify(client.hget).bind(client);
const deleteAsync = promisify(client.hdel).bind(client);

export class InMemoryProductStore {
    private static instance: InMemoryProductStore;

    public static getInstance(): InMemoryProductStore {
        if (!InMemoryProductStore.instance) {
            InMemoryProductStore.instance = new InMemoryProductStore();
        }
        return InMemoryProductStore.instance;
    }

    async get(key) {
        if(!client.ready) return undefined;
        const env = process.env.NODE_ENV === 'production' ? process.env.NODE_ENV : 'staging'
        let products = await getAsync(key, env);
        products = JSON.parse(products);
        return products;
    }

    async save(key, products) {
        if(!client.ready) return undefined;
        products = cloneObj(products)
        await setAsync(key, process.env.NODE_ENV === 'production' ? process.env.NODE_ENV : 'staging', JSON.stringify(products));
        return products
    }

    async exists(key) {
        const isExists = await client.exists(key)
        return isExists
    }

    async expire(key, TTL: TTL) {
        setExpire(key, TTL);
    }

    async getById(id: String) {
        if(!client.ready) return undefined;
        let product = await getAsync(KEY.STORE_PRODUCT, id);
        return JSON.parse(product);
    }

    async setById(product: any) {
        if(!client.ready) return;
        product = cloneObj(product);
        await setAsync(KEY.STORE_PRODUCT, product._id, JSON.stringify(product));
        return product;
    }

    async setMany(products: any) {
        if(!client.ready) return;
        await setManyAsync(KEY.STORE_PRODUCT, products);
    }

    async deleteById(id: String) {
        if(!client.ready) return;
        await deleteAsync(KEY.STORE_PRODUCT, id);
    }

}
