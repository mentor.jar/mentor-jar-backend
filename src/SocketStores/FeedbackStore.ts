import { cloneObj } from '../utils/helper';
import { promisify } from 'util';
import { client } from '../services/redis/redis';
import { KEY } from '../constants/cache';

const setAsync = promisify(client.hset).bind(client);
const getAsync = promisify(client.hget).bind(client);
const getAllKeys = promisify(client.hkeys).bind(client);
export class InMemoryFeedbackStore {
    private static instance: InMemoryFeedbackStore;

    public static getInstance(): InMemoryFeedbackStore {
        if (!InMemoryFeedbackStore.instance) {
            InMemoryFeedbackStore.instance = new InMemoryFeedbackStore();
        }
        return InMemoryFeedbackStore.instance;
    }

    async get(key: KEY, productId: String) {
        if(!client.ready) return undefined;
        let feedbacks = await getAsync(key, productId);
        feedbacks = JSON.parse(feedbacks);
        return feedbacks;
    }

    async set(key: KEY, productId, feedbacks) {
        if(!client.ready) return;
        feedbacks = cloneObj(feedbacks);
        await setAsync(key, productId, JSON.stringify(feedbacks));
        return feedbacks;
    }

    async getAllKeys(key: KEY) {
        if(!client.ready) return undefined;
        let field = await getAllKeys(key)

        return field;
    }
}
