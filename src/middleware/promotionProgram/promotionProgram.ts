import moment from "moment";
import responseCode from "../../base/responseCode";
import { ProductRepo } from "../../repositories/ProductRepo";
import { PromotionProgramRepo } from "../../repositories/PromotionProgramRepo";
import { ObjectId } from "../../utils/helper";

const { body, validationResult } = require("express-validator");

// Date and Time
export const REQ_DATE_TIME_FORMAT = 'YYYY-MM-DD HH:mm'

const productRepo = ProductRepo.getInstance();
const promotionProgramRepo = PromotionProgramRepo.getInstance();

export const createPromotionProgramRule = [
    body("name")
        .exists().withMessage((value, { req }) => req.__('promotion.missing_name_promotion'))
        .isLength({ min: 1, max: 150 }).withMessage((value, { req }) => req.__('promotion.invalid_name_length_promotion'))
        .trim(),
    body("start_time")
        .exists().withMessage((value, { req }) => req.__('promotion.missing_start_time')),
    body("end_time")
        .exists().withMessage((value, { req }) => req.__('promotion.missing_end_time')),
    body("discount")
        .exists().withMessage((value, { req }) => req.__('promotion.missing_discount_promotion'))
        .isFloat({ min: 1, max: 50 }).withMessage((value, { req }) => req.__('promotion.invalid_discount_range_promotion')),
    body("limit_quantity")
        .optional()
        .isNumeric().withMessage((value, { req }) => req.__('promotion.invalid_limit_quantity')),
    body("products")
        .exists().withMessage((value, { req }) => req.__('promotion.missing_products_in_promotion'))
        .isArray({ min: 1, max: 1000 }).withMessage((value, { req }) => req.__('promotion.max_products_in_promotion'))
]

export const createPromotionProgramValidate = (req, res, next) => {
    const error = validationResult(req).formatWith(({ msg }) => msg);
    const hasError = !error.isEmpty();
    if (hasError) {
        res.error('VALIDATION_ERROR', error.array()[0], 422)
    } else next();
}


export const validateProductInPromotionProgram = async (req, res, next) => {
    try {
        if (Array.isArray(req.body.products)) {
            const products = await productRepo.findWithShopAndIds(
                req.shop_id.toString(),
                req.body.products);
            if (products.length != req.body.products.length)
                throw new Error(req.__('promotion.not_exist_product'))
        }
        next();
    } catch (error) {
        res.error('VALIDATION_ERROR', error.message, 422)
    }
}

export const validateUniqueProductOnCreate = async (req, res, next) => {
    try {
        if (Array.isArray(req.body.products)) {
            const result = await promotionProgramRepo.checkUniqueProduct(req.body.products)
            if (result) {
                throw new Error(req.__('promotion.product_must_be_in_one_promotion'))
            }
        }
        next();
    } catch (error) {
        res.error('VALIDATION_ERROR', error.message, 422)
    }
}
export const validateUniqueProductOnUpdate = async (req, res, next) => {
    try {
        if (Array.isArray(req.body.products)) {
            const result = await promotionProgramRepo.checkUniqueProduct(req.body.products, req.params.id)
            if (result)
                throw new Error(req.__('promotion.product_must_be_in_one_promotion'))
        }
        next();
    } catch (error) {
        res.error('VALIDATION_ERROR', error.message, 422)
    }
}

export const validateTimeCreate = async (req, res, next) => {
    try {
        const { start_time, end_time } = req.body
        const startTime = moment(new Date(start_time), REQ_DATE_TIME_FORMAT, true)
        const endTime = moment(new Date(end_time), REQ_DATE_TIME_FORMAT, true)
        const currentTime = moment(new Date(), REQ_DATE_TIME_FORMAT, true)
        const duration = moment.duration(endTime.diff(startTime));

        if (!startTime.isValid() || !endTime.isValid()) {
            res.error(responseCode.BAD_REQUEST.name, req.__('promotion.invalid_start_and_end_time'), responseCode.BAD_REQUEST.code);
            return;
        }
        else {
            if (startTime.add(2, "minutes").isBefore(currentTime) || endTime.isBefore(currentTime)) {
                res.error(responseCode.BAD_REQUEST.name, req.__('promotion.start_time_must_greater_than_now'), responseCode.BAD_REQUEST.code);
                return;
            }
            if (!endTime.isAfter(startTime) || duration.asHours() < 1) {
                res.error(responseCode.BAD_REQUEST.name, req.__('promotion.end_time_must_greater_than_start_time'), responseCode.BAD_REQUEST.code);
                return;
            }
        }
        next();
    } catch (error) {
        res.error('VALIDATION_ERROR', error.message, 422)
    }
}

export const validateTimeUpdate = async (req, res, next) => {
    try {
        const { current_promotion } = req
        const { start_time, end_time } = req.body
        const startTime = moment(new Date(start_time), REQ_DATE_TIME_FORMAT, true)
        const endTime = moment(new Date(end_time), REQ_DATE_TIME_FORMAT, true)
        const currentTime = moment(new Date(), REQ_DATE_TIME_FORMAT, true)
        const duration = moment.duration(endTime.diff(startTime));
        const currentStartTime = moment(current_promotion.start_time, REQ_DATE_TIME_FORMAT, true)
        const currentEndTime = moment(current_promotion.end_time, REQ_DATE_TIME_FORMAT, true)

        if (!startTime.isValid() || !endTime.isValid()) {
            res.error(responseCode.BAD_REQUEST.name, req.__('promotion.invalid_start_and_end_time'), responseCode.BAD_REQUEST.code);
            return;
        }
        else {
            if ((currentStartTime.isAfter(currentTime) && startTime.isBefore(currentTime)) || endTime.isBefore(currentTime)) {
                res.error(responseCode.BAD_REQUEST.name, req.__('promotion.start_time_must_greater_than_now'), responseCode.BAD_REQUEST.code);
                return;
            }
            if (!endTime.isAfter(startTime) || duration.asHours() < 1) {
                res.error(responseCode.BAD_REQUEST.name, req.__('promotion.end_time_must_greater_than_start_time'), responseCode.BAD_REQUEST.code);
                return;
            }

            // More condition base on status

            // On running
            if (currentStartTime.isSameOrBefore(currentTime) && currentEndTime.isAfter(currentTime)) {
                // Only end time can update longer
                if (endTime.isBefore(currentEndTime)) {
                    res.error(responseCode.BAD_REQUEST.name, req.__('promotion.end_time_can_only_prolong_when_promotion_is_running'), responseCode.BAD_REQUEST.code)
                    return;
                }
                // Can not update start time
                if (!startTime.isSame(currentStartTime)) {
                    res.error(responseCode.BAD_REQUEST.name, req.__('promotion.start_time_can_not_change_when_promotion_is_running'), responseCode.BAD_REQUEST.code)
                    return;
                }
            }

            // On scheduler
            if (currentStartTime.isAfter(currentTime)) {
                // Only end time can update longer
                if (endTime.isBefore(currentEndTime)) {
                    res.error(responseCode.BAD_REQUEST.name, req.__('promotion.end_time_can_only_prolong_when_promotion_upcomming'), responseCode.BAD_REQUEST.code)
                    return;
                }
            }
        }
        next();
    } catch (error) {
        res.error('VALIDATION_ERROR', error.message, 422)
    }
}

export const findPromotionForEnd = async (req, res, next) => {
    try {
        const id = req.params.id
        const current = new Date()
        const promotion = await promotionProgramRepo.findOne({
            _id: ObjectId(id),
            start_time: { $lte: current },
            end_time: { $gte: current }
        })
        if (!promotion) {
            res.error(responseCode.BAD_REQUEST.name, req.__('promotion.promotion_can_only_be_ended_when_running'), responseCode.BAD_REQUEST.code);
            return;
        }
        next();
    } catch (error) {
        res.error('VALIDATION_ERROR', error.message, 422)
    }
}

export const checkOwnerPromotion = async (req, res, next) => {
    try {
        const id = req.params.id
        const shop_id = req.shop_id
        const promotion = await promotionProgramRepo.findOne({ _id: ObjectId(id), shop_id: ObjectId(shop_id), is_valid: true })
        if (!promotion) {
            res.error(responseCode.BAD_REQUEST.name, req.__('promotion.promotion_not_found'), responseCode.BAD_REQUEST.code);
            return;
        }
        req.current_promotion = promotion
        next();
    } catch (error) {
        res.error('VALIDATION_ERROR', error.message, 422)
    }
}

export const checkBeforeDelete = async (req, res, next) => {
    try {
        const currentStartTime = moment(req.current_promotion.start_time, REQ_DATE_TIME_FORMAT, true)
        const currentTime = moment(new Date(), REQ_DATE_TIME_FORMAT, true)
        if (currentStartTime.isAfter(currentTime)) {
            // Only end time can update longer
            if (currentTime.isAfter(currentStartTime)) {
                res.error(responseCode.BAD_REQUEST.name, req.__('promotion.promotion_can_only_be_removed_when_upcomming'), responseCode.BAD_REQUEST.code);
                return;
            }
        }
        next();
    } catch (error) {
        res.error('VALIDATION_ERROR', error.message, 422)
    }
}

export const updatePromotionProgramRule = createPromotionProgramRule;
export const updatePromotionProgramValidate = createPromotionProgramValidate;
