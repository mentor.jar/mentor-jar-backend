import responseCode from '../../base/responseCode';
import BannerService from '../../controllers/serviceHandles/banner';
import BannerRepo from '../../repositories/BannerRepo';
import { CategoryRepo } from '../../repositories/CategoryRepo';
import { ProductRepo } from "../../repositories/ProductRepo";
import { ShopRepo } from '../../repositories/ShopRepo';
import { VoucherRepository } from '../../repositories/VoucherRepository';
import { isSameDateTimestamp } from '../../utils/dateUtil';
import { cloneObj, isMappable, ObjectId } from '../../utils/helper';
const productRepo = ProductRepo.getInstance();
const voucherRepo = VoucherRepository.getInstance();
const bannerRepo = BannerRepo.getInstance();
const { body, validationResult } = require("express-validator");
const ACCEPT_RANGE = 15000;
const shopRepo = ShopRepo.getInstance();
const categoryRepo = CategoryRepo.getInstance();

export const createBannerRule = [
    body("name")
        .exists().withMessage((value, { req }) => req.__('banner.missing_name_banner'))
        .isLength({ min: 5, max: 200 }).withMessage((value, { req }) => req.__('banner.invalid_name_banner'))
        .trim(),
    body("image")
        .exists().withMessage((value, { req }) => req.__('banner.missing_image_banner'))
        .trim(),
    body("promo_link")
        .optional()
        .isLength({ min: 5, max: 500 }).withMessage((value, { req }) => req.__('banner.missing_promo_link_banner'))
        .trim(),
    body("start_time")
        .exists().withMessage((value, { req }) => req.__('banner.missing_start_time_banner')),
    body("end_time")
        .exists().withMessage((value, { req }) => req.__('banner.missing_end_time_banner')),
    body("classify")
        .optional()
        .isIn(['product', 'voucher', 'link']).withMessage((value, { req }) => req.__('banner.invalid_classify_banner')),
    body("description")
        .optional()
        .isLength(),
]

export const createBannerSystemRule = [
    body("name")
        .exists().withMessage((value, { req }) => req.__('banner.missing_name_banner'))
        .isLength({ min: 5, max: 200 }).withMessage((value, { req }) => req.__('banner.invalid_name_banner'))
        .trim(),
    body("images")
        .exists().withMessage((value, { req }) => req.__('banner.missing_image_banner')),
    body("promo_link")
        .optional()
        .isLength({ min: 5, max: 500 }).withMessage((value, { req }) => req.__('banner.missing_promo_link_banner'))
        .trim(),
    body("start_time")
        .exists().withMessage((value, { req }) => req.__('banner.missing_start_time_banner')),
    body("end_time")
        .exists().withMessage((value, { req }) => req.__('banner.missing_end_time_banner')),
    body("classify")
        .optional()
        .isIn(['product', 'voucher', 'link', 'none', 'shop_view', 'shop_list', 'top_shop', 'category_view']).withMessage((value, { req }) => req.__('banner.invalid_classify_banner')),
    body("position")
        .optional()
        .isIn(['top', 'middle', 'other']).withMessage((value, { req }) => req.__('banner.invalid_classify_banner')),
    body("priority").optional().isDecimal(),
    body("description")
        .optional()
        .isLength(),
]

export const createTopShopBannerSystemRule = [
    body("name")
        .exists().withMessage((value, { req }) => req.__('banner.missing_name_banner'))
        .isLength({ min: 5, max: 200 }).withMessage((value, { req }) => req.__('banner.invalid_name_banner'))
        .trim(),
    body("images")
        .exists().withMessage((value, { req }) => req.__('banner.missing_image_banner')),
    body("shop_ids")
        .exists()
        .isArray().withMessage((value, { req }) => req.__('banner.missing_banner_shop_list')),
]

export const createBannerValidate = (req, res, next) => {
    const error = validationResult(req).formatWith(({ msg }) => msg);
    const hasError = !error.isEmpty();
    if (hasError) {
        res.error('VALIDATION_ERROR', error.array()[0], 422)
    } else next();
}

export const verifyShopExistTopShopBanner = async (req, res, next) => {
    try {
        let shop_ids:any = []

        if (isMappable(req.body?.shop_ids)) {
            shop_ids = await Promise.all(
                req.body?.shop_ids?.map(async (id) => shopRepo.findById(id))
            );
            shop_ids = cloneObj(shop_ids);

            shop_ids = shop_ids
                .filter(
                    (shop) =>
                        !shop?.system_banner ||
                        (shop?.system_banner && shop?.system_banner === null)
                )
                .map((shop) => shop._id);
        }

        if (!isMappable(shop_ids)) {
            throw new Error("Danh sách cửa hàng cho banner không hợp lệ")
        }
    
        req.shop_ids = shop_ids;
        next()
    } catch (error) {
        res.error('VALIDATION_ERROR', error.message, 422)
    }
} 

export const validateLimitBanner = async (req, res, next) => {
    try {
        let currentBannerActive: any = await bannerRepo.countBannerSystem();
        currentBannerActive = currentBannerActive[0]

        if (currentBannerActive && currentBannerActive.total >= 20) {
            throw new Error(req.__('banner.max_active_banner'))
        }
        if (!currentBannerActive) {
            throw new Error(req.__('banner.error_occur'))
        }
        next()
    } catch (error) {
        res.error('VALIDATION_ERROR', error.message, 422)
    }
}

export const validateSystemBannerVoucher = async (req, res, next) => {
    try {
        const { classify, vouchers, description } = req.body
        if (classify && classify === "voucher") {
            if (!vouchers?.length && !description?.length) {
                throw new Error(req.__('banner.mising_voucher_list_or_description'))
            }
        }
        next()
    } catch (error) {
        res.error('VALIDATION_ERROR', error.message, 422)
    }
}

export const validateProduct = async (req, res, next) => {
    try {
        if (Array.isArray(req.body.products)) {
            const products = await productRepo.findWithShopAndIds(
                req.shop_id.toString(),
                req.body.products);
            if (products.length != req.body.products.length)
                throw new Error(req.__('banner.checked_product_not_found_or_belong_to_shop'))
        }

        next();
    } catch (error) {
        res.error('VALIDATION_ERROR', error.message, 422)
    }
}

export const validateProductBannerSystem = async (req, res, next) => {
    try {
        if (Array.isArray(req.body.products)) {
            const products = await productRepo.findWithIds(req.body.products);
            if (!products.length)
                throw new Error(req.__('banner.checked_product_not_found'))
        }
        if (req.body.classify === "product" && !req.body.products) {
            throw new Error(req.__('banner.missing_product_in_product_type'))
        }
        next();
    } catch (error) {
        res.error('VALIDATION_ERROR', error.message, 422)
    }
}

export const validateVoucherBannerSystem = async (req, res, next) => {
    try {
        const { vouchers } = req.body
        if (Array.isArray(vouchers)) {
            const result = await voucherRepo.findValidWithIds(vouchers)
            if (!result.length)
                throw new Error(req.__('banner.invalid_checked_voucher'))
        }
        next();
    } catch (error) {
        res.error('VALIDATION_ERROR', error.message, 422)
    }
}

export const validateProductCMS = async (req, res, next) => {
    try {
        if (Array.isArray(req.body.products)) {
            const products = await productRepo.findWithShopAndIds(
                req.body.shop_id.toString(),
                req.body.products);
            if (products.length != req.body.products.length)
                throw new Error(req.__('banner.checked_product_not_found_or_belong_to_shop'))
        }
        next();
    } catch (error) {
        res.error('VALIDATION_ERROR', error.message, 422)
    }
}

export const validateAdvanceAction = async (req, res, next) => {
    try {
        const { classify, advance_actions } = req.body

        if(classify) {
            if( (classify === 'shop_view' || classify === 'shop_list' || classify === 'category_view') && !advance_actions) {
                res.error(responseCode.BAD_REQUEST.name, req.__("feedback.invalid_request"), responseCode.BAD_REQUEST.code)
                return;
            } 
            // classify = shop_view
            if(classify === 'shop_view') {
                const { shop } = advance_actions
                if(!shop) {
                    res.error(responseCode.BAD_REQUEST.name, req.__("banner.missing_banner_shop"), responseCode.BAD_REQUEST.code)
                    return;
                }
                const shopQuery = await shopRepo.findOne({_id: ObjectId(shop)})
                if(!shopQuery) {
                    res.error(responseCode.BAD_REQUEST.name, req.__("shop.shop_not_found"), responseCode.BAD_REQUEST.code)
                    return;
                }
            }

            // classify = shop_list
            if(classify === 'shop_list') {
                const { shops } = advance_actions
                if(!shops) {
                    res.error(responseCode.BAD_REQUEST.name, req.__("banner.missing_banner_shop_list"), responseCode.BAD_REQUEST.code)
                    return;
                }
                const promises = shops.map(shop => {
                    return new Promise(async (resolve, reject) => {
                        const shopQuery = await shopRepo.findOne({_id: ObjectId(shop)})
                        if(!shopQuery) {
                            reject(req.__("shop.shop_not_found"))
                            return;
                        }
                        resolve(shopQuery)
                    })
                })
                await Promise.all(promises)
                    .then(() => {
                        next()
                    })
                    .catch(error => {
                        res.error(responseCode.BAD_REQUEST.name, error.message, responseCode.BAD_REQUEST.code)
                        return;
                    })
            }

            // classify = category_view
            if(classify === 'category_view') {
                const { category } = advance_actions
                if(!category) {
                    res.error(responseCode.BAD_REQUEST.name, req.__("banner.missing_banner_category"), responseCode.BAD_REQUEST.code)
                    return;
                }
                const categoryQuery = await categoryRepo.findOne({_id: ObjectId(category)})
                if(!categoryQuery) {
                    res.error(responseCode.BAD_REQUEST.name, req.__("category.missing_category"), responseCode.BAD_REQUEST.code)
                    return;
                }
            }
        }
        next()
        
    } catch (error) {
        res.error(responseCode.BAD_REQUEST.name, error.message, responseCode.BAD_REQUEST.code)
    }
}

export const validateTimeCreate = async (req, res, next) => {
    try {

        const now = Date.now() / 1000;
        if (req.body.start_time < now) {
            if (!isSameDateTimestamp(req.body.start_time, now))
                throw new Error(req.__('banner.invalid_start_time'))
        }

        if (req.body.end_time < now) {
            if (!isSameDateTimestamp(req.body.end_time, now))
                throw new Error(req.__('banner.invalid_end_time'))
        }

        if (req.body.start_time >= req.body.end_time)
            throw new Error(req.__('banner.start_time_must_greater_than_end_time'))

        next();
    } catch (error) {
        res.error('VALIDATION_ERROR', error.message, 422)
    }
}

export const validateTimeUpdate = async (req, res, next) => {
    try {
        if (req.body.start_time >= req.body.end_time)
            throw new Error(req.__('banner.start_time_must_greater_than_end_time'))
        next();
    } catch (error) {
        res.error('VALIDATION_ERROR', error.message, 422)
    }
}

export const isBannerOwner = async (req, res, next) => {
    if (req.params.id && req.shop_id) {
        const own = await BannerService._.checkShopOwnBanner(req.shop_id.toString(), req.params.id)
        if (own) next();
        else res.error('NOT_FOUND_ERROR', req.__('banner.banner_not_found'), 404)
    } else next();
}

export const updateBannerRule = createBannerRule;
export const updateBannerSystemRule = createBannerSystemRule;
export const updateTopShopBannerRule = createTopShopBannerSystemRule;
export const updateBannerValidate = createBannerValidate;
