import { executeError } from "../../base/appError";
import responseCode from "../../base/responseCode";
import { ShopRepo } from "../../repositories/ShopRepo";
const { body, validationResult } = require("express-validator");
const shopRepo = ShopRepo.getInstance();

export const findShopMiddleware = async (req, res, next) => {
    try {
        const shop_id = req.params.id || req.body.shop_id || req.shop_id;
        let shop: any = await shopRepo.findOne({ _id: shop_id });
        if (!shop) {
            res.error(responseCode.NOT_FOUND.name, req.__("shop.shop_not_found"), responseCode.NOT_FOUND.code);
            return;
        }
        next();
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}

export const shopSettingValidate = async (req, res, next) => {

    let error: any = {
        name: responseCode.BAD_REQUEST.name,
        message: '',
        statusCode: responseCode.BAD_REQUEST.code
    }
    let existError = false;
    // shop_type
    let shop_type = +req.body.shop_type;
    if (![0, 1].includes(shop_type)) {
        existError = true
        error.message = req.__("shop.type_option");
    }
    // refund_money_mode
    // let refund_money_mode = req.body.refund_money_mode;
    // if (![true, false].includes(refund_money_mode)) {
    //     existError = true

    //     error.message = req.__("shop.refund_mode");
    // }
    // pause_mode
    let pause_mode = req.body.pause_mode;
    if (![true, false].includes(pause_mode)) {
        existError = true
        error.message = req.__("shop.pause_mode");
    }
    // refund_money_regulations
    // let refund_money_regulations = req.body.refund_money_regulations
    // if (refund_money_mode && typeof refund_money_regulations != 'object') {
    //     existError = true
    //     error.message = req.__("shop.refund_money_regulations");
    // }
    if (existError) {
        res.error(error.name, error.message, error.statusCode);
        return;
    }
    next();
}