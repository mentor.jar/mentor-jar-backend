import moment from "moment";

import responseCode from "../../base/responseCode";
import { REQ_DATE_TIME_FORMAT } from "../../base/variable";
import { ProductRepo } from "../../repositories/ProductRepo";
import { VariantRepo } from "../../repositories/Variant/VariantRepo";
import { isMappable } from "../../utils/helper";

const { body, validationResult } = require("express-validator");
const productRepo = ProductRepo.getInstance();
const variantRepo = VariantRepo.getInstance();

export const createGroupBuyRule = [
    body("name")
        .exists().withMessage((value, { req }) => req.__('group_buy.missing_name_group_buy'))
        .isLength({ min: 1, max: 150 }).withMessage((value, { req }) => req.__('group_buy.invalid_name_length_group_buy'))
        .trim(),
    body("start_time")
        .exists().withMessage((value, { req }) => req.__('group_buy.missing_start_time')),
    body("end_time")
        .exists().withMessage((value, { req }) => req.__('group_buy.missing_end_time')),
    body("discount_type")
        .exists().withMessage((value, { req }) => req.__('group_buy.missing_discount_group_buy'))
        .isFloat({ min: 1, max: 50 }).withMessage((value, { req }) => req.__('group_buy.invalid_discount_range_group_buy')),
    body("products")
        .exists().withMessage((value, { req }) => req.__('group_buy.missing_products_in_group_buy'))
        .isArray({ min: 1, max: 1000 }).withMessage((value, { req }) => req.__('group_buy.max_products_in_group_buy'))
]

export const createGroupBuyValidate = (req, res, next) => {
    const error = validationResult(req).formatWith(({ msg }) => msg);
    const hasError = !error.isEmpty();
    if (hasError) {
        res.error('VALIDATION_ERROR', error.array()[0], 422)
    } else next();
}

export const validateTimeCreate = async (req, res, next) => {
    try {
        const { start_time, end_time } = req.body
        const startTime = moment(new Date(start_time), REQ_DATE_TIME_FORMAT, true)
        const endTime = moment(new Date(end_time), REQ_DATE_TIME_FORMAT, true)
        const currentTime = moment(new Date(), REQ_DATE_TIME_FORMAT, true)
        const duration = moment.duration(endTime.diff(startTime));

        if (!startTime.isValid() || !endTime.isValid()) {
            res.error(responseCode.BAD_REQUEST.name, req.__('group_buy.invalid_start_and_end_time'), responseCode.BAD_REQUEST.code);
            return;
        }
        else {
            if (startTime.add(2, "minutes").isBefore(currentTime) || endTime.isBefore(currentTime)) {
                res.error(responseCode.BAD_REQUEST.name, req.__('group_buy.start_time_must_greater_than_now'), responseCode.BAD_REQUEST.code);
                return;
            }
            if (!endTime.isAfter(startTime) || duration.asHours() < 1) {
                res.error(responseCode.BAD_REQUEST.name, req.__('group_buy.end_time_must_greater_than_start_time'), responseCode.BAD_REQUEST.code);
                return;
            }
            if (duration.asDays() > 30) {
                res.error(responseCode.BAD_REQUEST.name, req.__('group_buy.invalid_duration'), responseCode.BAD_REQUEST.code);
                return;
            }
        }
        next();
    } catch (error) {
        res.error('VALIDATION_ERROR', error.message, 422)
    }
}

export const validateGroupsProduct = async (req, res, next) => {
    try {
        const { discount_type, products } = req.body
        let invalidGroupProduct = false
        let invalidMember = false
        let invalidDiscountPercent = false
        if (!products) {
            res.error(responseCode.BAD_REQUEST.name, req.__('group_buy.missing_products'), responseCode.BAD_REQUEST.code);
            return;
        }
        products.map((value) => {
            if (!isMappable(value.groups) || value.groups?.length > 5) {
                invalidGroupProduct = true
            }
            value.groups.map((item) => {
                if (item.number_of_member < 2 || item.number_of_member > 10) {
                    invalidMember = true
                }
                if (discount_type === 'percent' && (item.discount < 1 || item.discount > 99)) {
                    invalidDiscountPercent = true
                }
            })
        })
        if (invalidGroupProduct) {
            res.error(responseCode.BAD_REQUEST.name, req.__('group_buy.invalid_groups'), responseCode.BAD_REQUEST.code);
            return;
        }
        if (invalidMember) {
            res.error(responseCode.BAD_REQUEST.name, req.__('group_buy.invalid_number_of_member'), responseCode.BAD_REQUEST.code);
            return;
        }
        if (invalidDiscountPercent) {
            res.error(responseCode.BAD_REQUEST.name, req.__('group_buy.invalid_discount_percent'), responseCode.BAD_REQUEST.code);
            return;
        }
        next()
    } catch (error) {
        res.error('VALIDATION_ERROR', error.message, 422)
    }
}

export const validateQuantityProduct = async (req, res, next) => {
    try {
        const { products } = req.body
        let invalidProduct = false
        if (!products) {
            res.error(responseCode.BAD_REQUEST.name, req.__('group_buy.missing_products'), responseCode.BAD_REQUEST.code);
            return;
        }
        const promise = products.map((value) => {
            return new Promise(async (resolve) => {
                let minVariantQuantity = 0;
                let product = await productRepo.findById(value._id);
                let variant = await variantRepo.getByProductId(value._id);
                if (isMappable(variant)) {
                    variant.map((item) => {
                        if (!minVariantQuantity) minVariantQuantity = item.quantity
                        if (minVariantQuantity > item.quantity) minVariantQuantity = item.quantity;
                    })
                    if (value.variant_quantity > minVariantQuantity) {
                        invalidProduct = true
                    }
                }
                if (value.quantity > product.quantity) {
                    invalidProduct = true
                }
                resolve(true)
            })
        })
        await Promise.all(promise)
        if (invalidProduct) {
            res.error(responseCode.BAD_REQUEST.name, req.__('group_buy.invalid_quantity'), responseCode.BAD_REQUEST.code);
            return;
        }
        next()
    } catch (error) {
        res.error('VALIDATION_ERROR', error.message, 422)
    }
}