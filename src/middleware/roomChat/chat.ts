import responseCode from "../../base/responseCode";
import { UserRepo } from "../../repositories/UserRepo";

export const createRoomValidate = async (req, res, next) => {

    let error: any = {
        name: responseCode.BAD_REQUEST.name,
        message: '',
        statusCode: responseCode.BAD_REQUEST.code
    }
    let existError = false;

    // pause_mode
    let is_group = req.body.is_group;

    if (![true, false].includes(is_group)) {
        existError = true
        error.message = req.__("room_chat.group");
    }

    // pause_mode
    let type = req.body.type;

    if (!["CHAT", "LIVE STREAM"].includes(type)) {
        existError = true
        error.message = req.__("room_chat.invalid_type");
    }
    let user_ids = req.body.user_ids;
    if (!Array.isArray(user_ids)) {
        existError = true
        error.message = req.__("room_chat.user_ids");
    }

    let users = await UserRepo.getInstance().getUserbyListUserId(user_ids);
    if (type == "CHAT" && user_ids.length != 2) {
        existError = true
        error.message = req.__("room_chat.limit_chat");
    }
    if (users.length != user_ids.length) {
        existError = true
        error.message = req.__("room_chat.not_found_user_id");
    }

    if (existError) {
        res.error(error.name, error.message, error.statusCode);
        return;
    }
    next();
}