import responseCode from "../../base/responseCode";
import { CategoryRepo } from "../../repositories/CategoryRepo";
import { ProductRepo } from "../../repositories/ProductRepo";
import { ShopRepo } from "../../repositories/ShopRepo";

const { check, body, validationResult } = require("express-validator");
const shopRepo = ShopRepo.getInstance();
const productRepo = ProductRepo.getInstance();
const categoryRepo = CategoryRepo.getInstance();

export const categoryCondition = [
    body("name")
        .exists().withMessage((value, { req }) => req.__('category.missing_name_category'))
        .isLength({ min: 1, max: 50 }).withMessage((value, { req }) => req.__('category.invalid_name_length_category'))
        .trim()
]

export const categorySystemCondition = [
    body("name")
        .exists().withMessage((value, { req }) => req.__('category.missing_name_category'))
        .isLength({ min: 1, max: 50 }).withMessage((value, { req }) => req.__('category.invalid_name_length_category'))
        .trim(),
    body("avatar")
        .exists().withMessage((value, { req }) => req.__('category.missing_avatar_category')),
    body("pdfAvatar")
        .exists().withMessage((value, { req }) => req.__('category.missing_pdf_avatar_category'))
]

export const validateBeforeSort = [
    body("category_ids")
        .exists().withMessage((value, { req }) => req.__('category.missing_list_category'))
        .isArray().withMessage((value, { req }) => req.__('category.list_category_must_be_array'))
]

export const validateCondition = (req, res, next) => {
    const error = validationResult(req).formatWith(({ msg }) => msg);

    const hasError = !error.isEmpty();

    if (hasError) {
        res.error('VALIDATION_ERROR', error.array()[0], 422);
        return;
    } else {
        next();
    }
}

export const createShopCategoryCMSMiddleware = async (req, res, next) => {
    try {
        const error = validationResult(req).formatWith(({ msg }) => msg);
        const hasError = !error.isEmpty();
        if (hasError) {
            res.error('VALIDATION_ERROR', error.array()[0], 422);
            return;
        }
        const shop: any = await shopRepo.findOne({ _id: req.body.shop_id });
        if (!shop) {
            res.error(responseCode.NOT_FOUND.name, req.__("shop.shop_not_found"), responseCode.NOT_FOUND.code);
            return;
        }
        const productIds = req.body.product_ids;
        const productItems = await productRepo.find({ _id: { $in: productIds } });
        productItems.forEach(el => {
            if (el.shop_id.toString() !== req.body.shop_id.toString()) {
                throw new Error(req.__("product.product_not_belong_to_shop"));
            }
        });
        next();
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
        return;
    }

}

export const updateShopCategoryCMSMiddleware = async (req, res, next) => {
    try {
        const error = validationResult(req).formatWith(({ msg }) => msg);
        const hasError = !error.isEmpty();
        if (hasError) {
            res.error('VALIDATION_ERROR', error.array()[0], 422);
            return;
        }
        const category: any = await categoryRepo.findOne({ _id: req.params.id });
        if (!category) {
            res.error(responseCode.NOT_FOUND.name, req.__('category.missing_category'), responseCode.NOT_FOUND.code);
            return;
        }
        const productIds = req.body.product_ids;
        if (productIds) {
            const productItems = await productRepo.find({ _id: { $in: productIds } });
            productItems.forEach(el => {
                if (el.shop_id.toString() !== category.shop_id.toString()) {
                    throw new Error(req.__("product.product_not_belong_to_shop"));
                }
            });
        }

        next();
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
        return;
    }

}
