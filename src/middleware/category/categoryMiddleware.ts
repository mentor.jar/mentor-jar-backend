
import { CategoryRepo } from "../../repositories/CategoryRepo";
import { CategoryInfoRepo } from "../../repositories/CategoryInfoRepo";
import responseCode from "../../base/responseCode";
import { NotFoundError } from "../../base/customError";
import { cloneObj } from "../../utils/helper";

const categoryRepo = CategoryRepo.getInstance();
const categoryInfoRepo = CategoryInfoRepo.getInstance();

export const deleteCategoryMiddleware = async (req, res, next) => {
    try {
        const allCategoryId = await categoryRepo.getAllChildCategoryId(req.params.id);
        if (!allCategoryId.length) {
            res.error("NOT_FOUND_ERROR", req.__("category.missing_category"), 404);
            return;
        }
        req.listCategoryId = allCategoryId;
        next();
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
        return;
    }
}

export const checkShopIdMiddleware = async (req, res, next) => {
    try {
        let checkCategory = await categoryRepo.findById(req.params.id);
        if (checkCategory.shop_id.toString() != req.shop_id && req.shop_id) {
            res.error("Not Found", req.__("category.shop_id_not_match"), 404);
            return;
        }
        next();
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
        return;
    }
}

export const findCategoryMiddleware = async (req, res, next) => {
    try {
        const category = await categoryRepo.findById(req.params.id);
        if (!category) {
            res.error("NOT_FOUND_ERROR", req.__("category.missing_category"), 404);
            return;
        }
        next();
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
        return;
    }
}
export const updateCategoryInfoMiddleware = async (req, res, next) => {
    try {
        const category = await categoryRepo.findById(req.params.id);
        const categoryInfo = await categoryInfoRepo.findById(req.params.idInfo);

        if (!category) {
            res.error("NOT_FOUND_ERROR", req.__("category.missing_category"), 404);
            return;
        }
        if (!categoryInfo) {
            res.error("NOT_FOUND_ERROR", req.__("category.missing_category_info"), 404);
            return;
        }
        if (categoryInfo.category_id.toString() != category._id.toString()) {
            res.error("SERVER_ERROR", req.__("category.category_info_not_match_category"), 404);
            return;
        }
        next();
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
        return;
    }
}

export const deleteCategoryInfoMiddleware = async (req, res, next) => {
    try {
        const category = await categoryRepo.findById(req.params.id);

        if (!category) {
            res.error("NOT_FOUND_ERROR", req.__("category.missing_category"), 404);
            return;
        }
        const categoryInfo = await categoryInfoRepo.findById(req.params.idInfo);
        if (!categoryInfo) {
            res.error("NOT_FOUND_ERROR", req.__("category.missing_category_info"), 404);
            return;
        }
        if (categoryInfo.category_id.toString() != category._id.toString()) {
            res.error("SERVER_ERROR", req.__("category.category_info_not_match_category"), 404);
            return;
        }
        next();
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
        return;
    }
}