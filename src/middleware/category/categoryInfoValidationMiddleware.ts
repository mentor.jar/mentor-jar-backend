const { check, body, validationResult } = require("express-validator");

export const categoryInfoCondition = [
    body("name")
        .exists().withMessage((value, { req }) => req.__('category.missing_name_category_info')),
        // .isObject().withMessage((value, { req }) => req.__('category.missing_localized_name_category_info')),
    body("name.vi")
        .exists().withMessage((value, { req }) => req.__('category.missing_localized_name_category_info'))
        .isLength({ min: 1, max: 50 }).withMessage((value, { req }) => req.__('category.invalid_name_length_category_info')),
    body("name.en")
        .exists().withMessage((value, { req }) => req.__('category.missing_localized_name_category_info'))
        .isLength({ min: 1, max: 50 }).withMessage((value, { req }) => req.__('category.invalid_name_length_category_info')),   
    body("name.ko")
        .exists().withMessage((value, { req }) => req.__('category.missing_localized_name_category_info'))
        .isLength({ min: 1, max: 50 }).withMessage((value, { req }) => req.__('category.invalid_name_length_category_info')),
    body("type")
        .exists().withMessage((value, { req }) => req.__('category.missing_type_category_info')),
    body("fashion_type")
        .optional(),
    body("is_required")
        .exists().withMessage((value, { req }) => req.__('category.missing_is_required_category_info')),
    body("is_allow_multiple_values")
        .optional(),
    body("list_option")
        .optional()
        .exists().withMessage((value, { req }) => req.__('category.missing_list_option_category_info')),
    body("list_option.*.vi")
        .isLength({ min: 1}).withMessage((value, { req }) => req.__('product.missing_option_name')),
    body("list_option.*.en")
        .isLength({ min: 1}).withMessage((value, { req }) => req.__('product.missing_option_name')),
    body("list_option.*.ko")
        .isLength({ min: 1}).withMessage((value, { req }) => req.__('product.missing_option_name')),
]
export const createCategoryInfoMiddleware = (req, res, next) => {
    const error = validationResult(req).formatWith(({ msg }) => msg);

    const hasError = !error.isEmpty();

    if (hasError) {
        res.error('VALIDATION_ERROR', error.array()[0], 422)
    } else {
        next();
    }
}
