const { body, validationResult } = require("express-validator");

export const validatorListShop = [
    body("list_shops")
        .isArray().withMessage((value, {req}) => req.__('checkout.invalid_list_shop')),
    body("list_shops.*.shop_id")
        .exists().withMessage((value, { req }) => req.__('checkout.invalid_list_shop'))
        .trim(),
    body("list_shops.*.name")
        .exists().withMessage((value, { req }) => req.__('checkout.invalid_list_shop'))
        .trim(),
    body("list_shops.*.avatar")
        .exists().withMessage((value, { req }) => req.__('checkout.invalid_list_shop'))
        .trim(),
    body("list_shops.*.total_value")
        .optional()
        .isFloat({ min: 1 }).withMessage((value, { req }) => req.__('checkout.invalid_list_shop')),
    body("list_shops.*.products")
        .isArray().withMessage((value, {req}) => req.__('checkout.invalid_list_shop')),
]

export const validationRound = async (req, res, next) => {
    const error = validationResult(req).formatWith(({ msg }) => msg);

    const hasError = !error.isEmpty();

    if (hasError) {
        res.error('VALIDATION_ERROR', error.array()[0], 422)
        return;
    } else {
        next()
    }
}