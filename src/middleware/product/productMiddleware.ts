import responseCode from "../../base/responseCode";
import { ProductRepo } from "../../repositories/ProductRepo";
import { executeError } from "../../base/appError";
import { cloneObj, ObjectId } from "../../utils/helper";
import { VariantRepo } from "../../repositories/Variant/VariantRepo";
import { CartItemRepo } from "../../repositories/CartItemRepo";
import { AddressRepo } from "../../repositories/AddressRepo";
import { ShopRepo } from "../../repositories/ShopRepo";
import { error_response_obj } from "../../base/injector";
import { UserRepo } from "../../repositories/UserRepo";
import { getListDescriptionImages } from "../../controllers/handleRequests/product/product";
import { EN_LANG, KO_LANG, PRODUCTION_WEB_URL, PRODUCTION_WEB_URL_SHORTEN, SIZE_INFOS_HEIGHT_REQUIRE, SIZE_INFOS_WEIGHT_REQUIRE, STAGING_WEB_URL, VIET_NAM } from "../../base/variable";
import { CategoryInfoRepo } from "../../repositories/CategoryInfoRepo";
import { isEqual, omit, uniqBy, isEmpty, pick } from "lodash";
import { SystemSettingRepo } from "../../repositories/SystemSettingRepo";
import { removeObjectReplacementCharacter } from "../../utils/stringUtil";

const productRepo = ProductRepo.getInstance();
const variantRepo = VariantRepo.getInstance()
const cartItemRepo = CartItemRepo.getInstance()
const shopRepo = ShopRepo.getInstance()
const cateInfoRepo = CategoryInfoRepo.getInstance();
const systemSettingRepo = SystemSettingRepo.getInstance();

const { body, validationResult } = require("express-validator");

// Tạm bỏ qua độ dài của validate name & description
export const productCondition = [
    body("name")
        .exists().withMessage((value, { req }) => req.__('product.missing_product_name'))
        // .isLength({ min: 10, max: 200 }).withMessage((value, { req }) => req.__('product.invalid_name_length_product'))
        .replace("/\uFFFC/", '')
        .trim(),
    body("description")
        .exists().withMessage((value, { req }) => req.__('product.missing_product_description'))
        // .isLength({ min: 100, max: 3000 }).withMessage((value, { req }) => req.__('product.invalid_description_length_product'))
        .trim(),
    body("category_id")
        .exists().withMessage((value, { req }) => req.__('product.missing_product_category_id')),
    body("images")
        .exists().withMessage((value, { req }) => req.__('product.missing_product_images'))
        .isArray({ min: 1, max: 15 }).withMessage((value, { req }) => req.__('product.invalid_images_length_product')),
    body("list_category_id")
        .exists().withMessage((value, { req }) => req.__('product.missing_product_list_category'))
        .isArray({ min: 1 }).withMessage((value, { req }) => req.__('product.invalid_list_category_product')),
    body("weight")
        .exists().withMessage((value, { req }) => req.__('product.missing_product_weight'))
        .isFloat({ min: 1, max: 1_000_000 }).withMessage((value, { req }) => req.__('product.invalid_weight_of_product')),
    body("height")
        .optional()
        .isFloat({ min: 1, max: 1_000_000 }).withMessage((value, { req }) => req.__('product.invalid_height_of_product')),
    body("width")
        .optional()
        .isFloat({ min: 1, max: 1_000_000 }).withMessage((value, { req }) => req.__('product.invalid_width_of_product')),
    body("length")
        .optional()
        .isFloat({ min: 1, max: 1_000_000 }).withMessage((value, { req }) => req.__('product.invalid_length_of_product')),
    body("detail_size")
        .optional()
        .isArray().withMessage((value, {req}) => req.__('product.invalid_list_detail_size')),
    body("detail_size.*.type_size")
        .optional()
        .isLength({ min: 1}).withMessage((value, { req }) => req.__('product.missing_detail_type_size_name'))
        .trim(),
    body("detail_size.*.size_infos")
        .optional()
        .isArray({ min: 1}).withMessage((value, {req}) => req.__('product.invalid_list_size_infos')),
    body("detail_size.*.size_infos.*.min")
        .optional()
        .isFloat({ min: 1, max: 1_000_000 }).withMessage((value, {req}) => req.__('product.invalid_list_detail_size')),
    body("detail_size.*.size_infos.*.max")
        .optional()
        .isFloat({ min: 1, max: 1_000_000 }).withMessage((value, {req}) => req.__('product.invalid_list_detail_size')),
    body("refund_conditions")
        .optional()
        .isArray().withMessage((value, {req}) => req.__('product.invalid_refund_conditions')),
    body("custom_images")
        .optional()
        .isArray(),
    body("is_guaranteed_item")
        .optional()
        .isBoolean(),
    body("is_genuine_item")
        .optional()
        .isBoolean(),
    body("authen_images")
        .optional()
        .isArray().withMessage((value, {req}) => req.__('product.missing_authen_images')),
]

export const detailSizeCondition = [
    body("category_id")
        .notEmpty()
        .withMessage((value, {req}) => req.__('product.invalid_category_id')),  
    body("array_size")
        .isArray({ min: 1}).withMessage((value, {req}) => req.__('product.invalid_array_type_size'))
]

export const getDetailSizeMiddleware = async (req, res, next) => {
    const error = validationResult(req).formatWith(({ msg }) => msg);

    const hasError = !error.isEmpty();

    if (hasError) {
        res.error('VALIDATION_ERROR', error.array()[0], 422)
    } else {
        next();
    }
}

export const requireSizeInfoInDetailSize = async (req, res, next) => {
    const { detail_size } = req.body;
    if (detail_size && Array.isArray(detail_size)) {
        detail_size.forEach((sizeInstance) => {
            if (
                sizeInstance?.size_infos &&
                Array.isArray(sizeInstance?.size_infos)
            ) {
                const existAttributor = sizeInstance?.size_infos.filter(
                    (item) =>
                        (item.name === SIZE_INFOS_HEIGHT_REQUIRE.vi_name ||
                            item.name === SIZE_INFOS_HEIGHT_REQUIRE.ko_name ||
                            item.name === SIZE_INFOS_WEIGHT_REQUIRE.vi_name ||
                            item.name === SIZE_INFOS_WEIGHT_REQUIRE.ko_name) &&
                        item.min &&
                        item.max
                );
                if (existAttributor?.length < 2) {
                    res.error(
                        responseCode.MISSING.name,
                        req.__(
                            'product.missing_height_and_weight_of_detail_size'
                        ),
                        responseCode.MISSING.code
                    );
                    return;
                }
            }
        });
    }
    next();
}

export const validateQuantityProduct = async (req, res, next) => {
    try {
        const { variants, quantity_ware_house } = req.body
        if(variants && variants.length) {
            const totalQuantity = variants.reduce((sum, item: any) => sum + item.quantity, 0)
            if(totalQuantity <= 0) {
                res.error(responseCode.BAD_REQUEST.name, req.__("product.invalid_quantity"), responseCode.BAD_REQUEST.code)
                return;
            }
            next()
        }
        else {
            if(+quantity_ware_house <= 0) {
                res.error(responseCode.BAD_REQUEST.name, req.__("product.missing_quantity_of_product_no_variant"), responseCode.BAD_REQUEST.code)
                return;
            }
            next()
        }
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}

export const createProductMiddleware = async (req, res, next) => {
    const error = validationResult(req).formatWith(({ msg }) => msg);

    const hasError = !error.isEmpty();

    if (hasError) {
        res.error('VALIDATION_ERROR', error.array()[0], 422)
    } else {
        const language = req.headers["accept-language"]
        // Check description if product not from VN
        const { shop } = req.user
        const { name, description } = req.body
        req.body.name = removeObjectReplacementCharacter(name);
        req.body.description = removeObjectReplacementCharacter(description);
        if (shop.country === VIET_NAM && (description.length < 100 || description > 3000)) {
            res.error(responseCode.BAD_REQUEST.name, req.__('product.invalid_description_length_product'), responseCode.BAD_REQUEST.code)
            return;
        }

        // Check hàng chính hãng & Hàng đảm bảo. Chỉ bật 1 trong 2
        if(req.body.is_guaranteed_item === true && req.body.is_genuine_item === true) {
            res.error(responseCode.BAD_REQUEST.name, req.__('product.invalid_guaranteed_select'), responseCode.BAD_REQUEST.code)
            return;
        }

        // Check product_url
        try {
            const { friendly_url } = req.body
    
            if(friendly_url && process.env.NODE_ENV === 'staging') {
                const url = new URL(friendly_url)
                if(url.origin !== STAGING_WEB_URL) {
                    res.error(responseCode.BAD_REQUEST.name, req.__('product.invalid_product_link'), responseCode.BAD_REQUEST.code)
                    return;
                }
            }
            else if (friendly_url && process.env.NODE_ENV === 'production') {
                const url = new URL(friendly_url)
                if (url.origin !== PRODUCTION_WEB_URL && url.origin !== PRODUCTION_WEB_URL_SHORTEN) {
                    res.error(responseCode.BAD_REQUEST.name, req.__('product.invalid_product_link'), responseCode.BAD_REQUEST.code)
                    return;
                }
            }
        } catch (error) {
            res.error(responseCode.BAD_REQUEST.name, req.__('product.invalid_product_link'), responseCode.BAD_REQUEST.code)
            return;
        }

        // Continue validate some field
        if (!req.body.variants || !req.body.variants.length) {
            if (!req.body.price) {
                res.error(responseCode.MISSING.name, req.__("product.missing_price_of_product_no_variant"), responseCode.MISSING.code);
                return;
            }
            if (!req.body.quantity_ware_house) {
                res.error(responseCode.MISSING.name, req.__("product.missing_quantity_of_product_no_variant"), responseCode.MISSING.code);
                return;
            }
        }
        else {
            if (!req.body.option_types || !req.body.option_types.length) {
                res.error(responseCode.MISSING.name, req.__("product.missing_option_type_of_product_have_variant"), responseCode.MISSING.code);
                return;
            }
        }

        // Validate product detail info
        const productDetailInfos = req.body.product_detail_infos
        const arrProductDetailInfos = []
        if (productDetailInfos && productDetailInfos.length) {
            const cateInfoId = productDetailInfos.map(item => ObjectId(item.category_info_id))
            let cateInfos:Array<any> = await cateInfoRepo.find({_id: {$in: cateInfoId}})
            cateInfos = cloneObj(cateInfos)
            let isValidProductDetailInfo = true;
            productDetailInfos.forEach(productDetailInfo => {
                if(!productDetailInfo.value) {
                    productDetailInfo.value = ""
                }
                const item = cateInfos.find((cateInfo => cateInfo._id?.toString() === productDetailInfo.category_info_id?.toString()))
                if(!item) {
                    isValidProductDetailInfo = false;
                    return;
                }
                if(item && item.is_required && !productDetailInfo.value && !productDetailInfo.values) {
                    isValidProductDetailInfo = false;
                    return;
                }

                // Format language: Always save vi version
                if(item.type === "custom" || item.type === "select") {
                    const itemValue = item.list_option.find(option => {
                        return language === KO_LANG ? option.ko === productDetailInfo.value
                        : language === EN_LANG ? option.en === productDetailInfo.value
                        : option.vi === productDetailInfo.value
                    })
                    if(itemValue) {
                        productDetailInfo.value = itemValue.vi
                    }
                    // for values array
                    if(productDetailInfo.values) {
                        productDetailInfo.values = productDetailInfo.values.map(val => {
                            
                            const itemValue = item.list_option.find(option => {
                                return language === KO_LANG ? option.ko === val
                                : language === EN_LANG ? option.en === val
                                : option.vi === val
                            })
                            
                            return itemValue?.vi || val
                        })
                    }
                    else {
                        productDetailInfo.values = [productDetailInfo.value]
                    }
                }
                else {
                    productDetailInfo.values = [productDetailInfo.value]
                } 
            });
            if (!isValidProductDetailInfo) {
                res.error(responseCode.NOT_FOUND.name, req.__("product.missing_product_info"), responseCode.NOT_FOUND.code);
                return;
            }
        }

        const addresses = await AddressRepo.getInstance().findOne({ accessible_id: req.user._id.toString(), is_pick_address_default: true });
        if (!addresses) {
            res.error(responseCode.NOT_FOUND.name, req.__("product.missing_address_of_shop"), responseCode.NOT_FOUND.code);
            return;
        }

        const { allow_refund, duration_refund } = req.body

        if (allow_refund && +duration_refund <= 0 || +duration_refund > 15) {
            res.error(responseCode.MISSING.name, req.__("product.invalid_duration_refund"), responseCode.MISSING.code);
            return;
        }

        // custom images
        let { custom_images, images } = req.body
        if(custom_images) {
            if(custom_images.length !== images.length) {
                res.error(responseCode.BAD_REQUEST.name, req.__("product.invalid_custom_image_length"), responseCode.BAD_REQUEST.code);
                return;
            }
            else {
                let validCustomImage = true
                custom_images = custom_images.map(item => {
                    if(!('x_percent_offset' in item) || +item.x_percent_offset < 0 || item.x_percent_offset > 100) {
                        item.x_percent_offset = null
                    }
                    if(!('y_percent_offset' in item) || +item.y_percent_offset < 0 || item.y_percent_offset > 100) {
                        item.y_percent_offset = null
                    }
                    return item
                })
                custom_images.forEach(image => {
                    if (!image.url || !image.url.length) {
                        validCustomImage = false
                    }
                })
                if(!validCustomImage) {
                    res.error(responseCode.BAD_REQUEST.name, req.__("product.invalid_custom_image_data"), responseCode.BAD_REQUEST.code);
                    return;
                }
            }
        }

        const { authen_images, is_genuine_item } = req.body;
        if(is_genuine_item && (!authen_images || authen_images.length === 0)) {
            res.error(responseCode.BAD_REQUEST.name, req.__("product.missing_authen_images"), responseCode.BAD_REQUEST.code);
            return;
        }

        let { description_images } = req.body;

        await getListDescriptionImages(req, description_images)
        .then ((result) => {
            req.body.description_images = result
            next();
        })
        .catch((error) => {
            res.error(responseCode.MISSING.name, error.message, responseCode.MISSING.code);
        })
    }
}

export const createProductCMSMiddleware = async (req, res, next) => {
    const error = validationResult(req).formatWith(({ msg }) => msg);

    const hasError = !error.isEmpty();

    if (hasError) {
        res.error('VALIDATION_ERROR', error.array()[0], 422)
    } else {
        // Continue validate some field
        const language = req.headers["accept-language"]
        if (!req.body.variants || !req.body.variants.length) {
            if (!req.body.price) {
                res.error(responseCode.MISSING.name, req.__("product.missing_price_of_product_no_variant"), responseCode.MISSING.code);
                return;
            }
            if (!req.body.quantity_ware_house) {
                res.error(responseCode.MISSING.name, req.__("product.missing_quantity_of_product_no_variant"), responseCode.MISSING.code);
                return;
            }
        }
        else {
            if (!req.body.option_types || !req.body.option_types.length) {
                res.error(responseCode.MISSING.name, req.__("product.missing_option_type_of_product_have_variant"), responseCode.MISSING.code);
                return;
            }
        }

        let user: any = await UserRepo.getInstance().findById(req.body.user_id);
        if (!user) {
            return res.status(responseCode.NOT_FOUND.code).send(
                error_response_obj(
                    req.__("controller.shop_not_found"),
                    responseCode.NOT_FOUND.name
                )
            );
        }

        let shop: any = await ShopRepo.getInstance().findOrCreateByUser(user);
        if (!shop) {
            return res.status(responseCode.NOT_FOUND.code).send(
                error_response_obj(
                    req.__("controller.shop_not_found"),
                    responseCode.NOT_FOUND.name
                )
            );
        }
        else {
            // Check description if product not from VN
            const { description } = req.body
            if (shop.country === VIET_NAM && (description.length < 100 || description > 3000)) {
                res.error(responseCode.BAD_REQUEST.name, req.__('product.invalid_description_length_product'), responseCode.BAD_REQUEST.code)
                return;
            }

            // Check product_url
            try {
                const { friendly_url } = req.body
        
                if(friendly_url && process.env.NODE_ENV === 'staging') {
                    const url = new URL(friendly_url)
                    if(url.origin !== STAGING_WEB_URL) {
                        res.error(responseCode.BAD_REQUEST.name, req.__('product.invalid_product_link'), responseCode.BAD_REQUEST.code)
                        return;
                    }
                }
                else if (friendly_url && process.env.NODE_ENV === 'production') {
                    const url = new URL(friendly_url)
                    if (url.origin !== PRODUCTION_WEB_URL && url.origin !== PRODUCTION_WEB_URL_SHORTEN) {
                        res.error(responseCode.BAD_REQUEST.name, req.__('product.invalid_product_link'), responseCode.BAD_REQUEST.code)
                        return;
                    }
                }
            } catch (error) {
                res.error(responseCode.BAD_REQUEST.name, req.__('product.invalid_product_link'), responseCode.BAD_REQUEST.code)
                return;
            }

        }

        try {
            req.shop_id = shop._id;
            await ShopRepo.getInstance().actionAfterCreateShop(shop.user_id.toString(), shop._id.toString());
        } catch (error) {
            return res.status(responseCode.SERVER.code).send(
                error_response_obj(
                    req.__("controller.error_occurs"),
                    responseCode.SERVER.name
                )
            );
        }

        // Validate product detail info
        const productDetailInfos = req.body.product_detail_infos
        const arrProductDetailInfos = []
        if (productDetailInfos && productDetailInfos.length) {
            const cateInfoId = productDetailInfos.map(item => ObjectId(item.category_info_id))
            let cateInfos:Array<any> = await cateInfoRepo.find({_id: {$in: cateInfoId}})
            cateInfos = cloneObj(cateInfos)
            let isValidProductDetailInfo = true;
            productDetailInfos.forEach(productDetailInfo => {
                if(!productDetailInfo.value) {
                    productDetailInfo.value = ""
                }
                const item = cateInfos.find((cateInfo => cateInfo._id?.toString() === productDetailInfo.category_info_id?.toString()))
                if(!item) {
                    isValidProductDetailInfo = false;
                    return;
                }
                if(item && item.is_required && !productDetailInfo.value && !productDetailInfo.values) {
                    isValidProductDetailInfo = false;
                    return;
                }

                // Format language: Always save vi version
                if(item.type === "custom" || item.type === "select") {
                    const itemValue = item.list_option.find(option => {
                        return language === KO_LANG ? option.ko === productDetailInfo.value
                        : language === EN_LANG ? option.en === productDetailInfo.value
                        : option.vi === productDetailInfo.value
                    })
                    if(itemValue) {
                        productDetailInfo.value = itemValue.vi
                    }
                    // for values array
                    if(productDetailInfo.values) {
                        productDetailInfo.values = productDetailInfo.values.map(val => {
                            
                            const itemValue = item.list_option.find(option => {
                                return language === KO_LANG ? option.ko === val
                                : language === EN_LANG ? option.en === val
                                : option.vi === val
                            })
                            
                            return itemValue?.vi || val
                        })
                    }
                    else {
                        productDetailInfo.values = [productDetailInfo.value]
                    }
                }
                else {
                    productDetailInfo.values = [productDetailInfo.value]
                } 
            });
            if (!isValidProductDetailInfo) {
                res.error(responseCode.NOT_FOUND.name, req.__("product.missing_product_info"), responseCode.NOT_FOUND.code);
                return;
            }
        }

        const addresses = await AddressRepo.getInstance().findOne({ accessible_id: shop.user_id.toString(), is_pick_address_default: true });
        if (!addresses) {
            res.error(responseCode.NOT_FOUND.name, req.__("product.missing_address_of_shop"), responseCode.NOT_FOUND.code);
            return;
        }

        const { allow_refund, duration_refund } = req.body
        
        if (allow_refund && +duration_refund <= 0 || +duration_refund > 15) {
            res.error(responseCode.MISSING.name, req.__("product.invalid_duration_refund"), responseCode.MISSING.code);
            return;
        }

        let { custom_images, images } = req.body
        if(custom_images) {
            if(custom_images.length !== images.length) {
                res.error(responseCode.BAD_REQUEST.name, req.__("product.invalid_custom_image_length"), responseCode.BAD_REQUEST.code);
                return;
            }
            else {
                let validCustomImage = true
                custom_images = custom_images.map(item => {
                    if(!('x_percent_offset' in item) || +item.x_percent_offset < 0 || item.x_percent_offset > 100) {
                        item.x_percent_offset = null
                    }
                    if(!('y_percent_offset' in item) || +item.y_percent_offset < 0 || item.y_percent_offset > 100) {
                        item.y_percent_offset = null
                    }
                    return item
                })
                custom_images.forEach(image => {
                    if (!image.url || !image.url.length) {
                        validCustomImage = false
                    }
                })
                if(!validCustomImage) {
                    res.error(responseCode.BAD_REQUEST.name, req.__("product.invalid_custom_image_data"), responseCode.BAD_REQUEST.code);
                    return;
                }
            }
        }

        const { authen_images, is_genuine_item } = req.body;
        if(is_genuine_item && (!authen_images || authen_images.length === 0)) {
            res.error(responseCode.BAD_REQUEST.name, req.__("product.missing_authen_images"), responseCode.BAD_REQUEST.code);
            return;
        }

        const { description_images } = req.body
        await getListDescriptionImages(req, description_images)
        .then ((result) => {
            req.body.description_images = result
            next();
        })
        .catch((error) => {
            res.error(responseCode.MISSING.name, error.message, responseCode.MISSING.code);
        })
    }
}

export const updateProductCMSMiddleware = async (req, res, next) => {
    const error = validationResult(req).formatWith(({ msg }) => msg);

    const hasError = !error.isEmpty();

    if (hasError) {
        res.error('VALIDATION_ERROR', error.array()[0], 422)
    } else {
        // Continue validate some field
        const language = req.headers["accept-language"]
        if (!req.body.variants || !req.body.variants.length) {
            if (!req.body.price) {
                res.error(responseCode.MISSING.name, req.__("product.missing_price_of_product_no_variant"), responseCode.MISSING.code);
                return;
            }
            if (!req.body.quantity_ware_house) {
                res.error(responseCode.MISSING.name, req.__("product.missing_quantity_of_product_no_variant"), responseCode.MISSING.code);
                return;
            }
        }
        else {
            if (!req.body.option_types || !req.body.option_types.length) {
                res.error(responseCode.MISSING.name, req.__("product.missing_option_type_of_product_have_variant"), responseCode.MISSING.code);
                return;
            }
        }

        let user: any = await UserRepo.getInstance().findById(req.body.user_id);
        if (!user) {
            return res.status(responseCode.NOT_FOUND.code).send(
                error_response_obj(
                    req.__("controller.shop_not_found"),
                    responseCode.NOT_FOUND.name
                )
            );
        }

        let shop: any = await ShopRepo.getInstance().findOrCreateByUser(user);
        if (!shop) {
            return res.status(responseCode.NOT_FOUND.code).send(
                error_response_obj(
                    req.__("controller.shop_not_found"),
                    responseCode.NOT_FOUND.name
                )
            );
        }
        else {
            // Check description if product not from VN
            const { description } = req.body
            if (shop.country === VIET_NAM && (description.length < 100 || description > 3000)) {
                res.error(responseCode.BAD_REQUEST.name, req.__('product.invalid_description_length_product'), responseCode.BAD_REQUEST.code)
                return;
            }

            // Check product_url
            try {
                const { friendly_url } = req.body
        
                if(friendly_url && process.env.NODE_ENV === 'staging') {
                    const url = new URL(friendly_url)
                    if(url.origin !== STAGING_WEB_URL) {
                        res.error(responseCode.BAD_REQUEST.name, req.__('product.invalid_product_link'), responseCode.BAD_REQUEST.code)
                        return;
                    }
                }
                else if (friendly_url && process.env.NODE_ENV === 'production') {
                    const url = new URL(friendly_url)
                    if (url.origin !== PRODUCTION_WEB_URL && url.origin !== PRODUCTION_WEB_URL_SHORTEN) {
                        res.error(responseCode.BAD_REQUEST.name, req.__('product.invalid_product_link'), responseCode.BAD_REQUEST.code)
                        return;
                    }
                }
            } catch (error) {
                res.error(responseCode.BAD_REQUEST.name, req.__('product.invalid_product_link'), responseCode.BAD_REQUEST.code)
                return;
            }
        }

        try {
            req.shop_id = shop._id;
            await ShopRepo.getInstance().actionAfterCreateShop(shop.user_id.toString(), shop._id.toString());
        } catch (error) {
            return res.status(responseCode.SERVER.code).send(
                error_response_obj(
                    req.__("controller.error_occurs"),
                    responseCode.SERVER.name
                )
            );
        }

        // Validate product detail info
        const productDetailInfos = req.body.product_detail_infos
        const arrProductDetailInfos = []
        if (productDetailInfos && productDetailInfos.length) {
            const cateInfoId = productDetailInfos.map(item => ObjectId(item.category_info_id))
            let cateInfos:Array<any> = await cateInfoRepo.find({_id: {$in: cateInfoId}})
            cateInfos = cloneObj(cateInfos)
            let isValidProductDetailInfo = true;
            productDetailInfos.forEach(productDetailInfo => {
                if(!productDetailInfo.value) {
                    productDetailInfo.value = ""
                }
                const item = cateInfos.find((cateInfo => cateInfo._id?.toString() === productDetailInfo.category_info_id?.toString()))
                if(!item) {
                    isValidProductDetailInfo = false;
                    return;
                }
                if(item && item.is_required && !productDetailInfo.value && !productDetailInfo.values) {
                    isValidProductDetailInfo = false;
                    return;
                }

                // Format language: Always save vi version
                if(item.type === "custom" || item.type === "select") {
                    const itemValue = item.list_option.find(option => {
                        return language === KO_LANG ? option.ko === productDetailInfo.value
                        : language === EN_LANG ? option.en === productDetailInfo.value
                        : option.vi === productDetailInfo.value
                    })
                    if(itemValue) {
                        productDetailInfo.value = itemValue.vi
                    }
                    // for values array
                    if(productDetailInfo.values) {
                        productDetailInfo.values = productDetailInfo.values.map(val => {

                            const itemValue = item.list_option.find(option => {
                                return language === KO_LANG ? option.ko === val
                                : language === EN_LANG ? option.en === val
                                : option.vi === val
                            })
                            
                            return itemValue?.vi || val
                        })
                    }
                    else {
                        productDetailInfo.values = [productDetailInfo.value]
                    }
                }
                else {
                    productDetailInfo.values = [productDetailInfo.value]
                } 
            });
            if (!isValidProductDetailInfo) {
                res.error(responseCode.NOT_FOUND.name, req.__("product.missing_product_info"), responseCode.NOT_FOUND.code);
                return;
            }
        }

        const addresses = await AddressRepo.getInstance().findOne({ accessible_id: shop.user_id.toString(), is_pick_address_default: true });
        if (!addresses) {
            res.error(responseCode.NOT_FOUND.name, req.__("product.missing_address_of_shop"), responseCode.NOT_FOUND.code);
            return;
        }

        const { allow_refund, duration_refund } = req.body
        
        if (allow_refund && +duration_refund <= 0 || +duration_refund > 15) {
            res.error(responseCode.MISSING.name, req.__("product.invalid_duration_refund"), responseCode.MISSING.code);
            return;
        }

        let { custom_images, images } = req.body
        if(custom_images) {
            if(custom_images.length !== images.length) {
                res.error(responseCode.BAD_REQUEST.name, req.__("product.invalid_custom_image_length"), responseCode.BAD_REQUEST.code);
                return;
            }
            else {
                let validCustomImage = true
                custom_images = custom_images.map(item => {
                    if(!('x_percent_offset' in item) || +item.x_percent_offset < 0 || item.x_percent_offset > 100) {
                        item.x_percent_offset = null
                    }
                    if(!('y_percent_offset' in item) || +item.y_percent_offset < 0 || item.y_percent_offset > 100) {
                        item.y_percent_offset = null
                    }
                    return item
                })
                custom_images.forEach(image => {
                    if (!image.url || !image.url.length) {
                        validCustomImage = false
                    }
                })
                if(!validCustomImage) {
                    res.error(responseCode.BAD_REQUEST.name, req.__("product.invalid_custom_image_data"), responseCode.BAD_REQUEST.code);
                    return;
                }
            }
        }

        const { authen_images, is_genuine_item } = req.body;
        if(is_genuine_item && (!authen_images || authen_images.length === 0)) {
            res.error(responseCode.BAD_REQUEST.name, req.__("product.missing_authen_images"), responseCode.BAD_REQUEST.code);
            return;
        }

        const { description_images } = req.body
        await getListDescriptionImages(req, description_images)
        .then ((result) => {
            req.body.description_images = result
            next();
        })
        .catch((error) => {
            res.error(responseCode.MISSING.name, error.message, responseCode.MISSING.code);
        })
    }
}

export const findProductMiddleware = async (req, res, next) => {
    try {
        let product: any = await productRepo.findOrFail({
            _id: req.params.id, $or: [
                { deleted_at: { $exists: false } },
                { deleted_at: null }
            ]
        });
        product = cloneObj(product)
        if (product.shop_id != req.shop_id && req.shop_id) {
            res.error(responseCode.NOT_FOUND.name, req.__("product.not_exist_product"), responseCode.NOT_FOUND.code);
            return;
        }
        req.product = product
        next();
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}

export const findProductBuyerMiddleware = async (req, res, next) => {
    try {
        const result = await productRepo.findOrFail({
            _id: req.params.id, $or: [
                { deleted_at: { $exists: false } },
                { deleted_at: null }
            ]
        });
        req.product = result
        next();
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}

// Only use for add product to cart
export const findVariantBuyerMiddleware = async (req, res, next) => {
    try {
        const { variant_id, product_id, quantity } = req.body;
        let totalQuantity = quantity
        if (variant_id && product_id) {
            const existCartItem: any = await cartItemRepo.findOne({
                user_id: ObjectId(req.user._id),
                variant_id: ObjectId(variant_id)
            })
            if (existCartItem) totalQuantity += existCartItem.quantity
            const variant = await variantRepo.findWithProduct(variant_id, product_id);
            if (!variant[0] || !variant[0].product) {
                res.error(responseCode.NOT_FOUND.name, req.__("product.not_exist_variant_to_buy"), responseCode.NOT_FOUND.code);
                return;
            }
            if (totalQuantity > variant[0].quantity) {
                res.error(responseCode.NOT_FOUND.name, req.__("product.not_enough_quantity_to_buy"), responseCode.NOT_FOUND.code);
                return;
            }
            if (variant[0] && variant[0].product) {
                const shopID = variant[0].product.shop_id
                const shop: any = await shopRepo.findOne({ _id: ObjectId(shopID) })
                if (shop.user_id.toString() === req.user._id.toString()) {
                    res.error(responseCode.NOT_FOUND.name, req.__("product.can_not_buy_your_own_product"), responseCode.NOT_FOUND.code);
                    return;
                }
                // Check pause mode of shop
                if (shop.pause_mode) {
                    res.error(responseCode.NOT_FOUND.name, req.__("shop.can_not_buy_if_shop_is_paused"), responseCode.NOT_FOUND.code);
                    return;
                }
                if (variant[0].product.is_sold_out) {
                    res.error(responseCode.BAD_REQUEST.name, req.__("order.invalid_product_to_order", variant[0].product.name), responseCode.BAD_REQUEST.code);
                    return;
                }
            }
            req.cart_item = {
                type: 'Variant',
                data: {
                    ...variant[0],
                    add_quantity: quantity
                }
            }
        }
        else if (product_id) {
            const existCartItem: any = await cartItemRepo.findOne({
                user_id: ObjectId(req.user._id),
                product_id: ObjectId(product_id)
            })
            if (existCartItem) totalQuantity += existCartItem.quantity
            const product = await productRepo.findProductForAddToCart(product_id);
            if (!product[0]) {
                res.error(responseCode.NOT_FOUND.name, req.__("product.not_exist_product_to_buy"), responseCode.NOT_FOUND.code);
                return;
            }
            else if (product[0].variants.length > 0) {
                res.error(responseCode.NOT_FOUND.name, req.__("product.missing_variant_to_buy"), responseCode.NOT_FOUND.code);
                return;
            }
            if (totalQuantity > product[0].quantity) {
                res.error(responseCode.NOT_FOUND.name, req.__("product.not_enough_quantity_to_buy"), responseCode.NOT_FOUND.code);
                return;
            }
            if (product[0]) {
                const shopID = product[0].shop_id
                const shop: any = await shopRepo.findOne({ _id: ObjectId(shopID) })
                if (shop.user_id.toString() === req.user._id.toString()) {
                    res.error(responseCode.NOT_FOUND.name, req.__("product.can_not_buy_your_own_product"), responseCode.NOT_FOUND.code);
                    return;
                }
                // Check pause mode of shop
                if (shop.pause_mode) {
                    res.error(responseCode.NOT_FOUND.name, req.__("shop.can_not_buy_if_shop_is_paused"), responseCode.NOT_FOUND.code);
                    return;
                }
                if (product[0].is_sold_out) {
                    res.error(responseCode.BAD_REQUEST.name, req.__("order.invalid_product_to_order", product[0].name), responseCode.BAD_REQUEST.code);
                    return;
                }
            }
            req.cart_item = {
                type: 'Product',
                data: {
                    ...product[0],
                    add_quantity: quantity
                }
            }
        }
        else {
            res.error(responseCode.MISSING.name, req.__("product.unknow_product_to_buy"), responseCode.MISSING.code);
            return;
        }
        next()
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}

export const checkBeforeUpdateVariant = async (req, res, next) => {
    const { variant_id, product_id, quantity } = req.body;
    const id = req.params.id
    if (!quantity || isNaN(+quantity) || +quantity <= 0) {
        res.error(responseCode.BAD_REQUEST.name, req.__("product.invalid_quantity_type"), responseCode.BAD_REQUEST.code);
        return;
    }

    if (variant_id && product_id) {
        const variant = await variantRepo.findWithProduct(variant_id, product_id);
        let totalQuantity = quantity
        if (!variant[0] || !variant[0].product) {
            res.error(responseCode.NOT_FOUND.name, req.__("product.not_exist_variant_to_buy"), responseCode.NOT_FOUND.code);
            return;
        }
        const { cartItem } = req
        if (!cartItem) {
            res.error(responseCode.BAD_REQUEST.name, req.__("product.not_exist_product_variant"), responseCode.BAD_REQUEST.code)
            return;
        }
        if (totalQuantity > variant[0].quantity) {
            res.error(responseCode.NOT_FOUND.name, req.__("product.not_enough_quantity_to_buy"), responseCode.NOT_FOUND.code);
            return;
        }
        req.cartItem = cartItem
    }
    else {
        res.error(responseCode.MISSING.name, req.__("product.unknow_variant_to_update_cart"), responseCode.MISSING.code);
        return;
    }
    next()
}

export const productUpdateCondition = [
    body("name")
        .exists().withMessage((value, { req }) => req.__('product.missing_product_name'))
        //.isLength({ min: 10, max: 200 }).withMessage((value, { req }) => req.__('product.invalid_name_length_product'))
        .trim(),
    body("description")
        .exists().withMessage((value, { req }) => req.__('product.missing_product_description'))
        // .isLength({ min: 100, max: 3000 }).withMessage((value, { req }) => req.__('product.invalid_description_length_product'))
        .trim(),
    body("category_id")
        .exists().withMessage((value, { req }) => req.__('product.missing_product_category_id')),
    body("images")
        .exists().withMessage((value, { req }) => req.__('product.missing_product_images'))
        .isArray({ min: 1, max: 15 }).withMessage((value, { req }) => req.__('product.invalid_images_length_product')),
    body("list_category_id")
        .exists().withMessage((value, { req }) => req.__('product.missing_product_list_category'))
        .isArray({ min: 1 }).withMessage((value, { req }) => req.__('product.invalid_list_category_product')),
    body("weight")
        .exists().withMessage((value, { req }) => req.__('product.missing_product_weight'))
        .isFloat({ min: 1, max: 1_000_000 }).withMessage((value, { req }) => req.__('product.invalid_weight_of_product')),
    body("height")
        .optional()
        .isFloat({ min: 1, max: 1_000_000 }).withMessage((value, { req }) => req.__('product.invalid_height_of_product')),
    body("width")
        .optional()
        .isFloat({ min: 1, max: 1_000_000 }).withMessage((value, { req }) => req.__('product.invalid_width_of_product')),
    body("length")
        .optional()
        .isFloat({ min: 1, max: 1_000_000 }).withMessage((value, { req }) => req.__('product.invalid_length_of_product')),
    body("detail_size")
        .optional()
        .isArray().withMessage((value, {req}) => req.__('product.invalid_list_detail_size')),
    body("detail_size.*.type_size")
        .optional()
        .isLength({ min: 1}).withMessage((value, { req }) => req.__('product.missing_detail_type_size_name'))
        .trim(),
    body("detail_size.*.size_infos")
        .optional()
        .isArray({ min: 1}).withMessage((value, {req}) => req.__('product.invalid_list_size_infos')),
    body("detail_size.*.size_infos.*.min")
        .optional()
        .isFloat({ min: 1, max: 1_000_000 }).withMessage((value, {req}) => req.__('product.invalid_list_detail_size')),
    body("detail_size.*.size_infos.*.max")
        .optional()
        .isFloat({ min: 1, max: 1_000_000 }).withMessage((value, {req}) => req.__('product.invalid_list_detail_size')),
    body("refund_conditions")
        .optional()
        .isArray().withMessage((value, {req}) => req.__('product.invalid_refund_conditions')),
    body("custom_images")
        .optional()
        .isArray(),
    body("is_guaranteed_item")
        .optional()
        .isBoolean(),
    body("is_genuine_item")
        .optional()
        .isBoolean(),
    body("is_sold_out")
        .optional()
        .isBoolean(),
    body("authen_images")
        .optional()
        .isArray().withMessage((value, {req}) => req.__('product.missing_authen_images')),
]

export const productUpdateWeightCondition = [
    body("*.weight")
        .exists().withMessage((value, { req }) => req.__('product.missing_product_weight'))
        .isFloat({ min: 10, max: 1_000_000 }).withMessage((value, { req }) => req.__('product.invalid_weight_of_product')),
]

export const updateWeightProductCMSMiddleware = async (req, res, next) => {
    const error = validationResult(req).formatWith(({ msg }) => msg);

    const hasError = !error.isEmpty();

    if (hasError) {
        res.error('VALIDATION_ERROR', error.array()[0], 422)
    } else {
        next();
    }
}

export const checkProductBeforeUpdateWeight = async (req, res, next) => {
    const productListToUpdate:any = req.body;
    try {

        const promises = productListToUpdate.map(async (element) => {
            let { product_id } = element;
            const product = await productRepo.findById(product_id)
            if (!product) {
                throw new Error(req.__("product.not_exist_product"))
            }

            return product;
        })
    
        Promise.all(promises)
        .then(() => {
            req.productListToUpdate = productListToUpdate
            next()
        })
        .catch((error) => {
            res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
        });  
        
    } catch (error) {
        console.log(error);
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
    }
    
}

export const updatePriceProductMiddleware = async (req, res, next) => {
    try {
        const { product } = req;

        if(product.price_updated_dates && product.price_updated_dates.length !== 0) {
            const currentYear = new Date().getFullYear();
            const lastUpdatePriceDate = new Date(product.price_updated_dates[product.price_updated_dates.length - 1]).getFullYear();
        
            if(currentYear !== lastUpdatePriceDate) {
                req.isRenewUpdatePriceCount = true;
                return next();
            }
        }
        
        const productVariants = await variantRepo.getByProductId(req.params.id);
        
        const { price, variants } = req.body;
        

        if (productVariants.length) {
            const variantPrices = productVariants.reduce((obj, {before_sale_price}) => {
                obj[before_sale_price] ? obj[before_sale_price] += 1 : obj[before_sale_price] = 1;
                return obj;
            }, {});

            if (variants && variants.length) {
                const newVariantPrices = variants.reduce((obj, variant) => {
                    obj[variant.price] ? obj[variant.price] += 1 : obj[variant.price] = 1;
                    return obj;
                }, {});

                // Case list variants not change
                if(isEqual(variantPrices, newVariantPrices)) {
                    return next();
                }
                
                
                // Case add new variant
                // Case delete variant
                const arrayNewVariantPrices = Object.keys(newVariantPrices);
                const listOldVariant = pick(variantPrices, arrayNewVariantPrices);

                if(Object.keys(listOldVariant).length === arrayNewVariantPrices.length) {
                    return next();
                }   
        
            } else {
                // Case remove same variants and update same price
                const arrayVariantPrices = Object.keys(variantPrices);
                if(arrayVariantPrices.length === 1 && +arrayVariantPrices[0] === price) {
                    return next();
                }
            }
        } else {
            // Case update price only
            if (!variants || !variants.length)  {
                if(product.before_sale_price === price) {
                    return next();
                }
            } else {
                // Case split variant but same price
                const variantSamePrice = uniqBy(variants, 'price');
                if(variantSamePrice.length === 1 && product.before_sale_price === +variantSamePrice[0].price) {   
                    return next();
                }
            }
        } 

        const systemSetting  = await systemSettingRepo.getOrCreateSystemSetting();
        if(!systemSetting.is_allow_edit_product_price) {
            return res.error(responseCode.BAD_REQUEST.name, req.__("controller.product.system_not_allow_to_update_price"), responseCode.BAD_REQUEST.code);  
        }

        if(product.update_price_remaining <= 0) {
            return res.error(responseCode.BAD_REQUEST.name, req.__("controller.product.exceed_update_price_times"), responseCode.BAD_REQUEST.code); 
        }

        const now = new Date().getTime();
        if(product.next_date_update_price && now < new Date(product.next_date_update_price).getTime()) {
            return res.error(responseCode.BAD_REQUEST.name, req.__("controller.product.not_allow_to_update_price"), responseCode.BAD_REQUEST.code); 
        }

        req.isChangePrice = true;
        
        return next();

    } catch (error) {
        console.log(error);
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
    }
}

export const checkShippingMethodsMiddleware = async (req, res, next) => {
  try {
      const shopShippingMethods = req.user.shop.shipping_methods
      const isActiveShippingMethods = shopShippingMethods.find(value => value.is_active === true)
      if(!isActiveShippingMethods) {
          res.error(responseCode.NOT_FOUND.name, req.__("shop.must_active_shipping_method"), responseCode.NOT_FOUND.code);
          return;
      }
      next()
  } catch (error) {
      console.log(error);
      res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
  }
}

export const checkShippingMethodsByAdminMiddleWare = async (req, res, next) => {
  try {
      let shop :any = await shopRepo.findOne({_id: ObjectId(req.shop_id)})
      shop = cloneObj(shop)
      const shopShippingMethods = shop.shipping_methods
      const isActiveShippingMethods = shopShippingMethods.find(value => value.is_active === true)
      if(!isActiveShippingMethods) {
          res.error(responseCode.NOT_FOUND.name, req.__("shop.must_active_shipping_method"), responseCode.NOT_FOUND.code);
          return;
      }
      next()
  } catch (error) {
      console.log(error);
      res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
  }
}