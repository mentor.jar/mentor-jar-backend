import responseCode from "../../base/responseCode";
import { StreamSessionRepository } from "../../repositories/StreamSessionRepository";
import { cloneObj } from "../../utils/helper";
const { body, validationResult } = require("express-validator");

const streamSessionRepository = StreamSessionRepository.getInstance();

export const findStreamSessionMiddleware = async (req, res, next) => {
    try {
        let streamSession = await streamSessionRepository.findById(req.params.id);
        if (!streamSession) {
            res.error("NOT_FOUND_ERROR", req.__('stream_session.stream_session_not_found'), 404);
            return;
        }
        next();
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
        return;
    }
}

export const createReportLivestreamMiddleware = async (req, res, next) => {
    try {
        let streamSession = await streamSessionRepository.findById(req.params.id);
        if (!streamSession) {
            res.error(responseCode.NOT_FOUND.name, req.__('stream_session.stream_session_not_found'), responseCode.NOT_FOUND.code);
            return;
        }
        if (!req.body.reasons || !req.body?.reasons.length) {
            res.error(responseCode.MISSING.name, req.__('stream_session.missing_report_reason'), responseCode.MISSING.code);
            return;
        }
        next();
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
        return;
    }
}

export const findStreamSessionCMSMiddleware = async (req, res, next) => {
    try {
        let streamSession = await streamSessionRepository.findById(req.params.id);
        if (!streamSession) {
            res.error("NOT_FOUND_ERROR", req.__('stream_session.stream_session_not_found'), 404);
            return;
        }
        next();
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
        return;
    }
}

export const createStreamSessionRule = [
    body("name")
        .exists().withMessage((value, { req }) => req.__('stream_session.missing_name_stream'))
        .isLength({ min: 5, max: 200 }).withMessage((value, { req }) => req.__('stream_session.invalid_name_stream'))
        .trim(),
    body("image")
        .exists().withMessage((value, { req }) => req.__('stream_session.missing_image_stream'))
        .trim(),
    body("type")
        .exists().withMessage((value, { req }) => req.__('stream_session.missing_type_stream'))
        .isLength({ min: 5, max: 100 }).withMessage((value, { req }) => req.__('stream_session.invalid_type_stream'))
        .trim(),
    body("type_of_viewer")
        .exists().withMessage((value, { req }) => req.__('stream_session.missing_type_of_viewer'))
        .trim(),
    body("list_id_product")
        .exists().withMessage((value, { req }) => req.__('stream_session.missing_product_stream')),
    // body("time_will_start")
    //     .exists().withMessage("Vui lòng chọn thời gian sẽ bắt đầu")
]

export const createStreamSessionValidate = (req, res, next) => {
    const error = validationResult(req).formatWith(({ msg }) => msg);
    const hasError = !error.isEmpty();
    if (hasError) {
        res.error('VALIDATION_ERROR', error.array()[0], 422)
    } else next();
}

export const validateTime = async (req, res, next) => {
    try {
        // if (req.body.time_will_start * 1000 < Date.now())
        //     throw new Error("Thời gian bắt đầu phải ở tương lai")
        next();
    } catch (error) {
        res.error('VALIDATION_ERROR', error.message, 422)
    }
}