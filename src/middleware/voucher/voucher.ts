import responseCode from "../../base/responseCode";
import { VoucherRepository } from "../../repositories/VoucherRepository";
import { REQ_DATE_TIME_FORMAT } from "../../base/variable"
import { ObjectId } from "../../utils/helper";
const voucherRepo = VoucherRepository.getInstance();
const moment = require('moment');

const { body, validationResult } = require("express-validator");

const validTarget = ['system', 'product', 'shop', 'user', 'label', 'introduce', 'introduced', 'kol', 'newbie']

export const voucherCondition = [
    body("name")
        .exists().withMessage((value, { req }) => req.__('voucher.missing_name_voucher'))
        .isLength({ min: 3, max: 40 }).withMessage((value, { req }) => req.__('voucher.invalid_name_length_voucher'))
        .trim(),
    body("code")
        .exists().withMessage((value, { req }) => req.__('voucher.missing_code_voucher'))
        .isLength({ min: 3, max: 9 }).withMessage((value, { req }) => req.__('voucher.invalid_code_length_vocuher'))
        .trim(),
    body("target")
        .exists().withMessage((value, { req }) => req.__('voucher.missing_target_voucher')),
    body("start_time")
        .exists().withMessage((value, { req }) => req.__('voucher.missing_start_time_voucher')),
    body("end_time")
        .exists().withMessage((value, { req }) => req.__('voucher.missing_end_time_voucher')),
    body("save_quantity")
        .exists().withMessage((value, { req }) => req.__('voucher.missing_quantity_save_voucher'))
        .isNumeric().withMessage((value, { req }) => req.__('voucher.invalid_quantity_type_voucher')),
    body("use_quantity")
        .exists().withMessage((value, { req }) => req.__('voucher.missing_quantity_use_voucher'))
        .isNumeric().withMessage((value, { req }) => req.__('voucher.invalid_quantity_use_voucher')),
    body("type")
        .exists().withMessage((value, { req }) => req.__('voucher.missing_type_voucher')),
    body("value")
        .exists().withMessage((value, { req }) => req.__('voucher.missing_value_voucher'))
        .isNumeric().withMessage((value, { req }) => req.__('voucher.invalid_value_voucher')),
    body("display_mode")
        .exists().withMessage((value, { req }) => req.__('voucher.missing_display_mode_voucher')),
    body("min_order_value")
        .exists().withMessage((value, { req }) => req.__('voucher.missing_min_order_value'))
        .isNumeric().withMessage((value, { req }) => req.__('voucher.invalid_min_order_value')),
    body("max_order_value").optional(),
    body("max_budget")
        .optional()
        .isNumeric().withMessage((value, { req }) => req.__('voucher.invalid_max_budget')),
    body("product_ids")
        .optional()
        .isArray().withMessage((value, { req }) => req.__('voucher.product_list_invalid'))
]

export const createVoucherMiddleware = async (req, res, next) => {
    const error = validationResult(req).formatWith(({ msg }) => msg);

    const hasError = !error.isEmpty();

    if (hasError) {
        res.error('VALIDATION_ERROR', error.array()[0], 422)
        return;
    } else {
        // Continue validate some field

        // type and value
        if (req.body.type === 'percent') {
            if (+req.body.value <= 0 || +req.body.value > 100) {
                res.error(responseCode.BAD_REQUEST.name, req.__('voucher.invalid_type_percent'), responseCode.BAD_REQUEST.code);
                return;
            }
        }
        else if (req.body.type === 'price') {
            if (+req.body.value <= 0) {
                res.error(responseCode.BAD_REQUEST.name, req.__('voucher.invalid_type_price'), responseCode.BAD_REQUEST.code);
                return;
            }

            if (req.body.max_budget && +req.body.value > +req.body.max_budget) {
                res.error(responseCode.BAD_REQUEST.name, req.__('voucher.value_greater_than_max_budget'), responseCode.BAD_REQUEST.code);
                return;
            }
            if (+req.body.value > 120000000) {
                res.error(responseCode.BAD_REQUEST.name, req.__('voucher.value_price_greater_than_120'), responseCode.BAD_REQUEST.code);
                return;
            }
        }
        else {
            res.error(responseCode.BAD_REQUEST.name, req.__('voucher.invalid_type_voucher'), responseCode.BAD_REQUEST.code);
            return;
        }

        // target
        if (req.body.target !== 'shop' && req.body.target !== 'product') {
            res.error(responseCode.BAD_REQUEST.name, req.__('voucher.invalid_target_voucher'), responseCode.BAD_REQUEST.code);
            return;
        }
        else if (req.body.target === 'product' && (!req.body.product_ids || !req.body.product_ids.length)) {
            res.error(responseCode.BAD_REQUEST.name, req.__('voucher.missing_product_ids_when_target_product'), responseCode.BAD_REQUEST.code);
            return;
        }

        // start_time and end_time
        const startTime = moment( new Date(req.body.start_time), REQ_DATE_TIME_FORMAT, true)
        const endTime = moment(new Date(req.body.end_time), REQ_DATE_TIME_FORMAT, true)
        if (!startTime.isValid() || !endTime.isValid()) {
            res.error(responseCode.BAD_REQUEST.name, req.__('voucher.invalid_start_time_and_end_time'), responseCode.BAD_REQUEST.code);
            return;
        }
        else {
            if (!endTime.isAfter(startTime)) {
                res.error(responseCode.BAD_REQUEST.name, req.__('voucher.end_time_must_greater_than_start_time'), responseCode.BAD_REQUEST.code);
                return;
            }
        }

        // save_quantity and use_quantity
        if (+req.body.save_quantity < +req.body.use_quantity) {
            res.error(responseCode.BAD_REQUEST.name, req.__('voucher.quantity_user_must_less_than_quantity_save'), responseCode.BAD_REQUEST.code);
            return;
        }

        // display_mode
        if (req.body.display_mode !== 'all' && req.body.display_mode !== 'none') {
            res.error(responseCode.BAD_REQUEST.name, req.__('voucher.invalid_display_mode'), responseCode.BAD_REQUEST.code);
            return;
        }
        next();
    }
}

export const updateVoucherMiddleware = async (req, res, next) => {
    const error = validationResult(req).formatWith(({ msg }) => msg);

    const hasError = !error.isEmpty();

    if (hasError) {
        res.error('VALIDATION_ERROR', error.array()[0], 422)
        return;
    } else {
        // Continue validate some field

        // type and value
        if (req.body.type === 'percent') {
            if (+req.body.value <= 0 || +req.body.value > 100) {
                res.error(responseCode.BAD_REQUEST.name, req.__('voucher.invalid_type_percent'), responseCode.BAD_REQUEST.code);
                return;
            }
        }
        else if (req.body.type === 'price') {
            if (+req.body.value <= 0) {
                res.error(responseCode.BAD_REQUEST.name, req.__('voucher.invalid_type_price'), responseCode.BAD_REQUEST.code);
                return;
            }

            if (req.body.max_budget && +req.body.value > +req.body.max_budget) {
                res.error(responseCode.BAD_REQUEST.name, req.__('voucher.value_greater_than_max_budget'), responseCode.BAD_REQUEST.code);
                return;
            }
            if (+req.body.value > 120000000) {
                res.error(responseCode.BAD_REQUEST.name, req.__('voucher.value_price_greater_than_120'), responseCode.BAD_REQUEST.code);
                return;
            }
        }
        else {
            res.error(responseCode.BAD_REQUEST.name, req.__('voucher.invalid_type_voucher'), responseCode.BAD_REQUEST.code);
            return;
        }

        // target
        if (req.body.target !== 'shop' && req.body.target !== 'product') {
            res.error(responseCode.BAD_REQUEST.name, req.__('voucher.invalid_target_voucher'), responseCode.BAD_REQUEST.code);
            return;
        }
        else if (req.body.target === 'product' && (!req.body.product_ids || !req.body.product_ids.length)) {
            res.error(responseCode.BAD_REQUEST.name, req.__('voucher.missing_product_ids_when_target_product'), responseCode.BAD_REQUEST.code);
            return;
        }

        // start_time and end_time
        const startTime = moment(new Date(req.body.start_time), REQ_DATE_TIME_FORMAT, true)
        const endTime = moment(new Date(req.body.end_time), REQ_DATE_TIME_FORMAT, true)
        if (!startTime.isValid() || !endTime.isValid()) {
            res.error(responseCode.BAD_REQUEST.name, req.__('voucher.invalid_start_time_and_end_time'), responseCode.BAD_REQUEST.code);
            return;
        }
        else {
            if (!endTime.isAfter(startTime)) {
                res.error(responseCode.BAD_REQUEST.name, req.__('voucher.end_time_must_greater_than_start_time'), responseCode.BAD_REQUEST.code);
                return;
            }
        }

        // save_quantity and use_quantity
        if (+req.body.save_quantity < +req.body.use_quantity) {
            res.error(responseCode.BAD_REQUEST.name, req.__('voucher.quantity_user_must_less_than_quantity_save'), responseCode.BAD_REQUEST.code);
            return;
        }

        // display_mode
        if (req.body.display_mode !== 'all' && req.body.display_mode !== 'none') {
            res.error(responseCode.BAD_REQUEST.name, req.__('voucher.invalid_display_mode'), responseCode.BAD_REQUEST.code);
            return;
        }
        next();
    }
}


/* ================================ VOUCHER CMS ================================  */


export const voucherCMSCondition = [
    body("name")
        .exists().withMessage((value, { req }) => req.__('voucher.missing_name_voucher'))
        .isLength({ min: 3, max: 40 }).withMessage((value, { req }) => req.__('voucher.invalid_name_length_voucher'))
        .trim(),
    body("code")
        .exists().withMessage((value, { req }) => req.__('voucher.missing_code_voucher'))
        .isLength({ min: 3, max: 9 }).withMessage((value, { req }) => req.__('voucher.invalid_code_length_vocuher'))
        .trim(),
    body("start_time")
        .exists().withMessage((value, { req }) => req.__('voucher.missing_start_time_voucher')),
    body("end_time")
        .exists().withMessage((value, { req }) => req.__('voucher.missing_end_time_voucher')),
    body("save_quantity")
        .exists().withMessage((value, { req }) => req.__('voucher.missing_quantity_save_voucher'))
        .isNumeric().withMessage((value, { req }) => req.__('voucher.invalid_quantity_type_voucher')),
    body("use_quantity")
        .exists().withMessage((value, { req }) => req.__('voucher.missing_quantity_use_voucher'))
        .isNumeric().withMessage((value, { req }) => req.__('voucher.invalid_quantity_use_voucher')),
    body("type")
        .exists().withMessage((value, { req }) => req.__('voucher.missing_type_voucher')),
    body("value")
        .exists().withMessage((value, { req }) => req.__('voucher.missing_value_voucher'))
        .isNumeric().withMessage((value, { req }) => req.__('voucher.invalid_value_voucher')),
    body("discount_by_range_price")
        .optional()
        .isArray({ min: 1 }).withMessage((value, { req }) => req.__('voucher.discount_by_range_must_be_an_array')),
    body("display_mode")
        .exists().withMessage((value, { req }) => req.__('voucher.missing_display_mode_voucher')),
    body("min_order_value")
        .exists().withMessage((value, { req }) => req.__('voucher.missing_min_order_value'))
        .isNumeric().withMessage((value, { req }) => req.__('voucher.invalid_min_order_value')),
    body("max_order_value").optional(),
    body("max_budget")
        .optional()
        .isNumeric().withMessage((value, { req }) => req.__('voucher.invalid_max_budget')),
    body("classify")
        .exists().withMessage((value, { req }) => req.__('voucher.missing_classify_voucher')),
    body("product_ids")
        .optional()
        .isArray().withMessage((value, { req }) => req.__('voucher.product_list_invalid')),
    body("is_public")
        .exists().withMessage((value, { req }) => req.__('voucher.missing_is_public'))
        .isBoolean().withMessage((value, { req }) => req.__('voucher.invalid_is_public'))
]

export const validateVoucherCode = async (req, res, next) => {
    try {
        const { code, start_time, end_time } = req.body
        const { id } = req.params
    
        const startTime = moment(start_time, REQ_DATE_TIME_FORMAT, true)
        const endTime = moment(end_time, REQ_DATE_TIME_FORMAT, true)

        let conditions:any = [
            {
                $and: [
                    { code: code },
                    { start_time: {$lte: startTime} },
                    { end_time: {$gte: startTime} },
                    { shop_id: req?.shop_id || null }
                ]
            },
            {
                $and: [
                    { code: code },
                    { start_time: {$lte: endTime} },
                    { end_time: {$gte: endTime} },
                    { shop_id: req?.shop_id || null }
                ]
            },
            {
                $and: [
                    { code: code },
                    { start_time: {$gte: startTime} },
                    { end_time: {$lte: endTime} },
                    { shop_id: req?.shop_id || null }
                ]
            }
        ]

        if(id) {
            // when updating a voucher
            conditions = conditions.map(item => {
                item["$and"].push({
                    _id: { $ne: ObjectId(id)}
                })
                return item
            })
        }

        const voucher = await voucherRepo.findOne({
            $or: conditions
        })

        if(voucher) {
            res.error(responseCode.UNIQUE_DATA.name, req.__("controller.voucher.code_already_exist"), responseCode.UNIQUE_DATA.code)
            return;
        }
        next()
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
        return;
    }

}

export const createVoucherCMSMiddleware = async (req, res, next) => {
    const error = validationResult(req).formatWith(({ msg }) => msg);

    const hasError = !error.isEmpty();

    if (hasError) {
        res.error('VALIDATION_ERROR', error.array()[0], 422)
        return;
    } else {
        // Continue validate some field

        // Continue validate some field
        const { type, value, discount_by_range_price, target, product_ids, shop_ids, start_time, end_time, save_quantity, use_quantity, 
            display_mode, classify, conditions, min_order_value, max_budget, kol_user, max_order_value } = req.body
        // type and value
        if (value) {
            if (type === 'percent') {
                if (+value <= 0 || +value > 100) {
                    res.error(responseCode.BAD_REQUEST.name, req.__('voucher.invalid_type_percent'), responseCode.BAD_REQUEST.code);
                    return;
                }
            }
            else if (type === 'price') {
                if (+value <= 0) {
                    res.error(responseCode.BAD_REQUEST.name, req.__('voucher.invalid_type_price'), responseCode.BAD_REQUEST.code);
                    return;
                }

                if (req.body.max_budget && +req.body.value > +req.body.max_budget) {
                    res.error(responseCode.BAD_REQUEST.name, req.__('voucher.value_greater_than_max_budget'), responseCode.BAD_REQUEST.code);
                    return;
                }
            }
            else {
                res.error(responseCode.BAD_REQUEST.name, req.__('voucher.invalid_type_voucher'), responseCode.BAD_REQUEST.code);
                return;
            }
        }
        else if (discount_by_range_price) {
            let min_from = 9999999999999999
            let checkErrDiscountRange = false
            const validKey = ['from', 'to', 'value']
            discount_by_range_price.forEach(item => {
                if (
                    Object.keys(item).filter(i => validKey.includes(i)).length !== validKey.length ||
                    isNaN(parseInt(item["from"])) ||
                    isNaN(parseInt(item["to"])) ||
                    isNaN(parseInt(item["value"])) ||
                    (type === 'percent' && (parseInt(item["value"]) <= 0 || parseInt(item["value"]) > 100)) ||
                    (type === 'price' && parseInt(item["value"]) > +max_budget && parseInt(item["value"]) > 120_000_000)
                ) {
                    checkErrDiscountRange = true
                    return;
                }
                if (parseInt(item["from"]) < min_from) {
                    min_from = parseInt(item["from"])
                }
            })

            if (checkErrDiscountRange) {
                res.error(responseCode.BAD_REQUEST.name, req.__('voucher.invalid_discount_by_range'), responseCode.BAD_REQUEST.code);
                return;
            };
            if (min_from < min_order_value) {
                res.error(responseCode.BAD_REQUEST.name, req.__('voucher.min_order_value_must_in_discount_range'), responseCode.BAD_REQUEST.code);
                return;
            }
        }
        else {
            res.error(responseCode.MISSING.name, req.__('voucher.missing_price_or_discount_type'), responseCode.MISSING.code)
        }

        // target
        if (!validTarget.includes(target)) {
            res.error(responseCode.BAD_REQUEST.name, `${req.__('voucher.invalid_target')} ${(validTarget.join('/'))}`, responseCode.BAD_REQUEST.code);
            return;
        }
        else if (target === 'product' && !product_ids) {
            res.error(responseCode.BAD_REQUEST.name, req.__('voucher.missing_product_ids_when_target_product'), responseCode.BAD_REQUEST.code);
            return;
        }
        else if (target === 'shop' && !shop_ids) {
            res.error(responseCode.BAD_REQUEST.name, req.__('voucher.missing_shop_ids_when_target_shop'), responseCode.BAD_REQUEST.code);
            return;
        }
        else if (target === 'kol' && !kol_user) {
            res.error(responseCode.BAD_REQUEST.name, req.__('voucher.missing_kol_user'), responseCode.BAD_REQUEST.code);
            return;
        }

        // start_time and end_time
        const startTime = moment(start_time, REQ_DATE_TIME_FORMAT, true)
        const endTime = moment(end_time, REQ_DATE_TIME_FORMAT, true)
        if (!startTime.isValid() || !endTime.isValid()) {
            res.error(responseCode.BAD_REQUEST.name, req.__('voucher.invalid_start_time_and_end_time'), responseCode.BAD_REQUEST.code);
            return;
        }
        else {
            if (!endTime.isAfter(startTime)) {
                res.error(responseCode.BAD_REQUEST.name, req.__('voucher.end_time_must_greater_than_start_time'), responseCode.BAD_REQUEST.code);
                return;
            }
        }

        // save_quantity and use_quantity
        if (+save_quantity < +use_quantity) {
            res.error(responseCode.BAD_REQUEST.name, req.__('voucher.quantity_user_must_less_than_quantity_save'), responseCode.BAD_REQUEST.code);
            return;
        }

        // display_mode
        if (display_mode !== 'all' && display_mode !== 'none') {
            res.error(responseCode.BAD_REQUEST.name, req.__('voucher.invalid_display_mode'), responseCode.BAD_REQUEST.code);
            return;
        }

        // classify
        if (classify !== 'free_shipping' && classify !== 'discount' && classify !== 'cash_back') {
            res.error(responseCode.BAD_REQUEST.name, req.__('voucher.invalid_classify'), responseCode.BAD_REQUEST.code);
            return;
        }
        
        // Validate for cash back voucher
        if(classify === 'cash_back') {
            if(!value) {
                res.error(responseCode.BAD_REQUEST.name, 'Vui lòng chọn khoảng giảm giá cho voucher cash back', responseCode.BAD_REQUEST.code);
                return;
            }
        }

        // Max order value
        if(max_order_value && +max_order_value < +min_order_value) {
            res.error(responseCode.BAD_REQUEST.name, 'Giá trị đơn hàng tối đa cần lớn hơn hoặc bằng giá trị đơn hàng tối thiểu', responseCode.BAD_REQUEST.code);
            return;
        }
        
        // Validate for cash back voucher
        if(classify === 'cash_back') {
            if(!value) {
                res.error(responseCode.BAD_REQUEST.name, 'Vui lòng chọn giá trị giảm giá cho voucher cash back', responseCode.BAD_REQUEST.code);
                return;
            }
        }

        // conditions
        if (conditions) {
            const validConditionKey = ['shipping_method', 'payment_method', 'limit_per_user', 'limit_per_user_by_day']
            if (
                Object.keys(conditions).filter(i => validConditionKey.includes(i)).length !== validConditionKey.length ||
                isNaN(+conditions["limit_per_user"]) ||
                +conditions["limit_per_user"] < 0 ||
                isNaN(+conditions["limit_per_user_by_day"]) ||
                +conditions["limit_per_user_by_day"] < 0 ||
                !Array.isArray(conditions["shipping_method"]) ||
                !Array.isArray(conditions["payment_method"])
            ) {
                res.error(responseCode.BAD_REQUEST.name, req.__('voucher.invalid_condition'), responseCode.BAD_REQUEST.code);
                return;
            }
        }
        next();
    }
}

export const updateVoucherCMSMiddleware = async (req, res, next) => {
    const error = validationResult(req).formatWith(({ msg }) => msg);

    const hasError = !error.isEmpty();

    if (hasError) {
        res.error('VALIDATION_ERROR', error.array()[0], 422)
        return;
    } else {
        // Continue validate some field
        const { type, value, discount_by_range_price, target, product_ids, shop_ids, start_time, end_time, save_quantity, use_quantity, 
            display_mode, classify, conditions, min_order_value, max_budget, kol_user, max_order_value } = req.body
        // type and value
        if (value) {
            if (type === 'percent') {
                if (+value <= 0 || +value > 100) {
                    res.error(responseCode.BAD_REQUEST.name, req.__('voucher.invalid_type_percent'), responseCode.BAD_REQUEST.code);
                    return;
                }
            }
            else if (type === 'price') {
                if (+value <= 0) {
                    res.error(responseCode.BAD_REQUEST.name, req.__('voucher.invalid_type_price'), responseCode.BAD_REQUEST.code);
                    return;
                }

                if (max_budget && +value > +max_budget) {
                    res.error(responseCode.BAD_REQUEST.name, req.__('voucher.value_greater_than_max_budget'), responseCode.BAD_REQUEST.code);
                    return;
                }
            }
            else {
                res.error(responseCode.BAD_REQUEST.name, req.__('voucher.invalid_type_voucher'), responseCode.BAD_REQUEST.code);
                return;
            }
        }
        else if (discount_by_range_price) {
            let min_from = 9999999999999999
            let checkErrDiscountRange = false
            const validKey = ['from', 'to', 'value']
            discount_by_range_price.forEach(item => {
                if (
                    Object.keys(item).filter(i => validKey.includes(i)).length !== validKey.length ||
                    isNaN(parseInt(item["from"])) ||
                    isNaN(parseInt(item["to"])) ||
                    isNaN(parseInt(item["value"])) ||
                    (type === 'percent' && (parseInt(item["value"]) <= 0 || parseInt(item["value"]) > 100)) ||
                    (type === 'price' && parseInt(item["value"]) > +max_budget && parseInt(item["value"]) > 120_000_000)
                ) {
                    checkErrDiscountRange = true
                    return;
                }
                if (parseInt(item["from"]) < min_from) {
                    min_from = parseInt(item["from"])
                }
            })

            if (checkErrDiscountRange) {
                res.error(responseCode.BAD_REQUEST.name, req.__('voucher.invalid_discount_by_range'), responseCode.BAD_REQUEST.code);
                return;
            };
            if (min_from < min_order_value) {
                res.error(responseCode.BAD_REQUEST.name, req.__('voucher.min_order_value_must_in_discount_range'), responseCode.BAD_REQUEST.code);
                return;
            }
        }
        else {
            res.error(responseCode.MISSING.name, req.__('voucher.missing_price_or_discount_type'), responseCode.MISSING.code)
        }

        // target
        if (!validTarget.includes(target)) {
            res.error(responseCode.BAD_REQUEST.name, `${req.__('voucher.invalid_target')} ${(validTarget.join('/'))}`, responseCode.BAD_REQUEST.code);
            return;
        }
        else if (target === 'product' && !product_ids) {
            res.error(responseCode.BAD_REQUEST.name, req.__('voucher.missing_product_ids_when_target_product'), responseCode.BAD_REQUEST.code);
            return;
        }
        else if (target === 'shop' && !shop_ids) {
            res.error(responseCode.BAD_REQUEST.name, req.__('voucher.missing_shop_ids_when_target_shop'), responseCode.BAD_REQUEST.code);
            return;
        }
        else if (target === 'kol' && !kol_user) {
            res.error(responseCode.BAD_REQUEST.name, req.__('voucher.missing_kol_user'), responseCode.BAD_REQUEST.code);
            return;
        }

        // start_time and end_time
        const startTime = moment(start_time, REQ_DATE_TIME_FORMAT, true)
        const endTime = moment(end_time, REQ_DATE_TIME_FORMAT, true)
        if (!startTime.isValid() || !endTime.isValid()) {
            res.error(responseCode.BAD_REQUEST.name, req.__('voucher.invalid_start_time_and_end_time'), responseCode.BAD_REQUEST.code);
            return;
        }
        else {
            if (!endTime.isAfter(startTime)) {
                res.error(responseCode.BAD_REQUEST.name, req.__('voucher.end_time_must_greater_than_start_time'), responseCode.BAD_REQUEST.code);
                return;
            }
        }

        // save_quantity and use_quantity
        if (+save_quantity < +use_quantity) {
            res.error(responseCode.BAD_REQUEST.name, req.__('voucher.quantity_user_must_less_than_quantity_save'), responseCode.BAD_REQUEST.code);
            return;
        }

        // display_mode
        if (display_mode !== 'all' && display_mode !== 'none') {
            res.error(responseCode.BAD_REQUEST.name, req.__('voucher.invalid_display_mode'), responseCode.BAD_REQUEST.code);
            return;
        }

        // classify
        if (classify !== 'free_shipping' && classify !== 'discount' && classify !== 'cash_back') {
            res.error(responseCode.BAD_REQUEST.name, req.__('voucher.invalid_classify'), responseCode.BAD_REQUEST.code);
            return;
        }

        // Max order value
        if(max_order_value && +max_order_value < +min_order_value) {
            res.error(responseCode.BAD_REQUEST.name, 'Giá trị đơn hàng tối đa cần lớn hơn hoặc bằng giá trị đơn hàng tối thiểu', responseCode.BAD_REQUEST.code);
            return;
        }

        // Validate for cash back voucher
        if(classify === 'cash_back') {
            if(!value) {
                res.error(responseCode.BAD_REQUEST.name, 'Vui lòng chọn khoảng giảm giá cho voucher cash back', responseCode.BAD_REQUEST.code);
                return;
            }
        }

        // conditions
        if (conditions) {
            const validConditionKey = ['shipping_method', 'payment_method', 'limit_per_user']
            if (
                Object.keys(conditions).filter(i => validConditionKey.includes(i)).length !== validConditionKey.length ||
                isNaN(+conditions["limit_per_user"]) ||
                +conditions["limit_per_user"] < 0 ||
                isNaN(+conditions["limit_per_user_by_day"]) ||
                +conditions["limit_per_user_by_day"] < 0 ||
                !Array.isArray(conditions["shipping_method"]) ||
                !Array.isArray(conditions["payment_method"])
            ) {
                res.error(responseCode.BAD_REQUEST.name, req.__('voucher.invalid_condition'), responseCode.BAD_REQUEST.code);
                return;
            }
        }
        next();
    }
}

export const findVoucherMiddleware = async (req, res, next) => {
    try {
        const voucher = await voucherRepo.findById(req.params.id);
        if (!voucher) {
            res.error("NOT_FOUND_ERROR", req.__('voucher.voucher_not_found'), 404);
            return;
        }
        next();
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
        return;
    }
}

export const requireShopOwner = async (req, res, next) => {
    if (req.body.shop_id) {
        const { start_time, end_time, code, shop_id } = req.body
        const { id } = req.params
        const startTime = moment(start_time, REQ_DATE_TIME_FORMAT, true)
        const endTime = moment(end_time, REQ_DATE_TIME_FORMAT, true)
        
        let conditions:any = [
            {
                $and: [
                    { code },
                    { start_time: {$lte: startTime} },
                    { end_time: {$gte: startTime} },
                    { shop_id: shop_id }
                ]
            },
            {
                $and: [
                    { code },
                    { start_time: {$lte: endTime} },
                    { end_time: {$gte: endTime} },
                    { shop_id: shop_id }
                ]
            },
            {
                $and: [
                    { code },
                    { start_time: {$gte: startTime} },
                    { end_time: {$lte: endTime} },
                    { shop_id: shop_id }
                ]
            }
        ]

        if(id) {
            // when updating a voucher
            conditions = conditions.map(item => {
                item["$and"].push({
                    _id: { $ne: ObjectId(id)}
                })
                return item
            })
        }

        const voucher = await voucherRepo.findOne({
            $or: conditions
        })
        
        if(voucher) {
            res.error(responseCode.UNIQUE_DATA.name, req.__("controller.voucher.code_already_exist"), responseCode.UNIQUE_DATA.code)
            return;
        }
        next()
    }
    else {
        res.error("VALIDATION_ERROR", req.__('voucher.missing_shop'), 422);
        return;
    }

}