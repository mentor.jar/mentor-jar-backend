
import * as variables from '../base/variable'
import path from 'path'

const multer = require("multer");


const filter = function (req, file, callback) {
    var ext = path.extname(file.originalname.toLowerCase())
    if (ext !== '.mp4' && ext != '.mov') {
        callback(new Error(`${req.__("media.file_type")} mp4|mov`), false)
    }
    callback(null, true)
}

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads/videos/');
    },
    filename: (req, file, cb) => {
        const ext = path.extname(file.originalname.toLowerCase())
        cb(null, `${Date.now()}_video_${Math.floor(Math.random() * 1_000_000)}${ext}`);
    },
});
const uploadVideoFile = multer({
    storage: storage,
    fileFilter: filter
})

export default uploadVideoFile