import moment from "moment";
const { body, validationResult } = require("express-validator");
import responseCode from "../../base/responseCode";
import { KOREA } from "../../base/variable";
import { CartItemRepo } from "../../repositories/CartItemRepo";
import { PaymentMethodRepo } from "../../repositories/PaymentMethodRepo";
import { ProductRepo } from "../../repositories/ProductRepo";
import { ShopRepo } from "../../repositories/ShopRepo";
import CacheService from "../../services/cache";

const cartItemRepo = CartItemRepo.getInstance()
const shopRepo = ShopRepo.getInstance()
const paymentMethodRepo = PaymentMethodRepo.getInstance()
const productRepo = ProductRepo.getInstance()
const cacheService = CacheService._

export const validateQuantity = async (req, res, next) => {
    if (req.body.quantity && !isNaN(+req.body.quantity) && +req.body.quantity > 0) {
        req.body.quantity = +req.body.quantity
        next()
    }
    else {
        res.error(responseCode.BAD_REQUEST.name, req.__("cart.invalid_quantity"), responseCode.BAD_REQUEST.code);
        return;
    }
}

export const checkAndFormatBeforeGet = async (req, res, next) => {
    const { total_value, shipping_method, payment_method, shops, products, code,  order_room_id} = req.body
    const user = req.user
    const conditionObj = { total_value, shipping_method, payment_method, shops, user, products, order_room_id }
    req.conditionObj = conditionObj
    req.code = code;
    next()
}

export const checkOwnerCartItem = async (req, res, next) => {
    const userID = req.user._id
    const cartItemID = req.params.id
    const cartItem = await cartItemRepo.findByIDAndUserID(cartItemID, userID)
    req.cartItem = cartItem
    if (!cartItem) {
        res.error(responseCode.NOT_FOUND.name, req.__("cart.product_onwer"), responseCode.NOT_FOUND.code);
        return;
    }
    next()
}

export const blockOrderByTime = async (req, res, next) => {
    const time = moment()
    const fromDate = moment().set({ year: 2022, month: 2, date: 13, hour: 0, minute: 0, second: 0 })
    const toDate = moment().set({ year: 2022, month: 2, date: 13, hour: 6, minute: 0, second: 0 })

    if(fromDate.isSameOrBefore(time) && toDate.isSameOrAfter(time)) {
        res.error(responseCode.BAD_REQUEST.name, 'Tính năng đặt hàng đang tạm dừng. Vui lòng quay lại sau 6:00 13/03/2022', responseCode.BAD_REQUEST.code)
        return;
    }
    next()
}

export const groupBuyCheckoutMiddleware = async (req, res, next) => {
    if (!req.body.payment_method) {
        let defaultPaymentMethod = null
        if(!cacheService.has('default_payment_method')) {
            defaultPaymentMethod = await paymentMethodRepo.getDefaultPaymentMethod();
            cacheService.set('default_payment_method', defaultPaymentMethod)
        }
        else {
            console.log("default payment method from cache");
            defaultPaymentMethod = cacheService.get('default_payment_method')
        }
        req.body.payment_method = defaultPaymentMethod._id
    }
    if(req.body.system_voucher?.cash_back_discount_voucher_id && req.body.system_voucher?.cash_back_voucher_id) {
        res.error(responseCode.BAD_REQUEST.name, req.__("checkout.invalid_cash_back_voucher"), responseCode.BAD_REQUEST.code)
        return;   
    }
    
    if(req.body.order_room_id && req.body.system_voucher) {
        req.body.system_voucher = {
            ...req.body.system_voucher,
            discount_voucher_id: null,
            cash_back_discount_voucher_id: null,
            cash_back_voucher_id: null
        }
    }

    // Check sold out
    const arrProductIds = [];
    req.body.shops.forEach(shop => {
        shop.items.forEach(id => {
            arrProductIds.push(id);
        });
    });
    let checkItem: any = {
        success: true,
        message: ''
    }
    const listPromise: any = arrProductIds.map(id => {
            return new Promise(async (resolve) => {
                const item = await cartItemRepo.findById(id);
                if(item) {
                    if(req.body.order_room_id) {
                        // group buy order must has exactly 1 quantity for the item
                        if(item.quantity > 1) {
                            let message = req.__('order.invalid_quantity_for_group_buy_order')
                            checkItem = {
                                success: false,
                                message
                            }
                        }
                    }
                    const product = await productRepo.findById(item?.product_id);
                    if (product.is_sold_out) {
                        let message = req.__('order.invalid_product_to_order', product.name)
                        checkItem = {
                            success: false,
                            message
                        }
                    }
                    if (product.group_buy && req.body.order_room_id) {
                        const now = new Date().getTime();
                        const groupBuyStartTime = new Date(product?.group_buy?.start_time).getTime();
                        if (groupBuyStartTime > now) {
                            let message = req.__('order.invalid_group_buy_time_to_order', product.name)
                            checkItem = {
                                success: false,
                                message
                            }
                        }

                        const group = product?.group_buy?.groups.find(g => g.id == req.body.group_id);
                        if(!group) {
                            let message = req.__('order.invalid_group_to_order', product.name)
                            checkItem = {
                                success: false,
                                message
                            }
                        }

                        const room = product?.order_rooms?.find(r => r.id === req.body.order_room_id) 
                        if(room) {
                            if(room.members.length >= group?.number_of_member) {
                                let message = req.__('order.maximum_member_to_order', product.name)
                                checkItem = {
                                    success: false,
                                    message
                                }
                            }
                        }
                    }
                    resolve(true);
                }
                resolve(true);
            })
    })
    
    await Promise.all(listPromise);


    if (!checkItem.success) {
        res.error(responseCode.BAD_REQUEST.name, checkItem.message, responseCode.BAD_REQUEST.code)
        return;
    }
    
    next();
}

export const checkoutMiddleware = async (req, res, next) => {
    if (!req.body.payment_method) {
        let defaultPaymentMethod = null
        if(!cacheService.has('default_payment_method')) {
            defaultPaymentMethod = await paymentMethodRepo.getDefaultPaymentMethod();
            cacheService.set('default_payment_method', defaultPaymentMethod)
        } else {
            defaultPaymentMethod = cacheService.get('default_payment_method')
        }
        req.body.payment_method = defaultPaymentMethod._id
    }
    if(req.body.system_voucher?.cash_back_discount_voucher_id && req.body.system_voucher?.cash_back_voucher_id) {
        res.error(responseCode.BAD_REQUEST.name, req.__("checkout.invalid_cash_back_voucher"), responseCode.BAD_REQUEST.code)
        return;   
    }
    // Check sold out
    const arrProductIds = [];
    req.body.shops.forEach(shop => {
        shop.items.forEach(id => {
            arrProductIds.push(id);
        });
    });
    let checkItem: any = {
        success: true,
        message: ''
    }
    const listPromise: any = arrProductIds.map(id => {
            return new Promise(async (resolve) => {
                const item = await cartItemRepo.findById(id);
                if(item) {
                    const product = await productRepo.findById(item?.product_id);
                    if (product.is_sold_out) {
                        let message = req.__('order.invalid_product_to_order', product.name)
                        checkItem = {
                            success: false,
                            message
                        }
                    }
                    resolve(true);
                }
                resolve(true);
            })
    })
    
    await Promise.all(listPromise);

    if (!checkItem.success) {
        res.error(responseCode.BAD_REQUEST.name, checkItem.message, responseCode.BAD_REQUEST.code);
        return;
    }
    
    next();
}

export const checkCountryShop = async (req, res, next) => {
    const userId = req.user._id
    const shop: any = await shopRepo.findOne({ user_id: userId });
    if (shop && shop.country === KOREA) {
        res.error(responseCode.SERVER.name, req.__("cart.invalid_vn_shop"), responseCode.SERVER.code);
        return;
    }
    next();
}

export const validatorListShop = [
    body("list_shops")
        .isArray().withMessage((value, {req}) => req.__('cart.invalid_list_shop')),
    body("list_shops.*.shop_id")
        .exists().withMessage((value, { req }) => req.__('cart.invalid_list_shop'))
        .trim()
]

export const validationRound = async (req, res, next) => {
    const error = validationResult(req).formatWith(({ msg }) => msg);

    const hasError = !error.isEmpty();

    if (hasError) {
        res.error('VALIDATION_ERROR', error.array()[0], 422)
        return;
    } else {
        next()
    }
}