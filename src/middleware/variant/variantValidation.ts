const { body, validationResult } = require("express-validator");

export const optionTypeCondition = [
    body("name")
        .exists().withMessage((value, {req}) => req.__("option_type.missing_name"))
        .isLength({ min: 1, max: 50 }).withMessage((value, {req}) => req.__("option_type.name_length"))
        .trim(),
    body("product_id")
        .exists().withMessage((value, {req}) => req.__("option_type.missing_product")),
]

export const optionTypeMiddleware = (req, res, next) => {
    const error = validationResult(req).formatWith(({ msg }) => msg);
    const hasError = !error.isEmpty();
    if (hasError) {
        res.error('VALIDATION_ERROR', error.array()[0], 422)
    } else {
        next();
    }
}
