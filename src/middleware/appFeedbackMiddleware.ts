import responseCode from "../base/responseCode";

const { body, validationResult } = require("express-validator");

export const appFeedbackConditions = [
    body('full_name')
        .exists().withMessage((value, { req }) => req.__('app_feedback.missing_full_name')),
    body('email')
        .exists().withMessage((value, { req }) => req.__('app_feedback.missing_email')),
    body('how_you_know_bidu')
        .exists().withMessage((value, { req }) => req.__('app_feedback.missing_how_you_know'))
        .isArray({min: 1}).withMessage((value, { req }) => req.__('app_feedback.missing_how_you_know')),
    body('satisfied_with_bidu_mark')
        .exists().withMessage((value, { req }) => req.__('app_feedback.missing_satisfied_with_bidu_mark'))
        .isFloat({min: 0, max: 5}).isIn([0,1,2,3,4,5]).withMessage((value, { req }) => req.__('app_feedback.invalid_mark')),
    body('ui_mark')
        .exists().withMessage((value, { req }) => req.__('app_feedback.missing_ui_mark'))
        .isFloat({min: 0, max: 5}).isIn([0,1,2,3,4,5]).withMessage((value, { req }) => req.__('app_feedback.invalid_mark')),
    body('shop_config_mark')
        .exists().withMessage((value, { req }) => req.__('app_feedback.missing_shop_config_mark'))
        .isFloat({min: 0, max: 5}).isIn([0,1,2,3,4,5]).withMessage((value, { req }) => req.__('app_feedback.invalid_mark')),
    body('product_manage_mark')
        .exists().withMessage((value, { req }) => req.__('app_feedback.missing_product_manage_mark'))
        .isFloat({min: 0, max: 5}).isIn([0,1,2,3,4,5]).withMessage((value, { req }) => req.__('app_feedback.invalid_mark')),
    body('livestream_mark')
        .exists().withMessage((value, { req }) => req.__('app_feedback.missing_livestream_mark'))
        .isFloat({min: 0, max: 5}).isIn([0,1,2,3,4,5]).withMessage((value, { req }) => req.__('app_feedback.invalid_mark')),
    body('order_manage_mark')
        .exists().withMessage((value, { req }) => req.__('app_feedback.missing_order_manage_mark'))
        .isFloat({min: 0, max: 5}).isIn([0,1,2,3,4,5]).withMessage((value, { req }) => req.__('app_feedback.invalid_mark')),
    body('opinion')
        .exists().withMessage((value, { req }) => req.__('app_feedback.missing_opinion'))
        .trim().isLength({min: 1}).withMessage((value, { req }) => req.__('app_feedback.missing_opinion'))
]

export const filterAppFeedbackConditions = [
    body("type")
        .exists().withMessage("Missing type")
        .isIn(["and", "or"]).withMessage("Invalid value for type field"),
    body("filter")
        .exists().withMessage("Missing filter")
        .isArray().withMessage('Filter must be an array'),
    body("filter.*.name")
        .exists().withMessage("Missing name field")
        .isIn(['satisfied_with_bidu_mark', 'ui_mark', 'shop_config_mark', 'product_manage_mark', 'livestream_mark', 'order_manage_mark'])
        .withMessage("Invalid value for name field"),
    body("filter.*.value")
        .exists().withMessage("Missing value field")
        .isIn([0, 1, 2, 3, 4, 5]).withMessage("Invalid value for value field")
]

export const appFeedbackMiddleware = async (req, res, next) => {
    const error = validationResult(req).formatWith(({ msg }) => msg);

    const hasError = !error.isEmpty();

    if (hasError) {
        res.error('VALIDATION_ERROR', error.array()[0], 422)
        return;
    } else {
        next()
    }
}