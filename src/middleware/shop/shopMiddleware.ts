import { param } from "express-validator";
import { executeError } from "../../base/appError";
import { NotFoundError } from "../../base/customError";
import responseCode from "../../base/responseCode";
import User from "../../models/User";
import { ShippingMethodRepo } from "../../repositories/ShippingMethodRepo";
import { ShopRepo } from "../../repositories/ShopRepo";
import { cloneObj, ObjectId } from "../../utils/helper";
const { body, validationResult } = require("express-validator");
const shopRepo = ShopRepo.getInstance();
const shippingMethodRepo = ShippingMethodRepo.getInstance();

export const findShopMiddleware = async (req, res, next) => {
    try {
        const shop_id = req.params.id || req.body.shop_id || req.shop_id;
        let shop: any = await shopRepo.findOne({ _id: shop_id });
        if (!shop) {
            res.error(responseCode.NOT_FOUND.name, req.__("shop.shop_not_found"), responseCode.NOT_FOUND.code);
            return;
        }
        next();
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}

export const shopSettingValidate = async (req, res, next) => {

    let error: any = {
        name: responseCode.BAD_REQUEST.name,
        message: '',
        statusCode: responseCode.BAD_REQUEST.code
    }
    let existError = false;
    // shop_type
    let shop_type = +req.body.shop_type;
    if (![0, 1].includes(shop_type)) {
        existError = true
        error.message = req.__("shop.type_option");
    }
    // refund_money_mode
    // let refund_money_mode = req.body.refund_money_mode;
    // if (![true, false].includes(refund_money_mode)) {
    //     existError = true

    //     error.message = req.__("shop.refund_mode");
    // }
    // pause_mode
    let pause_mode = req.body.pause_mode;
    if (![true, false].includes(pause_mode)) {
        existError = true
        error.message = req.__("shop.pause_mode");
    }
    // refund_money_regulations
    // let refund_money_regulations = req.body.refund_money_regulations
    // if (refund_money_mode && typeof refund_money_regulations != 'object') {
    //     existError = true
    //     error.message = req.__("shop.refund_money_regulations");
    // }
    if (existError) {
        res.error(error.name, error.message, error.statusCode);
        return;
    }
    next();
}

export const shopPauseModeValidation = (req, res, next) => {
    let pause_mode = req.body.pause_mode;
    if (![true, false].includes(pause_mode)) {
        res.error(responseCode.MISSING.name, req.__("shop.pause_mode"), responseCode.MISSING.code);
        return;
    }

    next();
}

export const updateCountryRule = [
    body("shop_country")
        .exists().withMessage((value, { req }) => req.__("shop.missing_country"))
]

export const activeShippingMethodValidate = [
    param("id")
        .exists().withMessage((value, { req }) => req.__("shop.missing_shipping_id"))
        .not()
        .isEmpty()
        .exists().withMessage((value, { req }) => req.__("shop.missing_shipping_id")),
    body("is_active")
        .exists().withMessage((value, { req }) => req.__("shop.missing_active_mode"))
        .not()
        .isEmpty()
        .withMessage((value, { req }) => req.__("shop.missing_active_mode"))
        .trim(),
]

export const activeShippingShopByAdminMethodValidate = [
    body("shop_id")
        .exists().withMessage((value, { req }) => req.__("shop.missing_shop_id"))
        .notEmpty().withMessage((value, { req }) => req.__("shop.missing_shop_id")),
    body("shipping_method_id")
        .exists().withMessage((value, { req }) => req.__("shop.missing_shipping_method_id"))
        .notEmpty().withMessage((value, { req }) => req.__("shop.missing_shipping_method_id")),
    body("is_active")
        .exists().withMessage((value, { req }) => req.__("shop.missing_active_mode"))
        .notEmpty().withMessage((value, { req }) => req.__("shop.missing_active_mode"))
]

export const createGHTKAccountValidate = [
    param("shippingId")
        .exists().withMessage((value, { req }) => req.__("shop.missing_shipping_id"))
        .not()
        .isEmpty()
        .exists().withMessage((value, { req }) => req.__("shop.missing_shipping_id")),
    body("address_id")
        .exists().withMessage((value, { req }) => req.__("shop.missing_address"))
        .not()
        .isEmpty()
        .exists().withMessage((value, { req }) => req.__("shop.missing_address")),
]

export const shopMiddleware = (req, res, next) => {
    const error = validationResult(req).formatWith(({ msg }) => msg);

    const hasError = !error.isEmpty();

    if (hasError) {

        res.error('VALIDATION_ERROR', error.array()[0], 422)
    } else {
        next();
    }
}

export const checkShopAccountExistMiddleware = async (req, res, next) => {
    try {
        const shipping_id = req.params.id;
        let shipping = await shippingMethodRepo.getShippingMethodForShop(req.user.shop, shipping_id);
        shipping = cloneObj(shipping)
        
        if (!shipping) throw new Error (req.__("shop.invalid_shipping_method_id"))
        if (shipping.is_active && shipping.is_active === req.body.is_active) throw new Error(req.__("shop.selected_shipping_method"));
        if (!shipping.is_active && shipping.is_active === req.body.is_active) throw new Error(req.__("shop.not_selected_shipping_method"));

        if (!shipping.code && !shipping.token && !shipping.is_active) {
            req.createAccount = true;
            req.name_query = shipping.name_query;
        }
        next();
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}

export const checkShopAccountExistByAdminMiddleware = async (req, res, next) => {
    try {
        const { shop_id, shipping_method_id, is_active } = req.body;

        let shop: any = await shopRepo.findById(shop_id)
        shop = cloneObj(shop)
        if (!shop) throw new Error(req.__("shop.shop_not_found"))

        let user: any = await User.findById(shop.user_id);
        user = cloneObj(user);
        if (!user) throw new NotFoundError(req.__("auth.user_not_found"));

        let shipping = await shippingMethodRepo.getShippingMethodForShop(shop, shipping_method_id);
        shipping = cloneObj(shipping)

        if(!shipping) throw new Error(req.__("shop.invalid_shipping_method_id"))
        if (shipping.is_active && shipping.is_active === is_active) throw new Error(req.__("shop.selected_shipping_method"));
        if (!shipping.is_active && shipping.is_active === is_active) throw new Error(req.__("shop.not_selected_shipping_method"));

        if (!shipping.code && !shipping.token && !shipping.is_active) {
            req.createAccount = true;
            req.name_query = shipping.name_query;
        }
        req.shop_id = shop._id;
        req.user = user

        next();
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}

export const updateCountryMiddleware = async (req, res, next) => {
    try {
        const availableCountries = ['KO', 'VN'];
        if (!availableCountries.includes(req.body.shop_country)) {
            res.error(responseCode.NOT_FOUND.name, req.__("shop.invalid_country"), responseCode.NOT_FOUND.code);
            return;
        }
        next();
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}

export const checkInforBeforeApprove = async (req, res, next) => {
    try {
        const { shop_id, is_approved } = req.body
        if (!shop_id) {
            res.error(responseCode.MISSING.name, req.__("shop.must_select_shop"), responseCode.MISSING.code)
            return;
        }
        if (is_approved === undefined || typeof(is_approved) !== "boolean") {
            res.error(responseCode.MISSING.name, req.__("shop.must_select_approve"), responseCode.MISSING.code)
            return;
        }
        const shop = await shopRepo.findOne({_id: ObjectId(shop_id)})
        if(!shop) {
            res.error(responseCode.NOT_FOUND.name, req.__("shop.shop_not_found"), responseCode.NOT_FOUND.code);
            return;
        }
        req.shop = shop
        next()
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}

export const checkShippingMethodsMiddleware = async (req, res, next) => {
  try {
      const shipping_id = req.params.id;
      let { is_active } = req.body;
      is_active = JSON.parse(is_active)
      const shopShippingMethods = req.user.shop.shipping_methods
      const shopFilter = shopShippingMethods.filter(value => value.shipping_method_id !== shipping_id)
      const isActiveShippingMethods = shopFilter.find(value => value.is_active === true)
      if(is_active === false && !isActiveShippingMethods) {
          res.error(responseCode.NOT_FOUND.name, req.__("shop.must_active_shipping_method"), responseCode.NOT_FOUND.code);
          return;
      }
      next()
  } catch (error) {
      error = executeError(error);
      res.error(error.name, error.message, error.statusCode);
      return;
  }
}

export const checkShippingMethodsByAdminMiddleware = async (req, res, next) => {
  try {
      const { is_active, shipping_method_id, shop_id } = req.body;
      let shop :any = await shopRepo.findOne({_id: ObjectId(shop_id)})
      shop = cloneObj(shop)
      const shopShippingMethods = shop.shipping_methods
      const shopFilter = shopShippingMethods.filter(value => value.shipping_method_id !== shipping_method_id)
      const isActiveShippingMethods = shopFilter.find(value => value.is_active === true)
      if(is_active === false && !isActiveShippingMethods) {
          res.error(responseCode.NOT_FOUND.name, req.__("shop.must_active_shipping_method"), responseCode.NOT_FOUND.code);
          return;
      }
      next()
  } catch (error) {
      error = executeError(error);
      res.error(error.name, error.message, error.statusCode);
      return;
  }
}