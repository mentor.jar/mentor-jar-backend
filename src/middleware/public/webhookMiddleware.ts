import { WebhookError } from "../../base/customError";
import GHTKService from "../../services/3rd/shippings/GHTK";
import ViettelPostService from "../../services/3rd/shippings/viettelpost/ViettelPost";

export const connectWebhookGHTKMiddleware = async (req, res, next) => {
  try {
    const { hash } = req.query;
    const ghtkService = await GHTKService.getInstance();

    if (hash !== ghtkService.getWebhookHashCode()) {
      throw new WebhookError(req.__("webhook.invalid_hash"));
    }
    next();
  } catch (error) {
    res.error('VALIDATION_ERROR', error.message, 422)
  }
}

export const connectWebhookViettelPostMiddleware = async (req, res, next) => {
  try {
    const { hash } = req.query;
    const code = await ViettelPostService._.getWebhookHashCode();

    if (hash !== code) {
      throw new WebhookError(req.__("webhook.invalid_hash"));
    }
    next();
  } catch (error) {
    res.error('VALIDATION_ERROR', error.message, 422)
  }
}