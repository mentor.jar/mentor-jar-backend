const jwt = require('jsonwebtoken');
import { NotFoundError } from '../base/customError';
import { error_response_obj } from '../base/injector';
import { ShopRepo } from '../repositories/ShopRepo';
import responseCode from '../base/responseCode';
import keys from '../config/env/keys';
import User from '../models/User';
import { cloneObj, countryByPhoneNumber, ObjectId } from '../utils/helper';
import { UserRepo } from '../repositories/UserRepo';
import { RoleRepo } from '../repositories/RoleRepo';
import { KOREA, VIET_NAM } from '../base/variable';
import AuthService from '../controllers/serviceHandles/user/auth';

const AUTHORIZATION_FIELD = 'Authorization';
const PREFIX = 'Bidu';
const shopRepository = ShopRepo.getInstance();
const roleRepository = RoleRepo.getInstance();
const userRepo = UserRepo.getInstance();

export const auth = async (req: any, res: any, next: any) => {
    const auth_jwt = req.header(AUTHORIZATION_FIELD);
    if (!auth_jwt)
        return res
            .status(responseCode.UN_AUTHENTICATION.code)
            .send(
                error_response_obj(
                    req.__("auth.not_authorize"),
                    responseCode.UN_AUTHENTICATION.name
                )
            );
    try {
        const token = auth_jwt.replace(PREFIX, '').trim();
        const data = jwt.verify(token, keys.jwtSecret);
        let user = await userRepo.getUserAndCacheById(data.id);
        req.user = user;
        next();
    } catch (error) {
        console.log('auth', error);
        return res
            .status(responseCode.UN_AUTHENTICATION.code)
            .send(
                error_response_obj(
                    req.__("auth.not_authorize"),
                    responseCode.UN_AUTHENTICATION.name
                )
            );
    }
};

export const authShop = async (req: any, res: any, next: any) => {
    const auth_jwt = req.header(AUTHORIZATION_FIELD);
    const country = req.body.shop_country
    const shop_country = country === VIET_NAM || country === KOREA ? country : VIET_NAM;
    if (!auth_jwt)
        return res
            .status(responseCode.UN_AUTHENTICATION.code)
            .send(
                error_response_obj(
                    req.__("auth.not_authorize"),
                    responseCode.UN_AUTHENTICATION.name
                )
            );
    try {
        const token = auth_jwt.replace(PREFIX, '').trim();
        const data = jwt.verify(token, keys.jwtSecret);

        let user: any = await userRepo.getUserAndCacheById(data.id);
        let shop: any = await shopRepository.findOrCreateByUser(user, shop_country);

        user.shop = shop;
        req.user = user;
        req.shop_id = shop._id;

        next();
    } catch (error) {
        console.log('auth', error);
        return res
            .status(responseCode.UN_AUTHENTICATION.code)
            .send(
                error_response_obj(
                    req.__("auth.not_authorize"),
                    responseCode.UN_AUTHENTICATION.name
                )
            );
    }
};

export const authWeb = async (req: any, res: any, next: any) => {
    try {
        const auth_jwt = req.header(AUTHORIZATION_FIELD);
        if (auth_jwt) {
            const token = auth_jwt.replace(PREFIX, '').trim();
            const data = jwt.verify(token, keys.jwtSecret);
            req.user = await userRepo.getUserAndCacheById(data.id);
        }
        next();
    } catch (error) {
        console.log('auth', error);
        return res
            .status(responseCode.UN_AUTHENTICATION.code)
            .send(
                error_response_obj(
                    error.message,
                    responseCode.UN_AUTHENTICATION.name
                )
            );
    }
};

export const authAdmin = async (req: any, res: any, next: any) => {
    const auth_jwt = req.header(AUTHORIZATION_FIELD);
    if (!auth_jwt)
        return res
            .error(responseCode.UN_AUTHENTICATION.name, req.__("auth.not_authorize"), responseCode.UN_AUTHENTICATION.code);
    try {
        const token = auth_jwt.replace(PREFIX, '').trim();
        const data = jwt.verify(token, keys.jwtSecret);
        let user: any = await User.findById(data.id);
        if (!user) throw new NotFoundError(req.__("auth.user_not_found"));

        const role = await roleRepository.findById(user.roleId);
        const arrAdmin = ['SUPER_ADMIN', 'ADMIN']
        if (!role || !arrAdmin.includes(role.roleName)) {
            return res
                .error(responseCode.UN_AUTHENTICATION.name, req.__("auth.not_authorize"), responseCode.UN_AUTHENTICATION.code);
        }

        req.user = user;
        req.userParseDTO = await UserRepo.getInstance().getUserById(ObjectId(user._id));
        req.token = token;
        next();
    } catch (error) {
        console.log('auth', error);
        return res
            .status(responseCode.UN_AUTHENTICATION.code)
            .send(
                error_response_obj(
                    req.__("auth.not_authorize"),
                    responseCode.UN_AUTHENTICATION.name
                )
            );
    }
};

export const authAdminWithTypeRole = async (req: any, res: any, next: any) => {
    const auth_jwt = req.header(AUTHORIZATION_FIELD);
    if (!auth_jwt) {
        return res.error(
            responseCode.UN_AUTHENTICATION.name,
            req.__('auth.not_authorize'),
            responseCode.UN_AUTHENTICATION.code
        );
    }
        
    try {
        const token = auth_jwt.replace(PREFIX, '').trim();
        const data = jwt.verify(token, keys.jwtSecret);
        let user: any = await User.findById(data.id);

        if (!user) throw new NotFoundError(req.__('auth.user_not_found'));
        const arrAdmin = ['ADMIN'];
        if (!arrAdmin.includes(user.type_role)) {
            return res.error(
                responseCode.UN_AUTHENTICATION.name,
                req.__('auth.not_authorize'),
                responseCode.UN_AUTHENTICATION.code
            );
        }

        req.user = user;
        req.userParseDTO = await UserRepo.getInstance().getUserById(
            ObjectId(user._id)
        );
        req.token = token;
        return next();
    } catch (error) {
        return res
            .status(responseCode.UN_AUTHENTICATION.code)
            .send(
                error_response_obj(
                    req.__('auth.not_authorize'),
                    responseCode.UN_AUTHENTICATION.name
                )
            );
    }
};

export const hasPermission = (arrayPermisson: string[]) => {
    return async (req: any, res: any, next: any) => {
        try {
            const { token, user } = req;
            if (!user) throw new NotFoundError(req.__('auth.user_not_found'));

            const permissions = await AuthService._.getAllPermissionByUser({
                token,
            });

            const permissionsExist = permissions
                .map((item) => item.name_query)
                .reduce((result, element) => {
                    if (arrayPermisson.includes(element)) result.push(element);
                    return result;
                }, []);

            if (permissionsExist.length > 0) return next();
            
            return res.error(
                responseCode.FORBIDDEN.name,
                'Không có quyền truy cập API.',
                responseCode.FORBIDDEN.code
            );
        } catch (error) {
            return res
                .status(responseCode.UN_AUTHENTICATION.code)
                .send(
                    error_response_obj(
                        req.__('auth.not_authorize'),
                        responseCode.UN_AUTHENTICATION.name
                    )
                );
        }
    };
};
