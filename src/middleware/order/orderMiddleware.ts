import moment from "moment";
import { executeError } from "../../base/appError";
import responseCode from "../../base/responseCode";
import { AddressRepo } from "../../repositories/AddressRepo";
import { OrderRepository } from "../../repositories/OrderRepo";
import { PaymentMethodRepo } from "../../repositories/PaymentMethodRepo";
import { ShippingMethodRepo } from "../../repositories/ShippingMethodRepo";
import { VoucherRepository } from "../../repositories/VoucherRepository";
import { cloneObj, isMappable, ObjectId } from "../../utils/helper";
import { PaymentMethodQuery, ShippingStatus, RefundStatus } from '../../models/enums/order';
import AddressService from "../../controllers/serviceHandles/address";
import { body, validationResult } from "express-validator";
import { NotFoundError } from "../../base/customError";
import { ShopRepo } from "../../repositories/ShopRepo";
import { OrderShippingRepo } from "../../repositories/OrderShippingRepo"
import { OrderItemRepository } from "../../repositories/OrderItemRepo";
import { ProductRepo } from "../../repositories/ProductRepo";
import { EVENT_VOUCHER_CODE, REQ_DATE_TIME_FORMAT } from "../../base/variable";
import { VIET_NAM } from "../../base/variable"
import { InMemoryVoucherStore } from "../../SocketStores/VoucherStore";

const productRepo = ProductRepo.getInstance();
const orderItemRepo = OrderItemRepository.getInstance();
const orderRepo = OrderRepository.getInstance();
const orderShippingRepo = OrderShippingRepo.getInstance();
const shippingMethodRepo = ShippingMethodRepo.getInstance();
const paymentMethodRepo = PaymentMethodRepo.getInstance();
const addressRepo = AddressRepo.getInstance();
const voucherRepo = VoucherRepository.getInstance()
const shopRepo = ShopRepo.getInstance()
const voucherAndOrderCache = InMemoryVoucherStore.getInstance();

const validToCancel = ["PENDING", "WAIT_TO_PICK"]
const validToConfirmCancel = ["CANCELING"]
const shippingMethods = ['6080f154ca33c1913de1be33', '620c942074e40ed44d99de11']
const paymentMethods = ['6080f24dca33c1913de1be35', '6080f319ca33c1913de1be36', '6080f987ca33c1913de1be38']

export const filterTypeRequire = async (req, res, next) => {
    const validValues = [
        'PENDING',
        'WAIT_TO_PICK',
        'SHIPPING',
        'SHIPPED',
        'CANCELED',
        'CANCELING',
        'CANCEL', // Including canceled & canceling
        'RETURN', // Including handling and closed request
        'RETURNING', // Only requested
        'RETURNED' // Except requested
    ]
    if (!validValues.includes(req.query.filter_type)) {
        res.error(responseCode.BAD_REQUEST.name, req.__('order.invalid_filter_type_order'), responseCode.BAD_REQUEST.code);
        return;
    }
    next()

}

export const findOrderMiddleware = async (req, res, next) => {
    try {
        const order_id = req.params.id;
        let order: any = await orderRepo.findOne({ _id: order_id });
        if (!order) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.order_not_found'), responseCode.NOT_FOUND.code);
            return;
        }
        if (req.shop_id && req.shop_id.toString() != order.shop_id) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.order_not_belong_to_shop'), responseCode.NOT_FOUND.code);
            return;
        }
        req.order = order;
        next();
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}

export const approveRejectOrderMiddleware = async (req, res, next) => {
    try {
        const { is_approved } = req.body;
        const paymentMethods = await paymentMethodRepo.find({ name: { $nin: ['CASH'] } });
        const paymentIds = paymentMethods.map(e => e._id.toString());
        const order = req.order;
        if (paymentIds.includes(order.payment_method_id.toString()) && is_approved == true && order.payment_status !== 'paid') {
            res.error(responseCode.BAD_REQUEST.name, req.__('order.can_not_accept_online_payment_but_not_pay_yet'), responseCode.BAD_REQUEST.code);
            return;
        }
        next();
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}

export const findOrderBuyerMiddleware = async (req, res, next) => {
    try {
        const order_id = req.params.id;
        let order: any = await orderRepo.findOne({ _id: order_id });
        if (!order || req.user._id.toString() != order.user_id) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.order_not_found'), responseCode.NOT_FOUND.code);
            return;
        }
        next();
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}

export const createRequestReason = [
    body("item_id")
        .exists().withMessage((value, { req }) => req.__('order.missing_item_id')),
    body("refund_quantity")
        .exists().withMessage((value, { req }) => req.__('order.missing_refund_quality')),
    body("address_id")
        .exists().withMessage((value, { req }) => req.__('order.missing_address_id')),
    body("reasons")
        .exists().withMessage((value, { req }) => req.__('order.missing_reason'))
        .isArray().isLength({ min: 1 }).withMessage((value, { req }) => req.__('order.invalid_list_reason')),
    body("images")
        .exists().withMessage((value, { req }) => req.__('order.missing_list_images'))
        .isArray().isLength({ min: 1 }).withMessage((value, { req }) => req.__('order.invalid_list_images')),
    body("note")
        .optional()
        .exists().withMessage((value, { req }) => req.__('order.missing_note')),
    body("manner")
        .exists().withMessage((value, { req }) => req.__('order.missing_manner')),
    body("email")
        .exists().withMessage((value, { req }) => req.__('order.missing_email')),
    body("phone_number")
        .exists().withMessage((value, { req }) => req.__('order.missing_phone_number'))
]

export const findOrderForRefundMiddleware = async (req, res, next) => {
    try {
        const order_id = req.params.id;
        let { item_id, refund_quantity, reasons } = req.body;

        let order: any = await orderRepo.findOne({ _id: order_id });
        order = cloneObj(order);

        let orderShippings: any = await orderShippingRepo.findOne({ order_id: order._id })
        orderShippings = cloneObj(orderShippings)

        let shop: any = await shopRepo.findOne({ _id: ObjectId(order.shop_id) })

        if (!order || req.user._id.toString() != order.user_id) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.order_not_found'), responseCode.NOT_FOUND.code);
            return;
        }

        let orderItem: any = await orderItemRepo.findOne({ _id: ObjectId(item_id), order_id: ObjectId(order._id) })
        orderItem = cloneObj(orderItem)
        if (!orderItem) {
            res.error(responseCode.BAD_REQUEST.name, req.__('order.product_item_not_on_order'), responseCode.BAD_REQUEST.code);
            return;
        }

        let { product } = orderItem;
        const { history  } = orderShippings;

        if (!product) {
            res.error(responseCode.BAD_REQUEST.name, req.__('order.product_invalid'), responseCode.BAD_REQUEST.code);

            return;
        }

        if (!history || !Array.isArray(history)) {
            res.error(responseCode.BAD_REQUEST.name, req.__('order.order_not_delivered'), responseCode.BAD_REQUEST.code);
            return;
        }

        const shippingInfo = history.find(item => item.status_id === 5);
        if(!shippingInfo && !shippingInfo?.action_time) {
            res.error(responseCode.BAD_REQUEST.name, req.__('order.order_not_delivered'), responseCode.BAD_REQUEST.code);
            return;
        }

        const createTime = moment(new Date(shippingInfo?.action_time), REQ_DATE_TIME_FORMAT, true)
        const currentTime = moment(new Date(), REQ_DATE_TIME_FORMAT, true)
        const duration = moment.duration(currentTime.diff(createTime));

        // IGNORE THIS CHECK FROM 07/12/2021
        // if (!product.allow_refund) {
        //     res.error(responseCode.BAD_REQUEST.name, req.__('order.product_not_refund'), responseCode.BAD_REQUEST.code);
        //     return;
        // }

        if (!product.hasOwnProperty("duration_refund")) {
            res.error(responseCode.BAD_REQUEST.name, req.__('order.not_found_duration_refund'), responseCode.BAD_REQUEST.code);
            return;
        }
        
        const timeRefund = product.duration_refund || 3
        if ( duration.asDays() > timeRefund) {
            res.error(responseCode.BAD_REQUEST.name, req.__('order.product_not_refund'), responseCode.BAD_REQUEST.code);
            return;
        }

        refund_quantity = Math.trunc(+refund_quantity)
        if (refund_quantity <= 0 || refund_quantity > +orderItem.quantity) {
            res.error(responseCode.MISSING.name, req.__('order.refund_quantity_invalid'), responseCode.MISSING.code);
            return;
        }

        let list_reasons: any = []
        product.refund_conditions.map(reason => {
            if (reason.active) list_reasons.push(reason.code)
        })

        // IGNORE THIS CHECK FROM 07/12/2021
        // let checker = false;
        // reasons.map(el => {
        //     if (list_reasons.includes(el)) {
        //         checker = true;
        //     }
        // })

        // if (!checker) {
        //     res.error(responseCode.MISSING.name, req.__('order.reason_invalid'), responseCode.MISSING.code);
        //     return;
        // }

        if (order.shipping_status !== 'shipping' && order.shipping_status !== 'shipped' && order.shipping_status !== 'return') {
            res.error(responseCode.BAD_REQUEST.name, req.__('order.order_not_received'), responseCode.BAD_REQUEST.code);
            return;
        }

        const delivered = orderShippings.history.filter(item => item.status_id === 5)

        if (!delivered || delivered.length <= 0) {
            res.error(responseCode.BAD_REQUEST.name, req.__('order.order_not_delivered'), responseCode.BAD_REQUEST.code);
            return;
        }

        const refund_item = order.refund_information.some(item => item.item_id === item_id)
        if (refund_item) {
            res.error(responseCode.BAD_REQUEST.name, req.__('order.request_refund_for_item_exists'), responseCode.BAD_REQUEST.code);
            return;
        }

        req.order = order;
        req.orderItem = orderItem;
        next();
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}

export const checkValidToCancelRefundByBuyer = async (req, res, next) => {
    try {
        const order_id = req.params.id;
        const { item_id } = req.body;

        let order: any = await orderRepo.findOne({ _id: order_id });
        order = cloneObj(order)
        const { refund_information } = order;

        if (!order) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.order_not_found'), responseCode.NOT_FOUND.code);
            return;
        }

        if (req.user._id != order.user_id.toString()) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.can_not_cancel_order_of_your_own'), responseCode.NOT_FOUND.code);
            return;
        }

        if (order.shipping_status.toUpperCase() !== "RETURN") {
            res.error(responseCode.NOT_FOUND.name, req.__('order.can_only_cancel_request_refund_when_it_was_requested'), responseCode.NOT_FOUND.code);
            return;
        }

        if (!item_id) {
            res.error(responseCode.SERVER.name, req.__('order.missing_item_id'), responseCode.SERVER.code);
            return;
        }

        const product_request_refund = refund_information.find(item => item.item_id === item_id)
        if (product_request_refund.status.toUpperCase() !== "OPENING") {
            res.error(responseCode.NOT_FOUND.name, req.__('order.can_only_cancel_request_refund_when_it_was_requested'), responseCode.NOT_FOUND.code);
            return;
        }

        let { status } = product_request_refund.shop_review
        if (status.toUpperCase() !== "PENDING") {
            res.error(responseCode.NOT_FOUND.name, req.__('order.irrevocable_request_refund'), responseCode.NOT_FOUND.code);
            return;
        }

        req.order = order;
        req.product_request_refund = product_request_refund
        next();
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}

export const checkValidOrderSellerNote = async (req, res, next) => {
    try {
        const order_id = req.params.id;
        let order: any = await orderRepo.findOne({ _id: order_id });
        if (req.body.seller_note === undefined) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.missing_note_order'), responseCode.NOT_FOUND.code);
            return;
        }
        if (!order) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.order_not_found'), responseCode.NOT_FOUND.code);
            return;
        }
        if (req.shop_id != order.shop_id.toString()) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.can_not_cancel_order_not_belong_to_shop'), responseCode.NOT_FOUND.code);
            return;
        }
        req.order = order
        next();
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}

export const checkValidToCancelSeller = async (req, res, next) => {
    try {
        const order_id = req.params.id;
        let order: any = await orderRepo.findOne({ _id: order_id });
        if (!req.body.cancel_reason) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.missing_reason_cancel_order'), responseCode.NOT_FOUND.code);
            return;
        }
        if (!order) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.order_not_found'), responseCode.NOT_FOUND.code);
            return;
        }
        if (req.shop_id != order.shop_id.toString()) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.order_not_found'), responseCode.NOT_FOUND.code);
            return;
        }
        if (!validToCancel.includes(order.shipping_status.toUpperCase())) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.status_order_not_match_to_cancel'), responseCode.NOT_FOUND.code);
            return;
        }
        let orderShipping:any = await orderShippingRepo.findOne({ order_id: order_id, shop_id: req.shop_id });
        orderShipping = cloneObj(orderShipping)
        if (orderShipping && orderShipping.shipping_info != null ) {
            res.error(responseCode.BAD_REQUEST.name, req.__('order.irrevocable_order'), responseCode.BAD_REQUEST.code);
            return;
        }
        req.order = order
        next();
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}

export const checkValidToConfirmCancel = async (req, res, next) => {
    try {
        const order_id = req.params.id;
        let order: any = await orderRepo.findOne({ _id: order_id });

        if (!req.body.is_approved && typeof req.body.is_approved != "boolean") {
            res.error(responseCode.NOT_FOUND.name, req.__('order.missing_is_approved_order'), responseCode.NOT_FOUND.code);
            return;
        }
        if (!order) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.order_not_found'), responseCode.NOT_FOUND.code);
            return;
        }
        if (req.shop_id != order.shop_id.toString()) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.can_not_cancel_order_not_belong_to_shop'), responseCode.NOT_FOUND.code);
            return;
        }
        if (!validToConfirmCancel.includes(order.shipping_status.toUpperCase())) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.status_order_not_match_to_cancel'), responseCode.NOT_FOUND.code);
            return;
        }
        req.order = order
        next();
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}

export const checkValidToCancel = async (req, res, next) => {
    try {
        const order_id = req.params.id;
        let order: any = await orderRepo.findOne({ _id: order_id });
        if (!req.body.cancel_reason) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.missing_reason_cancel_order'), responseCode.NOT_FOUND.code);
            return;
        }
        if (!order) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.order_not_found'), responseCode.NOT_FOUND.code);
            return;
        }
        if (req.user._id != order.user_id.toString()) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.can_not_cancel_order_of_your_own'), responseCode.NOT_FOUND.code);
            return;
        }
        if (!validToCancel.includes(order.shipping_status.toUpperCase())) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.can_only_cancel_order_when_not_delivery_yet'), responseCode.NOT_FOUND.code);
            return;
        }
        let orderShipping:any = await orderShippingRepo.findOne({ order_id: order_id, user_id: req.user._id });
        orderShipping = cloneObj(orderShipping)
        if (orderShipping && orderShipping.shipping_info != null ) {
            res.error(responseCode.BAD_REQUEST.name, req.__('order.irrevocable_order'), responseCode.BAD_REQUEST.code);
            return;
        }
        req.order = order
        next();
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}

export const checkValidToCreateOrder = async (req, res, next) => {
    try {
        const { shops, payment_method_id, address_id, total_order_payment, order_room_id, group_id } = req.body
        const userID = req.user._id.toString()
        let dataProduct: any
        req.order_info = {}
        let paymentQuery = null;

        if (shops) {
            // Check cart_item
            let checkItem: any = {
                success: true,
                message: ''
            }
            const promises = shops.map(shop => {
                return new Promise(async (resolve, reject) => {
                    const { items, shipping_method_id } = shop
                    // Check shipping method

                    if (shippingMethods.includes(shipping_method_id)) { // just for 21/12 events - do not remove
                        // const shippingMethod = await shippingMethodRepo.findOne({ _id: ObjectId(shipping_method_id) })
                        // if (shippingMethod) {
                        //     req.order_info.shipping_method = shippingMethod
                        // }
                        // else {
                        //     checkItem = {
                        //         success: false,
                        //         message: req.__('order.invalid_shipping_method')
                        //     }
                        //     reject(checkItem)
                        // }
                    }
                    else {
                        checkItem = {
                            success: false,
                            message: req.__('order.missing_shipping_method')
                        }
                        reject(checkItem)
                    }

                    if (items && Array.isArray(items) && items.length) {
                        try {
                            const result = await orderRepo.checkCartItemBeforeCreateOrder(items, userID, shop.shop_id, req, order_room_id, group_id)
                            dataProduct = result.dataItems[0].product
                            req.dataItems = req.dataItems ? req.dataItems.concat(result.dataItems) : result.dataItems
                            resolve(checkItem)
                        } catch (error) {
                            checkItem = {
                                success: false,
                                message: error.message
                            }
                            reject(checkItem)
                        }
                    }
                })
            })
            const checkResult: any = await Promise.all(promises).then(data => {
                return data[0]
            })

            if (!checkResult.success) {
                res.error(responseCode.BAD_REQUEST.name, checkResult.message, responseCode.BAD_REQUEST.code);
                return;
            }
        }
        else {
            res.error(responseCode.MISSING.name, req.__('order.missing_info_to_order'), responseCode.MISSING.code);
            return;
        }

        // Check payment method
        // if (payment_method_id === "6080f987ca33c1913de1be38") {
        if (paymentMethods.includes(payment_method_id)) { // just for 21/12 events - do not remove
            // const paymentMethod: any = await paymentMethodRepo.findOne({ _id: ObjectId(payment_method_id) })
            // if (paymentMethod) {
            //     req.order_info.payment_method = paymentMethod;
            //     paymentQuery = paymentMethod.name_query;
            // }
            // else {
            //     res.error(responseCode.BAD_REQUEST.name, req.__('order.invalid_payment_method'), responseCode.BAD_REQUEST.code);
            //     return;
            // }
        }
        else {
            res.error(responseCode.MISSING.name, req.__('order.missing_payment_method'), responseCode.MISSING.code);
            return;
        }

        // Check address
        if (address_id) {
            const addressOrder = await addressRepo.findWithUser(address_id, userID)
            if (addressOrder) {
                req.order_info.address = addressOrder
                if(order_room_id) {
                    const mainAddress = addressOrder.street + ", " + addressOrder.ward.name + ", " 
                    + addressOrder.district.name + ", " + addressOrder.state.name + ", " + addressOrder.phone
                    const id = dataProduct._id.toString()
                    const groupBuyId = dataProduct.group_buy.shop_group_buy_id.toString()
                    const key = `group_buy_${groupBuyId}_${id}`
                    const field = mainAddress.toLowerCase().replace(/\s+/g, ' ').trim()
                    const groupBuyByAddress = await voucherAndOrderCache.getByKeyAndField(key, field, true)
                    if(+groupBuyByAddress > 0) {
                        res.error(responseCode.BAD_REQUEST.name, 'Sản phẩm mua chung không còn hợp lệ cho bạn', responseCode.BAD_REQUEST.code)
                        return;
                    }
                    voucherAndOrderCache.increaseValueFromKeyAndField(key, field, 1)
                }
            }
            else {
                res.error(responseCode.BAD_REQUEST.name, req.__('order.invalid_address'), responseCode.BAD_REQUEST.code);
                return;
            }
        }
        else {
            res.error(responseCode.MISSING.name, req.__('order.missing_address'), responseCode.MISSING.code);
            return;
        }

        // Check price
        if (total_order_payment > 20_000_000 && paymentQuery === PaymentMethodQuery.CASH) {
            res.error(responseCode.MISSING.name, req.__('order.limit_price'), responseCode.MISSING.code);
            return;
        }

        // Check voucher

        next()
    } catch (error) {
        console.log(error)
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}

export const ConfirmShippedMiddleware = async (req, res, next) => {
    try {
        const order_id = req.params.id;
        let order: any = await orderRepo.findOne({ _id: order_id });
        if (!order || req.user._id.toString() != order.user_id) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.order_not_found'), responseCode.NOT_FOUND.code);
            return;
        }
        if (order.shipping_status != ShippingStatus.SHIPPING && order.shipping_status != ShippingStatus.RETURN) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.status_order_not_match'), responseCode.NOT_FOUND.code);
            return;
        }
        next();
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}

export const ConfirmShippedSellerMiddleware = async (req, res, next) => {
    try {
        const order_id = req.params.id;
        let order: any = await orderRepo.findOne({ _id: order_id });
        if (req.shop_id && req.shop_id.toString() != order.shop_id) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.order_not_belong_to_shop'), responseCode.NOT_FOUND.code);
            return;
        }
        if (order.shipping_status != ShippingStatus.SHIPPING && order.shipping_status != ShippingStatus.RETURN) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.status_order_not_match'), responseCode.NOT_FOUND.code);
            return;
        }
        next();
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}

export const isShopOwnerOrder = async (req, res, next) => {
    try {
        let { address_id, order_id, shop_id } = req.body;

        if (!shop_id) {
            shop_id = req.shop_id
        }

        const shop = req.user.shop

        if(shop.country !== VIET_NAM) {
            throw new Error(req.__('order.shop_from_ko_can_not_prepare_order'))
        }

        let orderShop = await orderRepo.findOne({ _id: order_id, shop_id: shop_id });
        if (!orderShop) throw new NotFoundError(req.__('order.order_not_belong_to_shop'));
        const addressShop = await AddressService._.checkValidPickAddressDefault(address_id, req.user._id, orderShop)
        if (!addressShop) {
            throw new Error(req.__('order.address_not_match'))
        }
        req.addressShop = addressShop;
        req.orderShop = orderShop;
        next();
    } catch (error) {
        console.log(error)
        res.error('VALIDATION_ERROR', error.message, 422)
    }
}

export const isShopOwnerOrderCMS = async (req, res, next) => {
    try {
        let { address_id, order_id, shop_id } = req.body;

        if (!shop_id) {
            shop_id = req.shop_id
        }

        let orderShop: any = await orderRepo.findOne({ _id: order_id, shop_id: shop_id });
        if (!orderShop) throw new NotFoundError(req.__('order.order_not_belong_to_shop'));

        let shopPrepare: any = await shopRepo.findOne({ _id: ObjectId(orderShop.shop_id) });
        let userId = shopPrepare.user_id;

        const addressShop = await AddressService._.checkValidPickAddressDefault(address_id, userId, orderShop)
        if (!addressShop) {
            throw new Error(req.__('order.address_not_match'))
        }
        req.addressShop = addressShop;
        req.orderShop = orderShop;
        // shop from order shop
        req.shop = shopPrepare;
        next();
    } catch (error) {
        console.log(error)
        res.error('VALIDATION_ERROR', error.message, 422)
    }
}

export const createOrderSellerRule = [
    body("order_id")
        .exists().withMessage((value, { req }) => req.__('order.missing_order_id')),
    body("address_id")
        .exists().withMessage((value, { req }) => req.__('order.missing_address_id'))
]

export const orderMiddleware = (req, res, next) => {
    const error = validationResult(req).formatWith(({ msg }) => msg);
    const hasError = !error.isEmpty();
    if (hasError) {
        res.error('VALIDATION_ERROR', error.array()[0], 422)
    } else {
        next();
    }
}

export const checkOrderOfShop = async (req, res, next) => {
    try {
        const { id } = req.params;
        let { shop_id } = req.body
        if (!shop_id) {
            shop_id = req.shop_id
        }

        let orderShop = await orderRepo.findOne({ _id: id, shop_id: shop_id });
        if (!orderShop) throw new NotFoundError(req.__('order.order_not_belong_to_shop'));
        req.orderShop = orderShop;
        next();
    } catch (error) {
        console.log(error)
        res.error('VALIDATION_ERROR', error.message, 422)
    }
}

export const checkValidVoucher = async (req, res, next) => {
    try {
        const { system_voucher: systemVoucher, shops, total_order_items: totalOrderItems, payment_method_id: paymentMethodId, order_room_id } = req.body;
        let discountSystemId, freeShipSystemId, cashBackId, cashBackDiscountId;

        // modify system_voucher when have param order_room_id
        if(order_room_id) {
            req.body.system_voucher = systemVoucher.filter(ele => ele.classify === 'free_shipping' ? freeShipSystemId = ele._id : false);   
        } else {
            systemVoucher.forEach(ele => {
                if (ele.classify === 'discount') {
                    discountSystemId = ele._id;
                } else if(ele.classify === 'cash_back') {
                    cashBackId = ele._id;
                }
                else if(ele.classify === 'free_shipping') {
                    freeShipSystemId = ele._id;
                }
                else if(ele.classify === 'cash_back_discount') {
                    cashBackDiscountId = ele._id
                }
            });
        }
        

        let shopIds: any = [];
        let productIds: any = [];
        let shippingMethodIds: any = [];
        let shopVouchers: any = [];
        shops.forEach(shop => {
            shopIds.push(shop.shop_id);
            shippingMethodIds.push(shop.shipping_method_id);
            shop.items.forEach(item => {
                productIds.push(item.product_id);
            });
            if (shop.vouchers && shop.vouchers[0]?._id) {
                let object = {
                    shop_id: shop.shop_id,
                    voucher_id: shop.vouchers[0]?._id
                }
                shopVouchers.push(object);
            }

        });
        const disctionShippingIds = [...new Set(shippingMethodIds)];

        // Object Condition to find valid voucher
        const ObjectCondition: any = {
            total_value: totalOrderItems,
            shops: shopIds,
            products: productIds,
            payment_method: paymentMethodId,
            shipping_method: disctionShippingIds,
            user_id: req.user._id
        }

        let voucherLists:any = []
        // check valid voucher system
        const [isValidDiscount, isValidShipping, isValidCashBack, isValidCashBackDiscount] = await Promise.all([
            (async () => {
                const discountVoucherSystem = await voucherRepo.checkValidVoucherOnCheckout(discountSystemId?.toString(), ObjectCondition, 'discount');
                if (discountVoucherSystem) {
                    voucherLists.push(discountVoucherSystem)
                    if(discountVoucherSystem.code === EVENT_VOUCHER_CODE) {
                        req.voucher_event = discountVoucherSystem
                    }
                }
                
                return discountVoucherSystem || !discountSystemId
            })(),
            (async () => {
                const freeShipVoucherSystem = await voucherRepo.checkValidVoucherOnCheckout(freeShipSystemId?.toString(), ObjectCondition, 'free_shipping');
                if(freeShipVoucherSystem) {
                    voucherLists.push(freeShipVoucherSystem)
                }
                return freeShipVoucherSystem || !freeShipSystemId
            })(),
            (async () => {
                const cashBackVoucherSystem = await voucherRepo.checkValidCashBackVoucher(ObjectCondition, cashBackId?.toString());
                if (cashBackVoucherSystem) {
                    voucherLists.push(cashBackVoucherSystem)
                }
                return cashBackVoucherSystem || !cashBackId ? true : false
            })(),
            (async () => {
                const cashBackDiscountVoucherSystem = await voucherRepo.checkValidVoucherSystemIncart(ObjectCondition, cashBackDiscountId?.toString());
                if (cashBackDiscountVoucherSystem.length) {
                    voucherLists.push(cashBackDiscountVoucherSystem[0])
                }
                return cashBackDiscountVoucherSystem.length || !cashBackDiscountId ? true : false
            })()
        ])
        
        if (!isValidDiscount || !isValidShipping || !isValidCashBack || !isValidCashBackDiscount) {
            res.error(responseCode.SERVER.name, req.__('voucher.invalid_system_voucher'), responseCode.SERVER.code);
            return;
        }

        // check valid voucher base on address
        let eventVoucher = null
        voucherLists.forEach(voucher => {
            if(voucher.code === EVENT_VOUCHER_CODE) {
                eventVoucher = voucher
            }
        })

        if(eventVoucher) {
            const addressOrder = req.order_info.address
            const mainAddress = addressOrder.street + ", " + addressOrder.ward.name + ", " 
            + addressOrder.district.name + ", " + addressOrder.state.name + ", " + addressOrder.phone
            const key = `voucher_event_${eventVoucher._id.toString()}`
            const field = mainAddress.toLowerCase().replace(/\s+/g, ' ').trim()
            const voucherByAddress = await voucherAndOrderCache.getByKeyAndField(key, field, true)
            if(+voucherByAddress > 0) {
                res.error(responseCode.BAD_REQUEST.name, 'Mã giảm giá sự kiện không còn hợp lệ cho bạn', responseCode.BAD_REQUEST.code)
                return;
            }

            voucherAndOrderCache.increaseValueFromKeyAndField(key, field, 1)
        }

        // increase number of voucher used by user in cache
        voucherLists.map(voucher => {
            const key = `voucher_${voucher._id.toString()}`
            const historyKey = `voucherHistoryDay_${voucher._id.toString()}`
            const fieldHistory = `${req.user._id.toString()}_${moment().format("DD-MM-YY")}`
            voucherAndOrderCache.increaseValueFromKeyAndField(key, req.user._id.toString(), 1)
            voucherAndOrderCache.increaseValueFromKeyAndField(historyKey, fieldHistory, 1)
        })

        let shopVoucher = await voucherRepo.checkShopVoucherCreateOrder(shopVouchers, ObjectCondition);
        shopVoucher = cloneObj(shopVoucher)
        let groupShopVoucher = shopVoucher.map(item => item[0])
        groupShopVoucher = groupShopVoucher.filter(item => item)
        voucherLists = voucherLists.concat(groupShopVoucher).filter(ele => ele)
        req.voucherLists = voucherLists
        next();
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }

}

export const createBannerRule = [
    body("name")
        .exists().withMessage((value, { req }) => req.__('banner.missing_name_banner'))
        .isLength({ min: 5, max: 200 }).withMessage((value, { req }) => req.__('banner.invalid_name_banner'))
        .trim(),
    body("image")
        .exists().withMessage((value, { req }) => req.__('banner.missing_image_banner'))
        .trim(),
    body("promo_link")
        .optional()
        .isLength({ min: 5, max: 500 }).withMessage((value, { req }) => req.__('banner.missing_promo_link_banner'))
        .trim(),
    body("start_time")
        .exists().withMessage((value, { req }) => req.__('banner.missing_start_time_banner')),
    body("end_time")
        .exists().withMessage((value, { req }) => req.__('banner.missing_end_time_banner')),
    body("classify")
        .optional()
        .isIn(['product', 'voucher', 'link']).withMessage((value, { req }) => req.__('banner.invalid_classify_banner'))
]

export const approveRejectOrderRule = [
    body("is_approved")
        .exists().withMessage((value, { req }) => req.__('order.missing_is_approved_refund')),
    body("item_id")
        .exists().withMessage((value, { req }) => req.__('order.invalid_item_id'))
]

export const approveRejectOrderValidate = (req, res, next) => {
    const error = validationResult(req).formatWith(({ msg }) => msg);
    const hasError = !error.isEmpty();
    if (hasError) {
        res.error('VALIDATION_ERROR', error.array()[0], 422)
    } else next();
}
export const approveRejectOrderRefundMiddleware = async (req, res, next) => {
    try {
        const order_id = req.params.id;
        const { is_approved, item_id, reject_reason } = req.body;
        let order:any, shop:any;
        [order, shop] = await Promise.all([
            (async () => {
                const data: any = await orderRepo.findOne({ _id: order_id });
                return data
            })(),
            (async () => {
                const data = await shopRepo.findShopByUserId(req.user._id.toString());
                return data
            })()
        ])
        
        
        if (!order || shop._id.toString() != order.shop_id.toString()) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.order_not_found'), responseCode.NOT_FOUND.code);
            return;
        }

        let info, indexObj = -1;
        let refundInfos = order.refund_information;
        refundInfos = cloneObj(refundInfos);
        refundInfos.forEach((el, index) => {
            if (el.item_id.toString() === item_id.toString()) {
                indexObj = index
                info = el;
            }
        });

        if (indexObj === -1) {
            res.error(responseCode.SERVER.name, req.__('order.invalid_item_id'), responseCode.SERVER.code);
            return;
        }

        if (info?.status !== RefundStatus.OPENING) {
            res.error(responseCode.SERVER.name, req.__('order.invalid_refund_status'), responseCode.SERVER.code);
            return;
        }
        if (!is_approved) {
            if (!reject_reason) {
                res.error(responseCode.SERVER.name, req.__('order.missing_reject_refund_reason'), responseCode.SERVER.code);
                return;
            }
            if (!reject_reason?.reasons || !reject_reason.reasons.length) {
                res.error(responseCode.SERVER.name, req.__('order.missing_reject_refund_reason'), responseCode.SERVER.code);
                return;
            }
            if (!reject_reason?.images || !reject_reason.images.length) {
                res.error(responseCode.SERVER.name, req.__('order.missing_reject_refund_image'), responseCode.SERVER.code);
                return;
            }
            if (!reject_reason?.email) {
                res.error(responseCode.SERVER.name, req.__('order.missing_reject_refund_email'), responseCode.SERVER.code);
                return;
            }
        }

        req.info = info;
        req.indexObj = indexObj;
        req.order = order;
        req.refundInfos = refundInfos;

        next();
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}


export const approveRejectOrderRefundAdminMiddleware = async (req, res, next) => {
    try {
        const order_id = req.params.id;
        const { item_id } = req.body;

        let order: any = await orderRepo.findOne({ _id: order_id });

        if (!order) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.order_not_found'), responseCode.NOT_FOUND.code);
            return;
        }

        let info, indexObj = -1;
        let refundInfos = order.refund_information;
        refundInfos = cloneObj(refundInfos);
        refundInfos.forEach((el, index) => {
            if (el.item_id.toString() === item_id.toString()) {
                indexObj = index
                info = el;
            }
        });

        if (indexObj === -1) {
            res.error(responseCode.SERVER.name, req.__('order.invalid_item_id'), responseCode.SERVER.code);
            return;
        }

        if (info?.status !== RefundStatus.OPENING) {
            res.error(responseCode.SERVER.name, req.__('order.invalid_refund_status'), responseCode.SERVER.code);
            return;
        }

        req.info = info;
        req.indexObj = indexObj;
        req.order = order;
        req.refundInfos = refundInfos;

        next();
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}

export const confirmTransferRefundMoneyAdminMiddleware = async (req, res, next) => {
    try {
        const order_id = req.params.id;
        const { item_id } = req.body;

        if (!item_id) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.invalid_item_id'), responseCode.NOT_FOUND.code);
            return;
        }
        
        let order: any = await orderRepo.findOne({ _id: order_id });
        order = cloneObj(order)

        if (!order) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.order_not_found'), responseCode.NOT_FOUND.code);
            return;
        }

        if (!order.refund_information) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.order_invalid'), responseCode.NOT_FOUND.code);
            return;
        }

        let info, indexObj = -1;
        let refundInfos = order.refund_information;
        refundInfos.forEach((el, index) => {
            if (el.item_id.toString() === item_id.toString()) {
                indexObj = index
                info = el;
            }
        });

        if (indexObj === -1) {
            res.error(responseCode.SERVER.name, req.__('order.invalid_item_id'), responseCode.SERVER.code);
            return;
        }

        if (info?.status !== RefundStatus.OPENING) {
            res.error(responseCode.SERVER.name, req.__('order.invalid_refund_status'), responseCode.SERVER.code);
            return;
        }

        req.item_id = item_id;
        req.info = info;
        req.indexObj = indexObj;
        req.order = order;
        req.refundInfos = refundInfos;

        next();
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}

export const confirmReceivedBySellerMiddleware = async (req, res, next) => {
    try {
        const order_id = req.params.id;
        const { item_id } = req.body;
        const { shop } = req.user;
        let order: any = await orderRepo.findOne({ _id: order_id });

        if (!order || shop._id.toString() != order.shop_id.toString()) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.order_not_found'), responseCode.NOT_FOUND.code);
            return;
        }

        if (order.shipping_status != ShippingStatus.RETURN) {
            res.error(responseCode.NOT_FOUND.name, req.__('order.status_order_not_match'), responseCode.NOT_FOUND.code);
            return;
        }

        let info, indexObj = -1;
        let refundInfos = order.refund_information;
        refundInfos = cloneObj(refundInfos);
        refundInfos.forEach((el, index) => {
            if (el.item_id.toString() === item_id.toString()) {
                indexObj = index
                info = el;
            }
        });

        if (indexObj === -1) {
            res.error(responseCode.SERVER.name, req.__('order.invalid_item_id'), responseCode.SERVER.code);
            return;
        }

        if (info?.status !== RefundStatus.OPENING) {
            res.error(responseCode.SERVER.name, req.__('order.invalid_refund_status'), responseCode.SERVER.code);
            return;
        }

        req.info = info;
        req.indexObj = indexObj;
        req.order = order;
        req.refundInfos = refundInfos;

        next();
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}

export const checkValidProductInformation = async (req, res, next) => {
    try {
        const {order_id} = req.body
        let orderItems:any = await orderItemRepo.find({order_id: ObjectId(order_id)})
        orderItems = cloneObj(orderItems)
        const isValid = orderItems.filter(item => !item.product.weight || item.product.weight === 0).length ? false : true
        if(!isValid) {
            res.error(responseCode.BAD_REQUEST.name, req.__('order.product_have_not_weight'), responseCode.BAD_REQUEST.code);
            return;
        }
        next()
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}