import { truncate } from "fs";
import responseCode from "../../base/responseCode";
import { AddressRepo } from "../../repositories/AddressRepo";
import { ShopRepo } from "../../repositories/ShopRepo";
const { body, validationResult } = require("express-validator");

const addressRepo = AddressRepo.getInstance();
const shopRepo = ShopRepo.getInstance();

export const createAddressRule = [
    body("name")
        .exists().withMessage((value, { req }) => req.__('address.missing_name')),
    body("state")
        .exists().withMessage((value, { req }) => req.__('address.missing_province')),
    body("district")
        .exists().withMessage((value, { req }) => req.__('address.missing_district')),
    body("ward")
        .exists().withMessage((value, { req }) => req.__('address.missing_ward')),
    body("street")
        .exists().withMessage((value, { req }) => req.__('address.missing_specific_address')),
    body("phone")
        .exists().withMessage((value, { req }) => req.__('address.missing_phone_number'))
]

export const createAddressValidate = (req, res, next) => {
    const error = validationResult(req).formatWith(({ msg }) => msg);
    const hasError = !error.isEmpty();
    if (hasError) {
        res.error('VALIDATION_ERROR', error.array()[0], 422)
    } else next();
}

export const phoneValidate = (req, res, next) => {
    let vnf_regex: any;
    let phone = req.body.phone.trim();
    if (phone.charAt(0) == '+') {
        vnf_regex = /((849|843|847|848|845)+([0-9]{8})\b)/g;
        phone = phone.slice(1);
    } else {
        vnf_regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
    }
    if (vnf_regex.test(phone) == false) {
        throw new Error(req.__('address.invalid_phone_number'));
    } else next();

}

export const objectAddressValidate = (req, res, next) => {
    if (!req.body.state?.id || !req.body.state?.name) {
        res.error("VALIDATION_ERROR", "Invalid Format State", 422);
        return;
    }
    if (!req.body.district?.id || !req.body.district?.name) {
        res.error("VALIDATION_ERROR", "Invalid Format District", 422);
        return;
    }
    if (!req.body.ward?.id || !req.body.ward?.name) {
        res.error("VALIDATION_ERROR", "Invalid Format Ward", 422);
        return;
    }
    next();
}

export const deleteAddressValidate = async (req, res, next) => {
    try {
        const address = await addressRepo.findById(req.params.id);
        if (!address || address.accessible_id != req.user._id.toString()) {
            res.error("NOT_FOUND_ERROR", req.__('address.address_not_found'), 404);
            return;
        }
        if (address.is_default == true) {
            res.error("VALIDATION_ERROR", req.__('address.can_not_delete_default_address'), 422);
            return;
        }
        const shop = await shopRepo.findShopByUserId(req.user._id);
        if (shop) {
            if (address.is_pick_address_default == true) {
                res.error("VALIDATION_ERROR", req.__('address.can_not_delete_default_pick_address'), 422);
                return;
            }
            if (address.is_return_address_default == true) {
                res.error("VALIDATION_ERROR", req.__('address.can_not_delete_default_return_address'), 422);
                return;
            }
        }
        next();
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
        return;
    }

}

export const updateAddressMiddlewware = async (req, res, next) => {
    try {
        const address = await addressRepo.findById(req.params.id);
        const { is_default, is_pick_address_default, is_return_address_default } = req.body;
        if (!address || address.accessible_id != req.user._id.toString()) {
            res.error("NOT_FOUND_ERROR", req.__('address.address_not_found'), 404);
            return;
        }
        const shop = await shopRepo.findShopByUserId(req.user._id);
        if (address.is_default && !address.is_default === is_default) {
            res.error("VALIDATION_ERROR", req.__('address.can_not_uncheck_default_address'), 422);
            return;
        }
        if (shop) {
            if (address.is_pick_address_default && !address.is_pick_address_default === is_pick_address_default) {
                res.error("VALIDATION_ERROR", req.__('address.can_not_uncheck_default_pick_address'), 422);
                return;
            }
            if (address.is_return_address_default && !address.is_return_address_default == is_return_address_default) {
                res.error("VALIDATION_ERROR", req.__('address.can_not_uncheck_default_return_address'), 422);
                return;
            }
        }
        next();
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
        return;
    }
}

export const checkTypeMiddleware = async (req, res, next) => {
    try {
        const { expected_delivery, address_type } = req.body
        if(expected_delivery && expected_delivery !== "any_time" && expected_delivery !== "work_time") {
            res.error("VALIDATION_ERROR", req.__('address.missing_expected_delivery'), 422);
            return;
        }
        if(address_type && address_type !== "home" && address_type !== "company") {
            res.error("VALIDATION_ERROR", req.__('address.mising_address_type'), 422);
            return;
        }
        next()
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
        return;
    }
}

export const updateAddressRule = createAddressRule;
export const updateAddressValidate = createAddressValidate;
