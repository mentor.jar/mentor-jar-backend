import i18n from "i18n";

export const localeMiddleware = async (req: any, res: any, next: any) => {
    try {
        let language = 'en';
        if (req.headers['language']) {
            language = req.headers['language'];
        }
        i18n.setLocale(language);
        next();
    } catch (error) {
    }
}