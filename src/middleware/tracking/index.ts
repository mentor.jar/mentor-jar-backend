import responseCode from "../../base/responseCode";
import { ProductRepo } from "../../repositories/ProductRepo";
import { TrackingType } from "../../services/tracking/definitions";
import { TrackingObjectRepo } from "../../repositories/TrackingObjectRepo";
import { cloneObj, ObjectId } from "../../utils/helper";

const trackingObjectRepo = TrackingObjectRepo.getInstance()

export const validateUserInteractiveProduct = async (req, res, next) => {

    let error: any = {
        name: responseCode.BAD_REQUEST.name,
        message: '',
        statusCode: responseCode.BAD_REQUEST.code
    }
    let existError = false;

    // pause_mode
    let { product_id, action_type } = req.body;

    let product: any = await ProductRepo.getInstance().findOrFail({
        _id: product_id, $or: [
            { deleted_at: { $exists: false } },
            { deleted_at: null }
        ]
    });

    if (!product) {
        existError = true
        error.message = req.__("tracking.product_not_found");
        error.name = responseCode.NOT_FOUND.name
        error.code = responseCode.NOT_FOUND.code
    }
    let action_type_list = [TrackingType.AddProductToCart, TrackingType.AddProductToCartStream, TrackingType.ViewProduct, TrackingType.BuyProduct, TrackingType.BuyProductInStream, TrackingType.AddProductToFavorite];
    if (!action_type_list.includes(action_type)) {
        existError = true
        error.message = req.__("tracking.invalid_action");
    }

    if (existError) {
        res.error(error.name, error.message, error.statusCode);
        return;
    }
    next();
}

export const validateTrackingObject = async (req, res, next) => {
    try {
        let { target_id } = req.body;
        let { user } = req;

        let trackingObject:any = await trackingObjectRepo.findOne(
            { 
                target_id: ObjectId(target_id), 
                user_id: ObjectId(user._id)
            }
        )
        trackingObject = cloneObj(trackingObject)

        if (!trackingObject) {
            res.error(responseCode.NOT_FOUND.name, req.__('tracking.invalid_target_id'), responseCode.NOT_FOUND.code)
            return;
        }

        if (trackingObject.is_read) {
            res.error(responseCode.NO_CONTENT.name, req.__('tracking.object_tracked'), responseCode.NO_CONTENT.code)
            return;
        }

        req.tracking_object_id = trackingObject._id
        
        next();
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
        return;
    }   
}