import { executeError } from '../../base/appError';
import responseCode from '../../base/responseCode';
import { UserRepo } from '../../repositories/UserRepo';
import { ObjectId } from '../../utils/helper';

const userRepo = UserRepo.getInstance();

export const verifyUserMiddleware = async (req, res, next) => {
    try {
        const { user } = req;

        const { type, phone_number, email } = req.body;
        if (type && type.toUpperCase() === 'PHONE') {
            if (!user.phoneNumber && !phone_number) {
                res.error(
                    responseCode.NOT_FOUND.name,
                    req.__("user.phone_not_found"),
                    responseCode.NOT_FOUND.code
                );
                return;
            }
        }

        if (type && type.toUpperCase() === 'EMAIL' && email) {
            const userByEmail = await userRepo.findOne({ email: email, _id: { $ne: ObjectId(user._id) } })
            if (userByEmail) {
                res.error(
                    responseCode.BAD_REQUEST.name,
                    req.__("user.email_already_used"),
                    responseCode.BAD_REQUEST.code
                );
                return;
            }
        }
        next();
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
};
