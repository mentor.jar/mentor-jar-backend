import responseCode from "../../base/responseCode";
import { DataCityRepo } from "../../repositories/DataCityRepo";
import { SellerSignUpRepo } from "../../repositories/SellerSignUpRepo";
import { UserRepo } from "../../repositories/UserRepo";
import { cloneObj, ObjectId, phoneNormalize } from "../../utils/helper";

const { body, validationResult } = require("express-validator");
const dataCityRepo = DataCityRepo.getInstance()
const repositories = SellerSignUpRepo.getInstance();
const userRepo = UserRepo.getInstance();

export const signUpSellerCondition = [
    body("personal_info")
        .exists().withMessage((value, { req }) => req.__('seller.missing_personal_info')),
    body("personal_info.full_name")
        .exists().withMessage((value, { req }) => req.__('seller.missing_full_name')),
    body("personal_info.phone_number")
        .exists().withMessage((value, { req }) => req.__('seller.missing_phone_number'))
        .isMobilePhone().withMessage((value, { req }) => req.__('seller.invalid_phone_value')),
    body("personal_info.email")
        .exists().withMessage((value, { req }) => req.__('seller.missing_email')),
    body("personal_info.card_number")
        .exists().withMessage((value, { req }) => req.__('seller.missing_card_number')),
    body("personal_info.tax_code").optional(),
    body("personal_info.country")
        .exists().withMessage((value, { req }) => req.__('seller.missing_country'))
        .isIn(['VN', 'KO']).withMessage((value, { req }) => req.__('seller.invalid_country')),
    body("shop_info")
        .exists().withMessage((value, { req }) => req.__('seller.missing_shop_info')),
    body("shop_info.shop_name")
        .exists().withMessage((value, { req }) => req.__('seller.missing_shop_name')),
    body("shop_info.is_has_business")
        .exists().withMessage((value, { req }) => req.__('seller.missing_is_has_business'))
        .isBoolean().withMessage((value, { req }) => req.__('seller.invalid_business_value')),
    body("shop_info.description")
        .exists().withMessage((value, { req }) => req.__('seller.missing_description'))
        .trim().isLength({min: 100, max: 500}).withMessage((value, { req }) => req.__('seller.invalid_description_length')),
    body("shop_info.categories")
        .exists().withMessage((value, { req }) => req.__('seller.missing_shop_categories'))
        .isArray({min: 1}).withMessage((value, { req }) => req.__('seller.invalid_categories_value')),
    body("shop_info.address").optional(),
    body("shop_info.social_link_info").optional(),
    body("shop_info.social_business")
        .exists().withMessage((value, { req }) => req.__('seller.missing_social_business'))
        .isArray({min: 1}).withMessage()
]

export const registerSellerMiddleware = async (req, res, next) => {
    const error = validationResult(req).formatWith(({ msg }) => msg);

    const hasError = !error.isEmpty();

    if (hasError) {
        res.error('VALIDATION_ERROR', error.array()[0], 422)
        return;
    } else {
        next()
    }
}

export const validateAddress = async (req, res, next) => {
    try {
        if(req.body?.shop_info?.address) {
            const { state_id, district_id, ward_id, street } = req.body?.shop_info?.address
            const stateQuery = await dataCityRepo.findOne({Id: state_id})
            const state = cloneObj(stateQuery)
            let isValidAddress = true
            if(!state) {
                isValidAddress = false
            }
            else {
                const district = state.Districts.find(item => item.Id === district_id)
                if(!district) {
                    isValidAddress = false
                }
                else {
                    const ward = district.Wards.find(item => item.Id === ward_id)
                    if(!ward) {
                        isValidAddress = false
                    }
                    else {
                        req.body.shop_info.address = { 
                            state: {
                                name: state.Name,
                                Id: state.Id
                            }, 
                            district: {
                                name: district.Name,
                                Id: district.Id
                            }, 
                            ward,
                            street: street || ''
                        }
                        next()
                        return;
                    }
                }
            }

            if(!isValidAddress) {
                res.error(responseCode.BAD_REQUEST.name, req.__("seller.address_not_valid"), responseCode.BAD_REQUEST.code)
                return;
            }
        }
        else {
            next()
        }
    } catch (error) {
        res.error(responseCode.BAD_REQUEST.name, req.__("seller.address_not_valid"), responseCode.BAD_REQUEST.code)
        return;
    }
}

export const validateExistPhoneAndEmail = async (req, res, next) => {
    try {
        const { phone_number, email } = req.body.personal_info
        const phoneNumber= phoneNormalize(phone_number)
        const register = await repositories.findOne({
            $and: [
                {
                    $or: [
                        {"personal_info.phone_number": phoneNumber},
                        {"personal_info.email": email}
                    ],
                },
                {
                    $or: [
                        {"status": 'pending'},
                        {"status": 'approved'}
                    ],
                }
            ]
        })
        if(register) {
            res.error(responseCode.BAD_REQUEST.name, req.__("seller.already_exist_phone_or_email"), responseCode.BAD_REQUEST.code)
            return;
        }
        next()
    } catch (error) {
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
        return;
    }
}

// admin
export const findRegisterSellerMiddleware = async (req, res, next) => {
    try {
        const { id } = req.params
        const register:any = await repositories.findOne({_id: ObjectId(id)})
        if(!register) {
            res.error(responseCode.NOT_FOUND.name, req.__("seller.register_not_found"), responseCode.NOT_FOUND.code)
            return;
        }

    	next()
        
    } catch (error) {
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
        return;
    }
    
}

export const validateBeforeHandle = async (req, res, next) => {
    try {
        
        const { user_id } = req.body
        const { id } = req.params
        const register:any = await repositories.findOne({_id: ObjectId(id)})
        if(!register) {
            res.error(responseCode.NOT_FOUND.name, req.__("seller.register_not_found"), responseCode.NOT_FOUND.code)
            return;
        }
        if(register.status !== 'pending') {
            res.error(responseCode.BAD_REQUEST.name, req.__("seller.already_handled"), responseCode.BAD_REQUEST.code)
            return;
        }
        if(!user_id) {
            res.error(responseCode.BAD_REQUEST.name, req.__("seller.user_not_found"), responseCode.BAD_REQUEST.code)
            return;
        }

        const userMapped = register.user_mapped.find(id => id.toString() === user_id)
        
        if(!userMapped) {
            res.error(responseCode.BAD_REQUEST.name, req.__("seller.user_not_found"), responseCode.BAD_REQUEST.code)
            return;
        }

        const user:any = await userRepo.findOne({_id: ObjectId(user_id)})
        if(!user) {
            res.error(responseCode.BAD_REQUEST.name, req.__("seller.user_not_found"), responseCode.BAD_REQUEST.code)
            return;
        }
        req.body.user = user
        req.register = register
        
        
        next()
         
    } catch (error) {
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
        return;
    }
}

export const validateBankInfoMiddleware = async (req, res, next) => {
    try {
        const { bank_info } = req.body
        if(!bank_info || !bank_info?.bank_name || !bank_info?.bank_number) {
            res.error(responseCode.BAD_REQUEST.name, req.__("seller.missing_bank_info"), responseCode.BAD_REQUEST.code)
            return;
        }
        next()
    } catch (error) {
        console.log(error)
        return;
    }
}