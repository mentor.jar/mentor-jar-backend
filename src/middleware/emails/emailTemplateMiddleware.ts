import responseCode from "../../base/responseCode";
import { EmailTemplateRepo } from "../../repositories/EmailTemplateRepo";
import { ObjectId } from "../../utils/helper";

const { body, validationResult } = require("express-validator");

const emailTemplateRepo = EmailTemplateRepo.getInstance();

export const emailTemplateConditions = [
    body('name')
        .exists().withMessage((value, { req }) => req.__('email_template.missing_name')),
    body('sendgrid_template_id')
        .exists().withMessage((value, { req }) => req.__('email_template.missing_sendgrid_template_id')),
    body('content')
        .optional(),
    body('variables')
        .optional()
        .isArray({min: 1}).withMessage((value, { req }) => req.__('email_template.invalid_variables')),
    body('variables.*.name')
        .exists().withMessage((value, { req }) => req.__('email_template.missing_variable_name')),
    body('variables.*.sign')
        .exists().withMessage((value, { req }) => req.__('email_template.missing_variable_sign')),
    body('variables.*.type')
        .exists().withMessage((value, { req }) => req.__('email_template.missing_variable_type')),
]

export const emailTemplateMiddleware = async (req, res, next) => {
    const error = validationResult(req).formatWith(({ msg }) => msg);

    const hasError = !error.isEmpty();

    if (hasError) {
        res.error('VALIDATION_ERROR', error.array()[0], 422)
        return;
    } else {
        next()
    }
}

export const findEmailTemplateMiddleware = async (req, res, next) => {
    try {
        const { id } = req.params
        const template = await emailTemplateRepo.findOne({_id: ObjectId(id)})
        if(!template) {
            res.error(responseCode.NOT_FOUND.name, req.__("email_template.not_found"), responseCode.NOT_FOUND.code)
            return;
        }
        req.templateItem = template
        next()
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
        return;
    }
}