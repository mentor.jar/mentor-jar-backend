import responseCode from "../../base/responseCode";
import { EmailListRepo } from "../../repositories/EmailListRepo";
import { EmailTemplateRepo } from "../../repositories/EmailTemplateRepo";
import { ObjectId } from "../../utils/helper";

const { body, validationResult } = require("express-validator");
const emailTemplateRepo = EmailTemplateRepo.getInstance();
const emailListRepo = EmailListRepo.getInstance();

export const sendEmailConditions = [
    body('from')
        .exists().withMessage('Vui lòng nhập địa chỉ email người gửi')
        .isEmail().withMessage('Địa chỉ email không hợp lệ'),
    body('to')
        .exists().withMessage('Vui lòng chọn đối tượng gửi')
        .isIn(['seller', 'buyer', 'all']).withMessage('Đối tượng gửi không hợp lệ'),
    body('sendgrid_template_id')
        .exists().withMessage('Vui lòng chọn Sendgrid Template ID'),
    body('dynamic_template_data')
        .optional()
]

export const emailListMiddleware = async (req, res, next) => {
    const error = validationResult(req).formatWith(({ msg }) => msg);

    const hasError = !error.isEmpty();

    if (hasError) {
        res.error('VALIDATION_ERROR', error.array()[0], 422)
        return;
    } else {
        next()
    }
}

export const findEmailListMiddleware = async (req, res, next) => {
    try {
        const { id } = req.params
        const email = await emailListRepo.findOne({_id: ObjectId(id)})
        if(!email) {
            res.error(responseCode.NOT_FOUND.name, 'Không tìm thấy email', responseCode.NOT_FOUND.code)
            return;
        }
        req.emailItem = email
        next()
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
        return;
    }
}

export const validateBeforeSendEmail = async (req, res, next) => {
    try {
        const { sendgrid_template_id, dynamic_template_data } = req.body
        const template:any = await emailTemplateRepo.findOne({ sendgrid_template_id: sendgrid_template_id })
        // console.log("template: ", template);
        
        if(!template) {
            res.error(responseCode.NOT_FOUND.name, 'Template không hợp lệ', responseCode.NOT_FOUND.code)
            return;
        }
        if(template.variables && template.variables.length) {
            let isValidDynamicData = true
            if(!dynamic_template_data) {
                res.error(responseCode.NOT_FOUND.name, 'Danh sách giá trị tuỳ biến không hợp lệ', responseCode.NOT_FOUND.code)
                return;
            }
            template.variables.forEach(item => {
                if(!dynamic_template_data[item.sign] || (item.type === 'list' && !Array.isArray(dynamic_template_data[item.sign]))) {
                    console.log("item.sign: ", item.sign);
                    
                    isValidDynamicData = false
                }
            })
            console.log("isValidDynamicData: ", isValidDynamicData)
            if(!isValidDynamicData) {
                res.error(responseCode.NOT_FOUND.name, 'Danh sách giá trị tuỳ biến không hợp lệ', responseCode.NOT_FOUND.code)
                return;
            }
        }
        req.template = template
        next();
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
    }
}