const jwt = require('jsonwebtoken');
import { AuthencationError } from '../../base/customError';
import { randomId } from '../../utils/stringUtil';
import keys from '../../config/env/keys';
import { InMemoryAuthUserStore } from '../../SocketStores/AuthStore';
import { UserRepo } from '../../repositories/UserRepo';
import { ObjectId } from '../../utils/helper';
const authStore = InMemoryAuthUserStore.getInstance();
const userRepo = UserRepo.getInstance();
export const authChat = async (socket, next) => {
    try {
        const auth_jwt =
            socket.handshake?.auth?.token || socket.handshake?.query?.token;
        if (!auth_jwt) {
            return next(new AuthencationError('Không có quyền truy cập'));
        }
        const token = auth_jwt.trim();
        const data = jwt.verify(token, keys.jwtSecret);
        if (!data || !data.id) {
            return next(new AuthencationError('Không có quyền truy cập'));
        }
        let user = await authStore.getUser(data.id);
        if (!user) {
            user = await userRepo.getUserById(ObjectId(data.id));
            if (!user)
                return next(new AuthencationError('Không có quyền truy cập'));
            await authStore.saveUser(user);
        }
        socket.user = user;
        return next();
    } catch (error) {
        console.log(error);
        return next(new AuthencationError('Không có quyền truy cập'));
    }
};
