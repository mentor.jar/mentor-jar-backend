import { executeError } from "../../base/appError";
import responseCode from "../../base/responseCode";
import { OrderRepository } from "../../repositories/OrderRepo";
import mongoose from 'mongoose';

export const findOrderMiddleware = async (req, res, next) => {
    try {
        const orderId = req.body.order_id;
        // let isValid = mongoose.Types.ObjectId.isValid(orderId);
        // if (!isValid) {
        //     res.error(responseCode.NOT_FOUND.name, req.__("payment.invalid_id"), responseCode.NOT_FOUND.code);
        //     return;
        // }
        let order: any = await OrderRepository.getInstance().findOne({ _id: orderId });
        if (!order) {
            res.error(responseCode.NOT_FOUND.name, req.__("payment.order_not_found"), responseCode.NOT_FOUND.code);
            return;
        }
        next();
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}