import { body, validationResult } from "express-validator";
import { executeError } from "../../base/appError";
import responseCode from "../../base/responseCode";
import { FeedbackRepo } from "../../repositories/FeedbackRepo";
import { OrderItemRepository } from "../../repositories/OrderItemRepo";
import { OrderRepository } from "../../repositories/OrderRepo";
import { ProductRepo } from "../../repositories/ProductRepo";
import { ObjectId } from "../../utils/helper";
import _ from "lodash";
import { REQ_DATE_TIME_FORMAT } from "../../base/variable";
import moment from "moment";
import { OrderShippingRepo } from "../../repositories/OrderShippingRepo";
import { ShippingStatus } from "../../models/enums/order";

const orderRepo = OrderRepository.getInstance();
const orderItemRepo = OrderItemRepository.getInstance();
const feedbackRepo = FeedbackRepo.getInstance();
const productRepo = ProductRepo.getInstance();
const orderShippingRepo = OrderShippingRepo.getInstance();

export const feedbackCondition = [
    body("vote_star")
        .exists().withMessage((value, {req}) => req.__('feedback.missing_vote_star'))
        .isInt({ min: 1, max: 5 }).withMessage("Số sao đánh giá cần là một số nguyên, lớn hơn hoặc bằng 1 và nhỏ hơn hoặc bằng 5"),
    body("order_id")
        .exists().withMessage((value, {req}) => req.__('feedback.missing_order'))
]
export const filterMiddleware = async (req, res, next) => {
    try {
        if (!req.query.option || !req.query.value) {
            res.error(responseCode.MISSING.name, req.__("feedback.missing_option"), responseCode.MISSING.code);
            return;
        }
        else if (req.query.option.toUpperCase() === 'TYPE') {
            const valueForType = ['ALL', 'COMMENT', 'MEDIA']
            if (!valueForType.includes(req.query.value.toUpperCase())) {
                res.error(responseCode.BAD_REQUEST.name, req.__("feedback.invalid_value"), responseCode.BAD_REQUEST.code);
                return;
            }
            req.query.value = req.query.value.toUpperCase()
        }
        else if (req.query.option.toUpperCase() === 'STAR') {
            if (req.query.value < 1 || req.query.value > 5) {
                res.error(responseCode.BAD_REQUEST.name, req.__("feedback.invalid_star"), responseCode.BAD_REQUEST.code);
                return;
            }
            req.query.value = parseInt(req.query.value)
        }
        else {
            res.error(responseCode.BAD_REQUEST.name, req.__("feedback.invalid_type"), responseCode.BAD_REQUEST.code);
            return;
        }
        req.query.option = req.query.option.toUpperCase()
        next();
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}

export const checkTimeFeedback = async (req, res, next) => {
    try {
        const orderID = req.body.order_id
        const order: any = await orderRepo.findOne({ _id: ObjectId(orderID) })
        if (order) {
            const orderShipping: any = await orderShippingRepo.findOne({ order_id: ObjectId(orderID) })

            if (!orderShipping) {
                res.error(responseCode.BAD_REQUEST.name, req.__("feedback.order_not_found"), responseCode.BAD_REQUEST.code);
                return;
            }
            const { history } = orderShipping

            if(order.shipping_status !== ShippingStatus.SHIPPED || !history || !Array.isArray(history)) {
                res.error(responseCode.BAD_REQUEST.name, req.__("feedback.invalid_order_to_feedback"), responseCode.BAD_REQUEST.code);
                return;
            }
            const shippedItem = history.find(item => item.status_id === 5)

            if(!shippedItem) {
                res.error(responseCode.BAD_REQUEST.name, req.__("feedback.invalid_order_to_feedback"), responseCode.BAD_REQUEST.code);
                return;
            }
            const lastTimeToFeedback = moment(shippedItem.action_time, REQ_DATE_TIME_FORMAT, true).add(15, 'days')

            const currentTime = moment(new Date(), REQ_DATE_TIME_FORMAT, true)

            if (lastTimeToFeedback.isBefore(currentTime)) {
                res.error(responseCode.BAD_REQUEST.name, req.__("feedback.expire_time"), responseCode.BAD_REQUEST.code);
                return;
            }
            next()
        }
        else {
            res.error(responseCode.BAD_REQUEST.name, req.__("feedback.order_not_found"), responseCode.BAD_REQUEST.code);
            return;
        }
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
        return;
    }
}

export const checkProductBeforeFeedback = async (req, res, next) => {
    try {
        const error = validationResult(req).formatWith(({ msg }) => msg);

        const hasError = !error.isEmpty();

        if (hasError) {
            res.error('VALIDATION_ERROR', error.array()[0], 422)
            return;
        }

        if (!req.body.product_id) {
            res.error(responseCode.BAD_REQUEST.name, req.__("feedback.buy_order"), responseCode.BAD_REQUEST.code);
            return;
        }

        const userID = req.user._id.toString()

        const productID = req.body.product_id
        const orderID = req.body.order_id
        const validStatus = ['SHIPPED', 'CANCELED', 'RETURNED']
        const product = await productRepo.findOne({ _id: ObjectId(productID) })
        if (!product) {
            res.error(responseCode.BAD_REQUEST.name, req.__("feedback.product_not_found"), responseCode.BAD_REQUEST.code);
            return;
        }
        req.product = product

        const feedback = await feedbackRepo.findOne({ user_id: ObjectId(userID), target_id: ObjectId(productID), order_id: ObjectId(orderID) })
        if (feedback) {
            res.error(responseCode.BAD_REQUEST.name, req.__("feedback.already_feedbacked"), responseCode.BAD_REQUEST.code);
            return;
        }
        const order: any = await orderRepo.findOne({ _id: ObjectId(orderID) })
        const orderItem: any = await orderItemRepo.findOne({
            order_id: ObjectId(orderID),
            product_id: ObjectId(productID)
        })
        if (order && orderItem) {
            const isValid = validStatus.includes(order.shipping_status.toUpperCase())
            if (!isValid) {
                res.error(responseCode.NOT_FOUND.name, req.__("feedback.order_not_completed"), responseCode.NOT_FOUND.code);
                return;
            }
        }
        else {
            res.error(responseCode.NOT_FOUND.name, req.__("feedback.not_found_or_not_buy"), responseCode.NOT_FOUND.code);
            return;
        }
        next()
    } catch (error) {
        console.log(error)
        res.error(responseCode.BAD_REQUEST.name, req.__("feedback.invalid_request"), responseCode.BAD_REQUEST.code);
        return;
    }
}

export const checkOrderBeforeFeedback = async (req, res, next) => {
    try {
        const error = validationResult(req).formatWith(({ msg }) => msg);

        const hasError = !error.isEmpty();

        if (hasError) {
            res.error('VALIDATION_ERROR', error.array()[0], 422)
            return;
        }

        const userID = req.user._id.toString()
        const orderID = req.body.order_id
        const validStatus = ['SHIPPED', 'CANCELED', 'RETURNED']
        const order: any = await orderRepo.findOne({ _id: ObjectId(orderID) })

        if (order) {
            req.order = order;
            const isValid = validStatus.includes(order.shipping_status.toUpperCase())
            if (!isValid) {
                res.error(responseCode.NOT_FOUND.name, req.__("feedback.feedback_user_need_order_completed"), responseCode.NOT_FOUND.code);
                return;
            }

            if (order.shop_id.toString() !== req.shop_id.toString()) {
                res.error(responseCode.NOT_FOUND.name, req.__("feedback.feedback_user_need_owner_order"), responseCode.NOT_FOUND.code);
                return;
            }
        }

        const feedback = await feedbackRepo.findOne({ user_id: ObjectId(userID), target_id: order.user_id, order_id: ObjectId(order._id) })
        if (feedback) {
            res.error(responseCode.BAD_REQUEST.name, req.__("feedback.already_feedbacked_user"), responseCode.BAD_REQUEST.code);
            return;
        }
        next()

    } catch (error) {
        console.log(error)
        res.error(responseCode.BAD_REQUEST.name, req.__("feedback.invalid_request"), responseCode.BAD_REQUEST.code);
        return;
    }
}

export const findFeedbackMiddleware = async (req, res, next) => {
    const { id } = req.params
    const { shop_id } = req
    let { content, medias } = req.body
    let feedback: any = await feedbackRepo.findOne({ _id: ObjectId(id), target_type: "Product" })
    if (!content || !content.trim().length) {
        res.error(responseCode.BAD_REQUEST.name, req.__("feedback.missing_content"), responseCode.BAD_REQUEST.code);
        return;
    }
    if (!feedback) {
        res.error(responseCode.BAD_REQUEST.name, req.__("feedback.not_found_to_update"), responseCode.BAD_REQUEST.code);
        return;
    }

    if (feedback.shop_id.toString() !== shop_id.toString()) {
        res.error(responseCode.BAD_REQUEST.name, req.__("feedback.reply_feedback_need_owner"), responseCode.BAD_REQUEST.code);
        return;
    }
    if (!_.isEmpty(feedback.shop_feedback)) {
        res.error(responseCode.BAD_REQUEST.name, req.__("feedback.already_reply_feedback"), responseCode.BAD_REQUEST.code);
        return;
    }

    req.feedback = feedback
    req.content = content
    req.medias = medias || null;
    next()
}

export const findFeedbackMiddlewareToUpdate = async (req, res, next) => {
    const { id } = req.params;
    let { content } = req.body;
    const user_id = req.user._id.toString();
    let feedback: any = await feedbackRepo.findOne({ _id: ObjectId(id), target_type: "Product" });
    if (!content || !content.trim().length) {
        res.error(responseCode.BAD_REQUEST.name, req.__("feedback.missing_content"), responseCode.BAD_REQUEST.code);
        return;
    }
    if (!feedback) {
        res.error(responseCode.BAD_REQUEST.name, req.__("feedback.not_found_to_update"), responseCode.BAD_REQUEST.code);
        return;
    }

    if(feedback.user_id.toString() !== user_id) {
        res.error(responseCode.BAD_REQUEST.name, req.__("feedback.invalid_request"), responseCode.BAD_REQUEST.code);
        return;
    }

    next();
}

export const findUserReplyFeedbackMiddleware = async (req, res, next) => {
    const { id } = req.params
    let { content } = req.body
    let feedback: any = await feedbackRepo.findOne({ _id: ObjectId(id), target_type: "User" })

    if (!content || !content.trim().length) {
        res.error(responseCode.BAD_REQUEST.name, req.__("feedback.missing_content"), responseCode.BAD_REQUEST.code);
        return;
    }
    if (!feedback) {
        res.error(responseCode.BAD_REQUEST.name, req.__("feedback.not_found_to_update"), responseCode.BAD_REQUEST.code);
        return;
    }

    if (feedback.target_id.toString() !== req.user._id.toString()) {
        res.error(responseCode.BAD_REQUEST.name, req.__("feedback.have_not_permission"), responseCode.BAD_REQUEST.code);
        return;
    }
    if (!_.isEmpty(feedback.buyer_feedback)) {
        res.error(responseCode.BAD_REQUEST.name, req.__("feedback.already_reply_feedback"), responseCode.BAD_REQUEST.code);
        return;
    }

    req.feedback = feedback
    req.content = content
    next()
}