
import * as variables from '../base/variable'
import path from 'path'

const multer = require("multer");

const validFileType = ['.png', '.jpg', '.gif', '.jpeg', '.svg', '.pdf']

const getPathToSave = (urlReq) => {
    switch (urlReq) {
        case variables.PRODUCT_IMAGE_PATH:
            return variables.PRODUCT_IMAGE_STORAGE
        default:
            return 'uploads/';
    }
}

const filter = function (req, file, callback) {
    var ext = path.extname(file.originalname.toLowerCase())
    if (!validFileType.includes(ext)) {
        callback(new Error(`${req.__("media.file_type")} ${validFileType.join(" | ")}`), false)
    }

    callback(null, true)
}

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, getPathToSave(req.originalUrl));
    },
    filename: (req, file, cb) => {
        const ext = path.extname(file.originalname.toLowerCase())
        cb(null, `${Date.now()}_image_${Math.floor(Math.random() * 1_000_000)}${ext}`);
    },
});
const uploadFile = multer({
    storage: storage,
    fileFilter: filter
})

export default uploadFile
