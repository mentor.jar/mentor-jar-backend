import responseCode from "./responseCode";
import { NotFoundError, ValidationError, SyntaxError, QueryDBError } from "./customError";

export class AppError extends Error {
    protected statusCode: Number;
    constructor(message, statusCode = 500) {
        super(message);
        this.statusCode = statusCode;
    }
    logger() {
        //log error
    }
    sendMail() {
        //send email
    }
    executeAll() {
        this.logger();
        this.sendMail();
    }

}

export const executeError = (error, isExec = false) => {
    let resError: any = null;
    if (error instanceof NotFoundError) {
        resError = error
    }
    if (error instanceof ValidationError) {

        resError = error
    }
    if (error instanceof SyntaxError) {
        resError = error
    }
    if (error instanceof QueryDBError) {
        resError = error
    }
    if (!resError) {
        resError = new AppError(error.message, responseCode.SERVER.code);
    }

    if (typeof resError['executeAll'] === 'function' && isExec) {
        resError.executeAll();
    }
    return resError;
}