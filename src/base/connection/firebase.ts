import firebase from 'firebase-admin'
import keys from '../../config/env/keys';

const serviceAccount = keys.firebase_sdk_credential
const firebaseUrl = keys.firebase_db_url

const credential = {
  type: serviceAccount.type,
  projectId: serviceAccount.project_id,
  privateKeyId: serviceAccount.private_key_id,
  privateKey: serviceAccount.private_key,
  clientEmail: serviceAccount.client_email,
  clientId: serviceAccount.client_id,
  authUri: serviceAccount.auth_uri,
  tokenUri: serviceAccount.token_uri,
  authProviderX509CertUrl: serviceAccount.auth_provider_x509_cert_url,
  clientC509CertUrl: serviceAccount.client_x509_cert_url,
}

firebase.initializeApp({
  credential: firebase.credential.cert(credential),
  databaseURL: firebaseUrl
})

const firestore = firebase.firestore();
firestore.settings({ ignoreUndefinedProperties: true });

export { firestore };
