import keys from "../../config/env/keys";
import { ElasticClient } from "../../services/elastic";

export const connectElasticSearch = () => {
    console.log('Elasticsearch: ' + keys.elsUrl)
    ElasticClient.connect({
        node: keys.elsUrl,
        auth: {
            username: 'elastic',
            password: keys.elsPassword
        },
    })
}