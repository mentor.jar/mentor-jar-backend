import keys from "../config/env/keys"

// API version
const URL_BASE = process.env.HOST || 'http://103.92.29.93'
//const URL_RTMP = process.env.HOST || 'rtmp://103.92.29.93'
const URL_RTMP = process.env.HOST || 'rtmp://103.69.193.161'
export const API_VERSION = 'v1'

// max images for product upload
export const MAX_PRODUCT_IMAGE = 15
export const MAX_SIZE = 10 * 1024 * 1024
export const MAX_SIZE_VIDEO = 100 * 1024 * 1024

// sendgrid templateId
export const SENDGRID_TEMPLATE_ID = 'd-5c95b204a11f4cc792a871bb44db3e8b'

// button URL seller register
export const URL_APPROVE_SELLER_PRODUCTION = 'https://admin-commerce.bidu.com.vn/registration/'
export const URL_APPROVE_SELLER_STAGING = 'http://103.92.29.93:4000/registration/'

// Storage for upload image
export const PRODUCT_IMAGE_STORAGE = 'uploads/images/products/'
export const CATEGORY_IMAGE_STORAGE = 'uploads/images/categories/'

// API Path for upload image
export const PRODUCT_IMAGE_PATH = `/api/${API_VERSION}/image/products`
export const CATEGORY_IMAGE_PATH = `/api/${API_VERSION}/image/categories`

// API Livestream
export const URL_STREAMFILE = `${URL_BASE}:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/applications/live/streamfiles`
export const URI_CREATE_STREAMFILE = `rtmp://103.92.29.93`
//export const URL_SERVER = `${URL_BASE}:1935/live/`
export const URL_SERVER = `http://103.69.193.161:1935/live/`
export const URL_SERVER_RTMP = `${URL_RTMP}:1935/live/`
//export const URL_INCOMING = `${URL_BASE}:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/applications/live/instances/_definst_/incomingstreams/`
export const URL_INCOMING = `http://103.69.193.161:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/applications/live/instances/_definst_/incomingstreams/`


// Date and Time
export const REQ_DATE_TIME_FORMAT = 'YYYY-MM-DD HH:mm'

// URL Comunity
export const URL_COMMUNITY = 'http://bidu.tech/'
export const API_COMMUNITY_PROD = 'https://api.bidu.com.vn'
export const API_COMMUNITY_STAG = 'https://api-staging.bidu.com.vn'
export const API_KEY_COMMUNITY = 'fashionapp.$2a$10$4Z1a2mIuS/rv6CqjGj9TSeuax2yT0s03IiX5hVAt9JrIKdkVuk8eq'

// Chat response rate
export const CHAT_RESPONSE_RATE = 95

// 14 days
export const NEXT_14_DAYS = 12096e5;

export const DEFAULT_POPULAR_SEARCH = [
	'Thời trang nam nữ',
	'Mỹ phẩm Hàn Quốc',
	'Điện gia dụng',
	'Hàng tiêu dùng nội địa',
	'Điện thoại di động'
]

export const REDIS_PORT = 6379
export const REDIS_HOST = '103.69.193.161'

export const VNPAY_LOCALE = 'vn'
export const VNPAY_CURRCODE = 'VND'
export const VNPAY_VERSION = '2'
export const VNPAY_COMMAND = 'pay'

export const VERIFY_ACCOUNT_EMAIL_TEMPLATE = 'd-677a3df8ecf04cd3ae7498c69cc3f1fd'

export const RELATIVE_SEARCH_FIELD = [
	'order_number'
]

// Need change if JSON language file change
export const SUGGEST_CHAT_BUYER = [
	"suggest_chat.buyer.ask_product",
	"suggest_chat.buyer.ask_shipping_status",
	"suggest_chat.buyer.ask_for_help",
	"suggest_chat.buyer.thanks",
	"suggest_chat.buyer.want_to_cancel_order",
	"suggest_chat.buyer.good_rate",
	"suggest_chat.buyer.bad_rate",
]

export const SUGGEST_CHAT_SELLER = [
	"suggest_chat.seller.welcome",
	"suggest_chat.seller.out_stock",
	"suggest_chat.seller.ask_receive_product",
	"suggest_chat.seller.thanks",
	"suggest_chat.seller.ask_for_request",
	"suggest_chat.seller.assign_to_shipping_delivery",
	"suggest_chat.seller.good_rate"
]

// Country
export const VIET_NAM = "VN"
export const KOREA = "KO"
export const VN_LANG = "vi"
export const EN_LANG = "en"
export const KO_LANG = "ko"

export const REFUND_CONDITIONS = [
	{
		code: 1,
		vn_name: "Sai kích thước/màu sắc",
		ko_name: "사이즈 또는 색상이 잘못됨",
		en_name: "Incorrect size or color"
	},
	{
		code: 2,
		vn_name: "Hàng không giống hình",
		ko_name: "상품 묘사와 다르거나 모조품인 경우",
		en_name: "Counterfeit/product description was not accurate"
	},
	{
		code: 3,
		vn_name: "Hàng kém chất lượng",
		ko_name: "상품의 품질이 낮음",
		en_name: "Low quality"
	},
	{
		code: 4,
		vn_name: "Hàng vỡ, hư hỏng, không nguyên vẹn",
		ko_name: "상품이 훼손되었거나 하자가 있을 경우",
		en_name: "Broken, damaged product"
	},
	{
		code: 5,
		vn_name: "Người mua không hài lòng với đơn hàng",
		ko_name: "상품에 만족하지 않음",
		en_name: "Dissatisfaction"
	}
]


export const REFUND_REJECT_CONDITIONS = [
	{
		code: 1,
		vn_name: "Tôi đã giao đúng hàng",
		ko_name: "올바른 상품 배송 완료",
		en_name: "I delivered the correct item"
	},
	{
		code: 2,
		vn_name: "Sản phẩm không bị lỗi",
		ko_name: "문제없는 상품",
		en_name: "Product is not defective"
	},
	{
		code: 3,
		vn_name: "Cửa hàng không hỗ trợ hoàn trả",
		ko_name: "반품 서비스 지원 안함",
		en_name: "The shop does not support returns"
	},
]


export const PICK_ORDER_KO_STATUS = [
	{
		code: 1,
		vn_name: "Đơn hàng đang được chuẩn bị",
		ko_name: "주문 상품 준비중",
		en_name: "The order is being prepared"
	},
	{
		code: 2,
		vn_name: "Đơn hàng đang vận chuyển về Việt Nam",
		ko_name: "주문 상품 베트남으로 배송중",
		en_name: "The order is being shipped to Vietnam"
	},
	{
		code: 3,
		vn_name: "Đơn hàng đã đến Việt Nam",
		ko_name: "주문 상품 베트남 도착",
		en_name: "The order has arrived in Vietnam"
	},
	{
		code: 4,
		vn_name: "Đơn hàng đang được vận chuyển đến kho hàng (50 Bạch Đằng)",
		ko_name: "주문 상품 물류창고 (50 Bach Dang) 로/으로 운송중",
		en_name: "The order is being shipped to the warehouse (50 Bach Dang)"
	},
	{
		code: 5,
		vn_name: "Đơn hàng đã đến kho hàng (50 Bạch Đằng)",
		ko_name: "주문 상품 물류창고 (50 Bach Dang) 도착",
		en_name: "The order has arrived at the warehouse (50 Bach Dang)"
	}
]



export const REPORT_LIVESTREAM_REASONS = [
    {
        code: 1,
        vi: 'Bán hàng giả, hàng nhái',
        ko: '위조품, 가품 판매',
        en: 'Counterfeit goods',
        custom_reason: '',
    },
    {
        code: 2,
        vi: 'Vi phạm bản quyền về sản phẩm, bán hàng giả',
        ko: '상품 지적재산권 위반',
        en: 'Infringing product copyright',
        custom_reason: '',
    },
    {
        code: 3,
        vi: 'Sử dụng âm thanh bản quyền',
        ko: '저작권 음악 사용',
        en: 'Using copyrighted audio',
        custom_reason: '',
    },
    {
        code: 4,
        vi: 'Spam quá nhiều',
        ko: '다량의 광고성 스팸',
        en: 'Too much spam',
        custom_reason: '',
    },
    {
        code: 5,
        vi: 'Lời lẽ thiếu văn hoá, thô tục, phản cảm',
        ko: '욕설, 시비성 어조, 음란성 언어',
        en: 'Uncultured, vulgar, offensive words',
        custom_reason: '',
    },
    {
        code: 6,
        vi: 'Nội dung livestream không phù hợp',
        ko: '부적절한 라이브스트림 주제',
        en: 'Inappropriate livestream content',
        custom_reason: '',
    },
    {
        code: 7,
        vi: 'Ảnh bìa livestream phản cảm/không phù hợp',
        ko: '부적절한 라이브스트림 커버이미지',
        en: 'The livestream’s cover photo is inappropriate',
        custom_reason: '',
    },
    {
        code: 8,
        vi: 'Lý do khác',
        ko: '기타',
        en: 'Others',
        custom_reason: '',
    },
];

export const LIVESTREAM_OTHER_REASON = 8;



// Object base for get detail size
export const ARRAY_DETAIL_SIZE_FASHION_TYPE_1 = [
	{
		"name": "Tổng chiều dài",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"name": "Vai",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"name": "Ngực",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"name": "Dài tay",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"name": "Chiều cao",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"name": "Cân nặng",
		"min": null,
		"max": null,
		"unit": "kg"
	}
]

export const ARRAY_DETAIL_SIZE_FASHION_TYPE_2 = [
	{
		"name": "Tổng chiều dài",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"name": "Đùi",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"name": "Hông",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"name": "Eo",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"name": "Độ rộng ống quần",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"name": "Chiều cao",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"name": "Cân nặng",
		"min": null,
		"max": null,
		"unit": "kg"
	}
]

export const ARRAY_DETAIL_SIZE_FASHION_TYPE_3 = [
	{
		"name": "Chiều cao",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"name": "Cân nặng",
		"min": null,
		"max": null,
		"unit": "kg"
	}
]

export const ARRAY_DETAIL_SIZE_FASHION_TYPE_4 = [
	{
		"name": "Tổng chiều dài",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"name": "Vai",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"name": "Ngực",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"name": "Hông",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"name": "Eo",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"name": "Chiều cao",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"name": "Cân nặng",
		"min": null,
		"max": null,
		"unit": "kg"
	}
]

export const ARRAY_DETAIL_SIZE_FASHION_TYPE_5 = [
	{
		"name": "Tổng chiều dài",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"name": "Hông",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"name": "Eo",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"name": "Chiều cao",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"name": "Cân nặng",
		"min": null,
		"max": null,
		"unit": "kg"
	}
]

export const DETAIL_SIZE_NAME = "Chi tiết kích cỡ";

export const FASHION_TYPE_OTHER = "3";

export const ALLOW_REFUND_IN_THREE_DAYS = 3;

export const ALLOW_REFUND_IN_SEVEN_DAYS = 7;

export const STAGING_WEB_URL = "https://web-staging.bidu.com.vn"

export const PRODUCTION_WEB_URL = "https://bidu.com.vn"
export const PRODUCTION_WEB_URL_SHORTEN = "https://bidu.asia"

export const FEATURE_CATEGORY = [
	{
		vi_name: "Thương hiệu",
		en_name: "Brand",
		ko_name: "브랜드",
	},
	{
		vi_name: "Xuất xứ",
		en_name: "Origin",
		ko_name: "원산지",
	},
	{
		vi_name: "Chất liệu",
		en_name: "Material",
		ko_name: "재질",
	},
	{
		vi_name: "Chi tiết kích cỡ",
		en_name: "Size detail",
		ko_name: "상세 사이즈",
	},
	{
		vi_name: "Bảo hành",
		en_name: "Warranty",
		ko_name: "보증",
	},
	{
		vi_name: "Kiểu tay",
		en_name: "Sleeve style",
		ko_name: "소매 스타일",
	},
]

export const OPTION_IN_CATEGORY = [
	{
		vi_option: "Mỹ",
		en_option: "USA",
		ko_option: "미국",
	},
	{
		vi_option: "Nhật Bản",
		en_option: "Japan",
		ko_option: "일본",
	},
	{
		vi_option: "Việt Nam",
		en_option: "Vietnam",
		ko_option: "베트남",
	},
	{
		vi_option: "Ấn Độ",
		en_option: "India",
		ko_option: "인도",
	},
	{
		vi_option: "Trung Quốc",
		en_option: "China",
		ko_option: "중국",
	},
	{
		vi_option: "Thái Lan",
		en_option: "Thailand",
		ko_option: "태국",
	},
	{
		vi_option: "Hàn Quốc",
		en_option: "Korea",
		ko_option: "대한민국",
	},
	{
		vi_option: "Khác",
		en_option: "Other",
		ko_option: "기타",
	},
	{
		vi_option: "1 tháng",
		en_option: "1 month",
		ko_option: "1개월",
	},
	{
		vi_option: "2 tháng",
		en_option: "2 months",
		ko_option: "2개월",
	},
	{
		vi_option: "3 tháng",
		en_option: "3 months",
		ko_option: "3개월",
	},
	{
		vi_option: "Lụa",
		en_option: "Silk",
		ko_option: "실크",
	},
	{
		vi_option: "Len",
		en_option: "Wool",
		ko_option: "울",
	},
	{
		vi_option: "Cotton",
		en_option: "Cotton",
		ko_option: "면",
	},
	{
		vi_option: "Vải lanh",
		en_option: "Linen",
		ko_option: "리넨",
	},
	{
		vi_option: "Nhung",
		en_option: "Velvet",
		ko_option: "벨벳",
	},
	{
		vi_option: "Voan",
		en_option: "Chiffon",
		ko_option: "쉬폰",
	},
	{
		vi_option: "Da",
		en_option: "Leather",
		ko_option: "가죽",
	},
	{
		vi_option: "Polyester",
		en_option: "Polyester",
		ko_option: "폴리에스테르",
	},
	{
		vi_option: "Satin",
		en_option: "Satin",
		ko_option: "사틴",
	},
	{
		vi_option: "Spandex",
		en_option: "Spandex",
		ko_option: "스판",
	},
	{
		vi_option: "Ren",
		en_option: "Lace",
		ko_option: "레이스",
	},
	{
		vi_option: "Tay dài",
		en_option: "Long sleeves",
		ko_option: "긴 소매",
	},
	{
		vi_option: "Tay ngắn",
		en_option: "Short sleeves",
		ko_option: "짧은 소매",
	},
	{
		vi_option: "Không tay",
		en_option: "Sleeveless",
		ko_option: "민소매",
	},
]

export const OVERSEAS_SHIPPING_FEE_BY_GAM = 270;

export const COSTS_INCURRED = 20000;

export const DEFAULT_NAME_ADDRESS = "Bithumb Company";

/****  Variables for API version 2 ****/
// Object base for get detail size
export const ARRAY_DETAIL_SIZE_FASHION_TYPE_1_V2 = [
	{
		"vi_name": "Tổng chiều dài",
		"en_name": "Length",
		"ko_name": "총장",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"vi_name": "Vai",
		"en_name": "Vai",
		"ko_name": "어깨",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"vi_name": "Ngực",
		"en_name": "Ngực",
		"ko_name": "가슴",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"vi_name": "Dài tay",
		"en_name": "Sleeve length",
		"ko_name": "소매길이",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"vi_name": "Chiều cao",
		"en_name": "Chiều cao",
		"ko_name": "키",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"vi_name": "Cân nặng",
		"en_name": "Cân nặng",
		"ko_name": "몸무게",
		"min": null,
		"max": null,
		"unit": "kg"
	}
]

export const ARRAY_DETAIL_SIZE_FASHION_TYPE_2_V2 = [
	{
		"vi_name": "Tổng chiều dài",
		"en_name": "Length",
		"ko_name": "총장",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"vi_name": "Đùi",
		"en_name": "Đùi",
		"ko_name": "허벅지",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"vi_name": "Hông",
		"en_name": "Hông",
		"ko_name": "엉덩이",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"vi_name": "Eo",
		"en_name": "Eo",
		"ko_name": "허리",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"vi_name": "Độ rộng ống quần",
		"en_name": "Hem width",
		"ko_name": "밑단단면",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"vi_name": "Chiều cao",
		"en_name": "Chiều cao",
		"ko_name": "키",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"vi_name": "Cân nặng",
		"en_name": "Cân nặng",
		"ko_name": "몸무게",
		"min": null,
		"max": null,
		"unit": "kg"
	}
]

export const ARRAY_DETAIL_SIZE_FASHION_TYPE_3_V2 = [
	{
		"vi_name": "Chiều cao",
		"en_name": "Chiều cao",
		"ko_name": "키",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"vi_name": "Cân nặng",
		"en_name": "Cân nặng",
		"ko_name": "몸무게",
		"min": null,
		"max": null,
		"unit": "kg"
	}
]

export const ARRAY_DETAIL_SIZE_FASHION_TYPE_4_V2 = [
	{
		"vi_name": "Tổng chiều dài",
		"en_name": "Length",
		"ko_name": "총장",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"vi_name": "Vai",
		"en_name": "Vai",
		"ko_name": "어깨",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"vi_name": "Ngực",
		"en_name": "Ngực",
		"ko_name": "가슴",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"vi_name": "Hông",
		"en_name": "Hông",
		"ko_name": "엉덩이",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"vi_name": "Eo",
		"en_name": "Eo",
		"ko_name": "허리",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"vi_name": "Chiều cao",
		"en_name": "Chiều cao",
		"ko_name": "키",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"vi_name": "Cân nặng",
		"en_name": "Cân nặng",
		"ko_name": "몸무게",
		"min": null,
		"max": null,
		"unit": "kg"
	}
]

export const ARRAY_DETAIL_SIZE_FASHION_TYPE_5_V2 = [
	{
		"vi_name": "Tổng chiều dài",
		"en_name": "Length",
		"ko_name": "총장",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"vi_name": "Hông",
		"en_name": "Hông",
		"ko_name": "엉덩이",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"vi_name": "Eo",
		"en_name": "Eo",
		"ko_name": "허리",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"vi_name": "Chiều cao",
		"en_name": "Chiều cao",
		"ko_name": "키",
		"min": null,
		"max": null,
		"unit": "cm"
	},
	{
		"vi_name": "Cân nặng",
		"en_name": "Cân nặng",
		"ko_name": "몸무게",
		"min": null,
		"max": null,
		"unit": "kg"
	}
]

export const ARRAY_NAME_SIZE_INFOS = [
	{
		"vi_name": "Tổng chiều dài",
		"en_name": "Length",
		"ko_name": "총장",
	},
	{
		"vi_name": "Vai",
		"en_name": "Vai",
		"ko_name": "어깨",
	},
	{
		"vi_name": "Ngực",
		"en_name": "Ngực",
		"ko_name": "가슴",
	},
	{
		"vi_name": "Dài tay",
		"en_name": "Sleeve length",
		"ko_name": "소매길이",
	},
	{
		"vi_name": "Đùi",
		"en_name": "Đùi",
		"ko_name": "허벅지",
	},
	{
		"vi_name": "Hông",
		"en_name": "Hông",
		"ko_name": "엉덩이",
	},
	{
		"vi_name": "Eo",
		"en_name": "Eo",
		"ko_name": "허리",
	},
	{
		"vi_name": "Độ rộng ống quần",
		"en_name": "Hem width",
		"ko_name": "밑단단면",
	},
	{
		"vi_name": "Chiều cao",
		"en_name": "Chiều cao",
		"ko_name": "키",
	},
	{
		"vi_name": "Cân nặng",
		"en_name": "Cân nặng",
		"ko_name": "몸무게",
	}
]

export const SIZE_INFOS_HEIGHT_REQUIRE = {
    vi_name: 'Chiều cao',
    en_name: 'Chiều cao',
    ko_name: '키',
};

export const SIZE_INFOS_WEIGHT_REQUIRE = {
    vi_name: 'Cân nặng',
    en_name: 'Cân nặng',
    ko_name: '몸무게',
};

export const DEFAULT_KOREA_SHOP = {
	"name": "Bithumb Company",
	"state": {
		"id": "48",
		"name": "Thành phố Đà Nẵng",
		"id_vtp": 4
	},
	"district": {
		"id": "492",
		"name": "Quận Hải Châu",
		"id_vtp": 74
	},
	"ward": {
		"id": "20236",
		"name": "Phường Hải Châu I",
		"id_vtp": 1200
	},
	"is_pick_address_default": true,
	"is_return_address_default": true,
	"street": "50 Bạch Đằng",
	"phone": "0935590955"
}

export const CATEGORIES_LOCALIZED = new Map([
    ['thời trang', { en: 'Fashion', ko: '패션' }],
    ['thời trang nữ', { en: 'no translation yet', ko: '여성 패션' }],	
    ['thời trang tam', { en: 'no translation yet', ko: '남성 패션' }],
    ['áo', { en: 'Tops', ko: '상의' }],
    ['áo thun', { en: 'no translation yet', ko: '티셔츠' }],
    ['áo sơ mi', { en: 'no translation yet', ko: '셔츠' }],
    ['áo kiểu', { en: 'no translation yet', ko: '블라우스' }],
    ['áo hai dây & Áo ba lỗ', { en: 'no translation yet', ko: '나시 & 민소매' }],
    ['áo Croptop', { en: 'no translation yet', ko: '크롭탑' }],
    ['đầm', { en: 'no translation yet', ko: '원피스' }],
    ['trang phục đông', { en: 'no translation yet', ko: '겨울 의류' }],

    ['đồ đôi', { en: 'no translation yet', ko: '커플옷' }],
    ['thời trang nam', { en: 'no translation yet', ko: '남성 패션' }],
    ['áo khoác & áo vest', { en: 'no translation yet', ko: '자켓 & 베스트' }],
    ['đồ đôi', { en: 'no translation yet', ko: '커플옷' }],
    ['quần', { en: 'Trouser', ko: '하의' }],
    ['balo/ túi/ ví', { en: 'no translation yet', ko: '배낭 / 가방 / 지갑' }],
    ['mắt kính', { en: 'no translation yet', ko: '안경' }],
    ['kính cận', { en: 'no translation yet', ko: '안경' }],
    ['kính lão', { en: 'no translation yet', ko: '돋보기' }],
    ['kính mát', { en: 'no translation yet', ko: '자외선 차단 안경' }],
    ['kính thời trang', { en: 'no translation yet', ko: '패션선글라스' }],
    ['phụ kiện nam', { en: 'no translation yet', ko: '남성 악세사리' }],
    ['đồ trung niên', { en: 'no translation yet', ko: '중년 아이템' }],
    ['trang phục', { en: 'no translation yet', ko: '의류' }],
    ['trang sức nam', { en: 'no translation yet', ko: '글루밍 아이템' }],
    ['nhẫn kim loại/nhẫn đá', { en: 'no translation yet', ko: '금속류 반지' }],
    ['vòng tay/lắc tay', { en: 'no translation yet', ko: '팔찌' }],
    ['dây chuyền', { en: 'no translation yet', ko: '목걸이' }],
    ['bông tai/khuyên tai', { en: 'no translation yet', ko: '귀걸이' }],
    ['thắt lưng', { en: 'no translation yet', ko: '벨트' }],

    ['áo ngắn tay có cổ', { en: 'no translation yet', ko: '카라 반팔티' }],
    ['áo ngắn tay không cổ', { en: 'no translation yet', ko: '반팔티' }],
    ['áo thun dài tay', { en: 'no translation yet', ko: '긴팔티' }],
    ['dài tay', { en: 'no translation yet', ko: '긴팔' }],
    ['cổ tàu', { en: 'no translation yet', ko: '차이나카라' }],
    ['áo kiểu', { en: 'no translation yet', ko: '블라우스' }],
    ['quân lót', { en: 'no translation yet', ko: '팬티' }],
    ['quần thể thao', { en: 'no translation yet', ko: '트레이닝 팬츠' }],
    ['quần short', { en: 'no translation yet', ko: '숏팬츠' }],
    ['quần kaki', { en: 'no translation yet', ko: '카키팬츠' }],
    ['quần úm', { en: 'no translation yet', ko: '조거팬츠' }],
    ['mỹ phẩm', { en: 'Cosmetic', ko: '화장품' }],
    ['giày dép', { en: 'Shoes', ko: '신발 구두' }],
    ['gia dụng', { en: 'no translation yet', ko: '가전제품' }],
    ['túi ví', { en: 'no translation yet', ko: '가방 지갑' }],
    ['quốc tế', { en: 'no translation yet', ko: '글로벌' }],
    [
        'phụ kiện & điện thoại',
        { en: 'no translation yet', ko: '악세사리 & 휴대폰' },
    ],
    ['thiết bị điện tử', { en: 'no translation yet', ko: '전자제품' }],

    ['áo phông tay ngắn', { en: 'no translation yet', ko: '반팔티셔츠' }],
    ['áo phông tay dài', { en: 'no translation yet', ko: '긴팔티셔츠' }],
    ['tay ngắn', { en: 'Short sleeve', ko: '반팔' }],
    ['tay dài', { en: 'Long sleeves', ko: '긴팔' }],
    ['sơ mi kiểu', { en: 'Blouse', ko: '블라우스' }],
    ['sơ mi', { en: 'Shirt', ko: '셔츠' }],
    ['không tay', { en: 'Sleeveless', ko: '민소매' }],
    ['áo nỉ', { en: 'Sweatshirt', ko: '맨투맨' }],
    ['áo nỉ có mũ', { en: 'Hoodie', ko: '후드' }],
    ['áo len', { en: 'Sweater', ko: '니트' }],
    ['áo gile', { en: 'no translation yet', ko: '조끼' }],
    ['áo ngắn', { en: 'Croptop', ko: '크롭' }],

    ['váy liền/set', { en: 'Dress', ko: '드레스' }],
    ['váy liền ngắn', { en: 'Mini dress', ko: '미니원피스' }],
    ['váy liền xoè', { en: 'Maxi dress', ko: '미디원피스' }],
    ['midi', { en: 'Midi', ko: '미디' }],
    ['set', { en: 'Set', ko: '투피스' }],
    ['jumpsuit', { en: 'Jumpsuit', ko: '점프수트' }],
    ['maxi', { en: 'Maxi', ko: '맥시' }],
    ['ngắn', { en: 'Short', ko: '숏츠' }],

    ['quần ngắn', { en: 'Shorts', ko: ' 반바지' }],
    ['quần dài', { en: 'Trouser', ko: '롱팬츠' }],
    ['quần jean', { en: 'Jeans', ko: '데님' }],
    ['quần tây', { en: 'Trouser', ko: '슬랙스' }],

    ['chân váy', { en: 'Skirt', ko: '스커트' }],
    ['chân váy ngắn', { en: 'Short', ko: '미니 스커트' }],
    ['xếp ly', { en: 'Pleated skirt', ko: '플리티드 스커트' }],
	['bút chì', { en: 'Pencil skirt', ko: ' 펜슬 스커트' }],

    ['áo khoác', { en: 'Outerwear', ko: '아우터 ' }],
    ['cardigan', { en: 'Cardigan', ko: '가디건' }],
    ['áo khoác bóng chày', { en: 'Bomber jacket', ko: '점퍼' }],
    ['áo khoác gió', { en: 'Wind jacket', ko: '바람막이' }],
    ['áo khoác kaki', { en: 'Kaki jacket', ko: '야상' }],
    ['áo măng tô', { en: 'Trench coat', ko: '코트' }],
    ['áo phao', { en: 'Puffer jacket', ko: '패딩 ' }],

    ['đồ tập', { en: 'no translation yet', ko: '트레이닝' }],
    ['bộ đồ tập', { en: 'no translation yet', ko: '트레이닝 세트' }],
    ['áo tập', { en: 'no translation yet', ko: '트레이닝 상의' }],
    ['quần tập', { en: 'no translation yet', ko: '트레이닝 하의' }],
    ['legging', { en: 'no translation yet', ko: '레깅스' }],

    ['túi', { en: 'Bag', ko: '가방' }],
	['túi xách', { en: 'Bags', ko: '가방' }],
    ['túi đeo chéo', { en: 'Shoulder bag', ko: '크로스백' }],
    ['túi đeo vai', { en: 'Shoulder bag', ko: '숄더백' }],
    ['túi xách tay', { en: 'Handbag', ko: '토트백' }],
    ['clutch', { en: 'Clutch', ko: '클러치' }],
    ['túi tote', { en: 'Tote bag', ko: '토드백' }],
    ['balo', { en: 'Backpack', ko: '백팩' }],
    ['ví', { en: 'Wallet', ko: '지갑' }],
    ['túi đựng mĩ phẩm', { en: 'Cosmetic bag', ko: '파우치' }],
    ['vali', { en: 'Suitcase', ko: '슈트케이스' }],
    ['backpack', { en: 'Backpack', ko: '백팩' }],

    ['giày', { en: 'Shoes', ko: '신발' }],
    ['giày cao gót', { en: 'High heels', ko: '힐' }],
    ['dép', { en: 'Slipper', ko: '슬리퍼' }],
    ['sandal', { en: 'Sandal', ko: '샌들' }],
    ['giày thể thao', { en: 'Sporty shoes', ko: '스니커즈' }],
    ['giày bệt/giày lười', { en: 'Loafer', ko: '플랫/로퍼' }],
    ['sục/mule', { en: 'Mule', ko: '블로퍼/뮬 ' }],
    ['boots', { en: 'Boots', ko: '워커/부츠' }],
    ['sneaker', { en: 'Sneaker', ko: '스니커즈' }],

    ['phụ kiện', { en: 'Accessorize', ko: '패션 악세서리' }],
    ['phụ kiện tóc', { en: 'Hair accessorize', ko: '헤어악세서리' }],
    ['mũ', { en: 'Hat', ko: '모자' }],
    ['khẩu trang', { en: 'Face mask', ko: '마스크' }],
    ['tất/tất dài', { en: 'Socks/Long socks', ko: '양말/스타킹' }],
    ['thắt lưng', { en: 'Belt', ko: '벨트' }],
    ['đồng hồ', { en: 'Watch', ko: '시계' }],
    ['khăn quàng/scarf', { en: 'Scarf', ko: '머플러/스카프' }],
    ['kính mắt', { en: 'Glasses', ko: '아이웨어' }],
    ['khác', { en: 'Other', ko: '기타' }],
    ['phụ kiện điện thoại', { en: 'Phone accessorize', ko: '휴대폰 악세서리' }],
    ['phụ kiện thể thao', { en: 'Sport accessorize', ko: '스포츠 악세서리' }],

    ['trang sức', { en: 'Jewelry', ko: '주얼리' }],
    ['hoa tai', { en: 'Earings', ko: '귀걸이' }],
    ['vòng cổ', { en: 'Necklace', ko: '목걸이' }],
    ['nhẫn', { en: 'Rings', ko: '반지' }],
    ['vòng tay', { en: 'Barcelet', ko: '팔지' }],
    ['lắc chân', { en: 'no translation yet', ko: '발찌' }],
    ['hộp đựng trang sức', { en: 'no translation yet', ko: '보석함' }],

	['đồ ngủ', { en: 'Homewear', ko: '파자마' }],
    ['đồ mặc nhà', { en: 'Homewear', ko: '홈웨어' }],
    ['đồ bộ', { en: 'Night-suit', ko: '세트' }],
    ['váy ngủ', { en: 'Nightgown', ko: '원피스' }],
    ['quần ngủ', { en: 'Sleeping paint', ko: '잠옷바지' }],
    ['áo choàng', { en: 'Cloak', ko: '로브/가운' }],
    ['dài', { en: 'Long', ko: '롱' }],

    ['đồ lót', { en: 'Underwear', ko: '언더웨어' }],
    ['áo bra', { en: 'Bra', ko: '브라' }],
    ['quần lót', { en: 'Underwear', ko: '팬티' }],
    ['bộ đồ lót', { en: 'Underwear set', ko: '속옷세트' }],
    ['áo lót', { en: 'Bra', ko: '이너' }],
    ['đồ định hình', { en: 'Shapewear', ko: '보정' }],
    ['ren', { en: 'Lace', ko: '레이스' }],

    ['đồ bơi', { en: 'Swimwear', ko: '수영복' }],
    ['bikini 2 mảnh', { en: 'no translation yet', ko: '비키니' }],
    ['bikini 1 mảnh', { en: 'no translation yet', ko: '모노키니' }],
    ['đồ bơi dài tay', { en: 'no translation yet', ko: '래쉬가드' }],
    ['váy bơi', { en: 'no translation yet', ko: '원피스수영복' }],
    ['áo bơi', { en: 'no translation yet', ko: '비치상의' }],
    ['quần bơi', { en: 'no translation yet', ko: '비치하의' }],
    ['giày lội nước', { en: 'no translation yet', ko: '아쿠아슈즈' }],
    ['bikini', { en: 'Bikini', ko: '비키니' }],

    ['sắc đẹp', { en: 'no translation yet', ko: '뷰티' }],
    [
        'trang điểm theo từng bộ phận',
        { en: 'no translation yet', ko: '포인트 메이크업' },
    ],
    ['trang điểm', { en: 'Makeup', ko: '메이크업' }],
    ['trang điểm nền', { en: 'no translation yet', ko: '베이스 메이크업' }],
    ['chăm sóc da', { en: 'Skincare', ko: '스킨케어' }],
    ['tẩy trang', { en: 'no translation yet', ko: '클렌징 ' }],
    ['chăm sóc cơ thể', { en: 'Body care', ko: '바디케어' }],
    ['chăm sóc tóc', { en: 'Hair care', ko: '헤어케어' }],
    ['phụ kiện làm đẹp', { en: 'no translation yet', ko: '뷰티소품' }],

    ['thể thao', { en: 'Sportwear', ko: '운동복' }],
    ['yoga', { en: 'Yoga', ko: '요가' }],
    ['gym', { en: 'Gym', ko: '웨이트' }],

    ['nước hoa', { en: 'Perfumes', ko: '향수' }],
    ['xịt thơm toàn thân', { en: 'Bodymist', ko: '바디미스트' }],

    ['dụng cụ làm đẹp', { en: 'Beauty gadgets', ko: '미용 뷰티' }],
    ['máy rửa mặt', { en: 'Face cleanser', ko: '세안제' }],
    ['máy chăm sóc da mặt', { en: 'Facial machine', ko: '피부 미용 도구' }],
    ['máy nâng cơ mặt', { en: 'Face lifting machine', ko: '주름 관리 제품' }],
    ['dụng cụ trang điểm', { en: 'Makeup tools', ko: '메이크업 도구' }],
	['ưu đãi', { en: 'Promotion', ko: '세일' }],
]);

export const NAME_LOCALIZED_SIZE = ["사이즈", "size", "kích cỡ"];

export const FAKE_TOP_SHOP_IDS = [
	"615db4aba1a53e0013dc534e", 
	"611e534be46a3d001252ec42", 
	"613ec1a703987d0012665a9c", 
	"6141fce480d0260012a7896e", 
	"6103b5073a4364001112f397"
]


export const SHUFFLE_SUGGEST_PRODUCT_PAGE = {
	"0": [9,12,5,18,13,16,2,14,6,8,1,3,15,10,17,7,4,11,19,20],
	"1": [15,14,3,6,7,10,4,2,18,11,17,9,8,13,16,1,12,5,19,20],
	"2": [1,8,6,13,15,12,11,7,4,9,16,14,2,17,5,3,18,10,19,20],
	"3": [5,16,6,14,1,13,15,12,3,17,9,4,8,7,11,10,2,18,19,20],
	"4": [6,9,8,15,1,13,12,14,4,17,2,3,18,5,10,7,11,16,19,20],
	"5": [2,12,9,16,11,18,10,17,8,6,4,5,3,1,13,15,14,7,19,20],
	"6": [10,9,2,14,7,6,13,5,1,15,11,3,12,4,8,18,17,16,19,20],
	"7": [11,17,10,14,16,9,12,18,8,2,15,4,5,7,13,1,6,3,19,20],
	"8": [8,1,10,4,13,6,2,12,3,15,17,7,18,5,11,14,16,9,19,20],
	"9": [9,13,18,15,5,8,16,11,3,7,2,10,17,6,4,14,12,1,19,20],
	"10": [11,5,10,6,3,17,8,15,9,16,1,4,2,12,14,13,18,7,19,20],
	"11": [17,15,3,14,11,18,13,7,8,5,2,6,12,10,4,16,9,1,19,20],
	"12": [14,11,16,7,15,9,12,1,10,8,17,5,13,3,6,2,4,18,19,20],
	"13": [1,6,15,7,10,12,9,18,3,17,11,16,2,4,13,5,14,8,19,20],
	"14": [3,8,6,10,17,13,2,1,14,11,12,15,9,16,18,5,4,7,19,20],
	"15": [11,9,18,17,6,15,8,4,14,3,12,16,1,10,13,7,2,5,19,20],
	"16": [8,11,13,1,17,4,12,7,2,10,16,18,3,9,14,6,5,15,19,20],
	"17": [16,11,7,3,9,8,1,17,4,15,13,6,5,2,18,10,12,14,19,20],
	"18": [3,7,9,8,18,11,5,10,6,2,17,1,13,15,14,12,16,4,19,20],
	"19": [9,4,3,13,15,18,16,12,2,14,6,17,11,7,10,8,1,5,19,20]
}

export const SHUFFLE_NEWEST_PRODUCT_PAGE = {
	"0":[1,7,6,4,10,5,2,8,9,3],
	"1":[3,6,8,2,5,4,7,10,9,1],
	"2":[7,6,5,10,3,1,9,8,4,2],
	"3":[2,7,6,5,3,4,10,9,8,1],
	"4":[1,2,7,6,8,10,3,9,5,4],
	"5":[4,5,10,8,1,2,3,7,6,9],
	"6":[4,9,10,7,6,8,2,5,1,3],
	"7":[7,5,1,10,8,4,3,9,2,6],
	"8":[2,4,7,1,6,9,3,10,5,8],
	"9":[9,5,1,3,10,7,4,8,6,2],
	"10":[4,1,9,2,3,5,6,7,10,8],
	"11":[5,6,3,7,8,2,4,10,9,1],
	"12":[8,4,3,10,2,6,7,9,1,5],
	"13":[10,3,9,2,1,5,6,4,8,7],
	"14":[4,10,9,2,3,1,8,6,7,5],
	"15":[6,2,10,9,3,8,1,4,7,5],
	"16":[2,10,7,3,9,6,4,1,8,5],
	"17":[3,7,8,5,10,6,2,1,4,9],
	"18":[5,9,1,6,3,2,8,7,10,4],
	"19":[7,8,5,2,1,3,4,10,6,9]
}

export const CATEGORIES_FASHION = [
	"áo", 
	"quần",
	"chân váy",
	"áo khoác", 
	"váy liền/set",
	"đồ lót",
	"đồ ngủ",
	"đồ bơi",
	"thể thao",
	"giày dép",
	"túi xách",
	"phụ kiện",
	"trang sức",
]


export const CATEGORIES_COSMETIC = [
	"mỹ phẩm",
	"nước hoa",
	"dụng cụ làm đẹp"
]
export const QUEUE_PRIORITY = {
	HIGH: "high",
	MEDIUM: "medium",
	LOW: "low"
}

export const CATEGORIES = // Category change
[{
    _id: "6128fda02d6f6b23c315fa45",
    name: "Áo",
    new_childs: [
        { name: "Áo ngắn", permalink: "ao-ngan" }
    ]
}, {
    _id: "6128fda02d6f6b23c315fa47",
    name: "Quần",
    new_childs: [],
    migrate_childs: [{ from: "6128fda02d6f6b23c315fa5e", to: "6128fda02d6f6b23c315fa60" }]
}, {
    _id: "6128fda02d6f6b23c315fa48",
    name: "Chân váy",
    new_childs: [
        { name: "Midi", permalink: "midi" },
        { name: "Bút chì", permalink: "but-chi"}
    ]
}, {
    _id: "6128fda02d6f6b23c315fa49",
    name: "Áo khoác",
    new_childs: []
}, {
    _id: "6128fda02d6f6b23c315fa46",
    name: "Váy liền / set",
    new_childs: [
        { name: "Maxi", permalink: "maxi" }
    ]
}, {
    _id: "6128fda02d6f6b23c315fa50",
    name: "Đồ lót",
    new_childs: [
        { name: "Thể thao", permalink: 'do-lot-the-thao' },
        { name: "Ren", permalink: "do-lot-ren" }
    ]
}, {
    _id: "6128fda02d6f6b23c315fa4f",
    name: "Đồ ngủ",
    new_childs: [
        { name: "Dài", permalink: "do-ngu-dai"},
        { name: "Ngắn", permalink: "do-ngu-ngan"}
    ]
}, {
    _id: "6128fda02d6f6b23c315fa51",
    name: "Đồ bơi",
    new_childs: [
        { name: "Ngắn", permalink: "do-boi-ngan"},
        { name: "Bikini", permalink: "bo-boi-bikini"}
    ]
}, {
    _id: null,
    name: "Thể thao",
	avatar: 'https://commerce.bidu.com.vn//uploads/1629954814737_image_161132.png',
	permalink: "the-thao",
    new_childs: [
        { name: "Yoga", permalink: "yoga" },
        { name: "Gym", permalink: "gym" }
    ]
}, {
    _id: "6128fda02d6f6b23c315fa4c",
    name: "Giày dép",
    new_childs: [
        { name: "Sneaker", permalink: "sneaker"}
    ]
}, {
    _id: "6128fda02d6f6b23c315fa4b",
    name: "Túi xách",
    new_childs: [
        { name: "Vali", permalink: 'vali' },
        { name: "Backpack", permalink: 'backpack' }
    ]
}, {
    _id: "6128fda02d6f6b23c315fa4d",
    name: "Phụ kiện thời trang",
    new_childs: [
        { name: "Phụ kiện điện thoại", permalink: "phu-kien-dien-thoai" },
        { name: "Phụ kiện thể thao", permalink: "phu-kien-the-thao" }
    ]
}, {
    _id: "6128fda02d6f6b23c315fa4e",
    name: "Trang sức",
    new_childs: []
}, {
    _id: "6128fda02d6f6b23c315fa52",
    name: "Mỹ phẩm", // Sắc đẹp
    new_childs: [],
    migrate_childs: [
        {from: "6128fda02d6f6b23c315faa6", to: "6128fda02d6f6b23c315faa5"}
    ]
}, {
    _id: null,
    name: "Nước hoa",
	avatar: 'https://commerce.bidu.com.vn//uploads/1629970920650_image_454802.png',
	permalink: "nuoc-hoa",
    new_childs: [
        { name: "Nước hoa", permalink: "nuoc-hoa" },
        { name: "Xịt thơm toàn thân", permalink: "xit-thom-toan-than" },
    ]
}, {
    _id: null,
    name: "Dụng cụ làm đẹp",
	avatar: 'https://commerce.bidu.com.vn//uploads/1629970920650_image_454802.png',
	permalink: "dung-cu-lam-dep",
    new_childs: [
        { name: "Máy rửa mặt", permalink: "may-rua-mat" },
        { name: "Máy chăm sóc da mặt", permalink: "may-cham-soc-da-mat" },
        { name: "Máy nâng cơ mặt", permalink: "may-nang-co-mat" },
        { name: "Dụng cụ trang điểm", permalink: "dung-cu-trang-diem" }
    ]
}]

export const TOP_SHOP_BANNER_DEFAULT = {
	name: "Top Shop Banner Default",
	is_active: true,
	images: {
		vi: `${keys.host_community}/uploads/1648487065606_image_308044.jpg`,
		en: `${keys.host_community}/uploads/1648487065606_image_308044.jpg`,
		ko: `${keys.host_community}/uploads/1648487065606_image_308044.jpg`
	}
}

export const BUCKET_MEDIA_COMMERCE = 'upload_media'

export const EVENT_VOUCHER_CODE = 'BIDU114'
