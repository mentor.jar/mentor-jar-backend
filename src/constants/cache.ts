export enum KEY {
    STORE_USER = 'store_user',

    STORE_PRODUCT = 'store_product',
    STORE_PREPARE_ORDER = 'store_prepare_order',
    STORE_GROUP_ORDER_OF_GB = 'group_orders_for_group_buy',
}