export enum TYPE {
    FLASH_SALE = 'flash_sale',
    HOT_DEAL = 'hot_deal',
    BEST_SELL = 'best_sell',
    NEWEST = 'newest',
}

export const MAXIMUM_NUMBER_RETURNED = 30;