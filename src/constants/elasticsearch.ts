export const enum SCAN_MODE {
    ALL = 'ALl',
    HOURLY = 'HOURLY',
    FIVE_MINUTE = 'FIVE_MINUTE',
}