import * as productFunctions from '../../../controllers/api/mobile_app/products'
import { authShop, authWeb } from '../../../middleware/auth';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/count-product')
    .get(authShop, productFunctions.getCountProduct)

module.exports = router;