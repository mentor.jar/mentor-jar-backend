// README: Add suffix 'CMS' after router file name
const appWeb = require('express')();

const homeWeb = require("./home");
const shopExploreWeb = require("./buyer/shop");
const productExploreWeb = require("./buyer/productExplore");
const feedbackWeb = require("./buyer/feedbacks");
const orderBuyerWeb = require('./buyer/order');
const saleWeb = require('./buyer/sale');
const checkoutWeb = require('./buyer/checkout');
const productMyshopWeb = require('./myshop/product');
const categoryWeb = require('./categories');
const cartWeb = require('./buyer/cart');

appWeb.use('/home', homeWeb)
appWeb.use('/shop-explores', shopExploreWeb)
appWeb.use('/product-explores', productExploreWeb)
appWeb.use('/feedbacks', feedbackWeb)
appWeb.use('/orders', orderBuyerWeb)
appWeb.use('/sale', saleWeb)
appWeb.use('/check-out', checkoutWeb)
appWeb.use('/carts', cartWeb)
// myshop
appWeb.use('/myshop/product', productMyshopWeb)
appWeb.use('/categories', categoryWeb)

module.exports = appWeb;