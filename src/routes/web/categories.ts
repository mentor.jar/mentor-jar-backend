
import * as categoryFunctions from '../../controllers/api/web/category';
import { auth } from '../../middleware/auth';
import { findCategoryMiddleware } from '../../middleware/category/categoryMiddleware';


const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/by-shop')
    .get(auth, categoryFunctions.getCategoriesByShop);

router.route('/shop-categories/:id')
    .get(auth, findCategoryMiddleware, categoryFunctions.getDetailShopCategory)

module.exports = router;
