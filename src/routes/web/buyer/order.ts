import * as orderFunctions from '../../../controllers/api/mobile_app/buyer/orders'
import { authWeb } from '../../../middleware/auth';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/count-order')
    .get(authWeb, orderFunctions.countOrder)

module.exports = router;