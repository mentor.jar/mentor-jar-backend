import * as feedbackFunctions from '../../../controllers/api/mobile_app/buyer/feedbacks'
import { feedbackCondition, filterMiddleware, checkProductBeforeFeedback, findFeedbackMiddleware } from '../../../middleware/feedback/feedbackMiddleware'

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/:product_id')
    .get(filterMiddleware, feedbackFunctions.getFeedbackByProductWithFilter);

module.exports = router;