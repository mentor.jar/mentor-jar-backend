import { authWeb } from "../../../middleware/auth";
import { checkoutMiddleware, groupBuyCheckoutMiddleware } from "../../../middleware/carts/cart";
import { getCheckoutPage, getGroupBuyCheckoutPage } from "../../../controllers/api/mobile_app/buyer/carts";

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/checkout')
    .post(authWeb, checkoutMiddleware, getCheckoutPage)


module.exports = router;