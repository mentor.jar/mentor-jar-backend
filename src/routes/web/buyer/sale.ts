import * as saleFunction from '../../../controllers/api/web/sale';
import { authWeb } from '../../../middleware/auth';
import { findShopMiddleware } from '../../../middleware/shop/shopMiddleware';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/sale-vouchers')
    .get(authWeb, saleFunction.getVoucherSale);

module.exports = router;