import { authWeb } from '../../../middleware/auth'
import { checkAndFormatBeforeGet } from '../../../middleware/carts/cart'
import * as cartFunctions from '../../../controllers/api/mobile_app/buyer/carts'

const express = require('express');
const router = express.Router({ mergeParams: true });


router.route('/system-vouchers')
    .post(authWeb, checkAndFormatBeforeGet, cartFunctions.getSystemVoucher)


module.exports = router;
