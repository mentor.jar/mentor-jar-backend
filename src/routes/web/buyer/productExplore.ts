import { findProductBuyerMiddleware } from '../../../middleware/product/productMiddleware'
import * as productFunctions from '../../../controllers/api/mobile_app/products'
import { authWeb } from '../../../middleware/auth';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/:id')
    .get(authWeb, productFunctions.getDetailWhenUserGoToShop)

router.route('/:id/similar')
    .get(authWeb, findProductBuyerMiddleware, productFunctions.getSimilarProducts)

module.exports = router;