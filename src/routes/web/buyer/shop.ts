import * as shopController from '../../../controllers/api/mobile_app/buyer/shop_explore';
import * as shopFunction from '../../../controllers/api/mobile_app/shop';
import { auth, authWeb } from '../../../middleware/auth';
import { findShopMiddleware } from '../../../middleware/shop/shopMiddleware';
import * as voucherFunctions from '../../../controllers/api/mobile_app/vouchers'
import * as streamFunctions from '../../../controllers/api/mobile_app/stream_session'

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/:id')
    .get(authWeb, findShopMiddleware, shopFunction.getDetailShopWithStatistic);

router.route('/:id/all-categories')
    .get(findShopMiddleware, shopController.getShopCategories);

router.route('/:id/search')
    .get(authWeb, findShopMiddleware, shopController.searchShopProducts);

router.route('/:id/top-products')
    .get(authWeb, findShopMiddleware, shopController.getTopProductOfShop);

router.route('/:id/vouchers')
    .get(authWeb, voucherFunctions.getVoucherWithFilterOfShopWithoutHiddenVoucher);

router.route('/vouchers/:id')
    .get(authWeb, voucherFunctions.getDetailVoucher);

router.route('/stream-sessions/:id')
    .get(authWeb, streamFunctions.getStreamSession);

module.exports = router;