import * as homeFunctions from '../../controllers/api/mobile_app/buyer/home';
import * as categoryFunctions from '../../controllers/api/web/category';
import * as categoryFunctionsMobile from '../../controllers/api/mobile_app/categories'
import { authWeb } from '../../middleware/auth';


const express = require('express');
const router = express.Router({ mergeParams: true });

// router.route('/')
//     .get(authWeb, homeFunctions.homeData)

router.route('/banners/:id')
    .get(authWeb, homeFunctions.bannerSystemDetail);

router.route('/search')
    .get(authWeb, homeFunctions.search);

router.route('/popular-search')
    .get(authWeb, homeFunctions.popularSearch);

router.route('/suggest-products')
    .get(authWeb, homeFunctions.getSuggestProduct);

router.route('/category-detail')
    .get(authWeb, categoryFunctions.categoryDetail);

router.route('/categories')
    .get(authWeb, categoryFunctionsMobile.getAllCategories);

module.exports = router;
