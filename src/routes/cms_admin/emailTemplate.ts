import { authAdmin } from '../../middleware/auth'
import * as emailFunctions from '../../controllers/api/cms_admin/email_template'
import { emailTemplateConditions, emailTemplateMiddleware, findEmailTemplateMiddleware } from '../../middleware/emails/emailTemplateMiddleware';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .post(authAdmin, emailTemplateConditions, emailTemplateMiddleware, emailFunctions.createEmailTemplate)
    .get(authAdmin, emailFunctions.getEmailTemplateList)

router.route('/:id')
    .put(authAdmin, emailTemplateConditions, emailTemplateMiddleware, findEmailTemplateMiddleware, emailFunctions.updateEmailTemplate)
    .delete(authAdmin, findEmailTemplateMiddleware, emailFunctions.deleteEmailTemplate)


module.exports = router;