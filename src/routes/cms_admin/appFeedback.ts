import { authAdmin } from '../../middleware/auth'
import * as appFeedbackFunctions from '../../controllers/api/cms_admin/app_feedback'
import { appFeedbackMiddleware, filterAppFeedbackConditions } from '../../middleware/appFeedbackMiddleware';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .post(authAdmin, filterAppFeedbackConditions, appFeedbackMiddleware, appFeedbackFunctions.getListFeedbackWithFilter);

router.route('/:id')
    .get(authAdmin, appFeedbackFunctions.detailAppFeedback);

module.exports = router;