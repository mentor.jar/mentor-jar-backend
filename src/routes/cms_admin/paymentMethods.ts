import * as paymentMethodController from '../../controllers/api/cms_admin/payment_methods';
import { authAdmin } from "../../middleware/auth";

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .get(authAdmin, paymentMethodController.getAllMethods);

module.exports = router;
