import * as bannerController from '../../controllers/api/cms_admin/banners';
import { authAdmin } from "../../middleware/auth";
import {
    createBannerRule, createBannerSystemRule, createBannerValidate, updateBannerRule, updateBannerSystemRule,
    updateBannerValidate, validateProductBannerSystem, validateProductCMS, validateTimeCreate,
    validateTimeUpdate, validateSystemBannerVoucher, validateVoucherBannerSystem, validateLimitBanner,
    validateAdvanceAction,
    createTopShopBannerSystemRule,
    updateTopShopBannerRule,
    verifyShopExistTopShopBanner
} from '../../middleware/banner/bannerMiddleware';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/top-shop')
    .get(authAdmin, bannerController.listTopSopBanner)
    .post(authAdmin, createTopShopBannerSystemRule, createBannerValidate, verifyShopExistTopShopBanner, bannerController.createTopShopBanner);
router.route('/top-shop/:id')
    .get(authAdmin, bannerController.getTopShopBannerDetail)
    .put(authAdmin, updateTopShopBannerRule, updateBannerValidate, bannerController.updateTopShopBanner)
    .delete(authAdmin, bannerController.deleteTopShopBanner);
router.route('/')
    .get(authAdmin, bannerController.listBanner)
    .post(authAdmin, createBannerRule, createBannerValidate, validateProductCMS, validateTimeCreate, bannerController.createBanner);
router.route('/system/')
    .get(authAdmin, bannerController.listBannerSystem)
    .post(authAdmin, createBannerSystemRule, createBannerValidate, validateLimitBanner, validateSystemBannerVoucher, validateProductBannerSystem, validateVoucherBannerSystem, validateAdvanceAction, validateTimeCreate, bannerController.createBannerSystem);
router.route('/system/:id')
    .get(authAdmin, bannerController.getBannerDetail)
    .put(authAdmin, updateBannerSystemRule, updateBannerValidate, validateLimitBanner, validateSystemBannerVoucher,
        validateProductBannerSystem, validateAdvanceAction, validateTimeUpdate, validateVoucherBannerSystem, bannerController.updateBannerSystem)
    .delete(authAdmin, bannerController.deleteBanner);
router.route('/:id')
    .get(authAdmin, bannerController.getBannerDetail)
    .put(authAdmin, updateBannerRule, updateBannerValidate,
        validateProductCMS, validateTimeUpdate, bannerController.updateBanner)
    .delete(authAdmin, bannerController.deleteBanner);
router.route('/:id/active')
    .put(authAdmin, bannerController.setActiveBanner);

module.exports = router;
