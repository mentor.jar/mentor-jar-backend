import * as ChatFunctions from '../../controllers/api/cms_admin/chat'
import { authAdmin } from '../../middleware/auth';
const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .get(authAdmin, ChatFunctions.getListRoomChat)
router.route('/total-red-dot')
    .post(authAdmin, ChatFunctions.getRedDot)
router.route('/:id')
    .get(authAdmin, ChatFunctions.getDetailRoomChat)

module.exports = router;
