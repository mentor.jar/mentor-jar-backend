import { getAnalystEcommerce, getAnalystEcommerceV2, getAnalystEcommerceFinal } from '../../controllers/api/cms_admin/analyst';
import { authAdmin } from '../../middleware/auth'

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .get(authAdmin, getAnalystEcommerce);

router.route('/new')
    .get(authAdmin, getAnalystEcommerceV2)
    .post(authAdmin, getAnalystEcommerceFinal)

module.exports = router;