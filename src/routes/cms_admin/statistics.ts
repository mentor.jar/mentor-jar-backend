
import { authAdmin } from '../../middleware/auth'
import * as statisticsFunctions from '../../controllers/api/cms_admin/statistics'

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/dashboard')
    .get(authAdmin, statisticsFunctions.dashboardInfo);
router.route('/shop-ranking')
    .get(authAdmin, statisticsFunctions.shopRanking)
router.route('/product-ranking')
    .get(authAdmin, statisticsFunctions.productRanking)
router.route('/orders')
    .get(authAdmin, statisticsFunctions.orderStatistic);

module.exports = router;
