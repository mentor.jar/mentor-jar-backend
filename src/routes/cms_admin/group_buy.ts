import { authAdmin } from './../../middleware/auth';
import * as GroupBuyFunctions from "../../controllers/api/cms_admin/group_buy"

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/approve')
    .put(authAdmin, GroupBuyFunctions.approveGroupBuyOfShop)

module.exports = router;