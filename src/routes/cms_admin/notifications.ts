import * as notificationController from '../../controllers/api/cms_admin/notification';
import { authAdmin } from '../../middleware/auth';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .get(authAdmin, notificationController.getSystemNotification)
    .post(authAdmin, notificationController.createSystemNotification)

router.route('/:id')
    .get(authAdmin, notificationController.getSystemNotificationDetail)

module.exports = router;
