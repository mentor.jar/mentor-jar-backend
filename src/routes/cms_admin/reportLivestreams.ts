import * as reportLivestreamController from '../../controllers/api/cms_admin/report_livestream';
import { auth, authAdmin } from "../../middleware/auth";

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .get(authAdmin, reportLivestreamController.getAllReports);

module.exports = router;
