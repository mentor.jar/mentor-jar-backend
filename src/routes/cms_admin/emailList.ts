import { sendEmailMKT, getEmailList, archiveEmail } from "../../controllers/api/cms_admin/email_list";
import { authAdmin } from "../../middleware/auth";
import { sendEmailConditions, emailListMiddleware, validateBeforeSendEmail, findEmailListMiddleware } from "../../middleware/emails/emailListMiddleware";

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .post(authAdmin, sendEmailConditions, emailListMiddleware, validateBeforeSendEmail, sendEmailMKT)
    .get(authAdmin, getEmailList)

router.route('/:id')
    .delete(authAdmin, findEmailListMiddleware, archiveEmail)

module.exports = router;