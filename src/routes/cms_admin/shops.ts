// import { imageMiddleware } from '../middleware/uploadMiddleware'
import { authAdmin } from '../../middleware/auth'
import * as shopFunctions from '../../controllers/api/cms_admin/shops'
import { 
    shopMiddleware, 
    findShopMiddleware,
    shopSettingValidate,
    checkShopAccountExistByAdminMiddleware, 
    activeShippingShopByAdminMethodValidate, 
    checkInforBeforeApprove,
    checkShippingMethodsByAdminMiddleware
} from '../../middleware/shop/shopMiddleware';

const express = require('express');
const router = express.Router({ mergeParams: true });


router.route('/')
    .get(authAdmin, shopFunctions.getListShop);

router.route('/:id')
    .get(authAdmin, shopFunctions.getDetailShop);
router.route('/:id/setting')
    .post(authAdmin, findShopMiddleware, shopSettingValidate, shopFunctions.settingShop);

router.route('/:id/show-on-top')
    .patch(authAdmin, shopFunctions.toggleShowShopOnTop)

router.route('/active-shipping')
    .put(authAdmin, activeShippingShopByAdminMethodValidate, shopMiddleware, checkShopAccountExistByAdminMiddleware, checkShippingMethodsByAdminMiddleware, shopFunctions.activeShippingMethodForShop);

router.route('/approve-shop')
    .patch(authAdmin, checkInforBeforeApprove, shopFunctions.adminApproveOrRejectSeller)

module.exports = router;
