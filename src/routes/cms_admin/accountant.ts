import { authAdminWithTypeRole, hasPermission } from '../../middleware/auth';
import * as orderFunctions from '../../controllers/api/cms_admin/accountant';

const express = require('express');
const router = express.Router({ mergeParams: true });

router
    .route('/transactions')
    .get(
        authAdminWithTypeRole,
        hasPermission(['transaction_accountant_read']),
        orderFunctions.transactionsForAccountant
    );
router
    .route('/transactions/export')
    .get(
        authAdminWithTypeRole,
        hasPermission(['transaction_accountant_export']),
        orderFunctions.exportTransactionsForAccountant
    );

module.exports = router;
