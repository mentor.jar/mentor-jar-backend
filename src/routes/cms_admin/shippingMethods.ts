import * as shippingMethodController from '../../controllers/api/cms_admin/shipping_methods';
import { authAdmin } from "../../middleware/auth";

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .get(authAdmin, shippingMethodController.getAllMethods);

module.exports = router;
