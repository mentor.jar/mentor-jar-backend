// import { imageMiddleware } from '../middleware/uploadMiddleware'
import * as productFunctions from '../../controllers/api/cms_admin/products';
import { authAdmin } from '../../middleware/auth';
import { createProductCMSMiddleware, findProductMiddleware, productCondition, productUpdateCondition, 
    updateProductCMSMiddleware, checkProductBeforeUpdateWeight, productUpdateWeightCondition, 
    updateWeightProductCMSMiddleware, requireSizeInfoInDetailSize, validateQuantityProduct, updatePriceProductMiddleware, checkShippingMethodsByAdminMiddleWare } from '../../middleware/product/productMiddleware';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/:id')
    .get(authAdmin, findProductMiddleware, productFunctions.getProductDetail)
    .put(authAdmin, productUpdateCondition, updateProductCMSMiddleware, validateQuantityProduct, requireSizeInfoInDetailSize, findProductMiddleware, updatePriceProductMiddleware,
        productFunctions.updateProduct)
    .delete(authAdmin, findProductMiddleware, productFunctions.deleteProduct);
router.route('/:id/approve')
    .put(authAdmin, findProductMiddleware, productFunctions.approveRejectProduct);
router.route('/:id/photo-full-mode')
    .patch(authAdmin, findProductMiddleware, productFunctions.toggleDisplayFullModeTopPhoto)
router.route('/')
    .get(authAdmin, productFunctions.getListProduct);
router.route('/')
    .post(authAdmin, productCondition, createProductCMSMiddleware, validateQuantityProduct, requireSizeInfoInDetailSize, checkShippingMethodsByAdminMiddleWare, productFunctions.createProduct)

router.route('/weights')
    .patch(authAdmin, productUpdateWeightCondition, updateWeightProductCMSMiddleware, checkProductBeforeUpdateWeight, productFunctions.updateWeightManyProduct);

module.exports = router;
