import { authAdmin } from '../../middleware/auth'
import * as sellerSignUpFunctions from '../../controllers/api/cms_admin/sellerSignUp'
import { findRegisterSellerMiddleware, validateBeforeHandle } from '../../middleware/user/sellerSignUpMiddleware';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .get(authAdmin, sellerSignUpFunctions.getListSellerRegister);

router.route('/:id')
    .get(authAdmin, findRegisterSellerMiddleware, sellerSignUpFunctions.getDetailSellerRegister)
    .put(authAdmin, validateBeforeHandle, sellerSignUpFunctions.handleSellerRegister);

router.route('/:id/reject')
    .put(authAdmin, validateBeforeHandle, sellerSignUpFunctions.rejectSellerRegister)

module.exports = router;