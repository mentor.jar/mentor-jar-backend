import * as voucherFunctions from '../../controllers/api/cms_admin/vouchers'
import * as voucherMobileFunctions from '../../controllers/api/mobile_app/vouchers'
import { auth,authAdmin } from '../../middleware/auth';
import { voucherCMSCondition, createVoucherCMSMiddleware, findVoucherMiddleware, updateVoucherCMSMiddleware, 
    voucherCondition, createVoucherMiddleware, requireShopOwner, updateVoucherMiddleware,
    validateVoucherCode } from '../../middleware/voucher/voucher'

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .post(auth, voucherCMSCondition, validateVoucherCode, createVoucherCMSMiddleware, voucherFunctions.createVoucher)
    .get(authAdmin, voucherFunctions.getListVoucher);
router.route('/:id')
    .get(authAdmin, voucherFunctions.getDetailVoucher)
    .put(authAdmin, findVoucherMiddleware, voucherCMSCondition, validateVoucherCode, updateVoucherCMSMiddleware, voucherFunctions.updateVoucher)
    .delete(authAdmin, findVoucherMiddleware, voucherFunctions.deleteVoucher)
router.route('/:id/end')
    .put(authAdmin, findVoucherMiddleware, voucherFunctions.endVoucher)
router.route('/shop-vouchers')
    .post(authAdmin, voucherCondition, requireShopOwner, createVoucherMiddleware, voucherMobileFunctions.createVoucher)
router.route('/shop-vouchers/:id')
    .put(authAdmin, voucherCondition, requireShopOwner, updateVoucherMiddleware, voucherMobileFunctions.updateCMSVoucherForMobile)

module.exports = router;
