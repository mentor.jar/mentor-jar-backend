import { authAdmin } from '../../middleware/auth'
import * as shopMetricFunctions from '../../controllers/api/cms_admin/shop_metric'

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/total-shop/export')
    .get(shopMetricFunctions.totalShopExport)

router.route('/shop-by-categories/export')
    .get(authAdmin, shopMetricFunctions.shopByCategoriesExport)

router.route('/rating-shop/export')
    .get(authAdmin, shopMetricFunctions.ratingShopExport)

router.route('/transaction/export')
    .get(authAdmin, shopMetricFunctions.transactionExport)

router.route('/revenue/export')
    .get(shopMetricFunctions.exportRevenue)

router.route('/revenue-by-categories/export')
    .get(shopMetricFunctions.exportRevenueByCategories)

router.route('/total-shop')
    .get(authAdmin, shopMetricFunctions.totalShopMetric)
    
router.route('/shop-by-categories')
    .get(authAdmin, shopMetricFunctions.shopByCategoriesMetric)

router.route('/rating-shop')
    .get(authAdmin, shopMetricFunctions.ratingShopMetric)

router.route('/total-revenue')
    .get(shopMetricFunctions.totalShopRevenue)

router.route('/overview')
    .get(shopMetricFunctions.overview)

router.route('/order-quantity')
    .get(authAdmin, shopMetricFunctions.orderQuantityStatistic)

router.route('/transaction-shop')
    .get(authAdmin, shopMetricFunctions.shopTransactionStatistic)

router.route('/transaction-spending')
    .get(authAdmin, shopMetricFunctions.shopSpending)

module.exports = router;
