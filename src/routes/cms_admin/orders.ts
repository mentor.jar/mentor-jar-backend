import { authAdmin } from '../../middleware/auth'
import * as orderFunctions from '../../controllers/api/cms_admin/orders'
import { findOrderMiddleware, approveRejectOrderMiddleware, createOrderSellerRule, orderMiddleware, checkValidProductInformation, checkOrderOfShop, isShopOwnerOrderCMS, approveRejectOrderRefundAdminMiddleware, approveRejectOrderRule, approveRejectOrderValidate, confirmTransferRefundMoneyAdminMiddleware } from '../../middleware/order/orderMiddleware';
import * as addressController from '../../controllers/api/mobile_app/buyer/addresses';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .get(authAdmin, orderFunctions.getListOrder);
router.route('/prepare-order')
    .post(authAdmin, createOrderSellerRule, orderMiddleware, isShopOwnerOrderCMS, checkValidProductInformation, orderFunctions.sellerCreateOrder);
router.route('/approve-reject-multy')
    .post(authAdmin, orderFunctions.approveRejectMultyOrders)
router.route('/:id')
    .get(authAdmin, findOrderMiddleware, orderFunctions.getDetailOrder)
    .put(authAdmin, findOrderMiddleware, approveRejectOrderMiddleware, orderFunctions.approveRejectOrder);
router.route('/:id/update-pick-status')
    .patch(authAdmin, orderFunctions.updatePickOrderStatus)
router.route('/:id/address-valid')
    .post(authAdmin, checkOrderOfShop, addressController.getAddressValidForSellerByAdmin)
router.route('/:id/transfer-refund-money')
    .put(authAdmin, confirmTransferRefundMoneyAdminMiddleware, orderFunctions.updateRefundMoneyStatus)

router.route('/:id/refund/approve-reject')
    .put(authAdmin, approveRejectOrderRule, approveRejectOrderValidate, approveRejectOrderRefundAdminMiddleware, orderFunctions.approveRejectRefundRequest);

router.route('/:id/penalty')
    .patch(authAdmin, findOrderMiddleware, orderFunctions.markPenaltyForAnOrder)

module.exports = router;
