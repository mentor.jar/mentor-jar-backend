import * as settingController from '../../controllers/api/cms_admin/settings';
import { authAdmin } from '../../middleware/auth';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/product-price').patch(authAdmin, settingController.toggleProductPriceMode);
router.route('/').get(authAdmin, settingController.getSystemSetting)

module.exports = router;