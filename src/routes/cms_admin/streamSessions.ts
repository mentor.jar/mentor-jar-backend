import * as streamSessionFunctions from '../../controllers/api/cms_admin/stream_session'
import { authAdmin } from '../../middleware/auth';
import { findStreamSessionCMSMiddleware } from '../../middleware/stream/streamSesstionMiddleware';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .get(authAdmin, streamSessionFunctions.getListStreamSessionByUser);
router.route('/:id')
    .get(authAdmin, findStreamSessionCMSMiddleware, streamSessionFunctions.getStreamSession);
router.route('/viewer/scheduled-streams')
    .get(authAdmin, streamSessionFunctions.getListScheduledStream);

module.exports = router;
