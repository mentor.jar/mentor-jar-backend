import * as categoryFunctions from '../../controllers/api/cms_admin/categories'
import * as categoryInfoFunctions from '../../controllers/api/cms_admin/categories_info'
import { authAdmin } from '../../middleware/auth'
import { categoryInfoCondition, createCategoryInfoMiddleware } from '../../middleware/category/categoryInfoValidationMiddleware'
import { deleteCategoryInfoMiddleware, deleteCategoryMiddleware, findCategoryMiddleware, updateCategoryInfoMiddleware } from '../../middleware/category/categoryMiddleware'
import { categoryCondition, categorySystemCondition, validateCondition, createShopCategoryCMSMiddleware, updateShopCategoryCMSMiddleware } from '../../middleware/category/categoryValidationMiddleware'
import { findShopMiddleware } from '../../middleware/shop/shopMiddleware'

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .get(authAdmin, categoryFunctions.getCategories)
    .post(authAdmin, categorySystemCondition, validateCondition, categoryFunctions.createCategory);

// Shop Category
router.route('/shop-categories')
    .post(authAdmin, categoryCondition, createShopCategoryCMSMiddleware, categoryFunctions.createShopCategoryCMS);

router.route('/shop-categories/:id')
    .get(authAdmin, findCategoryMiddleware, categoryFunctions.getDetailShopCategoryCMS)
    .put(authAdmin, categoryCondition, updateShopCategoryCMSMiddleware, categoryFunctions.updateShopCategoryCMS)
    .delete(authAdmin, findCategoryMiddleware, categoryFunctions.deleteShopCategoryCMS)

router.route('/shop-categories/:id/active')
    .put(authAdmin, categoryFunctions.updateActiveShopCategory)

router.route('/by-shop/:id')
    .get(authAdmin, findShopMiddleware, categoryFunctions.getCategoriesByShop)

router.route('/shop-categories/:id/products')
    .get(authAdmin, findCategoryMiddleware, categoryFunctions.getShopCategoriesById)

// System Category
router.route('/:id')
    .get(authAdmin, categoryFunctions.getCategoriesById)
    .put(authAdmin, categorySystemCondition, validateCondition, categoryFunctions.updateCategory)
    .delete(authAdmin, deleteCategoryMiddleware, categoryFunctions.deleteCategory);

router.route('/:id/category-info')
    .get(authAdmin, categoryInfoFunctions.getCategoriesById)
    .post(authAdmin, findCategoryMiddleware, categoryInfoCondition, createCategoryInfoMiddleware, categoryInfoFunctions.createCategoryInfo)

router.route('/:id/category-info/:idInfo')
    .put(authAdmin, categoryInfoCondition, createCategoryInfoMiddleware, updateCategoryInfoMiddleware, categoryInfoFunctions.updateCategoryInfo)
    .delete(authAdmin, deleteCategoryInfoMiddleware, categoryInfoFunctions.deleteCategoryInfo);

router.route('/:id/active')
    .put(authAdmin, categoryFunctions.updateActiveCategory)

module.exports = router;
