// README: Add suffix 'CMS' after router file name
const appCMS = require('express')();

const categoriesCMS = require("./categories");
const productCMS = require("./products");
const shopCMS = require("./shops");
const bannerCMS = require("./banners");
const voucherCMS = require("./vouchers")
const streamSessionCMS = require("./streamSessions")
const chat = require("./chat")
const orderCMS = require("./orders")
const notificationCMS = require("./notifications")
const shippingMethodCMS = require("./shippingMethods")
const paymentMethodCMS = require("./paymentMethods")
const statisticsCMS = require("./statistics")
const reportLivestreamCMS = require("./reportLivestreams")
const sellerSignUpCMS = require("./sellerSignUp")
const appFeedbackCMS = require("./appFeedback")
const shopMetric = require("./shopMetric")
const emailTemplate = require("./emailTemplate")
const emailMKT = require("./emailList")
const analystEcommerce = require("./analyst")
const settings = require("./settings")
const accountant = require("./accountant")
const groupBuy = require("./group_buy")

appCMS.use('/categories', categoriesCMS)
appCMS.use('/products', productCMS)
appCMS.use('/shops', shopCMS)
appCMS.use('/banners', bannerCMS)
appCMS.use('/vouchers', voucherCMS)
appCMS.use('/stream-sessions', streamSessionCMS)
appCMS.use('/chat', chat)
appCMS.use('/orders', orderCMS)
appCMS.use('/notifications', notificationCMS)
appCMS.use('/shipping-methods', shippingMethodCMS)
appCMS.use('/payment-methods', paymentMethodCMS)
appCMS.use('/statistics', statisticsCMS)
appCMS.use('/report-livestreams', reportLivestreamCMS)
appCMS.use('/seller-register', sellerSignUpCMS)
appCMS.use('/app-feedback', appFeedbackCMS)
appCMS.use('/shop-metric', shopMetric)
appCMS.use('/emails', emailTemplate)
appCMS.use('/mkt-emails', emailMKT)
appCMS.use('/analysts', analystEcommerce)
appCMS.use('/settings', settings)
appCMS.use('/accountant', accountant)
appCMS.use('/group-buy', groupBuy)

module.exports = appCMS;