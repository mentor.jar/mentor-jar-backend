import { auth } from '../../../middleware/auth';
import * as cartFunctions from '../../../controllers/api/mobile_app/buyer/carts';
import { validatorListShop, validationRound } from '../../../middleware/checkout';
import { groupBuyCheckoutMiddleware } from '../../../middleware/carts/cart';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/shop-vouchers')
    .post(auth, validatorListShop, validationRound, cartFunctions.getVoucherByShopWithConditionV2)
router.route('/checkout')
    .post(auth, groupBuyCheckoutMiddleware, cartFunctions.getGroupBuyCheckoutPage)

module.exports = router;