import * as notificationController from '../../controllers/api/mobile_app/buyer/notification';
import { auth } from '../../middleware/auth';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/notifications').get(auth, notificationController.listNotificationV2);

module.exports = router;