import { homeDataV2, getSuggestProductV2, getSuggestProductV3, getProductInternationalV2} from "../../controllers/api/mobile_app/buyer/home";
import { authWeb } from "../../middleware/auth";

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/home')
	.get(authWeb, homeDataV2);

router.route('/suggest-products')
    .get(authWeb, getSuggestProductV3);

router.route('/home/international-products')
    .get(authWeb, getProductInternationalV2)

module.exports = router;