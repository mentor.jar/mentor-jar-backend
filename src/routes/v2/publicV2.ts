import { getAppVersionV2 } from "../../controllers/api/public";

const express = require('express');
const router = express.Router({mergeParams: true});

router.route('/get-version').get(getAppVersionV2)

module.exports = router;