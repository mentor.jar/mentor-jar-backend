import {
    sectionBannerCategories,
    sectionNewestProduct,
    sectionTopKeyword,
    sectionTopLiveStreams,
    sectionTopProduct,
    sectionTopSeller,
    sectionBannerCategoriesV2,
    sectionTopSellerRevise,
    sectionProductsGroupBuy,
} from '../../controllers/api/mobile_app/buyer/homeSplit';
import { authWeb } from '../../middleware/auth';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/home/banner-categories-v2').get(authWeb, sectionBannerCategoriesV2);

router.route('/home/banner-categories').get(authWeb, sectionBannerCategories);

router.route('/home/newest-product').get(authWeb, sectionNewestProduct);

router.route('/home/top-keyword').get(authWeb, sectionTopKeyword);

router.route('/home/top-seller').get(authWeb, sectionTopSeller);

router.route('/home/top-seller-revise').get(authWeb, sectionTopSellerRevise);

router.route('/home/top-product').get(authWeb, sectionTopProduct);

router.route('/home/top-livestreams').get(authWeb, sectionTopLiveStreams);

router.route('/home/group-buy').get(authWeb, sectionProductsGroupBuy);

module.exports = router;
