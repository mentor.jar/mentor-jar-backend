import * as categoryFunctions from "../../controllers/api/mobile_app/categories"
import { authWeb } from "../../middleware/auth";
import { findCategoryMiddleware } from "../../middleware/category/categoryMiddleware";

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/categories/by-system')
	.get(categoryFunctions.getCategoriesBySystemV2);
router.route('/categories/:id/products-of-system-category')
	.get(authWeb, findCategoryMiddleware, categoryFunctions.productByCategoryV2)

module.exports = router;