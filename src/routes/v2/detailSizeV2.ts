import { authShop } from "../../middleware/auth";
import * as productFunctions from '../../controllers/api/mobile_app/products'
import { detailSizeCondition, getDetailSizeMiddleware } from '../../middleware/product/productMiddleware'

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/products/detail-size')
		.post(authShop, detailSizeCondition, getDetailSizeMiddleware, productFunctions.getDetailSizeV2);

module.exports = router;