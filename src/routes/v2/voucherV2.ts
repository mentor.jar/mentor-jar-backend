import * as voucherController from '../../controllers/api/mobile_app/buyer/vouchers';
import { auth } from '../../middleware/auth';


const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/my-budget')
    .get(auth, voucherController.getVoucherBudgetByUserV2)

router.route('/my-budget-revise')
    .get(auth, voucherController.getVoucherBudgetByUserV3)


module.exports = router;
