
import * as job from '../services/cronjob';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.get('/test', async (req, res) => {
    try {
        const { name } = req.query;
        // console.log(name);
        // console.log(job);
        await job[name]();

        res.json({ status: true }).status(200)
    } catch (error) {
        res.json({ error: error.message }).status(400)
    }

});

module.exports = router;
