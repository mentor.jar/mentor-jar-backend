// import { imageMiddleware } from '../middleware/uploadMiddleware'
import { createOrUpdateVideo } from '../controllers/api/videos'
import { auth } from '../middleware/auth'

import uploadVideoFile from '../middleware/uploadVideoMiddleware'

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .post(auth, uploadVideoFile.single("video"), createOrUpdateVideo);

module.exports = router;
