import { Router } from "express";
import { remindLivestreamRunner } from '../services/cronjob/jobs/remindLivestream';

const route = require('express')();

const admin_route = require("./cms_admin/index");
const mobile_route = require("./mobile_app/index");
const web_route = require("./web/index")
const image_route = require("./image")
const authencation = require("./auth")
const job = require("./job")
const video_route = require("./video")
const public_route = require("./public")
const webhook_routes = require("./webhook")

const homeDataV2 = require("./v2/homeDataV2")
const homeSplit = require("./v2/homeSplit")
const notificationsV2 = require("./v2/notificationsV2")
const detailSizeV2 = require("./v2/detailSizeV2")
const categoriesV2 = require("./v2/categoriesV2")
const voucherBuyerV2 = require("./v2/voucherV2")
const publicV2 = require("./v2/publicV2")
const carts = require("./v2/buyer/carts")
const paymentMethodV2 = require('./v2/paymentMethods')

route.use('/api/v1/cms-admin', admin_route)
route.use('/api/v1/mobile', mobile_route)
route.use('/api/v1/web', web_route)
route.use('/api/v1/image', image_route)
route.use('/api/v1/dev/auth', authencation)
route.use('/api/v1/dev/jobs', job)
route.use('/api/v1/video', video_route)
route.use('/api/v1/public', public_route)
route.use('/api/v1/webhook', webhook_routes)

// API version 2
route.use('/api/v2/mobile', homeSplit)
route.use('/api/v2/mobile', homeDataV2)
route.use('/api/v2/mobile', detailSizeV2)
route.use('/api/v2/mobile', categoriesV2)
route.use('/api/v2/mobile/voucher-buyer', voucherBuyerV2)
route.use('/api/v2/mobile', notificationsV2)
route.use('/api/v2/mobile/carts', carts)
route.use('/api/v2/public', publicV2)
route.use('/api/v2/mobile/carts', carts)
route.use("/api/v2/mobile/payment-methods", paymentMethodV2)

route.get('/', (req, res) => res.send('Welcome to E-commerce Project'));

module.exports = route;