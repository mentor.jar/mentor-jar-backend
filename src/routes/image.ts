// import { imageMiddleware } from '../middleware/uploadMiddleware'
import { createOrUpdateImage } from '../controllers/api/images'
import { auth } from '../middleware/auth'
import * as variables from '../base/variable'

import uploadFile from '../middleware/uploadMiddleware'

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/products')
    .post(auth, uploadFile.array("files", variables.MAX_PRODUCT_IMAGE), createOrUpdateImage);

router.route('/')
    .post(auth, uploadFile.single("file"), createOrUpdateImage);

module.exports = router;
