import {
    getSetDataCity,
    cleanDatabase,
    fakeData,
    testChat,
    ChangeStreamURL,
    changeStatusOrderCanceled,
    createOrderShipping,
    getReasons,
    cleanNotification,
    getRejectReason,
    getAppVersion,
    healthCheck,
    syncVouchers
} from '../controllers/api/public';
import { getStreamSession } from '../controllers/api/public_api/stream_session';
import { registerSeller } from '../controllers/api/public_api/seller_sign_up';
import { signUpSellerCondition, registerSellerMiddleware, validateAddress, validateExistPhoneAndEmail, validateBankInfoMiddleware } from '../middleware/user/sellerSignUpMiddleware';
import { authWeb } from '../middleware/auth';
import { appFeedbackConditions, appFeedbackMiddleware } from '../middleware/appFeedbackMiddleware';
import { createAppFeedback } from '../controllers/api/public_api/app_feedback';
import { getUserAndShop } from '../controllers/api/public_api/user_and_shop';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/get-data-city').get(getSetDataCity);

router.route('/clean-database').post(cleanDatabase);

router.route('/fake-data').post(fakeData);

router.route('/stream-sessions/:id').get(getStreamSession);

router.route('/testChat').get(testChat);

router.route('/change-url-live').get(ChangeStreamURL);

router.route('/change-order-type').get(changeStatusOrderCanceled);

router.route('/create-order-shipping').get(createOrderShipping);

router.route('/reasons').get(getReasons);

router.route('/reject-reasons').get(getRejectReason);

router.route('/clean-notification').get(cleanNotification);

router.route('/get-version').get(getAppVersion)

router.route('/user-and-shop').get(getUserAndShop)

// Seller register form
router.route('/seller-register').post(authWeb, signUpSellerCondition, registerSellerMiddleware, validateAddress, validateExistPhoneAndEmail, registerSeller)

// App feedback
router.route('/app-feedback').post(authWeb, appFeedbackConditions, appFeedbackMiddleware, createAppFeedback)

router.route('/health-check').get(authWeb, healthCheck)

router.route('/sync-vouchers').get(syncVouchers)

module.exports = router;
