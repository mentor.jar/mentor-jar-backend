import * as webhookFunctions from '../controllers/api/public_api/webhook';
import { connectWebhookGHTKMiddleware } from '../middleware/public/webhookMiddleware';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/GHTK/update-shipment')
    .post(connectWebhookGHTKMiddleware, webhookFunctions.updateStatusOrderGHTK)

router.route('/VIETTEL-POST/update-shipment')
    .post(webhookFunctions.updateStatusOrderViettelPost)

module.exports = router;
