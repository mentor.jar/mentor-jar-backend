import * as shippingMethodController from '../../controllers/api/mobile_app/shipping_method';
import { auth } from '../../middleware/auth';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .get(auth, shippingMethodController.getAllMethods)
module.exports = router;
