import * as groupBuyFunctions from "../../controllers/api/mobile_app/group_buy"
import { authShop } from "../../middleware/auth";
import { createGroupBuyRule, createGroupBuyValidate, validateTimeCreate, validateGroupsProduct, validateQuantityProduct } from "../../middleware/groupBuy/groupBuyMiddleware";
const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .post(authShop, validateTimeCreate, validateQuantityProduct, validateGroupsProduct, groupBuyFunctions.createGroupBuy)
    .get(groupBuyFunctions.getGroupBuyProductWithFilter)

router.route('/:id')
    .put(authShop, validateTimeCreate, validateQuantityProduct, validateGroupsProduct, groupBuyFunctions.updateGroupBuy)
    .delete(authShop, groupBuyFunctions.deleteGroupBuy)

router.route('/group-buy-in-week')
    .get(groupBuyFunctions.getGroupBuyProductInWeek)

router.route('/categories').get(groupBuyFunctions.getListCategoryHaveGroupBuyProduct)

router.route('/products').get(authShop, groupBuyFunctions.getValidGroupBuyProductByShopId)

module.exports = router