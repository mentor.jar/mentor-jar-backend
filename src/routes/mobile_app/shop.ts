import { auth, authShop } from '../../middleware/auth'
import * as shopFunctions from '../../controllers/api/mobile_app/shop'
import { activeShippingMethodValidate, shopMiddleware, findShopMiddleware, shopSettingValidate, checkShopAccountExistMiddleware, updateCountryRule, updateCountryMiddleware, shopPauseModeValidation, checkShippingMethodsMiddleware } from '../../middleware/shop/shopMiddleware';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route("/")
    .get(auth, shopFunctions.getListShop)
    .post(authShop, shopFunctions.fakeCreateShop)
router.route('/statistics')
    .get(authShop, findShopMiddleware, shopFunctions.generalStatistics)
router.route('/setting')
    .post(authShop, findShopMiddleware, shopSettingValidate, shopFunctions.settingShop);
router.route('/user-trends')
    .get(authShop, findShopMiddleware, shopFunctions.getUserTrends)
router.route('/refund-conditions')
    .put(authShop, findShopMiddleware, shopFunctions.settingRefundConditions)
router.route('/init-shop-country').post(authShop, shopFunctions.initShopCountry);
router.route('/update-country')
    .put(authShop, updateCountryRule, shopMiddleware, updateCountryMiddleware, shopFunctions.updateShopCountry);
router.route('/:id')
    .get(auth, findShopMiddleware, shopFunctions.getDetailShop);
router.route('/active/:id')
    .put(authShop, checkShopAccountExistMiddleware, activeShippingMethodValidate, shopMiddleware, checkShippingMethodsMiddleware, shopFunctions.activeShippingMethodForShop);
router.route('/pause-mode')
    .patch(authShop, findShopMiddleware, shopPauseModeValidation, shopFunctions.pauseMode);

module.exports = router;
