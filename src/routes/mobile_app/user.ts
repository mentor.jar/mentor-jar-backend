import * as userFunctions from '../../controllers/api/mobile_app/user';
import { auth } from '../../middleware/auth';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/update-redis').post(auth, userFunctions.updateUserRedis);

router.route('/update-language').post(auth, userFunctions.updateLanguage);

module.exports = router;
