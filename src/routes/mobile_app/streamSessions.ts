import * as streamSessionFunctions from '../../controllers/api/mobile_app/stream_session'
import { auth, authShop, authWeb } from '../../middleware/auth';
import { createReportLivestreamMiddleware, createStreamSessionRule, createStreamSessionValidate, findStreamSessionMiddleware, validateTime } from '../../middleware/stream/streamSesstionMiddleware';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .post(authShop, createStreamSessionRule, createStreamSessionValidate, validateTime, streamSessionFunctions.createStreamSession)
    .get(authShop, streamSessionFunctions.getListStreamSessionByUser);
router.route('/:id')
    .get(authShop, findStreamSessionMiddleware, streamSessionFunctions.getStreamSession);
router.route('/:id/products')
    .put(authShop, findStreamSessionMiddleware, streamSessionFunctions.updateProductStreamSession);
router.route('/:id/start')
    .put(authShop, findStreamSessionMiddleware, streamSessionFunctions.startStreamSession);
router.route('/:id/stop')
    .put(authShop, findStreamSessionMiddleware, streamSessionFunctions.stopStreamSession);
router.route('/:id/report')
    .post(auth, createReportLivestreamMiddleware, streamSessionFunctions.reportStreamSession);
router.route('/viewer/report-reasons')
    .get(auth, streamSessionFunctions.getListReportReason);
router.route('/viewer/all')
    .get(authWeb, streamSessionFunctions.getListStreamSessionByViewer);
router.route('/viewer/scheduled-streams')
    .get(auth, streamSessionFunctions.getListScheduledStream);

module.exports = router;
