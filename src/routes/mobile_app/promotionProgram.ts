import { authShop } from '../../middleware/auth'
import * as promotionFunctions from '../../controllers/api/mobile_app/promotion_program'
import { createPromotionProgramRule, updatePromotionProgramRule, createPromotionProgramValidate, updatePromotionProgramValidate, validateProductInPromotionProgram, validateTimeCreate, validateUniqueProductOnCreate, validateUniqueProductOnUpdate, findPromotionForEnd, checkOwnerPromotion, validateTimeUpdate, checkBeforeDelete } from '../../middleware/promotionProgram/promotionProgram';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route("/")
    .post(authShop, createPromotionProgramRule, createPromotionProgramValidate, validateTimeCreate, validateProductInPromotionProgram, validateUniqueProductOnCreate, promotionFunctions.createPromotion)
    .get(authShop, promotionFunctions.getlPromotionWithFilter);
router.route("/products")
    .get(authShop, promotionFunctions.getProductValidForPromotion)
router.route("/:id")
    .delete(authShop, checkOwnerPromotion, checkBeforeDelete, promotionFunctions.deletePromotionProgram)
    .get(authShop, checkOwnerPromotion, promotionFunctions.getDetailPromotionProgram)
    .put(authShop, checkOwnerPromotion, updatePromotionProgramRule, updatePromotionProgramValidate, validateTimeUpdate, validateProductInPromotionProgram, validateUniqueProductOnUpdate, promotionFunctions.updatePromotion)
router.route('/:id/end')
    .put(authShop, findPromotionForEnd, promotionFunctions.endPromotionProgram)

module.exports = router;
