import * as streamFunctions from '../../controllers/api/mobile_app/streams'
import { auth, authShop } from '../../middleware/auth';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .post(authShop, streamFunctions.createStream)
    .get(authShop, streamFunctions.getListStream);
router.route('/:id')
    .get(authShop, streamFunctions.getStream);
router.route('/connect')
    .put(authShop, streamFunctions.getListStream);

module.exports = router;
