// import { imageMiddleware } from '../middleware/uploadMiddleware'
import { authShop, authWeb } from '../../middleware/auth'
import { productCondition, createProductMiddleware, findProductMiddleware, productUpdateCondition, detailSizeCondition, 
    getDetailSizeMiddleware, requireSizeInfoInDetailSize, validateQuantityProduct, findProductBuyerMiddleware, updatePriceProductMiddleware, checkShippingMethodsMiddleware } from '../../middleware/product/productMiddleware'
import * as productFunctions from '../../controllers/api/mobile_app/products'

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .post(authShop, productCondition, createProductMiddleware, validateQuantityProduct, requireSizeInfoInDetailSize, checkShippingMethodsMiddleware, productFunctions.createProduct);
   
router.get('/my-shop', authShop, productFunctions.getProductByShopId);

router.get('/valid-for-shop', authShop, productFunctions.getValidProductByShopId)

router.post('/detail-size', authShop, detailSizeCondition, getDetailSizeMiddleware, productFunctions.getDetailSize);

router.get('/shape-history/:category_id?', authShop, productFunctions.getShapeHistory);

router.get('/category-info-historty/:category_id', authShop, productFunctions.getProductCategoryInfoHistory)

router.route('/:id')
    .get(authShop, findProductMiddleware, productFunctions.getProductDetail)
    .put(authShop, productUpdateCondition, createProductMiddleware, validateQuantityProduct, requireSizeInfoInDetailSize, findProductMiddleware, updatePriceProductMiddleware,
        productFunctions.updateProduct)
    .delete(authShop, findProductMiddleware, productFunctions.deleteProduct);

router.get('/:id/similar', authWeb, findProductBuyerMiddleware, productFunctions.getSimilarProductsV2);


router.route('/:id/active')
    .put(authShop, findProductMiddleware, productFunctions.showHideProduct);


module.exports = router;
