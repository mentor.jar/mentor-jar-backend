import * as bannerController from '../../controllers/api/mobile_app/banners';
import { authShop } from '../../middleware/auth';
import {
  createBannerRule, createBannerValidate,
  updateBannerRule, updateBannerValidate,
  isBannerOwner, validateTimeCreate, validateTimeUpdate, validateProduct
} from '../../middleware/banner/bannerMiddleware';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
  .get(authShop, bannerController.listBanners)
  .post(authShop,
    createBannerRule, createBannerValidate,
    validateProduct, validateTimeCreate,
    bannerController.createBanner)

router.route('/:id')
  .get(authShop, isBannerOwner, bannerController.getBannerDetail)
  .put(
    authShop, isBannerOwner,
    updateBannerRule, updateBannerValidate,
    validateProduct, validateTimeUpdate,
    bannerController.updateBanner
  )
  .delete(authShop, isBannerOwner,
    bannerController.deleteBanner)

module.exports = router;
