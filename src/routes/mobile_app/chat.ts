import * as ChatFunctions from '../../controllers/api/mobile_app/chat'
import { auth } from '../../middleware/auth';
import { createRoomValidate } from '../../middleware/roomChat/chat';
const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .get(auth, ChatFunctions.getListRoomChatOfUser)
router.route('/')
    .post(auth, createRoomValidate, ChatFunctions.createRoomChat)
router.route('/suggest-chat')
    .get(auth, ChatFunctions.suggestChat)
router.route('/total-red-dot')
    .get(auth, ChatFunctions.getRedDot)
router.route('/:id')
    .get(auth, ChatFunctions.getDetailRoomChat)
router.route('/hide-room-chat/:id')
    .post(auth, ChatFunctions.hideRoomChat)

router.route('/add-message-chat/:id')
    .post(auth, ChatFunctions.addMessageChat)

module.exports = router;
