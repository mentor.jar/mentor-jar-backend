import * as trackingFunctions from '../../controllers/api/mobile_app/tracking'
import { auth } from '../../middleware/auth';
import { validateUserInteractiveProduct, validateTrackingObject } from '../../middleware/tracking';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/user-interactive-product')
    .post(auth, validateUserInteractiveProduct, trackingFunctions.userInteractiveProduct)

// Do not remove
// router.route('/read-action-change')
//     .patch(auth, validateTrackingObject, trackingFunctions.userReadActionChange)

module.exports = router;
