import * as addressController from '../../controllers/api/mobile_app/buyer/addresses';
import { authShop } from '../../middleware/auth';
import { checkOrderOfShop } from '../../middleware/order/orderMiddleware';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/:id/address-valid')
    .get(authShop, checkOrderOfShop, addressController.getAddressValidForSeller)
router.route('/pick-address-default')
    .get(authShop, addressController.getPickAddressDefaultOfShop)

module.exports = router;
