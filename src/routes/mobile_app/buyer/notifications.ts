import * as notificationController from '../../../controllers/api/mobile_app/buyer/notification';
import { auth } from '../../../middleware/auth';
import { findShopMiddleware } from '../../../middleware/shop/shopMiddleware';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .get(auth, notificationController.listNotification);

router.route('/new')
    .get(auth, notificationController.topNew);

router.route('/test')
    .post(auth, notificationController.sendTest);

router.route('/mask-read')
    .put(auth, notificationController.markRead);

router.route('/unread-count')
    .get(auth, notificationController.unreadCount);

module.exports = router;
