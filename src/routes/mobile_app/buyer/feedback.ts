import { auth, authShop, authWeb } from '../../../middleware/auth'
import * as feedbackFunctions from '../../../controllers/api/mobile_app/buyer/feedbacks'
import { feedbackCondition, filterMiddleware, checkProductBeforeFeedback, checkOrderBeforeFeedback, findFeedbackMiddleware, findUserReplyFeedbackMiddleware, checkTimeFeedback, findFeedbackMiddlewareToUpdate } from '../../../middleware/feedback/feedbackMiddleware'

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .post(auth, feedbackCondition, checkProductBeforeFeedback, checkTimeFeedback, feedbackFunctions.createFeedback)
router.route('/:id')
    .put(auth, findFeedbackMiddlewareToUpdate, feedbackFunctions.updateFeedback)
router.route('/user')
    .post(authShop, feedbackCondition, checkOrderBeforeFeedback, feedbackFunctions.createFeedbackUser)
router.route('/:id/reply')
    .post(authShop, findFeedbackMiddleware, feedbackFunctions.shopReplyFeedback)
router.route('/:id/user-reply')
    .post(auth, findUserReplyFeedbackMiddleware, feedbackFunctions.userReplyFeedback)
router.route('/:id/toggle-like')
    .post(auth, feedbackFunctions.toggleLikeFeedback)
router.route('/:product_id')
    .get(authWeb, filterMiddleware, feedbackFunctions.getFeedbackByProductWithFilter);
router.route('/shop/:shop_id')
    .get(authWeb, filterMiddleware, feedbackFunctions.getFeedbackByShopWithFilter);

module.exports = router;
