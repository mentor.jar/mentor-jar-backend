import * as userFunctions from '../../../controllers/api/mobile_app/user';
import { auth } from '../../../middleware/auth';
import { verifyUserMiddleware } from '../../../middleware/user/userMiddleware';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/send-verify')
    .post(auth, verifyUserMiddleware, userFunctions.sendVerifyToUser)
router.route('/verify')
    .post(auth, userFunctions.verify)
router.route('/verify-status')
    .get(auth, userFunctions.getVerifyStatus)
router.route('/bookmark')
    .get(auth, userFunctions.getProductsBookmark)

module.exports = router;
