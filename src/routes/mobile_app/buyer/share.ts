// import { imageMiddleware } from '../middleware/uploadMiddleware'
import { auth, authWeb } from '../../../middleware/auth'
import { findProductBuyerMiddleware } from '../../../middleware/product/productMiddleware'
import * as shareFunctions from '../../../controllers/api/mobile_app/buyer/share'

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .post(authWeb, shareFunctions.getRealID)

module.exports = router;
