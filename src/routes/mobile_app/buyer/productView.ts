// import { imageMiddleware } from '../middleware/uploadMiddleware'
import { auth, authWeb } from '../../../middleware/auth'
import { findProductBuyerMiddleware } from '../../../middleware/product/productMiddleware'
import * as productFunctions from '../../../controllers/api/mobile_app/products'

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/keyword')
    .get(authWeb, productFunctions.getProductByKeyword)
router.route('/bookmarked')
    .get(auth, productFunctions.getValidBookmarkedProduct)
router.route('/all')
    .get(authWeb, productFunctions.getAllActiveProduct)
router.route('/:id')
    .get(authWeb, findProductBuyerMiddleware, productFunctions.getDetailWhenUserGoToShop)
router.route('/:id/toogleBookmark')
    .post(auth, findProductBuyerMiddleware, productFunctions.toogleBookmarkProduct)
router.route('/')
    .get(authWeb, productFunctions.getOtherProductOfShop)

module.exports = router;
