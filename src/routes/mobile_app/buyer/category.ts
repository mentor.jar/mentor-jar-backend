import { auth, authWeb } from '../../../middleware/auth'
import { findCategoryMiddleware } from '../../../middleware/category/categoryMiddleware'
import * as productFunctions from '../../../controllers/api/mobile_app/products'

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/:id/products')
    .get(authWeb, findCategoryMiddleware, productFunctions.getProductByCatgoryId)

module.exports = router;
