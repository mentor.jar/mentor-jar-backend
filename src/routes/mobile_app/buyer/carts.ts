import { auth } from '../../../middleware/auth'
import { findVariantBuyerMiddleware, checkBeforeUpdateVariant } from '../../../middleware/product/productMiddleware'
import { checkoutMiddleware, validateQuantity, checkAndFormatBeforeGet, checkOwnerCartItem, checkCountryShop, blockOrderByTime, groupBuyCheckoutMiddleware, validatorListShop, validationRound } from '../../../middleware/carts/cart'
import * as cartFunctions from '../../../controllers/api/mobile_app/buyer/carts'

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .post(auth, checkCountryShop, validateQuantity, findVariantBuyerMiddleware, cartFunctions.addProductToCart)
    .get(auth, cartFunctions.viewCart)
router.route('/ordered-products')
    .get(auth, cartFunctions.getOrderedProducts)
router.route('/shop-vouchers')
    .post(auth, cartFunctions.getVoucherByShopWithCondition)
router.route('/preview-shop')
    .post(auth, validatorListShop, validationRound, cartFunctions.getPreviewShopWithCondition)
router.route('/preview-system-voucher')
    .post(auth, cartFunctions.getPreviewSystemVoucher)
router.route('/checkout')
    .post(auth, checkoutMiddleware, cartFunctions.getCheckoutPage)
// router.route('/system-vouchers')
//     .post(auth, checkAndFormatBeforeGet, cartFunctions.getSystemVoucher)
router.route('/new-system-vouchers')
    .post(auth, checkAndFormatBeforeGet, cartFunctions.getSystemVoucher)
router.route('/user-system-vouchers')
    .post(auth, checkAndFormatBeforeGet, cartFunctions.getSystemVoucherNewFlow)
router.route('/shipping-method-default/:shopId')
    .get(auth, cartFunctions.getDefaultShippingMethod)
router.route('/payment-method-default')
    .get(auth, cartFunctions.getDefaultPaymentMethod)
router.route('/:id/quantities')
    .put(auth, checkOwnerCartItem, validateQuantity, cartFunctions.changeQuantity)
router.route('/:id/change-variants')
    .patch(auth, checkOwnerCartItem, checkBeforeUpdateVariant, cartFunctions.changeVariant)
router.route('/:id')
    .delete(auth, checkOwnerCartItem, cartFunctions.deleteProductFromCart)

router.route('/group-buy')
    .post(auth, groupBuyCheckoutMiddleware, cartFunctions.getGroupBuyCheckoutPage)

module.exports = router;
