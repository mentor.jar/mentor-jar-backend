import * as shopExploreController from '../../../controllers/api/mobile_app/buyer/shop_explore';
import { auth, authWeb } from '../../../middleware/auth';
import { findShopMiddleware } from '../../../middleware/shop/shopMiddleware';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/top-shop')
    .get(authWeb, shopExploreController.getTopShop);

router.route('/:id')
    .get(authWeb, findShopMiddleware, shopExploreController.getStaticSection);
router.route('/:id/categories')
    .get(authWeb, findShopMiddleware, shopExploreController.getShopSection);

router.route('/:id/products')
    .get(authWeb, findShopMiddleware, shopExploreController.getShopProductsV2);

router.route('/:id/overview-products')
    .get(authWeb, findShopMiddleware, shopExploreController.getOverviewProductsByShop);

router.route('/:id/suggest-products')
    .get(authWeb, findShopMiddleware, shopExploreController.getSuggestProductsByShop);

router.route('/:id/products-type')
    .get(authWeb, findShopMiddleware, shopExploreController.getProductsByType);

router.route('/:id/all-categories')
    .get(authWeb, findShopMiddleware, shopExploreController.getShopCategories);

router.route('/:id/search')
    .get(authWeb, findShopMiddleware, shopExploreController.searchShopProducts);

router.route('/:id/popular-search')
    .get(authWeb, findShopMiddleware, shopExploreController.popularSearchShop);

router.route('/:id/top-products')
    .get(authWeb, findShopMiddleware, shopExploreController.getTopProductOfShop);

router.route('/:id/vouchers')
    .get(authWeb, findShopMiddleware, shopExploreController.showVouchersShop);

router.route('/:id/banner')
    .patch(authWeb, findShopMiddleware, shopExploreController.updateBannerShop)

module.exports = router;
