import * as homeFunctions from '../../../controllers/api/mobile_app/buyer/home';
import { authWeb } from '../../../middleware/auth';
import { findCategoryMiddleware } from '../../../middleware/category/categoryMiddleware';

const express = require('express');
const router = express.Router({ mergeParams: true });

// router.route('/')
//     .get(authWeb, homeFunctions.homeData)

router.route('/ranking')
    .get(authWeb, homeFunctions.rankingPage)

router.route('/search')
    .get(authWeb, homeFunctions.searchV2);

router.route('/search-seller')
    .get(authWeb, homeFunctions.searchSeller);

router.route('/search-user')
    .get(authWeb, homeFunctions.searchUser);

router.route('/suggest-keyword-searching')
    .get(authWeb, homeFunctions.suggestKeywordWhenSearching)

router.route('/popular-search')
    .get(authWeb, homeFunctions.popularSearch);

router.route('/suggest-products')
    .get(authWeb, homeFunctions.getSuggestProduct);

router.route('/newest-products')
    .get(authWeb, homeFunctions.getNewestProductV2)

router.route('/search-detail')
    .get(authWeb, homeFunctions.getSearchDetail);

router.route('/:id/products-of-system-category')
    .get(authWeb, findCategoryMiddleware, homeFunctions.getListProductCategorySystem);

router.route('/banners/:id')
    .get(authWeb, homeFunctions.bannerSystemDetail);

router.route('/banners-detail/:id')
    .get(authWeb, homeFunctions.bannerSystemDetailV2);

router.route('/promotions')
    .get(authWeb, homeFunctions.getPromotions)

router.route('/international-products')
    .get(authWeb, homeFunctions.getProductInternational)

router.route('/ranking-seller')
    .get(authWeb, homeFunctions.rankingSeller)

module.exports = router;
