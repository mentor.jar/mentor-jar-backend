import { auth } from '../../../middleware/auth'
import * as paymentFunctions from '../../../controllers/api/mobile_app/buyer/payments'
import { findOrderMiddleware } from '../../../middleware/payment/paymentMiddleware';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/create-payment-url')
    .post(auth, paymentFunctions.createUrlPayment)

router.route('/momo/ipn-url')
    .post(paymentFunctions.IPNUrlMomoPayment)
router.route('/momo/return-url')
    .get(paymentFunctions.returnUrlMomoPayment)
router.route('/momo/refund-money')
    .post(paymentFunctions.refundTransaction)

router.route('/vnpay/return-url')
    .get(paymentFunctions.getReturnPaymentUrl);
router.route('/vnpay/ipn-url')
    .get(paymentFunctions.getReturnPaymentIpn);

module.exports = router;
