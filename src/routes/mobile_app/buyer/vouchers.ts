import * as voucherController from '../../../controllers/api/mobile_app/buyer/vouchers';
import { auth, authWeb } from '../../../middleware/auth';
import { findVoucherMiddleware } from '../../../middleware/voucher/voucher';


const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .get(auth, voucherController.getListVoucherSystem);
router.route('/my-budget')
    .get(auth, voucherController.getVoucherBudgetByUser)
router.route('/:id')
    .get(authWeb, voucherController.detailVoucherBuyer)
router.route('/:id/save')
    .post(auth, findVoucherMiddleware, voucherController.saveVoucherToUserBudget)

module.exports = router;
