import * as addressController from '../../../controllers/api/mobile_app/buyer/addresses';
import {
    createAddressRule,
    createAddressValidate,
    updateAddressRule,
    updateAddressValidate,
    phoneValidate,
    deleteAddressValidate,
    updateAddressMiddlewware,
    objectAddressValidate,
    checkTypeMiddleware
} from '../../../middleware/address/addressMiddleware';
import { auth, authWeb } from '../../../middleware/auth';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .get(auth, addressController.getAddressByUser)
    .post(auth, createAddressRule, createAddressValidate, objectAddressValidate, phoneValidate, checkTypeMiddleware, addressController.createAddress);
router.route('/check-address')
    .get(auth, addressController.checkAddressUser)
router.route('/:id')
    .put(auth, updateAddressRule, updateAddressValidate, objectAddressValidate, phoneValidate, checkTypeMiddleware, updateAddressMiddlewware, addressController.updateAddress)
    .delete(auth, deleteAddressValidate, addressController.deleteAddress);
router.route('/data/all')
    .get(authWeb, addressController.getAddressData)

module.exports = router;
