import { auth } from '../../../middleware/auth'
import * as orderFunctions from '../../../controllers/api/mobile_app/buyer/orders'
import {
    orderMiddleware,
    createRequestReason,
    checkValidToCancelRefundByBuyer,
    filterTypeRequire,
    checkValidToCancel,
    findOrderBuyerMiddleware,
    findOrderForRefundMiddleware,
    checkValidToCreateOrder,
    ConfirmShippedMiddleware,
    checkValidVoucher
} from '../../../middleware/order/orderMiddleware';
import { checkCountryShop } from '../../../middleware/carts/cart';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .get(auth, filterTypeRequire, orderFunctions.getOrderWithFilter)
    .post(auth, checkCountryShop, checkValidToCreateOrder, checkValidVoucher, orderFunctions.createOrderBuyer);

router.route('/search')
    .get(auth, orderFunctions.searchOrder)

router.route('/process')
    .get(auth, orderFunctions.getOrderProcess)

router.route('/:id')
    .get(auth, findOrderBuyerMiddleware, orderFunctions.getDetailOrder);

router.route('/:id/cancel')
    .patch(auth, checkValidToCancel, orderFunctions.cancelOrder)

router.route('/:id/confirm-shipped')
    .patch(auth, ConfirmShippedMiddleware, orderFunctions.confirmShipped)

router.route('/:id/feedbacks')
    .get(auth, orderFunctions.getFeedbackOfOrder)

router.route('/:id/refund')
    .post(auth, createRequestReason, orderMiddleware, findOrderForRefundMiddleware, orderFunctions.createRefundForBuyer);

router.route('/:id/refund/cancel')
    .patch(auth, checkValidToCancelRefundByBuyer, orderFunctions.cancelRefundForBuyer);

module.exports = router;
