import * as referralController from '../../../controllers/api/mobile_app/buyer/referral';
import { auth } from '../../../middleware/auth';


const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/get-code')
    .get(auth, referralController.getReferralCode);

router.route('/use-code')
    .post(auth, referralController.useReferralCode);

module.exports = router;
