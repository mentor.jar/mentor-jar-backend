import { authShop } from '../../middleware/auth'
import { approveRejectOrderRule, approveRejectOrderValidate, ConfirmShippedSellerMiddleware, createOrderSellerRule, filterTypeRequire, isShopOwnerOrder, orderMiddleware } from '../../middleware/order/orderMiddleware'
import * as orderFunctions from '../../controllers/api/mobile_app/orders'
import * as orderBuyerFunctions from '../../controllers/api/mobile_app/buyer/orders'
import { findOrderMiddleware, checkValidToCancelSeller, checkValidToConfirmCancel, checkValidOrderSellerNote, approveRejectOrderRefundMiddleware, confirmReceivedBySellerMiddleware } from '../../middleware/order/orderMiddleware';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/search')
    .get(authShop, orderFunctions.searchOrder);

router.route('/:id')
    .get(authShop, findOrderMiddleware, orderFunctions.getDetailOrder);

router.route('/')
    .get(authShop, filterTypeRequire, orderFunctions.getOrderWithFilter);

router.route('/create-order')
    .post(authShop, createOrderSellerRule, orderMiddleware, isShopOwnerOrder, orderFunctions.sellerCreateOrder);
router.route('/:id/feedbacks')
    .get(authShop, orderFunctions.getFeedbackOfOrder)
router.route('/:id/cancel')
    .patch(authShop, checkValidToCancelSeller, orderFunctions.cancelOrderSeller)
router.route('/:id/confirm-cancel')
    .patch(authShop, checkValidToConfirmCancel, orderFunctions.confirmCancelOrder)
router.route('/:id/seller-note')
    .patch(authShop, checkValidOrderSellerNote, orderFunctions.changeSellerNote)
router.route('/:id/confirm-shipped')
    .patch(authShop, ConfirmShippedSellerMiddleware, orderFunctions.confirmShippedBySeller)

router.route('/:id/shipping-history')
    .get(authShop, orderFunctions.shippingHistory)

router.route('/:id/refund/approve-reject')
    .put(authShop, approveRejectOrderRule, approveRejectOrderValidate, approveRejectOrderRefundMiddleware, orderFunctions.approveRejectRefundRequest);

router.route('/:id/confirm-received')
    .patch(authShop, confirmReceivedBySellerMiddleware, orderFunctions.confirmReceivedBySeller)

module.exports = router;
