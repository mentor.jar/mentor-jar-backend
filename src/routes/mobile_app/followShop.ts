import * as followShopController from '../../controllers/api/mobile_app/follow_shop';
import { auth } from '../../middleware/auth';
import { findShopMiddleware } from '../../middleware/shop/shopMiddleware';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .post(auth, findShopMiddleware, followShopController.createFollowShop)


module.exports = router;
