import * as voucherFunctions from '../../controllers/api/mobile_app/vouchers'
import { authWeb, authShop } from '../../middleware/auth';
import { voucherCondition, createVoucherMiddleware, updateVoucherMiddleware, validateVoucherCode } from '../../middleware/voucher/voucher'

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .post(authShop, voucherCondition, createVoucherMiddleware, validateVoucherCode, voucherFunctions.createVoucher)
    .get(authShop, voucherFunctions.getlVoucherWithFilter);
router.route('/:id')
    .get(authWeb, voucherFunctions.getDetailVoucher)
    .put(authShop, voucherCondition, updateVoucherMiddleware, validateVoucherCode, voucherFunctions.updateVoucher)
    .delete(authShop, voucherFunctions.deleteVoucher)
router.route('/:id/end')
    .put(authShop, voucherFunctions.endVoucher)

module.exports = router;
