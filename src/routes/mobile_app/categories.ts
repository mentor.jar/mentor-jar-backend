import * as categoryFunctions from '../../controllers/api/mobile_app/categories'
import * as categoryInfoFunctions from '../../controllers/api/mobile_app/categories_info'
import { authShop } from '../../middleware/auth'
import { checkShopIdMiddleware } from '../../middleware/category/categoryMiddleware'
import { categoryCondition, validateCondition, validateBeforeSort } from '../../middleware/category/categoryValidationMiddleware'

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
  .get(authShop, categoryFunctions.getAllCategories)
  .post(authShop, categoryCondition, validateCondition, categoryFunctions.createCategory);
router.post('/sort', authShop, validateBeforeSort, validateCondition, categoryFunctions.sortCategory)
router.get('/by-shop', authShop, categoryFunctions.getCategoriesByShop);
router.get('/by-system/', categoryFunctions.getCategoriesBySystem);
router.get('/by-system/search', categoryFunctions.searchCategoryBySystem);

router.route('/:id')
  .get(authShop, categoryFunctions.getCategoriesById)
  .put(authShop, checkShopIdMiddleware, categoryCondition, validateCondition, categoryFunctions.updateCategory)
  .delete(authShop, checkShopIdMiddleware, categoryFunctions.deleteCategory);

router.route('/:id/category-info')
  .get(authShop, categoryInfoFunctions.getCategoriesById)

router.route('/:id/active')
  .put(authShop, checkShopIdMiddleware, categoryFunctions.updateActiveCategory)

module.exports = router;
