import { authShop } from '../../middleware/auth'
import * as shopStatisticsFunctions from '../../controllers/api/mobile_app/shop_statistics'


const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/system-categories-of-shop')
    .get(authShop, shopStatisticsFunctions.getListSystemCategoriesOfShop);

router.route('/user-trends')
    .get(authShop, shopStatisticsFunctions.getUserTrends);

router.route('/index-general-and-chart')
    .get(authShop, shopStatisticsFunctions.generalStatistics);
router.route('/product-ranking')
    .get(authShop, shopStatisticsFunctions.productRanking);

module.exports = router;