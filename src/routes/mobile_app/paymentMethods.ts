import * as paymentMethodController from '../../controllers/api/mobile_app/payment_method';
import { auth } from '../../middleware/auth';

const express = require('express');
const router = express.Router({ mergeParams: true });

router.route('/')
    .get(auth, paymentMethodController.getAllMethods)
module.exports = router;
