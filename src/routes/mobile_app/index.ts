import { localeMiddleware } from '../../middleware/localeMiddleware';

const app = require('express')();

const categories = require('./categories');
const products = require('./products');
const streams = require('./streams');
const banners = require('./banners');
const streamSessions = require('./streamSessions');
const vouchers = require('./voucher');
const followShops = require('./followShop');
const shops = require('./shop');
const shopExplores = require('./buyer/shopExplore');
const home = require('./buyer/home');
const productExplores = require('./buyer/productView');
const chat = require('./chat');
const notificationBuyer = require('./buyer/notifications');
const carts = require('./buyer/carts');
const sellOrders = require('./orders');
const addresses = require('./buyer/addresses');
const categoryBuyer = require('./buyer/category');
const voucherBuyer = require('./buyer/vouchers');
const referralBuyer = require('./buyer/referral');
const orderBuyer = require('./buyer/orders');
const shopStatistics = require('./shopStatistics');
const feedbackBuyer = require('./buyer/feedback');
const payments = require('./buyer/payments');
const shippingMethods = require('./shippingMethods');
const trackings = require('./trackings');
const paymentMethods = require('./paymentMethods');
const buyers = require('./buyer/user');
const user = require('./user');
const promotionProgram = require('./promotionProgram')
const addressesSeller = require('./addresses');
const shareRouter = require('./buyer/share')
const groupBuy = require('./group_buy')

app.use(localeMiddleware);
app.use('/categories', categories);
app.use('/products', products);
app.use('/banners', banners);
app.use('/livestreams', streams);
app.use('/stream-sessions', streamSessions);
app.use('/vouchers', vouchers);
app.use('/follow-shops', followShops);
app.use('/shops', shops);
app.use('/shop-explores', shopExplores);
app.use('/home', home);
app.use('/product-explores', productExplores);
app.use('/category-buyer', categoryBuyer);
app.use('/chat', chat);
app.use('/orders-seller', sellOrders);
app.use('/notification-buyer', notificationBuyer);
app.use('/carts', carts);
app.use('/addresses', addresses);
app.use('/voucher-buyer', voucherBuyer);
app.use('/referral', referralBuyer);
app.use('/orders', orderBuyer);
app.use('/shop-statistics', shopStatistics);
app.use('/feedbacks', feedbackBuyer);
app.use('/payments', payments);
app.use('/shipping-methods', shippingMethods);
app.use('/trackings', trackings);
app.use('/payment-methods', paymentMethods);
app.use('/buyers', buyers);
app.use('/user', user);
app.use('/promotions', promotionProgram)
app.use('/addresses/seller', addressesSeller)
app.use('/shares', shareRouter)
app.use('/group-buy', groupBuy)

module.exports = app;
