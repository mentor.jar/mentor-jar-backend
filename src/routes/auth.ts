// import { imageMiddleware } from '../middleware/uploadMiddleware'

const express = require('express');
const router = express.Router({ mergeParams: true });
import { login } from '../controllers/api/auth'
router.route('/login')
    .post(login);

module.exports = router;
