
export const filterShop = (req) => {
    const paginate = req.query.limit && req.query.page ? {
        limit: req.query.limit || 20,
        page: req.query.page || 1
    } : null
    let params = {
        search: req.query.search,
        filter: req.query.filter,
        topShopBanner: req.query.top_shop_banner
    }
    return { paginate, params }
}


export const handleUpdateSetting = (req) => {
    let params: any = {
        shop_type: req.body.shop_type == 0 ? 0 : 1,
        refund_money_mode: req.body.refund_money_mode ? true : false,
        pause_mode: req.body.pause_mode ? true : false
    }
    let refund_money_regulations = req.body.refund_money_regulations;
    params.refund_money_regulations = params.refund_money_mode ? {
        refund_max: refund_money_regulations.refund_max || 0,
        regulations: refund_money_regulations.regulations || null
    } : null
    return params
}

export const handleTurnOnShippingMethodForShop = (req, shipping_method_id) => {
    const { is_active } =  req.body
    const params = {
        is_active: is_active,
        shop_id: req.shop_id,
        user_id: req.user._id,
        user: req.user,
        shipping_method_id: shipping_method_id,
    }
    return params
}