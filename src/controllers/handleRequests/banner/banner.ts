export const filterBanner = (req) => {
    let params = {
        shop_id: req.query.shop_id,
        filter_type: req.query.filter_type || 'ALL'
    }
    let paginate = {
        limit: req.query.limit || 10,
        page: req.query.page || 1
    }
    return { params, paginate }
}
export const filterBannerSystem = (req) => {
    let params = {
        filter_type: req.query.filter_type || 'ALL', 
        search: req.query.search,
    }
    let paginate = {
        limit: req.query.limit || 10,
        page: req.query.page || 1
    }
    return { params, paginate }
}

export const filterTopShopBanner = (req) => {
    let paginate = {
        limit: req.query.limit || 10,
        page: req.query.page || 1
    }
    return paginate;
}