import { isMappable } from "../../../utils/helper";

export const requestCreateGroupBuy  = (request) => {
    const { name, start_time, end_time, discount_type, products} = request;

    return {
        objectRequestComplete: {
            name,
            start_time,
            end_time,
            discount_type,
            products,
        }
    };
}

export const requestGroupBuyProduct  = (request, product) => {
    const { name, start_time, end_time, discount_type } = request;
    let { quantity, groups, compare_price } = product;

    groups = groups.map((group) => {
        if (isMappable(group.variants)) {
            group.variants = group.variants.map((variant) => {
                variant.sold = 0;
                return variant;
            })
        }
        return group;
    })

    return {
        shop_group_buy_id: request._id,
        name,
        start_time,
        end_time,
        status: "approved",
        discount_type,
        quantity,
        sold: 0,
        compare_price,
        groups
    };
}