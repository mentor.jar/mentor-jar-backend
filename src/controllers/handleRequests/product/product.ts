import { ProductRepo } from "../../../repositories/ProductRepo"
import { ShopRepo } from '../../../repositories/ShopRepo'
import { MediaRepo } from '../../../repositories/MediaRepo'
import { ProductDetailInfoRepo } from '../../../repositories/ProductDetailInfo'
import slugify from "slugify"
import { ObjectId } from "../../../utils/helper"
import { ARRAY_NAME_SIZE_INFOS, NAME_LOCALIZED_SIZE, REFUND_CONDITIONS, NEXT_14_DAYS } from "../../../base/variable"
import { makeShortenUrl } from "../../../utils/utils"
import { CategoryRepo } from "../../../repositories/CategoryRepo"
const probe = require('probe-image-size');

const repository = ProductRepo.getInstance();
const shopRepository = ShopRepo.getInstance();
const imageRepository = MediaRepo.getInstance();
const productDetailRepository = ProductDetailInfoRepo.getInstance();
const categoryRepo = CategoryRepo.getInstance();


export const createProductHandle = async (req) => {
    // Create a shop for user if shop doesn't exist
    // Make friendly_url for product
    let friendly_url: string = slugify(req.body.name.toLowerCase())
    const product_slug = await repository.findBySlug(friendly_url);

    if (product_slug != null) {
        friendly_url = friendly_url + '-' + Date.now();
    }

    // Count quanty
    let quantity = req.body.quantity_ware_house
    if (req.body.variants && req.body.variants.length) {
        quantity = req.body.variants.reduce((sum, item: any) => sum + item.quantity, 0)
    }

    // Refund condition
    const activeCondition = req.body.active_refund_conditions || []
    const productRefundCondition = REFUND_CONDITIONS.map((item: any) => {
        if (activeCondition.includes(item.code)) {
            item.active = true
        }
        else {
            item.active = false
        }
        return item
    })

    // Filter option types
    let typeSize: any = []
    if (req.body?.option_types) {
        const optionSize = req.body?.option_types.find(option => NAME_LOCALIZED_SIZE.includes(option.name.toLowerCase()));
        if (optionSize && optionSize.option_values && Array.isArray(optionSize.option_values)) {
            typeSize = optionSize?.option_values.map(optionValue => (optionValue.name))
        }
    }

    // Detail_size
    let detailSize:any = []
    if (req.body.detail_size && Array.isArray(req.body.detail_size)) {
        detailSize = req.body.detail_size.filter( (itemDetailSize) => {
            if (typeSize && typeSize.includes(itemDetailSize.type_size)) {
                itemDetailSize.size_infos = itemDetailSize.size_infos.map(itemSizeInfos => {
                    return {
                        name: ARRAY_NAME_SIZE_INFOS.filter(
                            (item) =>
                                item.vi_name === itemSizeInfos.name ||
                                item.ko_name === itemSizeInfos.name ||
                                item.en_name === itemSizeInfos.name
                        )?.[0]?.vi_name ?? itemSizeInfos.name,
                        min: itemSizeInfos.min,
                        max: itemSizeInfos.max,
                        unit: itemSizeInfos.unit,
                    };
                })
                return itemDetailSize;
            }

        })
    }

    // shorten link
    let shorten_link = makeShortenUrl(6, 'p')
    while(await repository.findOne({shorten_link}) !== null) {
        shorten_link = makeShortenUrl(6, 'p')
    }

    // handler BIDU air
    const isBIDUAir = await checkBIDUAir(req.body.category_id, req.body.list_category_id);

    // let images = await getListPathImage(req.body.images);
    const product = {
        name: req.body.name,
        description: req.body.description,
        short_description: req.body.short_description,
        friendly_url: req.body.friendly_url || friendly_url,
        weight: req.body.weight,
        width: req.body.width,
        height: req.body.height,
        length: req.body.length,
        before_sale_price: req.body.before_sale_price || req.body.price,
        sale_price: req.body.price,
        quantity,
        images: req.body.images,
        detail_size: detailSize,
        allow_to_sell: req.body.allow_to_sell || false,
        shop_id: req.shop_id,
        category_id: req.body.category_id,
        list_category_id: req.body.list_category_id,
        shipping_information: req.body.shipping_information,
        delivery_instruction: req.body.delivery_instruction,
        exchange_information: req.body.exchange_information,
        delivery_information: req.body.delivery_information,
        allow_refund: req.body.allow_refund || false,
        duration_refund: req.body.duration_refund,
        description_images: req.body.description_images,
        refund_conditions: productRefundCondition,
        shorten_link,
        custom_images: req.body.custom_images,
        is_guaranteed_item: req.body.is_guaranteed_item, // Hàng đảm bảo
        is_genuine_item: req.body.is_genuine_item, // Hàng chính hãng
        authen_images: req.body.authen_images,
        update_price_remaining: req.body.update_price_remaining || 3,
        next_date_update_price: req.body.next_date_update_price || new Date(),
        price_updated_dates: [],
        bidu_air: isBIDUAir,
    };

    // return product
    return product;
}

// export const getListPathImage = async (images) => {
//     let imagePromises: any = []
//     let listPath: any = [];
//     imagePromises = images.map(image_id => {
//         return new Promise(async (res, rej) => {
//             let imgItem = await imageRepository.findById(image_id)
//             res(imgItem.full_path);
//         })
//     });
//     return Promise.all(imagePromises)

// }

const checkBIDUAir = async (categoryId, listCategoryId) => {
    const internationalCategory: any = await categoryRepo.findOne({
        name: 'BIDU Air',
        is_active: true,
    });

    return listCategoryId.find(
        (categoryId) => categoryId === internationalCategory?._id?.toString()
    ) || categoryId === internationalCategory?._id?.toString()
        ? true
        : false;
};

export const getListDescriptionImages = async (req, descriptionImages) => {
    if (!descriptionImages) return [];

    const imagePromises = descriptionImages.map(async image_url => {
        try {
            let { url, width, height } = await probe(image_url);
            return { url, width, height }
        } catch (error) {
            throw new Error(req.__("product.invalid_url_image"))
        }
    });

    return Promise.all(imagePromises)
}

export const matchImageToProduct = async (id, images) => {
    const imagePromises: any = []
    images.forEach(image_full_path => {
        const promise = new Promise(async () => {
            let imgItem = await imageRepository.findByFullPath(image_full_path)
            if (imgItem) {
                const updateInfo: any = {
                    accessible_id: id
                }
                await imageRepository.update(imgItem._id, updateInfo)
            }
        })
        imagePromises.push(promise)
    });

    Promise.all(imagePromises)
}

export const matchImageToProductUpdate = async (id, images, oldImages) => {
    let matchImages: any = [];
    let deletedImages: any = [];
    //check if new images exist in old images
    images.map(e => {
        if (!oldImages.includes(e)) {
            matchImages.push(e);
        }
    });
    if (matchImages.length > 0) {
        matchImageToProduct(id, matchImages);
    }

    //check if old images exist in new images
    oldImages.map(e => {
        if (!images.includes(e)) {
            deletedImages.push(e);
        }
    });

    if (deletedImages.length > 0) {
        await imageRepository.deleteMany({ full_path: deletedImages });
    }
}

export const createProductDetailInfo = async (id, infos) => {
    const infoPromises: any = []
    infos.forEach(info => {
        infoPromises.push(
            productDetailRepository.create({
                category_info_id: info.category_info_id,
                product_id: id,
                value: info.value,
                values: info.values
            })
        )
    });
    Promise.all(infoPromises)
}

export const updateProductDetailInfo = async (id, infos) => {
    await productDetailRepository.deleteMany({ product_id: id });
    createProductDetailInfo(id, infos);
}
export const updateProductDetailInfoInsde = async (id, infos) => {
    const promises: any = []
    for (let info of infos) {
        const promise = new Promise(async () => {
            let item: any = info.value;
            await productDetailRepository.findOneAndUpdate({ category_info_id: ObjectId(info.category_info_id), product_id: id }, { value: item });
        });
        promises.push(promise);
    }
    Promise.all(promises);
}

export const filterProductByShop = (req) => {
    let params = {
        shop_id: req.shop_id || req.query.shop_id,
        filter_type: req.query.filter_type,
        search: req.query.search || req.query.product_name
    }
    let paginate = req.query.is_paginate ? {
        limit: req.query.limit || 2,
        page: req.query.page || 1
    } : null
    return { params, paginate }
}

export const updateProductHandle = async (req) => {

    // Make friendly_url for product
    let friendly_url: string = slugify(req.body.name.toLowerCase())
    const product_slug = await repository.findBySlug(friendly_url);

    if (product_slug != null) {
        friendly_url = friendly_url + '-' + Date.now();
    }

    // Count quanty
    let quantity = req.body.quantity_ware_house
    if (req.body.variants && req.body.variants.length) {
        quantity = req.body.variants.reduce((sum, item: any) => sum + item.quantity, 0)
    }

    // Refund condition
    const activeCondition = req.body.active_refund_conditions || []
    const productRefundCondition = REFUND_CONDITIONS.map((item: any) => {
        if (activeCondition.includes(item.code)) {
            item.active = true
        }
        else {
            item.active = false
        }
        return item
    })

    // Filter option types
    let typeSize: any = []
    if (req.body?.option_types) {
        const optionSize = req.body?.option_types.find(option => NAME_LOCALIZED_SIZE.includes(option.name.toLowerCase()));
        if (optionSize && optionSize.option_values && Array.isArray(optionSize.option_values)) {
            typeSize = optionSize?.option_values.map(optionValue => (optionValue.name))
        }
    }

    // Detail_size
    let detailSize:any = []
    if (req.body.detail_size && Array.isArray(req.body.detail_size)) {
        detailSize = req.body.detail_size.filter( (itemDetailSize) => {
            if (typeSize && typeSize.includes(itemDetailSize.type_size)) {
                itemDetailSize.size_infos = itemDetailSize.size_infos.map(itemSizeInfos => {
                    return {
                        name : itemSizeInfos.vi_name ?? itemSizeInfos.name,
                        min: itemSizeInfos.min,
                        max: itemSizeInfos.max,
                        unit: itemSizeInfos.unit,
                    }
                })
                return itemDetailSize;
            }

        })
    }

    // handler BIDU air
    const isBIDUAir = await checkBIDUAir(req.body.category_id, req.body.list_category_id);

    const product = {
        name: req.body.name,
        description: req.body.description,
        short_description: req.body.short_description,
        friendly_url: req.body.friendly_url || friendly_url,
        weight: req.body.weight,
        width: req.body.width,
        height: req.body.height,
        length: req.body.length,
        before_sale_price: req.body.before_sale_price || req.body.price,
        sale_price: req.body.price,
        quantity,
        images: req.body.images,
        detail_size: detailSize,
        allow_to_sell: req.body.allow_to_sell || false,
        category_id: req.body.category_id,
        list_category_id: req.body.list_category_id,
        shipping_information: req.body.shipping_information,
        delivery_instruction: req.body.delivery_instruction,
        exchange_information: req.body.exchange_information,
        delivery_information: req.body.delivery_information,
        allow_refund: req.body.allow_refund || false,
        duration_refund: req.body.duration_refund,
        description_images: req.body.description_images,
        refund_conditions: productRefundCondition,
        custom_images: req.body.custom_images,
        is_guaranteed_item: req.body.is_guaranteed_item || false, // Hàng đảm bảo
        is_genuine_item: req.body.is_genuine_item || false, // Hàng chính hãng
        is_sold_out: req.body.is_sold_out || false,
        authen_images: req.body.authen_images,
        next_date_update_price: req.isChangePrice || req.isRenewUpdatePriceCount ? new Date().getTime() + NEXT_14_DAYS : req.product.next_date_update_price,
        price_updated_dates: req.isChangePrice || req.isRenewUpdatePriceCount ? [...req.product.price_updated_dates, new Date()] : req.product.price_updated_dates,
        $inc: { update_price_remaining: req.isChangePrice ? -1 : req.isRenewUpdatePriceCount ? 2 - req.product.update_price_remaining :  0 },
        bidu_air: isBIDUAir,
    };
    // return product
    return product;
}

export const updateWeightHandle = async (productListToUpdate) => {
    const promises = productListToUpdate.map(async element => {
        const productUpdate = await repository.findOneAndUpdate(
            { _id: ObjectId(element.product_id) },
            { weight: element.weight }
        );
        return productUpdate;
    })
    return Promise.all(promises);
}