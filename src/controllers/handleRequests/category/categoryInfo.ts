
export const createCategoryInfoHandle: any = async (req) => {
    const category_id = req.params.id;
    const categoryInfo = {
        name: {
            vi:req.body.name.vi || "",
            en:req.body.name.en || "",
            ko:req.body.name.ko || "",
        },
        type: req.body.type || "input",
        fashion_type: req.body.fashion_type || 1,
        is_required: req.body.is_required || false,
        is_allow_multiple_values: req.body.is_allow_multiple_values || false,
        list_option: req.body.list_option,
        category_id: category_id
    };
    return categoryInfo;
}

export const updateCategoryInfoHandle: any = async (req) => {
    const categoryInfo = {
        name: {
            vi:req.body.name.vi || "",
            en:req.body.name.en || "",
            ko:req.body.name.ko || "",
        },
        type: req.body.type || "input",
        fashion_type: req.body.fashion_type || 1,
        is_required: req.body.is_required || false,
        is_allow_multiple_values: req.body.is_allow_multiple_values || false,
        list_option: req.body.list_option,
        category_id: req.params.id
    };
    return categoryInfo;
}