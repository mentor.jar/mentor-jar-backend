import { CategoryRepo } from "../../../repositories/CategoryRepo"
import slugify from "slugify"

const repository = CategoryRepo.getInstance();

export const createCategoryHandle: any = async (req) => {
    let permalink: string = slugify(req.body.name.toLowerCase());
    const category_slug = await repository.findBySlug(permalink);

    if (category_slug != null) {
        permalink = permalink + '-' + Date.now();
    }
    const { name, priority, avatar, pdfAvatar, parent_id } = req.body
    const category = {
        name,
        priority,
        shop_id: null,
        avatar,
        pdfAvatar: pdfAvatar,
        parent_id,
        permalink
    };
    return category;
}

export const createShopCategoryCMSHandle: any = async (req) => {
    let permalink: string = slugify(req.body.name.toLowerCase());
    const category_slug = await repository.findBySlug(permalink);

    if (category_slug != null) {
        permalink = permalink + '-' + Date.now();
    }
    const { name, priority, shop_id, is_active } = req.body
    const category = {
        name,
        priority,
        shop_id,
        parent_id: null,
        permalink: permalink,
        is_active
    };

    return category;
}

export const updateActiveCategoryHandle: any = async (req) => {

    const category = {
        is_active: req.body.is_active
    };
    return category;
}

export const updateActiveCategoryMobileHandle: any = async (req) => {

    const category = {
        is_active: req.body.is_active,
    };
    return category;
}

export const createCategoryMobile: any = async (req) => {
    let permalink: string = slugify(req.body.name.toLowerCase());
    const category_slug = await repository.findBySlug(permalink);

    if (category_slug != null) {
        permalink = permalink + '-' + Date.now();
    }

    const category = {
        name: req.body.name,
        priority: req.body.priority,
        shop_id: req.shop_id,
        parent_id: null,
        permalink: permalink,
        is_active: req.body.is_active
    };

    return category;
}

export const updateShopCategoryCMSHandle: any = async (req) => {
    let permalink: string = slugify(req.body.name.toLowerCase());
    const category_slug = await repository.findBySlug(permalink);

    if (category_slug != null) {
        permalink = permalink + '-' + Date.now();
    }
    const info_update: any = {
        name: req.body.name,
        permalink: permalink,
    }
    return info_update;
}

export const updateCategoryMobile: any = async (req) => {
    let permalink: string = slugify(req.body.name.toLowerCase());
    const category_slug = await repository.findBySlug(permalink);

    if (category_slug != null) {
        permalink = permalink + '-' + Date.now();
    }
    const info_update: any = {
        name: req.body.name,
        permalink: permalink,
        is_active: req.body.is_active
    }
    return info_update;
}

export const paginationHandle = (req) => {
    let paginate = req.query.is_paginate ? {
        limit: req.query.limit || 10,
        page: req.query.page || 1
    } : null
    return { paginate }
}

