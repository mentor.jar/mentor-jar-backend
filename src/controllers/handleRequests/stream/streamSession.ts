import { generateIdFromDate } from "../../../utils/dateUtil";

export const streamSessionHandle: any = (room_id, stream_id, req) => {
    const streamFile = {
        session_number: generateIdFromDate(),
        stream_id: stream_id,
        shop_id: req.shop_id,
        user_id: req.user._id,
        room_chat_id: room_id,
        name: req.body.name,
        image: req.body.image,
        type: req.body.type,
        type_of_viewer: req.body.type_of_viewer,
        list_id_product: req.body.list_id_product,
        time_will_start: req.body.time_will_start
    }
    return streamFile;
}

export const filterStreamSession = (req) => {
    let paginate = {
        limit: req.query.limit || 2,
        page: req.query.page || 1
    }
    return { paginate }
}

