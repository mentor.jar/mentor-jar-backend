import { URL_SERVER, URL_SERVER_RTMP } from "../../../base/variable";

export const streamHandle: any = (req) => {
    const streamFile = {
        name: req.user._id + ".stream",
        rtmp_link: URL_SERVER_RTMP + req.user._id + ".stream" || null,
        hls_link: URL_SERVER + req.user._id + ".stream/playlist.m3u8" || null,
        user_id: req.user._id,
        shop_id: req.shop_id
    }
    return streamFile;
}

