const moment = require('moment');

export const createVoucherHandle: any = async (req) => {

    const { name, code, target, start_time, end_time, save_quantity, use_quantity, type, value, max_discount_value, display_mode, min_order_value, product_ids, max_budget } = req.body

    const voucher: any = { name, code, target, start_time, end_time, save_quantity, use_quantity, available_quantity: use_quantity, type, value, max_discount_value, display_mode, min_order_value, products: product_ids || [], max_budget };

    if (req.body.shop_id) {
        // Admin create voucher for shop
        voucher.shop_id = req.body.shop_id
    }
    else {
        // Shop create voucher
        voucher.shop_id = req.shop_id
    }


    // approved status
    voucher.approve_status = 'approved'


    return voucher;
}

export const createVoucherCMSHandle: any = async (req) => {

    const { name, code, target, start_time, end_time, save_quantity, use_quantity, type, value, discount_by_range_price, 
        max_discount_value, display_mode, min_order_value, max_order_value, product_ids, shop_ids, max_budget, classify, 
        conditions, kol_user, is_public } = req.body

    const voucher: any = { name, code, target, start_time, end_time, save_quantity, use_quantity, 
        available_quantity: use_quantity, type, value, discount_by_range_price, max_discount_value, 
        display_mode, min_order_value, products: product_ids || [], shops: shop_ids || [], max_budget, 
        classify, conditions, kol_user, is_public, max_order_value: max_order_value || null };

    // approved status
    voucher.approve_status = 'approved'

    return voucher;
}

export const updateVoucherCMSHandle: any = async (req, voucher) => {

    const { name, code, target, start_time, end_time, save_quantity, use_quantity, type,
        value, discount_by_range_price, max_discount_value, display_mode, min_order_value, 
        max_order_value, product_ids, shop_ids, max_budget, classify, conditions, kol_user, is_public } = req.body
    const newVoucher: any = {
        name, code, target, start_time, end_time, save_quantity,
        use_quantity, type, value, discount_by_range_price, max_discount_value,
        display_mode, min_order_value, products: product_ids || [], shops: shop_ids || [], 
        max_budget, classify, conditions, kol_user, is_public, max_order_value: max_order_value || null
    };
    newVoucher.available_quantity = use_quantity - (voucher.use_quantity - voucher.available_quantity)
    return newVoucher;
}

/**
 * 
 * @param req 
 * @param voucher Use for update available field in the future (When has feature: Add voucher to order)
 */
export const updateVoucherHandle: any = async (req, voucher) => {
    const { name, code, target, start_time, end_time, save_quantity, use_quantity, type, value, max_discount_value, display_mode, min_order_value, product_ids, max_budget, conditions } = req.body

    const newVoucher: any = { name, code, target, start_time, end_time, save_quantity, use_quantity, type, value, max_discount_value, display_mode, min_order_value, products: product_ids || [], max_budget, conditions };

    newVoucher.available_quantity = use_quantity - (voucher.use_quantity - voucher.available_quantity)
    return newVoucher;
}

export const updateVoucherCMSHandleMobile: any = async (req, voucher) => {
    const { name, code, target, start_time, end_time, save_quantity, use_quantity, type, value, max_discount_value, display_mode, min_order_value, product_ids, max_budget } = req.body

    const newVoucher: any = { name, code, target, start_time, end_time, save_quantity, use_quantity, type, value, max_discount_value, display_mode, min_order_value, products: product_ids || [], max_budget };

    if (req.body.shop_id) {
        // Admin create voucher for shop
        newVoucher.shop_id = req.body.shop_id
        newVoucher.available_quantity = use_quantity - (voucher.use_quantity - voucher.available_quantity)
    }
    return newVoucher
}