
import { OptionTypeDTO } from "../../../DTO/OptionTypeDTO";
import { VariantDTO } from "../../../DTO/VariantDTO";

export const optionTypeHandle: any = (option_types: Array<OptionTypeDTO>) => {
    return option_types.map(e => new OptionTypeDTO(e));
}

export const variantHandle: any = (variants: Array<VariantDTO>) => {
    return variants.map(e => new VariantDTO(e));
}