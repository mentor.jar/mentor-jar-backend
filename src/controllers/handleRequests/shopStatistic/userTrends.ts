import { ValidationError } from "../../../base/customError";

const suport_time_type = [
    'today',
    'yesterday',
    '7days',
    '30days',
    'custom'
]

const date = (timestamps) => new Date(timestamps);
const newDate = () => new Date();

export const handleTimeReq = (req) => {
    let start_time, end_time = new Date();
    const { time_range } = req.query;
    const { time_begin, time_end } = req.query;
    if (!suport_time_type.includes(time_range))
        throw new ValidationError('Khung thời gian không được hỗ trợ');

    switch (time_range) {
        case 'today':
            start_time = date(newDate().setHours(0, 0, 0, 0));
            break;
        case 'yesterday':
            end_time = date(newDate().setHours(0, 0, 0, 0));
            end_time = date(end_time.setMinutes(end_time.getMinutes() - 1));
            start_time = date(newDate().setHours(0, 0, 0, 0));
            start_time = date(start_time.setDate(start_time.getDate() - 1));
            break;
        case '7days':
            start_time = date(newDate().setHours(0, 0, 0, 0));
            start_time = date(start_time.setDate(start_time.getDate() - 6));
            break;
        case '30days':
            start_time = date(newDate().setHours(0, 0, 0, 0));
            start_time = date(start_time.setDate(start_time.getDate() - 29));
            break;
        case 'custom':
            if (!time_begin || !time_end || time_begin >= time_end)
                throw new ValidationError(req.__("controller.shop_statistics.invalid_time"));
            start_time = new Date(time_begin * 1000);
            end_time = new Date(time_end * 1000);
            break;
        default: break;
    }
    // get end day.
    // end_time = date(end_time.setHours(0, 0, 0, 0));
    // end_time = date(end_time.setDate(end_time.getDate() + 1));
    // end_time = date(end_time.setMinutes(end_time.getMinutes() - 1));
    return { start_time, end_time }
}

export const handleType = (req) => {
    let type = '';

    return type;
}
