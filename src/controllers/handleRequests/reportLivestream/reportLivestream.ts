import { REPORT_LIVESTREAM_REASONS, LIVESTREAM_OTHER_REASON } from "../../../base/variable";
export interface CreateReportLiveStream {
    user_report_id: String,
    stream_session_id: String,
    owner_id: String,
    time_report: Date,
    reasons: Array<Object>
}
export const reportLiveStreamHandle: any = (req, ownerId) => {
    const { reasons, custom_reason } = req.body;
    const reasonReport = REPORT_LIVESTREAM_REASONS.reduce((results, item) => {
        if (reasons.includes(item.code)) {
            if (item.code === LIVESTREAM_OTHER_REASON) {
                item = {...item,custom_reason }
            }
            results.push(item)
        } 
        return results
    }, [])

    const report: CreateReportLiveStream = {
        user_report_id: req.user._id,
        stream_session_id: req.params.id,
        owner_id: ownerId,
        time_report: new Date(),
        reasons: reasonReport
    }
    return report;
}