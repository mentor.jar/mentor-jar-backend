import RoomChat from "../../../models/RoomChat";
import { UserRepo } from "../../../repositories/UserRepo";

export const handleRequestGetRoom: any = (req) => {
    const params: any = {
        userId: req.query.user_id || req.user?._id,
        userName: req.query.user_name,
        type: req.query.type || "CHAT",
        limit: req.query.limit || 20,
        page: req.query.page || 1
    }
    return params;
}

export const handleRequestcreateRoom: any = async (req) => {
    let users = await UserRepo.getInstance().getUserbyListUserId(req.body.user_ids);
    const params: any = {
        name: req.body.name,
        is_group: req.body.is_group,
        creator_id: req.user?._id,
        type: req.body.type || "CHAT",
        message: [],
        users: users
    }
    return params;
}

export const handleRequestcreateRoomLiveStream: any = async (req) => {
    let userId: any = [];
    userId.push(req.user._id)
    let users = await UserRepo.getInstance().getUserbyListUserId(userId);
    const params: any = {
        name: "",
        is_group: true,
        creator_id: req.user?._id,
        type: "LIVE STREAM",
        message: [],
        users: users
    }
    return params;
}
