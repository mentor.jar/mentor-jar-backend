import { MediaRepo } from "../../repositories/MediaRepo"
import responseCode from "../../base/responseCode"
import * as variables from '../../base/variable'
import { cloneObj } from "../../utils/helper";
import keys from '../../config/env/keys'


let imageRepo = MediaRepo.getInstance();


export const createOrUpdateImage = async (req: any, res: any, next: any) => {

    const { originalUrl } = req

    switch (originalUrl) {
        case variables.PRODUCT_IMAGE_PATH:
            const { files } = req
            if (files === undefined || !files.length) {
                res.error(responseCode.MISSING.name, 'Tập tin rỗng', responseCode.MISSING.code);
            }
            else if (files.length > variables.MAX_PRODUCT_IMAGE) {
                res.error(responseCode.MISSING.name, `${req.__("controller.product.maximum_product_images")}: ${variables.MAX_PRODUCT_IMAGE} `, responseCode.MISSING.code);
            }
            else {
                const promises: any = []
                let arrImages: any = []
                files.map(file => {
                    const image = {
                        accessible_type: 'Product',
                        accessible_id: null,
                        full_path: `${keys.host}/${file.path}`,
                        path: file.path,
                        mimetype: file.mimetype,
                        filename: file.filename,
                        encoding: file.encoding
                    }

                    promises.push(
                        image
                    )
                });

                await imageRepo.insertMany(promises)
                    .then((result: any) => {
                        arrImages = result.map(image => cloneObj(image));
                        return res.success(arrImages, responseCode.SUCCESS.name, responseCode.SUCCESS.code);
                    })
                    .catch(err => {
                        res.error(responseCode.MISSING.name, err.message, responseCode.MISSING.code);
                    })
            }
            break;
        default:
            const { file } = req
            if (!file) {
                res.error(responseCode.MISSING.name, 'file is empty', responseCode.MISSING.code);
            }
            else {
                const accessible_type = req.body.accessible_type || null
                const accessible_id = req.body.accessible_id || null
                const id = req.body.id
                if (id) {
                    let image: any
                    try {
                        image = await imageRepo.findById(id)
                    } catch (error) {
                        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
                    }

                    if (image) {
                        const updateInfo: any = {
                            path: file.path
                        }
                        let result: any = await imageRepo.update(id, updateInfo)
                        // result = cloneObj(result)
                        // result.fullPath = `${keys.host}/${result.path}`
                        res.success(result, responseCode.SUCCESS.name, responseCode.SUCCESS.code);
                    }
                }
                else {
                    const image = {
                        accessible_type: accessible_type,
                        accessible_id: accessible_id,
                        path: file.path,
                        full_path: `${keys.host}/${file.path}`,
                        mimetype: file.mimetype,
                        filename: file.filename,
                        encoding: file.encoding
                    }
                    let result: any = await imageRepo.create(image);
                    // result = cloneObj(result)
                    // result.fullPath = `${keys.host}/${result.path}`
                    res.success(result, responseCode.SUCCESS.name, responseCode.SUCCESS.code);
                }

            }
            break;
    }

    const reqUrl = req.originalUrl
}