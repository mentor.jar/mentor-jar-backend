import { VN_LANG, EN_LANG, KO_LANG } from './../../base/variable';
import { DataCityRepo } from '../../repositories/DataCityRepo';
import { ChangeDatabaseService } from '../serviceHandles/toolDev/changeDatabase';
import { FakeDataService } from '../serviceHandles/toolDev/fakeData';
import { InMemoryMessageForRoomStore } from '../../SocketStores/MessageStore';
import notificationService from '../../services/notification/index';
import { StreamSessionRepository } from '../../repositories/StreamSessionRepository';
import { cloneObj, ObjectId } from '../../utils/helper';
import { StreamRepository } from '../../repositories/StreamRepository';
import { OrderRepository } from '../../repositories/OrderRepo';
import { OrderShippingRepo } from '../../repositories/OrderShippingRepo';
import { UserRepo } from '../../repositories/UserRepo';
import OrderService from "../serviceHandles/order/order";
import NotificationRepo from '../../repositories/NotificationRepo';
import { SystemSettingRepo } from '../../repositories/SystemSettingRepo';
import _ from "lodash"
import { VoucherRepository } from '../../repositories/VoucherRepository';
import { InMemoryVoucherStore } from '../../SocketStores/VoucherStore';
import responseCode from '../../base/responseCode';
import { VoucherDTO } from '../../DTO/VoucherDTO';
import VoucherService from '../serviceHandles/voucher';
import { ProductRepo } from '../../repositories/ProductRepo';
import BannerRepo from '../../repositories/BannerRepo';
import { CategoryRepo } from '../../repositories/CategoryRepo';
import { OptionValueRepo } from '../../repositories/Variant/OptionValueRepo';
import { FeedbackRepo } from '../../repositories/FeedbackRepo';

const dataCityRepo = DataCityRepo.getInstance();
const systemSetting  = SystemSettingRepo.getInstance();
const voucherRepo = VoucherRepository.getInstance();
const voucherAndOrderCache = InMemoryVoucherStore.getInstance();
const productRepo = ProductRepo.getInstance()
const categoryRepo = CategoryRepo.getInstance();
const optionValueRepo = OptionValueRepo.getInstance();
const feedbackRepo = FeedbackRepo.getInstance();

export const getReasons = async (req, res, next) => {
    try {
        const reasons = OrderService._.getListReasons();
        res.success(reasons);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
};

export const getRejectReason = async (req, res, next) => {
    try {
        const reasons = OrderService._.getListRejectReasons();
        res.success(reasons);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getSetDataCity = async (req, res, next) => {
    try {
        const data = await dataCityRepo.findGetByName();
        res.success(data);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
};

export const cleanDatabase = async (req, res, next) => {
    try {
        // const data = await ChangeDatabaseService.clearRoomChatGreater2();
        // const data = await ChangeDatabaseService.updateViewMaxStreamSession();
        // const data = await ChangeDatabaseService.clearProductNotUser();
        const users = await UserRepo.getInstance().find({});
        let data: any = [];
        for (let user of users) {
            data.push(await ChangeDatabaseService.updateUserInSocket(user._id.toString()));
        }
        res.success(data);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
};

export const fakeData = async (req, res, next) => {
    try {
        // const data = await FakeDataService.chartOrder();
        // const data = await FakeDataService.trackingUser();
        // res.success(data);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
};

export const testChat = async (req, res, next) => {
    const { roomId, message } = req.body;
    const RoomStore = InMemoryMessageForRoomStore.getInstance();
    const room = await RoomStore.getRoom(roomId);
    if (!room) throw new Error('Can not find room');
    // console.log(room);
    const senderId = message.user_id;
    const receiver = room?.users?.filter(user => user._id != senderId);
    const receiverIds = receiver?.map(user => user._id);
    // console.log(receiverIds);
    const starttime = Date.now();
    await notificationService.sendChatNotification(room, message, receiverIds);
    // console.log(Date.now() - starttime);
    res.json({ ok: 1 });
};

export const ChangeStreamURL = async (req, res, next) => {
    // const streams: any = await StreamRepository.getInstance().find({});
    // for (let i = 0; i < streams.length; i++) {
    //     const new_rtmp = streams[i].rtmp_link.replace("103.232.123.57", "103.69.193.161");
    //     const new_hls = streams[i].hls_link.replace("103.232.123.57", "103.232.123.57");
    //     const newInstance = {
    //         rtmp_link: new_rtmp,
    //         hls_link: new_hls
    //     }
    //     await StreamRepository.getInstance().update(streams[i]._id, newInstance);
    // }

    // res.json({ ok: 1 });

};

export const changeStatusOrderCanceled = async (req, res, next) => {
    // const orders = await OrderRepository.getInstance().find({ shipping_status: 'canceled' });
    // for (let i = 0; i < orders.length; i++) {
    //     const newInstance = {
    //         cancel_type: "seller"
    //     }
    //     await OrderRepository.getInstance().update(orders[i]._id.toString(), newInstance);
    // }
    // res.json({ ok: 1 });

};

export const createOrderShipping = async (req, res, next) => {
    // const orders = await OrderRepository.getInstance().find({ shipping_status: { $ne: 'shipped' } });
    // for (let i = 0; i < orders.length; i++) {
    //     const orderShipping = await OrderShippingRepo.getInstance().findOne({ order_id: orders[i]._id });
    //     console.log(orderShipping);

    //     if (!orderShipping) {
    //         //create orderShipping
    //         const instance = {
    //             order_id: orders[i]._id,
    //             user_id: orders[i].user_id,
    //             shop_id: orders[i].shop_id
    //         }
    //         await OrderShippingRepo.getInstance().create(instance)
    //     }
    // }

    // const orders2 = await OrderRepository.getInstance().find({ shipping_status: 'shipped' });
    // for (let i = 0; i < orders2.length; i++) {
    //     const orderShipping2 = await OrderShippingRepo.getInstance().findOne({ order_id: orders2[i]._id });
    //     if (!orderShipping2) {
    //         //create orderShipping
    //         const instance2 = {
    //             order_id: orders2[i]._id,
    //             user_id: orders2[i].user_id,
    //             shop_id: orders2[i].shop_id,
    //             user_confirm_shipped_time: new Date()
    //         }
    //         await OrderShippingRepo.getInstance().create(instance2)
    //     }
    // }
    // res.json({ ok: 1 });

};

export const cleanNotification = async (req, res, next) => {
    try {
        // const notifications = await NotificationRepo.getInstance().deleteMany({ shardingKey: 'Ecommerce' });
        // res.success(notifications);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
};

export const getAppVersion = async (req, res, next) => {
    let settingVersion: any = await systemSetting.findOne({})

    settingVersion = cloneObj(settingVersion);
    res.success({
        production: settingVersion?.version_production || "1.0",
        staging: settingVersion?.version_staging || "1.0",
        production_android: settingVersion?.version_production_android || 0,
        staging_android: settingVersion?.version_staging_android || 0,
    })
}

export const getAppVersionV2 = async (req, res, next) => {
    let language = req.headers["accept-language"] || VN_LANG
    if(language !== VN_LANG && language !== EN_LANG && language !== KO_LANG){
        language = VN_LANG
    }
    let settingVersion: any = await systemSetting.findOne({})
    settingVersion = cloneObj(settingVersion);
    settingVersion.app_version.ios.message = settingVersion.app_version.ios.message[language]
    settingVersion.app_version.android.message = settingVersion.app_version.android.message[language]
    const appVersion = settingVersion.app_version
    res.success(appVersion)
}

export const healthCheck = async (req, res, next) => {
    res.success(process.memoryUsage())
}

export const syncVouchers = async (req, res, next) => {
    try {
        const vouchers = await VoucherService._.syncVoucherSystem()
        res.success(vouchers)
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
    }
}