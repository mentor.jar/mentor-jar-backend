
import { MediaRepo } from "../../repositories/MediaRepo"
import responseCode from "../../base/responseCode"
import keys from '../../config/env/keys'


let videoRepo = new MediaRepo();


export const createOrUpdateVideo = async (req: any, res: any, next: any) => {

    const { file } = req
    if (!file) {
        res.error(responseCode.MISSING.name, req.__("controller.video.empty_video"), responseCode.MISSING.code);
    }
    else {
        const accessible_type = req.body.accessible_type || null
        const accessible_id = req.body.accessible_id || null
        const id = req.body.id
        if (id) {
            let video: any
            try {
                video = await videoRepo.findById(id)
            } catch (error) {
                res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
            }

            if (video) {
                const updateInfo: any = {
                    path: video.path
                }
                let result: any = await videoRepo.update(id, updateInfo)
                res.success(result, responseCode.SUCCESS.name, responseCode.SUCCESS.code);
            }
        }
        else {
            const video = {
                accessible_type: accessible_type,
                accessible_id: accessible_id,
                path: file.path,
                full_path: `${keys.host}/${file.path}`,
                mimetype: file.mimetype,
                filename: file.filename,
                encoding: file.encoding
            }
            let result: any = await videoRepo.create(video);
            res.success(result, responseCode.SUCCESS.name, responseCode.SUCCESS.code);
        }

    }
    const reqUrl = req.originalUrl
}