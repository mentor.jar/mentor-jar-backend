import { createCategoryInfoHandle, updateCategoryInfoHandle } from "../../handleRequests/category/categoryInfo";
import responseCode from "../../../base/responseCode";
import { CategoryInfoRepo } from "../../../repositories/CategoryInfoRepo"
import { ProductDetailInfoRepo } from "../../../repositories/ProductDetailInfo"
import { executeError } from "../../../base/appError";
import { cloneObj } from "../../../utils/helper";

let categoryInfoRepo = CategoryInfoRepo.getInstance();
let productDetailInfo = ProductDetailInfoRepo.getInstance();

export const createCategoryInfo = async (req: any, res: any, next: any) => {
    try {

        const categoryInfoObject = await createCategoryInfoHandle(req);
        const categoryInfo = await categoryInfoRepo.create(categoryInfoObject);
        res.success(categoryInfo);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    }
}

export const updateCategoryInfo = async (req: any, res: any, next: any) => {
    try {
        const categoryInfoObject = await updateCategoryInfoHandle(req);
        const categoryInfo = await categoryInfoRepo.update(req.params.idInfo, categoryInfoObject);

        res.success(categoryInfo);

    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    }
}

export const deleteCategoryInfo = async (req: any, res: any, next: any) => {
    const categoryInfo_id = req.params.idInfo;
    try {
        await categoryInfoRepo.delete(categoryInfo_id);
        await productDetailInfo.deleteByCategoryInfo(categoryInfo_id);
        res.success("", "", 204)
    } catch (error) {
        executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getCategoriesById = async (req: any, res: any, next: any) => {
    const categoryId = req.params.id;
    try {
        let result = await categoryInfoRepo.getByCategoryId(categoryId);
        result = Object(result);
        res.success(result, responseCode.SUCCESS.code);
    } catch (error) {
        res.success([]);
    }
}