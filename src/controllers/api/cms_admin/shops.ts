import {
    filterShop,
    handleTurnOnShippingMethodForShop,
    handleUpdateSetting,
} from '../../handleRequests/shop/shop';
import { ShopRepo } from '../../../repositories/ShopRepo';
import { executeError } from '../../../base/appError';
import responseCode from '../../../base/responseCode';
import { ShippingMethodQuery } from '../../../models/enums/order';
import { createGHTKAccountForShop } from '../mobile_app/shop';
import ShopService from '../../serviceHandles/shop/service';
import { getTranslation, getUserLanguage } from '../../../services/i18nCustom';
import notificationService, { NotifyRequestFactory } from '../../../services/notification';
import { NOTIFY_TARGET_TYPE, NOTIFY_TYPE } from '../../../services/notification/constants';
import { addLink } from '../../../utils/stringUtil';
import keys from '../../../config/env/keys';
import { UserDTO } from '../../../DTO/UserDTO';
import { ObjectId } from '../../../utils/helper';

const shopRepo = ShopRepo.getInstance();

export const getListShop = async (req: any, res: any, next: any) => {
    try {
        let handleParam = filterShop(req);
        const shops = await shopRepo.getListShop(
            handleParam.paginate,
            handleParam.params
        );
        res.success(shops);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
};

export const getDetailShop = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id;
        if (id) {
            let shop: any = await shopRepo.getDetailShop(id);
            shop.user.avatar = addLink(`${keys.host_community}/`, shop.user.gallery_image.url)
            shop.user = new UserDTO(shop.user).toSimpleJSONForAdmin();
            // including user to shop:
            res.success(
                shop,
                responseCode.SUCCESS.name,
                responseCode.SUCCESS.code
            );
        } else {
            res.error(
                responseCode.NOT_FOUND.name,
                'Không tìm thấy cửa hàng',
                responseCode.NOT_FOUND.name
            );
        }
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
};

export const settingShop = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id;
        const paramUpdateSetting = handleUpdateSetting(req);
        await shopRepo.settingShop(id, paramUpdateSetting);
        const shop = await shopRepo.getDetailShop(id);
        res.success(shop, responseCode.SUCCESS.name, responseCode.SUCCESS.code);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
};

export const activeShippingMethodForShop = async (req: any, res: any, next: any) => {
    try {
        const params = handleTurnOnShippingMethodForShop(req, req.body.shipping_method_id);

        if (req.createAccount) {
            if (req.name_query === ShippingMethodQuery.GIAOHANGTIETKIEM) {
                await createGHTKAccountForShop(params, req);
            }
        }
        await ShopService._.updateActiveShippingMethodByShop(params);
        return res.success({ is_active: JSON.parse(params.is_active) });
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const adminApproveOrRejectSeller = async (req: any, res: any, next: any) => {
    try {
        const { is_approved } = req.body
        const { shop } = req
        const newShop = await shopRepo.update(shop._id, {
            is_approved
        })
        // Notification
        // Get Language
        const language = await getUserLanguage(shop.user_id);
        if (is_approved) {
            // send notification approved
            await notificationService.saveAndSend(
                NotifyRequestFactory.generate(
                    NOTIFY_TYPE.CREATE_SHOP_APPROVED,
                    NOTIFY_TARGET_TYPE.BUYER,
                    {
                        title: getTranslation(language, 'notification.bidu_notification'),
                        content: getTranslation(language, 'notification.create_shop_approved'),
                        receiverId: shop.user_id.toString(),
                        shopId: shop._id.toString(),
                        userId: shop.user_id.toString()
                    }
                ))
        } else {
            // send notification rejected
            await notificationService.saveAndSend(
                NotifyRequestFactory.generate(
                    NOTIFY_TYPE.CREATE_SHOP_REJECTED,
                    NOTIFY_TARGET_TYPE.BUYER,
                    {
                        title: getTranslation(language, 'notification.bidu_notification'),
                        content: getTranslation(language, 'notification.create_shop_rejected'),
                        receiverId: shop.user_id.toString(),
                        shopId: shop._id.toString(),
                        userId: shop.user_id.toString()
                    }
                ))
        }
        res.success(newShop)
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const toggleShowShopOnTop = async (req: any, res: any, next: any) => {
    try {
        const { allow_show_on_top } = req.body
        const { id } = req.params
        const shop:any = await shopRepo.findOne({_id: ObjectId(id)})
        if(!shop) {
            res.error(responseCode.NOT_FOUND.name, req.__("shop.shop_not_found"), responseCode.NOT_FOUND.code);
            return;
        }
        const newShop = await shopRepo.update(shop._id, {
            allow_show_on_top
        })
        res.success(newShop)
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
    }
}