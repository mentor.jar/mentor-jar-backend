import responseCode from "../../../base/responseCode"
import keys from "../../../config/env/keys"
import { UserDTO } from "../../../DTO/UserDTO"
import { AppFeedbackRepo } from "../../../repositories/AppFeedbackRepo"
import { cloneObj } from "../../../utils/helper"
import { addLink } from "../../../utils/stringUtil"

const appFeedbackRepo = AppFeedbackRepo.getInstance()

export const getListFeedbackWithFilter = async (req: any, res: any, next: any) => {
    try {
        const { page, limit } = req.query
        const { filter, type } = req.body
        const data = await appFeedbackRepo.getListAppFeedback(type, filter, page, limit)
        res.success(data)
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
        return;
    }
}

export const detailAppFeedback = async (req: any, res: any, next: any) => {
    try {
       const { id } = req.params
       const result = await appFeedbackRepo.getDetail(id)
       if(!result[0]) {
           res.error(responseCode.NOT_FOUND.name, req.__("app_feedback.not_found"), responseCode.NOT_FOUND.code)
           return;
       }
       const appFeedback = cloneObj(result?.[0])
       if(appFeedback.user) {
            const { user } = appFeedback
            user.avatar = addLink(`${keys.host_community}/`, user.gallery_image?.url)
            appFeedback.user = new UserDTO(user).toSimpleUserInfo()
       }
       res.success(appFeedback)
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
        return;
    }
}