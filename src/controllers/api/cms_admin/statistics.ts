import responseCode from '../../../base/responseCode';
import keys from '../../../config/env/keys';
import { UserDTO } from '../../../DTO/UserDTO';
import { CategoryRepo } from '../../../repositories/CategoryRepo';
import { OrderItemRepository } from '../../../repositories/OrderItemRepo';
import { OrderRepository } from '../../../repositories/OrderRepo';
import { ProductRepo } from '../../../repositories/ProductRepo';
import { ShopRepo } from '../../../repositories/ShopRepo';
import { cloneObj } from '../../../utils/helper';
import { addLink } from '../../../utils/stringUtil';
import { handleTimeReq } from '../../handleRequests/shopStatistic/userTrends';
import generalAnalystService from "../../serviceHandles/shopStatistic/generalAnalyst";

const orderRepo = OrderRepository.getInstance()
const productRepo = ProductRepo.getInstance()
const shopRepo = ShopRepo.getInstance()
const categoryRepo = CategoryRepo.getInstance()
const orderItemRepo = OrderItemRepository.getInstance()

export const orderStatistic = async (req: any, res: any, next: any) => {
    try {
        const { start_time, end_time } = handleTimeReq(req);
        let chart = await generalAnalystService.orderChartCMS(start_time, end_time);
        res.success(chart);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const dashboardInfo = async (req: any, res: any, next: any) => {
    try {
        const totalOrder = await orderRepo.count()
        const totalProduct = await productRepo.count()
        const totalShop = await shopRepo.count()
        const totalSystemCategory = await categoryRepo.count(
            { $or: [{ shop_id: { $exists: false } }, { shop_id: null }] }
        )
        res.success({ totalOrder, totalProduct, totalShop, totalSystemCategory })
    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}

export const shopRanking = async (req: any, res: any, next: any) => {
    try {
        let shops = await orderRepo.getShopRanking()
        shops = shops.map(item => {
            item = cloneObj(item)
            if (item.shop && item.shop.user) {
                item.shop.user.avatar = addLink(`${keys.host_community}/`, item.shop.user.gallery_image.url)
                item.shop.user = new UserDTO(item.shop.user).toSimpleUserInfo();
                return item
            }
        })
        res.success(shops)
    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}

export const productRanking = async (req: any, res: any, next: any) => {
    try {
        const orderSuccessIDs = await orderRepo.getOrderSuccess()
        let products = await orderItemRepo.getProductRanking(orderSuccessIDs)
        products = products.map(item => {
            item = cloneObj(item)
            if (item.product && item.product.shop && item.product.shop.user) {
                let user = item.product.shop.user
                user.avatar = addLink(`${keys.host_community}/`, user.gallery_image.url)
                user = new UserDTO(user).toSimpleUserInfo();
                item.product.shop.user = user
                return item
            }
        })
        res.success(products)
    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}
