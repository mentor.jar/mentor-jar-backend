import { executeError } from "../../../base/appError";
import notificationService, { NotifyType } from "../../../services/notification";
import { NotificationDTO } from '../../../DTO/NotificationDTO';
import { NotifyCampaignDTO } from '../../../DTO/NotifyCampaignDTO';
import { UserDTO } from '../../../DTO/UserDTO';
import { addLink } from "../../../utils/stringUtil";
import keys from '../../../config/env/keys';

export const getSystemNotification = async (req: any, res: any, next: any) => {
    try {
        const { limit = 10, page = 1 } = req.query;
        const data = await notificationService.getNotifyCampaigns({
            limit,
            page,
        });

        data.data = data.data.map(item => NotifyCampaignDTO.parse(item))

        return res.success(data);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}


export const getSystemNotificationDetail = async (req: any, res: any, next: any) => {
    try {
        const { id } = req.params;

        const data = await notificationService.getNotifyCampaignDetail(id);
        const result: any = NotifyCampaignDTO.parse(data)
        result.receivers = result.receivers.map(
            receiver => {
                delete receiver.avatar;
                receiver.avatar = addLink(`${keys.host_community}/`, receiver?.gallery_image?.url)
                return (new UserDTO(receiver)).toSimpleJSON();
            });
        return res.success(result);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const createSystemNotification = async (req: any, res: any, next: any) => {
    try {
        const {
            type,
            title,
            content,
            topic,
            description,
            receivers,
            schedule,
            image,
            os,
            data,
            sound,
            time_will_push,
            context,
        } = req.body;

        const userId = req.user._id;

        let result = await notificationService.saveAndPushCampaign({
            type,
            title,
            description,
            content,
            topic,
            data,
            receivers,
            schedule,
            image,
            os,
            sound,
            context,
            createdBy: userId.toString(),
            timeWillPush: new Date(time_will_push*1000)
        });

        result = NotifyCampaignDTO.parse(result)

        return res.success(result);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}