import { RoleRepo } from "../../../repositories/RoleRepo";


const roleRepo = RoleRepo.getInstance();


export const createRole = async (req: any, res: any, next: any) => {
    try {
        const name = req.body.name;
        let role = await roleRepo.create({ name });
        res.success(role);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}



