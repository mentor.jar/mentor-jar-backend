import { executeError } from "../../../base/appError";
import { StreamSessionDTO } from "../../../DTO/StreamSessionDTO";
import { StreamSessionRepository } from "../../../repositories/StreamSessionRepository";
import { filterStreamSession } from "../../handleRequests/stream/streamSession";

let streamSessionRepository = StreamSessionRepository.getInstance();

export const getStreamSession = async (req: any, res: any, next: any) => {
    try {
        let streamSession = await streamSessionRepository.getStreamSession(req.params.id);
        streamSession = StreamSessionDTO.newInstance(streamSession).completeDataWithDTO(req.user);
        res.success(streamSession);
    } catch (error) {
        executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getListScheduledStream = async (req: any, res: any, next: any) => {
    try {
        let handleParam = filterStreamSession(req);
        const listStreamSession = await streamSessionRepository.getListScheduledStream(handleParam.paginate);
        listStreamSession.data = listStreamSession.data.map(session => StreamSessionDTO.newInstance(session).completeDataWithDTO());
        res.success(listStreamSession);
    } catch (error) {
        executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getListStreamSessionByUser = async (req: any, res: any, next: any) => {
    try {
        let handleParam = filterStreamSession(req);
        const shop_id = req.query.shop_id;
        const listStreamSession = await streamSessionRepository.getListStreamSessionByShop(shop_id, handleParam.paginate);
        listStreamSession.data = listStreamSession.data.map(session => StreamSessionDTO.newInstance(session).completeDataWithDTO(req.user));
        res.success(listStreamSession);
    } catch (error) {
        executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

