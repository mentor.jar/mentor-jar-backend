import { executeError } from "../../../base/appError";
import { RoomChatRepo } from "../../../repositories/RoomChatRepo";
import { handleRequestGetRoom } from "../../handleRequests/roomChat";
const roomChatRepo = RoomChatRepo.getInstance();
export const getListRoomChat = async (req: any, res: any, next: any) => {
    try {
        let params = handleRequestGetRoom(req);
        let listRoom = await roomChatRepo.getRoom(params);
        res.success(listRoom);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getRedDot = async (req: any, res: any, next: any) => {
    try {
        let red_dots = await roomChatRepo.getTotalRedDotOfUser(req.query.user_id);
        res.success(red_dots);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getDetailRoomChat = async (req: any, res: any, next: any) => {
    try {
        let room = await roomChatRepo.getRoomById(req.params.id, true);
        res.success(room);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}
