// const Category = require('../../models/Category');
import { CategoryRepo } from "../../../repositories/CategoryRepo"
import { CategoryInfoRepo } from "../../../repositories/CategoryInfoRepo"
import {
    createCategoryHandle,
    createShopCategoryCMSHandle,
    updateShopCategoryCMSHandle,
    paginationHandle,
    updateActiveCategoryHandle
} from "../../handleRequests/category/category"
import responseCode from "../../../base/responseCode"
import { cloneObj } from "../../../utils/helper";
import { ProductCategoryRepo } from "../../../repositories/ProductCategoryRepo";
import { ProductRepo } from "../../../repositories/ProductRepo";

const repository = CategoryRepo.getInstance();
const repositoryInfo = CategoryInfoRepo.getInstance();
const productCategoryRepo = ProductCategoryRepo.getInstance();
const productRepo = ProductRepo.getInstance();

export const getCategories = async (req: any, res: any, next: any) => {
    try {
        let paginate = paginationHandle(req);
        const isAdmin = (req.query.is_admin === 'true');
        const result = await repository.getListPagination(paginate, req.query.search, isAdmin);
        res.success(result);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }

}

export const createCategory = async (req: any, res: any, next: any) => {
    try {
        const categoryObject = await createCategoryHandle(req);
        const category = await repository.create(categoryObject);
        res.success(category);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    }
}

export const createShopCategoryCMS = async (req: any, res: any, next: any) => {
    try {
        const categoryObject = await createShopCategoryCMSHandle(req);
        const category = await repository.create(categoryObject);

        // Add product if has
        let productIds = req.body.product_ids
        if (productIds && productIds.length) {
            await productCategoryRepo.createProductCategoryRecord(category._id, productIds)
        }
        else {
            productIds = await productCategoryRepo.getProductIdList(category._id)
        }

        const productItems = await productRepo.find({ _id: { $in: productIds }, deleted_at: null, is_approved: "approved" })

        const resCategory = cloneObj(category)
        resCategory.products = productItems

        res.success(resCategory);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    }
}

export const getCategoriesById = async (req: any, res: any, next: any) => {
    const id = req.params.id;
    try {
        const result = await repository.getTreeById(id);
        res.success(result);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    }
}

export const updateCategory = async (req: any, res: any, next: any) => {
    try {
        const categoryObject = await createCategoryHandle(req);
        const category = await repository.update(req.params.id, categoryObject);
        res.success(category);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode)
    }
}
export const deleteCategory = async (req: any, res: any, next: any) => {
    try {
        const allCategoryId = req.listCategoryId;
        await repository.deleteCategories(allCategoryId);
        await repositoryInfo.deleteCategoriesInfo(allCategoryId);
        res.success(null, responseCode.SUCCESS.name, responseCode.SUCCESS.code);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    }
}

export const updateActiveCategory = async (req: any, res: any, next: any) => {
    try {
        const categoryObject = await updateActiveCategoryHandle(req);
        const category = await repository.update(req.params.id, categoryObject);
        res.success(category);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode)
    }
}

export const getCategoriesByShop = async (req: any, res: any, next: any) => {
    const shop_id = req.params.id;
    const { page = 1, limit = 20 } = req.query;
    try {
        const result = await repository.getShopCategoryByShopId(shop_id, page, limit);
        res.success(result);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode)
    }
}

export const getShopCategoriesById = async (req: any, res: any, next: any) => {
    const id = req.params.id;
    const { page = 1, limit = 20 } = req.query;
    try {
        const productIds = await productCategoryRepo.getProductIdList(id);
        const productItems = await productRepo.getProductByShopCategory(productIds, page, limit);
        res.success(productItems);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    }
}

export const updateShopCategoryCMS = async (req: any, res: any, next: any) => {
    try {
        const infoUpdate = await updateShopCategoryCMSHandle(req);
        const category = await repository.update(req.params.id, infoUpdate);

        // Update product_list
        const newProductIds = req.body.product_ids
        if (newProductIds) {
            await productCategoryRepo.updateProductCategoryRecord(category._id, newProductIds)
        }

        const productItems = await productRepo.find({ _id: { $in: newProductIds }, deleted_at: null, is_approved: "approved" })

        const resCategory = cloneObj(category)
        resCategory.products = productItems

        res.success(resCategory);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    }
}

export const deleteShopCategoryCMS = async (req: any, res: any, next: any) => {
    try {
        await repository.delete(req.params.id);
        await productCategoryRepo.deleteByCategoryId(req.params.id);
        res.success(null, responseCode.SUCCESS.name, responseCode.SUCCESS.code);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    }
}

export const updateActiveShopCategory = async (req: any, res: any, next: any) => {
    try {
        const categoryObject = {
            is_active: req.body.is_active || false
        }
        const category = await repository.update(req.params.id, categoryObject);
        res.success(category);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode)
    }
}

export const getDetailShopCategoryCMS = async (req: any, res: any, next: any) => {
    const id = req.params.id;
    try {
        const category = await repository.findById(id);
        const productIds = await productCategoryRepo.getProductIdList(id);
        const productItems = await productRepo.find({ _id: { $in: productIds }, deleted_at: null, is_approved: "approved" });
        const resCategory = cloneObj(category)
        resCategory.products = productItems
        res.success(resCategory);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    }
}

