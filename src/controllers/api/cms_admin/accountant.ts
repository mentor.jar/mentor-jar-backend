import { ShippingStatus } from '../../../models/enums/order';
import { OrderRepository } from '../../../repositories/OrderRepo';
import exportExcelService from '../../../services/excel';
import { TransactionDto } from '../../../services/excel/dto';
import { COLUMN_TRANSACTION_ACCOUNTANT } from '../../../services/excel/variable';
import { toDateTime } from '../../../utils/dateUtil';
import { cloneObj } from '../../../utils/helper';
import { handleTimeReq } from '../../handleRequests/shopStatistic/userTrends';

let orderRepo = OrderRepository.getInstance();

const toShippingStatus = (status) => {
    let result = null;
    switch (status) {
        case 'wait_to_pick':
            result = 'Chờ lấy hàng';
            break;

        case 'shipped':
            result = 'Đã giao hàng';
            break;

        case 'shipping':
            result = 'Đang giao hàng';
            break;

        case 'canceled':
            result = 'Đã huỷ đơn';
            break;

        case 'canceling':
            result = 'Đang chờ huỷ';
            break;
    }

    return result;
};

export const transactionsForAccountant = async (
    req: any,
    res: any,
    next: any
) => {
    try {
        const {
            shop_id: shopId,
            filter_type: filterType,
            order_number: orderNumber,
        } = req.query;
        const { start_time: dateStart, end_time: dateEnd } = handleTimeReq(req);

        let resultOrders: any = await orderRepo.getTransactionForAccountant({
            dateStart,
            dateEnd,
            orderNumber,
            shopId,
            filterType,
        });
        let dataExport = setDataTransactions(resultOrders);
        res.success(dataExport.BIDUTransactions);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
};

const setDataTransactions = (data) => {
    if (!data.length || data == null) return null;

    data = cloneObj(data);

    data = data.map((transaction) => {
        let systemVoucher = transaction.system_vouchers;
        let shopVoucher = transaction.shop_vouchers;
        let delivery =
            transaction.shipping_status === ShippingStatus.SHIPPED
                ? transaction.order_shipping?.history?.find(
                      (i) => i.status_id === 5
                  )?.action_time
                : null;
        let checkedTime =
            transaction.shipping_status === ShippingStatus.SHIPPED
                ? transaction.order_shipping?.history?.find(
                      (i) => i.status_id === 6
                  )?.action_time
                : null;

        const transactionDto = new TransactionDto()
            .setShopId(transaction.shop_id)
            .setShopName(transaction.shop.user.userName)
            .setOrderNumber(transaction.order_number)
            .setOrderShippingId(
                transaction.order_shipping?.shipping_info?.label
            )
            .setCreatedAt(toDateTime(transaction.created_at))
            .setBuyerId(transaction.user._id)
            .setBuyerName(transaction.user.nameOrganizer.userName)
            .setTotalPriceProduct(transaction.payment_info.total_price_product)
            .setShippingFee(transaction.payment_info.shipping_fee)
            .setSystemVoucherDiscount(
                systemVoucher?.find((i) => i.classify == 'discount')?.discount
            )
            .setSystemVoucherShippingFee(
                systemVoucher?.find((i) => i.classify == 'free_shipping')
                    ?.discount
            )
            .setShopVoucherDiscount(shopVoucher[0]?.discount)
            .setTotalVoucherDiscount(
                transaction.payment_info?.total_voucher_discount
            )
            .setTotalPrice(transaction.payment_info?.total_price)
            .setDeliveryDate(toDateTime(delivery))
            .setCheckedTime(toDateTime(checkedTime))
            .setShippingName(transaction.shipping_method.name)
            .setShippingStatus(toShippingStatus(transaction.shipping_status))
            .get();

        return transactionDto;
    });

    return { BIDUTransactions: data };
};

export const exportTransactionsForAccountant = async (
    req: any,
    res: any,
    next: any
) => {
    try {
        const {
            shop_id: shopId,
            filter_type: filterType,
            order_number: orderNumber,
        } = req.query;
        const { start_time: dateStart, end_time: dateEnd } = handleTimeReq(req);

        let resultOrders: any = await orderRepo.getTransactionForAccountant({
            dateStart,
            dateEnd,
            orderNumber,
            shopId,
            filterType,
        });
        let dataExport = setDataTransactions(resultOrders);
        await exportExcelService.transactionAccountant(
            res,
            COLUMN_TRANSACTION_ACCOUNTANT,
            dataExport,
            dateStart,
            dateEnd
        );
    } catch (error) {
        console.log(error);
        res.error(error.name, error.message, error.statusCode);
    }
};
