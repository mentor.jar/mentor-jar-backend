import { executeError } from "../../../base/appError";
import keys from "../../../config/env/keys";
import { UserDTO } from "../../../DTO/UserDTO";
import { SellerSignUpRepo } from "../../../repositories/SellerSignUpRepo"
import { ShopRepo } from "../../../repositories/ShopRepo";
import { getTranslation, getUserLanguage } from "../../../services/i18nCustom";
import notificationService, { NotifyRequestFactory } from "../../../services/notification";
import { NOTIFY_TARGET_TYPE, NOTIFY_TYPE } from "../../../services/notification/constants";
import { cloneObj, ObjectId } from "../../../utils/helper";
import { addLink } from "../../../utils/stringUtil";

const repositories = SellerSignUpRepo.getInstance();
const shopRepo = ShopRepo.getInstance();

export const getListSellerRegister = async (req: any, res: any, next: any) => {
    try {
        const { status, page, limit } = req.query
        let results:any = await repositories.findAllRequest(status, page, limit)
        results = cloneObj(results)
        results.data = results.data.map(item => {
            item.user_mapped = item.user_mapped.map(user => {
                user.avatar = addLink(`${keys.host_community}/`, user.gallery_image?.url)
                return new UserDTO(user).toSimpleUserInfo()
            })
            return item
        })
        res.success(results)
    } catch (error) {
        executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getDetailSellerRegister = async (req: any, res: any, next: any) => {
    try {
        const { id } = req.params
        let results:any = await repositories.getDetail(id);
        results.user_mapped = results.user_mapped.map(user => {
            user.avatar = addLink(`${keys.host_community}/`, user.gallery_image?.url)
            return new UserDTO(user).toSimpleUserInfo()
        })

        res.success(results)
    } catch (error) {
        executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const handleSellerRegister = async (req: any, res: any, next: any) => {
    try {
        const { user } = req.body
        const { register } = req
        const shop:any = await shopRepo.findOne({user_id: ObjectId(user?._id)})
        const country = register?.personal_info?.country || "VN"
        let shopUserId = null
        let shop_id = null
        if(!shop) {
            let shopUser: any = await shopRepo.findOrCreateByUser(user, country, true);
            shopUser = cloneObj(shopUser)
            await shopRepo.actionAfterCreateShop(user._id, shopUser?._id, country);
            shop_id = shopUser._id
            shopUserId = shopUser.user_id
        }
        else {
            let shopUser:any = await shopRepo.update(shop?._id, {
                is_approved: true
            })
            shop_id = shopUser._id
            shopUserId = shopUser.user_id
        }

        const seller = await repositories.update(register._id, {
            status: "approved",
            approved_time: new Date(),
            shop_id: shop_id ? ObjectId(shop_id) : null
        })
        await shopRepo.update(shop_id,{
            bank_info: seller.bank_info
        })
        const language = await getUserLanguage(shopUserId);
        notificationService.saveAndSend(
            NotifyRequestFactory.generate(
                NOTIFY_TYPE.CREATE_SHOP_APPROVED,
                NOTIFY_TARGET_TYPE.BUYER,
                {
                    title: getTranslation(language, 'notification.bidu_notification'),
                    content: getTranslation(language, 'notification.create_shop_approved'),
                    receiverId: user._id.toString(),
                    shopId: shop_id.toString(),
                    userId: user._id.toString()
                }
            ))

        res.success()
    } catch (error) {
        executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const rejectSellerRegister = async (req: any, res: any, next: any) => {
    try {
        const { user } = req.body
        const { register } = req
        const shop:any = await shopRepo.findOne({user_id: ObjectId(user?._id)})

        let shop_id = null
        if(shop) {
            let shopUser:any = await shopRepo.update(shop?._id, {
                is_approved: false
            })
            shop_id = shopUser._id
        }

        await repositories.update(register._id, {
            status: "rejected",
            rejected_time: new Date(),
            shop_id: shop_id ? ObjectId(shop_id) : null
        })

        const language = await getUserLanguage(shop.user_id);
        // send notification rejected
        await notificationService.saveAndSend(
            NotifyRequestFactory.generate(
                NOTIFY_TYPE.CREATE_SHOP_REJECTED,
                NOTIFY_TARGET_TYPE.BUYER,
                {
                    title: getTranslation(language, 'notification.bidu_notification'),
                    content: getTranslation(language, 'notification.create_shop_rejected'),
                    receiverId: user._id.toString(),
                    shopId: shop_id.toString(),
                    userId: user._id.toString()
                }
            ))
        res.success()
    } catch (error) {
        executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}