import exportExcelService from '../../../services/excel';
import { COLUMN_RATING_SHOP, COLUMN_SHOP, COLUMN_TRANSACTION } from '../../../services/excel/variable';
import { dateToString } from '../../../utils/dateUtil';
import { handleTimeReq } from '../../handleRequests/shopStatistic/userTrends';
import generalAnalystService from "../../serviceHandles/shopStatistic/generalAnalyst";


export const totalShopMetric = async (req: any, res: any, next: any) => {
    try {
        const { start_time, end_time } = handleTimeReq(req);
        let statistics = await generalAnalystService._columnChartTotalShop(start_time, end_time);
        res.success(statistics);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const totalShopExport = async (req: any, res: any, next: any) => {
    try {
        const { start_time, end_time } = handleTimeReq(req);
        let statistics = await generalAnalystService.totalShopPeriod(start_time, end_time);
        await exportExcelService.common(res,
            COLUMN_SHOP,
            statistics);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const shopByCategoriesMetric = async (req: any, res: any, next: any) => {
    try {
        const { start_time, end_time } = handleTimeReq(req);
        let statistics = await generalAnalystService._columnChartShopByCategories(start_time, end_time);
        res.success(statistics);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const shopByCategoriesExport= async (req: any, res: any, next: any) => {
    try {
        const { start_time, end_time } = handleTimeReq(req);
        let statistics = await generalAnalystService.shopByCategoriesPeriod(start_time, end_time);
        await exportExcelService.common(res,
            COLUMN_SHOP,
            statistics);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const ratingShopMetric = async (req: any, res: any, next: any) => {
    try {
        const { start_time, end_time } = handleTimeReq(req);
        let statistics = await generalAnalystService.ratingShopPeriod(start_time, end_time);
        res.success(statistics);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const ratingShopExport = async (req: any, res: any, next: any) => {
    try {
        const { start_time, end_time } = handleTimeReq(req);
        let statistics = await generalAnalystService.ratingShopExport(start_time, end_time);
        await exportExcelService.common(res,
            COLUMN_RATING_SHOP,
            statistics);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const totalShopRevenue = async (req: any, res: any, next: any) => {
    try {
        const { start_time, end_time } = handleTimeReq(req);
        const statistics = await generalAnalystService.getTotalRevenue(start_time, end_time);
        res.success(statistics);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const exportRevenueByCategories = async (req: any, res: any, next: any) => {
    try {
        const { start_time, end_time } = handleTimeReq(req);
        const statistics = await generalAnalystService.getRevenueByCategoriesExport(start_time, end_time);
        await exportExcelService.revenue(res,
            COLUMN_TRANSACTION(),
            statistics);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const exportRevenue = async (req: any, res: any, next: any) => {
    try {
        const { start_time, end_time } = handleTimeReq(req);
        const statistics = await generalAnalystService.getRevenueExport(start_time, end_time);
        await exportExcelService.revenue(res,
            COLUMN_TRANSACTION(),
            statistics);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const overview = async (req: any, res: any, next) => {
    try {
        const { start_time, end_time } = handleTimeReq(req);
        const statistics: any = {};

        const [revenue_by_category, total_shop, transactions, shop_categories] = await Promise.all([
            generalAnalystService.getRevenueByCategories(start_time, end_time),
            generalAnalystService._pieChartTotalShop(start_time, end_time),
            generalAnalystService._pieChartTransaction(start_time, end_time),
            generalAnalystService._pieChartShopByCategories(start_time, end_time)
        ])

        statistics.revenue_by_category = revenue_by_category;
        statistics.total_shop = total_shop;
        statistics.transactions = transactions;
        statistics.shop_categories = shop_categories;

        res.success(statistics);
        
    }catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const orderQuantityStatistic = async (req: any, res: any, next) => {
    try {
        const { start_time, end_time } = handleTimeReq(req);
        const statistics = await generalAnalystService._columnChartTransaction(start_time, end_time);
        res.success(statistics);
        
    }catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const shopTransactionStatistic = async (req: any, res: any, next) => {
    try {
        const { start_time, end_time } = handleTimeReq(req);
        const statistics = await generalAnalystService.shopTransaction(start_time, end_time);
        res.success(statistics);
        
    }catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const transactionExport = async (req: any, res: any, next) => {
    try {
        const { start_time, end_time } = handleTimeReq(req);
        const statistics = await generalAnalystService.getOrderExport(start_time, end_time);
        await exportExcelService.transaction(res,
            COLUMN_TRANSACTION,
            statistics);
        
    }catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const shopSpending = async (req: any, res: any, next) => {
    try {
        const { start_time, end_time } = handleTimeReq(req);
        const statistics = await generalAnalystService.getShopSpending(start_time, end_time);
        res.success(statistics);
        
    }catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const transactionStatistic = async (req: any, res: any, next) => {
    try {
        const { start_time, end_time } = handleTimeReq(req);
        const statistics = await generalAnalystService.getOrderQuantity(start_time, end_time);
        res.success(statistics);
        
    }catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}