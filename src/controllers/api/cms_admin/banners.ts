import { filterShop } from "../../handleRequests/shop/shop";
import { ShopRepo } from "../../../repositories/ShopRepo";
import { executeError } from "../../../base/appError";
import BannerService, { CreateBannerRequest, CreateBannerSystemRequest, CreateTopShopBannerRequest, UpdateBannerRequest, UpdateBannerSystemRequest, UpdateTopShopBannerRequest } from "../../serviceHandles/banner";
import { filterBanner, filterBannerSystem, filterTopShopBanner } from "../../handleRequests/banner/banner";
import { cloneObj, isMappable, ObjectId } from "../../../utils/helper";

const shopRepo = ShopRepo.getInstance();

export const createBanner = async (req: any, res: any, next: any) => {
    try {
        const { name, image, promo_link, products, start_time, end_time, shop_id, description } = req.body;
        const request: CreateBannerRequest = {
            name,
            image,
            promo_link,
            products,
            start_time: new Date(start_time * 1000),
            end_time: new Date(end_time * 1000),
            shop_id,
            description
        };
        const banner = await BannerService._.createNewBanner(request);
        res.success(banner);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const createBannerSystem = async (req: any, res: any, next: any) => {
    try {
        const { name, image, promo_link, start_time, end_time, order, classify, images, position, advance_actions, priority, description } = req.body;
        const advanceOption:any = {
            shop: null,
            shops: [],
            category: null
        }
        let { products, vouchers } = req.body
        if (classify && classify === "product") {
            vouchers = []
        }
        if (classify && classify === "voucher") {
            products = []
        }
        if (classify && classify === "link") {
            products
        }
        if (classify && classify === "shop_view") {
            advanceOption.shop = ObjectId(advance_actions.shop)
        }
        if (classify && classify === "shop_list") {
            advanceOption.shops = advance_actions.shop.map(id => ObjectId(id))
        }
        if(classify && classify === "category_view") {
            advanceOption.category = ObjectId(advance_actions.category)
        }
        const request: CreateBannerSystemRequest = {
            name,
            image,
            images,
            promo_link,
            products,
            start_time: new Date(start_time * 1000),
            end_time: new Date(end_time * 1000),
            shop_id: null,
            order,
            classify,
            position,
            vouchers,
            advance_actions: advanceOption,
            priority: +priority || 1,
            description
        };
        const banner = await BannerService._.createBannerSystem(request);
        res.success(banner);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const createTopShopBanner = async (req: any, res: any, next: any) => {
    try {
        const { name, images  } = req.body;
        const shop_ids = req.shop_ids;

        const request: CreateTopShopBannerRequest = {
            name,
            images,
            shop_ids,
        };

        let banner: any = await BannerService._.createTopShopBanner(request);
    
        banner = cloneObj(banner);

        const { _id, is_active } = banner;
        
        Promise.all(
            shop_ids.map(async (id) => {
                shopRepo.update(id, {
                    system_banner: {
                        id: _id,
                        name,
                        images,
                        is_active,
                    },
                });
            })
        );

        res.success(banner);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const updateBanner = async (req: any, res: any, next: any) => {
    try {
        const { name, image, promo_link, products, start_time, end_time, description } = req.body;

        const request: UpdateBannerRequest = {
            name,
            image,
            promo_link,
            products,
            start_time: new Date(start_time * 1000),
            end_time: new Date(end_time * 1000),
            description
        };
        const id = req.params.id;
        const banner = await BannerService._.updateBanner(id, request);

        res.success(banner);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const updateBannerSystem = async (req: any, res: any, next: any) => {
    try {
        const { name,image, promo_link, start_time, end_time, order, classify, images, position, advance_actions, priority, description } = req.body;
        let { products, vouchers } = req.body
        const advanceOption:any = {
            shop: null,
            shops: [],
            category: null
        }
        if (classify && classify === "product") {
            vouchers = []
        }
        if (classify && classify === "voucher") {
            products = []
        }
        if (classify && classify === "link") {
            products
        }
        if (classify && classify === "shop_view") {
            advanceOption.shop = ObjectId(advance_actions.shop)
        }
        if (classify && classify === "shop_list") {
            advanceOption.shops = advance_actions.shop.map(id => ObjectId(id))
        }
        if(classify && classify === "category_view") {
            advanceOption.category = ObjectId(advance_actions.category)
        }
        const request: UpdateBannerSystemRequest = {
            name,
            image,
            images,
            promo_link,
            products,
            start_time: new Date(start_time * 1000),
            end_time: new Date(end_time * 1000),
            order,
            classify,
            position,
            vouchers,
            advance_actions: advanceOption,
            priority: +priority || 1,
            description
        };

        const id = req.params.id;
        const banner = await BannerService._.updateBannerSystem(id, request);
        res.success(banner);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const updateTopShopBanner = async (req: any, res: any, next: any) => {
    try {
        const { name, images, is_active} = req.body;
        let shop_ids = []
        if (isMappable(req.body?.shop_ids)) {
            shop_ids = await Promise.all(
                req.body?.shop_ids?.map(async (id) => shopRepo.findById(id))
            );
            shop_ids = shop_ids.map(shop => shop._id) || [];
        }

        const request: UpdateTopShopBannerRequest = {
            name,
            images,
            shop_ids,
            is_active: is_active || true,
        };

        const id = req.params.id;
        const banner = await BannerService._.updateTopShopBanner(id, request);

        res.success(banner);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const deleteBanner = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id;
        await BannerService._.deleteBanner(id);
        res.success(true);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const deleteTopShopBanner = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id;
        await BannerService._.deleteTopShopBanner(id);
        res.success(true);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const listBanner = async (req: any, res: any, next: any) => {
    try {
        const handleParams = filterBanner(req);
        const banners = await BannerService._.getAllBanners(handleParams);
        res.success(banners);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const listTopSopBanner = async (req: any, res: any, next: any) => {
    try {
        const handleParams = filterTopShopBanner(req);
        const banners = await BannerService._.getAllTopShopBanner(handleParams);
        res.success(banners);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const listBannerSystem = async (req: any, res: any, next: any) => {
    try {
        const handleParams = filterBannerSystem(req);
        const banners = await BannerService._.getAllBannerSystem(handleParams);
        res.success(banners);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getBannerDetail = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id;
        const banner = await BannerService._.getBannerByIdCMS(id);
        res.success(banner);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getTopShopBannerDetail = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id;
        const banner = await BannerService._.getTopShopBannerByIdCMS(id);
        res.success(banner);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const setActiveBanner = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id;
        const { is_active } = req.body;
        const instance = {
            is_active: is_active
        }
        const banner = await BannerService._.setActiveBanner(id, instance);
        res.success(banner);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

