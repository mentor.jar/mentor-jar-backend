import { ProductRepo } from './../../../repositories/ProductRepo';
import { ShopRepo } from "../../../repositories/ShopRepo";
import responseCode from '../../../base/responseCode';
import { GroupBuyService } from '../../serviceHandles/groupBuy/service';


const shopRepo = ShopRepo.getInstance()
const productRepo = ProductRepo.getInstance()
const groupBuyService = GroupBuyService.getInstance()

export const approveGroupBuyOfShop = async (req, res , next) => {
    try {
        const { group_buy_id, shop_id } = req.body
        const getShop: any = await groupBuyService.approveGroupBuyOfShop(shop_id,group_buy_id)
        res.success(getShop)
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    }
}