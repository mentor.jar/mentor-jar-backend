import { PaymentMethodRepo } from "../../../repositories/PaymentMethodRepo";

const paymentMethodRepo = PaymentMethodRepo.getInstance();

export const getAllMethods = async (req: any, res: any, next: any) => {
    const result = await paymentMethodRepo.getAllPaymentMethod();
    res.success(result);
}