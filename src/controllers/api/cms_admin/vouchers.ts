import responseCode from "../../../base/responseCode";
import keys from "../../../config/env/keys";
import { UserDTO } from "../../../DTO/UserDTO";
import { VoucherDTO } from "../../../DTO/VoucherDTO";
import { VoucherRepository } from "../../../repositories/VoucherRepository"
import { cloneObj } from "../../../utils/helper";
import { addLink } from "../../../utils/stringUtil";
import { createVoucherCMSHandle, updateVoucherCMSHandle, updateVoucherHandle } from "../../handleRequests/vouchers/voucher"
import VoucherService from "../../serviceHandles/voucher";

let voucherRepo = VoucherRepository.getInstance();

export const createVoucher = async (req: any, res: any, next: any) => {
    try {
        const { target } = req.body;
        if(target === "introduce") {
            const voucherByTargets = await voucherRepo.getVoucherByTargetIntroduce(target);
            if (voucherByTargets[0] !== null) {
                res.error(responseCode.UNIQUE_DATA.name, req.__("controller.voucher.voucher_introduce_already_exist"), responseCode.UNIQUE_DATA.code)
                return;
            }
        }
        const voucherItem = await createVoucherCMSHandle(req)
        let voucher: any = await voucherRepo.create(voucherItem)
        voucher = new VoucherDTO(voucher).convertVoucher()
        // re-sync voucher to cache
        VoucherService._.syncVoucherSystem()
        res.success(voucher, responseCode.SUCCESS.name, responseCode.SUCCESS.code);

    } catch (error) {
        if (error.code === 11000) {
            res.error(responseCode.UNIQUE_DATA.name, req.__("controller.voucher.code_already_exist"), responseCode.UNIQUE_DATA.code)
        }
        else {
            res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
        }
    }
}

export const getListVoucher = async (req: any, res: any, next: any) => {
    try {
        let results = await voucherRepo.getVoucherCMS(req)
        results = cloneObj(results)
        let vouchers = cloneObj(results.data)
        results.data = vouchers.map(voucher => {
            return new VoucherDTO(voucher).convertVoucher()
        })
        res.success(results);
    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}

export const updateVoucher = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id
        const voucher = await voucherRepo.findById(id)
        if (voucher) {
            const voucherItem = await updateVoucherCMSHandle(req, voucher)
            let newVoucher: any = await voucherRepo.update(id, voucherItem)
            newVoucher = new VoucherDTO(newVoucher).convertVoucher()
            // re-sync voucher to cache
            VoucherService._.syncVoucherSystem()
            res.success(newVoucher, responseCode.SUCCESS.name, responseCode.SUCCESS.code);
        }
        else {
            res.error(responseCode.NOT_FOUND.name, req.__("controller.voucher.voucher_not_found"), responseCode.NOT_FOUND.code);
        }
    } catch (error) {
        if (error.code === 11000) {
            res.error(responseCode.UNIQUE_DATA.name, req.__("controller.voucher.code_already_exist"), responseCode.UNIQUE_DATA.code)
        }
        else {
            res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
        }
    }
}

export const deleteVoucher = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id;
        await voucherRepo.delete(id);
        res.success(null, responseCode.SUCCESS.name, responseCode.SUCCESS.code);
    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    };
}

export const endVoucher = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id;
        const updateInfo = {
            end_time: new Date()
        }
        const result: any = await voucherRepo.update(id, updateInfo);
        res.success(result, responseCode.SUCCESS.name, responseCode.SUCCESS.code);
    } catch (error) {
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code);
    }
}



export const getDetailVoucher = async (req: any, res: any, next: any) => {
    try {
        let voucher = await voucherRepo.getDetailVoucherCMS(req.params.id);
        if (voucher) {
            voucher = VoucherDTO.newInstance(voucher).completeDataWithVariantJSON();
            voucher.shops = voucher.shops.map(shop => {
                shop = cloneObj(shop)
                shop.user.avatar = addLink(`${keys.host_community}/`, shop.user.gallery_image.url)
                shop.user = new UserDTO(shop.user).toSimpleUserInfo();
                return shop;
            })
            res.success(voucher, responseCode.SUCCESS.name, responseCode.SUCCESS.code);
        }
        else {
            res.error(responseCode.NOT_FOUND.name, req.__("controller.voucher.voucher_not_found"), responseCode.NOT_FOUND.code);
        }
    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}



