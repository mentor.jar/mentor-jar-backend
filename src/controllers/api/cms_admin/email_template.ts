import responseCode from "../../../base/responseCode";
import { EmailTemplateDTO } from "../../../DTO/EmailTemplateDTO";
import { EmailTemplateRepo } from "../../../repositories/EmailTemplateRepo";

const emailTemplateRepo = EmailTemplateRepo.getInstance()

export const createEmailTemplate = async (req: any, res: any, next: any) => {
    try {
        const { name, sendgrid_template_id, content, variables } = req.body
        const template = await emailTemplateRepo.create({ name, sendgrid_template_id, content, variables })
        res.success(template)
    } catch (error) {
        if(error.code === 11000) {
            res.error(responseCode.SERVER.name, req.__("email_template.duplicate_sendgrid_id"), responseCode.SERVER.code)
            return;
        }
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
        return;
    }
}

export const updateEmailTemplate = async (req: any, res: any, next: any) => {
    try {
        const { name, sendgrid_template_id, content, variables } = req.body
        const { templateItem } = req
        const template = await emailTemplateRepo.update(templateItem._id, { name, sendgrid_template_id, content, variables })
        res.success(template)
    } catch (error) {
        if(error.code === 11000) {
            res.error(responseCode.SERVER.name, req.__("email_template.duplicate_sendgrid_id"), responseCode.SERVER.code)
            return;
        }
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
        return;
    }
}

export const getEmailTemplateList = async (req: any, res: any, next: any) => {
    try {
        const { page = 1, limit = 20 } = req.query
        const templateList:any = await emailTemplateRepo.getListEmailTemplate(page, limit)
        templateList.data = templateList.data.map(template => new EmailTemplateDTO(template).toSimpleJSON())
        res.success(templateList)
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
        return;
    }
}

export const deleteEmailTemplate = async (req: any, res: any, next: any) => {
    try {
        const { templateItem } = req
        emailTemplateRepo.update(templateItem._id, {
            deleted_at: new Date()
        })
        res.success()
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
        return;
    }
}