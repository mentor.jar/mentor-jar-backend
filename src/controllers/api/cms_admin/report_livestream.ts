import keys from "../../../config/env/keys";
import { ReportStreamDTO } from "../../../DTO/ReportStreamDTO";
import { UserDTO } from "../../../DTO/UserDTO";
import { ReportLivestreamRepository } from "../../../repositories/ReportLivestreamRepo";
import { cloneObj } from "../../../utils/helper";
import { addLink } from "../../../utils/stringUtil";

const reportLivestreamRepository = ReportLivestreamRepository.getInstance();
export const getAllReports = async (req: any, res: any, next: any) => {
    let results: any = await reportLivestreamRepository.getListReport();
    results = cloneObj(results)
    let reports = cloneObj(results.data)
    results.data = reports.map(report => {
        report = (new ReportStreamDTO(report)).toSimpleJSON();
        report.user_report.avatar = addLink(`${keys.host_community}/`, report.user_report.gallery_image.url)
        report.user_report = new UserDTO(report.user_report).toSimpleUserInfo();

        report.owner.avatar = addLink(`${keys.host_community}/`, report.owner.gallery_image.url)
        report.owner = new UserDTO(report.owner).toSimpleUserInfo();
        return report
    })
    res.success(results);
}
