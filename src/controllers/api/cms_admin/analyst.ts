import moment from "moment";
import responseCode from "../../../base/responseCode";
import { PaymentStatus, ShippingStatus } from "../../../models/enums/order";
import { OrderRepository } from "../../../repositories/OrderRepo";
import { ProductRepo } from "../../../repositories/ProductRepo";
import { ShopRepo } from "../../../repositories/ShopRepo";
import { InMemoryTrackingStore } from "../../../SocketStores/TrackingStore";
import { cloneObj } from "../../../utils/helper";

const shopRepo = ShopRepo.getInstance();
const orderRepo = OrderRepository.getInstance();
const trackingCache = InMemoryTrackingStore.getInstance();
const productRepo = ProductRepo.getInstance();


export const getAnalystEcommerce = async (req: any, res: any, next: any) => {
    try {
        const last30Minutes = moment(new Date()).add(-30, 'minute')
        const sellers = await shopRepo.find({
            is_approved: true,
            pause_mode: false
        })
        const sellerIDs = sellers.map(item => item._id)
        const [totalOrderAndRevenue, accessAnalyst] = await Promise.all([
            (async () => {
                let data:Array<any> = await orderRepo.find({
                    shipping_status: ShippingStatus.SHIPPED,
                    payment_status: PaymentStatus.PAID,
                    shop_id: { $in: sellerIDs }
                })
                data = cloneObj(data)
                const totalRevenue = data.reduce((sum, item) => sum += item.total_price, 0)
                return {
                    totalOrder: data.length,
                    totalRevenue
                }
            })(),
            (async () => {
                const currentData:Array<any> = await trackingCache.get('accessAnalyst') || []
                return currentData
            })()
        ])
        const accessByTime = accessAnalyst.map(strTime => moment(strTime))
        const validAccess = accessByTime.filter(item => last30Minutes.isSameOrBefore(item))

        const result = [
            { name: "Số lượt truy cập", subName: 'trong 30 phút gần nhất', key: "total_view", value: validAccess.length },
            { name: "Số người bán", subName: '', key: "total_seller", value: sellers.length },
            { name: "Số lượt giao dịch", subName: 'chỉ bao gồm giao dịch thành công', key: "total_order", value: totalOrderAndRevenue.totalOrder },
            { name: "Tổng giá trị giao dịch", subName: 'chỉ bao gồm giao dịch thành công', key: "total_success_order", value: totalOrderAndRevenue.totalRevenue }
        ]
        res.success(result)
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
        return;
    }
}

export const getAnalystEcommerceV2 = async (req: any, res: any, next: any) => {
    try {
        const last30Minutes = moment(new Date()).add(-30, 'minute')
        const lastNewDates = moment().utcOffset(0).set({
            year: 2022, date: 1, month: 0, hour: 0, minute: 0, second: 0
        })

        const sellers = await shopRepo.find({
            is_approved: true,
            pause_mode: false
        })
        const newSellers = sellers.filter((seller:any) => {

            const shopCreatedAt = moment(seller.createdAt)
            if(shopCreatedAt.isSameOrAfter(lastNewDates)) {
                return seller
            }
        })

        const sellerIDs = sellers.map(item => item._id)
        const [totalOrderAndRevenue, accessAnalyst, productAnalyst] = await Promise.all([
            (async () => {
                let orders:Array<any> = await orderRepo.find({
                    shop_id: { $in: sellerIDs }
                })
                orders = cloneObj(orders)
                

                const successOrder = orders.filter(order => order.shipping_status === ShippingStatus.SHIPPED && order.payment_status === PaymentStatus.PAID)
                const unSuccessOrder = orders.filter(order => order.shipping_status === ShippingStatus.CANCELED || order.shipping_status === ShippingStatus.RETURN)
                const totalRevenue = orders.reduce((sum, item) => sum += item.total_price, 0)
                const totalSuccessRevenue = successOrder.reduce((sum, item) => sum += item.total_price, 0)
                const totalUnSuccessRevenue = unSuccessOrder.reduce((sum, item) => sum += item.total_price, 0)
                return {
                    totalOrder: orders.length,
                    successOrder: successOrder.length,
                    unSuccessOrder: unSuccessOrder.length,
                    inProgressOrder: orders.length - (successOrder.length + unSuccessOrder.length),
                    totalRevenue,
                    totalSuccessRevenue,
                    totalUnSuccessRevenue,
                    inProgressRevenue: totalRevenue - (totalSuccessRevenue + totalUnSuccessRevenue)
                }
            })(),
            (async () => {
                const currentData:Array<any> = await trackingCache.get('accessAnalyst') || []
                return currentData
            })(),
            (async () => {
                const activeProducts = await productRepo.find({
                    deleted_at: null,
                    is_approved: 'approved',
                    allow_to_sell: true,
                    quantity: {$gt: 0},
                    is_sold_out: false
                })

                const newProducts = activeProducts.filter((product:any) => {
                    const shopCreatedAt = moment(product.createdAt)
                    if(shopCreatedAt.isSameOrAfter(lastNewDates)) {
                        return product
                    }
                }) 
                return {
                    totalProduct: activeProducts.length,
                    newProduct: newProducts.length
                }
            })()
        ])
        const accessByTime = accessAnalyst.map(strTime => moment(strTime))
        const validAccess = accessByTime.filter(item => last30Minutes.isSameOrBefore(item))

        const result = [
            { name: "Số lượt truy cập", subName: 'trong 30 phút gần nhất', key: "total_view", value: validAccess.length },
            { name: "Số người bán", subName: '', key: "total_seller", value: sellers.length },
            { name: "Số người bán mới", key: 'total_new_seller', subName: 'Từ 01/01/2022', value: newSellers.length},
            { name: "Số lượt giao dịch", subName: '', key: "total_order", 
                value: {
                    total: totalOrderAndRevenue.totalOrder,
                    success: totalOrderAndRevenue.successOrder,
                    unSuccess: totalOrderAndRevenue.unSuccessOrder,
                    inProgress: totalOrderAndRevenue.inProgressOrder
                } 
            },
            { name: "Tổng giá trị giao dịch", subName: '', key: "total_order_revenue", 
                value: {
                    total: totalOrderAndRevenue.totalRevenue,
                    success: totalOrderAndRevenue.totalSuccessRevenue,
                    unSuccess: totalOrderAndRevenue.totalUnSuccessRevenue,
                    inProgress: totalOrderAndRevenue.inProgressRevenue
                } 
            },
            { name: "Số sản phẩm", subName: 'Chỉ bao gồm sản phẩm hợp lệ', key: "total_product", value: productAnalyst.totalProduct },
            { name: "Số sản phẩm mới", subName: 'Từ 01/01/2022', key: 'total_new_product', value: productAnalyst.newProduct},
        ]
        res.success(result)
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
        return;
    }
}

export const getAnalystEcommerceFinal = async (req: any, res: any, next: any) => {
    try {
        const last30Minutes = moment(new Date()).add(-30, 'minute')
        const lastNewDates = moment().utcOffset(0).set({
            year: 2022, date: 1, month: 0, hour: 0, minute: 0, second: 0
        })

        const sellers = await shopRepo.find({
            is_approved: true,
            pause_mode: false
        })
        const newSellers = sellers.filter((seller:any) => {

            const shopCreatedAt = moment(seller.createdAt)
            if(shopCreatedAt.isSameOrAfter(lastNewDates)) {
                return seller
            }
        })

        const sellerIDs = sellers.map(item => item._id)
        const [totalOrderAndRevenue, totalAccess, productAnalyst] = await Promise.all([
            (async () => {
                let orders:Array<any> = await orderRepo.find({
                    shop_id: { $in: sellerIDs }
                })
                orders = cloneObj(orders)
                

                const successOrder = orders.filter(order => order.shipping_status === ShippingStatus.SHIPPED && order.payment_status === PaymentStatus.PAID)
                const unSuccessOrder = orders.filter(order => order.shipping_status === ShippingStatus.CANCELED || order.shipping_status === ShippingStatus.RETURN)
                const totalRevenue = orders.reduce((sum, item) => sum += item.total_price, 0)
                const totalRevenueInYear = orders.filter(order => {
                    return moment(order.created_at).isSameOrAfter(lastNewDates)
                }).reduce((sum, item) => sum += item.total_price, 0)

                const totalSuccessRevenue = successOrder.reduce((sum, item) => sum += item.total_price, 0)
                const totalUnSuccessRevenue = unSuccessOrder.reduce((sum, item) => sum += item.total_price, 0)
                return {
                    totalOrder: orders.length,
                    successOrder: successOrder.length,
                    unSuccessOrder: unSuccessOrder.length,
                    inProgressOrder: orders.length - (successOrder.length + unSuccessOrder.length),
                    totalRevenue,
                    totalRevenueInYear,
                    totalSuccessRevenue,
                    totalUnSuccessRevenue,
                    inProgressRevenue: totalRevenue - (totalSuccessRevenue + totalUnSuccessRevenue)
                }
            })(),
            (async () => {
                const totalAccess:Array<any> = await trackingCache.get('accessTotal') || 0
                return +totalAccess
            })(),
            (async () => {
                const activeProducts = await productRepo.find({
                    deleted_at: null,
                    is_approved: 'approved',
                    allow_to_sell: true,
                    quantity: {$gt: 0},
                    is_sold_out: false
                })

                const newProducts = activeProducts.filter((product:any) => {
                    const shopCreatedAt = moment(product.createdAt)
                    if(shopCreatedAt.isSameOrAfter(lastNewDates)) {
                        return product
                    }
                }) 
                return {
                    totalProduct: activeProducts.length,
                    newProduct: newProducts.length
                }
            })()
        ])

        const responseData = {
            "SoLuongTruyCap": totalAccess,
            "SoNguoiBan": sellers.length,
            "SoNguoiBanMoi": newSellers.length,
            "TongSoSanPham": productAnalyst.totalProduct,
            "SoSanPhamMoi": productAnalyst.newProduct,
            "SoLuongGiaoDich": totalOrderAndRevenue.totalOrder,
            "TongSoDonHangThanhCong": totalOrderAndRevenue.successOrder,
            "TongSoDongHangKhongThanhCong": totalOrderAndRevenue.unSuccessOrder,
            "TongGiaTriGiaoDich": totalOrderAndRevenue.totalRevenueInYear
        }
        res.success(responseData)
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
        return;
    }
}