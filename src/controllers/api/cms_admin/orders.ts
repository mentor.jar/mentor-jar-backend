import keys from "../../../config/env/keys";
import { AddressDTO } from "../../../DTO/AddressDTO";
import { OrderDTO } from "../../../DTO/OrderDTO";
import { UserDTO } from "../../../DTO/UserDTO";
import { AdminReviewStatus, CancelType, RefundMoneyStatus, RefundStatus, ShippingMethodQuery, ShippingStatus } from "../../../models/enums/order";
import AppUserRepo from "../../../repositories/AppUserRepo";
import { OrderRepository } from "../../../repositories/OrderRepo"
import { UserRepo } from "../../../repositories/UserRepo";
import notificationService, { NotifyRequestFactory } from "../../../services/notification";
import { NOTIFY_TARGET_TYPE, NOTIFY_TYPE } from "../../../services/notification/constants";
import { cloneObj, isMappable, ObjectId } from "../../../utils/helper";
import { addLink } from "../../../utils/stringUtil";
import { getTranslation, getUserLanguage } from "../../../services/i18nCustom"
import { ShopRepo } from "../../../repositories/ShopRepo";
import { KOREA, PICK_ORDER_KO_STATUS, QUEUE_PRIORITY } from "../../../base/variable";
import responseCode from "../../../base/responseCode";
import { ShippingMethodRepo } from "../../../repositories/ShippingMethodRepo";
import { ShippingError } from "../../../base/customError";
import OrderService from "../../serviceHandles/order/order";
import GHTKService from "../../../services/3rd/shippings/GHTK";
import { executeError } from "../../../base/appError";
import { ProductRepo } from "../../../repositories/ProductRepo";
import { OrderItemRepository } from "../../../repositories/OrderItemRepo";
import { AdHoc, TargetType } from "../../../models/enums/TrackingObject";
import { TrackingObjectRepo } from "../../../repositories/TrackingObjectRepo";
import { approveRejectOrderQueue, handleUserAfterCancelOrder } from "../../../services/api/order";
import QueueService from "../../../services/queue";
import { PaymentMethodRepo } from "../../../repositories/PaymentMethodRepo";
import { OrderShippingRepo } from "../../../repositories/OrderShippingRepo";
import { GroupOrderRepo } from "../../../repositories/GroupOrderRepo";
import { updateQuantityCancelOrder } from "../mobile_app/orders";

const shippingMethodRepo = ShippingMethodRepo.getInstance();
let orderRepo = OrderRepository.getInstance();
const userRepo = UserRepo.getInstance();
const appUserRepo = AppUserRepo.getInstance();
const shopRepo = ShopRepo.getInstance();
const productRepo = ProductRepo.getInstance();
const orderItemRepo = OrderItemRepository.getInstance();
const trackingObjectRepo = TrackingObjectRepo.getInstance();
const paymentMethodRepo = PaymentMethodRepo.getInstance();
const orderShippingRepo = OrderShippingRepo.getInstance();
const groupOrderRepo = GroupOrderRepo.getInstance();

export const getDetailOrder = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id;
        let order = await orderRepo.getDetailOrderAdmin(id);
        order = cloneObj(order)
        if (isMappable(order?.system_vouchers)) {
            const otherOrders = order?.group_orders.orders.filter((item) => item._id !== id);
    
            order.group_orders = await Promise.all(
                otherOrders?.map(async (item) => {
                    let orderHandled = await orderRepo.getDetailOrder(item._id);
                    orderHandled =  new OrderDTO(orderHandled).orderBelongGroupOrder();
                    return orderHandled
                })
            )
        } else {
            order.group_orders = []
        }

        
        res.success(order);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const approveRejectOrder = async (req: any, res: any, next: any) => {
    try {
        const { is_approved } = req.body;
        let order = req.order
        const { _id: orderId, vouchers, group_order_id: groupOrderId } = order;

        const orderShipping: any = await orderShippingRepo.findOne({ order_id: orderId });
        if (orderShipping && orderShipping.shipping_info && orderShipping.shipping_info.label) {
            throw new Error(req.__("controller.order.can_not_cancel_order_delivered"));
        }

        const language = await getUserLanguage(order.user_id);

        let listVoucherSystem = [];
        if (isMappable(vouchers)) {
            listVoucherSystem = cloneObj(vouchers.filter((item) => item.classify && item.classify !== null))
        }

        let cancellableOrderList = [];
        if (isMappable(listVoucherSystem)) {
            let groupOrder = await groupOrderRepo.findById(groupOrderId.toString());
            groupOrder = cloneObj(groupOrder);

            const listOtherOrderBelongToGroupOrder = groupOrder?.orders?.filter(
                (item) => item._id.toString() !== orderId.toString()
            );
            
            const listOrderCanceledTogether = await Promise.all(
                listOtherOrderBelongToGroupOrder?.map(async (item: any) => 
                    orderRepo.findOne({ _id: item._id })
                )
            )

            cancellableOrderList = await Promise.all(
                listOrderCanceledTogether.map(async (item: any) => {
                    const orderShipping: any = await orderShippingRepo.findOne({ order_id: item._id });

                    if (orderShipping && orderShipping.shipping_info && orderShipping.shipping_info.label) {
                        return 0;
                    } else {
                        return item;
                    }
                })
            )

            cancellableOrderList = cancellableOrderList.filter((item) => item !== 0);     
        } 

        if (is_approved == true) {
            // send notification
            // const user = await userRepo.getUserByShopId(order.shop_id);
            await notificationService.saveAndSend(
                NotifyRequestFactory.generate(
                    NOTIFY_TYPE.ORDERS_ACCEPT,
                    NOTIFY_TARGET_TYPE.BUYER,
                    {
                        title: getTranslation(
                            language,
                            'notification.bidu_notification'
                        ),
                        content: getTranslation(
                            language,
                            'notification.buyer_order.order_accepted',
                            order.order_number
                        ),
                        receiverId: order.user_id.toString(),
                        orderId: order._id.toString(),
                        userId: order.user_id.toString(),
                        orderNumber: order.order_number,
                    }
                )
            );

            const languageSeller = await getUserLanguage(order.pick_address.accessible_id);
            await notificationService.saveAndSend(
                NotifyRequestFactory.generate(
                    NOTIFY_TYPE.ORDERS_ACCEPT,
                    NOTIFY_TARGET_TYPE.SELLER,
                    {
                        title: getTranslation(
                            languageSeller,
                            'notification.bidu_notification'
                        ),
                        content: getTranslation(
                            languageSeller,
                            'notification.seller_order.order_accepted',
                            order.order_number
                        ),
                        receiverId: order.pick_address.accessible_id,
                        orderId: order._id.toString(),
                        userId: order.pick_address.accessible_id,
                        orderNumber: order.order_number,
                    }
                )
            );

            order = await orderRepo.update(orderId, {
                shipping_status: ShippingStatus.WAIT_TO_PICK,
                approved_time: new Date(),
            });
        } else {
            order = await handleCanceledOrder(order, language);

            if (isMappable(listVoucherSystem) && isMappable(cancellableOrderList)) {
                Promise.all(
                    cancellableOrderList.map(async(order) => {
                        handleCanceledOrder(order, language);
                    })
                )
            }
        }

        // Insert Table TrackingObject Seller
        // const userShop = await userRepo.getUserByShopId(order.shop_id);
        // await trackingObjectRepo.createOrUpdateTrackingObject(userShop._id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);
        // Insert Table TrackingObject Buyer
        // await trackingObjectRepo.createOrUpdateTrackingObject(order.user_id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);

        res.success(order);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

const handleCanceledOrder = async (order, language) => {
    await notificationService.saveAndSend(
        NotifyRequestFactory.generate(
            NOTIFY_TYPE.ORDERS_CANCELED,
            NOTIFY_TARGET_TYPE.BUYER,
            {
                title: getTranslation(
                    language,
                    'notification.bidu_notification'
                ),
                content: getTranslation(
                    language,
                    'notification.buyer_order.order_canceled',
                    order.order_number
                ),
                receiverId: order.user_id.toString(),
                orderId: order._id.toString(),
                userId: order.user_id.toString(),
                orderNumber: order.order_number,
            }
        )
    );

    const orderCanceled = await orderRepo.update(order._id, {
        shipping_status: ShippingStatus.CANCELED,
        cancel_reason: 'Admin từ chối đơn hàng',
        cancel_type: CancelType.SYSTEM,
        cancel_time: Date.now(),
    });

    updateQuantityCancelOrder(order._id);

    handleUserAfterCancelOrder(order.user_id);

    return orderCanceled;
};

export const approveRejectMultyOrders = async (req: any, res: any, next: any) => {
    try {
        const { is_approved, order_lists } = req.body;
        const paymentMethods = await paymentMethodRepo.find({ name: { $nin: ['CASH'] } });
        const paymentIds = paymentMethods.map(e => e._id.toString());
        order_lists.forEach(orderID => {
            new QueueService().register(
                "handleApproveOrRejectOrder-" + process.env.NODE_ENV, 
                { is_approved, orderID, paymentIds }, 
                approveRejectOrderQueue,
                QUEUE_PRIORITY.LOW
            )
        });
        res.success("Processing");
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getListOrder = async (req: any, res: any, next: any) => {
    try {
        const { limit, page, shop_id: shopId, filter_type: filterType, order_number: orderNumber, date_start: dateStart, date_end: dateEnd } = req.query
        let results: any = await orderRepo.getOrderWithFilterCMS(dateStart, dateEnd, orderNumber, shopId, limit, page, filterType)
        results = cloneObj(results)
        results.data = results?.data?.map(item => new OrderDTO(item).toSimpleJSONForAdmin());

        res.success(results)
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

// Use for order from KO
export const updatePickOrderStatus = async (req: any, res: any, next: any) => {
    try {
        const { id } = req.params
        const { code } = req.body

        if (!code) {
            res.error(responseCode.SERVER.name, req.__("controller.order.missing_pick_order_status"), responseCode.SERVER.code)
            return;
        }
        const pickStatus = PICK_ORDER_KO_STATUS.filter(item => item.code === code)[0]
        if (!pickStatus) {
            res.error(responseCode.SERVER.name, req.__("controller.order.invalid_pick_order_status"), responseCode.SERVER.code)
            return;
        }

        const order: any = await orderRepo.findOne({ _id: ObjectId(id) })

        if (order && order.shipping_status === 'wait_to_pick') {
            const shop: any = await shopRepo.findOne({ _id: ObjectId(order.shop_id) })
            if (shop.country === KOREA) {
                let { pick_status } = order
                if (pick_status.filter(status => status.code === pickStatus.code).length) {
                    res.error(responseCode.SERVER.name, req.__("controller.order.already_in_status"), responseCode.SERVER.code)
                    return;
                }
                else {
                    pick_status.push({
                        ...pickStatus,
                        time: new Date()
                    })
                }
                const cloneOrder = {
                    ...order,
                    pick_status
                }
                const updatedOrder = await orderRepo.update(order._id, cloneOrder)
                res.success(updatedOrder)
            }
            else {
                res.error(responseCode.SERVER.name, req.__("controller.order.order_not_from_korea"), responseCode.SERVER.code)
                return;
            }
        }
        else {
            res.error(responseCode.SERVER.name, req.__("controller.order.not_found_or_invalid_order"), responseCode.SERVER.code)
            return;
        }
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

// Prepare order for shop
export const sellerCreateOrder = async (req: any, res: any, next: any) => {
    try {
        let createOrder;
        let result;
        let orderShop = req.orderShop;
        if (orderShop.shipping_status === ShippingStatus.WAIT_TO_PICK) {
            let shippingMethod = await shippingMethodRepo.findById(orderShop.shipping_method_id);
            switch (shippingMethod.name_query) {
                case ShippingMethodQuery.GIAOHANGTIETKIEM:

                    let shop;
                    // check shop turn on ghtk
                    if (req.user.shop) {
                        shop = req.user.shop
                    }
                    else {
                        // shop = await shopRepo.findOne({_id: ObjectId(orderShop.shop_id)})
                        shop = req.shop;
                    }
                    let isTurnOn = await shippingMethodRepo.checkShopTurnOnShippingMethod(shop, orderShop.shipping_method_id);
                    if (!isTurnOn) throw new ShippingError(req.__("controller.order.need_turn_on_ghtk"));

                    const ghtkService = await GHTKService.getInstance();
                    const token = await ghtkService.getTokenGHTKByShop(shop);
                    createOrder = await ghtkService.createOrder(orderShop, token, req.addressShop);
                    result = await OrderService._.saveShippingInfoToOrderShipping(orderShop, createOrder, shippingMethod.name_query);

                    break;
                default:
                    throw new ShippingError(req.__("controller.order.unsupport_shipping_method"));
            }
            return res.success(createOrder, req.__("controller.order.add_to_ghtk_success"))
        }

        throw new ShippingError(req.__("controller.order.not_wait_to_pick"));
    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
    }
}

export const updateRefundMoneyStatus = async (req: any, res: any, next: any) => {
    const { info, indexObj, order } = req
    let refundInfos = req.refundInfos
    const { item_id } = req.body;

    try {

        info.status = RefundStatus.CLOSED;

        info.refund_money = {
            ...info.refund_money,
            status: RefundMoneyStatus.COMPLETED,
            completed_time: new Date()
        }

        refundInfos[indexObj] = info;
        let newRefundInfos = {
            refund_information: refundInfos
        }

        const updatedOrder = await orderRepo.update(order._id, newRefundInfos);

        const orderItemEl: any = await orderItemRepo.findById(item_id.toString());
        const product = await productRepo.findById(orderItemEl.product_id.toString());
        const userShop = await userRepo.getUserByShopId(order.shop_id);
        const languageSeller = await getUserLanguage(userShop._id);
        const language = await getUserLanguage(order.user_id);

        // send notification to buyer
        await notificationService.saveAndSend(
            NotifyRequestFactory.generate(
                NOTIFY_TYPE.REFUND_ADMIN_COMPLETED,
                NOTIFY_TARGET_TYPE.BUYER,
                {
                    title: getTranslation(language, 'notification.bidu_notification'),
                    content: getTranslation(language, 'notification.refund_admin_completed', product.name, order.order_number),
                    receiverId: order.user_id.toString(),
                    orderId: order._id.toString(),
                    userId: order.user_id.toString(),
                    orderNumber: order.order_number,
                    itemId: item_id,
                    itemName: product.name
                }
            ))

        // send notification to seller
        await notificationService.saveAndSend(
            NotifyRequestFactory.generate(
                NOTIFY_TYPE.REFUND_ADMIN_COMPLETED,
                NOTIFY_TARGET_TYPE.SELLER,
                {
                    title: getTranslation(languageSeller, 'notification.bidu_notification'),
                    content: getTranslation(languageSeller, 'notification.refund_admin_completed', product.name, order.order_number),
                    receiverId: userShop._id.toString(),
                    orderId: order._id.toString(),
                    userId: order.user_id.toString(),
                    orderNumber: order.order_number,
                    itemId: item_id,
                    itemName: product.name
                }
            ))

        res.success(updatedOrder)
    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
    }
}

export const approveRejectRefundRequest = async (req: any, res: any, next: any) => {
    const { is_approved, reject_reason, item_id } = req.body;
    const order = req.order;
    const indexObj = req.indexObj;
    const info = req.info;
    let refundInfos = req.refundInfos;

    try {
        let orderItemEl: any, userShop:any, language:any
        [orderItemEl, userShop, language] = await Promise.all([
            (async () => {
                const data = await orderItemRepo.findById(item_id.toString());
                return data
            })(),
            (async () => {
                const data = await userRepo.getUserByShopId(order.shop_id);
                return data
            })(),
            (async () => {
                const data = await getUserLanguage(order.user_id);
                return data
            })()
        ])

        const [product, languageSeller] = await Promise.all([
            (async () => {
                const data = await productRepo.findById(orderItemEl.product_id.toString());
                return data
            })(),
            (async () => {
                const data = await getUserLanguage(userShop._id);
                return data
            })()
        ])

        if (is_approved) {
            info.admin_review = { ...info.admin_review, status: AdminReviewStatus.APPROVED, time: new Date() }

            // send notification to buyer
            notificationService.saveAndSend(
                NotifyRequestFactory.generate(
                    NOTIFY_TYPE.REFUND_ADMIN_APPROVED,
                    NOTIFY_TARGET_TYPE.BUYER,
                    {
                        title: getTranslation(language, 'notification.bidu_notification'),
                        content: getTranslation(language, 'notification.refund_admin_approved', product.name, order.order_number),
                        receiverId: order.user_id.toString(),
                        orderId: order._id.toString(),
                        userId: order.user_id.toString(),
                        orderNumber: order.order_number,
                        itemId: item_id,
                        itemName: product.name
                    }
                ))

            // send notification to seller
            notificationService.saveAndSend(
                NotifyRequestFactory.generate(
                    NOTIFY_TYPE.REFUND_ADMIN_APPROVED,
                    NOTIFY_TARGET_TYPE.SELLER,
                    {
                        title: getTranslation(languageSeller, 'notification.bidu_notification'),
                        content: getTranslation(languageSeller, 'notification.refund_admin_approved', product.name, order.order_number),
                        receiverId: userShop._id.toString(),
                        orderId: order._id.toString(),
                        userId: order.user_id.toString(),
                        orderNumber: order.order_number,
                        itemId: item_id,
                        itemName: product.name
                    }
                ))

        }
        else {
            info.admin_review = {
                ...info.shop_review,
                status: AdminReviewStatus.REJECTED,
                time: new Date(),
                reasons: reject_reason?.reasons || [],
                images: reject_reason?.images || [],
                video: reject_reason?.video || null,
                note: reject_reason?.note || null,
                email: reject_reason?.email || null
            }
            info.refund_money = { ...info.refund_money, status: RefundMoneyStatus.NONE }
            info.status = RefundStatus.CLOSED


            // send notification to buyer
            notificationService.saveAndSend(
                NotifyRequestFactory.generate(
                    NOTIFY_TYPE.REFUND_ADMIN_REJECTED,
                    NOTIFY_TARGET_TYPE.BUYER,
                    {
                        title: getTranslation(language, 'notification.bidu_notification'),
                        content: getTranslation(language, 'notification.refund_admin_rejected', product.name, order.order_number),
                        receiverId: order.user_id.toString(),
                        orderId: order._id.toString(),
                        userId: order.user_id.toString(),
                        orderNumber: order.order_number,
                        itemId: item_id,
                        itemName: product.name
                    }
                ))

            // send notification to seller
            notificationService.saveAndSend(
                NotifyRequestFactory.generate(
                    NOTIFY_TYPE.REFUND_ADMIN_REJECTED,
                    NOTIFY_TARGET_TYPE.SELLER,
                    {
                        title: getTranslation(languageSeller, 'notification.bidu_notification'),
                        content: getTranslation(languageSeller, 'notification.refund_admin_rejected', product.name, order.order_number),
                        receiverId: userShop._id.toString(),
                        orderId: order._id.toString(),
                        userId: order.user_id.toString(),
                        orderNumber: order.order_number,
                        itemId: item_id,
                        itemName: product.name
                    }
                ))
        }
        refundInfos[indexObj] = info;
        let instance = {
            refund_information: refundInfos
        }

        const orderUpdate = await orderRepo.update(order._id, instance);

        // Insert Table TrackingObject Seller
        // await trackingObjectRepo.createOrUpdateTrackingObject(userShop._id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);
        // Insert Table TrackingObject Buyer
        // await trackingObjectRepo.createOrUpdateTrackingObject(order.user_id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);

        res.success(orderUpdate);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }

}

export const markPenaltyForAnOrder = async (req: any, res: any, next: any) => {
    try {
        const { order } = req
        orderRepo.update(order._id, { is_mark_penalty: true })
        res.success()
    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
    }
}

