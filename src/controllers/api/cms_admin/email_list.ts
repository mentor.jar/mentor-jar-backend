import responseCode from "../../../base/responseCode";
import keys from "../../../config/env/keys";
import { EmailListDTO } from "../../../DTO/EmailListDTO";
import { EmailListRepo } from "../../../repositories/EmailListRepo";
import { ShopRepo } from "../../../repositories/ShopRepo";
import { UserRepo } from "../../../repositories/UserRepo";
import { sendEmailMKTFromList } from "../../serviceHandles/emails";


const shopRepo = ShopRepo.getInstance();
const userRepo = UserRepo.getInstance();
const emailListRepo = EmailListRepo.getInstance();


export const sendEmailMKT = async (req: any, res: any, next: any) => {
    try {
        let { from, to, dynamic_template_data } = req.body
        from = 'biducommunity@gmail.com'
        const { template } = req
        let targets = []
        if(to === 'seller') {
            targets = await shopRepo.getValidShopToSendEmail()
        }
        else if(to === 'buyer') {
            targets = await userRepo.getValidBuyerToSendEmail()
        }
        else if(to === 'all') {
            targets = await userRepo.getValidUserToSendEmail()
        }
        
        sendEmailMKTFromList(targets, dynamic_template_data, template)
        res.success()

    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
        return;
    }
}

export const getEmailList = async (req: any, res: any, next: any) => {
    try {
        const { type, page, limit } = req.query
        let result:any = await emailListRepo.getEmailListByType(type, page, limit)
        result.data = result.data.map(item => new EmailListDTO(item).toSimpleJSON())
        res.success(result)
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
        return;
    }
}

export const archiveEmail = async (req: any, res: any, next: any) => {
    try {
        const { emailItem } = req
        emailListRepo.update(emailItem._id, {
            deleted_at: new Date()
        })
        res.success()
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
        return;
    }
}