import { executeError } from "../../../base/appError";
import responseCode from "../../../base/responseCode";
import { CategoryRepo } from "../../../repositories/CategoryRepo";
import { ProductDetailInfoRepo } from "../../../repositories/ProductDetailInfo";
import { ProductRepo } from "../../../repositories/ProductRepo";
import { ShopRepo } from "../../../repositories/ShopRepo";
import { OptionTypeRepo } from "../../../repositories/Variant/OptionTypeRepo";
import { VoucherRepository } from "../../../repositories/VoucherRepository";
import { createProductDetailInfo, createProductHandle, filterProductByShop, matchImageToProduct, matchImageToProductUpdate, updateProductDetailInfo, updateProductDetailInfoInsde, updateProductHandle, updateWeightHandle } from "../../handleRequests/product/product";
import { createOptionTypeAndValue, createVariant } from "../mobile_app/variant"
import OrderService from "../../serviceHandles/order/order";
import { cloneObj } from "../../../utils/helper";
import { ProductDTO } from "../../../DTO/Product";
import { resetCacheAfterProductChange } from "../../serviceHandles/product";

const productRepository = ProductRepo.getInstance();
const productDetailInfoRepo = ProductDetailInfoRepo.getInstance();
const optionTypeRepo = OptionTypeRepo.getInstance();
const categoryRepo = CategoryRepo.getInstance();
const voucherRepo = VoucherRepository.getInstance();
const repository = ProductRepo.getInstance();

export const approveRejectProduct = async (req: any, res: any, next: any) => {
    try {
        const productApprove: any = {
            is_approved: req.body.is_approved || "pending"
        };
        const product = await productRepository.update(req.params.id, productApprove);
        res.success(product);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    };
}

export const getProductDetail = async (req: any, res: any, next: any) => {
    try {
        const product = await productRepository.getDetail(req.params.id);
        const optionTypes = await optionTypeRepo.getByProductId(req.params.id);
        const category = await categoryRepo.getCategory(product.category_id);
        product.option_types = optionTypes;
        product.last_category = category;

        const productDetailInfo = await productDetailInfoRepo.getByProductId(req.params.id);
        product.product_detail_infos = productDetailInfo;
        // const variants = await variantRepo.getByProductId(req.params.id);
        // Voucher of Shop
        const vouchers = await voucherRepo.getShopVoucherInProductDetail(req.params.id, product.shop._id);
        product.vouchers = vouchers;

        // simillar product
        let simillarProducts = await repository.findProductSimilar(
            {
                productId: product._id,
                userId: req.user?._id
            }, 1, 9);
        simillarProducts = cloneObj(simillarProducts)
        simillarProducts.data = simillarProducts.data.map(e => {
            let item:any = new ProductDTO(e);
            item = item.getProductComplete(req.user)
            // item.sold = e.order_items && e.order_items.length ? e.order_items[0].sold : 0
            delete item.order_items
            return item
        });
        product.simillarProducts = simillarProducts.data
        

        // suggest product
        let productSuggest: any = await repository.findProductHome({ orderBy: 'populate' }, 1, 9)
        productSuggest = cloneObj(productSuggest)
        productSuggest.data = productSuggest.data.map(e => {
            let item:any = new ProductDTO(e);
            item = item.getProductComplete(req.user)
            // item.sold = e.order_items && e.order_items.length ? e.order_items[0].sold : 0
            return  item
        });
        product.suggestProduct = productSuggest.data
        
        res.success(product);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getListProduct = async (req: any, res: any, next: any) => {
    try {
        let handleParam = filterProductByShop(req);
        const product = await productRepository.findFilter(handleParam.params, handleParam.paginate);
        res.success(product);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const createProduct = async (req: any, res: any, next: any) => {
    try {

        // Handler and build product
        const productObject = await createProductHandle(req);

        // Create product
        let product = await repository.create(productObject);

        const images = req.body.images
        // Add association between product and images
        await matchImageToProduct(product.id, images)

        // Create ProductDetailInfo if has
        const productDetailInfos = req.body.product_detail_infos
        if (productDetailInfos && productDetailInfos.length) {
            await createProductDetailInfo(product.id, productDetailInfos)
        }

        // Create Variant - for @Quy
        if (req.body.variants && req.body.variants.length) {
            await createOptionTypeAndValue(req.body.option_types, product._id)
            await createVariant(req.body.variants, product._id)
        }
        product = await repository.getDetail(product._id, req.user);
        res.success(product);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    };
}

export const updateProduct = async (req: any, res: any, next: any) => {
    try {
        // Handler and build product
        const productObject: any = await updateProductHandle(req);
        const oldProduct = await repository.findById(req.params.id);

        // Update product
        let product = await repository.update(req.params.id, productObject);

        const images = req.body.images
        // Add association between product and images
        await matchImageToProductUpdate(req.params.id, images, oldProduct.images)

        // Update ProductDetailInfo
        if (oldProduct.category_id != req.body.category_id) {
            const productDetailInfos = req.body.product_detail_infos
            await updateProductDetailInfo(oldProduct.id, productDetailInfos)
        } else {
            const productDetailInfos = req.body.product_detail_infos
            await updateProductDetailInfoInsde(oldProduct.id, productDetailInfos);
        }
        // Update Variant - for @Quy
        if (req.body.variants && req.body.variants.length) {
            await createOptionTypeAndValue(req.body.option_types, req.params.id)
            await createVariant(req.body.variants, req.params.id)
        }

        // reset some caches to make correct data
        resetCacheAfterProductChange(product._id)

        product = await repository.getDetail(req.params.id, req.user);
        res.success(product);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    };
}

export const updateWeightManyProduct = async (req: any, res: any, next: any) => {
    const { productListToUpdate } = req;
    try {
        const updatedListProducts = await updateWeightHandle(productListToUpdate);

        if( updatedListProducts.length > 0) {
            await OrderService._.updateWeightForOrderItem(updatedListProducts);
        }
       
        res.success(updatedListProducts);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    };
}

export const deleteProduct = async (req: any, res: any, next: any) => {
    try {
        const productId = req.params.id;
        await repository.delete(productId);
        
        // reset some caches to make correct data
        resetCacheAfterProductChange(productId)

        res.success(null, responseCode.SUCCESS.name, responseCode.SUCCESS.code);;
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    };
}

export const toggleDisplayFullModeTopPhoto = async (req: any, res: any, next: any) => {
    try {
        const { top_photo_display_full_mode } = req.body
        const { product } = req
        productRepository.update(product._id, { top_photo_display_full_mode })
        res.success()
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
    }
}
