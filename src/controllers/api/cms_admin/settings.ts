import responseCode from '../../../base/responseCode';
import { SystemSettingRepo } from '../../../repositories/SystemSettingRepo';
import { cloneObj } from '../../../utils/helper';

const systemSetting = SystemSettingRepo.getInstance();

export const toggleProductPriceMode = async (req, res, next) => {
    let settingVersion: any = await systemSetting.findOne({})
    const { is_allow_edit_product_price } = req.body
    const params = { is_allow_edit_product_price }
    const result = await systemSetting.update(settingVersion._id, params)
    res.success(result, responseCode.SUCCESS.name, responseCode.SUCCESS.code);
}

export const getSystemSetting = async (req, res, next) => {
    let settingVersion :any = await systemSetting.findOne({})
    settingVersion = cloneObj(settingVersion)
    const systemSettings = settingVersion
    res.success(systemSettings)
}
