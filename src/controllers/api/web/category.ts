import responseCode from "../../../base/responseCode";
import { CategoryRepo } from "../../../repositories/CategoryRepo";
import { ShopRepo } from "../../../repositories/ShopRepo";
import { sortByPriority } from "../../serviceHandles/categories"
import { Collection } from "../../../utils/collection";
import { ProductCategoryRepo } from "../../../repositories/ProductCategoryRepo";
import { ProductRepo } from "../../../repositories/ProductRepo";
import { cloneObj } from "../../../utils/helper";
import { VN_LANG } from "../../../base/variable";

const repository = CategoryRepo.getInstance();
const shopRepository = ShopRepo.getInstance();
const productCategoryRepo = ProductCategoryRepo.getInstance();
const productRepo = ProductRepo.getInstance();

export const categoryDetail = async (req: any, res: any, next: any) => {
    try {
        const language = req.headers["accept-language"] || VN_LANG
        const { id, slug } = req.query;
        const all = await repository.getTreeAll(language);
        let result = null;

        if (id) result = all?.find(e => e._id == id)
        else if (slug) result = all?.find(e => e.permalink == slug)

        res.success(result);
    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}

export const getCategoriesByShop = async (req: any, res: any, next: any) => {
    const shop: any = await shopRepository.findOne({ user_id: req.user._id });
    const { page = 1, limit = 10 } = req.query;
    try {
        let result = await repository.getTreeByShopId(shop._id);
        result.sort((a, b) => sortByPriority(a, b));
        let collection = new Collection(result);
        const arr = collection.pagination(page, limit);
        res.success(arr);
    } catch (error) {
        res.success([]);
    }
}

export const getDetailShopCategory = async (req: any, res: any, next: any) => {
    const id = req.params.id;
    try {
        const category = await repository.findById(id);
        const productIds = await productCategoryRepo.getProductIdList(id);
        const productItems = await productRepo.find({ _id: { $in: productIds }, deleted_at: null, is_approved: "approved" });
        const resCategory = cloneObj(category)
        resCategory.products = productItems
        res.success(resCategory);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    }
}





