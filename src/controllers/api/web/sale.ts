import responseCode from "../../../base/responseCode";
import { VoucherDTO } from "../../../DTO/VoucherDTO";
import { VoucherRepository } from "../../../repositories/VoucherRepository";
import { cloneObj, exclude } from "../../../utils/helper";

const voucherRepo = VoucherRepository.getInstance();

export const getVoucherWithFilterOfShop = async (req: any, res: any, next: any) => {
    try {
        const limit = req.query.limit
        const page = req.query.page
        const shopId = req.params.id
        const filterType = req.query.filter_type
        let results = await voucherRepo.getVoucherWithFilter(shopId, limit, page, filterType)
        results = cloneObj(results)
        let vouchers = cloneObj(results.data)
        results.data = vouchers.map(voucher => {
            return new VoucherDTO(voucher).convertVoucher()
        })
        res.success(results);
    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}

export const getVoucherSale = async (req: any, res: any, next: any) => {
    try {
        let results = await voucherRepo.getVoucherAllRunning()

        results = cloneObj(results)
        results = results.map(voucher => {
            return new VoucherDTO(voucher).convertVoucher()
        })

        const PREVIEW_SIZE = 6;

        const shopVoucher = results.filter(e => e.shop_id != null && e.shop_id != undefined).slice(0, PREVIEW_SIZE);
        results = exclude(results, shopVoucher.map(e => e._id))

        const freeShipVoucher = results.filter(e => e.classify == "free_shipping")?.slice(0, PREVIEW_SIZE)
        results = exclude(results, freeShipVoucher.map(e => e._id))

        const hotVoucher = results.sort((a, b) => b?.used_by?.length - a?.used_by?.length)?.slice(0, PREVIEW_SIZE);
        results = exclude(results, hotVoucher.map(e => e._id))

        const extremeVoucher = results
        .filter(e => e.type == "percent")
        ?.sort((a, b) => b?.value - a?.value)
        ?.slice(0, PREVIEW_SIZE);
        results = exclude(results, extremeVoucher.map(e => e._id))

        const partnerVoucher = [] // not able to implement yet

        res.success({
            freeShipVoucher,
            hotVoucher,
            extremeVoucher,
            shopVoucher,
            partnerVoucher
        });
    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}




