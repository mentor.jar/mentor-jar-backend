import keys from '../../../config/env/keys';
import responseCode from '../../../base/responseCode';
import { UserRepo } from '../../../repositories/UserRepo';
import { ShopRepo } from '../../../repositories/ShopRepo';
import { cloneObj, ObjectId } from '../../../utils/helper';
import { UserDTO } from '../../../DTO/UserDTO';
import VerifyService from '../../serviceHandles/user/verifyAccount';
import { InMemoryAuthUserStore } from '../../../SocketStores/AuthStore';
import { InMemoryMessageForRoomStore } from '../../../SocketStores/MessageStore';
import RoomChat from '../../../models/RoomChat';
import { RoomChatRepo } from '../../../repositories/RoomChatRepo';
import QueueService from '../../../services/queue';
import AppUserRepo from '../../../repositories/AppUserRepo';
import axios from "axios"

const userRepo = UserRepo.getInstance();
const shopRepository = ShopRepo.getInstance();
const authStore = InMemoryAuthUserStore.getInstance();
const roomStore = InMemoryMessageForRoomStore.getInstance();
const appUserRepo = AppUserRepo.getInstance();

export const sendVerifyToUser = async (req: any, res: any, next: any) => {
    try {
        const { user } = req;
        const { type, phone_number, email } = req.body;
        if (type && type.toUpperCase() === 'EMAIL') {
            if (user.email_verify.verified) {
                res.success(
                    responseCode.SUCCESS.name,
                    req.__("controller.user.already_verified_email"),
                    responseCode.SUCCESS.code
                );
                return;
            }
            // Update user email if social account
            let userUpdated = user
            if (!user.password && user.socialAccount) {
                const newUser = {
                    ...user,
                    email: email
                }
                userUpdated = await userRepo.update(user._id, newUser)
                console.log("userUpdated: ", userUpdated);

            }
            const result = await userRepo.sendVerifyEmail(user, email);
            if (result.success) {
                res.success();
            } else {
                res.error(
                    responseCode.SERVER.name,
                    req.__("controller.send_email_error"),
                    responseCode.SERVER.code
                );
            }
        } else if (type && type.toUpperCase() === 'PHONE') {
            if (user.phone_verify.verified) {
                res.error(
                    responseCode.BAD_REQUEST.name,
                    req.__("controller.user.already_verified_phone"),
                    responseCode.BAD_REQUEST.code
                );
                return;
            }
            const shop: any = await shopRepository.findOne({ user_id: user._id });
            if (shop?.country === "VN") {
                const result: any = await VerifyService._.sendVerifyPhone(
                    user,
                    phone_number
                );
                // ESMS
                if (result.CodeResult == '100') {
                    res.success(result);
                } else {
                    res.error(
                        responseCode.SERVER.name,
                        result.ErrorMessage,
                        responseCode.SERVER.code
                    );
                }
            } else {
                const result: any = await VerifyService._.sendVerifyPhoneTwillio(
                    user,
                    phone_number
                );
                // TWILIO
                if (result.errorCode == null) {
                    res.success(result);
                } else {
                    res.error(
                        responseCode.SERVER.name,
                        result.ErrorMessage,
                        responseCode.SERVER.code
                    );
                }
            }

            // SPEEDSMS
            // if (result.code == '00') {
            //     res.success(result);
            // } else {
            //     res.error(
            //         responseCode.SERVER.name,
            //         result.message,
            //         responseCode.SERVER.code
            //     );
            // }
        } else {
            res.error(
                responseCode.BAD_REQUEST.name,
                req.__("controller.user.invalid_verify_type"),
                responseCode.BAD_REQUEST.code
            );
        }
    } catch (error) {
        console.log(error);
        res.error(
            responseCode.SERVER.name,
            req.__("controller.error_occurs"),
            responseCode.SERVER.code
        );
    }
};

export const verify = async (req: any, res: any, next: any) => {
    try {
        let { user } = req;
        const { type, code } = req.body;
        user = cloneObj(user);
        const { email_verify, phone_verify } = user;
        if (type && code) {
            if (type.toUpperCase() === 'EMAIL') {
                if (email_verify.verified) {
                    res.error(
                        responseCode.BAD_REQUEST.name,
                        'Bạn đã xác thực email thành công trước đó',
                        responseCode.BAD_REQUEST.code
                    );
                    return;
                }
                if (
                    new Date(email_verify.expire).getTime() <
                    new Date().getTime()
                ) {
                    res.error(
                        responseCode.BAD_REQUEST.name,
                        req.__("controller.user.expired_verify_code"),
                        responseCode.BAD_REQUEST.code
                    );
                    return;
                }
                if (email_verify.code !== +code) {
                    res.error(
                        responseCode.BAD_REQUEST.name,
                        req.__("controller.user.wrong_verify_code"),
                        responseCode.BAD_REQUEST.code
                    );
                    return;
                }
                user.email_verify = {
                    expire: null,
                    code: null,
                    verified: true,
                };
                user = await userRepo.update(user._id, user);
                if (user.phone_verify.verified) {
                    // create shop
                    let shop: any = await shopRepository.findOrCreateByUser(
                        user
                    );
                    await shopRepository.actionAfterCreateShop(
                        user._id,
                        shop._id
                    );
                }
                res.success(new UserDTO(user).toSimpleUserInfo());
            } else {
                if (phone_verify.verified) {
                    res.error(
                        responseCode.BAD_REQUEST.name,
                        'Bạn đã xác thực số điện thoại thành công trước đó',
                        responseCode.BAD_REQUEST.code
                    );
                    return;
                }
                // if (
                //     new Date(phone_verify.expire).getTime() <
                //     new Date().getTime()
                // ) {
                //     res.error(
                //         responseCode.BAD_REQUEST.name,
                //         req.__("controller.user.expired_verify_code"),
                //         responseCode.BAD_REQUEST.code
                //     );
                //     return;
                // }
                // if (phone_verify.code !== +code) {
                //     res.error(
                //         responseCode.BAD_REQUEST.name,
                //         req.__("controller.user.wrong_verify_code"),
                //         responseCode.BAD_REQUEST.code
                //     );
                //     return;
                // }
                user.phone_verify = {
                    expire: null,
                    code: null,
                    verified: true,
                };

                user = await userRepo.update(user._id, user);
                if (user.phone_verify.verified) {
                    // create shop
                    let shop: any = await shopRepository.findOrCreateByUser(
                        user
                    );
                    await shopRepository.actionAfterCreateShop(
                        user._id,
                        shop._id
                    );
                }
                res.success(new UserDTO(user).toSimpleUserInfo());
            }
        } else {
            res.error(
                responseCode.BAD_REQUEST.name,
                req.__("controller.user.invalid_verify_code_or_verify_type"),
                responseCode.BAD_REQUEST.code
            );
        }
    } catch (error) {
        console.log(error);
        res.error(
            responseCode.SERVER.name,
            req.__("controller.error_occurs"),
            responseCode.SERVER.code
        );
    }
};

export const getVerifyStatus = async (req: any, res: any, next: any) => {
    try {
        let { user } = req;
        user = cloneObj(user);
        res.success({
            email_verified: user.email_verify
                ? user.email_verify.verified
                : false,
            phone_verified: user.phone_verify
                ? user.phone_verify.verified
                : false,
            email: user.email,
            phone: user.phoneNumber || null,
        });
    } catch (error) {
        console.log(error);
        res.error(
            responseCode.SERVER.name,
            req.__("controller.error_occurs"),
            responseCode.SERVER.code
        );
    }
};

export const getProductsBookmark = async (req: any, res: any, next: any) => {
    try {
        let { user } = req;
        user = cloneObj(user);
        
        res.success(user?.favorite_products || []);
    } catch (error) {
        console.log(error);
        res.error(
            responseCode.SERVER.name,
            req.__("controller.error_occurs"),
            responseCode.SERVER.code
        );
    }
};

const JobUpdateProfileInRedis = async (job, done) => {
    let user_id = job.data;
    let user = await userRepo.getUserById(
        ObjectId(user_id.toString())
    );
    await authStore.saveUser(user);
    let rooms: any = await RoomChatRepo.getInstance().find({
        users: {
            $elemMatch: { _id: user_id },
        }
    })
    let promises: any = [];
    let i = 1;
    for (let room of rooms) {
        promises.push(new Promise((res, rej) => {
            try {
                room.users = room.users.map(userRoom => {
                    if (userRoom._id.toString() == user_id) {
                        userRoom.userName = user.userName;
                        userRoom.avatar = user.avatar;
                        userRoom.email = user.email;
                        userRoom.gender = user.gender;
                    }
                    return userRoom;
                })
                room.messages = room.messages.map(message => {
                    if (message.user_id.toString() == user_id && message.user) {
                        message.user.userName = user.userName;
                        message.user.avatar = user.avatar;
                        message.user.email = user.email;
                        message.user.gender = user.gender;
                        message.user.shop_id = user.shop_id;
                    }
                    return message;
                })
                RoomChatRepo.getInstance().findOneAndUpdate({ _id: ObjectId(room._id) }, {
                    users: room.users,
                    messages: room.messages
                })
                roomStore.getRoom(room._id.toString()).then(roomDetail => {
                    if (roomDetail) {
                        roomDetail.users = room.users
                        roomDetail.messages = room.messages
                        roomStore.saveRoom(roomDetail);
                    }
                })
                res(true);
            } catch (error) {
                rej(error);
            }
        }));
    }
    console.log("Amount child job: " + rooms.length);
    await Promise.all(promises);
    console.log("done");
    done();
}
export const updateUserRedis = async (req: any, res: any, next: any) => {
    try {
        new QueueService().register("JobUpdateProfileInRedis-" + process.env.NODE_ENV, req.user._id.toString(), JobUpdateProfileInRedis)
        res.success(true);
    } catch (error) {
        console.log(error);
        res.error(
            responseCode.SERVER.name,
            req.__("controller.error_occurs"),
            responseCode.SERVER.code
        );
    }
};

export const updateLanguage = async (req: any, res: any, next: any) => {
    try {
        const { language, device_id } = req.body;
        const appUser: any = await appUserRepo.findOne({ deviceId: device_id, userId: req.user._id });
        const instance = await appUserRepo.update(appUser._id, { language: language });
        res.success(instance);
    } catch (error) {
        res.error(
            responseCode.SERVER.name,
            error.message,
            responseCode.SERVER.code
        );
    }

}


