import { executeError } from "../../../base/appError";
import { PaymentMethodRepo } from "../../../repositories/PaymentMethodRepo";

const paymentMethodRepo = PaymentMethodRepo.getInstance();

export const getAllMethods = async (req: any, res: any, next: any) => {
    const { is_group_buy } = req.query;
    let result = await paymentMethodRepo.getAllPaymentMethod();
    // Only return CASH
    result = result.filter(method => method.name.toUpperCase() === "CASH")
    res.success(result);
}

export const getAllMethodsV2 = async (req: any, res: any, next: any) => {
    const { is_group_buy } = req.query;
    let result = await paymentMethodRepo.getAllPaymentMethod();
    
    if(is_group_buy === 'true') {
        result = result.filter(method => method.name.toUpperCase() !== "CASH")
    }
    res.success(result);
}
