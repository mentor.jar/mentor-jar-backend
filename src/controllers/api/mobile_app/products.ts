// const Category = require('../../models/Category');
import { ProductRepo } from "../../../repositories/ProductRepo"
import { UserRepo } from "../../../repositories/UserRepo"
import { createProductHandle, updateProductHandle, matchImageToProduct, updateProductDetailInfo, matchImageToProductUpdate, createProductDetailInfo, filterProductByShop, updateProductDetailInfoInsde } from "../../handleRequests/product/product"
import responseCode from "../../../base/responseCode"
import { createOptionTypeAndValue, createVariant } from "./variant"
import { executeError } from "../../../base/appError"
import { cloneObj, ObjectId } from "../../../utils/helper"
import { ProductCategoryRepo } from "../../../repositories/ProductCategoryRepo"
import { ProductDTO } from "../../../DTO/Product"
import { getMinMax, resetCacheAfterProductChange, sortByTime } from "../../serviceHandles/product"
import IPaginateData from '../../../repositories/interfaces/IPaginateData';
import { UserDTO } from "../../../DTO/UserDTO"
import keys from "../../../config/env/keys"
import { addLink } from "../../../utils/stringUtil"
import { CategoryInfoRepo } from "../../../repositories/CategoryInfoRepo"
import { DETAIL_SIZE_NAME, CATEGORIES_LOCALIZED, VN_LANG, KO_LANG, EN_LANG, ARRAY_NAME_SIZE_INFOS, SHUFFLE_NEWEST_PRODUCT_PAGE } from "../../../base/variable"
import { PromotionProgramRepo } from "../../../repositories/PromotionProgramRepo"
import { detailProductFromBuyer } from "../../serviceHandles/product"
import CacheService from "../../../services/cache"
import { InMemoryProductStore } from "../../../SocketStores/ProductStore"
import { InMemoryAuthUserStore } from "../../../SocketStores/AuthStore"
import { ElasticQuery, elsIndexName } from "../../../services/elastic"
import { ProductElsDTO } from "../../../DTO/ElasticDTO/ProductDTO"
import setCacheHeaders from "../../../services/headerCache/headerCache"
import ProductService from "../../serviceHandles/product/service"
import UserService from "../../serviceHandles/user"

const productCache = InMemoryProductStore.getInstance();

const repository = ProductRepo.getInstance();
const userRepo = UserRepo.getInstance();
const productCategoryRepo = ProductCategoryRepo.getInstance();
const categoryInfoRepo = CategoryInfoRepo.getInstance();
const promotionRepo = PromotionProgramRepo.getInstance();
const productStore = InMemoryProductStore.getInstance();

export const createProduct = async (req: any, res: any, next: any) => {
    const language =
    [VN_LANG, EN_LANG, KO_LANG].indexOf(req.headers['accept-language']) > -1
        ? req.headers['accept-language']
        : VN_LANG;
    try {
        let { user } = req;
        user = cloneObj(user)

        // Handler and build product
        const productObject = await createProductHandle(req);

        // Create product
        let product: any = await repository.create(productObject);

        const images = req.body.images
        // Add association between product and images
        await matchImageToProduct(product.id, images)

        // Create ProductDetailInfo if has
        const productDetailInfos = req.body.product_detail_infos
        if (productDetailInfos && productDetailInfos.length) {
            await createProductDetailInfo(product.id, productDetailInfos)
        }

        // Create Variant - for @Quy
        if (req.body.variants && req.body.variants.length) {
            await createOptionTypeAndValue(req.body.option_types, product._id)
            await createVariant(req.body.variants, product._id)
        }

        // Save DetailSizeHistory in User
        const name = DETAIL_SIZE_NAME;
        const detailSize = product.detail_size;
        const categoryId = product.category_id;
        const fashionType = await categoryInfoRepo.getByCategoryIdAndName(categoryId, name);
         
        if (detailSize && detailSize.length > 0) {
            let data: any = []
            detailSize?.map(typeSize => {
                let size_infos: any = []
                let item = {
                    type_size: typeSize,
                    size_infos: size_infos
                }
                typeSize.size_infos.map(detail => {
                    item.size_infos.push(detail)
                })
                data.push(item)
            })
    
            user.detail_size_history = [
                {
                    fashion_type: fashionType,
                    data: detailSize
                }
            ]
        }

        if (user.shape_history.length <= 0 && +product.weight > 0) {
            user.shape_history.push({
                category_id: categoryId,
                weight: product?.weight ?? 0,
                height: product?.height ?? 0,
                width: product?.width ?? 0,
                length: product?.length ?? 0,
                time: new Date().getTime(),

            })
        } else {
            let exist = false;
            user.shape_history = user.shape_history.map(item => {
                if (item.category_id === categoryId.toString()) {
                    item.weight = product?.weight ?? 0;
                    item.height = product?.height ?? 0;
                    item.width = product?.width ?? 0;
                    item.length = product?.length ?? 0;
                    item.time = new Date().getTime();
                    exist = true;
                }
                return item;
            })

            if (!exist) {
                user.shape_history.push({
                    category_id: categoryId,
                    weight: product?.weight ?? 0,
                    height: product?.height ?? 0,
                    width: product?.width ?? 0,
                    length: product?.length ?? 0,
                    time: new Date().getTime(),
                })
            }
        }

        // Save the product detail info by category to user
        if(!user.category_info_history) {
            user.category_info_history = {}
            user.category_info_history[product.category_id] = productDetailInfos.map(item => {
                item.language = VN_LANG;
                return item
            })
        }
        else {
            user.category_info_history[product.category_id] = productDetailInfos.map(item => {
                item.language = VN_LANG;
                return item
            })
        }

        userRepo.update(user._id, user);

        product = await repository.getDetail(product._id, req.user);

        if(product.product_detail_infos) {
            product.product_detail_infos = product.product_detail_infos.map(categoryInfo => {
                categoryInfo = cloneObj(categoryInfo)
                categoryInfo.name = categoryInfo.category_info.name[language];
                categoryInfo.category_info.name = categoryInfo.category_info.name[language];
                return categoryInfo
            })
        }
        else {
            product.product_detail_infos = []
        }

        UserService._.clearUserInfoInCache(user._id.toString())

        res.success(product);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    };
}

export const getProductDetail = async (req: any, res: any, next: any) => {
    const language =
        [VN_LANG, EN_LANG, KO_LANG].indexOf(req.headers['accept-language']) > -1
            ? req.headers['accept-language']
            : VN_LANG;
    try {
        const product = await repository.getDetail(req.params.id, req.user, language);

        if(product.product_detail_infos) {
            product.product_detail_infos = product.product_detail_infos.map(categoryInfo => {
                categoryInfo = cloneObj(categoryInfo)
                categoryInfo.name = categoryInfo.category_info.name[language];
                categoryInfo.category_info.name = categoryInfo.category_info.name[language];
                return categoryInfo
            })
        }
        else {
            product.product_detail_infos = []
        }
        
        if (product.detail_size && Array.isArray(product.detail_size)) {
            product.detail_size = product.detail_size.map(item => {
                item.size_infos = item.size_infos.map(sizeInfo => {
                    const nameLocalized = ARRAY_NAME_SIZE_INFOS.filter( attributer => attributer.vi_name === sizeInfo.name || attributer.en_name === sizeInfo.name || attributer.ko_name === sizeInfo.name)?.[0]
                    sizeInfo.name = language === "vi" ? nameLocalized.vi_name : (language === "en" ? nameLocalized.en_name : (language === "ko" ? nameLocalized.ko_name : nameLocalized.vi_name))
                    return sizeInfo;
                })
                return item;
            }) 
        }
        
        if(!product.custom_images || !product.custom_images?.length) {
            product.custom_images = product.images.map(url => {
                return {
                    url,
                    x_percent_offset: null,
                    y_percent_offset: null
                }
            })
        }


        const users = await UserRepo.getInstance().getUserWithGallery(product.shop.user._id);
        if (!users.length)
            throw new Error('Không tìm thấy User');
        product.shop.user = new UserDTO(users[0]).toSimpleJSON();
        product.shop.user.avatar = addLink(`${keys.host_community}/`, users[0].gallery_image.url);

        res.success(product);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getDetailSize = async (req: any, res: any, next: any) => {
    try {
        const categoryId = req.body.category_id;
        const arraySize = req.body.array_size;
        const name = DETAIL_SIZE_NAME;
        const detailSizeHistory = req.user.detail_size_history;

        const fashionType = await categoryInfoRepo.getByCategoryIdAndName(categoryId, name);

        const detailSize = await repository.getDetailSize(`${fashionType}`, arraySize, detailSizeHistory)
        res.success(detailSize);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getDetailSizeV2 = async (req: any, res: any, next: any) => {
    const language =
    [VN_LANG, EN_LANG, KO_LANG].indexOf(req.headers['accept-language']) > -1
        ? req.headers['accept-language']
        : VN_LANG;

    try {
        const categoryId = req.body.category_id;
        const arraySize = req.body.array_size;
        const name = DETAIL_SIZE_NAME;
        const detailSizeHistory = req.user.detail_size_history;

        const fashionType = await categoryInfoRepo.getByCategoryIdAndName(categoryId, name);

        let detailSize = await repository.getDetailSizeV2(`${fashionType}`, arraySize, detailSizeHistory)
        detailSize = cloneObj(detailSize)
        if ( detailSize && Array.isArray(detailSize) && detailSize.length > 0) {
            detailSize = detailSize.map ((element) => {
                
                if ( element.size_infos && Array.isArray(element.size_infos) && element.size_infos.length > 0 ) {
                    
                    element.size_infos = element.size_infos.map((sizeInfo) => {
                        sizeInfo.name = language === KO_LANG ? sizeInfo.ko_name : sizeInfo.vi_name;
                        delete sizeInfo.vi_name;
                        delete sizeInfo.en_name;
                        delete sizeInfo.ko_name;
                        
                        return sizeInfo;
                    })
                    return element;
                }
            })
        }
           
        res.success(detailSize);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getShapeHistory = (req: any, res: any, next: any) => {
    try {
        let shape;
        const categoryId = req.params.category_id;
        const shapeHistory = req.user.shape_history;
        if (shapeHistory) {
            if (categoryId) {
                const shapeFound = shapeHistory.find(
                    (element) => element.category_id === categoryId
                );
                if (shapeFound) {
                    shape = {
                        weight: shapeFound.weight,
                        height: shapeFound.height,
                        width: shapeFound.width,
                        length: shapeFound.length,
                    };
                } else {
                    shapeHistory.sort((objectA, objectB) =>
                        sortByTime(objectA.time, objectB.time)
                    );

                    const { weight, height, width, length } =
                        shapeHistory.length > 0 && shapeHistory[0] !== null
                            ? shapeHistory[0]
                            : { weight: 0, height: 0, width: 0, length: 0 };

                    shape = {
                        weight,
                        height,
                        width,
                        length,
                    };
                }
            } else {
                shapeHistory.sort((objectA, objectB) =>
                    sortByTime(objectA.time, objectB.time)
                );

                const { weight, height, width, length } =
                    shapeHistory.length > 0 && shapeHistory[0] !== null
                        ? shapeHistory[0]
                        : { weight: 0, height: 0, width: 0, length: 0 };

                shape = {
                    weight,
                    height,
                    width,
                    length,
                };
            }
        } else {
            shape = {
                weight: 0,
                height: 0,
                width: 0,
                length: 0,
            };
        }
        res.success(shape);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getProductCategoryInfoHistory = async (req: any, res: any, next: any) => {
    try {
        const categoryId = req.params.category_id;
        const { user } = req;
        const language =
            [VN_LANG, EN_LANG, KO_LANG].indexOf(req.headers['accept-language']) > -1
                ? req.headers['accept-language']
                : VN_LANG;
        const productCategoryInfoHistory = user.category_info_history;
        if (productCategoryInfoHistory && categoryId) {
            const categoryInfos = productCategoryInfoHistory[categoryId]
            if(categoryInfos && categoryInfos.length) {
                let result = await categoryInfoRepo.getByCategoryId(categoryId);
                result = result.map((item) => {
                    item.name = item.name[language]
                    const productCategoryInfo = categoryInfos.find(ele => ele.category_info_id.toString() === item._id.toString())
                    if(productCategoryInfo) {
                        if(item.type === 'select') {
                            item.value = item.list_option.find(ele => ele[productCategoryInfo?.language] === productCategoryInfo?.value)?.[language] || null
                            if(productCategoryInfo.values) {
                                item.values = productCategoryInfo.values.map(val => {
                                    val = item.list_option.find(ele => ele[productCategoryInfo?.language] === val)?.[language] || null
                                    return val
                                })
                            }
                            else {
                                item.values = item?.value ? [ item?.value ] : []
                            }
                            
                            item.list_option = item.list_option.map(option => {
                                option = option[language]
                                return option
                            })
                        }
                        else if(item.type === 'custom') {
                            item.value = item.list_option.find(ele => ele[productCategoryInfo?.language] === productCategoryInfo?.value)?.[language] 
                            || productCategoryInfo?.value || null
                            if(productCategoryInfo.values) {
                                item.values = productCategoryInfo.values.map(val => {
                                    val = item.list_option.find(ele => ele[productCategoryInfo?.language] === val)?.[language] 
                                    || val || null
                                    return val
                                })
                                item.values = item.values.filter(ele => ele)
                                
                            }
                            else {
                                item.values = item?.value ? [ item?.value ] : []
                            }
                            item.list_option = item.list_option.map(option => {
                                option = option[language]
                                return option
                            })
                            if(item.values) {
                                item.list_option = item.list_option.concat(item.values)
                            }
                        }
                        else if(item.type === 'input') {
                            item.value = productCategoryInfo?.value || null
                            if(productCategoryInfo.values) {
                                item.values = productCategoryInfo.values.filter(val => val)
                            }
                            else {
                                item.values = item?.value ? [ item?.value ] : []
                            }
                        }
                    }
                    else {
                        item.list_option = item.list_option.map(option => {
                            option = option[language]
                            return option
                        })
                        item.value = null;
                        item.values = []
                    }

                    return item
                })
                res.success(result);
            }
            else {
                let result = await categoryInfoRepo.getByCategoryId(categoryId);
                result = result.map((item) => {
                    item.name = item.name[language]
                    item.list_option = item.list_option.map(option => {
                        option = option[language]
                        return option
                    }) 
                    item.value = null
                    item.values = []
                    return item
                })

                res.success(result);
            }
            
        } else {
            let result = await categoryInfoRepo.getByCategoryId(categoryId);
            result = result.map((item) => {
                item.name = item.name[language]
                item.list_option = item.list_option.map(option => {
                    option = option[language]
                    return option
                }) 
                item.value = null
                return item
            })

            res.success(result);
        }
        
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getProductByShopId = async (req: any, res: any, next: any) => {
    const language = req.headers["accept-language"] || VN_LANG
    try {
        let handleParam = filterProductByShop(req);
        let product = await repository.findFilter(handleParam.params, handleParam.paginate, req.user);
        product = cloneObj(product)
        if ( handleParam.paginate && product.data && Array.isArray(product.data) ) {
            product.data = product.data.map((item) => {
                const categoryLocalized = CATEGORIES_LOCALIZED.get(item.category.name.toLowerCase());
                categoryLocalized && categoryLocalized[language] ? item.category.name = categoryLocalized[language] : '';
                return item;
            })
        } else if ( product && Array.isArray(product) ) {
            product = product.map((item) => {
                const categoryLocalized = CATEGORIES_LOCALIZED.get(item.category.name.toLowerCase());
                categoryLocalized && categoryLocalized[language] ? item.category.name = categoryLocalized[language] : '';
                return item;
            })
        }
        res.success(product);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    };
}

export const getValidProductByShopId = async (req: any, res: any, next: any) => {
    try {
        let { page, limit } = req.query
        const shopID = req.user.shop._id.toString()
        if(!page) page = 1
        if(!limit) limit = 10
        let result = await repository.getValidProductByShopId(shopID, page, limit)
        result = cloneObj(result)
        result.data = result.data.map(product => {
            let item:any = new ProductDTO(product);
            item = item.getProductComplete(req.user)
            // item.sold = product.order_items && product.order_items.length ? product.order_items[0].sold : 0
            item.discount_percent = item.discount_percent ? item.discount_percent.discount : 0
            delete item.order_items
            return  item
        });
        res.success(result)
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
    }
}

export const getCountProduct = async (req: any, res: any, next: any) => {
    try {
        const product = await repository.countProduct(req.shop_id.toString());
        res.success(product);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    };
}

export const getSimilarProducts = async (req: any, res: any, next: any) => {
    try {
        setCacheHeaders(res, 30);
        const productId = req.params.id;
        const limit = req.query.limit || 20;
        const page = req.query.page || 1;
        let data: IPaginateData = await repository.findProductSimilar(
            {
                productId,
                userId: req.user?._id
            },
            page,
            limit);

        data = cloneObj(data)
        data.data = data.data.map(e => {
            let item: any = new ProductDTO(e);
            item = item.getProductComplete(req.user)
            // item.sold = e.order_items && e.order_items.length ? e.order_items[0].sold : 0
            delete item.order_items
            return item
        });
        res.success(data);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    };
}


export const getSimilarProductsV2 = async (req: any, res: any, next: any) => {
    try {
        setCacheHeaders(res, 30);
        let { page = 1, limit = 20, random_number = 0 } = req.query
        const productID = req.params.id;
        const categoryID = req.product.category_id;
        const categoryIDs = req.product.list_category_id
        let realPage = SHUFFLE_NEWEST_PRODUCT_PAGE?.[random_number]?.[page - 1]

        if(!realPage) {
            realPage = 1e9
        }

        let productSimilar: any = await productCache.get(`similarProductsByCate_${categoryID.toString()}`) // not implement
        if(!productSimilar || !productSimilar.length) {
            productSimilar = await ProductService._.getSimilarProductList(realPage, limit, productID, categoryID, categoryIDs);
            productSimilar.paginate.page = +page
            productSimilar.paginate.random_number = +random_number
        }
        else {
            const totalPage = productSimilar.length
            productSimilar = cloneObj(productSimilar).slice((realPage - 1) * limit, (realPage - 1) * limit + +limit)
            productSimilar.paginate = {
                "limit": limit,
                "total_page": Math.ceil(totalPage / +limit),
                "page": +page,
                "total_record": totalPage,
                "random_number": +random_number
            }
        }
        const promise = productSimilar.data.map((product) => {
            return new Promise(async (resolve, reject) => {
                try {
                    let item : any= new ProductDTO(product)
                    item = item.getProductComplete(req.user, true);
                    const promotion = await promotionRepo.getPromotionByProduct(item._id)
                    if (promotion) {
                        item.discount_percent = promotion.discount
                    }
                    else {
                        item.discount_percent = 0
                    }
                    resolve(item)
                } catch (error) {
                    reject(error)
                }
            })
        })
        let results:any =  await Promise.all(promise).then(data => {
            return data
        }).catch((error) => {
            console.log("Error: ", error.message);
            return []
        })
        productSimilar.data = results
        res.success(productSimilar);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    };
}

export const getProductByCatgoryId = async (req: any, res: any, next: any) => {
    try {
        const category_id = req.params.id
        const limit = req.query.limit || 20
        const page = req.query.page || 1
        const productIds = await productCategoryRepo.getProductIdList(category_id)
        let result = await repository.findProductListIncludingVariant(productIds, page, limit)
        result = cloneObj(result)
        let productItems = cloneObj(result.data)
        productItems = productItems.map(product => {
            if (product) {
                product = ProductDTO.newInstance(product)
                product.setUser(req.user);
                let simpleProduct = product.toJSON(['_id', 'name', 'before_sale_price', 'sale_price', 'quantity', 'shop_id', 'images']);
                let productOption = product.toJSON(['variants']);
                simpleProduct.price_min_max = getMinMax(productOption.variants);
                simpleProduct.is_bookmarked = !!product.checkBookmarkedProduct()
                return simpleProduct;
            }
        })
        result.data = productItems
        res.success(result);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    }
}

export const updateProduct = async (req: any, res: any, next: any) => {
    const language =
    [VN_LANG, EN_LANG, KO_LANG].indexOf(req.headers['accept-language']) > -1
        ? req.headers['accept-language']
        : VN_LANG;
    try {
        let { user } = req;
        user = cloneObj(user)

        // Handler and build product
        const productObject: any = await updateProductHandle(req);
        const oldProduct = await repository.findById(req.params.id);

        // Update product
        let product: any = await repository.update(req.params.id, productObject);

        if (!product.allow_to_sell) {
            productStore.deleteById(product?._id)
        }

        const images = req.body.images
        // Add association between product and images
        await matchImageToProductUpdate(req.params.id, images, oldProduct.images)

        // Update ProductDetailInfo
        const productDetailInfos = req.body.product_detail_infos
        await updateProductDetailInfo(oldProduct.id, productDetailInfos)
        
        // Update Variant - for @Quy
        if (req.body.variants && req.body.variants.length) {
            await createOptionTypeAndValue(req.body.option_types, req.params.id)
            await createVariant(req.body.variants, req.params.id)
        }

        // Save DetailSizeHistory in User
        const name = DETAIL_SIZE_NAME;
        const detailSize = product.detail_size;
        const categoryId = product.category_id;
        const fashionType = await categoryInfoRepo.getByCategoryIdAndName(categoryId, name);
         
        if (detailSize && detailSize.length > 0) {
            let data: any = []
            detailSize?.map(typeSize => {
                let size_infos: any = []
                let item = {
                    type_size: typeSize,
                    size_infos: size_infos
                }
                typeSize.size_infos.map(detail => {
                    item.size_infos.push(detail)
                })
                data.push(item)
            })
        }

        user.detail_size_history = [
            {
                fashion_type: fashionType,
                data: detailSize
            }
        ]

        if (user.shape_history.length <= 0 && +product.weight > 0) {
            user.shape_history.push({
                category_id: categoryId,
                weight: product?.weight ?? 0,
                height: product?.height ?? 0,
                width: product?.width ?? 0,
                length: product?.length ?? 0,
                time: new Date().getTime(),

            })
        } else {
            let exist = false;
            user.shape_history = user.shape_history.map(item => {
                if (item.category_id === categoryId.toString()) {
                    item.weight = product?.weight ?? 0;
                    item.height = product?.height ?? 0;
                    item.width = product?.width ?? 0;
                    item.length = product?.length ?? 0;
                    item.time = new Date().getTime();
                    exist = true;
                }
                return item;
            })

            if (!exist) {
                user.shape_history.push({
                    category_id: categoryId,
                    weight: product?.weight ?? 0,
                    height: product?.height ?? 0,
                    width: product?.width ?? 0,
                    length: product?.length ?? 0,
                    time: new Date().getTime(),
                })
            }
        }

        if(!user.category_info_history) {
            user.category_info_history = {}
            user.category_info_history[product.category_id] = productDetailInfos.map(item => {
                item.language = VN_LANG;
                return item
            })
        }
        else {
            user.category_info_history[product.category_id] = productDetailInfos.map(item => {
                item.language = VN_LANG;
                return item
            })
        }

        userRepo.update(user._id, user);

        // reset some caches to make correct data
        resetCacheAfterProductChange(product._id)

        product = await repository.getDetail(req.params.id, req.user);
        if(product.product_detail_infos) {
            product.product_detail_infos = product.product_detail_infos.map(categoryInfo => {
                categoryInfo = cloneObj(categoryInfo)
                categoryInfo.name = categoryInfo.category_info.name[language];
                categoryInfo.category_info.name = categoryInfo.category_info.name[language];
                return categoryInfo
            })
        }
        else {
            product.product_detail_infos = []
        }
        UserService._.clearUserInfoInCache(user._id.toString())
        res.success(product);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    };
}

export const deleteProduct = async (req: any, res: any, next: any) => {
    try {
        const productId = req.params.id;
        await repository.delete(productId);
        
        // reset some caches to make correct data
        resetCacheAfterProductChange(productId)

        res.success(null, responseCode.SUCCESS.name, responseCode.SUCCESS.code);;
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    };
}

export const showHideProduct = async (req: any, res: any, next: any) => {
    try {
        const productActive: any = {
            allow_to_sell: req.body.allow_to_sell || false
        };
        let product = await repository.update(req.params.id, productActive);
        product = cloneObj(product);
        if (!product.allow_to_sell) {
            resetCacheAfterProductChange(product?._id)
        }
        
        res.success(product);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    };
}


/**
 * ========================== BUYER PRODUCT API ========================== * 
 */

const handleByRequest = (product, user, language) => {
    if (user) {
        product.feedbacks.feedbacks = product?.feedbacks?.feedbacks?.map((item) => {
            item.is_liked = Array.isArray(item.user_liked) ? item.user_liked.includes(user._id.toString()) : false;
            return item;
        })

        product.is_bookmarked = false;//user?.favorite_products.includes(product._id.toString()) ? true : false;
    }

    product.description = product.description.replace(/\<img/g, "<img max-width='100%'")

    product.time_prepare_orders = product?.time_prepare_orders?.map((item) => {
        item.value = +item.value;
        return item;
    })

    product.product_detail_infos = product?.product_detail_infos?.map((item) => {
        item.name = item.name[language];
        return item;
    })

    product.sold = product?.sold < 10 ? 0 : product?.sold;

    return product;
}

export const getDetailWhenUserGoToShop = async (req: any, res: any, next: any) => {
    try {
        setCacheHeaders(res, 30);
        const language =
        [VN_LANG, EN_LANG, KO_LANG].indexOf(req.headers['accept-language']) > -1
        ? req.headers['accept-language']
        : VN_LANG;
        // let product = await productStore.getById(req.params.id);

        // if(product) {
        //     product = await detailProductFromBuyer(req.params.id)
        //     product.description = product.description.replace(/\<img/g, "<img max-width='100%'")
        //     product = handleByRequest(product, req.user, language);
        //     productStore.setById(product);
        // } else {
        //     product = handleByRequest(product, req.user, language);
        // }

        let product = await ElasticQuery.getInstance().getById(
            elsIndexName.product,
            req.params.id.toString()
        );

        product = new ProductElsDTO(product).get()
        product = handleByRequest(product, req.user, language);
        res.success(product)

    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const toogleBookmarkProduct = async (req: any, res: any, next: any) => {
    try {
        let user: any = req.user;
        const userObj = cloneObj(user);
        const product_id = req.params.id;
        let message = "";
        let userUpdated = userObj
        let result = {
            is_bookmarked: false
        }
        if (user.favorite_products.includes(product_id)) {
            // Unbookmark
            userObj.favorite_products = userObj.favorite_products.filter(e => e !== product_id);
            userUpdated = await userRepo.update(userObj._id, { favorite_products: userObj.favorite_products });
            InMemoryAuthUserStore.getInstance().updateBookMark(userObj._id, userObj.favorite_products)
            message = req.__("product.unliked");
            result.is_bookmarked = false;
        }
        else {
            // Bookmark
            userObj.favorite_products.push(product_id);
            userUpdated = await userRepo.update(userObj._id, { favorite_products: userObj.favorite_products });
            InMemoryAuthUserStore.getInstance().updateBookMark(userObj._id, userObj.favorite_products);
            message = req.__("product.liked");
            result.is_bookmarked = true;
        }
        res.success(result, message)
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getOtherProductOfShop = async (req: any, res: any, next: any) => {
    try {
        const page = req.query.page || 1;
        const limit = req.query.limit || 10;
        const shopId = req.query.shop_id;
        const productId = req.query.product_id;
        let product = await repository.getOtherProductOfShop(page, limit, shopId, productId);
        product = cloneObj(product)
        let productItems = cloneObj(product.data)
        productItems = productItems.map(product => {
            product = ProductDTO.newInstance(product)
            product.setUser(req.user);
            let simpleProduct = product.toJSON(['_id', 'name', 'before_sale_price', 'sale_price', 'quantity', 'shop_id', 'images']);
            let productOption = product.toJSON(['variants']);
            simpleProduct.price_min_max = getMinMax(productOption.variants);
            simpleProduct.is_bookmarked = !!product.checkBookmarkedProduct()
            return simpleProduct;
        })
        product.data = productItems
        res.success(product);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    };
}

export const getProductByKeyword = async (req: any, res: any, next: any) => {
    try {
        const { page, limit, keyword } = req.query
        let results: any = await repository.getProductByKeyword(keyword, page, limit)
        results = cloneObj(results)
        const productDTO = results.data.map(e => new ProductDTO(e).setUser(req.user));
        results.data = productDTO.map(e => {
            let getProduct: any = e.toJSONWithBookMark();
            getProduct.price_min_max = getMinMax(getProduct.variants);
            delete getProduct.variants;
            // getProduct.sold = getProduct.order_items && getProduct.order_items.length ? getProduct.order_items[0].sold : 0
            delete getProduct.order_items
            return getProduct;
        });
        res.success(results)
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
    }
}

export const getValidBookmarkedProduct = async (req: any, res: any, next: any) => {
    try {
        let { page, limit } = req.query
        if(!page) page = 1
        if(!limit) limit = 20
        const { user } = req
        const bookmarkedProduct = user.favorite_products || []
        
        let result: any = await repository.getValidBookmarkedProduct(bookmarkedProduct, page, limit)
        result = cloneObj(result)
        result.data = result.data.map(product => {
            let item:any = new ProductDTO(product);
            item = item.getProductComplete(req.user)
            // item.sold = product.order_items && product.order_items.length ? product.order_items[0].sold : 0
            item.discount_percent = item.discount_percent ? item.discount_percent.discount : 0
            delete item.order_items
            return item
        });
        res.success(result)
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
    }
}

export const getAllActiveProduct = async (req: any, res: any, next: any) => {
    try {
        let { page, limit } = req.query
        if(!page) page = 1
        if(!limit) limit = 10
        let result: any = await repository.findProductHome({ orderBy: 'newest' }, page, limit)
        result = cloneObj(result)
        const promise = result.data.map((product) => {
            return new Promise(async (resolve, reject) => {
                let item : any= new ProductDTO(product);
                item = item.getProductComplete(req.user);
                const promotion = await promotionRepo.getPromotionByProduct(item._id)
                if (promotion) {
                    item.discount_percent = promotion.discount
                }
                else {
                    item.discount_percent = 0
                }
                // item.sold = product.order_items && product.order_items.length ? product.order_items[0].sold : 0
                resolve(item)
            })
        })
        result.products = Promise.all(promise)
        res.success(result)
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
    }
}
