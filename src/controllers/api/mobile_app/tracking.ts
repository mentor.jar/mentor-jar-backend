import { executeError } from '../../../base/appError';
import TrackingService from '../../../services/tracking';
import { TrackingObjectRepo } from '../../../repositories/TrackingObjectRepo';
import { ObjectId } from '../../../utils/helper';
import { InMemoryTrackingStore } from '../../../SocketStores/TrackingStore';

const trackingObjectRepo = TrackingObjectRepo.getInstance();
const trackingCache = InMemoryTrackingStore.getInstance();

export const userInteractiveProduct = async (req: any, res: any, next: any) => {
    try {
        let { product_id, action_type } = req.body;
        let { user } = req;
        const currentTrackingCache = await trackingCache.get('interactiveProducts') || []
        currentTrackingCache.push({product_id, action_type, user_id: user._id})
        trackingCache.save('interactiveProducts', currentTrackingCache)
        res.success();
    } catch (error) {
        executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
};

export const userReadActionChange = async (req: any, res: any, next: any) => {
    try {
        let { tracking_object_id } = req;

        await trackingObjectRepo.findOneAndUpdate(
            { _id: ObjectId(tracking_object_id) }, 
            { is_read: true }
        )
        res.success();
    } catch (error) {
        executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
};
