import { executeError } from "../../../../base/appError";
import { NotFoundError } from '../../../../base/customError';
import { CategoryRepo } from "../../../../repositories/CategoryRepo";
import BannerService from '../../../serviceHandles/banner';
import { ProductSort, ShopListProductRequest, ShopSearchProductRequest } from '../../../serviceHandles/shop/definition';
import ShopService from '../../../serviceHandles/shop/service';
import UserService from "../../../serviceHandles/user";
import { DEFAULT_POPULAR_SEARCH, FAKE_TOP_SHOP_IDS } from "../../../../base/variable";
import { OrderRepository } from "../../../../repositories/OrderRepo";
import { OrderItemRepository } from "../../../../repositories/OrderItemRepo";
import { ProductDTO } from "../../../../DTO/Product";
import { getMinMax } from "../../../serviceHandles/product";
import { ProductRepo } from "../../../../repositories/ProductRepo";
import { cloneObj, isMappable } from "../../../../utils/helper";
import { PromotionProgramRepo } from "../../../../repositories/PromotionProgramRepo";
import { ObjectId } from "../../../../utils/helper";
import { ShopRepo } from "../../../../repositories/ShopRepo";
import { UserDTO } from "../../../../DTO/UserDTO";
import { addLink } from "../../../../utils/stringUtil";
import { FollowRepo } from "../../../../repositories/FollowRepo";
import keys from "../../../../config/env/keys";
import setCacheHeaders from "../../../../services/headerCache/headerCache";
import { VoucherRepository } from "../../../../repositories/VoucherRepository";
import VoucherService from "../../../serviceHandles/voucher";
import { MAXIMUM_NUMBER_RETURNED } from "../../../../constants/product_in_shop";

const categoryReposity = CategoryRepo.getInstance();
const orderRepo = OrderRepository.getInstance();
const orderItemRepo = OrderItemRepository.getInstance();
const productRepo = ProductRepo.getInstance();
const promotionRepo = PromotionProgramRepo.getInstance();
const shopRepo = ShopRepo.getInstance();
const voucherRepo = VoucherRepository.getInstance();
const shopService = ShopService._;

export const getStaticSection = async (req: any, res: any, next: any) => {
    try {
        let data: any = [];
        // User
        const userObject: any = {
            type: 'user',
            order: 1,
        }
        // Banner
        const bannerObject: any = {
            type: 'banner',
            order: 2,
        }
        const shopId = req.params.id;
        let user = await UserService._.getUserShopExplore(req, shopId);
        let banners = await BannerService._.getBannerShopExplore(shopId);
        bannerObject.data = banners;
        userObject.data = user;
        data.push(userObject, bannerObject);
        res.success(data);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getShopSection = async (req: any, res: any, next: any) => {
    try {
        const shopId = req.params.id;
        let listShopCategory = await categoryReposity.getTreeVariantByShopId(shopId);
        listShopCategory = await categoryReposity.getJSONWithMinMax(listShopCategory, req.user);
        return res.success(listShopCategory);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

/**
 * Get shop products controller
 * @param req 
 * @param res 
 * @param next 
 * @return Paging data
 */
export const getShopProducts = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id;
        if (!id) throw new NotFoundError('Không tìm thấy cửa hàng');

        const {
            orderBy = ProductSort.NEWEST,
            limit = 10,
            page = 1 } = req.query;
        const request: ShopListProductRequest = { orderBy, shop_id: id };

        let data = await ShopService._.getProductsExplore(request, limit, page, req.user);
        data = cloneObj(data)
        const promises = data.data.map( async  (product) => {
            // Handle discount percent for product
            const promotion = await promotionRepo.getPromotionByProduct(product._id)
            if (promotion) {
                product.discount_percent = promotion?.discount;
            } else {
                product.discount_percent = 0;
            }
            // product.sold = product.order_items && product.order_items.length ? product.order_items[0].sold : 0
            delete product.order_items
            return product
        })

        data.data = await Promise.all(promises);
        
        return res.success(data);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getShopProductsV2 = async (req: any, res: any, next: any) => {
    try {
        setCacheHeaders(res, 30)
        const id = req.params.id;
        if (!id) throw new NotFoundError('Không tìm thấy cửa hàng');

        const {
            orderBy = ProductSort.NEWEST,
            limit = 10,
            page = 1 } = req.query;
        const request: ShopListProductRequest = { orderBy, shop_id: id };
        
        const result = await ShopService._.getProductsExploreV2(request, limit, page, req.user);
        return res.success(result);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getOverviewProductsByShop = async (req: any, res: any, next: any) => {
    try {
        setCacheHeaders(res, 30)
        const shopId = req.params.id;
        if (!shopId) throw new NotFoundError('Không tìm thấy cửa hàng');

        const flashSaleProducts = [];//await shopService.getFlashSaleProducts(shopId);

        let result: any = []

        if (isMappable(flashSaleProducts)) {
            result.push({
                title: {
                    vi: "Flash Sale",
                    en: "Flash Sale",
                    ko: "플래시 세일"
                },
                type: "flash_sale",
                data: flashSaleProducts
            })
        } else {
            const hotDealProducts = await shopService.getHotDealProducts(shopId);

            if (isMappable(hotDealProducts)) {
                result.push({
                    title: {
                        vi: "Hot Deal",
                        en: "Hot Deal",
                        ko: "핫딜"
                    },
                    type: "hot_deal",
                    data: hotDealProducts
                })
            }
        }

        const bestSellProducts = await shopService.getBestSellProducts(shopId);

        if (isMappable(bestSellProducts)) {
            result.push({
                title: {
                    vi: "Sản Phẩm Bán Chạy",
                    en: "Best-Selling Products",
                    ko: "Best 상품"
                },
                type: "best_sell",
                data: bestSellProducts
            })
        } else {
            const newestProducts = await shopService.getNewestProducts(shopId);

            if (isMappable(newestProducts)) {
                result.push({
                    title: {
                        vi: "Sản Phẩm Mới Nhất",
                        en: "Latest Products",
                        ko: "신상품"
                    },
                    type: "newest",
                    data: newestProducts
                })
            }
        }

        return res.success(result);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getProductsByType = async (req: any, res: any, next: any) => {
    try {
        setCacheHeaders(res, 30)
        const { limit = MAXIMUM_NUMBER_RETURNED, page = 1, type } = req.query;
        const shopId = req.params.id;
        if (!shopId) throw new NotFoundError('Không tìm thấy cửa hàng');

        const result = await shopService.getProductsByType(shopId, type, limit, page);

        return res.success(result);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getSuggestProductsByShop = async (req: any, res: any, next: any) => {
    try {
        setCacheHeaders(res, 30)
        const { limit = 10, page = 1 } = req.query;
        const shopId = req.params.id;
        if (!shopId) throw new NotFoundError('Không tìm thấy cửa hàng');

        const suggestProducts = await shopService.getSuggestProducts(shopId, limit, page);

        return res.success(suggestProducts);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}
/**
 * Get shop products controller
 * @param req 
 * @param res 
 * @param next 
 * @return Normal array data
 */
export const getShopCategories = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id;
        if (!id) throw new NotFoundError('Không tìm thấy cửa hàng');

        const data = await ShopService._.getCategoryExplore(id);

        return res.success(data);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}


/**
 * Search shop products controller
 * @param req 
 * @param res 
 * @param next 
 * @return Normal array data
 */
export const searchShopProducts = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id;
        if (!id) throw new NotFoundError('Không tìm thấy cửa hàng');

        const {
            limit = 10,
            page = 1,
            keyword,
            category,
            location,
            ship_provider,
            price_min,
            price_max,
            status,
            pay_option,
            rate,
            services,
            orderBy = ProductSort.NEWEST,
        } = req.query;

        const request: ShopSearchProductRequest = {
            keyword,
            category,
            location,
            ship_provider,
            price_min: parseInt(price_min),
            price_max: parseInt(price_max),
            status,
            pay_option,
            rate,
            services,
            shop_id: id,
            orderBy
        };
        const data = await ShopService._.shopSearchExplore(request, limit, page, req.user);

        return res.success(data);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

/**
 * Top search shop keyword
 * @param req 
 * @param res 
 * @param next 
 * @return Normal array data
 * @rule We get top category's name of products
 */
export const popularSearchShop = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id;
        if (!id) throw new NotFoundError('Không tìm thấy cửa hàng');
        const data = await ShopService._.getCategoryExplore(id);
        let keyword = data
            ?.map(category => category.name)
            ?.filter(e => e != undefined && e != null && e != "")
            ?.slice(0, 5);

        if (keyword?.length <= 0)
            keyword = DEFAULT_POPULAR_SEARCH
        let result: any = {};
        result.top_keyword_shop = keyword;
        res.success(result);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getTopProductOfShop = async (req: any, res: any, next: any) => {
    try {
        const orderSuccessIDs = await orderRepo.getOrderSuccess(req.params.id)
        let result = await orderItemRepo.getProductRankingForBuyer(orderSuccessIDs);
        result = cloneObj(result)
        let products = result.data
        let otherProducts = await productRepo.getListNotInTopProdctOfShop(req.params.id, products);
        products = products.map(item => {
            if(item.product) {
                let product = new ProductDTO(item.product)
                let simpleProduct: any = product.toJSON(['_id', 'name', 'images', 'before_sale_price', 'sale_price', 'shop_id']);
                let productOption: any = product.toJSON(['variants']);
                let variantsDTO = product.getProductVariant(productOption.variants);
                simpleProduct.variants = [];
                variantsDTO.map(e => {
                    let varianJson = e.toJSON(['_id', 'before_sale_price', 'sale_price', 'quantity', 'is_master'])
                    simpleProduct.variants.push(varianJson);
                });
                simpleProduct.price_min_max = getMinMax(productOption.variants);
                product.setUser(req.user);
                simpleProduct.is_bookmarked = !!product.checkBookmarkedProduct();
                simpleProduct.sold = item.quantitySold ?? 0;
                return simpleProduct;
            }
        })
        products = products.filter(item => item)
        // add product to make data more beautiful
        // will remove later
        otherProducts = otherProducts.map(item => {
            let product = new ProductDTO(item)
            let simpleProduct: any = product.toJSON(['_id', 'name', 'images', 'before_sale_price', 'sale_price', 'shop_id', 'order_items', 'sold']);
            let productOption: any = product.toJSON(['variants']);

            let variantsDTO = product.getProductVariant(productOption.variants);
            simpleProduct.variants = [];
            variantsDTO.map(e => {
                let varianJson = e.toJSON(['_id', 'before_sale_price', 'sale_price', 'quantity', 'is_master'])
                simpleProduct.variants.push(varianJson);
            });
            simpleProduct.price_min_max = getMinMax(productOption.variants);
            product.setUser(req.user);
            simpleProduct.is_bookmarked = !!product.checkBookmarkedProduct();
            delete simpleProduct.order_items
            return simpleProduct;
        })
        products = products.concat(otherProducts);
        products = products.slice(0, 5);
        res.success(products)
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getTopShop = async (req: any, res: any, next: any) => {
    try {
        const page = req.query.page || 1;
        const limit = req.query.limit || 10; 
        let yesterdayShop:Array<any> = []
        let todayShops:Array<any> = []
        let paginate: Object = {
            limit: limit,
            total_page: 1,
            page: 1,
            total_record: 5,
        };
        if(process.env.NODE_ENV?.toLowerCase() !== "production") {
            [yesterdayShop, todayShops, paginate] = await orderRepo.getShopRankingForBuyerPaginate(page, limit)
        }
        else {
            const fakeTopShopIDs = FAKE_TOP_SHOP_IDS.map(id => ObjectId(id))       
            todayShops = await shopRepo.topShopForProductionSite(fakeTopShopIDs)
            todayShops = cloneObj(todayShops)
            todayShops = todayShops.map(shop => {
                return {
                    shop,
                    _id: shop._id,
                    totalRevenue: 1000000 // fake
                }
            })
            
        }
        let todayShopPromises = todayShops.map(async (item) => {
            const currentIndex = todayShops.indexOf(item)
            const oldIndex = yesterdayShop.indexOf(yesterdayShop.filter(oldItem => oldItem._id.toString() == item._id.toString())[0])
            item = cloneObj(item)
            if (item.shop && item.shop.user) {
                let is_followed = false;
                if (req.user) {
                    is_followed = await FollowRepo.getInstance().checkFollow(req.user._id, item.shop.user._id)
                }
                item.shop.user.is_follow = is_followed;

                item.change = {
                    type: oldIndex === -1 ? "NEW" : currentIndex === oldIndex ? "EQUAL" : currentIndex < oldIndex ? "UP" : "DOWN",
                    value: oldIndex === -1 ? -1 : Math.abs(currentIndex - oldIndex)
                }
                if (!item.shop.feedback.length) {
                    item.shop.feedback = {};
                    item.shop.feedback.averageFeedbackRate = 5.0;
                } else {
                    item.shop.feedback = item.shop.feedback[0];
                    item.shop.feedback.averageFeedbackRate = parseFloat(item.shop.feedback.averageFeedbackRate.toFixed(1));
                    delete item.shop.feedback._id
                }
                item.shop.user.avatar = addLink(`${keys.host_community}/`, item.shop.user.gallery_image.url)
                item.shop.user = new UserDTO(item.shop.user).toSimpleUserInfo();
                return item
            }
        })
        todayShops = await Promise.all(todayShopPromises)

        let sortShop = todayShops
        if(process.env.NODE_ENV?.toLowerCase() === "production") {
            sortShop = []
            sortShop.push(todayShops.find(item => item._id.toString() === "6103b5073a4364001112f397"))
            sortShop.push(todayShops.find(item => item._id.toString() === "611e534be46a3d001252ec42"))
            sortShop.push(todayShops.find(item => item._id.toString() === "613ec1a703987d0012665a9c"))
            sortShop.push(todayShops.find(item => item._id.toString() === "6141fce480d0260012a7896e"))
            sortShop.push(todayShops.find(item => item._id.toString() === "615db4aba1a53e0013dc534e"))
        }

        todayShops = sortShop
        res.success({data:todayShops,  paginate})
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const showVouchersShop = async (req, res, next) => {
    try {
        const shop_id = req.params.id
        const date = new Date();
        let result = await VoucherService._.getShopVouchers(shop_id,date)
        result = cloneObj(result);
        result = result.filter(voucher => voucher.display_mode === 'all')
        
        res.success(result)
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const updateBannerShop = async (req, res, next) => {
    try {
        const shopId = req.params.id
        const { middle_banner } = req.body
        const params = { middle_banner }
        const result = await shopRepo.update(shopId,params)
        res.success(result)
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}