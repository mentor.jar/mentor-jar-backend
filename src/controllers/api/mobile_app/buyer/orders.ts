

import { cloneObj, isMappable, ObjectId } from "../../../../utils/helper";
import responseCode from "../../../../base/responseCode";
import { OrderDTO } from "../../../../DTO/OrderDTO";
import { OrderRepository } from "../../../../repositories/OrderRepo"
import { UserRepo } from "../../../../repositories/UserRepo";
import { UserDTO } from "../../../../DTO/UserDTO";
import { addLink } from "../../../../utils/stringUtil";
import keys from "../../../../config/env/keys";
import { OrderItemRepository } from "../../../../repositories/OrderItemRepo";
import { ProductRepo } from "../../../../repositories/ProductRepo";
import { VariantRepo } from "../../../../repositories/Variant/VariantRepo";
import { VoucherRepository } from "../../../../repositories/VoucherRepository";
import { GroupOrderRepo } from "../../../../repositories/GroupOrderRepo";
import { OrderShippingRepo } from "../../../../repositories/OrderShippingRepo";
import { GroupOrderDTO } from "../../../../DTO/GroupOrderDTO";
import { CartItemRepo } from "../../../../repositories/CartItemRepo";
import { CancelType, RefundMoneyStatus, RefundStatus, ShippingStatus } from "../../../../models/enums/order";
import { executeError } from "../../../../base/appError";
import notificationService, { NotifyRequestFactory, NotifyType } from "../../../../services/notification";
import { NOTIFY_TARGET_TYPE, NOTIFY_TYPE } from "../../../../services/notification/constants";
import { AddressDTO } from "../../../../DTO/AddressDTO";
import { defaultCountValues } from "../../../serviceHandles/order/definition";
import { AddressRepo } from "../../../../repositories/AddressRepo";
import { ShopRepo } from "../../../../repositories/ShopRepo";
import { FeedbackRepo } from "../../../../repositories/FeedbackRepo";
import { generateOrderNumber } from "../../../../utils/utils";
import { updateQuantityCancelOrder } from "../../mobile_app/orders";
import { getTranslation, getUserLanguage } from "../../../../services/i18nCustom";
import OrderService from "../../../serviceHandles/order/order";
import { QUEUE_PRIORITY, REFUND_CONDITIONS } from "../../../../base/variable";
import { TrackingObjectRepo } from "../../../../repositories/TrackingObjectRepo";
import { AdHoc, TargetType } from "../../../../models/enums/TrackingObject";
import QueueService from "../../../../services/queue";
import { createOrderQueue, handleUserAfterCancelOrder } from "../../../../services/api/order";

const orderRepo = OrderRepository.getInstance();
const groupOrderRepo = GroupOrderRepo.getInstance();
const orderItemRepo = OrderItemRepository.getInstance();
const productRepo = ProductRepo.getInstance();
const variantRepo = VariantRepo.getInstance();
const voucherRepo = VoucherRepository.getInstance();
const cartItemRepo = CartItemRepo.getInstance();
const userRepo = UserRepo.getInstance();
const orderShippingRepo = OrderShippingRepo.getInstance();
const addressRepo = AddressRepo.getInstance();
const shopRepo = ShopRepo.getInstance();
const feedbackRepo = FeedbackRepo.getInstance();
const trackingObjectRepo = TrackingObjectRepo.getInstance();

/**
 * Code for create mock data
 * @param req 
 * @param res 
 * @param next 
 */
export const getOrderWithFilter = async (req: any, res: any, next: any) => {
    try {
        const limit = req.query.limit
        const page = req.query.page
        const userID = req.user._id
        const filterType = req.query.filter_type
        const keyword = req.query.keyword
        let results: any = await orderRepo.getOrderBuyerWithFilter(userID, limit, page, filterType, keyword)
        results = cloneObj(results)
        let orders = cloneObj(results.data)
        const promises = orders.map(order => {
            return new Promise(async (resolve, reject) => {
                try {
                    order = cloneObj(order)
                    order = await OrderDTO.newInstance(order).completeDataPaymentAndShippingInfo()
                    if(order.is_group_buy_order) {
                        order.order_items[0].order_group_buy_price = order.total_value_items
                        order.order_items[0].order_group_buy_member = order?.order_items?.[0]?.product?.group_buy?.groups?.find(item => item.group_buy_price === order.total_value_items)?.number_of_member
                    }
                    else {
                        order.order_items[0].order_group_buy_price = null
                        order.order_items[0].order_group_buy_member = null
                    }
                    resolve(order)
                } catch (error) {
                    reject(error)
                }
            })
        })

        Promise.all(promises).then((data) => {
            results.data = data
            res.success(results)
        })
            .catch((error) => { throw new Error(error.message) })
    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}

/**
 * Lưu ý khi tạo voucher
 * - [DONE] Copy address từ address_id
 * - [DONE] Tạo các order theo thông tin đã có
 * - [DONE] Clone cart_item và tạo order_item (truy vấn thêm thông tin variant hoặc product)
 * - [ABORN] Lưu ordershippings vào order con (Phí vận chuyển cũng sẽ được lưu ở đây)
 * - [DONE] Lưu used_by vào các voucher sử dụng và giảm số lượng voucher
 * - [DONE] Trừ số lượng còn lại của sản phẩm và variant
 * - [DONE] Lưu voucher (Kèm số tiền đã giảm tương ứng để check điều kiện budget áp dụng voucher sau này)
 * - [CHECKING] Validate voucher in middleware
 * - [BLOCK] Lưu voucher system dạng discount vào đâu cho hợp lý?. Hiện tại lưu ở order, nhưng nếu nhiều order 1 lúc sẽ lưu sai
 * @param req 
 * @param res 
 * @param next 
 */


export const createOrderBuyer = async (req: any, res: any, next: any) => {

    const orderInfo = req.order_info
    const { shops, payment_method_id, system_voucher, total_order_payment, total_order_items, 
        order_room_id, ref_link, ref_from_id, avatar_url, group_id} = req.body
    const { voucherLists, dataItems } = req
    const userID = req.user._id.toString()
    let request:any = {
        orderInfo, shops, payment_method_id, system_voucher, total_order_payment, total_order_items, 
        voucherLists, userID, dataItems, order_room_id, ref_link, ref_from_id, avatar_url, group_id
    }
    // create group order for multy order
    let groupOrder: any = await groupOrderRepo.create({
        user_id: ObjectId(req.user._id),
        total_price: total_order_payment,
        status: "handling"
    })
    groupOrder = cloneObj(groupOrder)
    request.groupOrderID = groupOrder._id.toString()

    // create order queue
    new QueueService().register(
        "handleCreateOrder-" + process.env.NODE_ENV, 
        request, 
        createOrderQueue,
        QUEUE_PRIORITY.HIGH
    )


    res.success({
        group_order_id: groupOrder._id,
        user_id: req.user._id,
        orders: [],
        total_price: total_order_payment,
        created_at: groupOrder.created_at,
        updated_at: groupOrder.updated_at
    })
}

/**
 * Create Order return/ refund
 * @param req 
 * @param res 
 * @param next 
 */
export const createRefundForBuyer = async (req: any, res: any, next: any) => {
    const { item_id, refund_quantity, address_id, reasons, images, video, note, manner, email, phone_number } = req.body
    let { order, orderItem } = req;
    const { total_value_items, vouchers } = order;

    try {

        let address = await addressRepo.findOne({ _id: ObjectId(address_id) })

        const refund_amount = OrderService._.calculatingRefundMoneyByItem(orderItem, total_value_items, vouchers);
        const reasonViaObjects = reasons.map(code => {
            const reasonObject = REFUND_CONDITIONS.filter(item => +item.code === +code)[0]
            if (reasonObject) {
                return reasonObject
            }
        })

        const refund_item = {
            item_id: item_id,
            refund_quantity: refund_quantity,
            status: RefundStatus.OPENING,
            opening_time: new Date(),
            request_info: {
                address: address,
                reasons: reasonViaObjects,
                images: images,
                video: video,
                note: note,
                manner: manner,
                email: email,
                phone_number: phone_number,
                time: new Date()
            },
            refund_money: {
                status: RefundMoneyStatus.PENDING,
                value: refund_amount
            }
        }

        order.refund_information.push(refund_item)
        order.shipping_status = ShippingStatus.RETURN;

        const updatedOrder = await orderRepo.update(order._id, order);

        // send notification
        const userShop = await userRepo.getUserByShopId(order.shop_id);
        const language = await getUserLanguage(userShop._id);
        const orderItemEl: any = await orderItemRepo.findById(item_id.toString());
        const product = await productRepo.findById(orderItemEl.product_id.toString());

        await notificationService.saveAndSend(
            NotifyRequestFactory.generate(
                NOTIFY_TYPE.REFUND_REQUEST_SENT,
                NOTIFY_TARGET_TYPE.SELLER,
                {
                    title: getTranslation(language, 'notification.bidu_notification'),
                    content: getTranslation(language, 'notification.refund_sent', order.order_number, product.name),
                    receiverId: userShop._id.toString(),
                    orderId: order._id.toString(),
                    userId: order.user_id.toString(),
                    orderNumber: order.order_number,
                    itemId: item_id,
                    itemName: product.name
                }
            ))

        // Insert Table TrackingObject Seller
        // await trackingObjectRepo.createOrUpdateTrackingObject(userShop._id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);
        // Insert Table TrackingObject Buyer
        // await trackingObjectRepo.createOrUpdateTrackingObject(order.user_id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);
        res.success(updatedOrder);
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
    }
}

/**
 * Create Order return/ refund
 * @param req 
 * @param res 
 * @param next 
 */
export const cancelRefundForBuyer = async (req: any, res: any, next: any) => {
    let { order, product_request_refund } = req
    const { item_id } = req.body;
    try {
        product_request_refund.status = RefundStatus.CANCELED;
        product_request_refund.canceled_time = new Date();

        const updatedOrder = await orderRepo.update(order._id, order);

        // send notification
        const userShop = await userRepo.getUserByShopId(order.shop_id);
        const language = await getUserLanguage(userShop._id);
        const orderItemEl: any = await orderItemRepo.findById(item_id.toString());
        const product = await productRepo.findById(orderItemEl.product_id.toString());
        await notificationService.saveAndSend(
            NotifyRequestFactory.generate(
                NOTIFY_TYPE.REFUND_REQUEST_CANCELED,
                NOTIFY_TARGET_TYPE.SELLER,
                {
                    title: getTranslation(language, 'notification.bidu_notification'),
                    content: getTranslation(language, 'notification.refund_request_cancelded', product.name, order.order_number),
                    receiverId: userShop._id.toString(),
                    orderId: order._id.toString(),
                    userId: order.user_id.toString(),
                    orderNumber: order.order_number,
                    itemId: item_id,
                    itemName: product.name
                }
            ))
        // Insert Table TrackingObject Seller
        // await trackingObjectRepo.createOrUpdateTrackingObject(userShop._id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);
        // Insert Table TrackingObject Buyer
        // await trackingObjectRepo.createOrUpdateTrackingObject(order.user_id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);
        res.success(updatedOrder);
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
    }
}

/**
 * You can only send a cancellation request if the order has not yet been scheduled for pick up by the courier. 
 * Please note to think twice before canceling, you can only cancel once for every order. Once the order has been scheduled for pick up by the courier, it will not be possible to cancel the order
 * @param req 
 * @param res 
 * @param next 
 */
export const cancelOrder = async (req: any, res: any, next: any) => {
    let order = req.order
    const { _id: orderId, vouchers, group_order_id: groupOrderId } = order;
    const reason = req.body.cancel_reason;
    try {
        // reject if order is group buy order
        if(order.is_group_buy_order) {
            res.error(responseCode.BAD_REQUEST.name, req.__("controller.order.can_not_cancel_group_buy_order"), responseCode.BAD_REQUEST.code)
            return;
        }

        // handle orderShipping
        const orderShipping: any = await orderShippingRepo.findOne({ order_id: order._id });
        if (orderShipping && orderShipping.shipping_info && orderShipping.shipping_info.label) {
            throw new Error(req.__("controller.order.can_not_cancel_order_delivered"));
        }
        const seller = await userRepo.getUserByShopId(order.shop_id);
        const language = await getUserLanguage(seller._id);
        
        let listVoucherSystem = [];
        if (isMappable(vouchers)) {
            listVoucherSystem = cloneObj(vouchers.filter((item) => item.classify && item.classify !== null))
        }

        let cancellableOrderList = [];
        if (isMappable(listVoucherSystem)) {
            let groupOrder = await groupOrderRepo.findById(groupOrderId.toString());
            groupOrder = cloneObj(groupOrder);

            const listOtherOrderBelongToGroupOrder = groupOrder?.orders?.filter(
                (item) => item._id.toString() !== orderId.toString()
            );
            
            const listOrderCanceledTogether = await Promise.all(
                listOtherOrderBelongToGroupOrder?.map(async (item: any) => 
                    orderRepo.findOne({ _id: item._id })
                )
            )

            cancellableOrderList = await Promise.all(
                listOrderCanceledTogether.map(async (item: any) => {
                    const orderShipping: any = await orderShippingRepo.findOne({ order_id: item._id });

                    if (orderShipping && orderShipping.shipping_info && orderShipping.shipping_info.label) {
                        return 0;
                    } else {
                        return item;
                    }
                })
            )

            cancellableOrderList = cancellableOrderList.filter((item) => item !== 0);     
        } 
        
        if (order.shipping_status === ShippingStatus.PENDING) {
            order = await handelCanceledOrder(order, reason, req.user, seller, language);
        } else {
            order = await handelCancelingOrder(order, reason, seller, language);
        }

        if (isMappable(listVoucherSystem) && isMappable(cancellableOrderList)) {
            Promise.all(
                cancellableOrderList.map(async(item) => {
                    const otherSeller = await userRepo.getUserByShopId(item.shop_id);
                    if (item.shipping_status === ShippingStatus.PENDING) {
                        handelCanceledOrder(item, reason, req.user, otherSeller, language);
                    } else {
                        handelCancelingOrder(item, reason, otherSeller, language);
                    }
                })
            )
        }

        // Insert Table TrackingObject Seller
        // await trackingObjectRepo.createOrUpdateTrackingObject(user._id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);
        // Insert Table TrackingObject Buyer
        // await trackingObjectRepo.createOrUpdateTrackingObject(order.user_id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);
        res.success(order)
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
    }
}

const handelCanceledOrder = async (order, reason, userRequest, seller, language) => {
    const result = await orderRepo.update(order._id, {
        shipping_status: ShippingStatus.CANCELED,
        cancel_reason: reason,
        cancel_type: CancelType.BUYER,
        cancel_by: userRequest._id,
        cancel_time: new Date()
    })
    await updateQuantityCancelOrder(order._id);

    handleUserAfterCancelOrder(order.user_id);

    // send notification
    await notificationService.saveAndSend(
        NotifyRequestFactory.generate(
            NOTIFY_TYPE.ORDERS_CANCELED,
            NOTIFY_TARGET_TYPE.SELLER,
            {
                title: getTranslation(
                    language,
                    'notification.bidu_notification'
                ),
                content: getTranslation(
                    language,
                    'notification.seller_order.order_canceled',
                    order.order_number
                ),
                receiverId: seller._id.toString(),
                orderId: order._id.toString(),
                userId: order.user_id.toString(),
                orderNumber: order.order_number,
            }
        )
    );

    return result;
}

const handelCancelingOrder = async (order, reason, seller, language) => {
    const result = await orderRepo.update(order._id, {
         shipping_status: ShippingStatus.CANCELING,
         cancel_reason: reason,
     });

     // send notification
     await notificationService.saveAndSend(
         NotifyRequestFactory.generate(
             NOTIFY_TYPE.ORDERS_CANCELING,
             NOTIFY_TARGET_TYPE.SELLER,
             {
                 title: getTranslation(
                     language,
                     'notification.bidu_notification'
                 ),
                 content: getTranslation(
                     language,
                     'notification.seller_order.order_canceling',
                     order.order_number
                 ),
                 receiverId: seller._id.toString(),
                 orderId: order._id.toString(),
                 userId: order.user_id.toString(),
                 orderNumber: order.order_number,
             }
         )
     );

     return result;
}



/**
 * Get Detail Order of User
 * @param req 
 * @param res 
 * @param next 
 */
export const getDetailOrder = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id;
        let order:any = await orderRepo.getDetailOrderBuyer(id);
        order = cloneObj(order)
        order = cloneObj(order)
        if (isMappable(order?.system_vouchers)) {
            const otherOrders = order?.group_orders.orders.filter((item) => item._id !== id);
    
            order.group_orders = await Promise.all(
                otherOrders?.map(async (item) => {
                    let orderHandled = await orderRepo.getDetailOrder(item._id);
                    orderHandled =  new OrderDTO(orderHandled).orderBelongGroupOrder();
                    return orderHandled
                })
            )
        } else {
            order.group_orders = []
        }

        order.is_completed_feedback = false
        const itemFeedbacked = order.order_items.filter(item => item.is_feedback === true)
        if (itemFeedbacked.length === order.order_items.length) {
            order.is_completed_feedback = true
        }

        if(order.is_group_buy_order) {
            order.order_items[0].order_group_buy_price = order.total_value_items
            order.order_items[0].order_group_buy_member = order?.order_items?.[0]?.product?.group_buy?.groups?.find(item => item.group_buy_price === order.total_value_items)?.number_of_member
        }
        else {
            order.order_items[0].order_group_buy_price = null
            order.order_items[0].order_group_buy_member = null
        }
        res.success(order);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const searchOrder = async (req: any, res: any, next: any) => {
    try {
        const limit = req.query.limit
        const page = req.query.page
        const userID = req.user._id
        const keyword = req.query.keyword
        let results: any = await orderRepo.searchOrderForBuyer(userID, limit, page, keyword)
        results = cloneObj(results)
        let orders = cloneObj(results.data)
        const promises = orders.map(order => {
            return new Promise(async (resolve, reject) => {
                order = cloneObj(order)
                order = await OrderDTO.newInstance(order).completeDataPaymentAndShippingInfo()
                resolve(order)
            })
        })

        Promise.all(promises)
            .then((data) => {
                results.data = data
                res.success(results)
            })
            .catch((error) => { throw new Error(error.message) })
    } catch (error) {
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}

export const getOrderProcess = async (req: any, res: any, next: any) => {
    try {
        const userID = req.user._id
        const orderOnProcess = await groupOrderRepo.find({status: 'handling', user_id: ObjectId(userID)})
        const result = orderOnProcess.length
        res.success(result)
    } catch (error) {
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}

export const confirmShipped = async (req: any, res: any, next: any) => {
    try {
        const { id } = req.params;
        const instance = {
            shipping_status: ShippingStatus.SHIPPING
        }
        const order = await orderRepo.update(id, instance);
        await orderShippingRepo.findOneAndUpdate({ order_id: id }, { user_confirm_shipped_time: Date.now() })

        // send notification
        const user = await userRepo.getUserByShopId(order.shop_id);
        // hide notify
        // const language = await getUserLanguage(user._id);
        // await notificationService.saveAndSend(
        //     NotifyRequestFactory.generate(
        //         NOTIFY_TYPE.ORDERS_SHIPPED,
        //         NOTIFY_TARGET_TYPE.SELLER,
        //         {
        //             title: getTranslation(language, 'notification.bidu_notification'),
        //             content: getTranslation(language, 'notification.order_shipped', order.order_number),
        //             receiverId: user._id.toString(),
        //             orderId: order._id.toString(),
        //             userId: order.user_id.toString(),
        //             orderNumber: order.order_number,
        //         }
        //     ))
        // Insert Table TrackingObject Seller
        // await trackingObjectRepo.createOrUpdateTrackingObject(user._id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);
        // Insert Table TrackingObject Buyer
        // await trackingObjectRepo.createOrUpdateTrackingObject(order.user_id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);
        res.success(order);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const countOrder = async (req: any, res: any, next: any) => {
    try {
        const userId = req.user._id;
        let data = await orderRepo.countOrder(userId);
        if (!data.length) {
            data = defaultCountValues;
        }

        // handle cancel = canceling + canceled
        let countCancel = 0;
        data.forEach(el => {
            if (el._id === 'canceled' || el._id === 'canceling') {
                countCancel += el.count
            }
        });
        data.push({
            _id: 'cancel',
            count: countCancel
        })

        res.success(data);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getFeedbackOfOrder = async (req: any, res: any, next: any) => {
    try {
        const { id } = req.params
        const feedbacks = await feedbackRepo.getFeedbackByOrderID(id)
        res.success(feedbacks)
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

