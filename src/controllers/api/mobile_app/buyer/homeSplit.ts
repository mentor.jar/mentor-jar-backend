import _ from 'lodash';

import BannerRepo from '../../../../repositories/BannerRepo';
import responseCode from '../../../../base/responseCode';

import { ShopRepo } from '../../../../repositories/ShopRepo';
import { BannerDTO } from '../../../../DTO/BannerDTO';
import { ProductDTO } from '../../../../DTO/Product';
import { ProductRepo } from '../../../../repositories/ProductRepo';
import { CategoryDTO } from '../../../../DTO/CategoryDTO';
import { CategoryRepo } from '../../../../repositories/CategoryRepo';
import { VoucherRepository } from '../../../../repositories/VoucherRepository';
import { SearchKeywordRepo } from '../../../../repositories/SearchKeywordRepo';
import { cloneObj, ObjectId, timeOutOfPromise } from '../../../../utils/helper';
import { PromotionProgramRepo } from '../../../../repositories/PromotionProgramRepo';
import { CATEGORIES_LOCALIZED, VN_LANG } from '../../../../base/variable';
import {
    handleTopShop,
    saveAccessHomePage,
    topProductOfSystem,
} from '../../../../services/api/home';
import { InMemoryProductStore } from '../../../../SocketStores/ProductStore';
import { TTL } from '../../../../SocketStores/constants';
import CacheService from '../../../../services/cache';
import setCacheHeaders from '../../../../services/headerCache/headerCache';
import { InMemorySellerStore } from '../../../../SocketStores/SellerStore';
import ProductService from '../../../serviceHandles/product/service';

const shopRepo = ShopRepo.getInstance();
const bannerRepo = BannerRepo.getInstance();
const voucherRepo = VoucherRepository.getInstance();
const productRepo = ProductRepo.getInstance();
const categoryRepo = CategoryRepo.getInstance();
const promotionRepo = PromotionProgramRepo.getInstance();
const searchKeywordRepo = SearchKeywordRepo.getInstance();
const productCache = InMemoryProductStore.getInstance();
const cacheService = CacheService._
const shopCache = InMemorySellerStore.getInstance();
const productService = ProductService._;

export const sectionBannerCategories = async (
    req: any,
    res: any,
    next: any
) => {
    try {
        setCacheHeaders(res, 300);
        const language = req.headers['accept-language'] || VN_LANG;

        let [system_banner, system_category] = await Promise.all([
            (async () => {
                try {
                    let systemBanners: any = await bannerRepo.findHomeBanner(
                        {
                            shop_id: null,
                            start_time: { $lte: new Date() },
                            end_time: { $gte: new Date() },
                            $or: [
                                { position: 'top' },
                                { classify: { $exists: false } },
                            ],
                        },
                        { priority: 1 },
                        10
                    );
                    const promisesCheckVoucher = systemBanners.map(
                        async (banner) => {
                            banner = cloneObj(banner);
                            if (
                                Array.isArray(banner?.vouchers) &&
                                banner?.vouchers.length > 0
                            ) {
                                const vouchers =
                                    await voucherRepo.findValidWithIds(
                                        banner?.vouchers
                                    );
                                banner.vouchers = vouchers.map(
                                    (voucher) => voucher._id
                                );
                            }
                            if (banner.classify === 'shop_view') {
                                const shopBanner: any = await shopRepo.findOne({
                                    _id: ObjectId(banner.advance_actions?.shop),
                                });
                                banner.advance_actions.user =
                                    shopBanner?.user_id;
                            }
                            return BannerDTO.newInstance(banner).toSimpleJSON();
                        }
                    );

                    return Promise.all(promisesCheckVoucher).then((data) => {
                        return data;
                    });
                } catch (error) {
                    console.log(error);
                    return [];
                }
            })(),
            (async () => {
                try {
                    let systemCategories: any =
                        await categoryRepo.getCategorySystemOrderBy();

                    return systemCategories
                        .filter((category) => category.name !== 'Ưu đãi')
                        .map((category) => {
                            const categoryLocalized = CATEGORIES_LOCALIZED.get(
                                category.name.trim().toLowerCase()
                            );

                            if (categoryLocalized) {
                                category.name_localized = {
                                    vi: category.name,
                                    en: categoryLocalized.en,
                                    ko: categoryLocalized.ko,
                                };
                                if (categoryLocalized[language]) {
                                    category.name = categoryLocalized[language];
                                }
                            } else {
                                category.name_localized = {
                                    vi: category.name,
                                    en: category.name,
                                    ko: category.name,
                                };
                            }

                            return new CategoryDTO(category).toSimpleCategoryItem();
                        });
                } catch (error) {
                    console.log(error);
                    return [];
                }
            })(),
        ]);

        const results = {
            system_banner: system_banner || [],
            system_category: system_category || [],
        };
        saveAccessHomePage();
        res.success(results);
    } catch (error) {
        console.log(error);
        res.error(
            responseCode.SERVER.name,
            req.__('controller.error_occurs'),
            responseCode.SERVER.code
        );
    }
};

export const sectionNewestProduct = async (req: any, res: any, next: any) => {
    try {
        setCacheHeaders(res, 30);

        let result: any;
        if(!cacheService.has('local_cache_home_newest_product')) { 
            result = await productService.getNewestProduct(1, 30);
            cacheService.setTTL('local_cache_home_newest_product', result.data, TTL.minute)
            result.data = _.shuffle(result?.data);
            res.success(result);
        } else {
            result = await cacheService.get('local_cache_home_newest_product');
            result= _.shuffle(result);
            res.success(result);
        }
    } catch (error) {
        console.log(error);
        res.error(
            responseCode.SERVER.name,
            req.__('controller.error_occurs'),
            responseCode.SERVER.code
        );
    }
};

export const sectionTopKeyword = async (req: any, res: any, next: any) => {
    try {
        const promises = [
            timeOutOfPromise(2000, []),
            (async () => productCache.get('home_top_keyword'))(),
        ];

        const topKeyword = await Promise.race(promises).then((data: any) => {
            return data;
        });

        if (!topKeyword || !topKeyword.length) {
            let keywords = await searchKeywordRepo.top(30);
            keywords = cloneObj(keywords);
            let promises = [];
            promises = keywords.map(async (item) => {
                return new Promise(async (resolve, reject) => {
                    item.avatar = '';
                    const products = await productRepo.countProductByKeyword(
                        item.keyword
                    );
                    item.total_product = products.length;
                    const image = products.find((item) => item.images.length)
                        ?.images[0];
                    item.avatar = image || '';
                    resolve(item);
                });
            });

            keywords = await Promise.all(promises);
            keywords = keywords.filter((item) => item?.total_product > 0);

            productCache.save('home_top_keyword', keywords);
            productCache.expire('home_top_keyword', TTL.fiveMinute);
            res.success(_.shuffle(keywords).slice(0, 10) || []);
        } else {
            res.success(_.shuffle(topKeyword).slice(0, 10) || []);
        }
    } catch (error) {
        console.log(error);
        res.error(
            responseCode.SERVER.name,
            req.__('controller.error_occurs'),
            responseCode.SERVER.code
        );
    }
};

export const sectionTopSeller = async (req: any, res: any, next: any) => {
    try {
        const promises = [
            timeOutOfPromise(2000, []),
            (async () => productCache.get('home_top_seller'))(),
        ];

        const topSeller = await Promise.race(promises).then((data: any) => {
            return data;
        });

        if (!topSeller || !topSeller.length) {
            let todayShops = await shopRepo.getShopRanking(1, 5);
            todayShops = await handleTopShop(todayShops, req.user, false);

            productCache.save('home_top_seller', todayShops);
            productCache.expire('home_top_seller', TTL.fiveMinute);

            res.success(todayShops || []);
        } else {
            res.success(topSeller || []);
        }
    } catch (error) {
        console.log(error);
        res.error(
            responseCode.SERVER.name,
            req.__('controller.error_occurs'),
            responseCode.SERVER.code
        );
    }
};

export const sectionTopSellerRevise = async (req: any, res: any, next: any) => {
    try {
        const promises = [
            timeOutOfPromise(2000, []),
            (async () => productCache.get('home_top_seller_revise'))(),
        ];

        const topSeller = await Promise.race(promises).then((data: any) => {
            return data;
        });

        if (!topSeller || !topSeller.length) {
            let todayShops = await shopRepo.getShopRanking(1, 5);
            todayShops = await handleTopShop(todayShops, req.user, true);
            todayShops = todayShops.map((item) => item.shop)

            productCache.save('home_top_seller_revise', todayShops);
            productCache.expire('home_top_seller_revise', TTL.fiveMinute);

            res.success(todayShops || []);
        } else {
            res.success(topSeller || []);
        }
    } catch (error) {
        console.log(error);
        res.error(
            responseCode.SERVER.name,
            req.__('controller.error_occurs'),
            responseCode.SERVER.code
        );
    }
};

export const sectionTopProduct = async (req: any, res: any, next: any) => {
    try {
        setCacheHeaders(res, 30);
        // const promises = [
        //     timeOutOfPromise(2000, []),
        //     (async () => productCache.get('home_top_product'))(),
        // ];

        // const topProduct = await Promise.race(promises).then((data: any) => {
        //     return data;
        // });

        // if (!topProduct || !topProduct.length) {
        //     let products: any = [];
        //     const topProduct: any = await topProductOfSystem(req.user, 1, 30);
        //     products = topProduct?.data || [];

        //     productCache.save('home_top_product', products);
        //     productCache.expire('home_top_product', TTL.fiveMinute);

        //     res.success(_.shuffle(products).slice(0, 18) || []);
        // } else {
        //     res.success(_.shuffle(topProduct).slice(0, 18) || []);
        // }
        let topProducts = await productService.getTopProductSystem(1, 18); 
        let result = _.shuffle(topProducts?.data?.slice(0, 10)).concat(topProducts?.data?.slice(10))
        res.success(result);

    } catch (error) {
        console.log(error);
        res.error(
            responseCode.SERVER.name,
            req.__('controller.error_occurs'),
            responseCode.SERVER.code
        );
    }
};

export const sectionTopLiveStreams = async (req: any, res: any, next: any) => {
    try {
        res.success([]);
    } catch (error) {
        console.log(error);
        res.error(
            responseCode.SERVER.name,
            req.__('controller.error_occurs'),
            responseCode.SERVER.code
        );
    }
};

export const sectionBannerCategoriesV2 = async (
    req: any,
    res: any,
    next: any
) => {
    try {
        const language = req.headers['accept-language'] || VN_LANG;

        let [system_banner, system_category] = await Promise.all([
            (async () => {
                try {
                    let systemBanners: any = await bannerRepo.findHomeBanner(
                        {
                            shop_id: null,
                            start_time: { $lte: new Date() },
                            end_time: { $gte: new Date() },
                            $or: [
                                { position: 'top' },
                                { classify: { $exists: false } },
                            ],
                        },
                        { priority: 1 },
                        10
                    );
                    const promisesCheckVoucher = systemBanners.map(
                        async (banner) => {
                            banner = cloneObj(banner);
                            if (
                                Array.isArray(banner?.vouchers) &&
                                banner?.vouchers.length > 0
                            ) {
                                const vouchers =
                                    await voucherRepo.findValidWithIds(
                                        banner?.vouchers
                                    );
                                banner.vouchers = vouchers.map(
                                    (voucher) => voucher._id
                                );
                            }
                            if (banner.classify === 'shop_view') {
                                const shopBanner: any = await shopRepo.findOne({
                                    _id: ObjectId(banner.advance_actions?.shop),
                                });
                                banner.advance_actions.user =
                                    shopBanner?.user_id;
                            }
                            return BannerDTO.newInstance(banner).toSimpleJSON();
                        }
                    );

                    return Promise.all(promisesCheckVoucher).then((data) => {
                        return data;
                    });
                } catch (error) {
                    console.log(error);
                    return [];
                }
            })(),
            (async () => {
                try {
                    let systemCategories: any =
                        await categoryRepo.getCategorySystemOrderBy();

                        return systemCategories.map((category) => {
                        const categoryLocalized = CATEGORIES_LOCALIZED.get(
                            category.name.trim().toLowerCase()
                        );

                        if (categoryLocalized) {
                            category.name_localized = {
                                vi: category.name,
                                en: categoryLocalized.en,
                                ko: categoryLocalized.ko,
                            };
                            if (categoryLocalized[language]) {
                                category.name = categoryLocalized[language];
                            }
                        } else {
                            category.name_localized = {
                                vi: category.name,
                                en: category.name,
                                ko: category.name,
                            };
                        }

                        return new CategoryDTO(category).toSimpleCategoryItem();
                    });
                } catch (error) {
                    console.log(error);
                    return [];
                }
            })(),
        ]);

        const results = {
            system_banner: system_banner || [],
            system_category: system_category || [],
        };
        saveAccessHomePage();
        res.success(results);
    } catch (error) {
        console.log(error);
        res.error(
            responseCode.SERVER.name,
            req.__('controller.error_occurs'),
            responseCode.SERVER.code
        );
    }
};

export const sectionProductsGroupBuy = async (req: any, res: any) => {
    try {
        const page = 1, limit = 30, type = '', category = '';
        const result = await productService.getListProductGroupBuy(page, limit, type, category);
        res.success(result);
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
    }
;}