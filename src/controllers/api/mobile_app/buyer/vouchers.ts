import moment from "moment";
import { executeError } from "../../../../base/appError";
import responseCode from "../../../../base/responseCode";
import keys from "../../../../config/env/keys";
import { ShopDTO } from "../../../../DTO/Shop";
import { PaymentMethodRepo } from "../../../../repositories/PaymentMethodRepo";
import { ShippingMethodRepo } from "../../../../repositories/ShippingMethodRepo";
import { ShopRepo } from "../../../../repositories/ShopRepo";
import { UserRepo } from "../../../../repositories/UserRepo";
import { VoucherRepository } from "../../../../repositories/VoucherRepository";
import { InMemoryVoucherStore } from "../../../../SocketStores/VoucherStore";
import { cloneObj, ObjectId } from "../../../../utils/helper";
import { addLink } from "../../../../utils/stringUtil";
import UserService from "../../../serviceHandles/user";

const voucherRepository = VoucherRepository.getInstance();
const userRepo = UserRepo.getInstance();
const shippingMethodRepo = ShippingMethodRepo.getInstance();
const paymentMethodRepo = PaymentMethodRepo.getInstance();
const voucherAndOrderCache = InMemoryVoucherStore.getInstance();
const shopRepo = ShopRepo.getInstance();


export const getListVoucherSystem = async (req: any, res: any, next: any) => {
    try {
        const page = req.query.page;
        const limit = req.query.limit;
        const filter = req.query.filter;
        const result = await voucherRepository.getVoucherBySystem(limit, page, filter);
        res.success(result);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const saveVoucherToUserBudget = async (req: any, res: any, next: any) => {
    try {
        let { user } = req
        const voucher_id = ObjectId(req.params.id)
        user = cloneObj(user)
        const voucher:any = await voucherRepository.findOne({_id: voucher_id})
        const usersSavedVoucher = await userRepo.find({
            saved_vouchers: voucher_id
        })

        if(voucher?.save_quantity <= usersSavedVoucher.length) {
            res.error(responseCode.BAD_REQUEST.name, req.__("controller.voucher.limit_voucher_save_quantity"), responseCode.BAD_REQUEST.code);
            return;
        }

        if(user?.saved_vouchers.filter(id => id.toString() === cloneObj(voucher_id)).length > 0) {
            res.error(responseCode.BAD_REQUEST.name, req.__("controller.voucher.voucher_already_saved"), responseCode.BAD_REQUEST.code);
            return;
        }
        user.saved_vouchers.push(voucher_id)
        await userRepo.update(user._id, user)
        UserService._.clearUserInfoInCache(user._id.toString())
        res.success(responseCode.SUCCESS.name, req.__("controller.voucher.save_voucher_success"), responseCode.SUCCESS.code)
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const detailVoucherBuyer = async (req: any, res: any, next: any) => {
    try {
        let voucher = await voucherRepository.getDetailVoucher(req.params.id);
        const { user } = req
        if (voucher) {
            voucher = cloneObj(voucher)
            const { conditions } = voucher
            if (conditions) {
                conditions.shipping_methods = await shippingMethodRepo.getShippingMethodByIds(conditions.shipping_method)
                conditions.payment_methods = await paymentMethodRepo.getPaymentMethodByIds(conditions.payment_method)
                if(conditions?.limit_per_user && user) {
                    const used = await voucherAndOrderCache.getByKeyAndField(`voucher_${voucher._id.toString()}`, `${user._id.toString()}`, true) || 0
                    voucher.remaining_quantity = Math.max(voucher?.conditions?.limit_per_user - used, 0)
                }
                else {
                    voucher.remaining_quantity = null
                }
            }
            else {
                voucher.remaining_quantity = null
            }
            res.success(voucher, responseCode.SUCCESS.name, responseCode.SUCCESS.code);
        }
        else {
            res.error(responseCode.NOT_FOUND.name, req.__("controller.voucher.voucher_not_found"), responseCode.NOT_FOUND.code);
        }
    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}

export const getVoucherBudgetByUser = async (req: any, res:any, next:any) => {
    try {
        const { user } = req
        const savedVoucher = user.saved_vouchers || []
        const vouchers = await voucherRepository.findValidWithIds(savedVoucher)
        const vouchersAssignFromAdmin = await voucherRepository.findVoucherSystemOfUser(user._id)
        let budgetVoucher = vouchers.concat(vouchersAssignFromAdmin)
        budgetVoucher = budgetVoucher.filter((item, index) => {
            return budgetVoucher.indexOf(item) == index;
        })
        budgetVoucher = budgetVoucher.filter(voucher => {
            if(voucher.conditions && voucher.conditions["limit_per_user"] && voucher.used_by) {
                return !voucher.used_by[user._id] || voucher.used_by[user._id] < +voucher.conditions["limit_per_user"]
            }
            return true
        })
        res.success(budgetVoucher)
    } catch (error) {
        console.log(error.message);
        
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getVoucherBudgetByUserV2 = async (req: any, res:any, next:any) => {
    try {
        const { user } = req
        const savedVoucher = user.saved_vouchers || []
        const vouchers = await voucherRepository.findValidWithIds(savedVoucher)
        const vouchersAssignFromAdmin = await voucherRepository.findVoucherSystemOfUser(user._id)
        let budgetVoucher = vouchers.concat(vouchersAssignFromAdmin)
        budgetVoucher = budgetVoucher.filter((item, index) => {
            return budgetVoucher.indexOf(item) == index;
        })
        budgetVoucher = budgetVoucher.filter(voucher => {
            if(voucher.conditions && voucher.conditions["limit_per_user"] && voucher.used_by) {
                return !voucher.used_by[user._id] || voucher.used_by[user._id] < +voucher.conditions["limit_per_user"]
            }
            return true
        })

        // group by classify before return
        const discountVouchers = budgetVoucher.filter(voucher => voucher.classify === 'discount' || voucher.shop_id) || [] // including shop's vouchers 
        const shippingVouchers = budgetVoucher.filter(voucher => voucher.classify === 'free_shipping') || []
        const cashBackVouchers = budgetVoucher.filter(voucher => voucher.classify === 'cash_back' || voucher.classify === 'cash_back_discount') || []

        res.success([
            {
                classify: "free_shipping",
                items: shippingVouchers
            },
            {
                classify: "discount",
                items: discountVouchers
            },
            {
                classify: 'cash_back',
                items: cashBackVouchers
            }
        ])

    } catch (error) {
        console.log(error.message);
        
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getVoucherBudgetByUserV3 = async (req: any, res:any, next:any) => {
    try {
        const { user } = req

        const savedVoucher = user.saved_vouchers || []
        const vouchers = await voucherRepository.findValidWithIds(savedVoucher)
        const vouchersAssignFromAdmin = await voucherRepository.findVoucherSystemOfUser(user._id)
        let budgetVoucher = vouchers.concat(vouchersAssignFromAdmin)
        budgetVoucher = budgetVoucher.filter((item, index) => {
            return budgetVoucher.indexOf(item) == index;
        })
        budgetVoucher = budgetVoucher.filter(voucher => {
            if(voucher.conditions && voucher.conditions["limit_per_user"] && voucher.used_by) {
                return !voucher.used_by[user._id] || voucher.used_by[user._id] < +voucher.conditions["limit_per_user"]
            }
            return true
        })

        // group by classify before return
        const discountVouchers = budgetVoucher.filter(voucher => voucher.classify === 'discount' || voucher.shop_id) || [] // including shop's vouchers 
        const shippingVouchers = budgetVoucher.filter(voucher => voucher.classify === 'free_shipping') || []
        const cashBackVouchers = budgetVoucher.filter(voucher => voucher.classify === 'cash_back' || voucher.classify === 'cash_back_discount') || []
        let shopVouchers: any = budgetVoucher.filter(voucher => voucher.shop_id)

        // check remaining usage amount
        for(let i = 0; i < discountVouchers.length; i++) {
            const voucher:any = cloneObj(discountVouchers[i])
            if(voucher?.conditions?.limit_per_user) {
                const used = await voucherAndOrderCache.getByKeyAndField(`voucher_${voucher._id.toString()}`, `${user._id.toString()}`, true) || 0
                voucher.remaining_quantity = Math.max(voucher?.conditions?.limit_per_user - used, 0)
            }
            else {
                voucher.remaining_quantity = null
            }
            discountVouchers[i] = voucher
        }

        for(let i = 0; i < shippingVouchers.length; i++) {
            const voucher:any = cloneObj(shippingVouchers[i])
            if(voucher?.conditions?.limit_per_user) {
                const used = await voucherAndOrderCache.getByKeyAndField(`voucher_${voucher._id.toString()}`, `${user._id.toString()}`) || 0
                voucher.remaining_quantity = Math.max(voucher?.conditions?.limit_per_user - +used, 0)
            }
            else {
                voucher.remaining_quantity = null
            }
            shippingVouchers[i] = voucher
        }

        for(let i = 0; i < cashBackVouchers.length; i++) {
            const voucher:any = cloneObj(cashBackVouchers[i])
            if(voucher?.conditions?.limit_per_user) {
                const used = await voucherAndOrderCache.getByKeyAndField(`voucher_${voucher._id.toString()}`, `${user._id.toString()}`) || 0
                voucher.remaining_quantity = Math.max(voucher?.conditions?.limit_per_user - +used, 0)
            }
            else {
                voucher.remaining_quantity = null
            }
            cashBackVouchers[i] = voucher
        }

        const vouchersShop = shopVouchers.map((voucher:any) => {
            return new Promise(async (resolve) => {
                voucher = cloneObj(voucher)
                const shopInfo = await shopRepo.getDetailShop(voucher.shop_id)
                voucher.shop = new ShopDTO(shopInfo).toShopVoucherItem()
                voucher.shop.avatar = addLink(`${keys.host_community}/`, shopInfo.user.gallery_image.url)
                voucher.remaining_quantity = null
                resolve(voucher)
            })
        })
        
        await Promise.all(vouchersShop).then(data => shopVouchers = data)

        res.success([
            {
                classify: "free_shipping",
                items: shippingVouchers
            },
            {
                classify: "discount",
                items: discountVouchers
            },
            {
                classify: 'cash_back',
                items: cashBackVouchers
            },
            {
                classify: "shop",
                items: shopVouchers
            }
        ])

    } catch (error) {
        console.log(error.message);
        
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}
