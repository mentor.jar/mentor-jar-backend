import responseCode from "../../../../base/responseCode"
import keys from "../../../../config/env/keys"
import { UserDTO } from "../../../../DTO/UserDTO"
import { FeedbackRepo } from "../../../../repositories/FeedbackRepo"
import { OrderRepository } from "../../../../repositories/OrderRepo"
import { UserRepo } from "../../../../repositories/UserRepo"
import { getTranslation, getUserLanguage } from "../../../../services/i18nCustom"
import notificationService, { NotifyRequestFactory } from "../../../../services/notification"
import { NOTIFY_TARGET_TYPE, NOTIFY_TYPE } from "../../../../services/notification/constants"
import { cloneObj, ObjectId } from "../../../../utils/helper"
import { addLink } from "../../../../utils/stringUtil"

const feedbackRepo = FeedbackRepo.getInstance()
const userRepo = UserRepo.getInstance()
const orderRepo = OrderRepository.getInstance()

/**
 * For now, this function will return hard JSON, so product_id will be hard value too
 * @param req 
 * @param res 
 * @param next 
 */
export const getFeedbackByProductWithFilter = async (req: any, res: any, next: any) => {
    const productID = req.params.product_id
    const paginate = req.query.limit && req.query.page ? {
        limit: req.query.limit || 20,
        page: req.query.page || 1
    } : null
    const filterAdvance = {
        option: req.query.option,
        value: req.query.value
    }

    let resultQuery = await feedbackRepo.getFeedbackByProductID(productID, filterAdvance, paginate, req.user?._id)
    resultQuery = cloneObj(resultQuery)

    resultQuery.data = resultQuery.data.map(feedback => {
        feedback = cloneObj(feedback)
        feedback.user.avatar = addLink(`${keys.host_community}/`, feedback.user.gallery_image?.url)
        feedback.user = new UserDTO(feedback.user).toSimpleUserInfo();
        return feedback
    })

    const responseData = {
        data: {
            generalFeedbackInfo: resultQuery.generalFeedbackInfo,
            feedbacks: resultQuery.data
        },
        paginate: resultQuery.paginate
    }

    res.success(responseData)
}

export const getFeedbackByShopWithFilter = async (req: any, res: any, next: any) => {
    try {
        const shopID = req.params.shop_id
        const paginate = req.query.limit && req.query.page ? {
            limit: req.query.limit || 20,
            page: req.query.page || 1
        } : null
        const filterAdvance = {
            option: req.query.option,
            value: req.query.value
        }

        let resultQuery = await feedbackRepo.getFeedbackListByShop(shopID, req.user?._id, filterAdvance, paginate)
        resultQuery = cloneObj(resultQuery)

        resultQuery.data = resultQuery.data.map(feedback => {
            feedback = cloneObj(feedback)
            feedback.user.avatar = addLink(`${keys.host_community}/`, feedback.user.gallery_image?.url)
            feedback.user = new UserDTO(feedback.user).toSimpleUserInfo();
            return feedback
        })

        const responseData = {
            data: {
                generalFeedbackInfo: resultQuery.generalFeedbackInfo,
                feedbacks: resultQuery.data
            },
            paginate: resultQuery.paginate
        }

        res.success(responseData)
    } catch (error) {
        console.log("error:", error.message);
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}

export const createFeedback = async (req: any, res: any, next: any) => {
    const user_id = req.user._id.toString()
    const { vote_star, content, medias, order_id, is_public , is_show_body_shape} = req.body
    const { product } = req

    const feedback = {
        target_id: product._id,
        target_type: "Product",
        content,
        vote_star,
        is_approved: true,
        medias,
        shop_feedback: {},
        shop_id: product.shop_id,
        user_id,
        order_id,
        is_public,
        is_show_body_shape
    }
    const feedbackCreated = await feedbackRepo.create(feedback)

    // send notification

    const targetUser = await userRepo.getUserByShopId(product.shop_id);
    const userSend: any = await userRepo.findOne({ _id: user_id });
    const order: any = await orderRepo.findOne({ _id: order_id.toString() });
    const language = await getUserLanguage(targetUser._id);
    await notificationService.saveAndSend(
        NotifyRequestFactory.generate(
            NOTIFY_TYPE.FEEDBACK_PRODUCT_CREATED,
            NOTIFY_TARGET_TYPE.SELLER,
            {
                title: getTranslation(language, 'notification.bidu_notification'),
                content: getTranslation(language, 'notification.seller_order.feedback_product_created', order.order_number),
                receiverId: targetUser._id.toString(),
                userName: userSend.nameOrganizer.userName,
                productName: product.name,
                productId: product._id.toString(),
                userId: user_id.toString(),
                orderId: order_id.toString(),
                orderNumber: order.order_number,
                feedbackId: feedbackCreated._id.toString(),
            }
        ))
    res.success(feedbackCreated, req.__("feedback.success"), responseCode.SUCCESS.code)
}

export const updateFeedback = async (req: any, res: any, next: any) => {
    const { id: feedback_id } = req.params;
    const { vote_star, content, medias, is_public , is_show_body_shape} = req.body;

    const feedback = {
        content,
        vote_star,
        medias,
        is_public,
        is_show_body_shape
    }
    const feedbackUpdated = await feedbackRepo.update(feedback_id, feedback);

    res.success(feedbackUpdated, req.__("feedback.success"), responseCode.SUCCESS.code)
}

export const createFeedbackUser = async (req: any, res: any, next: any) => {
    const user_id = req.user._id.toString()
    const { vote_star, content, medias, order_id } = req.body
    const { order } = req

    const feedback = {
        target_id: order.user_id,
        target_type: "User",
        content,
        vote_star,
        is_approved: true,
        medias,
        shop_feedback: {},
        shop_id: order.shop_id,
        user_id,
        order_id
    }
    const feedbackCreated = await feedbackRepo.create(feedback)
    // send notification
    const userSend: any = await userRepo.findOne({ _id: user_id });
    const language = await getUserLanguage(order.user_id);
    await notificationService.saveAndSend(
        NotifyRequestFactory.generate(
            NOTIFY_TYPE.FEEDBACK_USER_CREATED,
            NOTIFY_TARGET_TYPE.BUYER,
            {
                title: getTranslation(language, 'notification.bidu_notification'),
                content: getTranslation(language, 'notification.buyer_order.feedback_user_created', order.order_number),
                receiverId: order.user_id.toString(),
                userName: userSend.nameOrganizer.userName,
                shopId: order.shop_id.toString(),
                orderId: order._id.toString(),
                orderNumber: order.order_number,
                userId: user_id.toString(),
                feedbackId: feedbackCreated._id.toString(),
            }
        ))
    res.success(feedbackCreated)
}

export const shopReplyFeedback = async (req: any, res: any, next: any) => {
    try {
        let { feedback, content, medias } = req
        feedback = cloneObj(feedback)
        const time = new Date()
        let updateFeedback: any = {
            ...feedback,
            shop_feedback: {
                content,
                medias,
                created_at: time,
                updated_at: time
            }
        }
        updateFeedback = await feedbackRepo.update(feedback._id, updateFeedback)
        res.success(updateFeedback)
    } catch (error) {
        console.log(error);
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}

export const userReplyFeedback = async (req: any, res: any, next: any) => {
    try {
        let { feedback, content } = req
        feedback = cloneObj(feedback)
        const time = new Date()
        let updateFeedback: any = {
            ...feedback,
            buyer_feedback: {
                content,
                created_at: time,
                updated_at: time
            }
        }
        updateFeedback = await feedbackRepo.update(feedback._id, updateFeedback)
        res.success(updateFeedback)
    } catch (error) {
        console.log(error);
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}

export const toggleLikeFeedback = async (req: any, res: any, next: any) => {
    try {
        const { id } = req.params
        const user_id = req.user._id.toString()
        const feedback:any = await feedbackRepo.findOne({_id: ObjectId(id)})
        let message = ""
        if(feedback) {
            const currentUserLiked = feedback.user_liked || []
            let newUserLiked = currentUserLiked
            if(currentUserLiked.includes(ObjectId(user_id))) {
                newUserLiked = currentUserLiked.filter(userID => userID.toString() !== user_id)
                message = "feedback.unliked"
            }
            else {
                newUserLiked.push(ObjectId(user_id))
                message = "feedback.liked"
            }
            await feedbackRepo.update(feedback._id, {
                user_liked: newUserLiked
            })
            res.success(responseCode.SUCCESS.name, req.__(message), responseCode.SUCCESS.code)
        }
        else {
            res.error(responseCode.SERVER.name, req.__("feedback.not_found_to_update"), responseCode.SERVER.code)
        }

    } catch (error) {
        console.log(error);
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}