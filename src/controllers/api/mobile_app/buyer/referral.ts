import { executeError } from "../../../../base/appError";
import BannerRepo from "../../../../repositories/BannerRepo";
import referralService from "../../../../services/referral";

export const getReferralCode = async (req: any, res: any, next: any) => {
    try {
        const code = await referralService.generateUserCode(req.user._id);
        const bannerReferral:any = await BannerRepo.getInstance().findOne({classify: "referral"})

        res.success({ code, images: bannerReferral?.images || [] });
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const useReferralCode = async (req: any, res: any, next: any) => {
    try {
        const { referral_code } = req.body;
        const record = await referralService.useCode(req.user._id, referral_code);
        res.success(record);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}