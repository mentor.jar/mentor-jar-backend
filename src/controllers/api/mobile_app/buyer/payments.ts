

import responseCode from "../../../../base/responseCode";
import { MomoPaymentService } from "../../../../services/3rd/payments/momo";
import VnPayService from '../../../../services/3rd/payments/vnpay';
import { executeError } from "../../../../base/appError";
import FirebaseStoreService from "../../../../services/firebase";
import { TableName } from "../../../../services/firebase/constant";
import { TYPE_REQUEST } from "../../../../services/3rd/payments/definitions";


const momoPayment = MomoPaymentService.getInstance();
const firestoreService = FirebaseStoreService._();

/**
 * You can only send a cancellation request if the order has not yet been scheduled for pick up by the courier. 
 * Please note to think twice before canceling, you can only cancel once for every order. Once the order has been scheduled for pick up by the courier, it will not be possible to cancel the order
 * @param req 
 * @param res 
 * @param next 
 */
export const createUrlPayment = async (req: any, res: any, next: any) => {
    try {
        let response: any = null
        switch (req.body.type_payment) {
            case 'momo':
                response = await momoPayment.createUrlV2(req);
                break;
            case 'vnpay':
                response = await VnPayService._.createPaymentUrl(req);
        }
        res.success(response)
    } catch (error) {
        error = executeError(error);
        momoPayment.saveLogPayment({ ...req.body, message_error: error.message });
        res.error(error.name, error.message, error.statusCode);
    }
}

export const returnUrlMomoPayment = async (req: any, res: any, next: any) => {
    try {
        // console.log(req.query, "body", "return");
        res.success(null, responseCode.NO_CONTENT.name, responseCode.NO_CONTENT.code);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const IPNUrlMomoPayment = async (req: any, res: any, next: any) => {
    try {
        await momoPayment.checkSumIpnAndUpdateV2(req.body);
        momoPayment.saveLogPayment({ ...req.body, type: TYPE_REQUEST.RESPONSE_CREATED });
        res.success(null, responseCode.NO_CONTENT.name, responseCode.NO_CONTENT.code);
    } catch (error) {
        error = executeError(error);
        momoPayment.saveLogPayment({ ...req.body, message_error: error.message });
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getReturnPaymentUrl = async (req: any, res: any, next: any) => {
    try {
        const result = await VnPayService._.getReturnPaymentUrl(req);
        if (result) {
            //return deeplink thanh cong
            res.success(req.__("controller.payment.payment_success"));
        } else {
            res.success(req.__("controller.payment.payment_fail"));
        }
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getReturnPaymentIpn = async (req: any, res: any, next: any) => {
    try {
        const result = await VnPayService._.getReturnPaymentIpn(req);
        res.json(result);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const refundTransaction = async (req: any, res: any, next: any) => {
    try {
        const result = await momoPayment.refundTransaction(req.body);
        res.success('OK', result);
    } catch (error) {
        error = executeError(error);
        momoPayment.saveLogPayment({ ...req.body, message_error: error.message, type: TYPE_REQUEST.REQUEST_REFUND });
        res.error(error.name, error.message, error.statusCode);
    }
}