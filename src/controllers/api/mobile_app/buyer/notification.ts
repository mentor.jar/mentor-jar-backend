import { executeError } from "../../../../base/appError";
import { EN_LANG, KO_LANG, VN_LANG } from "../../../../base/variable";
import keys from "../../../../config/env/keys";
import { NotificationDTO } from '../../../../DTO/NotificationDTO';
import Notification from "../../../../models/Notification";
import NotificationRepo from "../../../../repositories/NotificationRepo";
import { OrderItemRepository } from "../../../../repositories/OrderItemRepo";
import { OrderRepository } from "../../../../repositories/OrderRepo";
import { ProductRepo } from "../../../../repositories/ProductRepo";
import { StreamSessionRepository } from "../../../../repositories/StreamSessionRepository";
import { UserRepo } from "../../../../repositories/UserRepo";
import { getTranslation } from "../../../../services/i18nCustom";
import notificationService, { NotificationHelper, NotifyType } from "../../../../services/notification";
import { NOTIFY_TARGET_TYPE, NOTIFY_TYPE } from "../../../../services/notification/constants";
import { addLink } from "../../../../utils/stringUtil";

const productRepo = ProductRepo.getInstance();
const userRepo = UserRepo.getInstance();
const streamSessionRepo = StreamSessionRepository.getInstance();
const orderItemRepo = OrderItemRepository.getInstance();

export const listNotification = async (req: any, res: any, next: any) => {
    try {
        const { limit = 10, page = 1, type } = req.query;
        const userId = req.user._id;
        const data = await notificationService.getNotification({
            userId,
            type,
            limit,
            page
        })
        let listPromise: any = data.data.map(notification => {
            return new Promise(async (resolve, reject) => {
                switch (notification.type) {
                    case NOTIFY_TYPE.ORDERS_CANCELED:
                    case NOTIFY_TYPE.ORDERS_CANCELING:
                    case NOTIFY_TYPE.ORDERS_CREATED:
                    case NOTIFY_TYPE.ORDERS_ACCEPT:
                    case NOTIFY_TYPE.ORDERS_SHIPPED:
                    case NOTIFY_TYPE.ORDERS_REMIND_FEEDBACK:
                        notification.context = await OrderRepository.getInstance().getDetailOrderInNotification(notification.context?.order_id);
                        break;
                    case NOTIFY_TYPE.ORDERS_GHTK:
                        notification.context.image = await OrderItemRepository.getInstance().getImageFormOrderId(notification.context?.order_id);
                        break;
                    case NOTIFY_TYPE.FEEDBACK_PRODUCT_CREATED:
                        const productFBProduct: any = await productRepo.findOne({ _id: notification.context.product_id });
                        const userFBProduct: any = await userRepo.findOne({ _id: notification.context.user_id })
                        notification.context.product_name = productFBProduct.name;
                        notification.context.user_name = userFBProduct.nameOrganizer.userName;
                        notification.context.image = productFBProduct.images[0];
                        break;
                    case NOTIFY_TYPE.FEEDBACK_USER_CREATED:
                        const userFBUser: any = await userRepo.getUserWithGallery(notification.context.user_id);
                        notification.context.user_name = userFBUser[0]?.nameOrganizer.userName;
                        notification.context.image = addLink(`${keys.host_community}/`, userFBUser[0]?.gallery_image.url);
                        break;
                    case NOTIFY_TYPE.START_LIVESTREAM:
                        const streamSessionStart: any = await streamSessionRepo.findOne({ _id: notification.context.stream_session_id });
                        notification.context.image = streamSessionStart?.image;
                        break;
                    case NOTIFY_TYPE.REFUND_REQUEST_SENT:
                    case NOTIFY_TYPE.REFUND_SHOP_APPROVED:
                    case NOTIFY_TYPE.REFUND_SHOP_REJECTED:
                    case NOTIFY_TYPE.REFUND_SHOP_RECEIVED:
                    case NOTIFY_TYPE.REFUND_ADMIN_APPROVED:
                    case NOTIFY_TYPE.REFUND_ADMIN_REJECTED:
                    case NOTIFY_TYPE.REFUND_ADMIN_COMPLETED:
                    case NOTIFY_TYPE.REFUND_REQUEST_CANCELED:
                        const orderItemEl: any = await orderItemRepo.findOne({ _id: notification.context.item_id });
                        const productRefund: any = await productRepo.findOne({ _id: orderItemEl.product_id });
                        notification.context.image = productRefund?.images[0];
                        notification.context.product_name = productRefund.name;
                        break;
                    default:
                }
                resolve(notification);
            })
        })
        await Promise.all(listPromise)
        // data.data = data.data.map(item => NotificationDTO.parse(item))

        return res.success(data);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

const _getContentWithLanguage = ({ language, notification, product, user }: any) => {
    
    let translate: string = null;
    switch (true) {
        case notification.type == NOTIFY_TYPE.ORDERS_CANCELED &&
            notification.targetType == NOTIFY_TARGET_TYPE.BUYER:
            translate = 'notification.buyer_order.order_canceled';
            break;
        case notification.type == NOTIFY_TYPE.ORDERS_CANCELED &&
            notification.targetType == NOTIFY_TARGET_TYPE.SELLER:
            translate = 'notification.seller_order.order_canceled';
            break;
        case notification.type == NOTIFY_TYPE.ORDERS_CANCELING &&
            notification.targetType == NOTIFY_TARGET_TYPE.SELLER:
            translate = 'notification.seller_order.order_canceling';
            break;
        case notification.type == NOTIFY_TYPE.ORDERS_CREATED &&
            notification.targetType == NOTIFY_TARGET_TYPE.SELLER:
            translate = 'notification.seller_order.order_created';
            break;
        case notification.type == NOTIFY_TYPE.ORDERS_ACCEPT &&
            notification.targetType == NOTIFY_TARGET_TYPE.BUYER:
            translate = 'notification.buyer_order.order_accepted';
            break;
        case notification.type == NOTIFY_TYPE.ORDERS_SHIPPED: // old notification
            translate = 'notification.order_shipped';
            break;
        case notification.type == NOTIFY_TYPE.ORDERS_REMIND_FEEDBACK &&
            notification.targetType == NOTIFY_TARGET_TYPE.BUYER:
            translate = 'notification.buyer_order.remind_feedback';
            break;
        case notification.type == NOTIFY_TYPE.FEEDBACK_PRODUCT_CREATED &&
            notification.targetType == NOTIFY_TARGET_TYPE.SELLER:
            translate = 'notification.seller_order.feedback_product_created';
            break;
        case notification.type == NOTIFY_TYPE.FEEDBACK_USER_CREATED &&
            notification.targetType == NOTIFY_TARGET_TYPE.BUYER:
            translate = 'notification.buyer_order.feedback_user_created';
            break;
        case notification.type == NOTIFY_TYPE.START_LIVESTREAM :
            translate = 'controller.stream.start_live_stream_notification';
            break;
        case notification.type == NOTIFY_TYPE.REFUND_REQUEST_SENT :
            translate = 'notification.refund_sent';
            break;
        case notification.type == NOTIFY_TYPE.REFUND_SHOP_APPROVED :
            translate = 'notification.refund_shop_approved';
            break;
        case notification.type == NOTIFY_TYPE.REFUND_SHOP_REJECTED :
            translate = 'notification.refund_shop_rejected';
            break;
        case notification.type == NOTIFY_TYPE.REFUND_SHOP_RECEIVED :
            translate = 'notification.refund_shop_received';
            break;
        case notification.type == NOTIFY_TYPE.REFUND_ADMIN_APPROVED :
            translate = 'notification.refund_admin_approved';
            break;
        case notification.type == NOTIFY_TYPE.REFUND_ADMIN_REJECTED :
            translate = 'notification.refund_admin_rejected';
            break;
        case notification.type == NOTIFY_TYPE.REFUND_ADMIN_COMPLETED :
            translate = 'notification.refund_admin_completed';
            break;
        case notification.type == NOTIFY_TYPE.REFUND_REQUEST_CANCELED :
            translate = 'notification.refund_request_cancelded';
            break;
        case notification.type == NOTIFY_TYPE.CREATE_SHOP_APPROVED :
            translate = 'notification.create_shop_approved';
            break;
        case notification.type == NOTIFY_TYPE.CREATE_SHOP_REJECTED :
            translate = 'notification.create_shop_rejected';
            break;
        case notification.type == NOTIFY_TYPE.ORDERS_GHTK && 
            notification.targetType == NOTIFY_TARGET_TYPE.SELLER:
            translate = `notification.seller_shipping_status.${notification.context.status_id}`;
            break;
        case notification.type == NOTIFY_TYPE.ORDERS_GHTK && 
            notification.targetType == NOTIFY_TARGET_TYPE.BUYER:
            translate = `notification.buyer_shipping_status.${notification.context.status_id}`;
            break;
        case notification.type == NOTIFY_TYPE.REWARD_VOUCHER &&
            notification.context.reward_type == 'introduced':
                translate = 'referral.introduced_reward';
                break;
        case notification.type == NOTIFY_TYPE.REWARD_VOUCHER &&
            notification.context.reward_type == 'introduce':
                translate = 'referral.introduce_reward_v2';
                break;
        case notification.type == NOTIFY_TYPE.ASSIGN_CASH_BACK:
            translate = 'notification.buyer_order.assign_cash_back';
            break;
        case notification.type == NOTIFY_TYPE.ORDER_INVALID:
            translate = 'notification.order_invalid';
            break;
        case notification.type == NOTIFY_TYPE.ORDERS_ACCEPT &&
            notification.targetType == NOTIFY_TARGET_TYPE.SELLER:
            translate = 'notification.seller_order.order_accepted';
            break;
        case notification.type == NOTIFY_TYPE.PAYMENT_MOMO_PAID &&
            notification.targetType == NOTIFY_TARGET_TYPE.BUYER:
            translate = 'notification.payment.payment_momo_paid';
            break;
        case notification.type == NOTIFY_TYPE.GB_ROOM_CREATED &&
            notification.targetType == NOTIFY_TARGET_TYPE.BUYER:
            translate = 'notification.order.group_buy_room_created';
            break;
        case notification.type == NOTIFY_TYPE.GB_JOIN &&
            notification.targetType == NOTIFY_TARGET_TYPE.BUYER:
            translate = 'notification.order.group_buy_join';
            break;
        case notification.type == NOTIFY_TYPE.GB_NEED_INVITE &&
            notification.targetType == NOTIFY_TARGET_TYPE.BUYER:
            translate = 'notification.order.group_buy_need_invite';
            break;
        case notification.type == NOTIFY_TYPE.GB_HOST_PAID &&
            notification.targetType == NOTIFY_TARGET_TYPE.BUYER:
            translate = 'notification.order.group_buy_host_paid';
            break;
        case notification.type == NOTIFY_TYPE.GB_HOST_MISSING_1_PER &&
            notification.targetType == NOTIFY_TARGET_TYPE.BUYER:
            translate = 'notification.order.group_buy_host_missing_1_person';
            break;
        case notification.type == NOTIFY_TYPE.GB_CANCELED &&
            notification.targetType == NOTIFY_TARGET_TYPE.BUYER:
            translate = 'notification.order.group_buy_canceld';
            break;
        case notification.type == NOTIFY_TYPE.GB_MEMBER_MISSING_1_PER &&
            notification.targetType == NOTIFY_TARGET_TYPE.BUYER:
            translate = 'notification.order.group_buy_missing_1_person';
            break;
        case notification.type == NOTIFY_TYPE.GB_MEMBER_PAID &&
            notification.targetType == NOTIFY_TARGET_TYPE.BUYER:
            translate = 'notification.order.group_buy_member_paid';
            break;
        case notification.type == NOTIFY_TYPE.GB_MEMBER_INVITED &&
            notification.targetType == NOTIFY_TARGET_TYPE.BUYER:
            translate = 'notification.order.group_buy_member_invited';
            break;
        default:
            break;
    }

    if (!translate) return;
    let result = null;
    switch (notification.type) {
        case NOTIFY_TYPE.ORDERS_CANCELED:
        case NOTIFY_TYPE.ORDERS_CANCELING:
        case NOTIFY_TYPE.ORDERS_CREATED:
        case NOTIFY_TYPE.ORDERS_ACCEPT:
        case NOTIFY_TYPE.ORDERS_SHIPPED:
        case NOTIFY_TYPE.ORDERS_REMIND_FEEDBACK:
        case NOTIFY_TYPE.FEEDBACK_PRODUCT_CREATED:
        case NOTIFY_TYPE.FEEDBACK_USER_CREATED:
        case NOTIFY_TYPE.ORDERS_GHTK:
            result = getTranslation(
                language,
                translate,
                notification.context?.order_number || '123456789'
            );
            break;
        case NOTIFY_TYPE.START_LIVESTREAM:
            result = getTranslation(
                language,
                translate,
                notification.context.user_name
            );
            break;
        case NOTIFY_TYPE.REFUND_REQUEST_SENT:
        case NOTIFY_TYPE.REFUND_SHOP_APPROVED:
        case NOTIFY_TYPE.REFUND_SHOP_REJECTED:
        case NOTIFY_TYPE.REFUND_SHOP_RECEIVED:
        case NOTIFY_TYPE.REFUND_ADMIN_APPROVED:
        case NOTIFY_TYPE.REFUND_ADMIN_REJECTED:
        case NOTIFY_TYPE.REFUND_ADMIN_COMPLETED:
        case NOTIFY_TYPE.REFUND_REQUEST_CANCELED:
            result = getTranslation(
                language,
                translate,
                product.name,
                notification.context.order_number
            );
            break;
        case NOTIFY_TYPE.CREATE_SHOP_APPROVED:
        case NOTIFY_TYPE.CREATE_SHOP_REJECTED:
            result = getTranslation(
                language,
                translate,
            );
            break;
        case NOTIFY_TYPE.REWARD_VOUCHER:
            const count = notification.context.count
            result = (count < 5) ? 
                getTranslation(language, translate, count) : 
                getTranslation(language, translate, count) + ' ' + getTranslation(language, 'referral.introduce_reward_voucher' , count);
        case NOTIFY_TYPE.ASSIGN_CASH_BACK:
            result = getTranslation(language, translate, notification.context.order_number)
            break;
        case NOTIFY_TYPE.ORDER_INVALID:
            result = getTranslation(
                language,
                translate,
            );
            break;
        case NOTIFY_TYPE.PAYMENT_MOMO_PAID:
            result = getTranslation(
                language,
                translate,
                notification.context.order_number
            );
            break;
        case NOTIFY_TYPE.GB_ROOM_CREATED:
            result = getTranslation(
                language,
                translate,
            );
            break;
        case NOTIFY_TYPE.GB_JOIN:
            result = getTranslation(
                language,
                translate,
                user?.nameOrganizer?.userName
            );
            break;
        case NOTIFY_TYPE.GB_NEED_INVITE:
            result = getTranslation(
                language,
                translate,
            );
            break;
        case NOTIFY_TYPE.GB_HOST_PAID:
            result = getTranslation(
                language,
                translate,
            );
            break;
        case NOTIFY_TYPE.GB_HOST_MISSING_1_PER:
            result = getTranslation(
                language,
                translate,
            );
            break;
        case NOTIFY_TYPE.GB_CANCELED:
            result = getTranslation(
                language,
                translate,
                notification.context.order_number
            );
            break;
        case NOTIFY_TYPE.GB_MEMBER_MISSING_1_PER:
            result = getTranslation(
                language,
                translate
            );
            break;
        case NOTIFY_TYPE.GB_MEMBER_PAID:
            result = getTranslation(
                language,
                translate
            );
            break;
        case NOTIFY_TYPE.GB_MEMBER_INVITED:
            result = getTranslation(
                language,
                translate,
                user.nameOrganizer.userName
            );
            break;
        default:
            break;
    }
    return result;
}

const _getUnderlined = (language) => {
    let result = "";
    switch (language) {
        case 'ko':
            result = "리뷰"
            break;
        case 'en':
            result = "reviewed"
            break;
        case 'vi':
            result = "đánh giá"
            break;
    }
    return result;
}

/**
 * 
 * @param language: language of current login 
 * @param link: link of locale under_lined 
 * @returns
 */

const _getUnderlinedByLanguage = (language: string, link: any) => {
    return getTranslation(language, link);
}

export const listNotificationV2 = async (req: any, res: any, next: any) => {
    try {
        const { limit = 10, page = 1, type } = req.query;
        const userId = req.user._id;
        const data = await notificationService.getNotification({
            userId,
            type,
            limit,
            page
        });

        const language =
        [VN_LANG, EN_LANG, KO_LANG].indexOf(req.headers['accept-language']) > -1
            ? req.headers['accept-language']
            : VN_LANG;
        let listPromise: any = data.data.map(notification => {
            return new Promise(async (resolve, reject) => {
                switch (notification.type) {
                    case NOTIFY_TYPE.ORDERS_CANCELED:
                    case NOTIFY_TYPE.ORDERS_CANCELING:
                    case NOTIFY_TYPE.ORDERS_CREATED:
                    case NOTIFY_TYPE.ORDERS_ACCEPT:
                    case NOTIFY_TYPE.ORDERS_SHIPPED:
                        const orderShipp = await OrderRepository.getInstance().getDetailOrderInNotification(notification.context?.order_id);
                        notification.content = {};
                        notification.content.image = orderShipp?.image;
                        notification.content.content = _getContentWithLanguage({ language, notification });
                        notification.content.bold_content = orderShipp?.order_number;
                        break;
                    case NOTIFY_TYPE.ORDERS_REMIND_FEEDBACK:
                        const orderFeedback = await OrderRepository.getInstance().getDetailOrderInNotification(notification.context?.order_id);
                        notification.content = {};
                        notification.content.image = orderFeedback?.image;
                        notification.content.content = _getContentWithLanguage({ language, notification });
                        notification.content.bold_content = orderFeedback.order_number;
                        notification.content.under_lined = _getUnderlined(language);
                        break;
                    case NOTIFY_TYPE.ORDERS_GHTK:
                        notification.content = {};
                        notification.content.image = await OrderItemRepository.getInstance().getImageFormOrderId(notification.context?.order_id);
                        notification.content.content = _getContentWithLanguage({ language, notification });
                        notification.content.bold_content = notification.context.order_number;
                        break;
                    case NOTIFY_TYPE.FEEDBACK_PRODUCT_CREATED:
                        const productFBProduct: any = await productRepo.findOne({ _id: notification.context.product_id });
                        notification.content = {};
                        // const userFBProduct: any = await userRepo.findOne({ _id: notification.context.user_id })
                        // notification.context.product_name = productFBProduct.name;
                        // notification.context.user_name = userFBProduct.nameOrganizer.userName;
                        notification.content.image = productFBProduct.images[0];
                        notification.content.content = _getContentWithLanguage({ language, notification });
                        notification.content.bold_content = notification.context.order_number;
                        break;
                    case NOTIFY_TYPE.FEEDBACK_USER_CREATED:
                        const userFBUser: any = await userRepo.getUserWithGallery(notification.context.user_id);
                        notification.content = {};
                        notification.content.image = addLink(`${keys.host_community}/`, userFBUser[0]?.gallery_image.url);
                        notification.content.content = _getContentWithLanguage({ language, notification });
                        notification.content.bold_content = notification.context.order_number;
                        break;
                    case NOTIFY_TYPE.START_LIVESTREAM:
                        const streamSessionStart: any = await streamSessionRepo.findOne({ _id: notification.context.stream_session_id });
                        notification.content = {};
                        notification.content.image = streamSessionStart?.image;
                        notification.content.content = _getContentWithLanguage({ language, notification });
                        notification.content.bold_content = notification.context.user_name;
                        break;
                    case NOTIFY_TYPE.REFUND_REQUEST_SENT:
                    case NOTIFY_TYPE.REFUND_SHOP_APPROVED:
                    case NOTIFY_TYPE.REFUND_SHOP_REJECTED:
                    case NOTIFY_TYPE.REFUND_SHOP_RECEIVED:
                    case NOTIFY_TYPE.REFUND_ADMIN_APPROVED:
                    case NOTIFY_TYPE.REFUND_ADMIN_REJECTED:
                    case NOTIFY_TYPE.REFUND_ADMIN_COMPLETED:
                    case NOTIFY_TYPE.REFUND_REQUEST_CANCELED:
                        const orderItemEl: any = await orderItemRepo.findOne({ _id: notification.context.item_id });
                        const productRefund: any = await productRepo.findOne({ _id: orderItemEl.product_id });
                        notification.content = {};
                        notification.content.image = productRefund?.images[0];
                        notification.content.content = _getContentWithLanguage({ language, notification, product: productRefund });
                        notification.content.bold_content = notification.context.order_number;
                        break;
                    case NOTIFY_TYPE.CREATE_SHOP_APPROVED:
                    case NOTIFY_TYPE.CREATE_SHOP_REJECTED:
                    case NOTIFY_TYPE.REWARD_VOUCHER:
                        const userVoucher: any = await userRepo.getUserWithGallery(notification.context.user_id);
                        notification.content = {};
                        notification.content.image = addLink(`${keys.host_community}/`, userVoucher[0]?.gallery_image.url);
                        notification.content.content = _getContentWithLanguage({ language, notification });
                        break;
                    case NOTIFY_TYPE.ASSIGN_CASH_BACK:
                        const order = await OrderRepository.getInstance().getDetailOrderInNotification(notification.context?.order_id);
                        notification.content = {};
                        notification.content.image = order?.image;
                        notification.content.content = _getContentWithLanguage({ language, notification });
                        notification.content.bold_content = order?.order_number;
                    case NOTIFY_TYPE.ORDER_INVALID:
                        const user: any = await userRepo.getUserWithGallery(notification.context.user_id);
                        notification.content = {};
                        notification.content.image = addLink(`${keys.host_community}/`, user[0]?.gallery_image.url);;
                        notification.content.content = _getContentWithLanguage({ language, notification });
                        break;
                    case NOTIFY_TYPE.PAYMENT_MOMO_PAID:
                        const orderMomoPaid = await OrderRepository.getInstance().getDetailOrderInNotification(notification.context?.order_id);
                        notification.content = {};
                        notification.content.image = orderMomoPaid?.image;
                        notification.content.content = _getContentWithLanguage({ language, notification });
                        notification.content.bold_content = orderMomoPaid?.order_number;
                        break;
                    case NOTIFY_TYPE.GB_ROOM_CREATED:
                        const productGBJoin: any = await productRepo.findOne({ _id: notification.context.product_id });
                        notification.content = {};
                        notification.content.image = productGBJoin?.images[0];
                        notification.content.content = _getContentWithLanguage({ language, notification, product: productGBJoin });
                        notification.content.under_lined = _getUnderlinedByLanguage(language, "under_lined.invite_more_friend");
                        break;
                    case NOTIFY_TYPE.GB_JOIN:
                        const userJoin: any = await userRepo.getUserWithGallery(notification.context.user_id);
                        notification.content = {};
                        notification.content.image = addLink(`${keys.host_community}/`, userJoin[0]?.gallery_image.url);
                        notification.content.content = _getContentWithLanguage({ language, notification, user: userJoin[0] });
                        break;
                    case NOTIFY_TYPE.GB_MEMBER_INVITED:
                        const userInvited: any = await userRepo.getUserWithGallery(notification.context.user_id);
                        notification.content = {};
                        notification.content.image = addLink(`${keys.host_community}/`, userInvited[0]?.gallery_image.url);
                        notification.content.content = _getContentWithLanguage({ language, notification, user: userInvited[0] });
                        break;
                    case NOTIFY_TYPE.GB_MEMBER_PAID:
                        const productPaid: any = await productRepo.findOne({ _id: notification.context.product_id });
                        notification.content = {};
                        notification.content.image = productPaid?.images[0];
                        notification.content.content = _getContentWithLanguage({ language, notification });
                        break;
                    case NOTIFY_TYPE.GB_HOST_PAID:
                        const productHostPaid: any = await productRepo.findOne({ _id: notification.context.product_id });
                        notification.content = {};
                        notification.content.image = productHostPaid?.images[0];
                        notification.content.content = _getContentWithLanguage({ language, notification });
                        break;
                    case NOTIFY_TYPE.GB_MEMBER_PAID:
                        const productMemberPaid: any = await productRepo.findOne({ _id: notification.context.product_id });
                        notification.content = {};
                        notification.content.image = productMemberPaid?.images[0];
                        notification.content.content = _getContentWithLanguage({ language, notification });
                        break;
                    case NOTIFY_TYPE.GB_NEED_INVITE:
                        const productNeedInvite: any = await productRepo.findOne({ _id: notification.context.product_id });
                        notification.content = {};
                        notification.content.image = productNeedInvite?.images[0];
                        notification.content.content = _getContentWithLanguage({ language, notification });
                        break;
                    case NOTIFY_TYPE.GB_HOST_MISSING_1_PER:
                        const productMissing1Person: any = await productRepo.findOne({ _id: notification.context.product_id });
                        notification.content = {};
                        notification.content.image = productMissing1Person?.images[0];
                        notification.content.content = _getContentWithLanguage({ language, notification });
                        notification.content.under_lined = _getUnderlinedByLanguage(language, "under_lined.invite_more");
                        break;
                    case NOTIFY_TYPE.GB_MEMBER_MISSING_1_PER:
                        const productMBMissing1Person: any = await productRepo.findOne({ _id: notification.context.product_id });
                        notification.content = {};
                        notification.content.image = productMBMissing1Person?.images[0];
                        notification.content.content = _getContentWithLanguage({ language, notification });
                        notification.content.under_lined = _getUnderlinedByLanguage(language, "under_lined.invite_friends");
                        break;
                    case NOTIFY_TYPE.GB_CANCELED:
                        const productCancel: any = await productRepo.findOne({ _id: notification.context.product_id });
                        notification.content = {};
                        notification.content.image = productCancel?.images[0];
                        notification.content.content = _getContentWithLanguage({ language, notification });
                        notification.content.under_lined = _getUnderlinedByLanguage(language, "under_lined.here");
                        break;
                    default:
                }
                resolve(notification);
            })
        })
        await Promise.all(listPromise)
        // data.data = data.data.map(item => NotificationDTO.parse(item))

        return res.success(data);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const topNew = async (req: any, res: any, next: any) => {
    try {
        const userId = req.user._id;
        const DEFINED_LIST = [
            NotifyType.NOTIFY_TYPE.PROMOTIONS,
            NotifyType.NOTIFY_TYPE.EVENTS,
            NotifyType.NOTIFY_TYPE.PRIZES,
        ]

        let data = await Promise.all(DEFINED_LIST.map(type => {
            return notificationService.getLatestNotification(userId, type)
                .then((data) => Promise.resolve({
                    type,
                    item: NotificationDTO.parse(data)
                }))
                .catch((err) => Promise.resolve({
                    type,
                    item: null
                }))
        }));



        return res.success(data);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const sendTest = async (req: any, res: any, next: any) => {
    try {
        const request = req.body;
        const userId = req.user._id;
        await notificationService.saveAndSend({
            receiverId: request.receiverId,
            contentType: request.contentType,
            type: request.type,
            creatorId: userId,
            ...request
        })
        return res.success();
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const markRead = async (req: any, res: any, next: any) => {
    try {
        const request = req.body;
        const userId = req.user._id;
        await notificationService.changeStatusNotification({
            status: request.isRead,
            notifyIds: request.notifyIds,
            userId
        })
        return res.success();
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const unreadCount = async (req: any, res: any, next: any) => {
    try {
        const unreadNumber = await NotificationHelper.getUnreadMessageByApp(req.user._id)
        const response = { unread: unreadNumber };
        return res.success(response);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}