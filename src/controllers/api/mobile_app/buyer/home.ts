import { executeError } from "../../../../base/appError";
import responseCode from "../../../../base/responseCode";
import { CATEGORIES_LOCALIZED, EN_LANG, KO_LANG, SHUFFLE_NEWEST_PRODUCT_PAGE, SHUFFLE_SUGGEST_PRODUCT_PAGE, VN_LANG } from "../../../../base/variable";
import keys from "../../../../config/env/keys";
import { BannerDTO } from '../../../../DTO/BannerDTO';
import { CategoryDTO } from "../../../../DTO/CategoryDTO";
import { ProductDTO } from '../../../../DTO/Product';
import { StreamSessionDTO } from '../../../../DTO/StreamSessionDTO';
import { UserDTO } from "../../../../DTO/UserDTO";
import BannerRepo from '../../../../repositories/BannerRepo';
import { CartItemRepo } from '../../../../repositories/CartItemRepo';
import { CategoryRepo } from "../../../../repositories/CategoryRepo";
import { FollowRepo } from "../../../../repositories/FollowRepo";
import NotificationRepo from '../../../../repositories/NotificationRepo';
import { OrderItemRepository } from "../../../../repositories/OrderItemRepo";
import { OrderRepository } from "../../../../repositories/OrderRepo";
import { ProductRepo } from "../../../../repositories/ProductRepo";
import { PromotionProgramRepo } from "../../../../repositories/PromotionProgramRepo";
import { SearchKeywordRepo } from '../../../../repositories/SearchKeywordRepo';
import { ShopRepo } from "../../../../repositories/ShopRepo";
import { StreamSessionRepository } from '../../../../repositories/StreamSessionRepository';
import { VoucherRepository } from "../../../../repositories/VoucherRepository";
import { handleShopAndProduct, handleTopShop, handleTopShopV2, saveAccessHomePage, topProductOfSystem } from "../../../../services/api/home";
import { cloneObj, isMappable, ObjectId, timeOutOfPromise } from "../../../../utils/helper";
import { addLink } from "../../../../utils/stringUtil";
import { filterShop } from "../../../handleRequests/shop/shop";
import { GlobalSearchProductRequest, ProductSort } from "../../../serviceHandles/home/definition";
import HomeService from '../../../serviceHandles/home/service';
import { getMinMax } from "../../../serviceHandles/product";
import _ from "lodash";
import CacheService from "../../../../services/cache";
import { UserRepo } from "../../../../repositories/UserRepo";
import { InMemoryProductStore } from "../../../../SocketStores/ProductStore";
import { InMemorySellerStore } from "../../../../SocketStores/SellerStore";
import { bookmarkByUserAndFilterSoldOut } from "../../../../utils/utils";
import { TTL } from "../../../../SocketStores/constants";
import setCacheHeaders from "../../../../services/headerCache/headerCache";
import ProductService from "../../../serviceHandles/product/service";
import { VoucherDTO } from "../../../../DTO/VoucherDTO";

const productRepo = ProductRepo.getInstance();
const cartItemRepo = CartItemRepo.getInstance();
const bannerRepo = BannerRepo.getInstance();
const notificationRepo = NotificationRepo.getInstance();
const streamSessionRepository = StreamSessionRepository.getInstance();
const orderRepo = OrderRepository.getInstance();
const orderItemRepo = OrderItemRepository.getInstance();
const searchKeywordRepo = SearchKeywordRepo.getInstance();
const voucherRepo = VoucherRepository.getInstance();
const shopRepo = ShopRepo.getInstance();
const promotionRepo = PromotionProgramRepo.getInstance();

const categoryRepo = CategoryRepo.getInstance()
const userRepo = UserRepo.getInstance()
const productCache = InMemoryProductStore.getInstance();
const shopCache = InMemorySellerStore.getInstance();
const homeService = HomeService._;
const productService = ProductService._;
/**
 * Code for create mock data
 * @param req 
 * @param res 
 * @param next 
 */

export const homeDataV2 = async (req: any, res: any, next: any) => {
    try {
        const language = req.headers["accept-language"] || VN_LANG;
        const userID = req.user?._id;
        const cacheService = CacheService._
        let [
            total_unread_notify,
            total_product_in_cart,
            system_banner,
            system_banner_voucher,
            top_livestreams,
            suggest_product,
            system_category,
            newest_product,
            top_seller,
            top_product,
            top_keyword
        ] = await Promise.all([
            (async () => {
                try {
                    if (userID) {
                        const dataNotification: any = await notificationRepo.getNotifications({
                            userId: userID,
                            status: false,
                            type: "",
                            page: 0,
                            limit: 100
                        })
                        return dataNotification?.paginate?.total_record;
                    }
                    return null
                } catch (error) {
                    console.log(error);
                    return 0;
                }
            })(),
            (async () => {
                try {
                    let cartItemByType = await cartItemRepo.findAndGroupByShop(userID)
                    let cartItems = [...cartItemByType.valid, ...cartItemByType.inValid];
                    return cartItems.reduce((total, currentItem) => total += currentItem?.items?.length, 0)
                } catch (error) {
                    console.log(error);
                    return 0;
                }
            })(),
            (async () => {
                try {
                    let systemBanners: any = await bannerRepo.findHomeBanner({
                        shop_id: null,
                        start_time: { '$lte': new Date() },
                        end_time: { '$gte': new Date() },
                        $or: [
                            { position: "top" },
                            { classify: { $exists: false } }
                        ]
                    }, { 'priority': 1 }, 10);
                    const promisesCheckVoucher =  systemBanners.map( async banner => {
                        banner = cloneObj(banner)
                        if (Array.isArray(banner?.vouchers) && banner?.vouchers.length > 0) {
                            const vouchers = await voucherRepo.findValidWithIds(banner?.vouchers);
                            banner.vouchers = vouchers.map(voucher => voucher._id);
                        }
                        if(banner.classify === "shop_view") {
                            const shopBanner:any = await shopRepo.findOne({_id: ObjectId(banner.advance_actions?.shop)})
                            banner.advance_actions.user = shopBanner?.user_id
                        }
                        return BannerDTO.newInstance(banner).toSimpleJSON()
                    });

                    const banner = await Promise.all(promisesCheckVoucher);
                    return banner;

                } catch (error) {
                    console.log(error);
                    return [];
                }
            })(),
            (async () => {
                try {
                    let systemBanners: any = await bannerRepo.findHomeBanner({
                        shop_id: null,
                        // start_time: { '$lte': new Date() },
                        // end_time: { '$gte': new Date() },
                        $or: [
                            { position: "middle" },
                            { classify: { $exists: false } }
                        ]
                    }, { 'created_at': 1 }, 10);
                    return systemBanners.map(banner => BannerDTO.newInstance(banner).toSimpleJSON());
                } catch (error) {
                    console.log(error);
                    return [];
                }
            })(),
            (async () => {
                try {
                    // let listStreamSession: any = await streamSessionRepository.getListStreamSessionByViewer({
                    //     page: 0,
                    //     limit: 5
                    // });
                    // return listStreamSession.data.map(session => StreamSessionDTO.newInstance(session).completeDataWithDTO(req.user));
                    return [];
                } catch (error) {
                    console.log(error);
                    return [];
                }
            })(),
            (async () => {
                try {
                    // Suggest product
                    let products:any = []
                    if(!cacheService.has('home_suggest_product')) {
                        let productSuggest: any = await productRepo.findProductHome({ orderBy: 'best_sell' }, 1, 20)
                        const promise = productSuggest.data.map((product) => {
                            return new Promise(async (resolve, reject) => {
                                let item : any= new ProductDTO(product)
                                item = item.getProductComplete(req.user);
                                const promotion = await promotionRepo.getPromotionByProduct(item._id)
                                if (promotion) {
                                    item.discount_percent = promotion.discount
                                }
                                else {
                                    item.discount_percent = 0
                                }
                                // item.sold = product.order_items && product.order_items.length ? product.order_items[0].sold : 0
                                resolve(item)
                            })
                        })
                        return Promise.all(promise).then(data => {
                            console.log("Create home_suggest_product");
                            cacheService.set('home_suggest_product', data)
                            return data
                        })
                    }
                    else {
                        return cacheService.get('home_suggest_product')
                    }
                } catch (error) {
                    console.log(error);
                    return [];
                }
            })(),
            (async () => {
                try {
                    let systemCategories: any = await categoryRepo.getCategorySystemOrderBy();
                    return systemCategories.map(category =>  {
                        const categoryLocalized = CATEGORIES_LOCALIZED.get(category.name.trim().toLowerCase());
                        categoryLocalized && categoryLocalized[language] ? category.name = categoryLocalized[language] : '';
                        return new CategoryDTO(category).toSimpleJSON();
                    })
                } catch (error) {
                    console.log(error);
                    return [];
                }
            })(),
            (async () => {
                try {
                    if(!cacheService.has('home_newest_product')) {
                        console.log("GET FROM DB");
                        
                        let newProduct = await productRepo.findProductHome({orderBy: 'newest'}, 0, 30)
                        const promise = newProduct.data.map((product) => {
                            return new Promise(async (resolve, reject) => {
                                let item : any= new ProductDTO(product);
                                item = item.getProductComplete(req.user);
                                const promotion = await promotionRepo.getPromotionByProduct(item._id)
                                if (promotion) {
                                    item.discount_percent = promotion.discount
                                }
                                else {
                                    item.discount_percent = 0
                                }
                                // item.sold = product.order_items && product.order_items.length ? product.order_items[0].sold : 0
                                resolve(item)
                            })
                        })
                        return Promise.all(promise).then(data => {
                            console.log("Create home_newest_product");
                            cacheService.set('home_newest_product', data)
                            return data
                        })
                    }
                    else {
                        console.log("GET FROM CACHE");
                        return cacheService.get('home_newest_product')
                    }
                } catch (error) {
                    console.log(error);
                    return [];
                }
            })(),
            (async () => {
                if(!cacheService.has('home_top_shop')) {
                    let todayShops = await shopRepo.getShopRanking(1, 5)
                    todayShops = await handleTopShop(todayShops, req.user, false)
                    console.log("Create home_top_shop");
                    cacheService.set('home_top_shop', todayShops)
                    return todayShops
                }
                else {
                    return cacheService.get('home_top_shop')
                }
            })(),
            (async () => {
                if(!cacheService.has('home_top_product')) {
                    let products:any = []
                    const topProduct:any = await topProductOfSystem(req.user, 1, 30)
                    console.log("Create home_top_product");
                    products = topProduct?.data || []
                    cacheService.set('home_top_product', products)
                    return products
                }
                else {
                    return cacheService.get('home_top_product')
                }
            })(),
            (async () => {
                if(!cacheService.has('home_top_keyword')) {
                    let keywords = await searchKeywordRepo.top(30)
                    keywords = cloneObj(keywords)
                    let promises = []
                    promises = keywords.map(async (item, index) => {
                        return new Promise(async (resolve, reject) => {
                            item.avatar = ""
                            const products = await productRepo.countProductByKeyword(item.keyword)
                            item.total_product = products.length
                            const image = products.find(item => item.images.length)?.images[0]
                            item.avatar = image || ""
                            resolve(item)
                        })
                    })

                    keywords = await Promise.all(promises)
                    keywords = keywords.filter(item => item?.total_product > 0)
                    console.log("Create home_top_keyword");
                    cacheService.set('home_top_keyword', keywords)
                    return keywords
                }
                else {
                    return cacheService.get('home_top_keyword')
                }
            })()
        ]);

        const results = {
            total_unread_notify: total_unread_notify || 0,
            total_product_in_cart: total_product_in_cart || 0,
            system_banner: system_banner || [],
            system_banner_voucher: system_banner_voucher || [],
            top_livestreams: top_livestreams || [],
            top_product: _.shuffle(top_product).slice(0, 5) || [],
            top_shop: top_seller,
            suggest_product: _.shuffle(suggest_product).slice(0, 10) || [],
            system_category: system_category || [],
            top_keyword: _.shuffle(top_keyword).slice(0, 10) || [],
            newest_product: _.shuffle(newest_product).slice(0, 10) || []
        }
        saveAccessHomePage()
        res.success(results);
    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}

export const rankingPage = async (req: any, res: any, next: any) => {
    try {
        setCacheHeaders(res, 30);
        const { type, page = 1, limit = 20 } = req.query
        const env = process.env.NODE_ENV === 'production' ? 'production' : 'staging'
        if (type.toUpperCase() === 'PRODUCT') {
            //const topProduct = await topProductOfSystem(req.user, page, limit)
            const topProduct = await productService.getTopProductSystem(+page, +limit);
            res.success(topProduct)
        }
        else if (type.toUpperCase() === 'SELLER') {
            let todayShops = []
            todayShops = await shopCache.get(`topSellers_${env}_page_${page}`) || []
            if(!todayShops || !todayShops?.length) {
                todayShops = await shopRepo.getShopRanking(page, limit)
                todayShops = cloneObj(todayShops)
            }
            else {
                // const listFavoriteProducts = req?.user?.favorite_products || [];
                // todayShops.forEach(item => {
                //     if(item.products?.length) {
                //         item.products = bookmarkByUserAndFilterSoldOut(item.products, listFavoriteProducts)
                //     }
                // })
            }
            res.success(todayShops)
        }
        else {
            res.error(responseCode.BAD_REQUEST.name, req.__("controller.home.invalid_rank_type"), responseCode.BAD_REQUEST.code)
        }
    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}

export const rankingSeller = async (req: any, res: any) => {
    try {
        const {page, limit} = req.query;
        let todayShops = await shopRepo.getShopRanking(page, limit);

        todayShops = cloneObj(todayShops)
        todayShops = await handleTopShopV2(todayShops, req.user, true)
        todayShops = todayShops.map(item => item.shop)
        res.success(todayShops)
    } catch (error) {
        console.log(error);
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code);
    }
}

export const search = async (req: any, res: any, next: any) => {
    try {
        const {
            limit = 10,
            page = 1,
            keyword,
            category,
            location,
            ship_provider,
            price_min,
            price_max,
            status,
            pay_option,
            rate,
            services,
            orderBy = ProductSort.NEWEST,
        } = req.query;

        const request: GlobalSearchProductRequest = {
            keyword,
            category,
            location,
            ship_provider,
            price_min: parseInt(price_min),
            price_max: parseInt(price_max),
            status,
            pay_option,
            rate,
            services,
            orderBy
        };
        const data = await HomeService._.searchExplore(request, limit, page, req.user);

        data.data = data.data.map(product => {
            // product.sold = product.order_items && product.order_items.length ? product.order_items[0].sold : 0
            return product
        })
        return res.success(data);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const searchV2 = async (req: any, res: any, next: any) => {
    try {
        const language =
        [VN_LANG, EN_LANG, KO_LANG].indexOf(req.headers['accept-language']) > -1
        ? req.headers['accept-language']
        : VN_LANG;
        const result = await homeService.homeSearchV2(req, language);
        return res.success(result);
    } catch (error) {
        console.log(error)
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const searchSeller = async (req: any, res: any, next: any) => {
    try {
        let handleParam = filterShop(req);
        const result = await shopRepo.getListShop(
            handleParam.paginate,
            handleParam.params
        );

        result.data = result.data.map(shopRaw => {
            const shop = cloneObj(shopRaw)
            shop.user.avatar = addLink(`${keys.host_community}/`, shop.user.gallery_image.url)
            shop.user = new UserDTO(shop.user).toSimpleUserInfo();
            return shop
        })

        res.success(result);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const suggestKeywordWhenSearching = async (req: any, res: any, next: any) => {
    try {
        const { keyword, shop_id } = req.query
        const keywords = await searchKeywordRepo.getSuggestKeyword(keyword, shop_id)
        res.success(keywords)
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

/**
 * Top search keyword
 * @param req 
 * @param res 
 * @param next 
 * @return Normal array data
 * @rule We get top category's name of products
 */
export const popularSearch = async (req: any, res: any, next: any) => {
    const language = req.headers["accept-language"] || VN_LANG;
    try {
        // const keywords = await searchKeywordRepo.top(5)
        // const truncated = keywords
        //     ?.map(item => item?.keyword)
        //     ?.filter(item => item != "");
        // return res.success(truncated);

        const responseCache = await productCache.get("popular-search");

        if (responseCache) {
            res.success(responseCache);
        } else {

            let keywords:any = await searchKeywordRepo.top(100)
            keywords = cloneObj(keywords)
            const fakeImages = [
                "http://103.92.29.93//uploads/images/products/1619510905308_image_972618.jpeg",
                "http://103.92.29.93//uploads/images/products/1619545270638_image_28115.jpeg",
                "http://103.92.29.93//uploads/images/products/1620267389388_image_16869.jpeg",
                "http://103.92.29.93//uploads/images/products/1616424995435_image_966445.jpeg",
                "http://103.92.29.93//uploads/1620149011585_image_459271.png"
            ]
            let promises = []
            promises = keywords.map(async (item, index) => {
                return new Promise(async (resolve, reject) => {
                    //item.avatar = fakeImages[index]
                    const products = await productRepo.countProductByKeyword(item.keyword)
                    item.total_product = products.length
                    // Fake image for top search
                    item.avatar = products.length && products[0].images.length ? products[0].images[0] : fakeImages[0]
                    resolve(item)
                })
            })
    
            keywords = await Promise.all(promises)
            keywords = keywords.filter(item => item?.total_product > 0)
    
            //system category
            let systemCategories: any = await categoryRepo.getCategorySystemOrderBy();
            systemCategories = systemCategories.map(category => {
                const categoryLocalized = CATEGORIES_LOCALIZED.get(
                    category.name.trim().toLowerCase()
                );

                if (categoryLocalized) {
                    category.name_localized = {
                        vi: category.name,
                        en: categoryLocalized.en,
                        ko: categoryLocalized.ko,
                    };
                    if (categoryLocalized[language]) {
                        category.name = categoryLocalized[language];
                    }
                } else {
                    category.name_localized = {
                        vi: category.name,
                        en: category.name,
                        ko: category.name,
                    };
                }

                return new CategoryDTO(category).toSimpleCategoryItem();
            });
    
            const results = {
                top_keyword: keywords,
                system_category: systemCategories || []
            }
            productCache.save('popular-search', results);
            productCache.expire('popular-search', TTL.oneHour);
            res.success(results);
        }
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getSuggestProduct = async (req: any, res: any, next: any) => {
    try {
        const page = req.query.page || 1;
        const limit = req.query.limit || 20;

        let productSuggest: any = await productRepo.findProductHome({ orderBy: 'best_sell' }, page, limit)

        const promise = productSuggest.data.map((product) => {
            return new Promise(async (resolve, reject) => {
                let item : any= new ProductDTO(product)
                item = item.getProductComplete(req.user);
                const promotion = await promotionRepo.getPromotionByProduct(item._id)
                if (promotion) {
                    item.discount_percent = promotion.discount
                }
                else {
                    item.discount_percent = 0
                }
                resolve(item)
            })
        })
        let results:any =  await Promise.all(promise).then(data => data).catch(() => {
            return []
        })

        results = _.shuffle(results) || []
        res.success(results);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    };
}

export const getSuggestProductV2 = async (req: any, res: any, next: any) => {
    try {
        let { page = 1, limit = 20, random_number = 0 } = req.query
        if(process.env.NODE_ENV !== 'production' && limit > 10) limit = 10
        if(random_number < 0 || random_number > 19) random_number = Math.floor(Math.random() * 20)
        let realPage = SHUFFLE_SUGGEST_PRODUCT_PAGE?.[random_number]?.[page - 1]
        if(!realPage) {
            realPage = 1e9
        }

        let productSuggest: any = await productRepo.findProductHome({ orderBy: 'populate' }, realPage, limit)
        productSuggest = cloneObj(productSuggest)

        const promise = productSuggest.data.map((product) => {
            return new Promise(async (resolve, reject) => {
                let item : any= new ProductDTO(product)
                item = item.getProductComplete(req.user);
                const promotion = await promotionRepo.getPromotionByProduct(item._id)
                if (promotion) {
                    item.discount_percent = promotion.discount
                }
                else {
                    item.discount_percent = 0
                }
                resolve(item)
            })
        })
        let results:any =  await Promise.all(promise).then(data => data).catch(() => {
            return []
        })
        productSuggest.data = results
        productSuggest.paginate.page = +page
        productSuggest.paginate.random_number = +random_number
        res.success(productSuggest);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    };
}

export const getSuggestProductV3 = async (req: any, res: any, next: any) => {
    try {
        setCacheHeaders(res, 30);
        let { page = 1, limit = 20, random_number = 0 } = req.query
        const user = req.user ? cloneObj(req.user) : null

        if(process.env.NODE_ENV !== 'production' && limit > 10) limit = 10
        if(random_number < 0 || random_number > 19) random_number = Math.floor(Math.random() * 20)
        let realPage = SHUFFLE_SUGGEST_PRODUCT_PAGE?.[random_number]?.[page - 1]

        if(!realPage) {
            realPage = 1e9
        }

        const bodyShapeType = user ? user?.bodyShapeType?.toString() : null

        const promises = [
            timeOutOfPromise(3000, {
                result: {data: [], paginate: null}
            }),
            (async () => {
                const data = await productCache.get("suggestProducts")
                return {
                    result: { data, paginate: {} }
                }
            })()
        ]
        let productSuggest:any = await Promise.race(promises).then((data:any) => {
            return data.result
        })

        if(!productSuggest.data || !productSuggest.data.length) {
            productSuggest = await productRepo.getSuggestProductList(realPage, limit, bodyShapeType)
            productSuggest.paginate.page = +page
            productSuggest.paginate.random_number = +random_number
        }
        else {
            const totalPage = productSuggest.data.length
            productSuggest.data = cloneObj(productSuggest.data).slice((realPage - 1) * limit, (realPage - 1) * limit + +limit)
            productSuggest.paginate = {
                "limit": +limit,
                "total_page": Math.ceil(totalPage / +limit),
                "page": +page,
                "total_record": totalPage,
                "random_number": +random_number
            }
        }
        const promise = productSuggest.data.map((product) => {
            return new Promise(async (resolve, reject) => {
                try {
                    let item : any= new ProductDTO(product)
                    item = item.getProductComplete(req.user, true);
                    const promotion = await promotionRepo.getPromotionByProduct(item._id)
                    if (promotion) {
                        item.discount_percent = promotion.discount
                    }
                    else {
                        item.discount_percent = 0
                    }
                    resolve(item)
                } catch (error) {
                    reject(error)
                }
            })
        })
        let results:any =  await Promise.all(promise).then(data => {
            return data
        }).catch((error) => {
            console.log("Error: ", error.message);
            return []
        })
        productSuggest.data = results
        res.success(productSuggest);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    };
}

export const getNewestProduct = async (req: any, res: any, next: any) => {
    try {
        let { page, limit } = req.query
        if(!page) page = 1
        if(!limit) limit = 10
        let newProduct:any = await productRepo.findProductHome({orderBy: 'newest'}, page, limit)
        const promise = newProduct.data.map((product) => {
            return new Promise(async (resolve, reject) => {
                let item : any= new ProductDTO(product);
                item = item.getProductComplete(req.user);
                const promotion = await promotionRepo.getPromotionByProduct(item._id)
                if (promotion) {
                    item.discount_percent = promotion.discount
                }
                else {
                    item.discount_percent = 0
                }
                resolve(item)
            })
        })
        newProduct.data = await Promise.all(promise)
        res.success(newProduct)
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getNewestProductV2 = async (req: any, res: any, next: any) => {
    try {
        let { page = 1, limit = 20 } = req.query;
        let result: any;
        result = await productService.getNewestProduct(page, limit);
        result.data = _.shuffle(result?.data);
        res.success(result);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    };
}

export const getListProductCategorySystem = async (req: any, res: any, next: any) => {
    try {
        const page = req.query.page || 1;
        const limit = req.query.limit || 10;
        const categoryId = req.params.id;
        let products = await productRepo.getListProductOfCategorySystem(categoryId, page, limit);
        products = cloneObj(products);
        let productItems = cloneObj(products.data);

        productItems = productItems.map(product => {
            product = ProductDTO.newInstance(product)
            product.setUser(req.user);
            let simpleProduct = product.toJSON(['_id', 'name', 'before_sale_price', 'sale_price', 'quantity', 'sold', 'shop_id', 'images', 'is_sold_out']);
            let productOption = product.toJSON(['variants']);
            simpleProduct.price_min_max = getMinMax(productOption.variants);
            simpleProduct.is_bookmarked = !!product.checkBookmarkedProduct();
            return simpleProduct;
        })
        products.data = productItems;
        res.success(products);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    };
}

export const getSearchDetail = async (req: any, res: any, next: any) => {
    try {

        let keywords = await searchKeywordRepo.top(10)
        keywords = cloneObj(keywords)
        const fakeImages = [
            "http://103.92.29.93//uploads/images/products/1619510905308_image_972618.jpeg",
            "http://103.92.29.93//uploads/images/products/1619545270638_image_28115.jpeg",
            "http://103.92.29.93//uploads/images/products/1620267389388_image_16869.jpeg",
            "http://103.92.29.93//uploads/images/products/1616424995435_image_966445.jpeg",
            "http://103.92.29.93//uploads/1620149011585_image_459271.png"
        ]
        let promises = []
        promises = keywords.map(async (item, index) => {
            return new Promise(async (resolve, reject) => {
                //item.avatar = fakeImages[index]
                const products = await productRepo.countProductByKeyword(item.keyword)
                item.total_product = products.length
                // Fake image for top search
                item.avatar = products.length && products[0].images.length ? products[0].images[0] : fakeImages[0]
                resolve(item)
            })
        })

        keywords = await Promise.all(promises)

        //system category
        let systemCategories: any = await categoryRepo.find({
            shop_id: null,
            avatar: { $ne: null }
        });
        systemCategories = systemCategories.map(category => new CategoryDTO(category).toSimpleJSON());
        const results = {
            top_keyword: keywords,
            system_category: systemCategories || []
        }
        res.success(results);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const bannerSystemDetail = async (req: any, res: any, next: any) => {
    try {
        const { id } = req.params
        const { page, limit } = req.query
        const language = req.headers["accept-language"] || VN_LANG
        let banner: any = await bannerRepo.findOne({ _id: ObjectId(id) })

        if (banner) {
            banner = cloneObj(banner)
            let result: any
            if (banner.classify === "product") {
                result = await productRepo.getProductByBanner(banner.products, page, limit)
                result = cloneObj(result)

                result.data = result.data.map(product => {

                    product = ProductDTO.newInstance(product)
                    product.setUser(req.user);
                    let simpleProduct = product.toJSON(['_id', 'name', 'before_sale_price', 'sale_price', 'quantity', 'shop_id', 'images', 'variants', 'option_types', 'order_items']);
                    let productOption = product.toJSON(['variants']);
                    simpleProduct.price_min_max = getMinMax(productOption.variants);
                    // simpleProduct.sold = simpleProduct.order_items && simpleProduct.order_items.length ? simpleProduct.order_items[0].sold : 0
                    delete simpleProduct.order_items
                    return simpleProduct;
                })
                result = {
                    products: result.data,
                    vouchers: banner.vouchers,
                    _id: banner._id,
                    name: banner.name,
                    image: banner.image,
                    shorten_link: banner.shorten_link,
                    classify: banner.classify,
                    description: banner.description
                }


            }
            else if (banner.classify === "top_shop") {
                // Shop ranking
                let todayShops = await shopRepo.getShopRanking(1, 5)
                todayShops = await handleTopShop(todayShops, req.user, true)
                banner.top_shop = todayShops
                result = banner
            }
            else if (banner.classify === "shop_list") {
                let shopIds = banner?.advance_actions?.shops || []
                shopIds = shopIds.map(id => ObjectId(id))
                let shops = await shopRepo.findShopsWithUserAndFeedback(shopIds)
                shops = cloneObj(shops)
                banner.shop_list = await handleShopAndProduct(shops, req.user)
                result = banner
            }
            else {
                result = await voucherRepo.getVoucherByBanner(banner.vouchers, page, limit)
                let imageDetail = null
                if(language === VN_LANG) {
                    imageDetail = banner.images?.find(item => item.lang === VN_LANG)?.detail || null
                }
                else if(language === KO_LANG) {
                    imageDetail = banner.images?.find(item => item.lang === KO_LANG)?.detail || null
                }
                else {
                    imageDetail = banner.images?.find(item => item.lang === 'en')?.detail || null
                }
                result = {
                    vouchers: result,
                    products: banner.products,
                    _id: banner._id,
                    name: banner.name,
                    image: imageDetail,
                    images: banner.images || [],
                    shorten_link: banner.shorten_link,
                    classify: banner.classify,
                    description: banner.description
                }
                
            }

            // add fake data
            switch (result._id.toString()) {
                case "6082a46254c1d43781c1b0bb":
                    result.mobileVNURL = "https://commerce-test.bidu.com.vn//uploads/1628083094488_image_183104.png"
                    result.mobileKOURL = "https://commerce-test.bidu.com.vn//uploads/1628083226826_image_838828.png"
                    result.detail_image_VN = "https://commerce-test.bidu.com.vn//uploads/1628083094488_image_183104.png"
                    result.detail_image_KO = "https://commerce-test.bidu.com.vn//uploads/1628083226826_image_838828.png"
                    break;
                case "6082a49002007137ab38e046":
                    result.mobileVNURL = "https://commerce-test.bidu.com.vn//uploads/1628083331736_image_708624.png"
                    result.mobileKOURL = "https://commerce-test.bidu.com.vn//uploads/1628083300949_image_735042.png"
                    result.detail_image_VN = "https://commerce-test.bidu.com.vn//uploads/1628083331736_image_708624.png"
                    result.detail_image_KO = "https://commerce-test.bidu.com.vn//uploads/1628083300949_image_735042.png"
                    break;
                case "6082a56cbf49873838345e10":
                    result.mobileVNURL = "https://commerce-test.bidu.com.vn//uploads/1628083331736_image_708624.png"
                    result.mobileKOURL = "https://commerce-test.bidu.com.vn//uploads/1628083300949_image_735042.png"
                    result.detail_image_VN = "https://commerce-test.bidu.com.vn//uploads/1628083331736_image_708624.png"
                    result.detail_image_KO = "https://commerce-test.bidu.com.vn//uploads/1628083300949_image_735042.png"
                    break;
                case "6093962c7635bc0ae21d9210":
                    result.mobileVNURL = "https://commerce-test.bidu.com.vn//uploads/1628083392202_image_599725.png"
                    result.mobileKOURL = "https://commerce-test.bidu.com.vn//uploads/1628083478666_image_539011.png"
                    result.detail_image_VN = "https://commerce-test.bidu.com.vn//uploads/1628083392202_image_599725.png"
                    result.detail_image_KO = "https://commerce-test.bidu.com.vn//uploads/1628083478666_image_539011.png"
                    break;
                case "60ae41534cec7d001258bc5b":
                    result.mobileVNURL = "https://commerce-test.bidu.com.vn//uploads/1628083420139_image_346294.png"
                    result.mobileKOURL = "https://commerce-test.bidu.com.vn//uploads/1628083503047_image_878400.png"
                    result.detail_image_VN = "https://commerce-test.bidu.com.vn//uploads/1628083420139_image_346294.png"
                    result.detail_image_KO = "https://commerce-test.bidu.com.vn//uploads/1628083503047_image_878400.png"
                    break;
                case "60acf7e8bdd86837a3b30f05":
                    if (language === VN_LANG || language === EN_LANG) {
                        result.detail_image = "https://commerce-test.bidu.com.vn//uploads/1624006537270_image_666463.png"
                    }
                    else {
                        result.image = "https://commerce-test.bidu.com.vn//uploads/1623976676736_image_647128.jpg"
                        result.detail_image = "https://commerce-test.bidu.com.vn//uploads/1624006775891_image_5914.png"
                    }
                    break;
                case "60c9cd44cab65d721f9af044":
                case "60c9cb76cab65d721f9af043":
                    result.detail_image = result.image
                    break;
                default:
                    break;
            }

            res.success(result)
        }
        else {
            res.error(responseCode.NOT_FOUND.name, "Không tìm thấy banner", responseCode.NOT_FOUND.code);
        }
        // let result = await bannerRepo.getDetailSystemBanner(id, page, limit)
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
    }
}


export const bannerSystemDetailV2 = async (req: any, res: any, next: any) => {
    try {
        const { id } = req.params
        const { page, limit } = req.query
        const language = req.headers["accept-language"] || VN_LANG
        let banner: any = await bannerRepo.findOne({ _id: ObjectId(id) })

        if (banner) {
            banner = cloneObj(banner)

            let imageDetail = null
            if(language === VN_LANG) {
                imageDetail = banner.images?.find(item => item.lang === VN_LANG)?.detail || null
            }
            else if(language === KO_LANG) {
                imageDetail = banner.images?.find(item => item.lang === KO_LANG)?.detail || null
            }
            else {
                imageDetail = banner.images?.find(item => item.lang === 'en')?.detail || null
            }
            banner.image = imageDetail

            let result: any
            if (banner.classify === "product") {
                result = await productRepo.getProductByBanner(banner.products, page, limit)
                result = cloneObj(result)

                result.data = result.data.map(product => {

                    product = ProductDTO.newInstance(product)
                    product.setUser(req.user);
                    let simpleProduct = product.toJSON(['_id', 'name', 'before_sale_price', 'sale_price', 'quantity', 'shop_id', 'images', 'variants', 'option_types', 'order_items']);
                    let productOption = product.toJSON(['variants']);
                    simpleProduct.price_min_max = getMinMax(productOption.variants);
                    // simpleProduct.sold = simpleProduct.order_items && simpleProduct.order_items.length ? simpleProduct.order_items[0].sold : 0
                    delete simpleProduct.order_items
                    return simpleProduct;
                })

                result = {
                    products: result.data,
                    vouchers: banner.vouchers,
                    _id: banner._id,
                    name: banner.name,
                    image: banner.image,
                    images: banner.images,
                    shorten_link: banner.shorten_link,
                    classify: banner.classify,
                    description: banner.description
                }


            }
            else if (banner.classify === "top_shop") {
                // Shop ranking
                let todayShops = await shopRepo.getShopRanking(1, 5)
                todayShops = await handleTopShop(todayShops, req.user, true)
                banner.top_shop = todayShops
                result = banner
            }
            else if (banner.classify === "shop_list") {
                let shopIds = banner?.advance_actions?.shops || []
                shopIds = shopIds.map(id => ObjectId(id))
                let shops = await shopRepo.findShopsWithUserAndFeedback(shopIds)
                shops = cloneObj(shops)
                banner.shop_list = await handleShopAndProduct(shops, req.user)
                result = banner
            }
            else {
                // banner voucher v2
                result = await voucherRepo.getVoucherByBannerV2(banner.vouchers)
                
                let imageDetail = null
                if(language === VN_LANG) {
                    imageDetail = banner.images?.find(item => item.lang === VN_LANG)?.detail || null
                }
                else if(language === KO_LANG) {
                    imageDetail = banner.images?.find(item => item.lang === KO_LANG)?.detail || null
                }
                else {
                    imageDetail = banner.images?.find(item => item.lang === 'en')?.detail || null
                }
                result = {
                    vouchers: !page || page <= 1 ? result : [],
                    products: banner.products,
                    _id: banner._id,
                    name: banner.name,
                    image: banner.image,
                    images: banner.images || [],
                    shorten_link: banner.shorten_link,
                    classify: banner.classify,
                    description: banner.description
                }
                
            }

            res.success(result)
        }
        else {
            res.error(responseCode.NOT_FOUND.name, "Không tìm thấy banner", responseCode.NOT_FOUND.code);
        }
        // let result = await bannerRepo.getDetailSystemBanner(id, page, limit)
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
    }
}

export const getPromotions = async (req: any, res: any, next: any) => {
    try {
        const { type = 'system', page = 1, limit = 10 } = req.query;
        let promotions: any;
        switch (type.trim()) {
            case 'system':
                let listVouchers = await voucherRepo.getVoucherSystemCombineBanner();
                listVouchers = cloneObj(listVouchers);

                let voucher_have_banner = listVouchers
                    .filter((voucher) => voucher?.banner)
                    .map((voucher) => new VoucherDTO(voucher).toSimpleJSON());
                voucher_have_banner = _.chain(voucher_have_banner)
                    .groupBy('banner')
                    .map((value) => {
                        let result = value[0].banner;
                        result.vouchers = value.map((item) => {
                            delete item.banner;
                            return item;
                        });
                        return result;
                    })
                    .value();
                    
                const voucher_normal = listVouchers
                    .filter((voucher) => !voucher?.banner)
                    .map((voucher) => new VoucherDTO(voucher).toSimpleJSON());
                
                promotions = { voucher_have_banner, voucher_normal };
                break;
            case 'shop':
                let {data, paginate} = await voucherRepo.getVoucherShop(page, limit);
                data = cloneObj(data);
                if (isMappable(data)) {
                    let dataHandle = [];

                    if (data.length === 1 && page === 1) {
                        dataHandle = data[0]?.vouchers || []
                    } else {
                        dataHandle = await Promise.all(
                            data?.map(async(shop) => {
                                if (isMappable(shop?.vouchers) && shop?.vouchers?.length === 1) {
                                    return shop?.vouchers[0];
                                } else {
                                    let biggestVoucher = null;
                                    const biggestPriceProduct = await productService.getBiggestPriceProductByShop(shop._id);
                                    shop?.vouchers?.forEach((voucher)=> {
                                        switch (true) {
                                            case !biggestVoucher:
                                                biggestVoucher = voucher;
                                                break;

                                            case biggestVoucher?.type === voucher?.type:
                                                biggestVoucher?.value < voucher?.value ? biggestVoucher = voucher : '';
                                                break;

                                            case biggestVoucher?.type === 'percent' && voucher?.type === 'price':
                                                (biggestVoucher?.value / 100) *
                                                    biggestPriceProduct?.sort_price <
                                                voucher?.value
                                                    ? (biggestVoucher = voucher)
                                                    : '';
                                                break;

                                            case biggestVoucher?.type === 'price' && voucher?.type === 'percent':
                                                biggestVoucher?.value <
                                                (voucher?.value / 100) *
                                                    biggestPriceProduct?.sort_price
                                                    ? (biggestVoucher = voucher)
                                                    : '';
                                                break;
                                    
                                            default:
                                                break;
                                        }
                                    })
                                    return biggestVoucher;
                                }
                            })
                        ) 
                    }

                    dataHandle = dataHandle.map((voucher) => {
                        voucher = new VoucherDTO(voucher).toSimpleJSON();
                        voucher.shop.user.avatar = addLink(`${keys.host_community}/`, voucher?.shop?.user?.gallery_image?.url)
                        return voucher;
                    })

                    promotions = {
                        data: dataHandle,
                        paginate
                    };

                    break;
                } else {    
                    promotions = {
                        data: [],
                        paginate
                    };

                    break;
                }

            default:
                break;
        }
        
        res.success(promotions);
    } catch (error: any) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
    }
}

export const getProductInternational = async (req: any, res: any, next: any) => {
    try {
        const { page, limit } = req.query
        const internationalCategory = await categoryRepo.findOne({name: "BIDU Air", is_active: true})
        
        let products: any = await ProductService._.getListProductInternational(page, limit, internationalCategory)
        products = cloneObj(products);
        res.success(products);
    } catch (error: any) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
    }
}

export const getProductInternationalV2 = async (req: any, res: any, next: any) => {
    try {
        const { page, limit, subcategory } = req.query;
        let internationalCategory: any = await categoryRepo.findOne({
            name: 'BIDU Air',
            is_active: true,
        });
        internationalCategory = cloneObj(internationalCategory);

        let listSubcategories: any = [];
        if (
            !subcategory ||
            subcategory === internationalCategory?._id.toString()
        ) {
            const listIdSubcategories =
                await ProductService._.getListSubcategoryInternational(
                    internationalCategory
                );
            
            listSubcategories = await categoryRepo.getCategoriesInfo(listIdSubcategories);
            listSubcategories = cloneObj(listSubcategories);

            listSubcategories = listSubcategories.map((category) => {
                let categoryLocalized = CATEGORIES_LOCALIZED.get(category.name.toLowerCase())
                category.name_localized = {
                    vi: category.name,
                    ...categoryLocalized,
                }
                return category;
            })
        }
        
        let products: any = await ProductService._.getListProductInternational(
            page,
            limit,
            internationalCategory,
            subcategory
        );
        products = cloneObj(products);
        res.success({ categories: listSubcategories, products });
    } catch (error: any) {
        res.error(
            responseCode.SERVER.name,
            error.message,
            responseCode.SERVER.code
        );
    }
};

export const searchUser = async (req: any, res: any, next: any) => {
    try {
        let handleParam = filterShop(req);
        const result = await userRepo.getListUser(
            handleParam.paginate,
            handleParam.params
        );

        result.data = result.data.map(userRaw => {
            let user = cloneObj(userRaw)
            user.avatar = addLink(`${keys.host_community}/`, user.gallery_image.url)
            user = new UserDTO(user).toSimpleUserInfo();
            return {
                user: user
            }
        })

        res.success(result);
    } catch (error) {
        console.log(error)
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}
