import { executeError } from "../../../../base/appError";
import { AddressRepo } from "../../../../repositories/AddressRepo";
import { DataCityRepo } from "../../../../repositories/DataCityRepo";
import { cloneObj } from "../../../../utils/helper";
import AddressService from "../../../serviceHandles/address";

const addressRepository = AddressRepo.getInstance();
const dataCityRepository = DataCityRepo.getInstance();

export const getAddressByUser = async (req: any, res: any, next: any) => {
    const userId = req.user._id;
    try {
        const result = await addressRepository.getAddressByUser(userId);
        res.success(result);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const createAddress = async (req: any, res: any, next: any) => {
    const userId = req.user._id;
    try {
        const { name, state, district, ward, street, phone, is_default, is_pick_address_default, is_return_address_default, expected_delivery, address_type } = req.body;
        const request = {
            name,
            state,
            district,
            ward,
            street,
            phone: phone.trim(),
            is_default,
            accessible_id: userId,
            is_pick_address_default,
            is_return_address_default,
            expected_delivery,
            address_type
        };
        const address = await addressRepository.createAddress(request);
        res.success(address);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const updateAddress = async (req: any, res: any, next: any) => {
    const id = req.params.id;
    try {
        const { name, state, district, ward, street, phone, is_default = false, is_pick_address_default = false, is_return_address_default = false, expected_delivery, address_type } = req.body;
        const request = {
            name,
            state,
            district,
            ward,
            street,
            phone: phone.trim(),
            is_default,
            is_pick_address_default,
            is_return_address_default,
            expected_delivery,
            address_type
        };
        const address = await addressRepository.updateAddress(id, request);
        res.success(address);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getAddressData = async (req: any, res: any, next: any) => {
    const stateId = req.query.state_id;
    const districtId = req.query.district_id;
    try {
        const address = await dataCityRepository.getAddressData(stateId, districtId);
        res.success(address);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const deleteAddress = async (req: any, res: any, next: any) => {
    const addressId = req.params.id;
    try {
        await addressRepository.deleteAddress(addressId);
        res.success(true);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}


export const checkAddressUser = async (req: any, res: any, next: any) => {
    const userId = req.user._id;
    try {
        const result = await addressRepository.checkAddressUer(userId);
        res.success(result);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getAddressValidForSellerByAdmin = async (req: any, res: any, next: any) => {
    try {
        const userId = req.user._id;

        const result = await AddressService._.getAddressValidForSeller(userId, req.orderShop, req.orderShop.shop_id);
        res.success(result);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getAddressValidForSeller = async (req: any, res: any, next: any) => {
    try {
        const userId = req.user._id;

        const result = await AddressService._.getAddressValidForSeller(userId, req.orderShop);
        res.success(result);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getPickAddressDefaultOfShop = async (req: any, res: any, next: any) => {
    try {
        const userId = req.user._id;

        const result = await AddressService._.getPickAddressUser(userId);
        res.success(result);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}