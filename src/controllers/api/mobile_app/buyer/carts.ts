
import moment from "moment";
import { executeError } from "../../../../base/appError";
import responseCode from "../../../../base/responseCode";
import { KOREA } from "../../../../base/variable";
import keys from "../../../../config/env/keys";
import { ProductDTO } from "../../../../DTO/Product";
import { UserDTO } from "../../../../DTO/UserDTO";
import { AddressRepo } from "../../../../repositories/AddressRepo";
import { CartItemRepo } from "../../../../repositories/CartItemRepo";
import { PaymentMethodRepo } from "../../../../repositories/PaymentMethodRepo";
import { ProductRepo } from "../../../../repositories/ProductRepo";
import { ShippingMethodRepo } from "../../../../repositories/ShippingMethodRepo";
import { ShopRepo } from "../../../../repositories/ShopRepo";
import { VoucherRepository } from "../../../../repositories/VoucherRepository";
import { cloneObj, ObjectId, isMappable } from "../../../../utils/helper";
import { addLink } from "../../../../utils/stringUtil";
// import { getCheckout } from "../../../serviceHandles/cart/carts";
import { CartService } from "../../../serviceHandles/cart/cartService";
import { getMinMax } from "../../../serviceHandles/product";

const cartItemRepo = CartItemRepo.getInstance();
const productRepo = ProductRepo.getInstance();
const voucherRepo = VoucherRepository.getInstance();
const addressRepo = AddressRepo.getInstance();
const shippingMethodRepo = ShippingMethodRepo.getInstance();
const shopRepo = ShopRepo.getInstance();
const paymentMethodRepo = PaymentMethodRepo.getInstance()

export const getOrderedProducts = async (req: any, res: any, next: any) => {
    try {
        const products = await productRepo.getOrderedProducts(req.user._id)
        res.success(products)
    } catch (error) {
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}

export const viewCart = async (req: any, res: any, next: any) => {
    try {
        let result = await cartItemRepo.findAndGroupByShop(req.user._id)
        result = cloneObj(result)
        // valid
        result.valid = result.valid.filter(item => item.shop)
        result.valid = result.valid.map(item => {
            item.shop.user.avatar = addLink(`${keys.host_community}/`, item.shop.user.gallery_image.url)
            item.shop.user = new UserDTO(item.shop.user).toSimpleJSON();
            item.items = item.items.map(element => {
                if (!element.product.feedbacks.length) {
                    element.product.feedbacks = {};
                    element.product.feedbacks._id = null;
                    element.product.feedbacks.averageFeedbackRate = null;
                } else {
                    element.product.feedbacks = element.product.feedbacks[0];
                }
                const product = ProductDTO.newInstance(element.product)
                let productOption: any = product.toJSON(['variants']);
                element.product.price_min_max = getMinMax(productOption.variants);
                return element;
            })
            return item;
        })
        // invalid
        result.inValid = result.inValid.filter(item => item.shop)
        result.inValid = result.inValid.map(item => {
            if (item.product) {
                if (!item.product.feedbacks.length) {
                    item.product.feedbacks = {};
                    item.product.feedbacks._id = null;
                    item.product.feedbacks.averageFeedbackRate = null;
                } else {
                    item.product.feedbacks = item.product.feedbacks[0];
                }
            }
            return item;
        })
        res.success(result)
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
    }
}

export const addProductToCart = async (req: any, res: any, next: any) => {
    if (req.cart_item) {
        const item = req.cart_item.data
        let cartItem: any = {}
        if (req.cart_item.type === 'Variant') {
            cartItem = await cartItemRepo.createOrUpdateItem({
                variant_id: item._id,
                product_id: item.product._id,
                shop_id: item.product.shop_id,
                quantity: item.add_quantity,
                user_id: req.user._id
            })
        }
        else {
            cartItem = await cartItemRepo.createOrUpdateItem({
                product_id: item._id,
                shop_id: item.shop_id,
                quantity: item.add_quantity,
                user_id: req.user._id
            })
        }
        res.success(cartItem)
    }
    else {
        res.error(responseCode.NOT_FOUND.name, req.__("controller.product.product_or_variant_not_found"), responseCode.NOT_FOUND.code)
    }
}

export const changeQuantity = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id
        const quantity = req.body.quantity
        let { cartItem } = req
        if (cartItem) {
            cartItem = await cartItemRepo.update(cartItem._id, {
                quantity: quantity
            })
            res.success(cartItem)
        }
        else {
            res.error(responseCode.NOT_FOUND.name, req.__("controller.product.variant_not_found_for_update"), responseCode.NOT_FOUND.code)
        }
    } catch (error) {
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}

export const deleteProductFromCart = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id
        let { cartItem } = req
        if (cartItem) {
            await cartItemRepo.delete(cartItem._id)
            res.success()
        }
        else {
            res.error(responseCode.NOT_FOUND.name, req.__("controller.product.product_not_found_for_delete"), responseCode.NOT_FOUND.code)
        }
    } catch (error) {
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}

export const changeVariant = async (req: any, res: any, next: any) => {
    try {
        let id = req.params.id
        const { variant_id, quantity } = req.body
        let { cartItem } = req
        let newItem = { ...cartItem }
        if (cartItem) {
            cartItem = cloneObj(cartItem)

            // check if new item is same as another item in cart
            let existsItem: any = await cartItemRepo.findOne({
                variant_id: variant_id,
                product_id: cartItem.product_id,
                user_id: ObjectId(req.user._id),
                _id: { $ne: ObjectId(id) }
            })

            if (existsItem) {
                // Merge cartItem
                existsItem = cloneObj(existsItem)
                newItem = {
                    ...existsItem,
                    quantity: existsItem.quantity + quantity
                }
                id = existsItem._id

                // Delete cartItem
                await cartItemRepo.delete(cartItem._id)

                // Update existItem
                cartItem = await cartItemRepo.update(id, newItem)
            }
            else {
                newItem = {
                    ...cartItem,
                    variant_id: variant_id,
                    quantity: quantity
                }
                // Delete cartItem
                await cartItemRepo.delete(cartItem._id)

                // Create new Item
                cartItem = await cartItemRepo.create(newItem)
            }

            // let result, shop;
            let [result, shop] = await Promise.all([
                (async () => {
                    const data = await cartItemRepo.getCartItemsByShopAndUser(cartItem.shop_id, req.user._id)
                    return data
                })(),
                (async () => {
                    const data = await shopRepo.findById(cartItem.shop_id)
                    return data
                })()
            ])
            
            result = result.filter(item => {
                const product = item.products && item.products[0] ? item.products[0] : {}
                item.product = product
                delete item.products
                if (!item.product.feedbacks.length) {
                    item.product.feedbacks = {};
                    item.product.feedbacks._id = null;
                    item.product.feedbacks.averageFeedbackRate = null;
                } else {
                    item.product.feedbacks = item.product.feedbacks[0];
                }
                return (product && product.allow_to_sell && product.is_approved === "approved" && product.quantity > 0 && !product.deleted_at)
            })

            const response = {
                shop_id: cartItem.shop_id,
                items: result,
                shop: shop
            }
            res.success(response)
        }
        else {
            res.error(responseCode.NOT_FOUND.name, 'Không tìm thấy mặt hàng để cập nhật', responseCode.NOT_FOUND.code)
        }
    } catch (error) {
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}

export const getVoucherByShopWithCondition = async (req: any, res: any, next: any) => {
    try {
        const { shop_id: shopId, total_value: totalPrice, products, code } = req.body;
        let vouchers = await voucherRepo.getVoucherByShopWithCondition(shopId, products);
        vouchers = cloneObj(vouchers);

        vouchers = vouchers.filter(voucher => {
            if(voucher.is_public) {
                voucher.available = voucher.min_order_value <= totalPrice ? true : false;
                return voucher;
            }
        }) || [] ;

        if (code && code.trim()) {
            vouchers = vouchers.filter(voucher => code && code.trim() && voucher?.code === code.trim());
        }
        else {
            vouchers = vouchers.filter(voucher => voucher.display_mode === 'all')
        }

        res.success(vouchers);

    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getVoucherByShopWithConditionV2 = async (req: any, res: any, next: any) => {
    try {
        const { list_shops: listOrderByShops, code } = req.body;
        let listVoucherByShop = [];

        await Promise.all(
            listOrderByShops?.map(async( item ) => {
                const { shop_id: shopId, total_value: totalPrice, products, name, avatar } = item;
                let vouchers = await voucherRepo.getVoucherByShopWithCondition(shopId, products);
                vouchers = cloneObj(vouchers);

                vouchers = vouchers.filter(voucher => {
                    if(voucher?.is_public) {
                        if (totalPrice) {
                            voucher.available = voucher.min_order_value <= totalPrice ? true : false;
                        }
                        return voucher;
                    }
                }) || [] ;

                if (code && code.trim()) {
                    vouchers = vouchers.filter(voucher => code && code.trim() && voucher?.code === code.trim());
                }
                else {
                    vouchers = vouchers.filter(voucher => voucher.display_mode === 'all')
                }

                if (isMappable(vouchers)) {
                    listVoucherByShop.push({shop_id: shopId, name, avatar, vouchers: vouchers})
                }
           })
        )

        res.success(listVoucherByShop);

    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getPreviewShopWithCondition = async (req: any, res: any, next: any) => {
    try {
        const { list_shops: listShops, address_id } = req.body;
        let listPreviewShop = [];

        const address = address_id ? await addressRepo.findWithUser(address_id, req.user._id) : await addressRepo.getDefaultAddress(req.user._id)

        await Promise.all(
            listShops?.map(async (item) => {
                const { shop_id: shopId, products, name, items } = item;
                let vouchers = await voucherRepo.getVoucherByShopForPreview(
                    shopId,
                    products
                );

                vouchers = cloneObj(vouchers);
                vouchers = vouchers.filter(voucher => voucher?.display_mode === 'all')
                let previewVoucher: any = {};
                let voucher: any;

                switch (true) {
                    case vouchers.length === 1:
                        voucher = vouchers[0]
                        previewVoucher.type = voucher?.type
                        previewVoucher.value = voucher?.value
                        previewVoucher.type_preview = "discount"
                        break;
                    
                    case vouchers.length > 1:
                        voucher = vouchers[0];
                        const voucherWithTheLargestPercentage = vouchers?.find(item => item?.type === "percent")
                        if (voucher?.type === "price"  && voucherWithTheLargestPercentage) {
                            let shop = await shopRepo.findById(shopId);
                            shop = cloneObj(shop);

                            if (voucher?.value && + voucher?.value < (+shop?.biggest_price/100 * voucherWithTheLargestPercentage?.value)) {
                                voucher = voucherWithTheLargestPercentage;
                            }
                        }
                        previewVoucher.type = voucher?.type
                        previewVoucher.value = voucher?.value
                        previewVoucher.type_preview = "up_to_discount"
                        break;
                
                    default:
                        break;
                }

                let shippingMethods = [];

                if (items) {
                    let itemCart = await cartItemRepo.getItems(items);
                    const shop: any = itemCart[0]?.shop;

                    if (shop) {
                        itemCart = itemCart.map((item) => {
                            delete item?.shop;
                            return item;
                        });
    
                        const totalWeight = itemCart
                            .map((item) => item.products.weight * item.quantity)
                            .reduce((prev, next) => prev + next, 0);
    
                        const totalPrice = itemCart
                            .map((item) => item.products.sale_price)
                            .reduce((prev, next) => prev + next, 0);
                        
                        shippingMethods = await Promise.all(
                            shop?.shipping_methods
                                ?.filter(
                                    (item) =>
                                        (shop?.country === KOREA &&
                                            item?.name_query !== 'VIETTELPOST' &&
                                            item?.is_active) ||
                                        (shop?.country !== KOREA && item?.is_active)
                                )
                                .map(async (item) => {
                                    delete item?.token;
                                    delete item?.pick_address;
    
                                    const avgShippedTime = shop?.avg_shipped_time
                                        ? shop?.avg_shipped_time
                                        : 0;
                                    item.estimated_delivery_date_from = moment()
                                        .add(avgShippedTime, 'days')
                                        .toDate();
                                    item.estimated_delivery_date_to = moment()
                                        .add(avgShippedTime + 2, 'days')
                                        .toDate();
                                    item.shipping_fee =
                                        await new CartService().getShippingFeeByMethod(
                                            req,
                                            shop?._id,
                                            address,
                                            totalWeight,
                                            totalPrice,
                                            item?.name_query
                                        );
                                    return item;
                                })
                        );
                    }
                }

                listPreviewShop.push({
                    shop_id: shopId,
                    name: name,
                    preview_voucher: previewVoucher,
                    shipping_methods: shippingMethods || []
                });
            })
        );

        res.success(listPreviewShop);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getPreviewSystemVoucher = async (req: any, res: any, next: any) => {
    try {

        let vouchers = await voucherRepo.getSystemVoucherForPreview();

        vouchers = cloneObj(vouchers);
        let previewVoucher: any = {};
        let voucher: any;

        switch (true) {
            case vouchers.length === 1:
                voucher = vouchers[0];
                previewVoucher.type = voucher?.type;
                previewVoucher.value = voucher?.value;
                previewVoucher.type_preview = 'discount';
                break;

            case vouchers.length > 1:
                voucher = vouchers[0];
                previewVoucher.type = voucher?.type;
                previewVoucher.value = voucher?.value;
                previewVoucher.type_preview = 'up_to_discount';
                break;

            default:
                break;
        }

        res.success(previewVoucher);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getCheckoutPage = async (req: any, res: any, next: any) => {
    try {
        //const result = await getCheckout(req);
        const result = await(new CartService()).getCheckout(req);
        res.success(result);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getGroupBuyCheckoutPage = async (req: any, res: any, next: any) => {
    try {
        //const result = await getCheckout(req);
        const result = await (new CartService).getGroupBuyCheckout(req);
        res.success(result);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

/**
 * Unused
 * @param req 
 * @param res 
 * @param next 
 */
 export const getSystemVoucher = async (req: any, res: any, next: any) => {
    try {
        const { code, conditionObj } = req;
        let vouchers = await voucherRepo.getSystemVoucherWithCondition(conditionObj)
        vouchers = cloneObj(vouchers)
        const discountVoucher = vouchers.find(item => item.classify === "discount")?.items || []
        const shippingVoucher = vouchers.find(item => item.classify === "free_shipping")?.items || []
        
        // Continue checking max budget condition
        let validDiscountVouchers:any
        let validShippingVouchers:any
        [validDiscountVouchers, validShippingVouchers] = await Promise.all([
            (async () => {
                const data = await voucherRepo.getValidVoucherByMaxBudget(discountVoucher)
                return data
            })(),
            (async () => {
                const data = await voucherRepo.getValidVoucherByMaxBudget(shippingVoucher)
                return data
            })()
        ])

        let isFinded = false
        validDiscountVouchers = validDiscountVouchers.filter(voucher => {
            if(voucher.target === 'newbie') {
                if(code && code.trim() && voucher?.code == code.trim()) {
                    isFinded = true
                }
                return voucher
                // if(conditionObj?.user?.is_newbie) {
                //     if(code && code.trim() && voucher?.code == code.trim()) {
                //         isFinded = true
                //     }
                //     return voucher
                // }
                // return false;
            }
            else {
                if(voucher.is_public) {
                    if(code && code.trim() && voucher?.code == code.trim()) {
                        isFinded = true
                    }
                    return voucher;
                }
                if(code && code.trim() && voucher?.code === code.trim()) {
                    isFinded = true
                    return voucher;
                }
            }
        });

        validShippingVouchers = validShippingVouchers.filter(voucher => {
            if(voucher.target === 'newbie' && conditionObj?.user?.is_newbie) {
                if(code && code.trim() && voucher?.code == code.trim()) {
                    isFinded = true
                }
                return voucher
                // if(conditionObj?.user?.is_newbie) {
                //     if(code && code.trim() && voucher?.code == code.trim()) {
                //         isFinded = true
                //     }
                //     return voucher
                // }
                // return false;
            }
            else {
                if(voucher.is_public) {
                    if(code && code.trim() && voucher?.code == code.trim()) {
                        isFinded = true
                    }
                    return voucher;
                }
                if(code && code.trim() && voucher?.code === code.trim()) {
                    isFinded = true
                    return voucher;
                }
            }
        });

        if(code && !isFinded) {
            validShippingVouchers = []
            validDiscountVouchers = []
        }

        res.success([
            {
                classify: "free_shipping",
                items: validShippingVouchers
            },
            {
                classify: "discount",
                items: validDiscountVouchers
            }
        ])

    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}


export const getSystemVoucherNewFlow = async (req: any, res: any, next: any) => {
    try {
        const currentTime = new Date()
        const { code, conditionObj } = req;
        let vouchers = await voucherRepo.getSystemVoucherWithCondition(conditionObj)
        vouchers = cloneObj(vouchers)
        const discountVoucher = vouchers.find(item => item.classify === "discount")?.items || []
        const shippingVoucher = vouchers.find(item => item.classify === "free_shipping")?.items || []
        let cashbackVoucher = vouchers.filter(item => item.classify === "cash_back" || item.classify === "cash_back_discount")
        cashbackVoucher = cashbackVoucher.reduce((list, ele) => {
            list = list.concat(ele?.items || [])
            return list
        }, [])
        // Continue checking max budget condition
        let validDiscountVouchers:any
        let validShippingVouchers:any
        let validCashbackVouchers:any
        [validDiscountVouchers, validShippingVouchers, validCashbackVouchers] = await Promise.all([
            (async () => {
                const data = await voucherRepo.getValidVoucherByMaxBudget(discountVoucher)
                return data
            })(),
            (async () => {
                const data = await voucherRepo.getValidVoucherByMaxBudget(shippingVoucher)
                return data
            })(),
            (async () => {
                const data = await voucherRepo.getValidVoucherByMaxBudget(cashbackVoucher)
                return data
            })()
        ])

        let isFinded = false
        validDiscountVouchers = validDiscountVouchers.filter(voucher => {
            if(voucher.target === 'newbie') {
                // if(code && code.trim() && voucher?.code == code.trim()) {
                //     isFinded = true
                // }
                // return voucher
                if(conditionObj?.user?.is_newbie) {
                    if(code && code.trim() && voucher?.code == code.trim()) {
                        isFinded = true
                    }
                    return voucher
                }
                return false;
            }
            else {
                if(voucher.is_public) {
                    if(code && code.trim() && voucher?.code == code.trim()) {
                        isFinded = true
                    }
                    return voucher;
                }
                if(code && code.trim() && voucher?.code === code.trim()) {
                    isFinded = true
                    return voucher;
                }
            }
        });

        validShippingVouchers = validShippingVouchers.filter(voucher => {
            if(voucher.target === 'newbie' && conditionObj?.user?.is_newbie) {
                // if(code && code.trim() && voucher?.code == code.trim()) {
                //     isFinded = true
                // }
                // return voucher
                if(conditionObj?.user?.is_newbie) {
                    if(code && code.trim() && voucher?.code == code.trim()) {
                        isFinded = true
                    }
                    return voucher
                }
                return false;
            }
            else {
                if(voucher.is_public) {
                    if(code && code.trim() && voucher?.code == code.trim()) {
                        isFinded = true
                    }
                    return voucher;
                }
                if(code && code.trim() && voucher?.code === code.trim()) {
                    isFinded = true
                    return voucher;
                }
            }
        });

        const promises = validCashbackVouchers.map(voucher => {
            return new Promise(async (resolve, reject) => {
                const newVoucher = {...voucher}
                if(voucher.available && voucher.classify === 'cash_back') {
                    if(voucher?.using_by?.[conditionObj?.user?._id]) {
                        newVoucher.available = false
                    }
                    else {
                        const subVoucherDiscount:any = await voucherRepo.findOne({
                            'start_time': { '$lte': currentTime },
                            'end_time': { '$gte': currentTime },
                            'cash_back_ref.user_id': ObjectId(conditionObj.user?._id),
                            'cash_back_ref.cash_back_id': ObjectId(voucher._id),
                            'available_quantity': { '$gt': 0 }
                        })
                        if(subVoucherDiscount) {
                            newVoucher.available = false
                        }
                    }
                }
                resolve(newVoucher)
            })
        })
        validCashbackVouchers = await Promise.all(promises)
        validCashbackVouchers.sort((x, y) => x.available === y.available ? +new Date(y.created_at) - +new Date(x.created_at) : +y.available - +x.available)

        if(code && !isFinded) {
            validShippingVouchers = []
            validDiscountVouchers = []
            validCashbackVouchers = []
        }

        res.success([
            {
                classify: "free_shipping",
                items: validShippingVouchers
            },
            {
                classify: "discount",
                items: validDiscountVouchers
            },
            {
                classify: "cash_back",
                items: validCashbackVouchers
            }
        ])

    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}

export const getDefaultShippingMethod = async (req: any, res: any, next: any) => {
    try {
        const { shopId } = req.params;
        const result = await shippingMethodRepo.getShippingMethodActiveByShop(shopId);
        res.success(result);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getDefaultPaymentMethod = async (req: any, res: any, next: any) => {
    try {
        const result = await paymentMethodRepo.getDefaultPaymentMethod();
        res.success(result);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}