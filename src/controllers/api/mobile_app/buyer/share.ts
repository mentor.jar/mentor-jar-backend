import { ProductRepo } from "../../../../repositories/ProductRepo"
import { ShopRepo } from "../../../../repositories/ShopRepo"
import { detailProductFromBuyer } from "../../../serviceHandles/product"
import {VN_LANG, EN_LANG, KO_LANG, API_COMMUNITY_PROD, API_COMMUNITY_STAG, API_KEY_COMMUNITY} from "../../../../base/variable"
import responseCode from "../../../../base/responseCode"
import axios from "axios"
import BannerRepo from "../../../../repositories/BannerRepo"
import { UserRepo } from "../../../../repositories/UserRepo"
import { PostRepo } from "../../../../repositories/PostRepo"
import { BBookUserRepo } from "../../../../repositories/BBookUserRepo"

const productRepo = ProductRepo.getInstance()
const shopRepo = ShopRepo.getInstance()
const bannerRepo = BannerRepo.getInstance()
const userRepo = UserRepo.getInstance()
const postRepo = PostRepo.getInstance()
const bbookUserRepo = BBookUserRepo.getInstance();

export const getInformation = async (req: any, res:any, next:any) => {
    try {
        const { link } = req.body
        let shortenLink = link.split("?")[0]
        shortenLink = shortenLink.split('asia')[1]

        const language =
        [VN_LANG, EN_LANG, KO_LANG].indexOf(req.headers['accept-language']) > -1
            ? req.headers['accept-language']
            : VN_LANG;
        if(shortenLink.includes('/p/')) {
            let product:any = await productRepo.findOne({shorten_link: `${shortenLink}`})
            if(!product) {
                res.error(responseCode.NOT_FOUND.name, req.__("product.not_exist_product"), responseCode.NOT_FOUND.code);
                return;
            }
            product = await detailProductFromBuyer(product._id, req.user, language)

            res.success(product)
        }
        else if(shortenLink.includes('/s/')) {
            const shop:any = await shopRepo.findOne({shorten_link: `${shortenLink}`})
            let url = ''

            if(process.env.NODE_ENV === 'production') {
                url = `${API_COMMUNITY_PROD}/v1/profile/user?userId=${shop?.user_id}`
            }
            else {
                url = `${API_COMMUNITY_STAG}/v1/profile/user?userId=${shop?.user_id}`
            }

            const result = await axios.get(url, {
                    headers: {
                        'x-api-key': API_KEY_COMMUNITY
                    }
                }).then(data => {
                    return data?.data?.data
                }).catch(error => {
                    throw new Error(error)
                })
                res.success(result)
        }
        else {
            res.success()
        }

    } catch (error) {
        res.error(responseCode.SERVER.name, error?.message, responseCode.SERVER.code)
    }
}

export const getRealID = async (req: any, res:any, next:any) => {
    try {
        const { link, from } = req.body
        let shortenLink = link.split("?")[0]
        shortenLink = shortenLink.split('asia')[1]

        const language =
        [VN_LANG, EN_LANG, KO_LANG].indexOf(req.headers['accept-language']) > -1
            ? req.headers['accept-language']
            : VN_LANG;
        if(shortenLink.includes('/p/')) {
            let product:any = await productRepo.findOne({shorten_link: `${shortenLink}`})
            if(!product) {
                res.error(responseCode.NOT_FOUND.name, req.__("product.not_exist_product"), responseCode.NOT_FOUND.code);
                return;
            }
            const response:any = {
                type: "PRODUCT",
                id: product._id
            }
            if(from === "web") {
                response.name = product?.name,
                response.image = product.images?.[0],
                response.description = product.description,
                response.price = product.sale_price || product.before_sale_price
            }

            res.success(response)
        }
        else if(shortenLink.includes('/s/')) {
            const shop:any = await shopRepo.findOne({shorten_link: `${shortenLink}`})

            if(!shop) {
                res.error(responseCode.NOT_FOUND.name, req.__("shop.shop_not_found"), responseCode.NOT_FOUND.code);
                return;
            }

            res.success({
                type: "SHOP EXPLORE",
                id: from === "web" ? shop._id : shop.user_id
            })
        }
        else if(shortenLink.includes('/u/')) {
            const user:any = await userRepo.findOne({shorten_link: `${shortenLink}`})

            if(!user) {
                res.error(responseCode.NOT_FOUND.name, req.__("seller.user_not_found"), responseCode.NOT_FOUND.code);
                return;
            }

            res.success({
                type: "USER",
                id: user._id
            })
        }
        else if(shortenLink.includes('/po/')) {
            const post:any = await postRepo.findOne({shorten_link: `${shortenLink}`})

            if(!post) {
                res.error(responseCode.NOT_FOUND.name, req.__("post.post_not_found"), responseCode.NOT_FOUND.code);
                return;
            }

            res.success({
                type: "POST",
                id: post._id
            })
        }
        else if(shortenLink.includes('/bb/')) {
            const bbook:any = await bbookUserRepo.findOne({shorten_link: `${shortenLink}`})

            if(!bbook) {
                res.error(responseCode.NOT_FOUND.name, req.__("post.bbook_not_found"), responseCode.NOT_FOUND.code);
                return;
            }

            res.success({
                type: "BBOOK",
                id: bbook._id
            })
        }
        else if(shortenLink.includes('/ba/') || shortenLink.includes('/bv/') || shortenLink.includes('/bp/')) {
            const banner: any = await bannerRepo.findOne({shorten_link: `${shortenLink}`})
            if(!banner) {
                res.error(responseCode.NOT_FOUND.name, req.__("banner.banner_not_found"), responseCode.NOT_FOUND.code);
                return;
            }
            const type = shortenLink.includes('/bv/') ? 'BANNER VOUCHER' : shortenLink.includes('/bp/') ? 'BANNER PRODUCT' : 'BANNER'
            res.success({
                type,
                id: banner._id
            })
        }
        else if(shortenLink.includes('/gb/')) {
            const product: any = await productRepo.findProductByReferralLink(shortenLink)
            if(product) {
                const room = product.order_rooms.find(room => {
                    return room.members.find(member => member.referal_link === shortenLink)
                })
                const referralID = room.members?.find(member => member.referal_link === shortenLink)?._id || null
                res.success({
                    type: "GROUP BUY",
                    id: product?._id,
                    referral_id: referralID,
                    order_room_id: room?._id || null
                })
            }
            else {
                res.success()
            }
        }
        else {
            res.success()
        }
    } catch (error) {
        res.error(responseCode.SERVER.name, error?.message, responseCode.SERVER.code)
    }
}