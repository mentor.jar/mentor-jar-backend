// const Category = require('../../models/Category');
import { CategoryRepo } from "../../../repositories/CategoryRepo"
import { createCategoryMobile, updateActiveCategoryMobileHandle, updateCategoryMobile } from "../../handleRequests/category/category"
import responseCode from "../../../base/responseCode"
import { ProductCategoryRepo } from "../../../repositories/ProductCategoryRepo"
import { ProductRepo } from "../../../repositories/ProductRepo"
import { cloneObj, ObjectId, timeOutOfPromise } from "../../../utils/helper"
import { sortByPriority } from "../../serviceHandles/categories"
import { CATEGORIES_LOCALIZED, VN_LANG } from "../../../base/variable"
import { ProductDTO } from "../../../DTO/Product"
import { getMinMax } from "../../serviceHandles/product"
import { PromotionProgramRepo } from "../../../repositories/PromotionProgramRepo"
import { InMemoryProductStore } from "../../../SocketStores/ProductStore"
import ProductService from "../../serviceHandles/product/service"
import setCacheHeaders from "../../../services/headerCache/headerCache"
import { CategoryDTO } from "../../../DTO/CategoryDTO"

const repository = CategoryRepo.getInstance();
const productCategoryRepo = ProductCategoryRepo.getInstance();
const productRepo = ProductRepo.getInstance();
const promotionRepo = PromotionProgramRepo.getInstance();
const productCache = InMemoryProductStore.getInstance();

export const getAllCategories = async (req: any, res: any, next: any) => {
    const language = req.headers["accept-language"] || VN_LANG
    let systemCategories: any = await repository.getCategorySystemOrderBy();

    systemCategories = systemCategories.map((category) => {
        const categoryLocalized = CATEGORIES_LOCALIZED.get(
            category.name.trim().toLowerCase()
        );

        if (categoryLocalized) {
            category.name_localized = {
                vi: category.name,
                en: categoryLocalized.en,
                ko: categoryLocalized.ko,
            };
            if (categoryLocalized[language]) {
                category.name = categoryLocalized[language];
            }
        } else {
            category.name_localized = {
                vi: category.name,
                en: category.name,
                ko: category.name,
            };
        }

        return new CategoryDTO(category).toSimpleJSON();
    });
    res.success(systemCategories);
}

export const getCategoriesByShop = async (req: any, res: any, next: any) => {
    const shop_id = req.shop_id;
    const language = req.headers["accept-language"] || VN_LANG
    try {
        let result = await repository.getTreeByShopId(shop_id, language);
        result.sort((a, b) => sortByPriority(a, b))
        res.success(result);
    } catch (error) {
        res.success([]);
    }
}

export const getCategoriesBySystem = async (req: any, res: any, next: any) => {
    try {
        const language = req.headers["accept-language"] || VN_LANG
        const getByAdmin = false;
        const result = await repository.getTreeBySystemByLocalized(getByAdmin, language);
        let categoriesBIDUAir = result.find((item) => item?.name === "BIDU Air");

        const listCategoryNotHaveBIDUAir = result
            .filter((item) => item?.name !== 'BIDU Air')
            .map((item) => {
                return {...item, parent_id: categoriesBIDUAir?._id.toString()};
            });

        categoriesBIDUAir.childs = listCategoryNotHaveBIDUAir;

        res.success(result);
    } catch (error) {
        res.success([]);
    }
}

export const getCategoriesBySystemV2 = async (req: any, res: any, next: any) => {
    try {
        const language = req.headers["accept-language"] || VN_LANG
        const getByAdmin = false;
        const result = await repository.getTreeBySystemByLocalized(getByAdmin, language);
        res.success(result);
    } catch (error) {
        res.success([]);
    }
}

export const getCategoriesById = async (req: any, res: any, next: any) => {
    const language = req.headers["accept-language"] || VN_LANG
    const id = req.params.id;
    try {
        const category = await repository.findById(id);

        const categoryLocalized = CATEGORIES_LOCALIZED.get(category.name.toLowerCase());
        categoryLocalized && categoryLocalized[language] ? category.name = categoryLocalized[language] : '';

        const productIds = await productCategoryRepo.getProductIdList(id)
        const productItems = await productRepo.find({ _id: { $in: productIds }, deleted_at: null, is_approved: "approved" })

        const resCategory = cloneObj(category)
        resCategory.products = productItems

        res.success(resCategory);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    }
}

export const searchCategoryBySystem = async (req: any, res: any, next: any) => {
    try {
        const category = await repository.searchCategoryBySystem(req.query.name);
        res.success(category);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    }
}

export const createCategory = async (req: any, res: any, next: any) => {
    try {
        const categoryObject = await createCategoryMobile(req);
        const category = await repository.create(categoryObject);

        // Add product if has
        let productIds = req.body.product_ids
        if (productIds && productIds.length) {
            await productCategoryRepo.createProductCategoryRecord(category._id, productIds)
        }
        else {
            productIds = await productCategoryRepo.getProductIdList(category._id)
        }

        const productItems = await productRepo.find({ _id: { $in: productIds }, deleted_at: null, is_approved: "approved" })

        const resCategory = cloneObj(category)
        resCategory.products = productItems

        res.success(resCategory);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    };
}

export const updateCategory = async (req: any, res: any, next: any) => {
    try {
        const infoUpdate = await updateCategoryMobile(req);
        const category = await repository.update(req.params.id, infoUpdate);

        // Update product_list
        const newProductIds = req.body.product_ids
        if (newProductIds) {
            await productCategoryRepo.updateProductCategoryRecord(category._id, newProductIds)
        }

        const productItems = await productRepo.find({ _id: { $in: newProductIds }, deleted_at: null, is_approved: "approved" })

        const resCategory = cloneObj(category)
        resCategory.products = productItems

        res.success(resCategory);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    }
}

export const deleteCategory = async (req: any, res: any, next: any) => {
    try {
        await repository.delete(req.params.id);
        await productCategoryRepo.deleteByCategoryId(req.params.id);
        res.success(null, responseCode.SUCCESS.name, responseCode.SUCCESS.code);
    } catch (error) {
        res.error(responseCode.NOT_FOUND.name, error.message, responseCode.NOT_FOUND.code);
    }

}

export const updateActiveCategory = async (req: any, res: any, next: any) => {
    try {
        const categoryObject = await updateActiveCategoryMobileHandle(req);
        const category = await repository.update(req.params.id, categoryObject);
        res.success(category);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode)
    }
}

export const sortCategory = async (req: any, res: any, next: any) => {
    try {
        const { category_ids } = req.body
        const length = category_ids.length
        const promises = category_ids.map((id, index) => {
            return new Promise(async (resolve, reject) => {
                let category:any = await repository.findOne({_id: ObjectId(id)})
                category = cloneObj(category)
                if(category) {
                    let newCategory = {...category, priority: length - index }
                    const updatedCategory = await repository.update(category._id, newCategory)
                    resolve(updatedCategory)
                }
                else {
                    reject(req.__("category.missing_category"))
                }
            })
        })

        const result = await Promise.all(promises)
        res.success(result)
    } catch (error) {
        console.log(error);
        res.error(responseCode.SERVER.name, req.__("category.missing_category"), responseCode.SERVER.code)
    }
}

export const productByCategoryV2 = async (req: any, res: any, next: any) => {
    try {
        setCacheHeaders(res, 30);
        const lang = req.headers['accept-language'] || VN_LANG
        const { id } = req.params
        const page = req.query.page || 1;
        const limit = req.query.limit || 10;

        let internationalCategory: any = await repository.findOne({
            name: 'BIDU Air',
            is_active: true,
        });
        internationalCategory = cloneObj(internationalCategory);

        // Get sub category
        const categories = await repository.getTreeAll(lang, true, id);
        
        // Get product by category
        // let products = await productRepo.getListProductOfCategorySystem(id, page, limit);
        // products = cloneObj(products);
        // let productItems = cloneObj(products.data);
        // const promiseProducts = productItems.map(async (product) => {
        //     let newProduct:any = ProductDTO.newInstance(product)
        //     newProduct = newProduct.getProductComplete(req.user)
        //     const promotion = await promotionRepo.getPromotionByProduct(newProduct._id)
        //     if (promotion) {
        //         newProduct.discount_percent = promotion.discount
        //     }
        //     else {
        //         newProduct.discount_percent = 0
        //     }
                          
        //     // newProduct.sold = product.order_items && product.order_items.length ? product.order_items[0].sold : 0
        //     delete newProduct.order_items
        //     return newProduct
        // })
        // products.data = await Promise.all(promiseProducts);

        let products:any = await ProductService._.getProductByCategory(id, page, limit, internationalCategory);
        products.data.forEach(product => {
            if(product.product_detail_infos) {
                product.sold = product?.sold < 10 ? 0 : product?.sold;
                product.product_detail_infos = product.product_detail_infos.map(categoryInfo => {
                    categoryInfo = cloneObj(categoryInfo)
                    categoryInfo.name = categoryInfo.name[lang];
                    return categoryInfo
                })
            }
            else {
                product.product_detail_infos = []
            }
        })

        // Get top product by category

        // const promises = [
        //     timeOutOfPromise(500, []),
        //     (async () => {
        //         const data = await productCache.get(`cate_${id}`)
        //         return data
        //     })()
        // ]

        // let results:any = await Promise.race(promises).then(data => data)

        // let topProductByCategory: any = {}
        // if(!results?.length) {
        //     // get from database
        //    topProductByCategory = await productRepo.findProductHome({ orderBy: 'best_sell', categoryID: id }, 1, 5)
        // }
        // else {
        //     topProductByCategory.data = results
        //     topProductByCategory.paginate = {
        //         "limit": results.length,
        //         "total_page": 1,
        //         "page": 1,
        //         "total_record": results.length
        //     }
        // }

        let topProductByCategory = await ProductService._.getTopProductByCategory(id, 1, 5, internationalCategory);

        topProductByCategory = cloneObj(topProductByCategory)
        const promiseProductByCategory = topProductByCategory.data.map(async (e)=> {
            let item:any = new ProductDTO(e);
            item = item.getProductComplete(req.user)
            item.sold = item?.sold < 10 ? 0 : item?.sold;
            const promotion = await promotionRepo.getPromotionByProduct(item._id)
            if (promotion) {
                item.discount_percent = promotion.discount
            }
            else {
                item.discount_percent = 0
            }
            // item.sold = e.order_items && e.order_items.length ? e.order_items[0].sold : 0
            return  item
        });
        topProductByCategory.data = await Promise.all(promiseProductByCategory);

        const responseData = {
            categories,
            products,
            topProduct: topProductByCategory
        }

        res.success(responseData);
    } catch (error) {
        console.log(error);
        res.error(responseCode.SERVER.name, req.__("category.missing_category"), responseCode.SERVER.code)
    }
}