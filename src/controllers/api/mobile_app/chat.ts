import { executeError } from "../../../base/appError";
import { RoomChatRepo } from "../../../repositories/RoomChatRepo";
import { cloneObj } from "../../../utils/helper";
import { handleRequestGetRoom, handleRequestcreateRoom } from "../../handleRequests/roomChat";
const roomChatRepo = RoomChatRepo.getInstance();
import { SUGGEST_CHAT_BUYER, SUGGEST_CHAT_SELLER } from "../../../base/variable"
export const getListRoomChatOfUser = async (req: any, res: any, next: any) => {
    try {
        let params = handleRequestGetRoom(req);
        let listRoom = await roomChatRepo.getRoom(params, false);
        res.success(listRoom);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getRedDot = async (req: any, res: any, next: any) => {
    try {
        let red_dots = await roomChatRepo.getTotalRedDotOfUser(req.user?._id);
        res.success(red_dots);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const createRoomChat = async (req: any, res: any, next: any) => {
    try {
        let params = await handleRequestcreateRoom(req);
        let listRoom = await roomChatRepo.createRoom(cloneObj(params));
        res.success(listRoom);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getDetailRoomChat = async (req: any, res: any, next: any) => {
    try {
        let room = await roomChatRepo.getRoomById(req.params.id, true);
        res.success(room);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const hideRoomChat = async (req: any, res: any, next: any) => {
    try {
        let room = await roomChatRepo.hideRoomChatByUser(req.params.id, req.user._id);
        res.success(room);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const addMessageChat = async (req: any, res: any, next: any) => {
    try {

        const message = {
            id: req.body.idMessage ? req.body.idMessage : Math.floor(Date.now() / 1000),
            content: req.body.content,
            type: req.body.type == 'image' ? 'image' : 'text',
            user_id: req.user._id.toString(),
            user: req.userParseDTO
        };
        let room = await roomChatRepo.addMessage(req.params.id, message, 0, 0);
        res.success(room);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const suggestChat = async (req: any, res: any, next: any) => {
    try {
        const buyerChat = SUGGEST_CHAT_BUYER.map(text => req.__(text))
        const sellerChat = SUGGEST_CHAT_SELLER.map(text => req.__(text))
        const suggestChat = {
            buyer_chat: buyerChat,
            seller_chat: sellerChat
        }
        res.success(suggestChat)
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}
