import { OptionTypeRepo } from '../../../repositories/Variant/OptionTypeRepo';
import { OptionValueRepo } from '../../../repositories/Variant/OptionValueRepo';
import { VariantOptionValueRepo } from '../../../repositories/Variant/VariantOptionValueRepo';
import { VariantRepo } from '../../../repositories/Variant/VariantRepo';
import { optionTypeHandle, variantHandle } from '../../handleRequests/variant/optionType';
import { VariantDTO } from '../../../DTO/VariantDTO';

const optionTypeRepo = OptionTypeRepo.getInstance();
const optionValueRepo = OptionValueRepo.getInstance();
const variantOptionValueRepo = VariantOptionValueRepo.getInstance();
const variant = VariantRepo.getInstance();

export const createOptionTypeAndValue = async (paramOptionTypes: Array<any>, product_id) => {
    let optionTypes = optionTypeHandle(paramOptionTypes);
    let optionTypeIdCurent: any = []

    await optionTypeRepo.deleteMany({ product_id: product_id })

    for (let optionType of optionTypes) {

        optionType.setAttribute('product_id', product_id);

        // get field option_type use create Repo
        let getOptionTypeDB = optionType.toDBJSON();
        let optionTypeSchema = await optionTypeRepo.createOrUpdate(getOptionTypeDB);
        let optionTypeRequest = optionType.toRequestJSON();
        optionType.setAttribute('_id', optionTypeSchema._id)
        let optionValues = optionType.getOptionValue(optionTypeRequest.option_values)

        // push id option type current
        optionTypeIdCurent.push(optionTypeSchema._id + "");
        const createOptionValuePromises: any = []
        await optionValueRepo.deleteMany({ option_type_id: optionTypeSchema._id + "" })

        optionValues.forEach(optionValue => {
            createOptionValuePromises.push(optionValueRepo.createOrUpdate(optionValue.toDBJSON()));
        });
        Promise.all(createOptionValuePromises);
    }

}

export const createVariant = async (pramVariantTypes: Array<VariantDTO>, product_id) => {
    let variants = variantHandle(pramVariantTypes);
    await variant.deleteMany({ product_id: product_id })
    for (let variantDTO of variants) {
        variantDTO.setAttribute('product_id', product_id);
        let getVariant = variantDTO.toDBJSON();
        let getVariantRequest = variantDTO.toRequestJSON();
        let optionTypeSchema = await variant.createOrUpdate(getVariant);

        variantDTO.setAttribute('_id', optionTypeSchema._id)
        let optionValues = variantDTO.getOptionValue(getVariantRequest.option_values)

        const createVariantOptionValuePromises: any = []

        optionValues.forEach(optionValue => {
            const promise = new Promise(async () => {
                let getOptionValue = optionValue.toDBJSON();
                let optionValueByname = await optionValueRepo.getByProductIdAndValue(product_id, getOptionValue.name)
                if (optionValueByname) {
                    let variantOptionValue = {
                        variant_id: optionTypeSchema._id,
                        option_value_id: optionValueByname._id
                    }
                    // search optionValue variant, if exist then update else create
                    await variantOptionValueRepo.createOrUpdate(variantOptionValue, variantOptionValue);
                }
            })
            createVariantOptionValuePromises.push(promise);
        });
        Promise.all(createVariantOptionValuePromises);
    }


}