import responseCode from "../../../base/responseCode";
import { EN_LANG, FEATURE_CATEGORY, KO_LANG, OPTION_IN_CATEGORY, VN_LANG } from "../../../base/variable";
import { CategoryInfoRepo } from "../../../repositories/CategoryInfoRepo"

let categoryInfoRepo = CategoryInfoRepo.getInstance();

export const getCategoriesById = async (req: any, res: any, next: any) => {
    const categoryId = req.params.id;
    const language =
        [VN_LANG, EN_LANG, KO_LANG].indexOf(req.headers['accept-language']) > -1
            ? req.headers['accept-language']
            : VN_LANG;
    try {
        let result = await categoryInfoRepo.getByCategoryId(categoryId);
        result = result.map((item) => {
            item.name = item.name[language]
            item.list_option = item.list_option.map(option => {
                option = option[language]
                return option
            }) 

            return item
        })

        res.success(result, responseCode.SUCCESS.code);
    } catch (error) {
        res.success([]);
    }
}