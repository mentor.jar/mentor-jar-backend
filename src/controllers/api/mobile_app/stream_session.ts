import { executeError } from "../../../base/appError";
import { StreamSessionRepository } from "../../../repositories/StreamSessionRepository";
import { streamSessionHandle } from "../../handleRequests/stream/streamSession";
import { StreamRepository } from "../../../repositories/StreamRepository";
import { cloneObj, ObjectId } from "../../../utils/helper";
import { filterStreamSession } from "../../handleRequests/stream/streamSession";
import { moveFile } from "../../../utils/fileHelper"
import { MediaRepo } from "../../../repositories/MediaRepo";
import { RoomChatRepo } from "../../../repositories/RoomChatRepo";
import { handleRequestcreateRoomLiveStream } from "../../handleRequests/roomChat";
import { getDetailStreamSession } from "../../serviceHandles/streams";
import { StreamSessionDTO } from "../../../DTO/StreamSessionDTO";
import { ReportLivestreamRepository } from "../../../repositories/ReportLivestreamRepo";
import { reportLiveStreamHandle } from "../../handleRequests/reportLivestream/reportLivestream";
import notificationService, { NotifyRequestFactory } from "../../../services/notification";
import { NOTIFY_TYPE, NOTIFY_TARGET_TYPE } from "../../../services/notification/constants";
import { FollowRepo } from "../../../repositories/FollowRepo";
import { getTranslation, getUserLanguage } from "../../../services/i18nCustom";
import { REPORT_LIVESTREAM_REASONS, VN_LANG } from "../../../base/variable";

let streamSessionRepository = StreamSessionRepository.getInstance();
let streamRepository = StreamRepository.getInstance();
const imageRepository = MediaRepo.getInstance();
const roomChatRepository = RoomChatRepo.getInstance();
const reportLivestreamRepository = ReportLivestreamRepository.getInstance();

export const createStreamSession = async (req: any, res: any, next: any) => {
    try {
        //check streamSession
        const liveStream = await streamSessionRepository.findOne({ user_id: req.user._id, active: true });
        if (liveStream) {
            throw new Error(req.__("controller.stream.stream_continuously"))
        }

        const stream = await streamRepository.getStreamByUserId(req.user._id);
        const paramsRoom = await handleRequestcreateRoomLiveStream(req)
        const room = await roomChatRepository.createRoom(paramsRoom);
        const streamSession = streamSessionHandle(room._id, stream._id, req);
        let oneStream: any = await streamSessionRepository.create(streamSession);
        oneStream = cloneObj(oneStream);
        oneStream.stream = stream;

        //update image
        let imgItem = await imageRepository.findByFullPath(streamSession.image);
        if (imgItem) {
            const updateInfo: any = {
                accessible_id: streamSession._id,
                accessible_type: 'Stream',
            }
            await imageRepository.update(imgItem._id, updateInfo)
        }
        res.success(oneStream);
    } catch (error) {
        executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getStreamSession = async (req: any, res: any, next: any) => {
    try {
        let streamSession = await streamSessionRepository.getStreamSession(req.params.id);
        streamSession = await StreamSessionDTO.newInstance(streamSession).completeDataStreamDetail(req);
        res.success(streamSession);
    } catch (error) {
        executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getListStreamSessionByUser = async (req: any, res: any, next: any) => {
    try {
        // let handleParam = filterStreamSession(req);
        // const listStreamSession = await streamSessionRepository.getListStreamSessionByShop(req.shop_id, handleParam.paginate);
        // listStreamSession.data = listStreamSession.data.map(session => StreamSessionDTO.newInstance(session).completeDataWithDTO());
        // res.success(listStreamSession);
        res.success([])
    } catch (error) {
        executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getListScheduledStream = async (req: any, res: any, next: any) => {
    try {
        let handleParam = filterStreamSession(req);
        const listStreamSession = await streamSessionRepository.getListScheduledStream(handleParam.paginate);
        listStreamSession.data = listStreamSession.data.map(session => StreamSessionDTO.newInstance(session).completeDataWithDTO());
        res.success(listStreamSession);
    } catch (error) {
        executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getListStreamSessionByViewer = async (req: any, res: any, next: any) => {
    try {
        // let handleParam = filterStreamSession(req);
        // const listStreamSession = await streamSessionRepository.getListStreamSessionByViewer(handleParam.paginate);
        // listStreamSession.data = listStreamSession.data.map(session => StreamSessionDTO.newInstance(session).completeDataWithDTO(req.user));
        // res.success(listStreamSession);
        res.success([])
    } catch (error) {
        executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const updateProductStreamSession = async (req: any, res: any, next: any) => {
    try {
        const { list_id_product } = req.body;
        const id = req.params.id;
        const instance = {
            list_id_product: list_id_product
        }
        const streamSession = await streamSessionRepository.update(id, instance)
        res.success(streamSession);
    } catch (error) {
        executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const startStreamSession = async (req: any, res: any, next: any) => {
    try {
        const streamSession = await streamSessionRepository.startStreamSession(req.params.id, req.user._id)
        const followers = await FollowRepo.getInstance().find({ userId: req.user._id });
        let listPromise: any = followers.map(follower => {
            return new Promise(async (resolve, reject) => {
                const language = await getUserLanguage(follower.followerId);
                await notificationService.saveAndSend(
                    NotifyRequestFactory.generate(
                        NOTIFY_TYPE.START_LIVESTREAM,
                        NOTIFY_TARGET_TYPE.BUYER,
                        {
                            title: getTranslation(language, 'notification.bidu_notification'),
                            content: getTranslation(language, 'controller.stream.start_live_stream_notification', req.user.nameOrganizer.userName),
                            receiverId: follower.followerId.toString(),
                            userId: follower.userId.toString(),
                            userName: req.user.nameOrganizer.userName,
                            streamSessionId: streamSession._id.toString()
                        }
                    ))
                resolve(true);
            })
        })
        await Promise.all(listPromise);

        res.success(streamSession);
    } catch (error) {
        executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const stopStreamSession = async (req: any, res: any, next: any) => {
    try {
        const streamSession = await streamSessionRepository.stopStreamSession(req.params.id)
        res.success(streamSession);
    } catch (error) {
        executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const reportStreamSession = async (req: any, res: any, next: any) => {
    try {
        let streamSession: any = await streamSessionRepository.findOne({ _id: req.params.id });
        const params = reportLiveStreamHandle(req, streamSession.user_id);

        const report = await reportLivestreamRepository.create(params);

        // send notification

        res.success(report);
    } catch (error) {
        executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getListReportReason = async (req: any, res: any, next: any) => {
    try {
        const lang = req.headers['accept-language'] || VN_LANG;     
        const listReport = REPORT_LIVESTREAM_REASONS.map(reason => {
            return {
                code: reason.code,
                name: reason[lang]
            }
        });
        res.success(listReport);
    } catch (error) {
        executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}