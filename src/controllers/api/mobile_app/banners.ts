import { executeError } from "../../../base/appError";
import BannerService, { CreateBannerRequest, UpdateBannerRequest } from '../../serviceHandles/banner';

/**
 * 
 * @middleware authShop
 * @param req 
 * @param res 
 * @param next 
 */
export const createBanner = async (req: any, res: any, next: any) => {
    try {
        const { name, image, promo_link, products, start_time, end_time, description } = req.body;
        const request: CreateBannerRequest = {
            name,
            image,
            promo_link,
            products,
            start_time: new Date(start_time * 1000),
            end_time: new Date(end_time * 1000),
            shop_id: req.shop_id.toString(),
            description
        };
        const banner = await BannerService._.createNewBanner(request);
        res.success(banner);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const updateBanner = async (req: any, res: any, next: any) => {
    try {
        const { name, image, promo_link, products, start_time, end_time, description } = req.body;

        const request: UpdateBannerRequest = {
            name,
            image,
            promo_link,
            products,
            start_time: new Date(start_time * 1000),
            end_time: new Date(end_time * 1000),
            description
        };
        const id = req.params.id;
        const banner = await BannerService._.updateBanner(id, request);

        res.success(banner);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const listBanners = async (req: any, res: any, next: any) => {
    try {
        const limit = req.query.limit || 10;
        const page = req.query.page || 1;
        const filter_type = req.query.filter_type || 'ALL';
        const banners = await BannerService._.getBannersByShop(req.shop_id.toString(), limit, page, filter_type);
        res.success(banners);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const deleteBanner = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id;
        await BannerService._.deleteBanner(id);
        res.success(true);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getBannerDetail = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id;
        const banner = await BannerService._.getBannerById(id);
        res.success(banner);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

