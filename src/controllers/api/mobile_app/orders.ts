

import { cloneObj, ObjectId } from "../../../utils/helper";
import responseCode from "../../../base/responseCode";
import { OrderDTO } from "../../../DTO/OrderDTO";
import { OrderRepository } from "../../../repositories/OrderRepo"
import GHTKService from "../../../services/3rd/shippings/GHTK";
import { ShippingError } from "../../../base/customError";
import { ShippingStatus, ShippingMethodQuery, CancelType, ShopReviewStatus, AdminReviewStatus, RefundMoneyStatus } from "../../../models/enums/order";
import { executeError } from "../../../base/appError";
import { FeedbackRepo } from "../../../repositories/FeedbackRepo";
import OrderService from "../../serviceHandles/order/order";
import { ShippingMethodRepo } from "../../../repositories/ShippingMethodRepo";
import { OrderShippingRepo } from "../../../repositories/OrderShippingRepo";
import notificationService, { NotifyRequestFactory } from "../../../services/notification";
import { NOTIFY_TARGET_TYPE, NOTIFY_TYPE } from "../../../services/notification/constants";
import { OrderItemRepository } from "../../../repositories/OrderItemRepo";
import { ProductRepo } from "../../../repositories/ProductRepo";
import { VariantRepo } from "../../../repositories/Variant/VariantRepo";
import { getTranslation, getUserLanguage } from "../../../services/i18nCustom";
import { UserRepo } from "../../../repositories/UserRepo";
import { AdHoc, TargetType } from "../../../models/enums/TrackingObject";
import { TrackingObjectRepo } from "../../../repositories/TrackingObjectRepo";
import { REFUND_REJECT_CONDITIONS } from "../../../base/variable";
import { handleUserAfterCancelOrder } from "../../../services/api/order";
import ViettelPostService from "../../../services/3rd/shippings/viettelpost/ViettelPost";
// import { createOrderHandle, updateOrderHandle, updateOrderCMSHandleMobile } from "../../handleRequests/Orders/Order"

let orderRepo = OrderRepository.getInstance();
const feedbackRepo = FeedbackRepo.getInstance();
const orderShippingRepo = OrderShippingRepo.getInstance();
const orderItemRepo = OrderItemRepository.getInstance();
const productRepo = ProductRepo.getInstance();
const variantRepo = VariantRepo.getInstance();
const shippingMethodRepo = ShippingMethodRepo.getInstance();
const userRepo = UserRepo.getInstance();
const trackingObjectRepo = TrackingObjectRepo.getInstance();

export const getDetailOrder = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id;
        let order = await orderRepo.getDetailOrder(id);
        order = cloneObj(order)
        const feedback = await feedbackRepo.findOne({ order_id: ObjectId(order._id), target_type: "User", target_id: ObjectId(order.user_id) })
        order.is_feedbacked_user = feedback ? true : false
        if(order.is_group_buy_order) {
            order.order_items[0].order_group_buy_price = order.total_value_items
            order.order_items[0].order_group_buy_member = order?.order_items?.[0]?.product?.group_buy?.groups?.find(item => item.group_buy_price === order.total_value_items)?.number_of_member
        }
        else {
            order.order_items[0].order_group_buy_price = null
            order.order_items[0].order_group_buy_member = null
        }
        res.success(order);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

/**
 * Code for create mock data
 * @param req 
 * @param res 
 * @param next 
 */
export const getOrderWithFilter = async (req: any, res: any, next: any) => {
    try {
        const shopId = req.shop_id
        const { limit, page, filter_type: filterType, keyword } = req.query
        let results: any = await orderRepo.getOrderWithFilter(shopId, limit, page, filterType, keyword)
        results = cloneObj(results)
        let orders = cloneObj(results.data)
        const promises = orders.map(order => {
            return new Promise(async (resolve, reject) => {
                order = cloneObj(order)
                order = await OrderDTO.newInstance(order).completeDataPaymentAndShippingInfo()
                if(order.is_group_buy_order) {
                    order.order_items[0].order_group_buy_price = order.total_value_items
                    order.order_items[0].order_group_buy_member = order?.order_items?.[0]?.product?.group_buy?.groups?.find(item => item.group_buy_price === order.total_value_items)?.number_of_member
                }
                else {
                    order.order_items[0].order_group_buy_price = null
                    order.order_items[0].order_group_buy_member = null
                }
                resolve(order)
            })
        })

        Promise.all(promises).then((data) => {
            results.data = data
            res.success(results)
        })
            .catch((error) => { throw new Error(error.message) })

    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}

export const searchOrder = async (req: any, res: any, next: any) => {
    try {
        const limit = req.query.limit
        const page = req.query.page
        const shopId = req.shop_id
        const keyword = req.query.keyword
        let results: any = await orderRepo.searchOrder(shopId, limit, page, keyword)

        results = cloneObj(results)
        let orders = cloneObj(results.data)
        const promises = orders.map(order => {
            return new Promise(async (resolve, reject) => {
                order = cloneObj(order)
                order = await OrderDTO.newInstance(order).completeDataPaymentAndShippingInfo()
                resolve(order)
            })
        })

        Promise.all(promises)
            .then((data) => {
                results.data = data
                res.success(results)
            })
            .catch((error) => { throw new Error(error.message) })
    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}

export const sellerCreateOrder = async (req: any, res: any, next: any) => {
    try {
        let createOrder;
        let result, messageSuccess;
        let orderShop = req.orderShop;
        if (orderShop.shipping_status === ShippingStatus.WAIT_TO_PICK) {
            let shippingMethod = await shippingMethodRepo.findById(orderShop.shipping_method_id);
            switch (shippingMethod.name_query) {
                case ShippingMethodQuery.GIAOHANGTIETKIEM:

                    // check shop turn on ghtk
                    let isTurnOn = await shippingMethodRepo.checkShopTurnOnShippingMethod(req.user.shop, orderShop.shipping_method_id);
                    if (!isTurnOn) throw new ShippingError(req.__("controller.order.need_turn_on_ghtk"));

                    const ghtkService = await GHTKService.getInstance();
                    const token = await ghtkService.getTokenGHTKByShop(req.user.shop);
                    createOrder = await ghtkService.createOrder(orderShop, token, req.addressShop);
                    result = await OrderService._.saveShippingInfoToOrderShipping(orderShop, createOrder, shippingMethod);
                    messageSuccess = req.__("controller.order.add_to_ghtk_success");
                    break;
                case ShippingMethodQuery.VIETTELPOST:
                    let isTurnOnVtp = await shippingMethodRepo.checkShopTurnOnShippingMethod(req.user.shop, orderShop.shipping_method_id);
                    if (!isTurnOnVtp) throw new ShippingError(req.__("controller.order.need_turn_on_vtp"));

                    createOrder = await ViettelPostService._.createBill({
                        orderShop,
                        addressShop: req.addressShop
                    });
                    result = await OrderService._.saveShippingInfoToOrderShipping(orderShop, createOrder, shippingMethod);
                    messageSuccess = req.__("controller.order.add_to_viettel_post_success");
                    break;
                default:
                    throw new ShippingError(req.__("controller.order.unsupport_shipping_method"));
            }
            return res.success(createOrder, messageSuccess);
        }

        throw new ShippingError(req.__("controller.order.not_wait_to_pick"));
    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
    }
}
export const getFeedbackOfOrder = async (req: any, res: any, next: any) => {
    try {
        const { id } = req.params
        const feedbacks = await feedbackRepo.getFeedbackByOrderID(id)
        res.success(feedbacks)
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const confirmShippedBySeller = async (req: any, res: any, next: any) => {
    try {
        const { id } = req.params;
        const instance = {
            shipping_status: ShippingStatus.SHIPPING
        }
        const order = await orderRepo.update(id, instance);
        await orderShippingRepo.findOneAndUpdate({ order_id: id }, { user_confirm_shipped_time: Date.now() })

        // Insert Table TrackingObject Seller
        const userShop = await userRepo.getUserByShopId(order.shop_id);
        // await trackingObjectRepo.createOrUpdateTrackingObject(userShop._id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);
        // Insert Table TrackingObject Buyer
        // await trackingObjectRepo.createOrUpdateTrackingObject(order.user_id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);
        res.success(order);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const shippingHistory = async (req: any, res: any, next: any) => {
    try {
        const { id } = req.params;

        const result = await OrderService._.getShippingHistory(id, req.user.shop);
        res.success(result);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const confirmCancelOrder = async (req: any, res: any, next: any) => {
    let order:any = req.order
    const is_approved = req.body.is_approved;
    try {
        if (is_approved) {
            order = await orderRepo.update(order._id, {
                shipping_status: ShippingStatus.CANCELED,
                cancel_type: CancelType.BUYER,
                cancel_by: order.user_id,
                cancel_time: new Date()
            })

            await updateQuantityCancelOrder(order._id);

            handleUserAfterCancelOrder(order.user_id)

            // send notification
            const language = await getUserLanguage(order.user_id);
            await notificationService.saveAndSend(
                NotifyRequestFactory.generate(
                    NOTIFY_TYPE.ORDERS_CANCELED,
                    NOTIFY_TARGET_TYPE.BUYER,
                    {
                        title: getTranslation(language, 'notification.bidu_notification'),
                        content: getTranslation(language, 'notification.buyer_order.order_canceled', order.order_number),
                        receiverId: order.user_id.toString(),
                        orderId: order._id.toString(),
                        userId: order.user_id.toString(),
                        orderNumber: order.order_number,
                    }
                ))
        } else {
            if (order.payment_status === 'paid') {
                order = await orderRepo.update(order._id, {
                    shipping_status: ShippingStatus.WAIT_TO_PICK
                })
            } else {
                if(order.approved_time) {
                    order = await orderRepo.update(order._id, {
                        shipping_status: ShippingStatus.WAIT_TO_PICK
                    })
                }
                else {
                    order = await orderRepo.update(order._id, {
                        shipping_status: ShippingStatus.PENDING
                    })
                }
            }

        }

        // Insert Table TrackingObject Seller
        const userShop = await userRepo.getUserByShopId(order.shop_id);
        // await trackingObjectRepo.createOrUpdateTrackingObject(userShop._id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);
        // Insert Table TrackingObject Buyer
        // await trackingObjectRepo.createOrUpdateTrackingObject(order.user_id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);

        res.success(order)
    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}

export const cancelOrderSeller = async (req: any, res: any, next: any) => {
    let order = req.order
    const reason = req.body.cancel_reason;
    try {
        // handle orderShipping
        const orderShipping: any = await orderShippingRepo.findOne({ order_id: order._id });
        if (orderShipping && orderShipping.shipping_info && orderShipping.shipping_info.label) {
            throw new Error(req.__("controller.order.can_not_cancel_order_delivered"));
        }

        order = await orderRepo.update(order._id, {
            shipping_status: ShippingStatus.CANCELED,
            cancel_reason: reason,
            cancel_type: CancelType.SELLER,
            cancel_by: req.user._id,
            cancel_time: new Date()
        })
        await updateQuantityCancelOrder(order._id);

        handleUserAfterCancelOrder(order.user_id)

        // send notification
        const language = await getUserLanguage(order.user_id);
        await notificationService.saveAndSend(
            NotifyRequestFactory.generate(
                NOTIFY_TYPE.ORDERS_CANCELED,
                NOTIFY_TARGET_TYPE.BUYER,
                {
                    title: getTranslation(language, 'notification.bidu_notification'),
                    content: getTranslation(language, 'notification.buyer_order.order_canceled', order.order_number),
                    receiverId: order.user_id.toString(),
                    orderId: order._id.toString(),
                    userId: order.user_id.toString(),
                    orderNumber: order.order_number,
                }
            ))

        // Insert Table TrackingObject Seller
        const userShop = await userRepo.getUserByShopId(order.shop_id);
        // await trackingObjectRepo.createOrUpdateTrackingObject(userShop._id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);
        // Insert Table TrackingObject Buyer
        // await trackingObjectRepo.createOrUpdateTrackingObject(order.user_id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);

        res.success(order)
    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}

export const updateQuantityCancelOrder = async (orderId) => {
    // Update quantity
    const orderItems = await orderItemRepo.find({ order_id: orderId });
    let listProductId: any = [];
    const promises = orderItems.map(orderItem => {
        return new Promise(async (resolve, reject) => {
            const product: any = await productRepo.findOne({ _id: orderItem.product_id });
            const quantity = orderItem.quantity;
            if (orderItem.variant) {
                const variant: any = orderItem.variant;
                const variantProduct: any = await variantRepo.findOne({ _id: variant._id });
                if (variantProduct) {
                    const newVariantQuantity = variantProduct.quantity + quantity;
                    listProductId.push(orderItem.product_id.toString());
                    await variantRepo.update(variantProduct._id, { quantity: newVariantQuantity });
                }
            } else {
                const newProductQuantity = product.quantity + quantity;
                await productRepo.update(product._id, { quantity: newProductQuantity });
            }
            resolve(orderItem);
        })
    })
    await Promise.all(promises);
    const listUniqueProduct: any = [...new Set(listProductId)];

    //update product quantity after update variant
    const promiseProducts = listUniqueProduct.map(productId => {
        return new Promise(async (resolve, reject) => {
            let totalQuantity: number = 0;
            const listVariant: any = await variantRepo.find({ product_id: productId });
            listVariant.forEach(variant => {
                totalQuantity += variant.quantity;
            });
            await productRepo.update(productId, { quantity: totalQuantity });
            resolve(productId);
        })
    })
    Promise.all(promiseProducts);

}

export const changeSellerNote = async (req: any, res: any, next: any) => {
    let order = req.order;
    const seller_note = req.body.seller_note;
    try {
        order = await orderRepo.update(order._id, {
            seller_note: seller_note,
        });
        res.success(order)
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const approveRejectRefundRequest = async (req: any, res: any, next: any) => {
    const { is_approved, reject_reason, item_id } = req.body;
    const order = req.order;
    const indexObj = req.indexObj;
    const info = req.info;
    let refundInfos = req.refundInfos;

    try {
        const language = await getUserLanguage(order.user_id);
        const orderItemEl: any = await orderItemRepo.findById(item_id.toString());
        const product = await productRepo.findById(orderItemEl.product_id.toString());
        if (is_approved) {
            info.shop_review = { ...info.shop_review, status: ShopReviewStatus.APPROVED, time: new Date() }
            info.admin_review = { ...info.admin_review, status: AdminReviewStatus.APPROVED, time: new Date() }

            // send notification
            await notificationService.saveAndSend(
                NotifyRequestFactory.generate(
                    NOTIFY_TYPE.REFUND_SHOP_APPROVED,
                    NOTIFY_TARGET_TYPE.BUYER,
                    {
                        title: getTranslation(language, 'notification.bidu_notification'),
                        content: getTranslation(language, 'notification.refund_shop_approved', product.name, order.order_number),
                        receiverId: order.user_id.toString(),
                        orderId: order._id.toString(),
                        userId: order.user_id.toString(),
                        orderNumber: order.order_number,
                        itemId: item_id,
                        itemName: product.name
                    }
                ))
        }
        else {
            info.admin_review = { ...info.admin_review, status: AdminReviewStatus.PENDING }

            const reasonViaObjects = reject_reason.reasons.map(code => {
                const reasonObject = REFUND_REJECT_CONDITIONS.filter(item => +item.code === +code)[0]
                if (reasonObject) {
                    return reasonObject
                }
            })

            info.shop_review = {
                ...info.shop_review,
                status: ShopReviewStatus.REJECTED,
                time: new Date(),
                reasons: reasonViaObjects || [],
                images: reject_reason?.images || [],
                video: reject_reason?.video || null,
                note: reject_reason?.note || null,
                email: reject_reason?.email || null
            }

            // send notification
            await notificationService.saveAndSend(
                NotifyRequestFactory.generate(
                    NOTIFY_TYPE.REFUND_SHOP_REJECTED,
                    NOTIFY_TARGET_TYPE.BUYER,
                    {
                        title: getTranslation(language, 'notification.bidu_notification'),
                        content: getTranslation(language, 'notification.refund_shop_rejected', product.name, order.order_number),
                        receiverId: order.user_id.toString(),
                        orderId: order._id.toString(),
                        userId: order.user_id.toString(),
                        orderNumber: order.order_number,
                        itemId: item_id,
                        itemName: product.name
                    }
                ))
        }
        refundInfos[indexObj] = info;
        let instance = {
            refund_information: refundInfos
        }

        const orderUpdate = await orderRepo.update(order._id, instance);

        // Insert Table TrackingObject Seller
        const userShop = await userRepo.getUserByShopId(order.shop_id);
        // await trackingObjectRepo.createOrUpdateTrackingObject(userShop._id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);
        // Insert Table TrackingObject Buyer
        // await trackingObjectRepo.createOrUpdateTrackingObject(order.user_id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);

        res.success(orderUpdate);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }

}

export const confirmReceivedBySeller = async (req: any, res: any, next: any) => {
    const { item_id } = req.body;
    const order = req.order;
    const indexObj = req.indexObj;
    const info = req.info;
    let refundInfos = req.refundInfos;
    try {
        info.shop_review = {
            ...info.shop_review,
            seller_confirm_received_time: new Date()
        }
        info.refund_money = {
            ...info.refund_money,
            status: RefundMoneyStatus.TRANSFERRING
        }

        refundInfos[indexObj] = info;
        let instance = {
            refund_information: refundInfos
        }

        // send notification
        const language = await getUserLanguage(order.user_id);
        const orderItemEl: any = await orderItemRepo.findById(item_id.toString());
        const product = await productRepo.findById(orderItemEl.product_id.toString());
        await notificationService.saveAndSend(
            NotifyRequestFactory.generate(
                NOTIFY_TYPE.REFUND_SHOP_RECEIVED,
                NOTIFY_TARGET_TYPE.BUYER,
                {
                    title: getTranslation(language, 'notification.bidu_notification'),
                    content: getTranslation(language, 'notification.refund_shop_received', product.name, order.order_number),
                    receiverId: order.user_id.toString(),
                    orderId: order._id.toString(),
                    userId: order.user_id.toString(),
                    orderNumber: order.order_number,
                    itemId: item_id,
                    itemName: product.name
                }
            ))

        const orderUpdate = await orderRepo.update(order._id, instance);

        // Insert Table TrackingObject Seller
        const userShop = await userRepo.getUserByShopId(order.shop_id);
        // await trackingObjectRepo.createOrUpdateTrackingObject(userShop._id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);
        // Insert Table TrackingObject Buyer
        // await trackingObjectRepo.createOrUpdateTrackingObject(order.user_id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);

        res.success(orderUpdate);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}