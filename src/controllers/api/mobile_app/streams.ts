import { StreamRepository } from "../../../repositories/StreamRepository";
import { executeError } from "../../../base/appError";
import { createStreamFile, getListStreamFile } from "../../serviceHandles/streams";
import { streamHandle } from "../../handleRequests/stream/stream";

let streamRepository = StreamRepository.getInstance();
export const createStream = async (req: any, res: any, next: any) => {
    try {
        const streamFile = streamHandle(req);
        const oneStream = await streamRepository.create(streamFile);
        res.success(oneStream);

    } catch (error) {
        executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getStream = async (req: any, res: any, next: any) => {
    const stream_id = req.params.id;
    try {
        const streamFile = await await streamRepository.findById(stream_id);
        res.success(streamFile);
    } catch (error) {
        executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getListStream = async (req: any, res: any, next: any) => {
    try {
        const listStreamFile = await getListStreamFile();
        res.success(listStreamFile);
    } catch (error) {
        executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

