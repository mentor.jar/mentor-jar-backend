import { executeError } from "../../../base/appError";
import FollowShopService, { CreteaFollowShop } from "../../serviceHandles/followShop";

/**
 * 
 * @middleware auth
 * @param req 
 * @param res 
 * @param next 
 */
export const createFollowShop = async (req: any, res: any, next: any) => {
    try {
        const request: CreteaFollowShop = {
            user_id: req.user._id.toString(),
            shop_id: req.body.shop_id
        };
        const followShop = await FollowShopService._.createFollowShop(request);
        res.success(followShop);
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}


