import { ObjectId } from "../../../utils/helper";
import responseCode from "../../../base/responseCode";
import { VoucherDTO } from "../../../DTO/VoucherDTO";
import { VoucherRepository } from "../../../repositories/VoucherRepository"
import { cloneObj } from "../../../utils/helper";
import { createVoucherHandle, updateVoucherHandle, updateVoucherCMSHandleMobile } from "../../handleRequests/vouchers/voucher"
import { ShippingMethodRepo } from "../../../repositories/ShippingMethodRepo";
import { PaymentMethodRepo } from "../../../repositories/PaymentMethodRepo";

const voucherRepo = VoucherRepository.getInstance();
const shippingMethodRepo = ShippingMethodRepo.getInstance()
const paymentMethodRepo = PaymentMethodRepo.getInstance()

export const createVoucher = async (req: any, res: any, next: any) => {
    try {
        const voucherItem = await createVoucherHandle(req)
        let voucher = await voucherRepo.create(voucherItem)
        voucher = new VoucherDTO(voucher).convertVoucher()
        res.success(voucher, responseCode.SUCCESS.name, responseCode.SUCCESS.code);
    } catch (error) {
        if (error.code === 11000) {
            res.error(responseCode.UNIQUE_DATA.name, req.__("controller.voucher.code_already_exist"), responseCode.UNIQUE_DATA.code)
        }
        else {
            res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
        }
    }
}

export const getDetailVoucher = async (req: any, res: any, next: any) => {
    try {
        let voucher = await voucherRepo.getDetailVoucher(req.params.id);
        if (voucher) {
            voucher = cloneObj(voucher)
            const { conditions } = voucher
            if (conditions) {
                conditions.shipping_methods = await shippingMethodRepo.getShippingMethodByIds(conditions.shipping_method)
                conditions.payment_methods = await paymentMethodRepo.getPaymentMethodByIds(conditions.payment_method)
            }
            res.success(voucher, responseCode.SUCCESS.name, responseCode.SUCCESS.code);
        }
        else {
            res.error(responseCode.NOT_FOUND.name, req.__("controller.voucher.voucher_not_found"), responseCode.NOT_FOUND.code);
        }
    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}

export const getlVoucherWithFilter = async (req: any, res: any, next: any) => {
    try {
        const limit = req.query.limit
        const page = req.query.page
        const shopId = req.shop_id
        const filterType = req.query.filter_type
        let results = await voucherRepo.getVoucherWithFilter(shopId, limit, page, filterType)
        results = cloneObj(results)
        let vouchers = cloneObj(results.data)
        results.data = vouchers.map(voucher => {
            return new VoucherDTO(voucher).convertVoucher()
        })
        res.success(results);
    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}

export const getVoucherWithFilterOfShop = async (req: any, res: any, next: any) => {
    try {
        const limit = req.query.limit
        const page = req.query.page
        const shopId = req.params.id
        const filterType = req.query.filter_type
        let results = await voucherRepo.getVoucherWithFilter(shopId, limit, page, filterType)
        results = cloneObj(results)
        let vouchers = cloneObj(results.data)
        results.data = vouchers.map(voucher => {
            return new VoucherDTO(voucher).convertVoucher()
        })
        res.success(results);
    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}

export const getVoucherWithFilterOfShopWithoutHiddenVoucher = async (req: any, res: any, next: any) => {
    try {
        const limit = req.query.limit
        const page = req.query.page
        const shopId = req.params.id
        const filterType = req.query.filter_type
        let results = await voucherRepo.getVoucherWithFilter(shopId, limit, page, filterType, false)
        results = cloneObj(results)
        let vouchers = cloneObj(results.data)
        results.data = vouchers.map(voucher => {
            return new VoucherDTO(voucher).convertVoucher()
        })
        res.success(results);
    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}

export const updateVoucher = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id
        const voucher = await voucherRepo.findById(id)
        if (voucher) {
            const voucherItem = await updateVoucherHandle(req, voucher)
            let newVoucher: any = await voucherRepo.update(id, voucherItem)
            newVoucher = new VoucherDTO(newVoucher).convertVoucher()
            res.success(newVoucher, responseCode.SUCCESS.name, responseCode.SUCCESS.code);
        }
        else {
            res.error(responseCode.NOT_FOUND.name, req.__("controller.voucher.voucher_not_found"), responseCode.NOT_FOUND.code);
        }
    } catch (error) {
        if (error.code === 11000) {
            res.error(responseCode.UNIQUE_DATA.name, req.__("controller.voucher.code_already_exist"), responseCode.UNIQUE_DATA.code)
        }
        else {
            res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
        }
    }
}

export const updateCMSVoucherForMobile = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id
        const voucher = await voucherRepo.findById(id)
        if (voucher) {
            const voucherItem = await updateVoucherCMSHandleMobile(req, voucher)
            let newVoucher: any = await voucherRepo.update(id, voucherItem)
            newVoucher = new VoucherDTO(newVoucher).convertVoucher()
            res.success(newVoucher, responseCode.SUCCESS.name, responseCode.SUCCESS.code);
        }
        else {
            res.error(responseCode.NOT_FOUND.name, req.__("controller.voucher.voucher_not_found"), responseCode.NOT_FOUND.code);
        }
    } catch (error) {
        if (error.code === 11000) {
            res.error(responseCode.UNIQUE_DATA.name, req.__("controller.voucher.code_already_exist"), responseCode.UNIQUE_DATA.code)
        }
        else {
            res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
        }
    }
}

export const endVoucher = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id
        const shop_id = req.shop_id
        const voucher = await voucherRepo.findOne({
            $and: [
                { _id: ObjectId(id) },
                { shop_id: shop_id },
                { $or: [{ deleted_at: { $exists: false } }, { deleted_at: null }] }
            ]
        })
        if (voucher) {
            const updateInfo = {
                end_time: new Date()
            }
            let result: any = await voucherRepo.update(id, updateInfo)
            result = new VoucherDTO(result).convertVoucher()
            res.success(result, responseCode.SUCCESS.name, responseCode.SUCCESS.code);
        }
        else {
            res.error(responseCode.NOT_FOUND.name, req.__("controller.voucher.voucher_not_found"), responseCode.NOT_FOUND.code);
        }
    } catch (error) {
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}

export const deleteVoucher = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id
        const voucher = await voucherRepo.findById(id)
        if (voucher) {
            await voucherRepo.delete(id);
            res.success(null, responseCode.SUCCESS.name, responseCode.SUCCESS.code);
        }
        else {
            res.error(responseCode.NOT_FOUND.name, req.__("controller.voucher.voucher_not_found"), responseCode.NOT_FOUND.code);
        }

    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    };
}


