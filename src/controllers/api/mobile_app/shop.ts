import { executeError } from "../../../base/appError";
import responseCode from "../../../base/responseCode";
import keys from "../../../config/env/keys";
import { StreamSessionDTO } from "../../../DTO/StreamSessionDTO";
import { UserDTO } from "../../../DTO/UserDTO";
import { FeedbackRepo } from "../../../repositories/FeedbackRepo";
import { ShopRepo } from "../../../repositories/ShopRepo";
import { StreamSessionRepository } from "../../../repositories/StreamSessionRepository";
import { cloneObj } from "../../../utils/helper";
import { addLink } from "../../../utils/stringUtil";
import { filterShop, handleTurnOnShippingMethodForShop, handleUpdateSetting } from "../../handleRequests/shop/shop";
import { FollowRepo } from '../../../repositories/FollowRepo';
import { FollowShopRepository } from '../../../repositories/FollowShopRepository';
import { ProductRepo } from "../../../repositories/ProductRepo";
import ShopService from "../../serviceHandles/shop/service";
import GHTKService from "../../../services/3rd/shippings/GHTK";
import { AddressRepo } from "../../../repositories/AddressRepo";
import AddressService from "../../serviceHandles/address";
import { ShippingMethodQuery } from "../../../models/enums/order";

const shopRepo = ShopRepo.getInstance();
const feedbackRepo = FeedbackRepo.getInstance();
let streamSessionRepository = StreamSessionRepository.getInstance();
let addressRepository = AddressRepo.getInstance();

import { DEFAULT_KOREA_SHOP, KOREA, REFUND_CONDITIONS, VIET_NAM } from '../../../base/variable'
import UserService from "../../serviceHandles/user";

export const fakeCreateShop = async (req: any, res: any, next: any) => {
    res.success()
}

export const getListShop = async (req: any, res: any, next: any) => {
    try {
        let handleParam = filterShop(req);
        let result = await shopRepo.getListShop(handleParam.paginate, handleParam.params);
        result = cloneObj(result)
        const promises = result.data.map(async (shop) => {
            return new Promise(async (resolve, reject) => {
                try {
                    shop = cloneObj(shop)
                    if (shop.user) {
                        shop.user.avatar = addLink(`${keys.host_community}/`, shop.user.gallery_image?.url)
                        shop.user = new UserDTO(shop.user).toSimpleUserInfo();
                    }
                    const averageFeedbackRate = await feedbackRepo.getFeedbackAvgByShopID(shop._id)
                    shop.feedback = {
                        averageFeedbackRate
                    }
                    resolve(shop)
                } catch (error) {
                    reject(error)
                }
            })
        })
        Promise.all(promises).then(data => {
            result.data = data
            res.success(result);
        }).catch(error => {
            error = executeError(error);
            res.error(error.name, error.message, error.statusCode);
        })
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getDetailShop = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id;

        if (id) {
            const shop = await shopRepo.getDetailShop(id);
            res.success(shop, responseCode.SUCCESS.name, responseCode.SUCCESS.code);
        }
        else {
            res.error(responseCode.NOT_FOUND.name, 'Không tìm thấy cửa hàng', responseCode.NOT_FOUND.name);
        }
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getDetailShopWithStatistic = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id;
        if (!id) return res.error(responseCode.NOT_FOUND.name, 'Không tìm thấy cửa hàng', responseCode.NOT_FOUND.name);

        const shop: any = await shopRepo.getDetailShop(id);
        let is_followed = false;
        if (req.user) {
            is_followed = await FollowShopRepository.getInstance().checkFollow(id, req.user._id)
        }
        shop.is_follow = is_followed;
        shop.total_product = (await ProductRepo.getInstance().findWithShop(id))?.length;
        shop.avg_rating = await FeedbackRepo.getInstance().getFeedbackAvgByShopID(id);
        shop.total_rating = (await FeedbackRepo.getInstance().getTotalFeedbackByShopID(id))?.length;
        res.success(shop, responseCode.SUCCESS.name, responseCode.SUCCESS.code);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const settingShop = async (req: any, res: any, next: any) => {
    try {
        const id = req.shop_id;
        const paramUpdateSetting = handleUpdateSetting(req);
        await shopRepo.settingShop(id, paramUpdateSetting);
        const shop = await shopRepo.getDetailShop(id);
        res.success(shop, responseCode.SUCCESS.name, responseCode.SUCCESS.code);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const pauseMode = async (req: any, res: any, next: any) => {
    try {
        const id = req.shop_id;
        const pauseMode = req.body.pause_mode
        await shopRepo.settingShop(id, { pause_mode: pauseMode });
        const shop = await shopRepo.getDetailShop(id);
        res.success(shop, responseCode.SUCCESS.name, responseCode.SUCCESS.code);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getUserTrends = async (req: any, res: any, next: any) => {
    let list_stream_session = await streamSessionRepository.getListStreamSessionByShopStatistics(req.shop_id);
    list_stream_session = list_stream_session.map(session => StreamSessionDTO.newInstance(session).toUserTrendJSON());
    let user_trends = {};
    switch (req.query.trend_type) {
        case 'AGE':
            user_trends = {
                trend_type: 'AGE',
                data: [
                    {
                        label: "~18",
                        value: "14"
                    },
                    {
                        label: "19~23",
                        value: "15"
                    },
                    {
                        label: "24~28",
                        value: "46"
                    },
                    {
                        label: "29~33",
                        value: "15"
                    },
                    {
                        label: "34~39",
                        value: "5"
                    },
                    {
                        label: "~40+",
                        value: "5"
                    }
                ],
            }
            break;
        case 'GENDER':
            user_trends = {
                trend_type: 'GENDER',
                data: [
                    {
                        label: "~18",
                        value: "50"
                    },
                    {
                        label: "19~23",
                        value: "30"
                    },
                    {
                        label: "24~28",
                        value: "20"
                    }
                ],
            }
            break;
        case 'REGULAR_CUSTOMER':
            user_trends = {
                trend_type: 'REGULAR_CUSTOMER',
                data: [
                    {
                        label: "~18",
                        value: {
                            "nam": 60,
                            "nữ": 60,
                            "khác": 5,
                        }
                    },
                    {
                        label: "19~23",
                        value: {
                            "nam": 60,
                            "nữ": 60,
                            "khác": 5,
                        }
                    },
                    {
                        label: "24~28",
                        value: {
                            "nam": 60,
                            "nữ": 60,
                            "khác": 5,
                        }
                    },
                    {
                        label: "29~33",
                        value: {
                            "nam": 60,
                            "nữ": 60,
                            "khác": 5,
                        }
                    },
                    {
                        label: "34~39",
                        value: {
                            "nam": 60,
                            "nữ": 60,
                            "khác": 5,
                        }
                    },
                    {
                        label: "~40+",
                        value: {
                            "nam": 60,
                            "nữ": 60,
                            "khác": 5,
                        }
                    }
                ],
            }
            break;
        case 'LIVE_STREAM':
            user_trends = {
                trendType: 'LIVE_STREAM',
                data: list_stream_session
            }
            break;
        default:
    }
    res.success(user_trends)
}

export const generalStatistics = async (req: any, res: any, next: any) => {
    const result = {
        indicator: {
            order: {
                total: 10,
                change_type: 'DOWN',
                change_percent: '20%'
            },
            revenue: {
                total: 246000,
                change_type: 'UP',
                change_percent: '5%'
            },
            conversion_rate: {
                total: 50,
                change_type: 'UP',
                change_percent: '14%'
            },
            revenue_per_order: {
                total: 24600,
                change_type: 'EQUAL',
                change_percent: '0%'
            },
            access_times: {
                total: 24,
                change_type: 'DOWN',
                change_percent: '67%'
            },
            view: {
                total: 15,
                change_type: 'DOWN',
                change_percent: '30%'
            }

        },
        order_chart: {
            date_type: 'TODAY',
            indicator_type: 'ORDER',
            data: [
                {
                    label: "00:00",
                    value: 0,
                    ox: 0
                },
                {
                    label: "04:00",
                    value: 4,
                    ox: 4
                },
                {
                    label: "08:00",
                    value: 6,
                    ox: 8
                },
                {
                    label: "12:00",
                    value: 3,
                    ox: 12
                },
                {
                    label: "16:00",
                    value: 0,
                    ox: 16
                },
                {
                    label: "20:00",
                    value: 0,
                    ox: 20
                },
                {
                    label: "23:00",
                    value: 1,
                    ox: 23
                }
            ]
        },
        product_ranking: {
            category: null,
            ranking_type: 'REVENUE',
            ranking_order: 'DESC',
            data: [
                {
                    _id: "6058a13988f38b0012dcf64f",
                    name: "Đầm Trắng Tinh Tế giành cho nữ",
                    images: [
                        "http://103.92.29.93//uploads/images/products/1616421120657_image_292066.jpeg",
                        "http://103.92.29.93//uploads/images/products/1616421132200_image_325835.jpeg",
                        "http://103.92.29.93//uploads/images/products/1616421132182_image_967652.jpeg"
                    ],
                    list_category_id: [
                        "60346d40f2a39af86e6bfe7d",
                        "60347191a2b15cf961647a76",
                        "60347258a2b15cf961647a7f"
                    ]
                },
                {
                    _id: "6058a13988f38b0012dcf64f",
                    name: "Đầm Trắng Tinh Tế giành cho nữ",
                    images: [
                        "http://103.92.29.93//uploads/images/products/1616421120657_image_292066.jpeg",
                        "http://103.92.29.93//uploads/images/products/1616421132200_image_325835.jpeg",
                        "http://103.92.29.93//uploads/images/products/1616421132182_image_967652.jpeg"
                    ],
                    list_category_id: [
                        "60346d40f2a39af86e6bfe7d",
                        "60347191a2b15cf961647a76",
                        "60347258a2b15cf961647a7f"
                    ]
                },
                {
                    _id: "6058a13988f38b0012dcf64f",
                    name: "Đầm Trắng Tinh Tế giành cho nữ",
                    images: [
                        "http://103.92.29.93//uploads/images/products/1616421120657_image_292066.jpeg",
                        "http://103.92.29.93//uploads/images/products/1616421132200_image_325835.jpeg",
                        "http://103.92.29.93//uploads/images/products/1616421132182_image_967652.jpeg"
                    ],
                    list_category_id: [
                        "60346d40f2a39af86e6bfe7d",
                        "60347191a2b15cf961647a76",
                        "60347258a2b15cf961647a7f"
                    ]
                }
            ]
        },
        user_trends: {
            trend_type: 'OLD',
            data: [
                {
                    label: "~18",
                    value: 14
                },
                {
                    label: "19~23",
                    value: 15
                },
                {
                    label: "24~28",
                    value: 45.5
                },
                {
                    label: "29~33",
                    value: 15.5
                },
                {
                    label: "34~39",
                    value: 5
                },
                {
                    label: "~40+",
                    value: 5
                }
            ]
        }
    }
    res.success(result)
}


export const activeShippingMethodForShop = async (req: any, res: any, next: any) => {
    try {
        const params = handleTurnOnShippingMethodForShop(req, req.params.id);

        if (req.createAccount) {
            switch (req.name_query) {
                case ShippingMethodQuery.GIAOHANGTIETKIEM:
                    await createGHTKAccountForShop(params, req);
                    break;
                case ShippingMethodQuery.VIETTELPOST:
                    await ShopService._.createStoreViettelPost(params, req);
                    break;
            }
        }
        await ShopService._.updateActiveShippingMethodByShop(params);
        UserService._.clearUserInfoInCache(params.user_id.toString())
        return res.success({ is_active: JSON.parse(params.is_active) });
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const createGHTKAccountForShop = async (params, req) => {
    const pickAddressDefault = await addressRepository.getPickAddressUser(params.user_id);
    if (!pickAddressDefault) throw new Error(req.__("controller.shop.default_pick_address_not_found"));

    let ghtkService = await GHTKService.getInstance();
    let account = ghtkService.setAccountForShop();
    let data = await ShopService._.updateShippingMethodByShop(params, account);
    return data;
}

const setAccountCreateGHTK = (user, addressUser) => {
    const account = {
        name: user.nameOrganizer.userName,
        first_address: `${addressUser.street} ${addressUser.ward.name}`,
        province: addressUser.state.name,
        district: addressUser.district.name,
        tel: addressUser.phone,
        email: user.email
    }
    return account;
}

export const settingRefundConditions = async (req: any, res: any, next: any) => {
    try {
        const activeCondition = req.body.active_conditions

        if (!activeCondition) {
            res.error(responseCode.MISSING.name, req.__("controller.shop.missing_refund_condition", responseCode.MISSING.code))
            return;
        }
        const shop = req.user.shop
        const shopRefundCondition = REFUND_CONDITIONS.map((item: any) => {
            if (activeCondition.includes(item.code)) {
                item.active = true
            }
            else {
                item.active = false
            }
            return item
        })
        shop.refund_conditions = shopRefundCondition
        const shopUpdated = await shopRepo.update(shop._id, shop)
        res.success(shopUpdated)
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }

}

export const initShopCountry = async (req: any, res: any, next: any) => {
    try {
        const shop = req.user.shop;
        res.success(shop);
    } catch (error) {
        res.error(
            responseCode.SERVER.name,
            error.message,
            responseCode.SERVER.code
        );
    }
}

export const updateShopCountry = async (req: any, res: any, next: any) => {
    try {
        const country = req.body.shop_country ?? VIET_NAM;
        const user = req.user;
        const shop = await shopRepo.update(req.user.shop._id, {
            country: country
        })
        const addresses: any = await addressRepository.find({ accessible_id: user._id });

        if (country === KOREA) {
            // Check Address Shop Korea Exist
            const koreaShopAddress = addresses.filter(address => {
                return (
                    address?.state?.id === DEFAULT_KOREA_SHOP?.state?.id &&
                    address?.district?.id === DEFAULT_KOREA_SHOP?.district?.id &&
                    address?.ward?.id === DEFAULT_KOREA_SHOP?.ward?.id &&
                    address?.street?.trim() === DEFAULT_KOREA_SHOP?.street?.trim()
                )
            });

            let listPromise: any = addresses.map(address => {
                return new Promise(async (resolve, reject) => {
                    await addressRepository.update(address._id, {
                        is_pick_address_default: false,
                        is_return_address_default: false
                    });
                    resolve(true);
                })
            });
            await Promise.all(listPromise)

            if (koreaShopAddress.length) {
                await addressRepository.update(koreaShopAddress[0]._id, {
                    is_pick_address_default: true,
                    is_return_address_default: true
                });
            } else {
                const newAddress = { ...DEFAULT_KOREA_SHOP, accessible_id: user._id.toString(), }
                await addressRepository.create(newAddress);
            }
        }
        res.success(shop);
    } catch (error) {
        res.error(
            responseCode.SERVER.name,
            error.message,
            responseCode.SERVER.code
        );
    }
}