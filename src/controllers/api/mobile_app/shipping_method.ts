import { ShippingMethodRepo } from "../../../repositories/ShippingMethodRepo";

const shippingMethodRepo = ShippingMethodRepo.getInstance();

export const getAllMethods = async (req: any, res: any, next: any) => {
    const result = await shippingMethodRepo.getAllShippingMethod();
    res.success(result);
}
