import moment from "moment";
import responseCode from "../../../base/responseCode";
import { REQ_DATE_TIME_FORMAT } from "../../../base/variable";
import { ProductDTO } from "../../../DTO/Product";
import { ProductRepo } from "../../../repositories/ProductRepo";
import { PromotionProgramRepo } from "../../../repositories/PromotionProgramRepo";
import { cloneObj } from "../../../utils/helper";
import { getMinMax } from "../../serviceHandles/product";

const promotionProgramRepo = PromotionProgramRepo.getInstance()
const productRepo = ProductRepo.getInstance();

export const createPromotion = async (req: any, res: any, next: any) => {
    try {
        const { name, start_time, end_time, discount, limit_quantity, products } = req.body
        const { shop_id } = req
        const promotion = await promotionProgramRepo.create({
            name, start_time, end_time, discount, limit_quantity, products, shop_id
        })
        res.success()
        
        /**
         * Đưa phần này vào cronjob
         */
        // const productIdsUpdated = await promotionProgramRepo.updateProductList(products, promotion)

        // // Update promotion data to avoid some unexpected errors
        // if (!productIdsUpdated.length) {
        //     const updatePromotion = {
        //         ...promotion,
        //         is_valid: false,
        //         products: []
        //     }
        //     await promotionProgramRepo.update(promotion._id, updatePromotion)
        //     res.error(responseCode.SERVER.name, req.__("controller.promotion.invalid_product_list"), responseCode.SERVER.code)
        // }
        // else {
        //     const updatePromotion = {
        //         ...promotion,
        //         products: productIdsUpdated
        //     }
        //     await promotionProgramRepo.update(promotion._id, updatePromotion)
        //     res.success()
        // }
        // end
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const updatePromotion = async (req: any, res: any, next: any) => {
    try {
        const { name, start_time, end_time, discount, limit_quantity, products } = req.body
        const { shop_id } = req
        const { current_promotion } = req
        const currentProduct = current_promotion.products.map(id => id.toString())
        const removedProduct = currentProduct.filter(id => {
            return products.indexOf(id) < 0
        })

        const updatePriceOfRemovedProduct = promotionProgramRepo.backProductToOldPrice(removedProduct)
        // res.success()

        const promotion = await promotionProgramRepo.update(current_promotion._id, {
            name, start_time, end_time, discount, limit_quantity, products, shop_id
        })
        const productIdsUpdated = await promotionProgramRepo.updateProductList(products, promotion)
        
        // Update promotion data to avoid some unexpected errors
        // if(!productIdsUpdated.length) {
        //     const updatePromotion = {
        //         ...promotion,
        //         products: productIdsUpdated
        //     }
        //     await promotionProgramRepo.update(promotion._id, updatePromotion)
        //     res.error(responseCode.SERVER.name, req.__("controller.promotion.invalid_product_list"), responseCode.SERVER.code)
        // }
        // else {
        //     const updatePromotion = {
        //         ...promotion,
        //         products: productIdsUpdated
        //     }
        //     await promotionProgramRepo.update(promotion._id, updatePromotion)
        //     res.success()
        // }
        res.success()
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getProductValidForPromotion = async (req: any, res: any, next: any) => {
    try {
        const current = new Date()
        let { page, limit } = req.query
        if (!page) page = 1;
        if (!limit) limit = 20;
        const match = {
            $or: [
                {
                    $and: [
                        { start_time: { $lte: current } },
                        { end_time: { $gte: current } }
                    ]
                },
                { start_time: { $gt: current } }
            ]
        }
        const promotions = await promotionProgramRepo.find(match)
        const product_ids = promotions.reduce((ids: Array<any>, item) => ids.concat(item.products), [])
        let listProduct:any = await productRepo.getProductValidForPromotion(req.shop_id, product_ids, page, limit)
        listProduct = cloneObj(listProduct)
        const productDTO = listProduct.data.map(e => new ProductDTO(e).setUser(req.user));
        listProduct.data = productDTO.map(e => {
            let getProduct: any = e.toJSONWithBookMark();
            getProduct.price_min_max = getMinMax(getProduct.variants);
            delete getProduct.variants;
            return getProduct;
        });
        res.success(listProduct)
    } catch (error) {
        console.log(error);
        res.error(error.name, error.message, error.statusCode);
    }
}

export const endPromotionProgram = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id;
        const updateInfo = {
            end_time: new Date()
        }
        const result: any = await promotionProgramRepo.update(id, updateInfo);
        // The reset product sale task will be running on delay job every 1 minute, so don't handle it here
        res.success(result, responseCode.SUCCESS.name, responseCode.SUCCESS.code);
    } catch (error) {
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code);
    }
}

export const deletePromotionProgram = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id;
        await promotionProgramRepo.delete(id);
        // The reset product sale task will be running on delay job every 1 minute, so don't handle it here
        res.success(null, responseCode.SUCCESS.name, responseCode.SUCCESS.code);
    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    };
}

export const getDetailPromotionProgram = async (req: any, res: any, next: any) => {
    try {
        const id = req.params.id
        let promotion = await promotionProgramRepo.detailPromotionProgram(id)
        if (promotion.length) {
            promotion = promotion[0]
        }
        promotion = cloneObj(promotion)
        console.log(promotion);

        const productDTO = promotion.products.map(e => new ProductDTO(e).setUser(req.user));
        promotion.products = productDTO.map(e => {
            let getProduct: any = e.toJSONWithBookMark();
            getProduct.price_min_max = getMinMax(getProduct.variants);
            delete getProduct.variants;
            return getProduct;
        });
        res.success(promotion)
    } catch (error) {
        console.log(error)
        res.error(responseCode.SERVER.name, req.__("controller.error_occurs"), responseCode.SERVER.code)
    }
}

export const getlPromotionWithFilter = async (req: any, res: any, next: any) => {
    try {
        const { limit, page } = req.query;
        const filterType = req.query.filter_type;
        const shopId = req.shop_id;
        let promotions: any = await promotionProgramRepo.getPromotionsWithFilter(shopId, limit, page, filterType);
        promotions.data = promotions.data.map(promotion => {
            promotion = cloneObj(promotion)
            promotion.products = promotion.products.map(e => {
                e = new ProductDTO(e).setUser(req.user)
                let getProduct: any = e.toJSONWithBookMark();
                getProduct.price_min_max = getMinMax(getProduct.variants);
                delete getProduct.variants;
                return getProduct;
            });
            return promotion;
        })
        res.success(promotions);
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
    }
}