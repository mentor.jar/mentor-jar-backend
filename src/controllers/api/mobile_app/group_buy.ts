import responseCode from "../../../base/responseCode";
import ProductService from "../../serviceHandles/product/service";
import { ProductRepo as ProductRepository }from "../../../repositories/ProductRepo";
import { CategoryRepo as CategoryRepository }from "../../../repositories/CategoryRepo";
import { CategoryDTO } from "../../../DTO/CategoryDTO";
import { EN_LANG, KO_LANG, VN_LANG } from "../../../base/variable";
import { cloneObj, isMappable, ObjectId } from "../../../utils/helper";
import { ProductDTO } from "../../../DTO/Product";
import { GroupBuyService } from "../../serviceHandles/groupBuy/service";

const ProductRepo = ProductRepository.getInstance();
const CategoryRepo = CategoryRepository.getInstance(); 
const groupBuyService = GroupBuyService.getInstance();

export const createGroupBuy =  async (req ,res ,next) => {
    try {
        const { user } = req;
        const shop = await groupBuyService.create(user, req.body);
        res.success(shop)
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
    }
}

export const updateGroupBuy = async (req, res, next) => {
    try {
        const { user } = req;
        const id = req.params.id;
        const shop = await groupBuyService.update(user, req.body, id);
        res.success(shop)
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
    }
}

export const deleteGroupBuy = async (req ,res ,next) => {
    try {
        const id = req.params.id;
        await groupBuyService.delete(id);
        res.success(true, responseCode.SUCCESS.name, responseCode.SUCCESS.code)
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
    }
}

export const getGroupBuyProductInWeek = async (req ,res ,next) => {
    try {
        let { page = 1,limit = 10 } = req.query
        const result = await ProductService._.getGroupBuyProductInWeek(limit, page)
        res.success(result)
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
    }
}

export const getGroupBuyProductWithFilter = async(req: any, res: any) => {
    try {
        const { page = 1, limit = 10, type = '', category = '' } = req.query;
        const result = await ProductService._.getListProductGroupBuy(page, limit, type, category);

        res.success(result);
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
    }
}

export const getListCategoryHaveGroupBuyProduct = async (req: any, res: any) => {
    try {
        // dynamic language
        const defaultCategory = [
            {
                _id: '', 
                name: 'Tất cả',
                description: null
            }
        ];
        const { type = 'RUNNING' } = req.query;

        const listCategoriesId = await ProductRepo.getCategoriesHaveGroupBuyFromProduct(type);
        if(!listCategoriesId.length) {
            return  res.success(defaultCategory);   
        } 
        const categories = await CategoryRepo.getCategoriesInfo(listCategoriesId)

        const result = categories.map(c => new CategoryDTO(c).toRequestJSON());
        res.success([...defaultCategory, ...result]);
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code);
    }
}

export const getValidGroupBuyProductByShopId = async (req: any, res: any, next: any) => {
    try {
        let { page = 1, limit = 10 , group_buy_id } = req.query
        let products: any = []
        const shopID = req.shop_id.toString()
        if (isMappable(req.user.shop.group_buy_list)) {
            req.user.shop.group_buy_list.map(value => {
                if(value.status === "pending") {
                    value.products.map(item => {
                        products.push(ObjectId(item._id))
                    })
                }
            })
        }

        let result: any = await ProductRepo.getValidGroupBuyProductByShopId(shopID, page, limit, products, true, group_buy_id)
        result = cloneObj(result)
        result.data = result.data.map(product => {
            let item:any = new ProductDTO(product);
            item = item.getProductComplete(req.user)
            // item.sold = product.order_items && product.order_items.length ? product.order_items[0].sold : 0
            item.discount_percent = item.discount_percent ? item.discount_percent.discount : 0
            delete item.order_items
            return item
        });
        res.success(result)
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
    }
}