import { VN_LANG } from "../../../base/variable";
import { CategoryRepo } from "../../../repositories/CategoryRepo"
import { ProductRepo } from "../../../repositories/ProductRepo";
import { handleTimeReq, handleType } from '../../handleRequests/shopStatistic/userTrends';
import generalAnalystService from "../../serviceHandles/shopStatistic/generalAnalyst";
import productRankingService, { ProductRankingType } from "../../serviceHandles/shopStatistic/productRanking";
import userTrendService, { UserTrendType } from '../../serviceHandles/shopStatistic/userTrends';


const categoryRepo = CategoryRepo.getInstance();
const productRepo = ProductRepo.getInstance();
export const getListSystemCategoriesOfShop = async (req: any, res: any, next: any) => {
    try {
        const language = req.headers["accept-language"] || VN_LANG
        const listIdCategory = await productRepo.getListIdCategoryThatShopUse(req.shop_id);
        const result = await categoryRepo.getCategoriesShopStatistics(listIdCategory, language);
        res.success(result);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const getUserTrends = async (req: any, res: any, next: any) => {
    try {
        const { start_time, end_time } = handleTimeReq(req);
        const { trend_type, sort_direction } = req.query

        const request = {
            start_time,
            end_time,
            shop_id: req.shop_id,
            sort_direction: sort_direction == 'asc' ? 1 : -1
        }
        let user_trends = {};
        switch (trend_type) {
            case UserTrendType.AGE:
                user_trends = await userTrendService.userTrendByAge(request);
                break;
            case UserTrendType.GENDER:
                user_trends = await userTrendService.userTrendByGender(request);
                break;
            case UserTrendType.REGULAR_CUSTOMER:
                user_trends = await userTrendService.userTrendByRegularCustomer(request);
                break;
            case UserTrendType.LIVE_STREAM:
                user_trends = await userTrendService.userTrendByStream(request);
                break;
            default: break;
        }

        res.success(user_trends)
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}

export const generalStatistics = async (req: any, res: any, next: any) => {
    try {
        const { start_time, end_time } = handleTimeReq(req);
        const [indicator, chart] = await Promise.all([
            (async () => {
                return generalAnalystService.callIndexGeneral(req.shop_id, start_time, end_time);
            })(),
            (async () => {
                return generalAnalystService.chart(req.shop_id, start_time, end_time);
            })()
        ])
        res.success({ indicator, chart });
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}
export const productRanking = async (req: any, res: any, next: any) => {
    try {
        const { start_time, end_time } = handleTimeReq(req);
        const { ranking_type, sort_direction, category_id, limit, page } = req.query
        const request = {
            start_time,
            end_time,
            shop_id: req.shop_id,
            sort_direction: sort_direction == 'asc' ? 1 : -1,
            category_id,
            limit: +limit || 3,
            page: +page || 1,
        }
        let user_trends = {};
        switch (ranking_type) {
            case ProductRankingType.REVENUE:
                user_trends = await productRankingService.revenue(request);
                break;
            case ProductRankingType.SOLD_AMOUNT:
                user_trends = await productRankingService.soldAmount(request);
                break;
            case ProductRankingType.VIEW_AMOUNT:
                user_trends = await productRankingService.viewAmount(request);
                break;
            default: break;
        }

        res.success(user_trends)
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}
