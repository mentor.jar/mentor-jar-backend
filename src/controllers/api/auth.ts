import { UserRepo } from "../../repositories/UserRepo";

let userRepo = new UserRepo();
export const login = async (req, res, next) => {
    try {
        const category = await userRepo.loginDefault(req.body.email, req.body.password);
        res.success(category);
    } catch (error) {
        res.error(error.name, error.message, error.statusCode);
    }
}