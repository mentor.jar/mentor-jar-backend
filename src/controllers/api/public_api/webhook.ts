import { executeError } from "../../../base/appError";
import GHTKService from "../../../services/3rd/shippings/GHTK";
import { WebhookError } from '../../../base/customError';
import { OrderRepository } from "../../../repositories/OrderRepo";
import { ShopRepo } from "../../../repositories/ShopRepo";
import OrderShippingService from "../../serviceHandles/orderShipping/orderShipping";
import ViettelPostService from "../../../services/3rd/shippings/viettelpost/ViettelPost";
import { ShippingMethodQuery } from "../../../models/enums/order";

const shopRepository = ShopRepo.getInstance();

export const updateStatusOrderGHTK = async (req: any, res: any, next: any) => {
  try {

    res.status(200);

    const result = await OrderShippingService._.handlerUpdateHistoryShipping(req.body);
    if (!result) throw new WebhookError('WEBHOOK_ERROR');
    return res.success(null, 'OK');
  } catch (error) {
    error = executeError(error);
    res.error(error.name, error.message, error.statusCode);
  }
}

export const updateStatusOrderViettelPost = async (req: any, res: any, next: any) => {
  try {

    res.status(200);
    const result = await OrderShippingService._.handlerUpdateHistoryMutilShipping(req.body, ShippingMethodQuery.VIETTELPOST);
    return res.success(null, 'OK');
  } catch (error) {
    console.log(error)
    error = executeError(error);
    res.error(error.name, error.message, error.statusCode);
  }
}
