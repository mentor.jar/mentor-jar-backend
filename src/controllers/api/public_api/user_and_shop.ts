import { executeError } from "../../../base/appError";
import { InMemoryUserStore } from "../../../SocketStores/UserStore";

const userCache = InMemoryUserStore.getInstance();

export const getUserAndShop = async (req, res, next) => {
    try {
        const result = await userCache.get("userAndShops");
        res.success(result)
    } catch (error) {
        error = executeError(error);
        res.error(error.name, error.message, error.statusCode);
      }
}