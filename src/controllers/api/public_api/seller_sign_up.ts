import responseCode from "../../../base/responseCode"
import { SENDGRID_TEMPLATE_ID, URL_APPROVE_SELLER_PRODUCTION, URL_APPROVE_SELLER_STAGING } from "../../../base/variable";
import { SellerSignUpRepo } from "../../../repositories/SellerSignUpRepo"
import { ShopRepo } from "../../../repositories/ShopRepo";
import { UserRepo } from "../../../repositories/UserRepo";
import { sendEmail } from "../../../services/api/sellerSignUp";
import { cloneObj, ObjectId, phoneNormalize } from "../../../utils/helper";

const repository = SellerSignUpRepo.getInstance();
const userRepo = UserRepo.getInstance();
const shopRepo = ShopRepo.getInstance();

export const registerSeller = async (req: any, res: any, next: any) => {
    try {
        const {personal_info, shop_info, bank_info} = req.body
        const { user } = req
        let phoneNumber = phoneNormalize(personal_info.phone_number)
        personal_info.phone_number = phoneNumber
        let usersMapped = []

        const register:any = {
            personal_info,
            shop_info,
            bank_info
        }
        if(!shop_info.social_business.includes(req.__("seller.no")) && !shop_info?.social_link_info){
            res.error(responseCode.BAD_REQUEST.name, req.__("seller.missing_social_link_info"), responseCode.BAD_REQUEST.code)
            return;
        }
        if(user) {
            const shop:any = await shopRepo.findOne({user_id: ObjectId(user?._id)})
            if(shop) {
                res.error(responseCode.BAD_REQUEST.name, req.__("seller.shop_exists"), responseCode.BAD_REQUEST.code)
                return;
            }
            let shopUser: any = await shopRepo.findOrCreateByUser(user, personal_info.country, true);
            shopUser = cloneObj(shopUser)
            shopRepo.actionAfterCreateShop(user._id, shopUser?._id, personal_info.country);
            usersMapped = [ObjectId(user._id)]
            register.user_mapped = usersMapped
            register.shop_id = ObjectId(shopUser._id)
        }
        else {
            const email = personal_info.email
            const users = await userRepo.find({
                $or: [
                    {phoneNumber: phoneNumber},
                    {email: email}
                ]
            })
            usersMapped = users.map(user => ObjectId(user._id))
            register.user_mapped = usersMapped
            
        }
        const result :any = await repository.create(register)
        let button_url = ""
        if(process.env.NODE_ENV === "production"){
            button_url = URL_APPROVE_SELLER_PRODUCTION + result._id
        }
        else{
            button_url = URL_APPROVE_SELLER_STAGING + result._id
        }
        let category = result.shop_info.categories.join(', ')
        let addressInfo :any = result.shop_info.address?.street + ", " + result.shop_info.address?.ward?.Name + ", " +
        result.shop_info.address?.district?.name + ", " + result.shop_info.address?.state?.name
        let address :any = result.shop_info.address ? addressInfo : "Không có"
        let email :any = result.personal_info.email
        let dynamic_template_data :any = {
            "user_name": result.personal_info.full_name,
            "category_name": category,
            "phone_number": result.personal_info.phone_number,
            "email": email,
            "address": address,
            "created_at": result.created_at,
            "social_link_info": result.shop_info.social_link_info || "Không có",
            "button_url": button_url
        }
        sendEmail(email, dynamic_template_data, SENDGRID_TEMPLATE_ID)
        res.success(responseCode.SUCCESS.name, req.__("seller.register_success"), responseCode.SUCCESS.code)
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
        return;
    }
}