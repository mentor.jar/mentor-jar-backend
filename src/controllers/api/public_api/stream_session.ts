import { executeError } from "../../../base/appError";
import { StreamSessionDTO } from "../../../DTO/StreamSessionDTO";
import { StreamSessionRepository } from "../../../repositories/StreamSessionRepository";

let streamSessionRepository = StreamSessionRepository.getInstance();

export const getStreamSession = async (req: any, res: any, next: any) => {
    try {
        let streamSession = await streamSessionRepository.getStreamSession(req.params.id);
        streamSession = StreamSessionDTO.newInstance(streamSession).completeDataWithDTO(req.user);
        res.success(streamSession);
    } catch (error) {
        executeError(error);
        res.error(error.name, error.message, error.statusCode);
    }
}



