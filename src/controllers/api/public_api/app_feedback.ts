import responseCode from "../../../base/responseCode";
import { AppFeedbackRepo } from "../../../repositories/AppFeedbackRepo";

const appFeedbackRepo = AppFeedbackRepo.getInstance();

export const createAppFeedback = async (req: any, res: any, next: any) => {
    try {
        const { full_name, email, how_you_know_bidu, satisfied_with_bidu_mark, 
            ui_mark, shop_config_mark, product_manage_mark, livestream_mark, order_manage_mark, opinion } = req.body
        const user_id = req.user ? req.user?._id : null
        const appFeedback = {
            full_name, email, how_you_know_bidu, satisfied_with_bidu_mark, 
            ui_mark, shop_config_mark, product_manage_mark, livestream_mark, order_manage_mark, opinion, user_id
        }
        await appFeedbackRepo.create(appFeedback)
        res.success(responseCode.SUCCESS.name, req.__("app_feedback.create_success"), responseCode.SUCCESS.code)
    } catch (error) {
        res.error(responseCode.SERVER.name, error.message, responseCode.SERVER.code)
        return;
    }
}