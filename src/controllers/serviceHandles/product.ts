import { FeedbackRepo } from "../../repositories/FeedbackRepo";
import { ProductRepo } from "../../repositories/ProductRepo";
import CacheService from "../../services/cache";
import { cloneObj, ObjectId } from "../../utils/helper";
import { addLink } from "../../utils/stringUtil";
import { VoucherRepository } from "../../repositories/VoucherRepository";
import keys from "../../config/env/keys";
import { UserDTO } from "../../DTO/UserDTO";
import { UserRepo } from "../../repositories/UserRepo";
import { PromotionProgramRepo } from "../../repositories/PromotionProgramRepo";
import { OrderRepository } from "../../repositories/OrderRepo";
import { OVERSEAS_SHIPPING_FEE_BY_GAM, VIET_NAM, COSTS_INCURRED } from "../../base/variable";
import { getBodyShapeMark, percentRound } from "../../utils/utils";
import { CategoryInfoRepo } from "../../repositories/CategoryInfoRepo";
import { ProductDetailInfoRepo } from "../../repositories/ProductDetailInfo";
import { BodyShapeRepo } from "../../repositories/BodyShapeRepo";
import { ShopRepo } from "../../repositories/ShopRepo";
import { CategoryRepo } from "../../repositories/CategoryRepo";
import { InMemoryFeedbackStore } from "../../SocketStores/FeedbackStore";
import { KEY } from "../../constants/cache";
import { InMemoryPrepareOrderStore } from "../../SocketStores/PrepareOrderStore";
import { InMemoryProductStore } from "../../SocketStores/ProductStore";
import { ElasticCommand, elsIndexName } from "../../services/elastic";

const repository = ProductRepo.getInstance()
const feedbackRepo = FeedbackRepo.getInstance();
const voucherRepo = VoucherRepository.getInstance();
const userRepo = UserRepo.getInstance();
const promotionRepo = PromotionProgramRepo.getInstance();
const orderRepo = OrderRepository.getInstance();
const categortInfoRepo = CategoryInfoRepo.getInstance();
const productDetailRepo = ProductDetailInfoRepo.getInstance();
const shopRepo = ShopRepo.getInstance();
const bodyShapeRepo = BodyShapeRepo.getInstance();
const categoryRepo = CategoryRepo.getInstance();
const feedbackStore = InMemoryFeedbackStore.getInstance();
const prepareOrderStore = InMemoryPrepareOrderStore.getInstance();
const productStore =  InMemoryProductStore.getInstance();
const elasticCommand = ElasticCommand.getInstance();

export function getMinMax(arr: Array<any>) {
	let minMax: any = {
		min: null,
		max: null
	}
	arr.map(e => {
		minMax.min = minMax.min === null
			? e.sale_price
			: Math.min(minMax.min, e.sale_price);
		minMax.max = minMax.max === null
			? minMax.max = e.sale_price
			: Math.max(minMax.max, e.sale_price);
	})
	return minMax
}

// DESC
export const sortByTime = (objectA, objectB) => {
	const timeA: any = new Date(objectA), timeB: any = new Date(objectB)
	return timeB - timeA;
}

export const resetCacheAfterProductChange = async (productID) => {
	productID = productID.toString()
	const cacheService = CacheService._
	const suggestProduct = cacheService.get('home_suggest_product') || []
	const newestProduct = cacheService.get('home_newest_product') || []
	const topProduct = cacheService.get('home_top_product') || []

	if (
		suggestProduct.find(item => item._id === productID)
	) {
		cacheService.delete('home_suggest_product')
	}

	if (
		newestProduct.find(item => item._id === productID)
	) {
		cacheService.delete('home_newest_product')
	}

	if (
		topProduct.find(item => item._id === productID)
	) {
		cacheService.delete('home_top_product')
	}

	productStore.deleteById(productID);

	await elasticCommand.deleteById(elsIndexName.product, productID);
}

export const detailProductFromBuyer = async (productID, userReq = null, language=null) => {
	try {
		let productIDString = productID;
		if (productID && typeof productID !== 'string' ) {
			productIDString = productID.toString();
		}
		
		productID = ObjectId(productID)
		let [product, feedback, promotion] = await Promise.all([
			(async () => {
				const data = await repository.getDetailWhenUserGoToShopExplore(productID, userReq, language);
				return data
			})(),
			(async () => {
				const feedbackGetDB = await feedbackRepo.getFeedbackByProductID(productID, { option: 'TYPE', value: 'ALL' }, { limit: 3, page: 1 })
				return feedbackGetDB;
			})(),
			(async () => {
				const data = await promotionRepo.getPromotionByProduct(productID)
				return data
			})()
		])

		product = cloneObj(product)
		product.shop.user = new UserDTO(product.shop.user).toSimpleUserInfo()

		let { generalFeedbackInfo, data } = feedback
		data = data.map(item => {
			item = cloneObj(item)
			item.user.avatar = addLink(`${keys.host_community}/`, item.user.gallery_image?.url)
			item.user = new UserDTO(item.user).toSimpleUserInfo();
			return item
		})

		let [user, vouchers, timePrepareOfShop] = await Promise.all([
			(async () => {
				const data = await userRepo.getUserByShopId(product.shop_id);
				return data
			})(),
			(async () => {
				const data = await voucherRepo.getShopVoucherInProductDetail(productID, product.shop._id);
				return data
			})(),
			(async () => {
				const shopId = product.shop._id.toString();
				const prepareOrderCache = await prepareOrderStore.get(KEY.STORE_PREPARE_ORDER, shopId);
				if(prepareOrderCache) {
					return prepareOrderCache
				} else {
					const prepareOrderGetDB = await orderRepo.getTimePrepareOrder(product.shop._id)
					prepareOrderStore.set(KEY.STORE_PREPARE_ORDER, shopId, prepareOrderGetDB )
					return prepareOrderGetDB;
				}
			})()
		])

		user.avatar = addLink(`${keys.host_community}/`, user.gallery_image?.url)
		product.user = new UserDTO(user).toSimpleJSON();

		product.feedbacks = {
			...generalFeedbackInfo,
			feedbacks: data
		}
		// Voucher of Shop
		product.vouchers = vouchers;

		if (!product.custom_images || !product.custom_images?.length) {
			product.custom_images = product.images.map(url => {
				return {
					url,
					x_percent_offset: null,
					y_percent_offset: null
				}
			})
		}

		// max voucher discount
		let maxVoucherDiscount = 0
		vouchers.forEach(voucher => {
			let expectedDiscount = 0
			if (voucher.type === "price") {
				expectedDiscount = Math.min(voucher.value || voucher.max_discount_value) || 0
			}
			else if (voucher.type === "percent") {
				expectedDiscount = Math.min(+(voucher.value * product.sale_price / 100).toFixed(0) || voucher.max_discount_value) || 0
			}

			if (maxVoucherDiscount < expectedDiscount) {
				maxVoucherDiscount = expectedDiscount
			}
		})
		product.max_voucher_discount = maxVoucherDiscount

		// Discount percent
		product.discount_percent = 0
		if (promotion && (product.sale_price !== product.before_sale_price || product.before_sale_price === 0)) {
			product.discount_percent = promotion.discount
		}

		// analyst time prepare order
		const dayToAnalyst = [1, 2, 3, 4]

		// Shipping expected price
		const [minGHTKFee, maxGHTKFee] = [16500, 60000]
		const expectedShippingFee: any = []
		if (product.shop.country === VIET_NAM) {
			expectedShippingFee.push({
				name: "GHTK",
				min: minGHTKFee,
				max: maxGHTKFee
			})
		}
		else {
			const koShippingFee = product.weight ? +product.weight * OVERSEAS_SHIPPING_FEE_BY_GAM + COSTS_INCURRED : COSTS_INCURRED
			expectedShippingFee.push({
				name: "GHTK",
				min: minGHTKFee + koShippingFee,
				max: maxGHTKFee + koShippingFee
			})
		}

		// simillar product
		let simillarProducts = []
		product.simillarProducts = simillarProducts


		// suggest product
		let productSuggest = []
		product.suggestProduct = productSuggest


		const totalMoreThanFourDays = timePrepareOfShop.reduce((sum, item) => {
			return sum + (item.day > 4 ? item.count : 0)
		}, 0)

		timePrepareOfShop = dayToAnalyst.map((day) => {
			const filterDay = timePrepareOfShop.filter(item => item.day === day)[0]
			return filterDay ? filterDay : { day, count: 0 }
		})
		timePrepareOfShop.push({
			day: "> 4",
			count: totalMoreThanFourDays
		})
		const arrPercent = percentRound(timePrepareOfShop.map(ele => ele.count), 3)
		product.time_prepare_orders = timePrepareOfShop.map((item, index) => {
			item.day = item.day.toString()
			item.value = arrPercent[index]
			item.unit = "%"
			delete item.count
			return item
		})

		product.expected_shipping_fee = expectedShippingFee

		return product
	} catch (error) {
		throw new Error(error)
	}
}

export const updateProductBodyShapeMark = async () => {
	try {
		const bodyShapeCategoryInfo = await categortInfoRepo.find({
			"name.vi": "Dáng người"
		})
		const cateInfoIDs = bodyShapeCategoryInfo.map(item => item._id)

		const productCateInfo = await productDetailRepo.find({
			category_info_id: { $in: cateInfoIDs }
		})
		const validDetailInfos = productCateInfo.filter(info => {
			return info.value
		})
		const productIDs = validDetailInfos.map(item => item.product_id)

		// get active shop
		const activeShops = await shopRepo.find({
			is_approved: true
		})
		const shopIDs = activeShops.map(shop => shop._id)

		// get valid products
		const products = await repository.find({
			is_approved: 'approved',
			quantity: { $gt: 0 },
			deleted_at: null,
			allow_to_sell: true,
			_id: { $in: productIDs },
			shop_id: { $in: shopIDs }
		})
		const bodyShapes = await bodyShapeRepo.find({})
		const promises = products.map(product => {
			return new Promise(async (resolve, reject) => {
				try {
					product = cloneObj(product)
					const detailItem = validDetailInfos.find(item => item.product_id.toString() === product._id.toString())
					if (detailItem) {
						const value = detailItem.value.toUpperCase()
						const bodyShapeMark = getBodyShapeMark(value, bodyShapes)
						product = await repository.update(product._id, {
						  body_shape_mark: bodyShapeMark,
						  is_suggested: true
						})
						resolve(product._id)
					}
					else {
						resolve(null)
					}
				} catch (error) {
					reject(error)
				}
			})
		})

		const productCompletedIDs = await Promise.all(promises).then(data => {
			return data.filter(id => id)
		}).catch(error => {
			console.log("error: ", error.message);

			throw new Error(error)
		})

		const appendpopularProduct = await repository.updateSuggestList(productCompletedIDs, 400 - productCompletedIDs.length)
	} catch (error) {
		throw new Error(error)
	}
}

export const topProductByCategory = async () => {
	try {
		const categories = await categoryRepo.find({is_active: true})
		const promises = categories.map(cate => {
			return new Promise(async (resolve, reject) => {
				try {
					let result: any = await repository.findProductHome({ orderBy: 'best_sell', categoryID: cate._id.toString() }, 1, 5)
					const topProductByCategory = cloneObj(result?.data)
					resolve({
						categoryID: cate._id,
						topProducts:  topProductByCategory
					})
				} catch (error) {
					reject(error)
				}
			})
		})
		const data = await Promise.all(promises).then(data => data).catch(error => {
			throw new Error(error)
		})
		return data
	} catch (error) {
		throw new Error(error)
	}
}