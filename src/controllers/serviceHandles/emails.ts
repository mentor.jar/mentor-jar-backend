import keys from "../../config/env/keys"
import { EmailListRepo } from "../../repositories/EmailListRepo";

const emailListRepo = EmailListRepo.getInstance();
const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(keys.sendgrid_api_key)

export const sendEmailMKTFromList = async (targets, dynamic_template_data, template) => {
    try {
        if(process.env.NODE_ENV !== 'production') {
            targets = [
                // {
                //     email: "jude@mjartgroup.com",
                //     user_name: 'Tri Nguyen',
                //     user_id: null,
                //     shop_id: null
                // },
                {
                    email: "ducphucit1202@gmail.com",
                    user_name: 'Otis',
                    user_id: null,
                    shop_id: null
                },
                {
                    email: "nhauyen96yag@gmail.com",
                    user_name: 'Uyen QA',
                    user_id: null,
                    shop_id: null
                },
                {
                    email: "thanhthuyqth96@gmail.com",
                    user_name: 'Xu Xu QA',
                    user_id: null,
                    shop_id: null
                },
                {
                    email: "ntdieulinh3199@gmail.com",
                    user_name: 'Linh QA',
                    user_id: null,
                    shop_id: null
                },
            ]
        }
        const promises = targets.map(item => {
            return new Promise(async (resolve, reject) => {
                try {
                    const msg: any = {
                        to: item.email,
                        from: "biducommunity@gmail.com",
                        templateId: template.sendgrid_template_id,
                        dynamic_template_data: {
                            ...dynamic_template_data,
                            target_name: item.user_name
                        }
                    }
            
                    const data = await sgMail
                        .send(msg)
                        .then(data => data)
                        .catch((error) => {
                            console.error(error)
                            throw new Error(error)
                        })
                    resolve({
                        target: item.email,
                        success: data[0]?.statusCode >= 200 && data[0]?.statusCode < 300,
                        message: data[0]?.body?.toString()
                    })
                } catch (error) {
                    resolve({
                        target: item.email,
                        success: false,
                        message: error.message
                    })
                }
            })
        })
        await Promise.all(promises).then(data => {
            emailListRepo.create({
                name: template.name,
                status: data,
                sendgrid_template_id: template.sendgrid_template_id,
                data: dynamic_template_data
            })
        })
    } catch (error) {
        throw new Error(error)
    }
}