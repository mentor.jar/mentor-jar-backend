import { ProductDTO } from '../../../DTO/Product';
import { ProductRepo } from '../../../repositories/ProductRepo';
import { getMinMax } from '../product';
import { GlobalSearchProductRequest } from './definition';
import { SearchKeywordRepo } from '../../../repositories/SearchKeywordRepo';
import { ProductElsDTO } from '../../../DTO/ElasticDTO/ProductDTO';
import { Pagination } from '../../../utils/paginate';
import { ElasticQuery, elsIndexName } from '../../../services/elastic';
import { VN_LANG } from '../../../base/variable';
const productRepo = ProductRepo.getInstance();
const searchKeywordRepo = SearchKeywordRepo.getInstance();
class HomeService {

  private static _instance: HomeService;

  static get _() {
    if (!this._instance) {
      this._instance = new HomeService();
    }
    return this._instance;
  }


  /**
  * search product of shop explore
  * 
  * @param request 
  *  - orderBy: String 
  *  - criteria: String
  */
  async searchExplore(request: GlobalSearchProductRequest, limit, page, user: any) {
    const dataBundle = await productRepo.searchProductForExplore(request, page, limit);

    let results: any = null;
    let productDTO = dataBundle.data.map(e => new ProductDTO(e));

    results = productDTO.map(e => {
      e.setUser(user);
      let getProduct: any = e.toJSONWithBookMark();
      let getProductOption: any = e.toJSON(['variants']);
      //Handle variants field
      let variantsDTO = e.getProductVariant(getProductOption.variants);
      getProduct.variants = [];
      variantsDTO.map(e => {
        let varianJson = e.toJSON(['_id', 'option_values', 'before_sale_price', 'sale_price', 'quantity', 'is_master'])
        getProduct.variants.push(varianJson);
      });
      getProduct.price_min_max = getMinMax(getProduct.variants);
      delete getProduct.variants;
      return getProduct;
    });
    dataBundle.data = results;

    if (request.keyword) searchKeywordRepo.insertOrUpdate(request.keyword)

    return dataBundle;
  }

  async homeSearchV2(req, language = VN_LANG) {
    const { limit = 10, page = 1, keyword, category, shop_id, size_type } = req.query;
    let size: any = ""
    if(size_type) {
        size = size_type.split("-")
    }

    let boolQuery: any = this.buildQuerySearchProduct({
        category,
        shop_id,
        keyword,
        size
    });

    let data = await ElasticQuery.getInstance().search(
        elsIndexName.product,
        boolQuery,
        {
            from: (page-1) * limit,
            size: limit,
        }
    );
    let productsDTO = data.record.map((e) => {
        let product = new ProductElsDTO(e).get();
        product = new ProductDTO(product).toSimpleJSON();
        product.product_detail_infos = product?.product_detail_infos?.map(
            (item) => {
                item.name = item.name[language];
                return item;
            }
        );
        product.sold = product?.sold < 10 ? 0 : product?.sold;

        return product;
    });

    const resultPaginate = new Pagination(productsDTO).pagination(
        parseInt(page),
        parseInt(limit),
        data.total.value
    );
    return resultPaginate;
  }

  buildQuerySearchProduct({ keyword, category, shop_id, size }) {
    let boolQuery: any = {};
    let mustQuery: any = [
        {
            match: {
                is_approved: "approved"
            }
        },
        {
            term: {
                allow_to_sell: true
            }
        },
        {
            term: {
                is_sold_out: false
            }
        },
        {
            term: {
                "shop.is_approved": true
            }
        },
        {
            term: {
                "shop.pause_mode": false
            }
        },
        {
            range: {
                quantity: {
                  gt: 0,
                }
              }
        }
    ];
    let shouldQuery: any = [];
    if (keyword) {
        shouldQuery.push(
            {
                match: {
                    name: keyword,
                },
            },
            {
                match: {
                    description: keyword,
                },
            }
        );
    }

    if (category) {
        mustQuery.push(
            {
                match: {
                    category_id: category,
                },
            },
            {
                terms: {
                    list_category_id: [category],
                },
            }
        );
    }

    if (shop_id) {
        mustQuery.push({
            match: {
                shop_id: shop_id,
            },
        });
    }

    if(size) {
        mustQuery.push({
            terms: {
                "option_types.option_values.name.keyword": size
            }
        })
    }
    boolQuery.should = shouldQuery;
    boolQuery.must = mustQuery;
    boolQuery.must_not = [
        {
            exists: {
              "field": "deleted_at"
            }
        }
    ]
    return { bool: boolQuery };
  }
}
export default HomeService;