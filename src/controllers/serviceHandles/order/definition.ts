import { ShippingStatus } from "../../../models/enums/order";

export const defaultCountValues = [
    {
        "_id": ShippingStatus.PENDING,
        "count": 0
    },
    {
        "_id": ShippingStatus.RETURN,
        "count": 0
    },
    {
        "_id": ShippingStatus.SHIPPED,
        "count": 0
    },
    {
        "_id": ShippingStatus.SHIPPING,
        "count": 0
    },
    {
        "_id": ShippingStatus.CANCELED,
        "count": 0
    },
    {
        "_id": ShippingStatus.CANCELING,
        "count": 0
    },
    {
        "_id": ShippingStatus.WAIT_TO_PICK,
        "count": 0
    }
];

export const listShippingStatus = [
    ShippingStatus.PENDING,
    ShippingStatus.RETURN,
    ShippingStatus.SHIPPED,
    ShippingStatus.SHIPPING,
    ShippingStatus.CANCELED,
    ShippingStatus.CANCELING,
    ShippingStatus.WAIT_TO_PICK,
];