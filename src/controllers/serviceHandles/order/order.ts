import { OrderShippingDTO } from "../../../DTO/OrderShippingHistoryDTO";
import { ShippingMethodQuery, ShippingStatus } from "../../../models/enums/order";
import { OrderRepository } from "../../../repositories/OrderRepo";
import { OrderShippingRepo } from "../../../repositories/OrderShippingRepo";
import { ShippingMethodRepo } from "../../../repositories/ShippingMethodRepo";
import GHTKService from "../../../services/3rd/shippings/GHTK";
import OrderShippingService from "../orderShipping/orderShipping";
import { REFUND_CONDITIONS, REFUND_REJECT_CONDITIONS } from "../../../base/variable";
import { OrderItemRepository } from "../../../repositories/OrderItemRepo";

const orderRepo = OrderRepository.getInstance();
const orderShippingRepo = OrderShippingRepo.getInstance();
const shippingMethodRepo = ShippingMethodRepo.getInstance();
const orderItemRepo = OrderItemRepository.getInstance();
class OrderService {

  private static _instance: OrderService;

  static get _() {
    if (!this._instance) {
      this._instance = new OrderService();
    }
    return this._instance;
  }

  saveShippingInfoToOrderShipping = async (order, orderResult, shippingMethod) => {
    const orderShipingRepo = await OrderShippingRepo.getInstance();
    switch (shippingMethod.name_query) {
      case ShippingMethodQuery.GIAOHANGTIETKIEM:
        const ghtkService = await GHTKService.getInstance();
        orderResult.shipping_status = ghtkService.getStatusTextOrder(orderResult.status_id);
        break;
      case ShippingMethodQuery.VIETTELPOST:
        // handler status shipping if any
        break;
      default:
        break;
    }
    // send notification 
    // await OrderShippingService._.sendNotificationWithStatus(order, orderResult);
    await orderShipingRepo.updateOrderShipping(order._id, orderResult);
    return;
  }

  getShippingHistory = async (orderId, shop) => {
    const order = await orderRepo.findById(orderId);
    const orderHistory = await orderShippingRepo.getShippingHistoryByOrderId(orderId);
    const shipping = await shippingMethodRepo.getShippingMethodForShop(shop, order.shipping_method_id);

    const result = OrderShippingDTO.newInstance(orderHistory).toCompeleteJSON(order, shipping.name, orderHistory);
    return result;
  }

  calculatingRefundMoneyByItem = (item, total_value_items, vouchers) => {
    try {
      const { variant, product, quantity } = item
      const price = variant ? variant.sale_price : product.sale_price
      const itemPrice = price * quantity
      const itemRatio = itemPrice / total_value_items > 1 ?  1.00 : +(itemPrice / total_value_items).toFixed(2)
      
      const voucherDiscount = vouchers && vouchers.length ? 
      vouchers.filter(item => !item.classify || (item.classify !== "free_shipping" && item.classify !== "cash_back"))
              .reduce((sum, item) => sum += item.discount, 0) || 0 : 0
      return +(itemPrice - itemRatio * voucherDiscount).toFixed(0)
    } catch (error) {
      throw new Error(error.message);
    }
  }

  getListReasons = () => {
    try {
      return REFUND_CONDITIONS;
    } catch (error) {
      throw new Error(error.message);
    }
  }

  getListRejectReasons = () => {
    try {
      return REFUND_REJECT_CONDITIONS;
    } catch (error) {
      throw new Error(error.message);
    }
  }

  updateWeightForOrderItem = async (products) => {
    try {
      let orderItemFinal:any = [];
      const orderItemsWillUpdate = products.map( async (product:any) => {
        const { id, weight } = product
        const orderItemContainProduct =  await orderItemRepo.getOrderItemUnprocessed(id);

        if (orderItemContainProduct) {
          const orderItemContainProductShortened = orderItemContainProduct.map((item) => {
            const orderItemID = item._id;
            const order = item.order;
            return { orderItemID, weight, order}
          });
          
          return orderItemContainProductShortened
          
        } else {
          return {}
        }
      })
      
      const orderItemAggregate = await Promise.all(orderItemsWillUpdate)
      orderItemAggregate.map((element:any) => {
        orderItemFinal.push(...element)
      })

      orderItemFinal.map((orderItem: any) => {
        if (Array.isArray(orderItem.order) && orderItem.order.length > 0){
           orderItemRepo.update(orderItem.orderItemID, {"product.weight": orderItem.weight})
        }
      })

    } catch (error) {
      throw new Error(error.message);
    }
  }
}

export default OrderService;