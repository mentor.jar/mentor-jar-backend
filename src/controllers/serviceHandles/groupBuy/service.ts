
import { ShopRepo } from "../../../repositories/ShopRepo";
import { ProductRepo } from "../../../repositories/ProductRepo";
import { cloneObj, isMappable } from "../../../utils/helper";
import { requestCreateGroupBuy, requestGroupBuyProduct } from "../../handleRequests/groupBuy/request";
import UserService from "../user";

const shopRepo = ShopRepo.getInstance();
const productRepo = ProductRepo.getInstance();
const userService = UserService._;

export class GroupBuyService {
    private static _instance: GroupBuyService;

    static getInstance() {
        if (!this._instance) {
            this._instance = new GroupBuyService();
        }
        return this._instance;
    }

    async create(user, request) {
        try {
            const { shop_id: shopId, _id: userId } = user;
            const { objectRequestComplete } = requestCreateGroupBuy(request);

            let getShopDB: any = await shopRepo.findOne({ _id: shopId });
            getShopDB = cloneObj(getShopDB);

            const appendGroupBuy =  [...getShopDB?.group_buy_list, objectRequestComplete];
            const updateShopDB = await shopRepo.update(shopId, { group_buy_list: appendGroupBuy });

            userService.clearUserInfoInCache(userId);
            return updateShopDB;
        } catch (error) {
            console.log("error", error)
        }
    }

    async delete(id) {
        try {
            console.log("id", id)
        } catch (error) {
            console.log("error", error)
        }
    }

    async approveGroupBuyOfShop (id, groupBuyId) {
        try {
            let data: any = []
            let getShop: any = await shopRepo.findById(id)
            getShop = cloneObj(getShop)
            if (getShop.group_buy_list && isMappable(getShop.group_buy_list)) {
                data = getShop.group_buy_list.find(value => value._id === groupBuyId)
            }

            data.status = "approved"

            const updateShop = await shopRepo.update(id, { group_buy_list: getShop.group_buy_list })
            await Promise.all(
                data.products.map(async (product) => {
                    const groupBuyInProduct = requestGroupBuyProduct(data,product)
                    await productRepo.findOneAndUpdate(
                        { _id: product?._id },
                        { group_buy: groupBuyInProduct }
                    );
                })
            )

            return updateShop
        } catch (error) {
            console.log("error", error)
        }
    }

    async update (user, request, groupBuyId) {
        try {
            const { shop_id: shopId } = user
            const { name, start_time, end_time, discount_type, products } = request
            let data: any = []
            let getShopDB: any = await shopRepo.findOne({ _id: shopId });
            getShopDB = cloneObj(getShopDB);
            data = getShopDB.group_buy_list.find(value => value._id === groupBuyId)
            if (data.status === "approved") {
                await Promise.all(
                    data.products.map(async (product) => {
                        await productRepo.findOneAndUpdate(
                            { _id: product?._id },
                            { group_buy: null }
                        );
                    })
                )
            }
            data.name = name
            data.start_time = start_time
            data.end_time = end_time
            data.discount_type = discount_type
            data.products = products
            data.status = "pending"
            const updateShop = await shopRepo.update(shopId, { group_buy_list: getShopDB.group_buy_list })
            return updateShop
        } catch (error) {
            console.log("error", error)
        }
    }
}