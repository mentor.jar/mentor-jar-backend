import moment from 'moment';
import { ProductSort, ShopListProductRequest, ShopSearchProductRequest } from './definition';
import { ShopRepo } from '../../../repositories/ShopRepo';
import { ProductRepo } from '../../../repositories/ProductRepo';
import { CategoryRepo } from '../../../repositories/CategoryRepo';
import { CategoryDTO } from '../../../DTO/CategoryDTO';
import { ProductDTO } from '../../../DTO/Product';
import { getMinMax } from '../product';
import { SearchKeywordRepo } from '../../../repositories/SearchKeywordRepo';
import ViettelPostService from '../../../services/3rd/shippings/viettelpost/ViettelPost';
import { AddressRepo } from '../../../repositories/AddressRepo';
import { ShippingMethodQuery } from '../../../models/enums/order';
import keys from '../../../config/env/keys';
import { ElasticQuery, elsIndexName } from '../../../services/elastic';
import { ProductElsDTO } from '../../../DTO/ElasticDTO/ProductDTO';
import { Pagination } from '../../../utils/paginate';
import { MAXIMUM_NUMBER_RETURNED, TYPE } from '../../../constants/product_in_shop';

const shopRepo = ShopRepo.getInstance();
const categoryRepo = CategoryRepo.getInstance();
const productRepo = ProductRepo.getInstance();
const searchKeywordRepo = SearchKeywordRepo.getInstance();
const addressRepository = AddressRepo.getInstance();
class ShopService {

  private static _instance: ShopService;

  static get _() {
    if (!this._instance) {
      this._instance = new ShopService();
    }
    return this._instance;
  }

  /**
   * get product explore
   * 
   * @param request 
   *  - orderBy: String 
   *  - criteria: String
   *  - shop_id: String
   */
  async getProductsExplore(request: ShopListProductRequest, limit, page, user: any) {
    const dataBundle = await productRepo.findProductForExplore(request, page, limit);

    let results: any = null;
    let productDTO = dataBundle.data.map(e => new ProductDTO(e));

    dataBundle.data = dataBundle.data.map(product => {
      let item:any = new ProductDTO(product);
      item = item.getProductComplete(user)
      // item.sold = product.order_items && product.order_items.length ? product.order_items[0].sold : 0
      item.discount_percent = item.discount_percent ? item.discount_percent.discount : 0
      delete item.order_items
      return  item
    });
    return dataBundle;
  }

  /**
   * get product explore v2 with ELS
   * 
   * @param request 
   *  - orderBy: String 
   *  - shop_id: String
   */
  async getProductsExploreV2(request: ShopListProductRequest, limit, page, user: any) {
    const { query, sort } = this.buildQuerySearchProductsByShopId(request);

    let elsResult = await ElasticQuery.getInstance().search(
      elsIndexName.product,
      query,
      {
          from: (page-1) * limit,
          size: limit,
      },
      sort
  );

  let productsDTO = elsResult.record.map((e) => {
      let product = new ProductElsDTO(e).get();
      product = new ProductDTO(product).toSimpleJSON();
      return product;
  });

  const resultPaginate = new Pagination(productsDTO).pagination(
      parseInt(page),
      parseInt(limit),
      elsResult.total.value
  );

  return resultPaginate;
  }

  private buildQuerySearchProductsByShopId(request: ShopListProductRequest) {
    let query: any = {
      bool: { 
        must: [
          {
            match: {
              shop_id: request.shop_id,
            }
          },
          {
            match: {
              is_approved: "approved"
            }
          },
          {
            term: {
              allow_to_sell: true
            }
          }
        ],
        must_not: [
          {
            exists: {
              "field": "deleted_at"
            }
          }
        ]
      }
    };
    const sort = [];

    switch (request.orderBy) {
      case ProductSort.Price_ASC:
        sort.push({ stocking: "desc" });
        sort.push({ is_sold_out: "asc" });
        sort.push({ sort_price: "asc" });
        sort.push({ is_genuine_item: "desc" });
        sort.push({ is_guaranteed_item: "desc" });
        break;
      case ProductSort.Price_DESC:
        sort.push({ stocking: "desc" });
        sort.push({ is_sold_out: "asc" });
        sort.push({sort_price: "desc" });
        sort.push({is_genuine_item: "desc" });
        sort.push({is_guaranteed_item: "desc" });
        break;
      case ProductSort.POPULATE:
        sort.push({ stocking: "desc" });
        sort.push({ is_sold_out: "asc" });
        sort.push({ popular_mark: "desc" });
        sort.push({ is_genuine_item: "desc" });
        sort.push({ is_guaranteed_item: "desc" });
        break;
      case ProductSort.NEWEST:
        sort.push({ stocking: "desc" });
        sort.push({ is_sold_out: "asc" });
        sort.push({ is_genuine_item: "desc" });
        sort.push({ is_guaranteed_item: "desc" });
        sort.push({ createdAt: "desc" });
      break;
      case ProductSort.BEST_SELL:
        query.bool.must.push({
            range: {
                sold: {
                    gt: 0,
                },
            },
        });

        sort.push({ stocking: "desc" });
        sort.push({ is_sold_out: "asc" });
        sort.push({ sold: "desc" });
        sort.push({ is_genuine_item: "desc" });
        sort.push({ is_guaranteed_item: "desc" });
        break;
      default:
        break;
    }

    return { query, sort };
  }

  async getFlashSaleProducts(shopId) {
    const query = this.buildQueryFlashSaleProducts(shopId);

    let elsResult = await ElasticQuery.getInstance().search(
      elsIndexName.product,
      query,
      {
        size: MAXIMUM_NUMBER_RETURNED,
      }
  );

  let productsDTO = elsResult.record.map((e) => {
      let product = new ProductElsDTO(e).get();
      product = new ProductDTO(product).toSimpleJSON();
      return product;
  });

  return productsDTO;
  }

  private buildQueryFlashSaleProducts(shopId) {
    const query: any = {
      bool: { 
        must: [
          {
            match: {
              shop_id: shopId,
            }
          },
          {
            match: {
              is_approved: "approved"
            }
          },
          {
            term: {
              allow_to_sell: true
            }
          },
          {
            term: {
                is_sold_out: false
            }  
          },
          {
            range: {
                quantity: {
                  gt: 0,
                }
              }
          },
          {
            exists: {
              "field": "flash_sale"
            }
          }
        ],
        must_not: [
          {
            exists: {
              "field": "deleted_at"
            }
          }
        ]
      }
    };
    
    return query;
  }

  async getHotDealProducts(shopId) {
    const query = this.buildQueryHotDealProducts(shopId);

    let elsResult = await ElasticQuery.getInstance().search(
      elsIndexName.product,
      query,
      {
        size: MAXIMUM_NUMBER_RETURNED,
      }
  );

  let productsDTO = elsResult.record.map((e) => {
      let product = new ProductElsDTO(e).get();
      product = new ProductDTO(product).toSimpleJSON();
      return product;
  });

  return productsDTO;
  }

  private buildQueryHotDealProducts(shopId) {
    const query: any = {
      bool: { 
        must: [
          {
            match: {
              shop_id: shopId,
            }
          },
          {
            match: {
              is_approved: "approved"
            }
          },
          {
            term: {
              allow_to_sell: true
            }
          },
          {
            term: {
                is_sold_out: false
            }  
        },
        {
            range: {
                quantity: {
                  gt: 0,
                }
              }
        },
        {
          range: {
            discount_percent: {
                gt: 0,
              }
            }
        }
        ],
        must_not: [
          {
            exists: {
              "field": "deleted_at"
            }
          }
        ]
      }
    };
    
    return query;
  }

  async getBestSellProducts(shopId) {
    const  { query, sort } = this.buildQueryBestSellProducts(shopId);

    let elsResult = await ElasticQuery.getInstance().search(
      elsIndexName.product,
      query,
      {
        size: MAXIMUM_NUMBER_RETURNED,
      },
      sort
  );

  let productsDTO = elsResult.record.map((e) => {
      let product = new ProductElsDTO(e).get();
      product = new ProductDTO(product).toSimpleJSON();
      return product;
  });

  return productsDTO;
  }

  private buildQueryBestSellProducts(shopId) {
    const query: any = {
      bool: { 
        must: [
          {
            match: {
              shop_id: shopId,
            }
          },
          {
            match: {
              is_approved: "approved"
            }
          },
          {
            term: {
              allow_to_sell: true
            }
          },
          {
            range: {
              sold: {
                gt: 0,
              }
            }  
          },
        ],
        must_not: [
          {
            exists: {
              "field": "deleted_at"
            }
          }
        ]
      }
    };

    const sort = [
      { stocking: "desc" },
      { is_sold_out: "asc" },
      { sold: "desc" },
      { is_genuine_item: "desc" },
      { is_guaranteed_item: "desc" }
    ];
    
    return  { query, sort };
  }

  async getNewestProducts(shopId) {
    const  { query, sort } = this.buildQueryNewestProducts(shopId);

    let elsResult = await ElasticQuery.getInstance().search(
      elsIndexName.product,
      query,
      {
        size: MAXIMUM_NUMBER_RETURNED,
      },
      sort
  );

  let productsDTO = elsResult.record.map((e) => {
      let product = new ProductElsDTO(e).get();
      product = new ProductDTO(product).toSimpleJSON();
      return product;
  });

  return productsDTO;
  }

  private buildQueryNewestProducts(shopId) {
    const fiveDaysAgo = moment().subtract(5, 'day').endOf('day').toDate();
    const query: any = {
      bool: { 
        must: [
          {
            match: {
              shop_id: shopId,
            }
          },
          {
            match: {
              is_approved: "approved"
            }
          },
          {
            term: {
              allow_to_sell: true
            }
          },
          {
            term: {
                is_sold_out: false
            }  
          },
          {
            range: {
              createdAt: {
                gte: fiveDaysAgo
              }
            }
          },
          {
            range: {
                quantity: {
                  gt: 0,
                }
              }
          },
        ],
        must_not: [
          {
            exists: {
              "field": "deleted_at"
            }
          }
        ]
      }
    };

    const sort = [
      { createdAt: "desc" }
    ];
    
    return  { query, sort };
  }

  async getProductsByType(shopId, type: TYPE, limit, page,) {
    let query = {}, sort = [];

    if (!type) type = TYPE.NEWEST;

    switch (type) {
      case TYPE.FLASH_SALE:
        query = this.buildQueryFlashSaleProducts(shopId);
        break;
      case TYPE.HOT_DEAL:
        query = this.buildQueryHotDealProducts(shopId);
        break;
      case TYPE.BEST_SELL:
        ({ query, sort} = this.buildQueryBestSellProducts(shopId));
        break;
      case TYPE.NEWEST:
        ({ query, sort} = this.buildQueryNewestProducts(shopId));
        break;
    
      default:
        break;
    }

    let elsResult = await ElasticQuery.getInstance().search(
      elsIndexName.product,
      query,
      {
        from: (page-1) * limit,
        size: limit,
      },
      sort
  );

  let productsDTO = elsResult.record.map((e) => {
      let product = new ProductElsDTO(e).get();
      product = new ProductDTO(product).toSimpleJSON();
      return product;
  });

  const resultPaginate = new Pagination(productsDTO).pagination(
    parseInt(page),
    parseInt(limit),
    elsResult.total.value
);

  return resultPaginate;
  }

  async getSuggestProducts(shopId, limit, page,) {
    const { query, sort } = this.buildQuerySuggestProducts(shopId);

    let elsResult = await ElasticQuery.getInstance().search(
      elsIndexName.product,
      query,
      {
        from: (page-1) * limit,
        size: limit,
      },
      sort
  );

  let productsDTO = elsResult.record.map((e) => {
      let product = new ProductElsDTO(e).get();
      product = new ProductDTO(product).toSimpleJSON();
      product.sold = product?.sold < 10 ? 0 : product?.sold;
      return product;
  });

  const resultPaginate = new Pagination(productsDTO).pagination(
    parseInt(page),
    parseInt(limit),
    elsResult.total.value
);

  return resultPaginate;
  }

  private buildQuerySuggestProducts(shopId) {
    const query: any = {
      bool: { 
        must: [
          {
            match: {
              shop_id: shopId,
            }
          },
          {
            match: {
              is_approved: "approved"
            }
          },
          {
            term: {
              allow_to_sell: true
            }
          },
          {
            term: {
                is_sold_out: false
            }  
        },
        {
            range: {
                quantity: {
                  gt: 0,
                }
              }
        }
        ],
        must_not: [
          {
            exists: {
              "field": "deleted_at"
            }
          }
        ]
      }
    };

    const sort = [
      { is_suggested: "desc" },
      { sold: "desc" },
      { createdAt: "desc" },
      { is_genuine_item: "desc" },
      { is_guaranteed_item: "desc" }
    ];
    
    return { query, sort };
  }
  
  /**
   * 
   * @param shop_id 
   */
  async getCategoryExplore(shop_id: String) {
    const data = await categoryRepo.getCategoryOfShopExplore(shop_id);
    return data.map(category => {
      const item: any = new CategoryDTO(category).toSimpleJSON();
      item.product_numbers = category.product_numbers || 0;
      item.image_url = category.image_url || '';
      return item;
    })
  }

  /**
  * search product of shop explore
  * 
  * @param request 
  *  - orderBy: String 
  *  - criteria: String
  *  - shop_id: String
  */
  async shopSearchExplore(request: ShopSearchProductRequest, limit, page, user: any) {
    const dataBundle: any = await productRepo.searchProductForExplore(request, page, limit);

    let results: any = null;
    let productDTO = dataBundle.data.map(e => new ProductDTO(e));

    results = productDTO.map(e => {
      e.setUser(user);
      let getProduct: any = e.toJSONWithBookMark();
      let getProductOption: any = e.toJSON(['variants']);
      //Handle variants field
      let variantsDTO = e.getProductVariant(getProductOption.variants);
      getProduct.variants = [];
      variantsDTO.map(e => {
        let varianJson = e.toJSON(['_id', 'option_values', 'before_sale_price', 'sale_price', 'quantity', 'is_master'])
        getProduct.variants.push(varianJson);
      });
      getProduct.price_min_max = getMinMax(getProduct.variants);
      // getProduct.sold = getProduct.order_items && getProduct.order_items.length ? getProduct.order_items[0].sold : 0;
      delete getProduct.order_items
      delete getProduct.variants;

      return getProduct;
    });
    dataBundle.data = results;

    if (request.keyword) searchKeywordRepo.insertOrUpdate(request.keyword, request.shop_id)

    return dataBundle;
  }

  getShippingMethodByShop = async (params) => {

    const shop = await shopRepo.getShopByShippingMethod(params.shop_id, params.user_id, params.shipping_method_id);
    const shipping_methods = shop.shipping_methods.filter(method =>  
        method.shipping_method_id.toString() ===  params.shipping_method_id.toString()
      );
    return shipping_methods[0];
  }

  updateActiveShippingMethodByShop = async (params) => {
   
    const isActive = params.is_active;
    const configParams = {
      'shipping_methods.$.is_active': isActive,
    }

    const result = await shopRepo.updateShippingMethodByShop(params, configParams);
    return result;
  }
  
  updateShippingMethodByShop = async (params, paramsUpdate) => {
   
    let configParams: any = {
      'shipping_methods.$.code': paramsUpdate.code,
      'shipping_methods.$.token': paramsUpdate.token,
    }

    const result = await shopRepo.updateShippingMethodByShop(params, configParams);
    return result;
  }

  createStoreViettelPost = async (params, req) => {
    const pickAddressDefault = await addressRepository.getPickAddressUser(params.user_id);
    if (!pickAddressDefault) throw new Error(req.__("controller.shop.default_pick_address_not_found"));

    let storeInfo = await ViettelPostService._.setStoreDefault({
      token: keys.VIETTELPOST.token,
    });
    let data = await ShopService._.updateShippingMethodByShop(params, storeInfo);
    return data;
  }
}
export default ShopService;