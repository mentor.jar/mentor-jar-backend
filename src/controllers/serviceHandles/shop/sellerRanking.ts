import { PaymentStatus, ShippingStatus } from "../../../models/enums/order";
import { OrderRepository } from "../../../repositories/OrderRepo";
import { ObjectId } from "../../../utils/helper";

const orderRepo = OrderRepository.getInstance()

class SellerRankingService {
	
	protected shop;
	protected shopData;
	
	constructor(shop) {
		this.shop = shop
	}

	async checkRankOfSeller() {
		const data = await this.getData();
		this.shopData = data
		return {
			data,
			name: this.checkRubyRank() ? 'RUBY' : this.checkDiamondRank() ? "DIAMOND" : this.checkGoldRank() ? 'GOLD' : null
		}
	}

	async getData() {
		const numberOfBuyers = await this.calculateNumberOfBuyer()
		const totalOrders = await this.calculateTotalOrder()
		const successOrders = await this.calculateSuccessfulOrder()
		const unSuccessOrders = await this.calculateUnSuccessfulOrder()
		const penaltyOrders = await this.calculateNumberOfPenaltyOrder()
		const prepareOrderTime = await this.calculateTimePrepareOrder()
		const chatResponseByPercent = 95
		const preOrderByPercent = 30

		const unSuccessOrderByPercent = totalOrders === 0 ? 0 : +(unSuccessOrders/totalOrders * 100).toFixed(2)
		const successOrderByPercent = totalOrders === 0 ? 0 : +(successOrders/totalOrders * 100).toFixed(2)
		const penaltyOrderByPercent = totalOrders === 0 ? 0 : +(penaltyOrders/totalOrders * 100).toFixed(2)
		return {
			numberOfBuyers,
			totalOrders,
			successOrderByPercent,
			chatResponseByPercent,
			shopRating: this.shop.avg_rating,
			unSuccessOrderByPercent,
			lateDelivery: prepareOrderTime.latePrepareByPercent,
			penaltyOrderByPercent,
			preOrderByPercent
		}
		
	}

	checkGoldRank() {
		const { numberOfBuyers, successOrderByPercent, chatResponseByPercent, unSuccessOrderByPercent, 
			lateDelivery, penaltyOrderByPercent, preOrderByPercent, shopRating
		} = this.shopData
		return numberOfBuyers >= 20 && successOrderByPercent >= 50 && chatResponseByPercent >= 70
				&& shopRating >= 4.00 && unSuccessOrderByPercent <= 5 && lateDelivery < 5 
				&& penaltyOrderByPercent <= 0 && preOrderByPercent <= 30
	}

	checkDiamondRank() {
		const { numberOfBuyers, successOrderByPercent, chatResponseByPercent, unSuccessOrderByPercent, 
			lateDelivery, penaltyOrderByPercent, preOrderByPercent, shopRating
		} = this.shopData
		return numberOfBuyers >= 35 && successOrderByPercent >= 75 && chatResponseByPercent >= 80
				&& shopRating >= 4.50 && unSuccessOrderByPercent <= 3 && lateDelivery < 5 
				&& penaltyOrderByPercent <= 0 && preOrderByPercent <= 30
	}

	checkRubyRank() {
		const { numberOfBuyers, successOrderByPercent, chatResponseByPercent, unSuccessOrderByPercent, 
			lateDelivery, penaltyOrderByPercent, preOrderByPercent, shopRating
		} = this.shopData

		return numberOfBuyers >= 50 && successOrderByPercent >= 100 && chatResponseByPercent >= 90
				&& shopRating >= 4.80 && unSuccessOrderByPercent <= 3 && lateDelivery < 3 
				&& penaltyOrderByPercent <= 0 && preOrderByPercent <= 30
	}

	async calculateNumberOfBuyer() {
		const numberOfBuyers = await orderRepo.findNumberOfBuyerByShop(ObjectId(this.shop._id))
		return numberOfBuyers.length
	}

	async calculateTotalOrder() {
		const totalOrders = await orderRepo.find({
			shop_id: ObjectId(this.shop._id)
		})
		return totalOrders.length
	}

	async calculateSuccessfulOrder() {
		const successOrders = await orderRepo.find({
			shipping_status: ShippingStatus.SHIPPED,
			payment_status: PaymentStatus.PAID,
			shop_id: ObjectId(this.shop._id)
		})
		return successOrders.length
	}

	async calculateUnSuccessfulOrder() {
		const unSuccessOrders = await orderRepo.find({
			shipping_status: {$nin: [ShippingStatus.SHIPPED, ShippingStatus.SHIPPING]},
			shop_id: ObjectId(this.shop._id)
		})
		return unSuccessOrders.length
	}

	async calculateNumberOfPenaltyOrder() {
		const penaltyOrders = await orderRepo.find({
			is_mark_penalty: true,
			shop_id: ObjectId(this.shop._id)
		})
		return penaltyOrders.length
	}

	async calculateTimePrepareOrder() {
		const timePrepareOfShop = await orderRepo.getTimePrepareOrder(this.shop._id)
		const totalMoreThanThreeDays = timePrepareOfShop.reduce((sum, item) => {
			return sum + (item.day > 3 ? item.count : 0)
		}, 0)
		const totalOrderPrepared = timePrepareOfShop.reduce((sum, item) => {
			return sum + item.count
		}, 0)
		let latePrepareByPercent;
		if(totalOrderPrepared === 0) {
			latePrepareByPercent = 0
		}
		else {
			latePrepareByPercent = +(totalMoreThanThreeDays/totalOrderPrepared * 100).toFixed(2)
		}
		return {totalMoreThanThreeDays, totalOrderPrepared, latePrepareByPercent}
	}

	async 

}

export default SellerRankingService