export enum Limit {
  One = 1,
  Two = 2,
  Three = 3,
  Four = 4,
  Five = 5
}

export enum ProductSort {
  Price_ASC = 'price_asc',
  Price_DESC = 'price_desc',
  POPULATE = 'populate',
  NEWEST = 'newest',
  BEST_SELL = 'best_sell'
}

export interface ShopListProductRequest {
  orderBy: String,
  shop_id: String
}

export interface ShopSearchProductRequest {
  keyword: String,
  category: String,
  location: String,
  ship_provider: String,
  price_min: Number,
  price_max: Number,
  status: String,
  pay_option: String,
  rate: String,
  services: String,
  shop_id?: String,
  orderBy: String,
}
export interface SectionData {
  type: String,
  order: Number,
  data: any
}