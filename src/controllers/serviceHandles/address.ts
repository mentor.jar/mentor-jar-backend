import { NotFoundError } from "../../base/customError";
import { AddressRepo } from "../../repositories/AddressRepo";
import { OrderRepository } from "../../repositories/OrderRepo";
import { ShopRepo } from "../../repositories/ShopRepo";
import { cloneObj, ObjectId } from "../../utils/helper";


class AddressService {

  private static _instance: AddressService;

  static get _() {
    if (!this._instance) {
      this._instance = new AddressService();
    }
    return this._instance;
  }

  private shopRepo = ShopRepo.getInstance()


  getAddressByUser = async (addressId, userId) => {
    const addressRepo = await AddressRepo.getInstance();
    const addressUser = await addressRepo.findWithUser(addressId, userId);
    if(!addressUser) {
      throw new NotFoundError("Không tìm thấy địa chỉ.");
    }
    return addressUser;
  }

  checkValidPickAddressDefault = async (addressId, userId, orderShop) => {
    let result = null;

    const addressRepo = await AddressRepo.getInstance();
    let addressShop = await addressRepo.findWithUser(addressId, userId);
    if (addressShop && orderShop) {
      if (addressShop.state.name === orderShop.pick_address.state.name && addressShop.district.name === orderShop.pick_address.district.name) {
        result = addressShop;
      }
    }
    return result;
  }
  
  getAddressValidForSeller = async (userId, orderDetail, shopID = undefined) => {

    const addressRepo = await AddressRepo.getInstance();
    let addresses
    if(shopID) {
      const shop:any = await this.shopRepo.findOne({_id: ObjectId(shopID)})
      addresses = await addressRepo.getAddressByUser(shop.user_id);
    }
    else {
      addresses = await addressRepo.getAddressByUser(userId);
    }
    

    addresses = addresses.map((item) => {
      item = cloneObj(item);
      if(orderDetail.pick_address.state.name === item.state.name && orderDetail.pick_address.district.name === item.district.name) {
          item.is_valid = true;
      } else {
          item.is_valid = false;
      }
      return item;
    })

    return addresses;
  }

  getPickAddressUser = async (userId) => {
    const addressRepo = await AddressRepo.getInstance();
    const addressUser = await addressRepo.getPickAddressUser(userId);
    if(!addressUser) {
      throw new NotFoundError("Không tìm thấy địa chỉ lấy hàng mặc định.");
    }
    return addressUser;
  }

}


export default AddressService;