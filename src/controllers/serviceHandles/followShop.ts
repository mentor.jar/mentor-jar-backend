import { BannerDTO } from '../../DTO/BannerDTO';
import BannerRepo from '../../repositories/BannerRepo';
import IPaginateData from '../../repositories/interfaces/IPaginateData';
import { NotFoundError } from '../../base/customError';
import { FollowShopRepository } from '../../repositories/FollowShopRepository';

const followShoprepository = FollowShopRepository.getInstance();

export interface CreteaFollowShop {
    user_id: String,
    shop_id: String
}

class FollowShopService {

    private static _instance: FollowShopService;

    static get _() {
        if (!this._instance) {
            this._instance = new FollowShopService();
        }
        return this._instance;
    }

    async createFollowShop(request: CreteaFollowShop) {
        const newInstance = await followShoprepository.create(request);
        return newInstance;
    }

}
export default FollowShopService;