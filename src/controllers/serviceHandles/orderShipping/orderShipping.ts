import { BIDUStatusAction, CancelType, GHTKStatusAction, GHTK_BUYER_STATUS_NOTIFY, GHTK_SELLER_STATUS_NOTIFY, GHTK_STATUS_NOTIFY, PaymentStatus, ShippingMethodQuery, ShippingStatus } from "../../../models/enums/order";
import { AdHoc, TargetType } from "../../../models/enums/TrackingObject";
import { OrderRepository } from "../../../repositories/OrderRepo";
import { OrderShippingRepo } from "../../../repositories/OrderShippingRepo";
import { TrackingObjectRepo } from "../../../repositories/TrackingObjectRepo";
import { UserRepo } from "../../../repositories/UserRepo";
import { VoucherRepository } from "../../../repositories/VoucherRepository";
import GHTKService from "../../../services/3rd/shippings/GHTK";
import ViettelPostService from "../../../services/3rd/shippings/viettelpost/ViettelPost";
import { handleUserAfterCancelOrder } from "../../../services/api/order";
import { getTranslation, getUserLanguage } from "../../../services/i18nCustom";
import notificationService, { NotifyRequestFactory } from "../../../services/notification";
import { NOTIFY_TARGET_TYPE, NOTIFY_TYPE } from "../../../services/notification/constants";
import { ObjectId } from "../../../utils/helper";
import { randomStringWithLength } from "../../../utils/utils";
import { updateQuantityCancelOrder } from "../../api/mobile_app/orders";

const orderRepo = OrderRepository.getInstance();
const orderShippingRepo = OrderShippingRepo.getInstance();
const ghtkService = GHTKService.getInstance();
const voucherRepo = VoucherRepository.getInstance();
class OrderShippingService {

  private static _instance: OrderShippingService;

  static get _() {
    if (!this._instance) {
      this._instance = new OrderShippingService();
    }
    return this._instance;
  }

  handlerUpdateHistoryShipping = async (params) => {

    const query = { 'shipping_info.partner_id': params.partner_id, 'shipping_info.label': params.label_id };
    const result = await orderShippingRepo.findOrderShipping(query);
    if (!result) return;
    const order = await orderRepo.findOrderByOrderNumber({ order_number: params.partner_id });
    params.shipping_status = ghtkService.getStatusTextOrder(params.status_id);
    params.reason_code_shipping = ghtkService.getReasonTextOrder(params.reason_code);
    params.action_time = new Date(params.action_time).toISOString();
    // save history
    result.history.push(params);
    result.save();
    // update shipping
    let orderUpdate = await this.updateStatusShippingOrder(order, params);
    // handler order canceled.
    if (orderUpdate.shipping_status === ShippingStatus.CANCELED) {
      handleUserAfterCancelOrder(order.user_id);
      // update quantity cancel
      await updateQuantityCancelOrder(order._id);
    }

    if(orderUpdate.shipping_status === ShippingStatus.SHIPPED) {
      this.assignCashBackVoucherToUser(order.user_id, order.vouchers, order.total_value_items, order.order_number, order._id)
    }

    // send notification
    this.sendNotificationWithStatus(order, params, result);
    return result;
  }

  /**
   * Cập nhật shipping_status khi nhận các acction từ GHTK (-1, 7)
   * Cập nhật quantity khi nhận các acction CANCEL từ GHTK
   * - Khi nhận status_id = 3 (Đã lấy hàng/Đã nhập kho) cập nhật lại shipping_status = shipping
   * - Khi nhận status_id = 7 (Không lấy được hàng) cập nhật lại shipping_status = cancel, cancel_reason, cancel_type, cancel_by, cancel_time
   * - Khi nhận status_id = -1 (Hủy đơn hàng) cập nhật lại shipping_status = cancel,  cancel_reason, cancel_type, cancel_by, cancel_time
   * @param req 
   * @param res 
   * @param next 
   */
  updateStatusShippingOrder = async (order, params) => {
    // const order = await orderRepo.findOrderByOrderNumber({ order_number: params.partner_id });

    switch (parseInt(params.status_id)) {
      case GHTKStatusAction.PICKED_UP:
        if (order.shipping_status === ShippingStatus.WAIT_TO_PICK) {
          order.shipping_status = ShippingStatus.SHIPPING;
        }
        break;
      case GHTKStatusAction.CAN_NOT_PICK:
      case GHTKStatusAction.CANCELED:
      case GHTKStatusAction.RETURNED_TO_SHOP:
        order.shipping_status = ShippingStatus.CANCELED;
        order.cancel_reason = ghtkService.setReasonCancel(params);
        order.cancel_type = CancelType.SYSTEM;
        order.cancel_time = new Date();
        // update quantity cancel
        // await updateQuantityCancelOrder(order._id);
        break;
      case GHTKStatusAction.DELIVERIED:
        order.shipping_status = ShippingStatus.SHIPPED;
        order.payment_status = PaymentStatus.PAID;
        break;
      case GHTKStatusAction.CHECKED:
        order.payment_status = PaymentStatus.PAID;
        break;
      default:
        break;
    }

    // Insert Table TrackingObject Seller
    const userRepo = UserRepo.getInstance();
    const trackingObjectRepo = TrackingObjectRepo.getInstance();
    const userShop = await userRepo.getUserByShopId(order.shop_id);
    // await trackingObjectRepo.createOrUpdateTrackingObject(userShop._id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);
    // Insert Table TrackingObject Buyer
    // await trackingObjectRepo.createOrUpdateTrackingObject(order.user_id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);

    await order.save();
    return order;
  }

  handlerUpdateHistoryMutilShipping = async (params, nameQuery) => {

    if (nameQuery === ShippingMethodQuery.VIETTELPOST) {
      const parseData = await ViettelPostService._.webhookToShippingHistory(params);

      params = parseData.history;
      const query = { 'shipping_info.label': params.label_id };
      const result = await orderShippingRepo.findOrderShipping(query);
      if (!result) return;
      const order = await orderRepo.findOrderByOrderNumber({ order_number: params.partner_id });
      params.shipping_status = parseData.order_status.message;
      params.reason_code_shipping = parseData.order_status.message;
      params.action_time = new Date(parseData.history.action_time).toISOString();

      // save history
      result.history.push(params);
      result.save();
      // update shipping
      let orderUpdate = await this.updateStatusShippingMutilOrder(order, params);
      // handler order canceled.
      if (orderUpdate.shipping_status === ShippingStatus.CANCELED) {
        handleUserAfterCancelOrder(order.user_id);
        // update quantity cancel
        await updateQuantityCancelOrder(order._id);
      }

      if(orderUpdate.shipping_status === ShippingStatus.SHIPPED) {
        this.assignCashBackVoucherToUser(order.user_id, order.vouchers, order.total_value_items, order.order_number, order._id)
      }

      // send notification
      this.sendNotificationWithStatus(order, parseData.history, result);
      return result;
    }

  }

  /**
   * Cập nhật shipping_status khi nhận các acction từ GHTK (-1, 7)
   * Cập nhật quantity khi nhận các acction CANCEL từ GHTK
   * - Khi nhận status_id = 3 (Đã lấy hàng/Đã nhập kho) cập nhật lại shipping_status = shipping
   * - Khi nhận status_id = 7 (Không lấy được hàng) cập nhật lại shipping_status = cancel, cancel_reason, cancel_type, cancel_by, cancel_time
   * - Khi nhận status_id = -1 (Hủy đơn hàng) cập nhật lại shipping_status = cancel,  cancel_reason, cancel_type, cancel_by, cancel_time
   * @param req 
   * @param res 
   * @param next 
   */
   updateStatusShippingMutilOrder = async (order, params) => {
    // const order = await orderRepo.findOrderByOrderNumber({ order_number: params.partner_id });
    switch (parseInt(params.status_id)) {
      case BIDUStatusAction.PICKED_UP:
        if (order.shipping_status === ShippingStatus.WAIT_TO_PICK) {
          order.shipping_status = ShippingStatus.SHIPPING;
        }
        break;
      case BIDUStatusAction.CAN_NOT_PICK:
      case BIDUStatusAction.CANCELED:
      case BIDUStatusAction.RETURNED_TO_SHOP:
        order.shipping_status = ShippingStatus.CANCELED;
        order.cancel_reason = params.message || "Đơn hàng giao không thành công.";
        order.cancel_type = CancelType.SYSTEM;
        order.cancel_time = new Date();
        // update quantity cancel
        // await updateQuantityCancelOrder(order._id);
        break;
      case BIDUStatusAction.DELIVERIED:
        order.shipping_status = ShippingStatus.SHIPPED;
        order.payment_status = PaymentStatus.PAID;
        break;
      case BIDUStatusAction.CHECKED:
        order.payment_status = PaymentStatus.PAID;
        break;
      default:
        break;
    }

    // Insert Table TrackingObject Seller
    const userRepo = UserRepo.getInstance();
    const trackingObjectRepo = TrackingObjectRepo.getInstance();
    const userShop = await userRepo.getUserByShopId(order.shop_id);
    // await trackingObjectRepo.createOrUpdateTrackingObject(userShop._id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);
    // Insert Table TrackingObject Buyer
    // await trackingObjectRepo.createOrUpdateTrackingObject(order.user_id, TargetType.ORDER, order._id, AdHoc.ORDER_MANAGE);

    await order.save();
    return order;
  }

  sendNotificationWithStatus = async (order, params, orderShipping) => {

    if (this._statusInOrderHistory(params, orderShipping)) 
      return;

    let generateRequestSeller, generateRequestBuyer, generateRequestFeedback;

    if (GHTK_SELLER_STATUS_NOTIFY.includes(parseInt(params.status_id))) {
      const sellerId = order.pick_address.accessible_id;
      const languageSeller = await getUserLanguage(sellerId);
  
      generateRequestSeller = NotifyRequestFactory.generate(
        NOTIFY_TYPE.ORDERS_GHTK,
        NOTIFY_TARGET_TYPE.SELLER,
        {
          title: getTranslation(languageSeller, 'notification.bidu_notification'),
          content: getTranslation(languageSeller, `notification.seller_shipping_status.${params.status_id}`, order.order_number),
          receiverId: sellerId.toString(),
          orderId: order._id.toString(),
          userId: sellerId.toString(),
          orderNumber: order.order_number,
          shipping_status: params.shipping_status.toLowerCase(),
          status_id: parseInt(params.status_id),
        }
      )
    };

    if (GHTK_BUYER_STATUS_NOTIFY.includes(parseInt(params.status_id))) {
      const buyerId = order.user_id;
      const languageBuyer = await getUserLanguage(buyerId);
      generateRequestBuyer = NotifyRequestFactory.generate(
        NOTIFY_TYPE.ORDERS_GHTK,
        NOTIFY_TARGET_TYPE.BUYER,
        {
          title: getTranslation(languageBuyer, 'notification.bidu_notification'),
          content: getTranslation(languageBuyer, `notification.buyer_shipping_status.${params.status_id}`, order.order_number),
          receiverId: buyerId.toString(),
          orderId: order._id.toString(),
          userId: buyerId.toString(),
          orderNumber: order.order_number,
          shipping_status: params.shipping_status.toLowerCase(),
          status_id: parseInt(params.status_id),
        }
      )

      if (parseInt(params.status_id) == GHTKStatusAction.DELIVERIED) {
        generateRequestFeedback = this._sendNotifyRemindFeedback({ languageBuyer, order });
      }
    };

    return [
      generateRequestSeller,
      generateRequestBuyer,
      generateRequestFeedback,
    ].map(async (item) => {
      if (!item) return;
      return notificationService.saveAndSend(item);
    });
  }

  _sendNotifyRemindFeedback = ({languageBuyer, order }) => {
    return NotifyRequestFactory.generate(
      NOTIFY_TYPE.ORDERS_REMIND_FEEDBACK,
      NOTIFY_TARGET_TYPE.BUYER,
      {title: getTranslation(languageBuyer, 'notification.bidu_notification'),
      content: getTranslation(languageBuyer, 'notification.buyer_order.remind_feedback', order.order_number),
      receiverId: order.user_id.toString(),
      orderId: order._id.toString(),
      userId: order.user_id.toString(),
      orderNumber: order.order_number,
    })
  }

  _statusInOrderHistory = (params, orderShipping) => {
    const histories = orderShipping.history;
    const shippingInfo = orderShipping.shipping_info;
  
    if (parseInt(shippingInfo.status_id) === parseInt(params.status_id)) 
      return true;
    return histories?.filter((status) =>  parseInt(status.status_id) === parseInt(params.status_id)).length > 1;
  }

  assignCashBackVoucherToUser = async (userID, vouchers, totalItemValue, orderNumber, orderID) => {
    const currentTime = new Date()
    const cashbackVoucher = vouchers.find(item => item.classify === 'cash_back')
    const cashBackItem:any = await voucherRepo.findOne({
      _id: cashbackVoucher?._id
    })

    if(cashbackVoucher && cashBackItem) {
      const subVoucherDiscount = await voucherRepo.findOne({
        'start_time': { '$lte': currentTime },
        'end_time': { '$gte': currentTime },
        'cash_back_ref.user_id': ObjectId(userID),
        'cash_back_ref.cash_back_id': ObjectId(cashBackItem._id),
        'available_quantity': { '$gt': 0 }
      })
      if(!subVoucherDiscount) {
        let discount = cashBackItem.type === 'price' ? cashBackItem?.value : +(totalItemValue * cashBackItem?.value / 100).toFixed(0)
        if(cashBackItem.max_discount_value) {
          discount = Math.min(discount, cashBackItem.max_discount_value)
        }
        const newSubVoucherDiscount = {
          "target": "system", "type": "price",
          "value": discount,
          "discount_by_range_price": [],
          "max_discount_value": discount,
          "display_mode": "all",
          "products": [], "shops": [], "users": [], "labels": [],
          "cash_back_ref": {
            "user_id": ObjectId(userID),
            "cash_back_id": ObjectId(cashBackItem._id)
          },
          "approve_status": "approved",
          "name": "Cash Back Discount",
          "code": randomStringWithLength(8),
          "start_time": currentTime,
          "end_time": cashBackItem?.end_time,
          "save_quantity": 1,
          "use_quantity": 1,
          "available_quantity": 1,
          "min_order_value": cashBackItem?.min_order_value,
          "max_order_value": cashBackItem?.max_order_value,
          "max_budget": discount,
          "classify": "cash_back_discount",
          "is_public": true
        }
        await voucherRepo.create(newSubVoucherDiscount)
        await voucherRepo.update(cashBackItem._id, {
          $inc: { available_quantity: -1, used_budget: discount }
        })
        const language = await getUserLanguage(userID);
        notificationService.saveAndSend(
          NotifyRequestFactory.generate(
              NOTIFY_TYPE.ASSIGN_CASH_BACK,
              NOTIFY_TARGET_TYPE.BUYER,
              {
                  title: getTranslation(language, 'notification.bidu_notification'),
                  content: getTranslation(language, 'notification.buyer_order.assign_cash_back', orderNumber),
                  receiverId: userID.toString(),
                  orderId: orderID.toString(),
                  userId: userID.toString(),
                  orderNumber: orderNumber,
              }
          ))
      }
      else {
        console.log("Already exist voucher"); // keep it for debugging time
        return;
      }
    } else {
      console.log("Order is not valid to assign cash back voucher"); // keep it for debugging time
      return;
    }
  }
}

export default OrderShippingService;