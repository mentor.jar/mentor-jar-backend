import { ProductElsDTO } from '../../../DTO/ElasticDTO/ProductDTO';
import { Pagination } from '../../../utils/paginate';
import { ElasticQuery, elsIndexName } from '../../../services/elastic';
import { ProductDTO } from '../../../DTO/Product';
import moment from 'moment';
class ProductService {
    private static _instance: ProductService;

    static get _() {
        if (!this._instance) {
            this._instance = new ProductService();
        }
        return this._instance;
    }

    async getProductByCategory(categoryId, page, limit, internationalCategory) {
        let boolQuery: any = this.buildQueryGetProductByCategory(categoryId, internationalCategory);

        let data = await ElasticQuery.getInstance().search(
            elsIndexName.product,
            boolQuery,
            {
                from: (page - 1) * limit,
                size: limit,
            }
        );

        let productsDTO = data.record.map((e) => new ProductElsDTO(e).get());
        const resultPaginate = new Pagination(productsDTO).pagination(
            parseInt(page),
            parseInt(limit),
            data.total.value
        );
        return resultPaginate;
    }

    buildQueryGetProductByCategory(categoryId, internationalCategory) {
        let boolQuery: any = {};
        let mustQuery: any = [
            {
                match: {
                    is_approved: 'approved',
                },
            },
            {
                terms: {
                    list_category_id: [`${categoryId}`],
                },
            },
            {
                term: {
                  allow_to_sell: true
                }
            },
            {
                term: {
                    is_sold_out: false
                }  
            },
            {
                range: {
                    quantity: {
                      gt: 0,
                    }
                  }
            },
            {
                term: {
                    "shop.is_approved": true
                }
            },
            {
                term: {
                    "shop.pause_mode": false
                }
            },
        ];

        let mustNotQuery: any = [ 
            {
                term: {
                    list_category_id: internationalCategory._id,
                },
            },
            {
                exists: {
                  "field": "deleted_at"
                }
            },
            {
                exists: {
                  "field": "group_buy"
                },
            }    
        ]

        boolQuery.must = mustQuery;
        boolQuery.must_not = mustNotQuery;
        return { bool: boolQuery };
    }

    async getTopProductSystem(page:number = 1, limit: number = 20) {
        let {query, sort }: any = this.buildQueryGetTopProductSystem();

        let data = await ElasticQuery.getInstance().search(
            elsIndexName.product,
            query,
            {
                from: (page - 1) * limit,
                size: limit,
            },
            sort
        );

        let productsDTO = data.record.map((product) =>
            new ProductDTO(new ProductElsDTO(product).get()).compressAtHome()
        );
        const resultPaginate = new Pagination(productsDTO).pagination(
            page,
            limit,
            data.total.value
        );
        return resultPaginate;
    }

    private buildQueryGetTopProductSystem() {
        let boolQuery: any = {};
        let mustQuery: any = [
            {
                match: {
                    is_approved: 'approved',
                },
            },
            {
                term: {
                  allow_to_sell: true
                }
            },
            {
                term: {
                    is_sold_out: false
                }  
            },
            {
                range: {
                    quantity: {
                      gt: 0,
                    }
                  }
            },
            {
                range: {
                    sold: {
                      gt: 0,
                    }
                  }
            },
            {
                term: {
                    "shop.is_approved": true
                }
            },
            {
                term: {
                    "shop.pause_mode": false
                }
            },
        ];

        let mustNotQuery: any = [ 
            {
                exists: {
                  "field": "deleted_at"
                }
            },
            {
                exists: {
                  "field": "group_buy"
                },
            }    
        ]

        boolQuery.must = mustQuery;
        boolQuery.must_not = mustNotQuery;

        const sort = [
            { sold: "desc" },
            { quantity: "desc" },
            { is_genuine_item: "desc" },
            { is_guaranteed_item: "desc" }
        ]

        return { query: {bool: boolQuery} , sort};
    }

    async getTopProductByCategory(categoryId, page, limit, internationalCategory) {
        let {query, sort }: any = this.buildQueryGetTopProductByCategory(categoryId, internationalCategory);

        let data = await ElasticQuery.getInstance().search(
            elsIndexName.product,
            query,
            {
                from: (page - 1) * limit,
                size: limit,
            },
            sort
        );

        let productsDTO = data.record.map((e) => new ProductElsDTO(e).get());
        const resultPaginate = new Pagination(productsDTO).pagination(
            parseInt(page),
            parseInt(limit),
            data.total.value
        );
        return resultPaginate;
    }

    buildQueryGetTopProductByCategory(categoryId, internationalCategory) {
        let boolQuery: any = {};
        let mustQuery: any = [
            {
                match: {
                    is_approved: 'approved',
                },
            },
            {
                match: {
                    category_id: categoryId,
                },
            },
            {
                term: {
                  allow_to_sell: true
                }
            },
            {
                term: {
                    is_sold_out: false
                }  
            },
            {
                range: {
                    quantity: {
                      gt: 0,
                    }
                  }
            },
            {
                range: {
                    sold: {
                      gt: 0,
                    }
                  }
            },
            {
                term: {
                    "shop.is_approved": true
                }
            },
            {
                term: {
                    "shop.pause_mode": false
                }
            },
        ];

        let mustNotQuery: any = [ 
            {
                term: {
                    list_category_id: internationalCategory._id,
                },
            },
            {
                exists: {
                  "field": "deleted_at"
                }
            },
            {
                exists: {
                  "field": "group_buy"
                },
            }    
        ]

        boolQuery.must = mustQuery;
        boolQuery.must_not = mustNotQuery;

        const sort = [
            { sold: "desc" },
            { quantity: "desc" },
            { is_genuine_item: "desc" },
            { is_guaranteed_item: "desc" }
        ]

        return { query: {bool: boolQuery} , sort};
    }

    async getSimilarProductList(page, limit, productID, categoryID, categoryIDs) {
        const { query, sort }: any = this.buildQueryGetSimilarProducts(productID, categoryID, categoryIDs);
        
        let data = await ElasticQuery.getInstance().search(
            elsIndexName.product,
            query,
            {
                from: (page - 1) * limit,
                size: limit,
            },
            sort
        );

        let productsDTO = data.record.map((e) => new ProductElsDTO(e).get());
        const resultPaginate = new Pagination(productsDTO).pagination(
            parseInt(page),
            parseInt(limit),
            data.total.value
        );
        return resultPaginate;

    }

    buildQueryGetSimilarProducts(productID, categoryID, categoryIDs) {
        let boolQuery: any = {};
        let mustQuery: any = [
            {
                match: {
                    category_id: categoryID.toString(),
                },
            },
            {
                match: {
                    is_approved: 'approved',
                },
            },
            {
                term: {
                    allow_to_sell: true
                }
            },
            {
                term: {
                    is_sold_out: false
                }
            },
            {
                range: {
                    quantity: {
                        gt: 0,
                    }
                }
            },
            {
                term: {
                    "shop.is_approved": true
                }
            },
            {
                term: {
                    "shop.pause_mode": false
                }
            },
        ];

        let mustNotQuery: any = [
            {
                exists: {
                    "field": "deleted_at"
                }
            },
            {
                match: {
                    _id: productID.toString(),
                },
            },
            {
                exists: {
                  "field": "group_buy"
                },
            }
        ];

        let filterQuery: any = {
            terms: {
                list_category_id: categoryIDs
            } 
        };

        boolQuery.must = mustQuery;
        boolQuery.filter = filterQuery;
        boolQuery.must_not = mustNotQuery;

        const sort = [
            { createdAt: "desc" },
            { quantity: "desc" },
            { is_genuine_item: "desc" },
            { is_guaranteed_item: "desc" }
        ]

        return { query: { bool: boolQuery }, sort };
    }
    
    async getNewestProduct(page, limit) {
        let {query, sort }: any = this.buildQueryGetNewestProductByCategory();

        let data = await ElasticQuery.getInstance().search(
            elsIndexName.product,
            query,
            {
                from: (page - 1) * limit,
                size: limit,
            },
            sort
        );

        let productsDTO = data.record.map((e) => new ProductDTO(new ProductElsDTO(e).get()).compressAtHome());
        const resultPaginate = new Pagination(productsDTO).pagination(
            parseInt(page),
            parseInt(limit),
            data.total.value
        );
        return resultPaginate;
    }

    buildQueryGetNewestProductByCategory() {
        let boolQuery: any = {};
        let mustQuery: any = [
            {
                match: {
                    is_approved: 'approved',
                },
            },
            {
                term: {
                  allow_to_sell: true
                }
            },
            {
                term: {
                    is_sold_out: false
                }  
            },
            {
                range: {
                    quantity: {
                      gt: 0,
                    }
                  }
            },
            {
                term: {
                    "shop.is_approved": true
                }
            },
            {
                term: {
                    "shop.pause_mode": false
                }
            },
        ];

        let mustNotQuery: any = [ 
            {
                exists: {
                  "field": "deleted_at"
                }
            },
            {
                exists: {
                  "field": "group_buy"
                },
            }    
        ]

        boolQuery.must = mustQuery;
        boolQuery.must_not = mustNotQuery;

        const sort = [
            { createdAt: "desc" },
            { is_genuine_item: "desc" },
            { is_guaranteed_item: "desc" }
        ]

        return { query: {bool: boolQuery} , sort};
    }

    async getListSubcategoryInternational(internationalCategory) {
        let { query, aggs }: any = this.buildQueryGetListSubcategoryInternational(internationalCategory)
        let data = await ElasticQuery.getInstance().searchDistinct(
            elsIndexName.product,
            query,
            aggs
        );

        data = data
            .filter((item) => item?.key !== internationalCategory?._id)
            .map((item) => item?.key);
        
        return data;
    }

    private buildQueryGetListSubcategoryInternational (internationalCategory) {
        let boolQuery: any = {}
        let mustQuery: any = [
            {
                term: {
                    list_category_id: internationalCategory._id,
                },
            },
            {
                match: {
                    is_approved: 'approved'
                }
            },
            {
                term: {
                    allow_to_sell: true
                }
            },
            {
                term: {
                    is_sold_out: false
                }
            },
            {
                range: {
                    quantity: {
                        gt: 0,
                    }
                }
            }
        ]
        let mustNotQuery: any = [ 
            {
                exists: {
                  "field": 'deleted_at'
                }
            },
            {
                exists: {
                  "field": "group_buy"
                },
            }    
        ]
        boolQuery.must = mustQuery
        boolQuery.must_not = mustNotQuery

        const aggregations = {
            "list_category": {
                terms: {
                    field: "category._id.keyword",
                    size: 1000
                }
            }
        }

        return { query: { bool: boolQuery }, aggs: aggregations };
    }

    async getListProductInternational(page, limit, internationalCategory, subcategory = null) {
        let { query, sort }: any = this.buildQueryGetListProductInternational(internationalCategory, subcategory)
        let data = await ElasticQuery.getInstance().search(
            elsIndexName.product,
            query,
            {
                from: (page - 1) * limit,
                size: limit,
            },
            sort
        );
        let productsDTO = data.record.map((e) => new ProductDTO(new ProductElsDTO(e).get()).compressAtHome());
        const resultPaginate = new Pagination(productsDTO).pagination(
            parseInt(page),
            parseInt(limit),
            data.total.value
        );
        
        return resultPaginate;
    }

    buildQueryGetListProductInternational (internationalCategory, subcategory = null) {
        let boolQuery: any = {}
        let mustQuery: any = [
            {
                term: {
                    list_category_id: internationalCategory._id,
                },
            },
            {
                match: {
                    is_approved: 'approved'
                }
            },
            {
                term: {
                    allow_to_sell: true
                }
            },
            {
                term: {
                    is_sold_out: false
                }
            },
            {
                range: {
                    quantity: {
                        gt: 0,
                    }
                }
            }
        ]
        let mustNotQuery: any = [ 
            {
                exists: {
                  "field": 'deleted_at'
                }
            },
            {
                exists: {
                  "field": "group_buy"
                },
            }    
        ]

        if (subcategory) {
            mustQuery.push(
                {
                    term: {
                        list_category_id: subcategory,
                    },
                }
            )
        }

        boolQuery.must = mustQuery
        boolQuery.must_not = mustNotQuery

        const sort = [
            { createdAt: "desc" },
        ]

        return { query: { bool: boolQuery }, sort };
    }

    async getBiggestPriceProductByShop(shopId) {
        let { query, sort } = this.buildQueryGetBiggestPriceProductByShop(shopId);

        let data = await ElasticQuery.getInstance().search(
            elsIndexName.product,
            query,
            {
                size: 1,
            },
            sort
        );
        if(!data.total.value) {
            return null;
        }
        let productsDTO = new ProductElsDTO(data.record[0]).get();
        return productsDTO;
    }

    private buildQueryGetBiggestPriceProductByShop(shopId) {
        let boolQuery: any = {};
        let mustQuery: any = [
            {
                match: {
                    is_approved: 'approved',
                },
            },
            {
                match: {
                    shop_id: shopId,
                },
            },
            {
                term: {
                  allow_to_sell: true
                }
            },
            {
                term: {
                    "shop.is_approved": true
                }
            },
            {
                term: {
                    "shop.pause_mode": false
                }
            },
        ];

        let mustNotQuery: any = [ 
            {
                exists: {
                  "field": "deleted_at"
                }
              }    
        ]

        boolQuery.must = mustQuery;
        boolQuery.must_not = mustNotQuery;

        const sort = [
            { sort_price: "desc" },
        ]

        return { query: {bool: boolQuery} , sort};
    }

    async getListProductGroupBuy(page, limit, type = '', categoryID = '') {
        const { query }: any = this.buildQueryGetGroupBuyProducts(type, categoryID);
        
        let data = await ElasticQuery.getInstance().search(
            elsIndexName.product,
            query,
            {
                from: (page - 1) * limit,
                size: limit,
            }
        );

        let productsDTO = data.record.map((e) =>  new ProductDTO(new ProductElsDTO(e).get()).toSimpleGroupBuyProductItem());
        const resultPaginate = new Pagination(productsDTO).pagination(
            parseInt(page),
            parseInt(limit),
            data.total.value
        );
        return resultPaginate;
    }

    buildQueryGetGroupBuyProducts(type, categoryID) {
        let boolQuery: any = {};
        let mustQuery: any = [
            {
                match: {
                    is_approved: 'approved',
                },
            },
            {
                term: {
                    allow_to_sell: true
                }
            },
            {
                term: {
                    is_sold_out: false
                }
            },
            {
                range: {
                    quantity: {
                        gt: 0,
                    }
                }
            },
            {
                term: {
                    "shop.is_approved": true
                }
            },
        ];

        switch (type) {
            case 'INCOMING':
                mustQuery.push({
                    range: { "group_buy.start_time": { gt: new Date() } } 
                });
                break;
            default: // RUNNING
                mustQuery.push({
                    range: { "group_buy.end_time": { gte: new Date() } } 
                });
                mustQuery.push({
                    range: { "group_buy.start_time": { lte: new Date() } } 
                });
                break;
        }

        if(categoryID) {
            mustQuery.push({
                match: {
                    category_id: categoryID.toString(),
                }
            });
        }
    
        let mustNotQuery: any = [
            {
                exists: {
                    "field": "deleted_at"
                }
            }
        ];


        boolQuery.must = mustQuery;
        boolQuery.must_not = mustNotQuery;

        return { query: { bool: boolQuery } };
    }

    async getGroupBuyProductInWeek (limit, page) {
        let { query, sort } = this.buildQueryGetGroupBuyProductInWeek();
    
        let data = await ElasticQuery.getInstance().search(
            elsIndexName.product,
            query,
            {
                from: (page - 1) * limit,
                size: limit,
            },
            sort
        )
        let productsDTO = data.record.map((e) => new ProductDTO(new ProductElsDTO(e).get()).toSimpleGroupBuyProductItem());
        const resultPaginate = new Pagination(productsDTO).pagination(
            parseInt(page),
            parseInt(limit),
            data.total.value
        );
        return resultPaginate;
    }
    
    private buildQueryGetGroupBuyProductInWeek() {
        const endTime = moment().clone().endOf('week').add(1,'days').toDate()
        let boolQuery: any = {}
        let mustQuery: any = [
            {
                match: {
                    is_approved: 'approved',
                },
            },
            {
                term: {
                  allow_to_sell: true
                }
            },
            {
                term: {
                    is_sold_out: false
                }  
            },
            {
                range: {
                    quantity: {
                      gt: 0,
                    }
                  }
            },
            {
                term: {
                    "shop.is_approved": true
                }
            },
            {
                term: {
                    "shop.pause_mode": false
                }
            },
            {
                range: {
                    "group_buy.start_time": {
                        gt: new Date() 
                    }
                }
            },
            {
                range: {
                    "group_buy.end_time": {
                        lte: endTime 
                    }
                }
            }
        ]
    
        let mustNotQuery: any = [ 
            {
                exists: {
                  "field": "deleted_at"
                }
              }    
        ]
    
        boolQuery.must = mustQuery;
        boolQuery.must_not = mustNotQuery;
    
        const sort = [
            { "group_buy.start_time": "desc" },
        ]
    
        return { query: {bool: boolQuery} , sort};
    }
}
export default ProductService;