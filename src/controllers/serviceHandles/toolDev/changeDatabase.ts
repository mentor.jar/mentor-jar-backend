import RoomChat from '../../../models/RoomChat';
import { ProductRepo } from '../../../repositories/ProductRepo';
import { RoomChatRepo } from '../../../repositories/RoomChatRepo';
import { ShopRepo } from '../../../repositories/ShopRepo';
import { StreamSessionRepository } from '../../../repositories/StreamSessionRepository';
import { UserRepo } from '../../../repositories/UserRepo';
import { InMemoryAuthUserStore } from '../../../SocketStores/AuthStore';
import { InMemoryMessageForRoomStore } from '../../../SocketStores/MessageStore';
import { cloneObj, ObjectId } from '../../../utils/helper';

export class ChangeDatabaseService {
    static async clearRoomChatGreater2() {
        let result = await RoomChatRepo.getInstance()
            .getModel()
            .aggregate([
                {
                    $project: {
                        _id: '$_id',
                        type: '$type',
                        size_of_users: { $size: '$users' },
                    },
                },
                {
                    $match: {
                        size_of_users: { $gt: 2 },
                        type: 'CHAT',
                    },
                },
                {
                    $project: { _id: '$_id' },
                },
            ]);
        let list_id = result.map(element => {
            return element._id;
        });
        // return await RoomChatRepo.getInstance().deleteMany({
        //     _id: { $in: list_id },
        // });
        return list_id;
    }

    static async clearProductNotUser() {
        let productRepo = ProductRepo.getInstance();
        let relations = [
            productRepo
                .getModel()
                .relationship()
                .variants(),
            productRepo
                .getModel()
                .relationship()
                .productDetailInfos(),
            productRepo
                .getModel()
                .relationship()
                .shops(),
            productRepo
                .getModel()
                .relationship()
                .categories(),
        ];
        let products = await productRepo.getExpandValues(
            {
                deleted_at: null,
            },
            relations
        );
        products = cloneObj(products);
        let list_id: any = [];
        let shopRepo = ShopRepo.getInstance();
        for (const product of products) {
            let detail_shop: any = await shopRepo.getDetailShop(product.shop_id);
            if (!detail_shop || (detail_shop && !detail_shop.user))
                list_id.push(product._id);
        }

        // return await productRepo.deleteMany({ _id: { $in: list_id } });
        return list_id;
    }

    static async updateViewMaxStreamSession() {
        let stream_session_repo = StreamSessionRepository.getInstance();
        let stream_sessesions = await stream_session_repo.find({});
        for (let stream_session of stream_sessesions) {
            let room_chat = await RoomChatRepo.getInstance().findById(stream_session.room_chat_id.toString());
            let view_max = room_chat ? room_chat.users.length : 1;
            let user_onlines = room_chat.users.filter(user => {
                return user.is_online;
            });
            let view = user_onlines.length || 1;
            await stream_session_repo.findOneAndUpdate({ _id: stream_session._id.toString() }, { view_max, view });
        }
        return await stream_session_repo.find({});
    }

    static async updateUserInSocket(user_id) {
        const userRepo = UserRepo.getInstance();
        const shopRepository = ShopRepo.getInstance();
        const authStore = InMemoryAuthUserStore.getInstance();
        const roomStore = InMemoryMessageForRoomStore.getInstance();
        let user = await userRepo.getUserById(
            ObjectId(user_id.toString())
        );
        await authStore.saveUser(user);
        let rooms: any = await RoomChatRepo.getInstance().find({
            users: {
                $elemMatch: { _id: user_id },
            },
        })
        let promises: any = [];
        let i = 1;
        console.log("Amount child job: " + rooms.length);
        for (let room of rooms) {
            // promises.push(new Promise((res, rej) => {
            try {
                room.users = room.users.map(userRoom => {
                    if (userRoom._id.toString() == user_id) {
                        userRoom.userName = user.userName;
                        userRoom.avatar = user.avatar;
                        userRoom.email = user.email;
                        userRoom.gender = user.gender;
                        userRoom.shop_id = user.shop_id;
                    }
                    return userRoom;
                })
                // room.messages = [];
                room.messages = room.messages.map(message => {
                    if (message.user_id.toString() == user_id && message.user) {
                        message.user.userName = user.userName;
                        message.user.avatar = user.avatar;
                        message.user.email = user.email;
                        message.user.gender = user.gender;
                        message.user.shop_id = user.shop_id;
                    }
                    return message;
                })
                await RoomChatRepo.getInstance().findOneAndUpdate({ _id: ObjectId(room._id) }, {
                    users: room.users,
                    messages: room.messages
                })
                await roomStore.getRoom(room._id.toString()).then(roomDetail => {
                    if (roomDetail) {
                        roomDetail.users = room.users
                        roomDetail.messages = room.messages
                        roomStore.saveRoom(roomDetail);
                    }
                })
                // res(true);
            } catch (error) {
                // rej(error);
            }
            // }));

        }

        // await Promise.all(promises);
        console.log("done " + user_id);
        return user_id;
    }

    static async mergeRedisToDB(id = []) {
        const roomChatRepo = RoomChatRepo.getInstance();
        const roomStore = InMemoryMessageForRoomStore.getInstance()
        let key_room_chats = await roomStore.getFullKeyRoom()
        if (id.length) {
          key_room_chats = id
        }
        let rooms = await roomChatRepo.find({
          _id: { $in: key_room_chats }
        })
        let listPromise: any = rooms.map(room => {
          return new Promise(async (resolve, reject) => {
            let roomRedis = await roomStore.getRoom(room._id.toString())
            let last_message = roomRedis.last_message
            let lengthMessage = roomRedis.messages.length;
            for (let i = lengthMessage - 1; i >= 0; i--) {
              let message = roomRedis.messages[i];
              if (!message.deleted_at) {
                lengthMessage = message
                break
              }
            }
            await roomChatRepo.update(room._id.toString(), {
              messages: roomRedis.messages,
              users: roomRedis.users,
              last_message: lengthMessage,
              file_storages: roomRedis.file_storages,
              last_object: roomRedis.last_object || null
            })
            resolve(room._id)
          })
        })
        console.log(await Promise.all(listPromise))
      }
    
}
