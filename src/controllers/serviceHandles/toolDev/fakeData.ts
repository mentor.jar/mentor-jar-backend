import { ShippingStatus } from '../../../models/enums/order';
import { OrderRepository } from '../../../repositories/OrderRepo';
import { ProductRepo } from '../../../repositories/ProductRepo';
import TrackingRepo from '../../../repositories/TrackingRepo';
import { UserRepo } from '../../../repositories/UserRepo';
import TrackingService from '../../../services/tracking';
import { TrackingType } from '../../../services/tracking/definitions';
import { cloneObj, ObjectId, randomInt } from '../../../utils/helper';

export class FakeDataService {
    static async chartOrder() {
        // admin stag
        let shop_id = '60484013a8a26111b4617606';
        let orderRepo = await OrderRepository.getInstance();
        let query_other = [
            // {
            //     shipping_status: {
            //         $in: [ShippingStatus.PENDING],
            //     },
            // },
        ];
        let result = await orderRepo.getOrderOfShop(
            shop_id,
            null,
            null,
            query_other
        );
        result = cloneObj(result);
        let ship_status = [
            ShippingStatus.PENDING,
            ShippingStatus.CANCELED,
            ShippingStatus.RETURN,
            ShippingStatus.SHIPPED,
            ShippingStatus.SHIPPING,
            ShippingStatus.WAIT_TO_PICK,
        ];
        let res: any = [];
        for (const order of result) {
            // let now = new Date();
            // let day = randomInt(0, 30);
            // now.setDate(now.getDate() - day);
            // let x: any = await orderRepo.findOneAndUpdate(
            //     { _id: ObjectId(order._id.toString()) },
            //     {
            //         updated_at: now,
            //     }
            // );
            // res.push(x);
        }
        return res;
    }

    static async trackingUser() {
        // admin stag
        let shop_id = '60484013a8a26111b4617606';
        let user_id = '60484013a8a26111b4617606';
        const trackingRepo = TrackingRepo.getInstance();
        const productRepo = ProductRepo.getInstance();
        const userRepo = UserRepo.getInstance();

        let userTrackings = await userRepo
            .getModel()
            .find({ _id: { $ne: ObjectId('5feeb54619b159001966ad20') } })
            .skip(40)
            .limit(40);
        userTrackings = cloneObj(userTrackings);
        let search_query = {
            shop_id,
            filter_type: 'ALL',
        };
        let action_type = [
            TrackingType.ViewProduct,
            TrackingType.AddProductToCartStream,
        ];
        let products = await productRepo.findFilter(search_query, null);
        let list_product_id = products.map(product => ObjectId(product._id));
        let result: any = [];
        for (const user of userTrackings) {
            let amountAccess = randomInt(1, 5);
            for (let i = 1; i <= amountAccess; i++) {
                let index_product = randomInt(0, list_product_id.length - 1);
                let now = new Date();
                let day = randomInt(0, 30);
                now.setDate(now.getDate() - day);
                const tracking = await trackingRepo.create({
                    target_id: list_product_id[index_product],
                    target_type: 'Product',
                    actor_id: user._id,
                    actor_type: 'User',
                    action_type: action_type[randomInt(0, 1)],
                    count_number: 1,
                    created_at: now,
                    updated_at: now,
                    visited_ats: [now],
                });
                result.push(tracking);
            }
        }
        return result;
    }
}
