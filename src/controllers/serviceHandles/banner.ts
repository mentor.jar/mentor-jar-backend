import { BannerDTO } from '../../DTO/BannerDTO';
import BannerRepo from '../../repositories/BannerRepo';
import IPaginateData from '../../repositories/interfaces/IPaginateData';
import { NotFoundError } from '../../base/customError';
import { makeShortenUrl } from '../../utils/utils';
import TopShopBannerRepo from '../../repositories/TopShopBanner';
import { TopShopBannerDTO } from '../../DTO/TopShopBanner';
import { ShopRepo } from '../../repositories/ShopRepo';
const repository = BannerRepo.getInstance();
const topShopBannerRepo = TopShopBannerRepo.getInstance();
const shopRepo = ShopRepo.getInstance();

export interface CreateBannerRequest {
  name: String,
  image: String,
  promo_link: String,
  products: Array<String>,
  start_time: Date,
  end_time: Date,
  shop_id: String,
  description?: String,
}

export interface CreateBannerSystemRequest {
  name: String,
  image: String,
  images: Array<any>,
  promo_link?: String,
  products?: Array<String>,
  start_time: Date,
  end_time: Date,
  shop_id: null,
  order: Number,
  classify?: String,
  position?: String,
  vouchers?: Array<String>
  advance_actions?: Object,
  priority?: Number,
  shorten_link?: String,
  description?: String,
}

export interface UpdateBannerRequest {
  name: String,
  image: String,
  promo_link: String,
  products: Array<String>,
  start_time: Date,
  end_time: Date,
  description?: String,
}

export interface UpdateBannerSystemRequest {
  name: String,
  image: String,
  images: Array<any>,
  promo_link?: String,
  products?: Array<String>,
  start_time: Date,
  end_time: Date,
  order: Number,
  classify?: String,
  position?: String,
  vouchers?: Array<String> ,
  advance_actions?: Object,
  priority?: Number,
  shorten_link?: String,
  description?: String,
}

export interface CreateTopShopBannerRequest {
  name: String,
  images: Array<any>,
  shop_ids: Array<any>,
}

export interface UpdateTopShopBannerRequest {
  name: String,
  images: Array<any>,
  shop_ids: Array<any>,
  is_active: Boolean,
}

class BannerService {

  private static _instance: BannerService;

  static get _() {
    if (!this._instance) {
      this._instance = new BannerService();
    }
    return this._instance;
  }

  getCompleteData(model) {
    const bannerRepo = BannerRepo.getInstance();
    return bannerRepo.getBannerById(model._id.toString())
  }

  async getAllBanners(handleParams: any): Promise<IPaginateData> {
    const { params, paginate } = handleParams;
    const banners = await repository.getAllBanners(paginate.limit, paginate.page, params.filter_type, params.shop_id);
    banners.data = banners.data.map(banner => BannerDTO.newInstance(banner).completeDataWithVariantJSON());
    return banners;
  }

  async getAllBannerSystem(handleParams: any): Promise<IPaginateData> {
    const { params, paginate } = handleParams;
    const banners = await repository.getAllBannerSystem(paginate.limit, paginate.page, params.filter_type, params.search);
    banners.data = banners.data.map(banner => BannerDTO.newInstance(banner).completeDataWithVariantJSON());
    return banners;
  }

  async getAllTopShopBanner(handleParams: any): Promise<IPaginateData> {
    const { limit, page } = handleParams;
    let banners = await topShopBannerRepo.getAllBanners(limit, page);

    banners.data = banners?.data?.map(banner => TopShopBannerDTO.newInstance(banner).completeDataJSON());
    return banners;
  }

  /**
   * 
   * @param shop_id 
   * @returns Promise<any>
   */
  async getBannersByShop(shop_id: string, limit, page, filter_type): Promise<IPaginateData> {
    const banners = await repository.getBannerByShopId(shop_id, limit, page, filter_type);
    banners.data = banners.data.map(banner => BannerDTO.newInstance(banner).completeDataJSON());
    return banners;
  }

  /**
   * 
   * @param shopId 
   * @returns Promise<any>
   */
  async getBannerShopExplore(shopId: string): Promise<IPaginateData> {
    const banners = await repository.getBannerShopExplore(shopId);
    return banners;
  }

  /**
   * 
   * @param id 
   * @returns Promises
   */
  async getBannerById(id: string) {
    const banner = await repository.findById(id);
    if (!banner) {
      throw new NotFoundError("Không tìm thấy");
    }
    const dbData = await this.getCompleteData(banner)
    const data = BannerDTO.newInstance(dbData).completeDataJSON();
    return data;
  }

  /**
   * 
   * @param id 
   * @returns Promises
   */
  async getBannerByIdCMS(id: string) {
    const banner = await repository.findById(id);
    if (!banner) {
      throw new NotFoundError("Không tìm thấy");
    }
    const dbData = await repository.getBannerById(id)
    const data = BannerDTO.newInstance(dbData).completeDataWithVariantJSON();
    return data;
  }

  async getTopShopBannerByIdCMS(id: string) {
    const banner = await topShopBannerRepo.findById(id);
    if (!banner) {
      throw new NotFoundError("Không tìm thấy");
    }
    const dbData = await topShopBannerRepo.getBannerById(id)
    const data = TopShopBannerDTO.newInstance(dbData).completeDataJSON();
    return data;
  }

  async createNewBanner(request: CreateBannerRequest) {
    const newInstance = await repository.create(request);
    const dbData = await this.getCompleteData(newInstance);
    const data = BannerDTO.newInstance(dbData).completeDataJSON();
    return data;
  }

  async createBannerSystem(request: CreateBannerSystemRequest) {
    // shorten link
    const prefix = request.classify === 'voucher' ? 'bv' : request.classify === 'product' ? 'bp' : 'ba'
    let shorten_link = makeShortenUrl(6, prefix)
    while(await repository.findOne({shorten_link}) !== null) {
        shorten_link = makeShortenUrl(6, prefix)
    }
    request.shorten_link = shorten_link
    const newInstance = await repository.create(request);
    const dbData = await this.getCompleteData(newInstance);
    const data = BannerDTO.newInstance(dbData).completeDataJSON();
    return data;
  }

  async createTopShopBanner(request: CreateTopShopBannerRequest) {
    const newInstance = await topShopBannerRepo.create(request);

    const data = TopShopBannerDTO.newInstance(newInstance).toSimpleJSON();
    return data;
  }

  async updateBanner(id: string, request: UpdateBannerRequest) {
    const currentInstance = await repository.findById(id);
    if (!currentInstance) {
      throw new NotFoundError("Không tìm thấy");
    }
    currentInstance.name = request.name;
    currentInstance.image = request.image;
    currentInstance.promo_link = request.promo_link;
    currentInstance.products = request.products;
    currentInstance.start_time = request.start_time;
    currentInstance.end_time = request.end_time;
    currentInstance.description = request.description;

    const updatedInstance = await repository.update(id, currentInstance);

    const dbData = await this.getCompleteData(updatedInstance)
    const data = BannerDTO.newInstance(dbData).completeDataJSON();
    return data;
  }

  async updateBannerSystem(id: string, request: UpdateBannerSystemRequest) {
    const currentInstance = await repository.findById(id);
    if (!currentInstance) {
      throw new NotFoundError("Không tìm thấy");
    }
    currentInstance.name = request.name;
    currentInstance.promo_link = request.promo_link;
    currentInstance.products = request.products;
    currentInstance.start_time = request.start_time;
    currentInstance.end_time = request.end_time;
    currentInstance.order = request.order;
    currentInstance.classify = request.classify;
    currentInstance.position = request.position;
    currentInstance.description = request.description;
    
    if(request.classify === "product") {
      currentInstance.vouchers = []
      currentInstance.products = request.products
    }
    else {
      currentInstance.products = []
      currentInstance.vouchers = request.vouchers
    }

    if(currentInstance.classify === 'product') {
      if(request.classify === "voucher") {
        currentInstance.shorten_link = currentInstance.shorten_link.replace('/bp/', '/bv/')
      }
      else if(request.classify !== "product") {
        currentInstance.shorten_link = currentInstance.shorten_link.replace('/bp/', '/ba/')
      }
    }
    else if(currentInstance.classify === 'voucher') {
      if(request.classify === "product") {
        currentInstance.shorten_link = currentInstance.shorten_link.replace('/bv/', '/bp/')
      }
      else if(request.classify !== "voucher") {
        currentInstance.shorten_link = currentInstance.shorten_link.replace('/bv/', '/ba/')
      }
    }
    else {
      if(request.classify === "voucher") {
        currentInstance.shorten_link = currentInstance.shorten_link.replace('/ba', '/bv')
      }
      else if(request.classify === "product") {
        currentInstance.shorten_link = currentInstance.shorten_link.replace('/ba', '/bp')
      }
    }

    const updatedInstance = await repository.update(id, currentInstance);

    const dbData = await this.getCompleteData(updatedInstance)
    const data = BannerDTO.newInstance(dbData).completeDataJSON();
    return data;
  }

  async updateTopShopBanner(id: string, request: UpdateTopShopBannerRequest) {
    const instanceBanner = await topShopBannerRepo.findById(id);

    if (!instanceBanner) {
      throw new NotFoundError("Không tìm thấy");
    }

    Promise.all(
      instanceBanner?.shop_ids.map(async (id) => {
          shopRepo.update(id, {
              system_banner: null,
          });
      })
    );

    const result: any = await topShopBannerRepo.update(id, request);

    await Promise.all(
      result.shop_ids.map(async(id) => 
        shopRepo.update(id, {
          system_banner: {
            name: result.name,
            images: result.images,
            is_active: result.is_active,
          }
        })
      )
    )

    return TopShopBannerDTO.newInstance(result).toSimpleJSON();
  }

  async setActiveBanner(id: string, instance) {
    const currentInstance = await repository.findById(id);
    if (!currentInstance) {
      throw new NotFoundError("Không tìm thấy");
    }
    currentInstance.is_active = instance.is_active;
    const updatedInstance = await repository.update(id, currentInstance);
    const dbData = await this.getCompleteData(updatedInstance)
    const data = BannerDTO.newInstance(dbData).completeDataJSON();
    return data;
  }
  async deleteBanner(id: string) {
    const result = await repository.delete(id);
    return result;
  }

  async deleteTopShopBanner(id: string) {
    const instanceBanner = await topShopBannerRepo.findById(id);

    Promise.all(
      instanceBanner?.shop_ids.map(async (id) => {
          shopRepo.update(id, {
              system_banner: null,
          });
      })
    );

    topShopBannerRepo.delete(id);
  }

  countBannerClick(id: string) {

  }

  checkShopOwnBanner(shopId, bannerId) {
    return repository.getBanner(shopId, bannerId);
  }
}
export default BannerService;