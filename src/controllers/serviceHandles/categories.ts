
// data là mảng danh sách category
// parent_id là id của category cha

import { CATEGORIES_LOCALIZED } from "../../base/variable";
import { cloneObj } from "../../utils/helper";

// res tham chiếu để lưu lại record.
export function recursionTree(data, parent_id, res) {
    for (let i = 0; i < data.length; i++) {
        let item = data[i];
        // Nếu là chuyên mục con thì push vào
        if (item.parent_id == parent_id) {
            item.childs = [];
            res.push(item);
            // Tiếp tục đệ quy để tìm chuyên mục con của chuyên mục đang lặp
            recursionTree(data, item._id, item.childs);
        }
    }
}

export function recursionTreeByLocalized(data, parent_id, res, language) {
    for (let i = 0; i < data.length; i++) {
        let item = data[i];
        if (item.parent_id == parent_id) {
            let categoryLocalized = CATEGORIES_LOCALIZED.get(item.name.toLowerCase())
            categoryLocalized && categoryLocalized[language] ? item.name = categoryLocalized[language] : '';

            item.childs = [];
            res.push(item);
            recursionTreeByLocalized(data, item._id, item.childs, language);
        }
    }
}

export function recursionLocalizedCategory(data, language) {
    for (let i = 0; i < data.length; i++) {
        if (data[i]) {
            let categoryLocalized = CATEGORIES_LOCALIZED.get(data[i].name.toLowerCase())
            categoryLocalized && categoryLocalized[language] ? data[i].name = categoryLocalized[language] : '';

            if ( data[i].childs && data[i].childs.length > 0) {
                recursionLocalizedCategory(data[i].childs, language);
            }
        }
    }
}

export function getSnakeFromTree(tree, parent_id, res, level = 0) {
    let findArrayContainCate: any = [];
    res.map(
        e => {
            if (e[e.length - 1]._id == parent_id)
                findArrayContainCate = e;
        }
    )
    tree.map((category) => {
        let arrCate: any = cloneObj(findArrayContainCate);
        let childs = category.childs;
        if (category.parent_id == parent_id) {
            let cate = {
                _id: category._id,
                name: category.name,
                level: level,
                parent_id: category.parent_id,
                childs: category.childs.length
            }
            arrCate.push(cate);
            res.push(arrCate)
            getSnakeFromTree(childs, category._id, res, level + 1)
        }

    })
}

export function recursionGetListIdCategory(data, parent_id, res) {
    for (let i = 0; i < data.length; i++) {
        let item = data[i];
        // Nếu là chuyên mục con thì push vào
        if (item.parent_id == parent_id) {
            res.push(item._id);
            // Tiếp tục đệ quy để tìm chuyên mục con của chuyên mục đang lặp
            recursionGetListIdCategory(data, item._id, res);
        }
    }
}

export const sortByName = (objectA, objectB) => {
    return objectA.name.localeCompare(objectB.name);
}

// DESC
export const sortByPriority = (objectA, objectB) => {
    return objectB.priority - objectA.priority;
}