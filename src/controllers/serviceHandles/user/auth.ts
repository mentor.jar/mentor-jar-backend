import axios from 'axios';
import keys from '../../../config/env/keys';

class AuthService {
    private static _instance: AuthService;

    static get _() {
        if (!this._instance) {
            this._instance = new AuthService();
        }
        return this._instance;
    }

    async getAllPermissionByUser({ token }) {
        try {
            let url = `${keys.community_api.url}/v1/cms/role-permission/user/all-permission`;
            const config = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`,
                },
            };

            const response = await axios.get(url, config);
            return response.data.data;
        } catch (error) {
            Promise.reject(error);
        }
    }
}
export default AuthService;
