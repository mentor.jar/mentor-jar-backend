import { UserRepo } from "../../../repositories/UserRepo";
import { cloneObj } from "../../../utils/helper";
import axios from "axios";
import keys from "../../../config/env/keys";
import { ShopRepo } from "../../../repositories/ShopRepo";
import twilio from "twilio";

const userRepo = UserRepo.getInstance();

class VerifyService {

    private static _instance: VerifyService;

    static get _() {
        if (!this._instance) {
            this._instance = new VerifyService();
        }
        return this._instance;
    }

    async saveUserData(user, phone_number) {
        user = cloneObj(user)
        const code = Math.floor(100000 + Math.random() * 900000);
        // const code = 123456
        let phone;

        // Save info to database
        const updateUser = { ...user }
        const currentTime = new Date().getTime()
        updateUser.phone_verify.code = code
        updateUser.phone_verify.expire = new Date(currentTime + 2 * 60000)
        if (phone_number) {
            updateUser.phoneNumber = phone_number.toString();
            phone = phone_number;
        } else {
            phone = user.phoneNumber;
        }
        user = await userRepo.update(user._id, updateUser)
        return { phone, code };
    }

    async sendVerifyPhoneTwillio(user, phone_number) {
        let { phone, code } = await this.saveUserData(user, phone_number);
        // TWILIO
        const accountSid = process.env.TWILIO_ACCOUNT_SID;
        const authToken = process.env.TWILIO_AUTH_TOKEN;
        const phoneNumberTwilio = process.env.TWILIO_PHONE_NUMBER;
        const client = twilio(accountSid, authToken);
        if (!phone.startsWith("+")) {
            phone = `+${phone}`;
        }

        const result = await client.messages
            .create({
                body: `${code} is your BIDU verified code`,
                from: phoneNumberTwilio,
                to: phone
            })
        return result;
    }

    async sendVerifyPhone(user, phone_number) {
        const esmsUrl = keys.esms.esms_url;
        let { phone, code } = await this.saveUserData(user, phone_number);

        // ESMS
        const content = `${code} la ma xac minh dang ky Baotrixemay cua ban`;
        // SmsType: 2, type with brandname
        let params = {
            Phone: phone,
            Content: content,
            ApiKey: keys.esms.esms_api_key,
            SecretKey: keys.esms.esms_secret_key,
            SmsType: 2,
            Brandname: keys.esms.esms_brand_name
        }
        const result = await axios.get(esmsUrl, { params });
        return result.data;


        // SPEEDSMS
        // const speedSmsUrl = keys.speed_sms.speed_sms_url;
        // const accessToken = keys.speed_sms.speed_sms_access_token;
        // const content = `Ma xac thuc SPEEDSMS cua ban la ${code}`;
        // var buf = new Buffer(accessToken + ':x');
        // var auth = "Basic " + buf.toString('base64');
        // var params = {
        //     to: phone,
        //     content: content,
        //     sms_type: 3,
        //     sender: "SPEEDSMS"
        // };
        // const result = await axios.post(speedSmsUrl,
        //     params,
        //     {
        //         headers: { 'Authorization': auth }
        //     }
        // );

        // return result.data;
    }

}
export default VerifyService;