import axios from "axios";
import { URI_CREATE_STREAMFILE, URL_INCOMING, URL_STREAMFILE } from "../../base/variable";
import { config } from 'dotenv'
import { moveFile } from "../../utils/fileHelper";
import { randomInt } from "../../utils/helper";
import keys from "../../config/env/keys";
const fs = require('fs')
config()

export async function createStreamFile(streamfileName: String) {
    try {
        let sessionUrl = URL_STREAMFILE;
        let wowzaUsername: any = process.env.WOWZA_USERNAME;
        let wowzaPassword: any = process.env.WOWZA_PASSWORD;
        await axios.post(sessionUrl, {
            "name": streamfileName,
            "serverName": "_defaultServer_",
            "uri": URI_CREATE_STREAMFILE
        }, {
            auth: {
                username: wowzaUsername,
                password: wowzaPassword
            }
        });
    } catch (error) {
        console.log(error);
    }
}

export async function getListStreamFile() {
    try {
        let sessionUrl = URL_STREAMFILE;
        let wowzaUsername: any = process.env.WOWZA_USERNAME;
        let wowzaPassword: any = process.env.WOWZA_PASSWORD;
        const listStreamFile = await axios.get(sessionUrl, {
            auth: {
                username: wowzaUsername,
                password: wowzaPassword
            }
        });
        return listStreamFile.data.streamFiles;
    } catch (error) {
        console.log(error);
    }
}

export async function getDetailStreamSession(id: string) {
    try {
        let sessionUrl = URL_INCOMING + id;
        let wowzaUsername: any = process.env.WOWZA_USERNAME;
        let wowzaPassword: any = process.env.WOWZA_PASSWORD;
        const streamDetail = await axios.get(sessionUrl, {
            auth: {
                username: wowzaUsername,
                password: wowzaPassword
            }
        });
        return streamDetail.data;
    } catch (error) {

    }
    return null;
}

export async function connectStreamFile(streamName: String) {
    try {
        let sessionUrl = URL_STREAMFILE + "/" + streamName + "/actions/connect?connectAppName=live&appInstance=_definst_&mediaCasterType=rtp";
        let wowzaUsername: any = process.env.WOWZA_USERNAME;
        let wowzaPassword: any = process.env.WOWZA_PASSWORD;
        const listStreamFile = await axios.put(sessionUrl, {}, {
            auth: {
                username: wowzaUsername,
                password: wowzaPassword
            }
        });
        // console.log(listStreamFile.data.message);
        return listStreamFile;

    } catch (error) {
        console.log(error);
    }
}

export async function disconnectStreamFile(streamName: String) {
    try {
        let sessionUrl = URL_INCOMING + streamName + "/actions/disconnectStream";
        let wowzaUsername: any = process.env.WOWZA_USERNAME;
        let wowzaPassword: any = process.env.WOWZA_PASSWORD;
        const listStreamFile = await axios.put(sessionUrl, {}, {
            auth: {
                username: wowzaUsername,
                password: wowzaPassword
            }
        });
        return listStreamFile;
    } catch (error) {
        console.log(error);
    }
}

export async function moveFileStreamToFolderStreamUser(user_id: string, session_id: string) {
    const server = process.env.NODE_ENV === 'production' ? 'production' : 'staging';
    let dirFile = `uploads/video-live-streams`;
    let dirFileServer = `uploads/video-live-streams/${server}`;

    let newDirStream = `${dirFileServer}/${user_id}/${session_id}`
    var file_results: any = [];
    try {
        let files = fs.readdirSync(dirFile);
        for (let i in files) {
            if (!files.hasOwnProperty(i)) continue;

            let oldPath = dirFile + '/' + files[i];
            // console.log(oldPath, user_id, oldPath.search(user_id))
            if (!fs.statSync(oldPath).isDirectory() && oldPath.search(user_id) > -1) {
                if (!fs.existsSync(`${dirFileServer}/${user_id}`)) {
                    fs.mkdirSync(`${dirFileServer}/${user_id}`, { recursive: true });
                }
                if (!fs.existsSync(newDirStream)) {
                    fs.mkdirSync(newDirStream, { recursive: true });
                }
                let size = fs.readdirSync(newDirStream).length;
                let newNameFile = `${Date.now()}_stream_${size}.mp4`;
                let newPath = `${newDirStream}/${newNameFile}`
                let checkMove = await moveFile(oldPath, newPath)
                if (checkMove)
                    file_results.push(`${keys.host}/${newPath}`)
            }
        }
    } catch (error) {
        console.log(error)
    }
    return file_results;

}