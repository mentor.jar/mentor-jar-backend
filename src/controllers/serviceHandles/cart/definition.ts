export interface ShopItemCheckout {
    _id: string,
    items: Array<ItemCheckout>

}
export interface ItemCheckout {
    _id: string,
    product_id: string,
    variant_id: string,
    quantity: number,
    price: number,
}