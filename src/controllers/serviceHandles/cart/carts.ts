import { CartItemDTO } from "../../../DTO/CartItem";
import { AddressRepo } from "../../../repositories/AddressRepo";
import { CartItemRepo } from "../../../repositories/CartItemRepo";
import { PaymentMethodRepo } from "../../../repositories/PaymentMethodRepo";
import { ShippingMethodRepo } from "../../../repositories/ShippingMethodRepo";
import { VoucherRepository } from "../../../repositories/VoucherRepository";
import { cloneObj } from "../../../utils/helper";

const cartItemRepo = CartItemRepo.getInstance();
const voucherRepo = VoucherRepository.getInstance();
const addressRepo = AddressRepo.getInstance();
const shippingMethodRepo = ShippingMethodRepo.getInstance();
const paymentMethodRepo = PaymentMethodRepo.getInstance();


export const convertItemFromCart = async (itemIds) => {
    let items = await cartItemRepo.getItemsCheckoutPage(itemIds);
    items = cloneObj(items)
    items = items.map(res => {
        const items = res.items.map(item => {
            item = cloneObj(item)
            item.product = item.products[0] || null
            item.product.variant = item.product.variants[0] || null
            delete item.products
            delete item.product.variants
            return item
        })
        res.shop = res.shop[0] || null
        res.items = items
        return res
    });
    return items
}

const updateShippingMethod = async (shopTotal, shop) => {
    // update shipping method
    shopTotal.forEach(element => {
        shop.forEach(el => {
            if (el.shop_id == element._id) {
                element.shipping = {};
                element.shipping.shipping_method_id = el.shipping_method_id;
                element.shipping.shipping_fee = el.shipping_fee;
            }

        });
    });

    const listShippingMethod = await shippingMethodRepo.getAllShippingMethod();
    shopTotal.forEach(shop => {
        listShippingMethod.forEach(method => {
            if (shop.shipping.shipping_method_id == method._id.toString()) {
                shop.shipping_method = method;
            }
        });
    });
    return shopTotal;
}