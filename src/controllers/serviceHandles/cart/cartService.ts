import moment from "moment";
import { NotFoundError } from "../../../base/customError";
import { COSTS_INCURRED, KOREA, OVERSEAS_SHIPPING_FEE_BY_GAM } from "../../../base/variable";
import { CartItemDTO } from "../../../DTO/CartItem";
import { ShippingMethodQuery } from "../../../models/enums/order";
import { AddressRepo } from "../../../repositories/AddressRepo";
import { CartItemRepo } from "../../../repositories/CartItemRepo";
import { OrderRepository } from "../../../repositories/OrderRepo";
import { PaymentMethodRepo } from "../../../repositories/PaymentMethodRepo";
import { ShippingMethodRepo } from "../../../repositories/ShippingMethodRepo";
import { ShopRepo } from "../../../repositories/ShopRepo";
import { VoucherRepository } from "../../../repositories/VoucherRepository";
import GHTKService from "../../../services/3rd/shippings/GHTK";
import { NATIONAL_TYPE_SHIPPING, PRODUCT_TYPE_SHIPPING } from "../../../services/3rd/shippings/viettelpost/definitions";
import ViettelPostService from "../../../services/3rd/shippings/viettelpost/ViettelPost";
import { generateOrderRoomReferralByUser } from "../../../services/api/order";
import CacheService from "../../../services/cache";
import { InMemoryVoucherStore } from "../../../SocketStores/VoucherStore";
import { cloneObj, ObjectId, timeOutOfPromise } from "../../../utils/helper";

const cartItemRepo = CartItemRepo.getInstance();
const voucherRepo = VoucherRepository.getInstance();
const addressRepo = AddressRepo.getInstance();
const shippingMethodRepo = ShippingMethodRepo.getInstance();
const paymentMethodRepo = PaymentMethodRepo.getInstance();
const shopRepo = ShopRepo.getInstance();
const orderRepo = OrderRepository.getInstance();
const cacheService = CacheService._
const voucherAndOrderCache = InMemoryVoucherStore.getInstance();

export class CartService {

    private merchandiseSubtotal: number = 0;
    private shippingSubtotal: number = 0;
    private shippingTotal: number = 0;
    private shippingTotalDiscount: number = 0;
    private totalDiscountOfShop: number = 0;
    private totalDiscount: number = 0;
    private totalCashBackDiscount: number = 0;
    private totalPayment: number = 0;
    private orderService: string;
    // List Item in cart
    private orderShop;
    private result: any = {};

    getCheckout = async (req) => {
        let { system_voucher: systemVoucher, shops: shop, payment_method, address_id } = req.body;
        let itemIds: any = [];
        shop.forEach(element => {
            element.items.forEach(item => {
                itemIds.push(item);
            });
        });

        let [address, paymentMethodObject, items, listShippingMethod] = await Promise.all([
            (async () => {
                const data = address_id ? await addressRepo.findWithUser(address_id, req.user._id) : await addressRepo.getDefaultAddress(req.user._id);
                return data
            })(),
            (async () => {
                if(!cacheService.has('payment_methods')) {
                    const data = await paymentMethodRepo.find({is_active: true});
                    cacheService.set('payment_methods', data)
                    return data.find(item => item._id.toString() === payment_method.toString())
                }
                else {
                    const data = await cacheService.get('payment_methods')
                    return data.find(item => item._id.toString() === payment_method.toString())
                }
            })(),
            (async () => {
                const data = await this.convertItemFromCart(itemIds);
                return data
            })(),
            (async () => {
                if(!cacheService.has('shipping_methods')) {
                    const data = await shippingMethodRepo.getAllShippingMethod();
                    cacheService.set('shipping_methods', data)
                    return data
                }
                else {
                    return cacheService.get('shipping_methods')
                }
            })()
        ])
        

        //if (!address) throw new NotFoundError("Không tìm thấy");
        items = items.filter(item => item.shop)
        
        let shopTotal = items.map(item => CartItemDTO.newInstance(item).convertItemData());

        // UPDATE SHIPPING FEE
        shopTotal = await this.updateShippingFee(
            req,
            shopTotal,
            address,
            shop,
            listShippingMethod
        );
        [shop, shopTotal] = await this.updateShippingMethod(
            shopTotal,
            shop,
            listShippingMethod
        );

        this.orderShop = shopTotal.map(item => CartItemDTO.newInstance(item).calculateOrderTotal());


        this.orderShop.forEach(shop => {
            this.merchandiseSubtotal += shop.shop_merchandise_total;
            this.totalPayment += shop.shop_total
            this.shippingSubtotal += shop.shipping.shipping_fee;
            this.shippingTotal += shop.shipping.shipping_fee;
        });

        // HANDLE VOUCHER

        // Get all shopId in cart
        const shopIds: Array<String> = this.orderShop.map(shop => shop._id);
        // Get all productId in cart
        const productIds: Array<String> = [];
        this.orderShop.forEach(shop => {
            shop.items.forEach(item => {
                productIds.push(item.product_id);
            })
        })

        // Get all shipping method in cart
        const shippingMethodIds: Array<String> = shop.map(el => el.shipping_method_id);
        const disctionShippingIds = [...new Set(shippingMethodIds)]


        // Object Condition to find valid voucher
        const ObjectCondition: any = {
            total_value: this.merchandiseSubtotal,
            shops: shopIds,
            products: productIds,
            payment_method: payment_method,
            shipping_method: disctionShippingIds,
            user_id: req.user._id
        }

        //return result
        this.result.address = address;
        this.result.list_item = this.orderShop;
        this.result.system_free_shipping_voucher = null;
        this.result.system_discount_voucher = null;
        this.result.cash_back_voucher = null;
        this.result.cash_back_discount_voucher = null

        //handle system voucher
        await Promise.all([
            (async () => {
                if (systemVoucher) {
                    await this.handleSystemVoucher(req, systemVoucher, shop, ObjectCondition);
                    // must run after handleSystemVoucher 
                    await this.handleCashBackDiscountVoucher(systemVoucher?.cash_back_discount_voucher_id, ObjectCondition)
                }
            })(),
            (async () => {
                await this.handleCashBackVoucher(systemVoucher?.cash_back_voucher_id, ObjectCondition)
            })()
        ])

        if (shop) {
            await this.handleShopVoucher(req, shop, ObjectCondition);
            this.orderShop.forEach(shop => { 
                shop?.voucher?.discount_for_this_shop ? this.totalDiscountOfShop += shop?.voucher?.discount_for_this_shop : "";
            })
        }

        this.result.payment_method = paymentMethodObject;
        this.result.merchandise_subtotal = this.merchandiseSubtotal;
        this.result.shipping_total = this.shippingTotal
        this.result.shipping_subtotal = this.shippingSubtotal;
        this.result.shipping_total_discount = this.shippingTotalDiscount;
        this.result.total_discount_of_shop = this.totalDiscountOfShop;
        this.result.total_discount = this.totalDiscount;
        this.result.total_cash_back_discount = this.totalCashBackDiscount;
        this.result.total_payment = this.totalPayment;
        if(this.result.cash_back_voucher) {
            this.result.cash_back_voucher.discount_for_this_cart = this.result?.cash_back_next_order || 0
        }
        

        return this.result;
    }

    getGroupBuyCheckout = async (req) => {
        let { system_voucher: systemVoucher, shops: shop, payment_method, address_id, group_id, ref_from_id } = req.body;
        let itemIds: any = [];
        shop.forEach(element => {
            element.items.forEach(item => {
                itemIds.push(item);
            });
        });

        let [address, paymentMethodObject, items, listShippingMethod] = await Promise.all([
            (async () => {
                const data = address_id ? await addressRepo.findWithUser(address_id, req.user._id) : await addressRepo.getDefaultAddress(req.user._id);
                return data
            })(),
            (async () => {
                if(!cacheService.has('payment_methods')) {
                    const data = await paymentMethodRepo.find({is_active: true});
                    cacheService.set('payment_methods', data)
                    return data.find(item => item._id.toString() === payment_method.toString())
                }
                else {
                    const data = await cacheService.get('payment_methods')
                    return data.find(item => item._id.toString() === payment_method.toString())
                }
            })(),
            (async () => {
                const data = await this.convertItemFromCart(itemIds);
                return data
            })(),
            (async () => {
                if(!cacheService.has('shipping_methods')) {
                    const data = await shippingMethodRepo.getAllShippingMethod();
                    cacheService.set('shipping_methods', data)
                    return data
                }
                else {
                    return cacheService.get('shipping_methods')
                }
            })()
        ])
        

        //if (!address) throw new NotFoundError("Không tìm thấy");
        let shopTotal
        if(group_id) {
            shopTotal = items.map(item => CartItemDTO.newInstance(item).convertItemHaveGroupBuyData(group_id));
        } else {
            shopTotal = items.map(item => CartItemDTO.newInstance(item).convertItemData());
        }

        // UPDATE SHIPPING FEE
        shopTotal = await this.updateShippingFee(
            req,
            shopTotal,
            address,
            shop,
            listShippingMethod
        );
        [shop, shopTotal] = await this.updateShippingMethod(
            shopTotal,
            shop,
            listShippingMethod
        );

        this.orderShop = shopTotal.map(item => CartItemDTO.newInstance(item).calculateOrderTotal());


        this.orderShop.forEach(shop => {
            this.merchandiseSubtotal += shop.shop_merchandise_total;
            this.totalPayment += shop.shop_total
            this.shippingSubtotal += shop.shipping.shipping_fee;
            this.shippingTotal += shop.shipping.shipping_fee;
        });

        // HANDLE VOUCHER

        // Get all shopId in cart
        const shopIds: Array<String> = this.orderShop.map(shop => shop._id);
        // Get all productId in cart
        const productIds: Array<String> = [];
        this.orderShop.forEach(shop => {
            shop.items.forEach(item => {
                productIds.push(item.product_id);
            })
        })

        // Get all shipping method in cart
        const shippingMethodIds: Array<String> = shop.map(el => el.shipping_method_id);
        const disctionShippingIds = [...new Set(shippingMethodIds)]


        // Object Condition to find valid voucher
        const ObjectCondition: any = {
            total_value: this.merchandiseSubtotal,
            shops: shopIds,
            products: productIds,
            payment_method: payment_method,
            shipping_method: disctionShippingIds,
            user_id: req.user._id
        }

        //return result
        this.result.address = address;
        this.result.list_item = this.orderShop;
        this.result.system_free_shipping_voucher = null;
        this.result.system_discount_voucher = null;
        this.result.cash_back_voucher = null;
        this.result.cash_back_discount_voucher = null
        this.result.ref_from_id = ref_from_id

        //handle system voucher
        await Promise.all([
            (async () => {
                if (systemVoucher) {
                    await this.handleSystemVoucher(req, systemVoucher, shop, ObjectCondition);
                    // must run after handleSystemVoucher 
                    await this.handleCashBackDiscountVoucher(systemVoucher?.cash_back_discount_voucher_id, ObjectCondition)
                }
            })(),
            (async () => {
                await this.handleCashBackVoucher(systemVoucher?.cash_back_voucher_id, ObjectCondition)
            })()
        ])

        if (shop) {
            await this.handleShopVoucher(req, shop, ObjectCondition);
            this.orderShop.forEach(shop => { 
                shop?.voucher?.discount_for_this_shop ? this.totalDiscountOfShop += shop?.voucher?.discount_for_this_shop : "";
            })
        }

        if(group_id) this.result.ref_link = await generateOrderRoomReferralByUser();
        
        this.result.payment_method = paymentMethodObject;
        this.result.merchandise_subtotal = this.merchandiseSubtotal;
        this.result.shipping_total = this.shippingTotal
        this.result.shipping_subtotal = this.shippingSubtotal;
        this.result.shipping_total_discount = this.shippingTotalDiscount;
        this.result.total_discount_of_shop = this.totalDiscountOfShop;
        this.result.total_discount = this.totalDiscount;
        this.result.total_cash_back_discount = this.totalCashBackDiscount;
        this.result.total_payment = this.totalPayment;
        
        if(this.result.cash_back_voucher) {
            this.result.cash_back_voucher.discount_for_this_cart = this.result?.cash_back_next_order || 0
        }
        

        return this.result;
    }

    async handleSystemVoucher(req, systemVoucher, shop, ObjectCondition) {
        const { free_shipping_voucher_id, discount_voucher_id } = systemVoucher;
        // handle free_shipping system voucher, must run first
        if (free_shipping_voucher_id) {
            await this.handleSystemFreeShippingVoucherV2(free_shipping_voucher_id, ObjectCondition, shop);
        }

        // handle discount system voucher, must run after free shipping voucher
        if (discount_voucher_id) {
            await this.handleSystemDiscountVoucherV2(discount_voucher_id, ObjectCondition)
        }

    }

    async handleSystemFreeShippingVoucher(free_shipping_voucher_id, ObjectCondition, shop) {
        //find shop have maximum fee
        let shipping_fee_max = 0;
        let idShopMax: string = shop[0].shop_id;
        shop.forEach(el => {
            if (shipping_fee_max < el.shipping_fee) {
                shipping_fee_max = el.shipping_fee;
                idShopMax = el.shop_id;
            }
        })
        //check whether voucher is valid or not, if not, it will return an empty array
        let voucher = await voucherRepo.checkValidVoucherSystemIncart(ObjectCondition, free_shipping_voucher_id.toString());

        if (voucher.length && voucher.classify == 'free_shipping') {
            let shippingDiscount;
            let discount_for_this_cart = 0;
            // get budget used of voucher
            const budgetUsed:number = await orderRepo.getBudgetUsedOfVoucher(voucher[0]._id)

            this.orderShop.forEach(element => {
                shop.forEach(el => {
                    if (el.shop_id == element._id) {
                        element.shipping_fee = el.shipping_fee;
                    }
                });
                //check shop have max shipping fee
                if (element._id == idShopMax) {
                    // Check whether the voucher have discount_by_range_price
                    let value = 0;
                    // flag = 0: không có trường range, flag = 1: range rỗng, flag = 2: có trường range
                    let flag = 0;
                    if (voucher[0].discount_by_range_price && voucher[0].discount_by_range_price.length == 0) { flag = 1 }
                    if (voucher[0].discount_by_range_price && voucher[0].discount_by_range_price.length) {
                        const range = voucher[0].discount_by_range_price;
                        range.forEach(element => {
                            if (this.merchandiseSubtotal >= element.from && this.merchandiseSubtotal < element.to) {
                                value = element.value;
                            }
                            if (element.to == -1 && this.merchandiseSubtotal >= element.from) {
                                value = element.value
                            }
                        });
                        flag = 2;
                    }
                    let discount = 0;
                    if (voucher[0].type == 'price') {
                        if (value) {
                            discount = value;
                        }
                        if (value == 0 && (flag == 0 || flag == 1)) {
                            discount = voucher[0].value;
                        }
                    }
                    if (voucher[0].type == 'percent') {
                        if (value) {
                            discount = Math.round(value * shipping_fee_max / 100);
                        }
                        if (value == 0 && (flag == 0 || flag == 1)) {
                            discount = Math.round(voucher[0].value * shipping_fee_max / 100);
                        }
                    }

                    if (voucher[0].max_discount_value) {
                        discount = discount < voucher[0].max_discount_value ? discount : voucher[0].max_discount_value;
                    }

                    //check whether the discount great than the voucher's max budget
                    if(voucher[0].max_budget) {
                        discount = Math.min(discount, voucher[0].max_budget - budgetUsed > 0 ? voucher[0].max_budget - budgetUsed : 0)
                    }

                    shippingDiscount = shipping_fee_max - discount;

                    element.shipping_fee_discount = discount;
                    discount_for_this_cart = discount;
                    if (shippingDiscount < 0) {
                        discount_for_this_cart = shipping_fee_max;
                        element.shipping_fee_discount = shipping_fee_max;
                        shippingDiscount = 0;
                    }

                    let total = 0;
                    element.items.forEach(el => {
                        total += el.quantity * el.price;
                    });
                    element.shop_total = total + shippingDiscount;
                }
            });
            //re-calculate
            this.shippingSubtotal = 0;
            this.totalPayment = 0;
            this.orderShop.forEach(shop => {
                this.totalPayment += shop.shop_total;
                if (shop.shipping_fee_discount != 0) {
                    this.shippingSubtotal += shippingDiscount;
                } else {
                    this.shippingSubtotal += shop.shipping_fee;
                }
            });
            this.result.system_free_shipping_voucher = voucher[0];
            this.result.system_free_shipping_voucher.discount_for_this_cart = discount_for_this_cart;
            this.shippingTotalDiscount = discount_for_this_cart
        }
        else {
            this.result.system_free_shipping_voucher = null;
        }
    }

    async handleSystemFreeShippingVoucherV2(free_shipping_voucher_id, ObjectCondition, shop) {
        //find shop have maximum fee
        let shipping_fee_max = 0;
        let idShopMax: string = shop[0].shop_id;
        shop.forEach(el => {
            if (shipping_fee_max < el.shipping_fee) {
                shipping_fee_max = el.shipping_fee;
                idShopMax = el.shop_id;
            }
        })

        const voucher = await voucherRepo.checkValidVoucherOnCheckout(free_shipping_voucher_id, ObjectCondition, 'free_shipping')
    
        if(voucher) {
            let shippingDiscount;
            let discount_for_this_cart = 0;

            // get budget used of voucher
            const budgetUsed:number = await orderRepo.getBudgetUsedOfVoucher(voucher._id)

            this.orderShop.forEach(element => {
                shop.forEach(el => {
                    if (el.shop_id == element._id) {
                        element.shipping_fee = el.shipping_fee;
                    }
                });
                //check shop have max shipping fee
                if (element._id == idShopMax) {
                    // Check whether the voucher have discount_by_range_price
                    let value = 0;
                    // flag = 0: không có trường range, flag = 1: range rỗng, flag = 2: có trường range
                    let flag = 0;
                    if (voucher.discount_by_range_price && voucher.discount_by_range_price.length == 0) { flag = 1 }
                    if (voucher.discount_by_range_price && voucher.discount_by_range_price.length) {
                        const range = voucher.discount_by_range_price;
                        range.forEach(element => {
                            if (this.merchandiseSubtotal >= element.from && this.merchandiseSubtotal < element.to) {
                                value = element.value;
                            }
                            if (element.to == -1 && this.merchandiseSubtotal >= element.from) {
                                value = element.value
                            }
                        });
                        flag = 2;
                    }
                    let discount = 0;
                    if (voucher.type === 'price') {
                        if (value) {
                            discount = value;
                        }
                        if (value === 0 && (flag === 0 || flag === 1)) {
                            discount = voucher.value;
                        }
                    }
                    if (voucher.type === 'percent') {
                        if (value) {
                            discount = Math.round(value * shipping_fee_max / 100);
                        }
                        if (value === 0 && (flag === 0 || flag === 1)) {
                            discount = Math.round(voucher.value * shipping_fee_max / 100);
                        }
                    }

                    if (voucher.max_discount_value) {
                        discount = discount < voucher.max_discount_value ? discount : voucher.max_discount_value;
                    }

                    //check whether the discount great than the voucher's max budget
                    if(voucher.max_budget) {
                        discount = Math.min(discount, voucher.max_budget - budgetUsed > 0 ? voucher.max_budget - budgetUsed : 0)
                    }

                    shippingDiscount = shipping_fee_max - discount;

                    element.shipping_fee_discount = discount;
                    discount_for_this_cart = discount;
                    if (shippingDiscount < 0) {
                        discount_for_this_cart = shipping_fee_max;
                        element.shipping_fee_discount = shipping_fee_max;
                        shippingDiscount = 0;
                    }

                    let total = 0;
                    element.items.forEach(el => {
                        total += el.quantity * el.price;
                    });
                    element.shop_total = total + shippingDiscount;
                }
            });
            //re-calculate
            this.shippingSubtotal = 0;
            this.totalPayment = 0;
            this.orderShop.forEach(shop => {
                this.totalPayment += shop.shop_total;
                if (shop.shipping_fee_discount != 0) {
                    this.shippingSubtotal += shippingDiscount;
                } else {
                    this.shippingSubtotal += shop.shipping_fee;
                }
            });
            this.result.system_free_shipping_voucher = voucher;
            this.result.system_free_shipping_voucher.discount_for_this_cart = discount_for_this_cart;
            this.shippingTotalDiscount = discount_for_this_cart
        }
        else {
            this.result.system_free_shipping_voucher = null;
        }

    }

    async handleSystemDiscountVoucher(discount_voucher_id, ObjectCondition) {
        //check whether voucher is valid or not, if not, it will return an empty array
        let voucher = await voucherRepo.checkValidVoucherSystemIncart(ObjectCondition, discount_voucher_id.toString());
        if (voucher.length && voucher[0].classify == 'discount') {
            //shippingSubtotal = 0;
            // this.totalPayment = 0;
            let discount_for_this_cart = 0;
            // Check whether the voucher have discount_by_range_price
            let value = 0;
            let flag = 0;
            if (voucher[0].discount_by_range_price && voucher[0].discount_by_range_price.length == 0) { flag = 1 }
            if (voucher[0].discount_by_range_price && voucher[0].discount_by_range_price.length) {
                const range = voucher[0].discount_by_range_price;
                range.forEach(element => {
                    if (this.merchandiseSubtotal >= element.from && this.merchandiseSubtotal < element.to) {
                        value = element.value;
                    }
                    if (element.to == -1 && this.merchandiseSubtotal >= element.from) {
                        value = element.value
                    }
                });
                flag = 2;
            }
            let discount = 0;
            if (voucher[0].type == 'price') {
                if (value) {
                    discount = value
                }
                // value = 0 có 3 trường hợp: range ko có, range rỗng, không đủ điều kiện
                // flag = 0: không có trường range, flag = 1: range rỗng, flag = 2: có trường range
                if (value == 0 && (flag == 0 || flag == 1)) {
                    discount = voucher[0].value;
                }
                if (discount > this.merchandiseSubtotal) discount = this.merchandiseSubtotal;
            }
            if (voucher[0].type == 'percent') {
                if (value) {
                    discount = Math.round(this.merchandiseSubtotal * (value / 100));
                }
                if (value == 0 && (flag == 0 || flag == 1)) {
                    discount = Math.round(this.merchandiseSubtotal * (voucher[0].value / 100));
                }
            }

            if (voucher[0].max_discount_value) {
                discount = discount < voucher[0].max_discount_value ? discount : voucher[0].max_discount_value;
            }

            //check whether the discount great than the voucher's max budget
            if(voucher[0].max_budget) {
                const budgetUsed:number = await orderRepo.getBudgetUsedOfVoucher(voucher[0]._id)
                discount = Math.min(discount, voucher[0].max_budget - budgetUsed > 0 ? voucher[0].max_budget - budgetUsed : 0)
            }
            //re-calculate of shop
            this.orderShop.forEach(shop => {
                const rate = shop.shop_merchandise_total / this.merchandiseSubtotal;
                const discount_of_system = Math.round(rate * discount);
                shop.discount_of_system_voucher = discount_of_system;
                //shop.shop_total -= shop.discount_of_system_voucher;
            });

            this.totalDiscount += discount;
            discount_for_this_cart = discount;
            if (this.totalDiscount > this.merchandiseSubtotal) {
                this.totalDiscount = this.merchandiseSubtotal;
            }
            this.totalPayment = this.merchandiseSubtotal + this.shippingTotal - this.shippingTotalDiscount - this.totalDiscount;
            this.result.system_discount_voucher = voucher[0];
            this.result.system_discount_voucher.discount_for_this_cart = discount_for_this_cart;
        }
        else {
            this.result.system_discount_voucher = null;
        }

    }

    async handleSystemDiscountVoucherV2(discount_voucher_id, ObjectCondition) {
        //check whether voucher is valid or not, if not, it will return an empty array
        const voucher = await voucherRepo.checkValidVoucherOnCheckout(discount_voucher_id, ObjectCondition, 'discount')
        if (voucher) {
            let discount_for_this_cart = 0;
            // Check whether the voucher have discount_by_range_price
            let value = 0;
            let flag = 0;
            if (voucher.discount_by_range_price && voucher.discount_by_range_price.length == 0) { flag = 1 }
            if (voucher.discount_by_range_price && voucher.discount_by_range_price.length) {
                const range = voucher.discount_by_range_price;
                range.forEach(element => {
                    if (this.merchandiseSubtotal >= element.from && this.merchandiseSubtotal < element.to) {
                        value = element.value;
                    }
                    if (element.to == -1 && this.merchandiseSubtotal >= element.from) {
                        value = element.value
                    }
                });
                flag = 2;
            }
            let discount = 0;
            if (voucher.type == 'price') {
                if (value) {
                    discount = value
                }
                // value = 0 có 3 trường hợp: range ko có, range rỗng, không đủ điều kiện
                // flag = 0: không có trường range, flag = 1: range rỗng, flag = 2: có trường range
                if (value == 0 && (flag == 0 || flag == 1)) {
                    discount = voucher.value;
                }
                if (discount > this.merchandiseSubtotal) discount = this.merchandiseSubtotal;
            }
            if (voucher.type == 'percent') {
                if (value) {
                    discount = Math.round(this.merchandiseSubtotal * (value / 100));
                }
                if (value == 0 && (flag == 0 || flag == 1)) {
                    discount = Math.round(this.merchandiseSubtotal * (voucher.value / 100));
                }
            }

            if (voucher.max_discount_value) {
                discount = discount < voucher.max_discount_value ? discount : voucher.max_discount_value;
            }

            //check whether the discount great than the voucher's max budget
            if(voucher.max_budget) {
                const budgetUsed:number = await orderRepo.getBudgetUsedOfVoucher(voucher._id)
                discount = Math.min(discount, voucher.max_budget - budgetUsed > 0 ? voucher.max_budget - budgetUsed : 0)
            }
            //re-calculate of shop
            this.orderShop.forEach(shop => {
                const rate = shop.shop_merchandise_total / this.merchandiseSubtotal;
                const discount_of_system = Math.round(rate * discount);
                shop.discount_of_system_voucher = discount_of_system;
                //shop.shop_total -= shop.discount_of_system_voucher;
            });

            this.totalDiscount += discount;
            discount_for_this_cart = discount;
            if (this.totalDiscount > this.merchandiseSubtotal) {
                this.totalDiscount = this.merchandiseSubtotal;
            }
            this.totalPayment = this.merchandiseSubtotal + this.shippingTotal - this.shippingTotalDiscount - this.totalDiscount;
            this.result.system_discount_voucher = voucher;
            this.result.system_discount_voucher.discount_for_this_cart = discount_for_this_cart;
            
        }
        else {
            this.result.system_discount_voucher = null;
        }

    }

    async handleCashBackVoucher(cash_back_voucher_id, ObjectCondition) {
        const currentTime = new Date()
        let voucherCashback:any = await voucherRepo.findOne({
            _id: ObjectId(cash_back_voucher_id),
            classify: 'cash_back',
            start_time: { '$lte': new Date() },
            end_time: { '$gte': new Date() },
            available_quantity: { '$gt': 0 },
            min_order_value: { '$lte': ObjectCondition.total_value },
            $or: [
                { max_order_value: { '$gte': ObjectCondition.total_value } },
                { max_order_value: null }
            ]
            
        })
        voucherCashback = cloneObj(voucherCashback)
        const isValidForUser = !voucherCashback?.using_by?.[ObjectCondition.user_id] ? true : false
        if(voucherCashback && isValidForUser) {
            // Continuing check cash back voucher
            const subVoucherDiscount = await voucherRepo.findOne({
                'start_time': { '$lte': currentTime },
                'end_time': { '$gte': currentTime },
                'cash_back_ref.user_id': ObjectId(ObjectCondition.user_id),
                'cash_back_ref.cash_back_id': ObjectId(voucherCashback._id),
                'available_quantity': { '$gt': 0 }
            })
            let discount = voucherCashback.type === 'price' 
            ? voucherCashback?.value 
            : +(this.merchandiseSubtotal * +voucherCashback?.value / 100).toFixed(0)

            if(voucherCashback.max_discount_value) {
                discount = Math.min(discount, voucherCashback.max_discount_value)
            }

            // Check budget & exists voucher
            if(!subVoucherDiscount &&
                !voucherCashback.max_budget || (voucherCashback.max_budget && +voucherCashback.max_budget >= +voucherCashback.used_budget + discount)
            ) {
                this.result.cash_back_voucher = voucherCashback
                this.result.cash_back_next_order = discount
            }
            else {
                this.result.cash_back_voucher = null
                this.result.cash_back_next_order = 0
            }
        }
        else {
            this.result.cash_back_next_order = 0
        }

    }

    /**
     * Haven't use yet
     * @param discount_voucher_id 
     * @param ObjectCondition 
     */
    async handleCashBackDiscountVoucher(cash_back_voucher_id, ObjectCondition) {
        let voucherCashbackDiscount:any = await voucherRepo.findOne({
            _id: ObjectId(cash_back_voucher_id),
            classify: 'cash_back_discount',
            start_time: { '$lte': new Date() },
            end_time: { '$gte': new Date() },
            available_quantity: { '$gt': 0 },
            min_order_value: { '$lte': ObjectCondition.total_value },
            $or: [
                { max_order_value: { '$gte': ObjectCondition.total_value } },
                { max_order_value: null }
            ],
            'cash_back_ref.user_id': ObjectId(ObjectCondition.user_id)
        })
        voucherCashbackDiscount = cloneObj(voucherCashbackDiscount)
        
        if (voucherCashbackDiscount) {
            let discount_for_this_cart = 0;
            // Check whether the voucher have discount_by_range_price
            let discount = 0;
            if (voucherCashbackDiscount.type == 'price') {
                discount = voucherCashbackDiscount?.value
                if (discount > this.merchandiseSubtotal) discount = this.merchandiseSubtotal;
            }

            //re-calculate of shop
            this.orderShop.forEach(shop => {
                const rate = shop.shop_merchandise_total / this.merchandiseSubtotal;
                shop.discount_of_cash_back = Math.round(rate * discount);
                console.log("shop.discount_of_cash_back: ", shop.discount_of_cash_back);
                
            });

            this.totalCashBackDiscount = discount;
            discount_for_this_cart = discount;

            // Calculate discount value. This value can't greater than difference between merchandiseSubtotal and totalDiscount (system_discount_voucher)
            if (this.totalCashBackDiscount > this.merchandiseSubtotal - this.totalDiscount) {
                this.totalCashBackDiscount = this.merchandiseSubtotal - this.totalDiscount;
            }
            this.totalPayment = this.merchandiseSubtotal + (this.shippingTotal - this.shippingTotalDiscount) - this.totalDiscount - this.totalCashBackDiscount;
            this.result.cash_back_discount_voucher = voucherCashbackDiscount;
            this.result.cash_back_discount_voucher.discount_for_this_cart = discount_for_this_cart;
        }
        else {
            
        }
    }

    async handleShopVoucher(req, shop, ObjectCondition) {
        try {
            //filter shop that have voucher
            const shopVouchers = shop.filter(shop => shop.voucher_id)
            const result: any = await voucherRepo.checkShopVoucher(shopVouchers, ObjectCondition);
            //shippingSubtotal = 0;
            this.totalPayment = 0;
            const instance = this
            const promises = result.map(voucher => {
                return new Promise(async (resolve, reject) => {
                    const shopItem = this.orderShop.find(item => item._id == voucher.shop_id)
                    if (shopItem) {
                        let discount;
                        if (voucher.type == 'percent') {
                            discount = Math.round(shopItem.shop_merchandise_total * voucher.value / 100);
                        }
                        if (voucher.type == 'price') {
                            discount = voucher.value
                        }
                        //check whether the discount great than the max_discount_value
                        if (voucher.max_discount_value) {
                            discount = discount < voucher.max_discount_value ? discount : voucher.max_discount_value;
                        }                        
                        //check whether the discount great than the shop_merchandise_total
                        discount = discount < shopItem.shop_merchandise_total ? discount : shopItem.shop_merchandise_total;

                        //check whether the discount great than the voucher's max budget
                        if(voucher.max_budget) {
                            const budgetUsed:number = await orderRepo.getBudgetUsedOfVoucher(voucher._id)
                            discount = Math.min(discount, voucher.max_budget - budgetUsed > 0 ? voucher.max_budget - budgetUsed : 0)
                        }
                        shopItem.shop_total -= discount;
                        instance.totalDiscount += discount;

                        shopItem.voucher = voucher;
                        shopItem.voucher.discount_for_this_shop = discount
                    }
                    resolve("Completed")
                })
            });

            await Promise.all(promises).catch(error => {
                throw new Error(error);
            })

            if (this.totalDiscount > this.merchandiseSubtotal) this.totalDiscount = this.merchandiseSubtotal
            this.totalPayment = this.merchandiseSubtotal + this.shippingTotal - this.shippingTotalDiscount - this.totalDiscount;
            if (this.totalPayment < 0) this.totalPayment = 0;

        } catch (error) {
            throw new Error(req.__('service.cart.invalid_shop_voucher'))
        }

    }

    convertItemFromCart = async (itemIds) => {
        let items = await cartItemRepo.getItemsCheckoutPage(itemIds);
        items = cloneObj(items)
        items = items.map(res => {
            const items = res.items.map(item => {
                item = cloneObj(item)
                item.product = item.products[0] || null
                item.product.variant = item.product.variants[0] || null
                delete item.products
                delete item.product.variants
                return item
            })
            res.shop = res.shop[0] || null
            res.items = items
            return res
        });
        return items
    }

    updateShippingMethod = async (shopTotal, shop, listShippingMethod) => {      
        // update shipping method
        shopTotal.forEach(element => {
            shop.forEach(el => {
                if (el.shop_id == element._id) {
                    element.shipping = {};
                    element.shipping.shipping_method_id = el.shipping_method_id;
                    element.shipping.shipping_fee = element.shipping_fee;
                    element.shipping.order_service = element.order_service;
                    //update shipping fee for shop
                    el.shipping_fee = element.shipping_fee;
                }

            });
        });

        shopTotal.forEach(shop => {
            listShippingMethod.forEach(method => {
                if (shop.shipping.shipping_method_id == method._id.toString()) {
                    method = cloneObj(method);
                    shop.shipping_method = method;
                    shop.shipping_method.shipping_fee = shop.shipping_fee
                    shop.shipping_method.order_service = shop.order_service
                    shop.shipping_method.shipping_method_id = method._id
                }
            });
        });
        return [shop, shopTotal];
    }

    getNameQueryShipment = (shops, listShippingMethod, shopId) => {
        const shipmentIdByShop = shops.find(
            (shop) => shop.shop_id.toString() === shopId.toString()
        ).shipping_method_id;

        const nameQuery = listShippingMethod.find(
            (method) => method._id.toString() == shipmentIdByShop
        ).name_query;
        return nameQuery;
    };

    updateShippingFee = async (
        req,
        shopTotal,
        address,
        shops,
        listShippingMethod
    ) => {
        // update shipping FEE
        if (address) {
            shopTotal = shopTotal.map(async (element) => {
                const nameQuery = this.getNameQueryShipment(
                    shops,
                    listShippingMethod,
                    element._id
                );
                let shipping_fee = await this.getShippingFeeByAddressByShop(
                    req,
                    address,
                    element._id,
                    element.total_weight,
                    nameQuery,
                    element
                );
                element.shipping_fee = shipping_fee.fee;
                element.order_service = shipping_fee.order_service ?? null;
                return element;
            });
        } else {
            shopTotal = shopTotal.map(async (element) => {
                element.shipping_fee = 32500;
                return element;
            });
        }
        let result = await Promise.all(shopTotal);
        return result;
    };

    getShippingFeeByAddressByShop = async (
        req,
        address,
        shopId,
        totalWeight,
        nameQuery,
        elementShop
    ) => {
        const shop = await this.getShopByShopId(shopId);
        if (shop.country !== "VN") {
            const overseasShippingFee = totalWeight * OVERSEAS_SHIPPING_FEE_BY_GAM;
            const shippingMethodOfShop = await this.getShippingMethodByShop(shop);

            const shopAddress = await this.getPickAddressDefaultByShopFromKorea(shop);
            if (!shopAddress) throw new NotFoundError(req.__('service.cart.default_address_shop_not_found'));

            let objPickAddress = {};
            objPickAddress['pick_province'] = shopAddress.state.name;
            objPickAddress['pick_district'] = shopAddress.district.name;
            objPickAddress['province'] = address.state.name;
            objPickAddress['district'] = address.district.name;
            objPickAddress['weight'] = totalWeight; // set default
            objPickAddress['deliver_option'] = 'none'; // set default
            objPickAddress['transport'] = 'road'; // set default

            const ghtkService = await GHTKService.getInstance();
            const domesticShippingFee = await ghtkService.getShippingFeeFromAddressCheckoutPage(objPickAddress, shippingMethodOfShop.token);

            let fee = overseasShippingFee + domesticShippingFee.fee + COSTS_INCURRED;
            fee = Number.isInteger(fee) ? fee : 500000;
            return {
                name: 'area1',
                fee: fee,
                insurance_fee: 0,
                include_vat: '0',
                cost_id: '1803',
                delivery_type: '191211-Niemyet-Noitinh-61tinh',
                a: 1,
                dt: 'local',
                extFees: [],
                ship_fee_only: fee,
                promotion_key: '',
                delivery: true,
            };
        }
        let fee = null;
        // shop country = VN
        const shopAddress = await this.getPickAddressDefaultByShop(shop);
        if (!shopAddress)
            throw new NotFoundError(
                req.__('service.cart.default_address_shop_not_found')
            );
        switch (nameQuery) {
            case ShippingMethodQuery.GIAOHANGTIETKIEM:
                fee = await this.getFeeByGHTK({
                    shop,
                    address,
                    shopAddress,
                    totalWeight,
                });
                break;
            case ShippingMethodQuery.VIETTELPOST:
                fee = await this.getFeeByViettelPost({
                    productWeight: totalWeight,
                    productPrice: elementShop.total_price,
                    moneyCollection: elementShop.total_price,
                    productType: PRODUCT_TYPE_SHIPPING.HH,
                    senderProvince: shopAddress.state.id_vtp,
                    senderDistrict: shopAddress.district.id_vtp,
                    recieverProvince: address.state.id_vtp,
                    recieverDistrict: address.district.id_vtp,
                    type: 1,
                    nationalType: NATIONAL_TYPE_SHIPPING.INLAND,
                });
                break;
        }
        return fee;
    };

    getShippingFeeByMethod = async (
        req,
        shopId,
        address,
        totalWeight,
        totalPrice,
        nameQuery
    ): Promise<Number> => {
        const shop = await this.getShopByShopId(shopId);
        let fee = 32500;

        const shopAddress = await this.getPickAddressDefaultByShopFromKorea(shop);
        if (!shopAddress) throw new NotFoundError(req.__('service.cart.default_address_shop_not_found'));

        switch (nameQuery) {
            case ShippingMethodQuery.GIAOHANGTIETKIEM:
                const objFeeGHTK = await this.getFeeByGHTK({
                    shop,
                    address,
                    shopAddress,
                    totalWeight,
                });

                objFeeGHTK?.fee ? fee = objFeeGHTK?.fee : "";

                break;

            case ShippingMethodQuery.VIETTELPOST:
                
                const objFeeViettelPost = await this.getFeeByViettelPost({
                    productWeight: totalWeight,
                    productPrice: totalPrice,
                    moneyCollection: totalPrice,
                    productType: PRODUCT_TYPE_SHIPPING.HH,
                    senderProvince: shopAddress.state.id_vtp,
                    senderDistrict: shopAddress.district.id_vtp,
                    recieverProvince: address.state.id_vtp,
                    recieverDistrict: address.district.id_vtp,
                    type: 1,
                    nationalType: NATIONAL_TYPE_SHIPPING.INLAND,
                });

                objFeeViettelPost?.fee ? fee = objFeeViettelPost?.fee : "";

                break;
        
            default:
                break;
        }

        if (shop?.country === KOREA) {
            fee = Number.isInteger(fee)
                ? fee +
                  totalWeight * OVERSEAS_SHIPPING_FEE_BY_GAM +
                  COSTS_INCURRED
                : 500000;
        }
        
        return +fee;
    };

    getFeeByViettelPost = async ({
        productWeight,
        productPrice,
        moneyCollection,
        productType,
        senderProvince,
        senderDistrict,
        recieverProvince,
        recieverDistrict,
        type,
        nationalType,
    }) => {
        const service = await ViettelPostService._.bestOfServiceWithPricing({
            productWeight: productWeight,
            productPrice: productPrice,
            moneyCollection: moneyCollection,
            productType: productType,
            senderProvince: senderProvince,
            senderDistrict: senderDistrict,
            recieverProvince: recieverProvince,
            recieverDistrict: recieverDistrict,
            type: type,
        });

        const fee = await ViettelPostService._.pricing({
            orderService: service.order_service,
            productWeight: productWeight,
            productPrice: productPrice,
            moneyCollection: moneyCollection,
            orderServiceAdd: '',
            senderProvince: senderProvince,
            senderDistrict: senderDistrict,
            receiverProvince: recieverProvince,
            receiverDistrict: recieverDistrict,
            productType: productType,
            nationalType: nationalType,
        });

        if (!fee) {
            return {
                fee: 32000,
                order_service: 'bidu_default',
            };
        }

        return fee;
    };

    getFeeByGHTK = async ({ shopAddress, shop, address, totalWeight }) => {
        const shippingMethodOfShop = await this.getShippingMethodByShop(shop);
        let objPickAddress = {};
        objPickAddress['pick_province'] = shopAddress.state.name;
        objPickAddress['pick_district'] = shopAddress.district.name;
        objPickAddress['province'] = address.state.name;
        objPickAddress['district'] = address.district.name;
        objPickAddress['weight'] = totalWeight; // set default
        objPickAddress['deliver_option'] = 'none'; // set default
        objPickAddress['transport'] = 'road'; // set default

        const ghtkService = await GHTKService.getInstance();
        return await ghtkService.getShippingFeeFromAddressCheckoutPage(
            objPickAddress,
            shippingMethodOfShop.token
        );
    }

    getShopByShopId = async (shopId) => {
        let shop = await shopRepo.findShopByShopId(shopId);
        return shop;
    }

    getShippingMethodByShop = async (shop) => {
        let shippingMethods = await shopRepo.getShippingMethodShop(shop);
        return shippingMethods[0];
    }

    getPickAddressDefaultByShop = async (shop) => {
        let address = await shopRepo.getPickAddressDefaultByShop(shop);
        return address;
    }

    getPickAddressDefaultByShopFromKorea = async (shop) => {
        let address = await shopRepo.getPickAddressDefaultByShopKorea(shop);
        return address;
    }

}