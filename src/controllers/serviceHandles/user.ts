import { BannerDTO } from '../../DTO/BannerDTO';
import BannerRepo from '../../repositories/BannerRepo';
import IPaginateData from '../../repositories/interfaces/IPaginateData';
import { NotFoundError } from '../../base/customError';
import { UserRepo } from '../../repositories/UserRepo';
import { URL_COMMUNITY } from "../../base/variable";
import { randomFloat } from "../../utils/helper";
import { addLink } from '../../utils/stringUtil';
import { StreamSessionRepository } from '../../repositories/StreamSessionRepository';
import keys from '../../config/env/keys';
import { UserDTO } from '../../DTO/UserDTO';
import { FollowRepo } from '../../repositories/FollowRepo';
import { FeedbackRepo } from '../../repositories/FeedbackRepo';
import { InMemoryAuthUserStore } from '../../SocketStores/AuthStore';
import { KEY } from '../../constants/cache';
const userRepository = UserRepo.getInstance();
const feedbackRepo = FeedbackRepo.getInstance();
const userCache = InMemoryAuthUserStore.getInstance();
class UserService {

    private static _instance: UserService;

    static get _() {
        if (!this._instance) {
            this._instance = new UserService();
        }
        return this._instance;
    }

    /**
     * 
     * @param shopId 
     * @returns Promise<any>
     */
    async getUserShopExplore(req, shopId: string): Promise<any> {
        let streamSessionRepository = StreamSessionRepository.getInstance();
        let followRepo = FollowRepo.getInstance();
        let user = await userRepository.getUserByShopId(shopId);
        user.avatar = addLink(`${keys.host_community}/`, user.gallery_image.url)
        user.feedback = await feedbackRepo.getFeedbackAvgByShopID(shopId);
        if(!user.feedback) {
            user.feedback = 0
        }
        else {
            user.feedback = user.feedback.toFixed(1)
        }
        user.is_live = await streamSessionRepository.getStreamSessionLiveByUser(user._id);
        user.is_follow = await followRepo.checkFollow(req.user._id, user._id);
        user = new UserDTO(user).toShopExploreJSON();
        return user;
    }

    async clearUserInfoInCache(userID) {
        userCache.delAsync(KEY.STORE_USER, userID)
    }
}
export default UserService;