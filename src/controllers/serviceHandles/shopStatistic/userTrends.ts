
import { StreamSessionDTO } from '../../../DTO/StreamSessionDTO';
import { OrderRepository } from '../../../repositories/OrderRepo';
import { StreamSessionRepository } from '../../../repositories/StreamSessionRepository';
import { yearOld } from '../../../utils/dateUtil';
const moment = require('moment');

export enum UserTrendType {
    AGE = 'AGE',
    GENDER = 'GENDER',
    REGULAR_CUSTOMER = 'REGULAR_CUSTOMER',
    LIVE_STREAM = 'LIVE_STREAM',
}

export enum GenderValue {
    MALE = 'Nam',
    FEMALE = 'Nữ',
    OTHER = 'Khác'
}

export enum ChangeType {
    UP = 'UP',
    EQUAL = 'EQUAL',
    DOWN = 'DOWN'
}
interface UserTrendRequest {
    start_time,
    end_time,
    shop_id: string,
    sort_direction?
}

const defaultBirthDay = new Date("1998-06-21 17:00")

class UserTrendService {

    private async getUserOfShop(shop_id, start_time, end_time, additional_condition: any = null): Promise<any[]> {
        const orderRepository = OrderRepository.getInstance();

        const order_data = await orderRepository.getOrderOfShop(shop_id, start_time, end_time, additional_condition);

        const users: any = [];
        order_data.forEach(order => {
            if (!users.find((added_user) => added_user._id.toString() == order.userOrder?._id.toString()))
                users.push(order.userOrder)
        });

        return users;
    }

    async userTrendByAge(request: UserTrendRequest) {
        const { shop_id, start_time, end_time } = request;

        const range_ages = [
            { min: null, max: 18 },
            { min: 19, max: 23 },
            { min: 24, max: 28 },
            { min: 29, max: 33 },
            { min: 34, max: 39 },
            { min: 40, max: null },
        ];
        const match_array = new Array(6).fill(0);

        const users = await this.getUserOfShop(shop_id, start_time, end_time);

        users.forEach((user) => {
            const age = yearOld(user.birthday
                ? moment(user.birthday, "DD-MM-YYYY").toDate()
                : defaultBirthDay);
            range_ages.some((range, index) => {
                let condition = true;
                if (range.min) condition = condition && age >= range.min;
                if (range.max) condition = condition && age <= range.max;
                if (condition) {
                    match_array[index] = match_array[index] + 1;
                    return true;
                }
            })
        })

        const data = range_ages.map((range, index) => {
            const label = (range.min ?? '') + '~' + (range.max ?? '');
            const value = users.length > 0 ? Math.round((match_array[index] / users.length) * 100) : 0;
            return { label, value };
        })

        return {
            trend_type: UserTrendType.AGE,
            data,
        }
    }

    async userTrendByGender(request: UserTrendRequest) {
        const { shop_id, start_time, end_time } = request;

        const genders = [1, 2, 3];
        const match_array = new Array(3).fill(0);

        const users = await this.getUserOfShop(shop_id, start_time, end_time);

        users.forEach((user) => {
            genders.some((value, index) => {
                if (value == user.gender) {
                    match_array[index] = match_array[index] + 1;
                    return true;
                }
            })
        })

        const data = genders.map((gender, index) => {
            const label = gender == 1 ? GenderValue.MALE : (gender == 2 ? GenderValue.FEMALE : GenderValue.OTHER);
            const value = users.length > 0 ? Math.round((match_array[index] / users.length) * 100) : 0;
            return { label, value };
        })

        return {
            trend_type: UserTrendType.GENDER,
            data,
        }
    }

    async userTrendByRegularCustomer(request: UserTrendRequest) {
        const { shop_id, start_time, end_time } = request;

        const range_ages = [
            { min: null, max: 18 },
            { min: 19, max: 23 },
            { min: 24, max: 28 },
            { min: 29, max: 33 },
            { min: 34, max: 39 },
            { min: 40, max: null },
        ];
        const match_array = new Array(6).fill({});

        const users = await this.getUserOfShop(shop_id, start_time, end_time);

        users.forEach((user) => {
            const age = yearOld(user.birthday
                ? moment(user.birthday, "DD-MM-YYYY").toDate()
                : defaultBirthDay);
            range_ages.some((range, index) => {
                let condition = true;
                if (range.min) condition = condition && age >= range.min;
                if (range.max) condition = condition && age <= range.max;
                if (condition) {
                    const currentValue = { ...match_array[index] };
                    const label = user.gender || 3;
                    currentValue[label] = (currentValue[label] || 0) + 1
                    match_array[index] = currentValue;
                    return true;
                }
            })
        })

        const data = range_ages.map((range, index) => {
            const label = (range.min ?? '') + '~' + (range.max ?? '');
            const item = match_array[index];
            const total: any = Object.values(item).reduce((o: any, t) => o + t, 0);
            const itemResult = {
                [GenderValue.MALE]: 0,
                [GenderValue.FEMALE]: 0,
                [GenderValue.OTHER]: 0
            }
            for (const counple of Object.entries(item)) {
                const [key, value]: [string, any] = counple;
                const label = +key == 1 ? GenderValue.MALE : (+key == 2 ? GenderValue.FEMALE : GenderValue.OTHER);
                itemResult[label] = value ? Math.round((value / total) * 100) : 0;
            }
            return { label, value: itemResult };
        })

        return {
            trend_type: UserTrendType.REGULAR_CUSTOMER,
            data,
        }

    }

    async userTrendByStream(request: UserTrendRequest) {
        let streamSessionRepository = StreamSessionRepository.getInstance();
        let list_stream_session = await streamSessionRepository.getListStreamSessionByShopStatistics(
            request.shop_id,
            request.sort_direction,
            request.start_time,
            request.end_time,
        );
        list_stream_session = list_stream_session.map(session => StreamSessionDTO.newInstance(session).toUserTrendJSON());

        let startValue = 50000;// TODO: Remove mock data
        list_stream_session = list_stream_session.map(session => {
            const { change_type, change_percent } = this.getStreamFakeStatistic(session);
            startValue -= 5000;
            return {
                ...session,
                change_type: change_type,
                change_percent: change_percent,
                total_value: startValue,
            }
        });
        return {
            trend_type: UserTrendType.LIVE_STREAM,
            data: list_stream_session
        }
    }

    getStreamFakeStatistic(session) {
        // TODO: Remove mock data
        const random = Math.random();
        return {
            change_type: random == 0.5 ? ChangeType.EQUAL : random < 0.5 ? ChangeType.DOWN : ChangeType.UP,
            change_percent: random == 0.5 ? '0%' : `${Math.floor(random * 100)}%`
        }
    }
}

const userTrendService = new UserTrendService();
export default userTrendService;