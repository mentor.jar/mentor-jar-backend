
import { OrderRepository } from '../../../repositories/OrderRepo';
import { ProductRepo } from '../../../repositories/ProductRepo';
import { ProductDTO } from '../../../DTO/Product';

export enum ProductRankingType {
    REVENUE = 'REVENUE',
    SOLD_AMOUNT = 'SOLD_AMOUNT',
    VIEW_AMOUNT = 'VIEW_AMOUNT',
}

interface ProductRankingRequest {
    start_time,
    end_time,
    shop_id: string,
    sort_direction?,
    category_id: string,
    limit?: number,
    page?: number
}

interface ProductRankingResponse {
    ranking_type: string,
    data,
    paginate
}

interface RankingItem {
    unit: string | null,
    total: number,
    change_type: ChangeType,
    change_percent: string | null
}

enum ChangeType {
    UP = 'UP',
    EQUAL = 'EQUAL',
    DOWN = 'DOWN'
}

class ProductRankingService {

    getChange(previous_value, current_value) {
        let change_type = ChangeType.EQUAL;
        let change_percent = '0%';
        if (previous_value <= 0) {
            if (current_value <= 0) {
                change_type = ChangeType.EQUAL;
                change_percent = '0%';
            } else {
                change_type = ChangeType.UP;
                change_percent = '100%';
            }
        } else {
            change_type = current_value > previous_value ? ChangeType.UP : (
                current_value == previous_value ? ChangeType.EQUAL : ChangeType.DOWN
            );
            change_percent = `${Math.round(Math.abs(current_value - previous_value) * 100 / previous_value)}%`;
        }
        return [change_type, change_percent];
    }

    async revenue(request: ProductRankingRequest): Promise<ProductRankingResponse> {
        const { shop_id, start_time, end_time, category_id, sort_direction, limit, page } = request;

        const productRepository = ProductRepo.getInstance();

        let { data, paginate } = await productRepository.getProductOfShopForStatic({
            page,
            limit,
            shop_id,
            sort_direction,
            category_id,
            start_time,
            end_time,
            type: ProductRankingType.REVENUE
        });

        data = data.map(product => {
            // console.log(product);
            const [change_type, change_percent] = this.getChange(product.order_revenue_previous, product.order_revenue);
            const item: any = new ProductDTO(product).toSimpleJSON();
            item.change_type = change_type;
            item.change_percent = change_percent;
            item.total_value = product.order_revenue;
            return item;
        })

        return {
            ranking_type: ProductRankingType.REVENUE,
            data,
            paginate
        }
    }

    async soldAmount(request: ProductRankingRequest): Promise<ProductRankingResponse> {
        const { shop_id, start_time, end_time, category_id, sort_direction, limit, page } = request;

        const productRepository = ProductRepo.getInstance();

        let { data, paginate } = await productRepository.getProductOfShopForSoldAmount({
            page,
            limit,
            shop_id,
            sort_direction,
            category_id
        });

        data = data.map(product => {
            const item: any = new ProductDTO(product).toSimpleSoldProductItem();
            item.change_type = "EQUAL";
            item.change_percent = "0%";
            item.total_value = item.sold;
            return item;
        })

        return {
            ranking_type: ProductRankingType.SOLD_AMOUNT,
            data,
            paginate
        }
    }

    async viewAmount(request: ProductRankingRequest): Promise<ProductRankingResponse> {
        const { shop_id, start_time, end_time, category_id, sort_direction, limit, page } = request;

        const productRepository = ProductRepo.getInstance();

        let { data, paginate } = await productRepository.getProductOfShopForStatic({
            page,
            limit,
            shop_id,
            sort_direction,
            category_id,
            start_time,
            end_time,
            type: ProductRankingType.VIEW_AMOUNT
        });

        data = data.map(product => {
            const [change_type, change_percent] = this.getChange(product.product_view_previous, product.product_view);
            const item: any = new ProductDTO(product).toSimpleJSON();
            item.change_type = change_type;
            item.change_percent = change_percent;
            item.total_value = product.product_view;
            return item;
        })

        return {
            ranking_type: ProductRankingType.VIEW_AMOUNT,
            data,
            paginate
        }
    }

}

const productRankingService = new ProductRankingService();
export default productRankingService;