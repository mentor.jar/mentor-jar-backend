import moment from 'moment';
import { isConditionalExpression } from 'typescript';
import { CATEGORIES_COSMETIC, CATEGORIES_FASHION } from '../../../base/variable';
import { PaymentStatus, ShippingStatus } from '../../../models/enums/order';
import { FeedbackRepo } from '../../../repositories/FeedbackRepo';
import { OrderRepository } from '../../../repositories/OrderRepo';
import { ProductRepo } from '../../../repositories/ProductRepo';
import { ShopRepo } from '../../../repositories/ShopRepo';
import { SystemSettingRepo } from '../../../repositories/SystemSettingRepo';
import TrackingRepo from '../../../repositories/TrackingRepo';
import { dateToString, timeToString } from '../../../utils/dateUtil';
import { roundNumberPercent } from '../../../utils/helper';

const shopRepo = ShopRepo.getInstance();
const feedbackRepo = FeedbackRepo.getInstance();
const orderRepository = OrderRepository.getInstance();
const systemSetting  = SystemSettingRepo.getInstance();

class GeneralAnalystService {
    //1 Đơn hàng: Tổng giá trị đơn hàng xác nhận trong khoảng thời gian đã chọn
    private async getTotalOrder(shop_id, start_time, end_time) {
        let query_other = [
            {
                payment_status: {
                    $in: [PaymentStatus.PAID, PaymentStatus.PENDING],
                },
                $or: [
                    {
                        shipping_status: {
                            $in: [
                                ShippingStatus.SHIPPED,
                                ShippingStatus.SHIPPING,
                                ShippingStatus.WAIT_TO_PICK,
                                ShippingStatus.RETURN,
                            ],
                        },
                    },
                    {
                        shipping_status: {
                            $in: [
                                ShippingStatus.CANCELING,
                                ShippingStatus.CANCELED,
                            ],
                        },
                        approved_time: { $ne: null, $exists: true },
                    },
                ],
            },
        ];

        const orders = await orderRepository.getOrderOfShop(
            shop_id,
            start_time,
            end_time,
            query_other,
            false
        );
        return orders.length;
    }
    //2 Doanh thu: tổng giá trị các đơn hàng đã xác nhận, bao gồm cả đơn đã thanh toán và chưa thanh toán, đơn trả hàng và đơn huỷ trong khoảng thời gian đã chọn
    private async getRevenue(shop_id, start_time, end_time) {
        let query_other = [
            {
                payment_status: {
                    $in: [PaymentStatus.PAID, PaymentStatus.PENDING],
                },
                $or: [
                    {
                        shipping_status: {
                            $in: [
                                ShippingStatus.SHIPPED,
                                ShippingStatus.SHIPPING,
                                ShippingStatus.WAIT_TO_PICK,
                                ShippingStatus.RETURN,
                            ],
                        },
                    },
                    {
                        shipping_status: {
                            $in: [
                                ShippingStatus.CANCELING,
                                ShippingStatus.CANCELED,
                            ],
                        },
                        approved_time: { $ne: null, $exists: true },
                    },
                ],
            },
        ];

        const orders = await orderRepository.getOrderOfShop(
            shop_id,
            start_time,
            end_time,
            query_other,
            false
        );
        let total = 0;
        if (!orders.length) return 0;
        orders.forEach((order) => {
            total += order.total_price;
        });
        return total;
    }

    //3 tỉ lệ chuyển đổi: số khách hàng truy cập có đơn xác nhân chia cho tổng số khách truy cập
    private async getRatioConvert(shop_id, start_time, end_time) {
        const [total_user_have_order, total_user_access_shop] = await Promise.all([
            (async () => {
                return this.getAmountUserHaveOrderConfirm(
                    shop_id,
                    start_time,
                    end_time
                )
            })(),
            (async () => {
                return this.getMountUserAccessShop(
                    shop_id,
                    start_time,
                    end_time
                )
            })()
        ])

        if (!total_user_access_shop) return 0;
        return total_user_have_order / total_user_access_shop;
    }

    //4 Doanh thu/Đơn: Doanh trung bình của một đơn hàng phát sinh trong khoảng thời gian đã chọn
    private async getRevenueAverage(shop_id, start_time, end_time) {
        let query_other = [
            {
                payment_status: PaymentStatus.PAID,
            },
        ];
        const orders = await orderRepository.getOrderOfShop(
            shop_id,
            start_time,
            end_time,
            query_other,
            false
        );
        let total = 0;
        if (!orders.length) return 0;
        orders.forEach((order) => {
            total += order.total_price;
        });
        return total / orders.length;
    }

    //5 Lượt truy cập: Tổng lượt truy cập sản phầm duy nhất trong khoảng thời gian báo cáo từ chi tiết sản phẩm
    // .Một người mua 1 sản phầm nhiều lần nhưng vẫn tính 1 lượt truy cập
    private async getMountUserAccessShop(shop_id, start_time, end_time) {
        let trackings = await TrackingRepo.getInstance().getUserAccessShop(
            shop_id,
            start_time,
            end_time
        );
        return trackings.length;
    }

    //6 Lượt xem: tổng số lượt xem chi tiết sản phẩm trong khoảng thời gian đã chọn
    private async getMountViewShop(shop_id, start_time, end_time) {
        let trackings = await TrackingRepo.getInstance().getUserAccessShop(
            shop_id,
            start_time,
            end_time
        );
        let amount_view_shop = 0;
        trackings.forEach((tracking) => {
            amount_view_shop += tracking.visited_ats.length;
        });
        return amount_view_shop;
    }

    // get amount user have order confirm
    private async getAmountUserHaveOrderConfirm(shop_id, start_time, end_time) {
        let query_other = [
            {
                payment_status: {
                    $in: [PaymentStatus.PAID, PaymentStatus.PENDING],
                },
                $or: [
                    {
                        shipping_status: {
                            $in: [
                                ShippingStatus.SHIPPED,
                                ShippingStatus.SHIPPING,
                                ShippingStatus.WAIT_TO_PICK,
                                ShippingStatus.RETURN,
                            ],
                        },
                    },
                    {
                        shipping_status: {
                            $in: [
                                ShippingStatus.CANCELING,
                                ShippingStatus.CANCELED,
                            ],
                        },
                        approved_time: { $ne: null, $exists: true },
                    },
                ],
            },
        ];
        const orders = await orderRepository.getOrderOfShop(
            shop_id,
            start_time,
            end_time,
            query_other,
            false
        );
        let total_user_have_order = 0;
        let check_exist_user = {};
        orders.forEach((order) => {
            if (!check_exist_user[order.user_id]) {
                check_exist_user[order.user_id] = true;
                total_user_have_order++;
            }
        });
        return total_user_have_order;
    }

    private callIndexReduceIncrease(old_data, new_data) {
        let ratio;
        let status_type;

        if (!old_data) {
            if (!new_data) {
                ratio = '0%';
                status_type = 'EQUAL';
            } else {
                ratio = '100%';
                status_type = 'UP';
            }
        } else {
            let tmp = ((new_data - old_data) * 100) / old_data;
            if (tmp < 0) {
                status_type = 'DOWN';
                ratio = -Math.round(tmp) + '%';
            } else if (tmp > 0) {
                status_type = 'UP';
                ratio = Math.round(tmp) + '%';
            } else {
                status_type = 'EQUAL';
                ratio = 0;
            }
        }
        return { ratio, status_type };
    }
    public async callIndexGeneral(shop_id, date_start_new, date_end_new) {
        let difference = date_end_new.getTime() - date_start_new.getTime();
        let days = Math.ceil(difference / (1000 * 3600 * 24));
        let date_end_old = new Date(date_start_new.toString());
        let date_start_old = new Date(date_start_new.toString());

        date_end_old.setDate(date_end_old.getDate());
        date_start_old.setDate(date_end_old.getDate() - days);

        let [total_order, total_revenue, total_ratio_convert, total_access, total_view] = await Promise.all([
            (async () => {
                const [oldData, newData] = await Promise.all([
                    (async () => {
                        return this.getTotalOrder(
                            shop_id,
                            date_start_old,
                            date_end_old
                        )
                    })(),
                    (async () => {
                        return this.getTotalOrder(
                            shop_id,
                            date_start_new,
                            date_end_new
                        )
                    })(),
                ])
                return {old: oldData, new: newData}
            })(),
            (async () => {
                const [oldData, newData] = await Promise.all([
                    (async () => {
                        return this.getRevenue(shop_id, date_start_old, date_end_old)
                    })(),
                    (async () => {
                        return this.getRevenue(shop_id, date_start_new, date_end_new)
                    })(),
                ])
                return {old: oldData, new: newData}
            })(),
            (async () => {
                const [oldData, newData] = await Promise.all([
                    (async () => {
                        return this.getRatioConvert(
                            shop_id,
                            date_start_old,
                            date_end_old
                        )
                    })(),
                    (async () => {
                        return this.getRatioConvert(
                            shop_id,
                            date_start_new,
                            date_end_new
                        )
                    })(),
                ])
                return {old: oldData, new: newData}
            })(),
            (async () => {
                const [oldData, newData] = await Promise.all([
                    (async () => {
                        return this.getMountUserAccessShop(
                            shop_id,
                            date_start_old,
                            date_end_old
                        )
                    })(),
                    (async () => {
                        return this.getMountUserAccessShop(
                            shop_id,
                            date_start_new,
                            date_end_new
                        )
                    })(),
                ])
                return {old: oldData, new: newData}
            })(),
            (async () => {
                const [oldData, newData] = await Promise.all([
                    (async () => {
                        return this.getMountViewShop(
                            shop_id,
                            date_start_old,
                            date_end_old
                        )
                    })(),
                    (async () => {
                        return this.getMountViewShop(
                            shop_id,
                            date_start_new,
                            date_end_new
                        )
                    })(),
                ])
                return {old: oldData, new: newData}
            })()
        ])
        // 1 Đơn hàng:
        // let total_order = {
        //     old: await this.getTotalOrder(
        //         shop_id,
        //         date_start_old,
        //         date_end_old
        //     ),
        //     new: await this.getTotalOrder(
        //         shop_id,
        //         date_start_new,
        //         date_end_new
        //     ),
        // };
        let order = this.callIndexReduceIncrease(
            total_order.old,
            total_order.new
        );

        //2 Doanh thu:
        // let total_revenue = {
        //     old: await this.getRevenue(shop_id, date_start_old, date_end_old),
        //     new: await this.getRevenue(shop_id, date_start_new, date_end_new),
        // };
        let revenue = this.callIndexReduceIncrease(
            total_revenue.old,
            total_revenue.new
        );
        //3 Tỷ lệ chuyển đổi:
        // let total_ratio_convert = {
        //     old: await this.getRatioConvert(
        //         shop_id,
        //         date_start_old,
        //         date_end_old
        //     ),
        //     new: await this.getRatioConvert(
        //         shop_id,
        //         date_start_new,
        //         date_end_new
        //     ),
        // };
        let conversion_rate = this.callIndexReduceIncrease(
            total_ratio_convert.old,
            total_ratio_convert.new
        );

        //4 Doanh thu/Đơn:
        let total_revenue_average = {
            old: total_revenue.old / total_order.old,
            new: total_revenue.new / total_order.new,
        };
        let revenue_per_order = this.callIndexReduceIncrease(
            total_revenue_average.old,
            total_revenue_average.new
        );

        //5 Lượt truy cập
        // let total_access = {
        //     old: await this.getMountUserAccessShop(
        //         shop_id,
        //         date_start_old,
        //         date_end_old
        //     ),
        //     new: await this.getMountUserAccessShop(
        //         shop_id,
        //         date_start_new,
        //         date_end_new
        //     ),
        // };
        let access_times = this.callIndexReduceIncrease(
            total_access.old,
            total_access.new
        );

        //6 Lượt xem
        // let total_view = {
        //     old: await this.getMountViewShop(
        //         shop_id,
        //         date_start_old,
        //         date_end_old
        //     ),
        //     new: await this.getMountViewShop(
        //         shop_id,
        //         date_start_new,
        //         date_end_new
        //     ),
        // };
        let view = this.callIndexReduceIncrease(total_view.old, total_view.new);
        return {
            order: {
                total: parseFloat(total_order.new.toFixed(2)),
                change_type: order.status_type,
                change_percent: order.ratio,
            },
            revenue: {
                total: parseFloat(total_revenue.new.toFixed(2)),
                change_type: revenue.status_type,
                change_percent: revenue.ratio,
            },
            conversion_rate: {
                total: parseFloat(total_ratio_convert.new.toFixed(2)),
                change_type: conversion_rate.status_type,
                change_percent: conversion_rate.ratio,
            },
            revenue_per_order: {
                total: parseFloat(total_revenue_average.new.toFixed(2)),
                change_type: revenue_per_order.status_type,
                change_percent: revenue_per_order.ratio,
            },
            access_times: {
                total: parseFloat(total_access.new.toFixed(2)),
                change_type: access_times.status_type,
                change_percent: access_times.ratio,
            },
            view: {
                total: parseFloat(total_view.new.toFixed(2)),
                change_type: view.status_type,
                change_percent: view.ratio,
            },
        };
    }

    private init_chart_order(date_start, date_end) {
        let difference = date_end.getTime() - date_start.getTime();
        let days = Math.ceil(difference / (1000 * 3600 * 24));
        let data_chart_init = {};
        if (days > 1) {
            for (let i = 0; i < days; i++) {
                let date = new Date(date_start.setHours(0, 0, 0, 0));
                date.setDate(date.getDate() + i);
                let str_date = dateToString(date);
                data_chart_init[str_date] = {
                    label: dateToString(date, false),
                    date: date,
                    order: 0,
                    ox: i,
                };
            }
        } else {
            for (let i = 0; i < 24; i++) {
                let date = new Date(date_start.setHours(0, 0, 0, 0));
                date.setHours(date.getHours() + i);
                let str_time = timeToString(date);
                data_chart_init[str_time] = {
                    label: str_time,
                    date: date,
                    order: 0,
                    ox: i,
                };
            }
        }
        return data_chart_init;
    }

    private init_data_chart(date_start, date_end) {
        let difference = date_end.getTime() - date_start.getTime();
        let days = Math.ceil(difference / (1000 * 3600 * 24));
        let data_chart_init = {};
        if (days > 1) {
            for (let i = 0; i < days; i++) {
                let date = new Date(date_start.setHours(0, 0, 0, 0));
                date.setDate(date.getDate() + i);
                let str_date = dateToString(date);
                data_chart_init[str_date] = {
                    label: dateToString(date, false),
                    date: date,
                    order: 0,
                    revenue: 0,
                    conversion_rate: 0,
                    revenue_per_order: 0,
                    access_times: 0,
                    total_user_have_order: 0,
                    view: 0,
                    ox: i,
                };
            }
        } else {
            for (let i = 0; i < 24; i++) {
                let date = new Date(date_start.setHours(0, 0, 0, 0));
                date.setHours(date.getHours() + i);
                let str_time = timeToString(date);
                data_chart_init[str_time] = {
                    label: str_time,
                    date: date,
                    order: 0,
                    revenue: 0,
                    conversion_rate: 0,
                    revenue_per_order: 0,
                    total_user_have_order: 0,
                    access_times: 0,
                    view: 0,
                    ox: i,
                };
            }
        }
        return data_chart_init;
    }

    private handleTarget(date_start, date_end, target) {
        const monthStart = date_start.getMonth() + 1;
        const monthEnd = date_end.getMonth() + 1;
        const yearStart = date_start.getFullYear();
        const yearEnd = date_end.getFullYear();
        let difference = date_end.getTime() - date_start.getTime();
        let days = Math.ceil(difference / (1000 * 3600 * 24));

        switch (true) {
            case days <= 1:
            case (1 < days && days <= 30) ||
                (monthStart === monthEnd && yearStart === yearEnd):
                return target;

            case monthStart !== monthEnd && yearStart === yearEnd:
            case yearStart < yearEnd && (12 - monthStart + 1 + monthEnd) <= 12:
                return target * 30;

            case yearStart < yearEnd && (12 - monthStart + 1 + monthEnd) > 12:
                return target * 30 * 12;

            default:
                return target;
        }
    }

    private init_chart(date_start, date_end) {
        const monthStart = date_start.getMonth() + 1;
        const monthEnd = date_end.getMonth() + 1;
        const yearStart = date_start.getFullYear();
        const yearEnd = date_end.getFullYear();
        let difference = date_end.getTime() - date_start.getTime();
        let days = Math.ceil(difference / (1000 * 3600 * 24));
        let data_chart_init = [];

        switch (true) {
            case days <= 1:
                for (let i = 0; i < 24; i++) {
                    let date = new Date(date_start.setHours(0, 0, 0, 0));
                    date.setHours(date.getHours() + i);
                    let str_time = timeToString(date);
                    data_chart_init.push({
                        label: str_time,
                        count: 0,
                    });
                }
                break;

            case (1 < days && days <= 30) ||
                (monthStart === monthEnd && yearStart === yearEnd):
                for (let i = 0; i < days; i++) {
                    let date = new Date(date_start.setHours(0, 0, 0, 0));
                    date.setDate(date.getDate() + i);
                    data_chart_init.push({
                        label: moment(date).format('DD/MM'),
                        count: 0,
                    });
                }
                break;

            case monthStart !== monthEnd && yearStart === yearEnd:
                for (let i = monthStart; i <= monthEnd; i++) {
                    data_chart_init.push({
                        label: `${i >= 10 ? i : '0' + i}/${yearStart}`,
                        count: 0,
                    });
                }
                break;

            case yearStart < yearEnd && (12 - monthStart + 1 + monthEnd) <= 12:
                for (let i = monthStart; i <= 12; i++) {
                    data_chart_init.push({
                        label: `${i >= 10 ? i : '0' + i}/${yearStart}`,
                        count: 0,
                    });
                }

                for (let i = 1; i <= monthEnd; i++) {
                    data_chart_init.push({
                        label: `${i >= 10 ? i : '0' + i}/${yearEnd}`,
                        count: 0,
                    });
                }

                break;

            case yearStart < yearEnd && (12 - monthStart + 1 + monthEnd) > 12:
                for (let i = yearStart; i <= yearEnd; i++) {
                    data_chart_init.push({
                        label: i + '',
                        count: 0,
                    });
                }
                break;

            default:
                break;
        }

        return data_chart_init;
    }

    private fillDataForChart(
        days,
        monthStart,
        monthEnd,
        yearStart,
        yearEnd,
        timeItem,
        valueItem,
        resource,
        condition = {
            transaction: false,
            isApproved: false,
            pauseMode: true,
            orderTransaction: false,
            shippingStatus: null,
        }
    ) {
        let label = '';
        switch (true) {
            case days <= 1:
                label = timeToString(timeItem);
                label = label.split(':')[0] + ':00';
                break;

            case (1 < days && days <= 30) ||
                (monthStart === monthEnd && yearStart === yearEnd):
                label = moment(timeItem).format('DD/MM');
                break;

            case monthStart !== monthEnd && yearStart === yearEnd:
                label = moment(timeItem).format('MM/YYYY');
                break;

            case yearStart < yearEnd && (12 - monthStart + 1 + monthEnd) <= 12:
                label = moment(timeItem).format('MM/YYYY');
                break;

            case yearStart < yearEnd && (12 - monthStart + 1 + monthEnd) > 12:
                label = moment(timeItem).format('YYYY');
                break;

            default:
                break;
        }

        const data = resource.find((item) => item.label === label);

        if (data) {
            switch (true) {
                case condition.transaction:
                    data.all_shop += 1;
                    if (condition.isApproved && !condition.pauseMode) {
                        data.active_shop += 1;
                    }
                    break;

                case condition.orderTransaction:
                    data[condition.shippingStatus] += 1;
                    data.total += 1;
                    break;

                default:
                    data.count += valueItem;
                    break;
            }
        }
    }

    private init_order_quantity_chart(date_start, date_end) {
        const monthStart = date_start.getMonth() + 1;
        const monthEnd = date_end.getMonth() + 1;
        const yearStart = date_start.getFullYear();
        const yearEnd = date_end.getFullYear();
        let difference = date_end.getTime() - date_start.getTime();
        let days = Math.ceil(difference / (1000 * 3600 * 24));
        let data_chart_init = [];

        switch (true) {
            case days <= 1:
                for (let i = 0; i < 24; i++) {
                    let date = new Date(date_start.setHours(0, 0, 0, 0));
                    date.setHours(date.getHours() + i);
                    let str_time = timeToString(date);
                    data_chart_init.push({
                        label: str_time,
                        total: 0,
                        pending: 0,
                        wait_to_pick: 0,
                        shipping: 0,
                        shipped: 0,
                        canceled: 0,
                        canceling: 0,
                        return: 0,
                    });
                }
                break;

            case (1 < days && days <= 30) ||
                (monthStart === monthEnd && yearStart === yearEnd):
                for (let i = 0; i < days; i++) {
                    let date = new Date(date_start.setHours(0, 0, 0, 0));
                    date.setDate(date.getDate() + i);
                    data_chart_init.push({
                        label: moment(date).format('DD/MM'),
                        total: 0,
                        pending: 0,
                        wait_to_pick: 0,
                        shipping: 0,
                        shipped: 0,
                        canceled: 0,
                        canceling: 0,
                        return: 0,
                    });
                }
                break;

            case monthStart !== monthEnd && yearStart === yearEnd:
                for (let i = monthStart; i <= monthEnd; i++) {
                    data_chart_init.push({
                        label: `${i >= 10 ? i : '0' + i}/${yearStart}`,
                        total: 0,
                        pending: 0,
                        wait_to_pick: 0,
                        shipping: 0,
                        shipped: 0,
                        canceled: 0,
                        canceling: 0,
                        return: 0,
                    });
                }
                break;

            case yearStart < yearEnd && (12 - monthStart + 1 + monthEnd) <= 12:
                for (let i = monthStart; i <= 12; i++) {
                    data_chart_init.push({
                        label: `${i >= 10 ? i : '0' + i}/${yearStart}`,
                        total: 0,
                        pending: 0,
                        wait_to_pick: 0,
                        shipping: 0,
                        shipped: 0,
                        canceled: 0,
                        canceling: 0,
                        return: 0,
                    });
                }

                for (let i = 1; i <= monthEnd; i++) {
                    data_chart_init.push({
                        label: `${i >= 10 ? i : '0' + i}/${yearEnd}`,
                        total: 0,
                        pending: 0,
                        wait_to_pick: 0,
                        shipping: 0,
                        shipped: 0,
                        canceled: 0,
                        canceling: 0,
                        return: 0,
                    });
                }

                break;

            case yearStart < yearEnd && (12 - monthStart + 1 + monthEnd) > 12:
                for (let i = yearStart; i <= yearEnd; i++) {
                    data_chart_init.push({
                        label: i + '',
                        total: 0,
                        pending: 0,
                        wait_to_pick: 0,
                        shipping: 0,
                        shipped: 0,
                        canceled: 0,
                        canceling: 0,
                        return: 0,
                    });
                }
                break;

            default:
                break;
        }

        return data_chart_init;
    }

    private init_shop_order_quantity_chart(date_start, date_end) {
        const monthStart = date_start.getMonth() + 1;
        const monthEnd = date_end.getMonth() + 1;
        const yearStart = date_start.getFullYear();
        const yearEnd = date_end.getFullYear();
        let difference = date_end.getTime() - date_start.getTime();
        let days = Math.ceil(difference / (1000 * 3600 * 24));
        let data_chart_init = [];

        switch (true) {
            case days <= 1:
                for (let i = 0; i < 24; i++) {
                    let date = new Date(date_start.setHours(0, 0, 0, 0));
                    date.setHours(date.getHours() + i);
                    let str_time = timeToString(date);
                    data_chart_init.push({
                        label: str_time,
                        all_shop: 0,
                        active_shop: 0,
                    });
                }
                break;

            case (1 < days && days <= 30) ||
                (monthStart === monthEnd && yearStart === yearEnd):
                for (let i = 0; i < days; i++) {
                    let date = new Date(date_start.setHours(0, 0, 0, 0));
                    date.setDate(date.getDate() + i);
                    data_chart_init.push({
                        label: moment(date).format('DD/MM'),
                        all_shop: 0,
                        active_shop: 0,
                    });
                }
                break;

            case monthStart !== monthEnd && yearStart === yearEnd:
                for (let i = monthStart; i <= monthEnd; i++) {
                    data_chart_init.push({
                        label: `${i >= 10 ? i : '0' + i}/${yearStart}`,
                        all_shop: 0,
                        active_shop: 0,
                    });
                }
                break;

            case yearStart < yearEnd && (12 - monthStart + 1 + monthEnd) <= 12:
                for (let i = monthStart; i <= 12; i++) {
                    data_chart_init.push({
                        label: `${i >= 10 ? i : '0' + i}/${yearStart}`,
                        all_shop: 0,
                        active_shop: 0,
                    });
                }

                for (let i = 1; i <= monthEnd; i++) {
                    data_chart_init.push({
                        label: `${i >= 10 ? i : '0' + i}/${yearEnd}`,
                        all_shop: 0,
                        active_shop: 0,
                    });
                }

                break;

            case yearStart < yearEnd && (12 - monthStart + 1 + monthEnd) > 12:
                for (let i = yearStart; i <= yearEnd; i++) {
                    data_chart_init.push({
                        label: i + '',
                        all_shop: 0,
                        active_shop: 0,
                    });
                }
                break;

            default:
                break;
        }

        return data_chart_init;
    }

    public async orderChartCMS(date_start, date_end) {
        let query_other = [
            {
                payment_status: {
                    $in: [PaymentStatus.PAID, PaymentStatus.PENDING],
                },
                $or: [
                    {
                        shipping_status: {
                            $in: [
                                ShippingStatus.SHIPPED,
                                ShippingStatus.SHIPPING,
                                ShippingStatus.WAIT_TO_PICK,
                                ShippingStatus.RETURN,
                            ],
                        },
                    },
                    {
                        shipping_status: {
                            $in: [
                                ShippingStatus.CANCELING,
                                ShippingStatus.CANCELED,
                            ],
                        },
                        approved_time: { $ne: null, $exists: true },
                    },
                ],
            },
        ];
        let orders = await orderRepository.getOrderOfShop(
            null,
            date_start,
            date_end,
            query_other,
            false
        );

        let difference = date_end.getTime() - date_start.getTime();
        let days = Math.ceil(difference / (1000 * 3600 * 24));
        let result: any = this.init_chart_order(date_start, date_end);
        orders.forEach((order) => {
            let label = dateToString(order.created_at);
            if (days == 1) {
                // làm tròn giờ
                label = timeToString(order.created_at);
                label = label.split(':')[0] + ':00';
            }
            result[label].order += 1;
        });

        return Object.values(result);
    }
    public async chart(shop_id, date_start, date_end) {
        let difference = date_end.getTime() - date_start.getTime();
        let days = Math.ceil(difference / (1000 * 3600 * 24));
        let result: any = this.init_data_chart(date_start, date_end);

        let query_other = [
            {
                payment_status: {
                    $in: [PaymentStatus.PAID, PaymentStatus.PENDING],
                },
                $or: [
                    {
                        shipping_status: {
                            $in: [
                                ShippingStatus.SHIPPED,
                                ShippingStatus.SHIPPING,
                                ShippingStatus.WAIT_TO_PICK,
                                ShippingStatus.RETURN,
                            ],
                        },
                    },
                    {
                        shipping_status: {
                            $in: [
                                ShippingStatus.CANCELING,
                                ShippingStatus.CANCELED,
                            ],
                        },
                        approved_time: { $ne: null, $exists: true },
                    },
                ],
            },
        ];
        const [orders, trackings] = await Promise.all([
            (async () => {
                return orderRepository.getOrderOfShop(
                    shop_id,
                    date_start,
                    date_end,
                    query_other,
                    false
                );
            })(),
            (async () => {
                return TrackingRepo.getInstance().getUserAccessShop(
                    shop_id,
                    date_start,
                    date_end
                );
            })(),
        ])
        //1: Đơn hàng
        //2: Doanh thu
        let check_exist_user = {};
        orders.forEach((order) => {
            let label = dateToString(order.created_at);
            if (days == 1) {
                // làm tròn giờ
                label = timeToString(order.created_at);
                label = label.split(':')[0] + ':00';
            }
            result[label].order += 1;
            result[label].revenue += order.total_price;
            if (!check_exist_user[order.user_id]) {
                check_exist_user[order.user_id] = true;
                result[label].total_user_have_order++;
            }
        });

        //6: Lượt xem
        //5: Lượt truy cập
        trackings.forEach((track) => {
            track.visited_ats.forEach((time_visted) => {
                let label = dateToString(time_visted);
                if (days == 1) {
                    // làm tròn giờ
                    label = timeToString(time_visted);
                    label = label.split(':')[0] + ':00';
                }
                result[label] ? (result[label].view += 1) : null;
            });
            let time_visted = track.visited_ats[0];
            let label = dateToString(time_visted);
            if (days == 1) {
                // làm tròn giờ
                label = timeToString(time_visted);
                label = label.split(':')[0] + ':00';
            }
            result[label] ? (result[label].access_times += 1) : null;
        });

        //4: Doanh thu/ Đơn
        //3 Tỷ lệ chuyển đổi:
        result = Object.values(result);
        result = result.map((element) => {
            if (element.order) {
                element.revenue_per_order = parseFloat(
                    (element.revenue / element.order).toFixed(2)
                );
            }
            if (element.view) {
                element.conversion_rate = parseFloat(
                    (element.order / element.view).toFixed(2)
                );
            }
            delete element.total_user_have_order;
            return element;
        });
        return Object.values(result);
    }

    // Shop Metric
    public async totalShopPeriod(date_start, date_end) {
        const allShop = await shopRepo.getListShopActiveAndNewShop(
            date_start,
            date_end
        );

        const newShop = allShop.approved_shop?.filter(
            (shop) => shop.products <= 0
        );
        const activeShop = allShop.approved_shop.filter(
            (shop) => shop.products > 0
        );
        delete allShop.approved_shop;

        return {
            allShop: allShop.all_shop,
            activeShop,
            newShop,
        };
    }

    public async _columnChartTotalShop(date_start, date_end) {
        const shopObject = await this.totalShopPeriod(date_start, date_end);
        const settingVersion: any = await systemSetting.findOne({});
        let result = { 
            all_shop: {}, 
            new_shop: {}, 
            active_shop: {}, 
            target: this.handleTarget(date_start, date_end, settingVersion?.target_new_shop)
        };

        const monthStart = date_start.getMonth() + 1;
        const monthEnd = date_end.getMonth() + 1;
        const yearStart = date_start.getFullYear();
        const yearEnd = date_end.getFullYear();
        let difference = date_end.getTime() - date_start.getTime();
        let days = Math.ceil(difference / (1000 * 3600 * 24));
        const resultAllShop: any = this.init_chart(date_start, date_end);
        const resultNewShop: any = this.init_chart(date_start, date_end);
        const resultActiveShop: any = this.init_chart(date_start, date_end);

        shopObject.allShop?.forEach((shop) => {
            this.fillDataForChart(
                days,
                monthStart,
                monthEnd,
                yearStart,
                yearEnd,
                shop.createdAt,
                1,
                resultAllShop
            );
        });

        result.all_shop = {
            total: shopObject.allShop?.length || 0,
            range: resultAllShop,
        };

        shopObject.newShop.forEach((shop) => {
            this.fillDataForChart(
                days,
                monthStart,
                monthEnd,
                yearStart,
                yearEnd,
                shop.createdAt,
                1,
                resultNewShop
            );
        });

        result.new_shop = {
            total: shopObject.newShop?.length || 0,
            range: resultNewShop,
        };

        shopObject.activeShop.forEach((shop) => {
            this.fillDataForChart(
                days,
                monthStart,
                monthEnd,
                yearStart,
                yearEnd,
                shop.createdAt,
                1,
                resultActiveShop
            );
        });

        result.active_shop = {
            total: shopObject.activeShop?.length || 0,
            range: resultActiveShop,
        };

        return result;
    }

    public async _pieChartTotalShop(date_start, date_end) {
        const shopObject = await this.totalShopPeriod(date_start, date_end);
        return {
            all_shop: shopObject.allShop?.length ?? 0,
            active_shop: shopObject.activeShop?.length ?? 0,
            new_shop: shopObject.newShop?.length ?? 0,
        };
    }

    public async shopByCategoriesPeriod(date_start, date_end) {
        let shops = await shopRepo.shopDividedByCategories(
            date_start,
            date_end
        );

        const listShopBelongCategories = shops?.filter(
            (shop) => shop.products.length > 0
        );

        // Divided by categories
        const listFashionShop = [];
        const listCosmeticShop = [];
        const listFashionShopAndCosmeticShop = [];
        listShopBelongCategories.forEach((shop) => {
            let categories = shop?.products?.map((product) =>
                product?.categories?.name?.toLowerCase()
            );
            categories = [...new Set(categories)];

            const intersectionCategoriesFashion = categories.filter((item) =>
                CATEGORIES_FASHION.includes(item)
            );
            const intersectionCategoriesCosmetic = categories.filter((item) =>
                CATEGORIES_COSMETIC.includes(item)
            );

            if (
                intersectionCategoriesFashion?.length > 0 &&
                intersectionCategoriesCosmetic.length > 0
            ) {
                listFashionShopAndCosmeticShop.push(shop);
            } else if (
                intersectionCategoriesFashion?.length > 0 &&
                intersectionCategoriesCosmetic.length <= 0
            ) {
                listFashionShop.push(shop);
            } else if (
                intersectionCategoriesFashion?.length <= 0 &&
                intersectionCategoriesCosmetic.length > 0
            ) {
                listCosmeticShop.push(shop);
            }
        });

        return {
            fashionShop: listFashionShop,
            cosmeticShop: listCosmeticShop,
            other: listFashionShopAndCosmeticShop,
        };
    }

    public async _columnChartShopByCategories(date_start, date_end) {
        const shopCategories = await this.shopByCategoriesPeriod(
            date_start,
            date_end
        );

        const monthStart = date_start.getMonth() + 1;
        const monthEnd = date_end.getMonth() + 1;
        const yearStart = date_start.getFullYear();
        const yearEnd = date_end.getFullYear();
        let difference = date_end.getTime() - date_start.getTime();
        let days = Math.ceil(difference / (1000 * 3600 * 24));
        const resultFashionShop: any = this.init_chart(date_start, date_end);
        const resultCosmeticShop: any = this.init_chart(date_start, date_end);
        const resultFashionShopAndCosmeticShop: any = this.init_chart(
            date_start,
            date_end
        );

        let result = {
            fashion_shop: {},
            cosmetic_shop: {},
            fashion_cosmetic_shop: {},
        };

        shopCategories.fashionShop?.forEach((shop) => {
            this.fillDataForChart(
                days,
                monthStart,
                monthEnd,
                yearStart,
                yearEnd,
                shop.createdAt,
                1,
                resultFashionShop
            );
        });

        result.fashion_shop = {
            total: shopCategories.fashionShop.length ?? 0,
            range: resultFashionShop,
        };

        shopCategories.cosmeticShop?.forEach((shop) => {
            this.fillDataForChart(
                days,
                monthStart,
                monthEnd,
                yearStart,
                yearEnd,
                shop.createdAt,
                1,
                resultCosmeticShop
            );
        });

        result.cosmetic_shop = {
            total: shopCategories.cosmeticShop?.length ?? 0,
            range: resultCosmeticShop,
        };

        shopCategories.other?.forEach((shop) => {
            this.fillDataForChart(
                days,
                monthStart,
                monthEnd,
                yearStart,
                yearEnd,
                shop.createdAt,
                1,
                resultFashionShopAndCosmeticShop
            );
        });

        result.fashion_cosmetic_shop = {
            total: shopCategories.other?.length ?? 0,
            range: resultFashionShopAndCosmeticShop,
        };

        return result;
    }

    public async _pieChartShopByCategories(date_start, date_end) {
        const shopCategories = await this.shopByCategoriesPeriod(
            date_start,
            date_end
        );
        let result = {
            fashion_shop: shopCategories?.fashionShop?.length ?? 0,
            cosmetic_shop: shopCategories?.cosmeticShop?.length ?? 0,
            fashion_cosmetic_shop:
                shopCategories?.other?.length ?? 0,
        };
        return result;
    }

    public async ratingShopPeriod(date_start, date_end) {
        try {
            let ratingShops = await feedbackRepo.getRatingAllShop(
                date_start,
                date_end
            );

            const totalRating = Object.values(ratingShops).reduce(
                (total: number, index: number) => total + index
            );

            ratingShops.four_five_stars = {
                number: ratingShops.four_five_stars,
                percent:
                    roundNumberPercent(
                        (ratingShops.four_five_stars * 100) / +totalRating,
                        2
                    ) || 0,
            };

            ratingShops.two_three_stars = {
                number: ratingShops.two_three_stars,
                percent:
                    roundNumberPercent(
                        (ratingShops.two_three_stars * 100) / +totalRating,
                        2
                    ) || 0,
            };

            ratingShops.less_than_two_stars = {
                number: ratingShops.less_than_two_stars,
                percent:
                    roundNumberPercent(
                        (ratingShops.less_than_two_stars * 100) / +totalRating,
                        2
                    ) || 0,
            };

            return ratingShops;
        } catch (error) {
            console.log(error);
        }
    }

    public async ratingShopExport(date_start, date_end) {
        try {
            return  feedbackRepo.getRatingAllShopExport(
                date_start,
                date_end
            );

        } catch (error) {
            console.log(error);
        }
    }

    public async getTotalRevenue(date_start, date_end) {
        const query = [
            {
                shipping_status: {
                    $in: [
                        ShippingStatus.SHIPPED,
                        ShippingStatus.SHIPPING,
                        ShippingStatus.WAIT_TO_PICK,
                        ShippingStatus.PENDING,
                    ],
                },
            },
            {
                created_at: {
                    $gte: date_start,
                    $lte: date_end,
                },
            },
        ];

        const monthStart = date_start.getMonth() + 1;
        const monthEnd = date_end.getMonth() + 1;
        const yearStart = date_start.getFullYear();
        const yearEnd = date_end.getFullYear();
        let difference = date_end.getTime() - date_start.getTime();
        let days = Math.ceil(difference / (1000 * 3600 * 24));
        let result: any = this.init_chart(date_start, date_end);

        let orders = await orderRepository.getOrdersByCondition(query);
        let total = 0;

        orders.forEach((order) => {
            total += order.total_price;
            this.fillDataForChart(
                days,
                monthStart,
                monthEnd,
                yearStart,
                yearEnd,
                order.created_at,
                order.total_price,
                result
            );
        });
        const totalRevenue = {
            totalRevenue: total,
            range: result,
        };
        return totalRevenue;
    }

    public async getRevenueByCategoriesExport(date_start, date_end) {
        const query = [
            {
                shipping_status: {
                    $in: [
                        ShippingStatus.SHIPPED,
                        ShippingStatus.SHIPPING,
                        ShippingStatus.WAIT_TO_PICK,
                        ShippingStatus.PENDING,
                    ],
                },
            },
            {
                created_at: {
                    $gte: date_start,
                    $lte: date_end,
                },
            },
        ];
        let orders = await orderRepository.getOrdersByCategory(query);

        let result = {
            fashionRevenue: [],
            cosmeticRevenue: [],
            otherRevenue: [],
        };

        orders?.forEach((item: any) => {
            const listVoucherDiscount = item.vouchers.filter((voucher: any) => voucher?.classify !== "free_shipping");
            item.voucher_system_discount = 0;
            item.voucher_shop_discount = 0;
            item.total_voucher_discount = 0;

            listVoucherDiscount.forEach((voucher: any) => {
                if (voucher?.classify === "discount") {
                    item.voucher_system_discount += (+voucher?.discount);
                } else {
                    item.voucher_shop_discount += (+voucher?.discount);
                }

                item.total_voucher_discount += (+voucher?.discount);
            })

            delete item?.vouchers;

            const handleDate: any = moment(
                new Date(item?.history?.[0]?.action_time)
            ).format('YYYY-DD-MM HH:MM:SS');

            item.date_by_shipping_status =
                handleDate !== 'Invalid date' ? handleDate : '';

            delete item?.history;

            let categories = item.order_items?.map((item) =>
                item.product?.category?.name?.toLowerCase()
            );

            delete item.order_items;

            categories = [...new Set(categories)];

            const intersectionCategoriesFashion = categories.filter((item) =>
                CATEGORIES_FASHION.includes(item)
            );
            const intersectionCategoriesCosmetic = categories.filter((item) =>
                CATEGORIES_COSMETIC.includes(item)
            );

            switch (true) {
                case intersectionCategoriesFashion?.length > 0 && intersectionCategoriesCosmetic.length > 0:
                    result.otherRevenue.push(item);
                    break;

                case intersectionCategoriesFashion?.length > 0 && intersectionCategoriesCosmetic.length <= 0:
                    result.fashionRevenue.push(item);
                    break;

                case intersectionCategoriesFashion?.length <= 0 && intersectionCategoriesCosmetic.length > 0:
                    result.cosmeticRevenue.push(item);
                    break;
            
                default:
                    break;
            }
        });
    
        return result;
    }

    public async getRevenueExport(date_start, date_end) {
        const condition = {
            shipping_status: {
                $in: [
                    ShippingStatus.SHIPPED,
                    ShippingStatus.SHIPPING,
                    ShippingStatus.WAIT_TO_PICK,
                    ShippingStatus.PENDING,
                ],
            },
        };
        
        let orders = await orderRepository.getOrdersExport(date_start, date_end, condition);

        const revenue = orders?.map((item: any) => {
            const listVoucherDiscount = item.vouchers.filter((voucher: any) => voucher?.classify !== "free_shipping");
            item.voucher_system_discount = 0;
            item.voucher_shop_discount = 0;
            item.voucher_cashback_discount = 0;
            item.total_voucher_discount = 0;

            listVoucherDiscount.forEach((voucher: any) => {
                if (voucher?.classify === "discount") {
                    item.voucher_system_discount += (+voucher?.discount);
                } else if (voucher?.classify === "cash_back") {
                    item.voucher_cashback_discount += (+voucher?.discount);
                } else {
                    item.voucher_shop_discount += (+voucher?.discount);
                }

                item.total_voucher_discount += (+voucher?.discount);
            })

            delete item?.vouchers;

            const handleDate: any = moment(
                new Date(item?.history?.[0]?.action_time)
            ).format('YYYY-DD-MM HH:MM:SS');

            item.date_by_shipping_status =
                handleDate !== 'Invalid date' ? handleDate : '';

            delete item?.history;

            return item;
        });

        return { revenue };
    }

    public async getRevenueByCategories(date_start, date_end) {
        const query = [
            {
                shipping_status: {
                    $in: [
                        ShippingStatus.SHIPPED,
                        ShippingStatus.SHIPPING,
                        ShippingStatus.WAIT_TO_PICK,
                        ShippingStatus.PENDING,
                    ],
                },
            },
            {
                created_at: {
                    $gte: date_start,
                    $lte: date_end,
                },
            },
        ];
        let orders = await orderRepository.getOrdersByCategory(query);

        let result = {
            fashion_order: {},
            cosmetic_order: {},
            fashion_cosmetic_order: {},
        };

        let listFashionOrder = 0;
        let listCosmeticOrder = 0;
        let listFashionOrderAndCosmeticOrder = 0;
        orders.forEach((order) => {
            let categories = order.order_items?.map((item) =>
                item.product?.category?.name?.toLowerCase()
            );
            categories = [...new Set(categories)];

            const intersectionCategoriesFashion = categories.filter((item) =>
                CATEGORIES_FASHION.includes(item)
            );
            const intersectionCategoriesCosmetic = categories.filter((item) =>
                CATEGORIES_COSMETIC.includes(item)
            );

            if (
                intersectionCategoriesFashion?.length > 0 &&
                intersectionCategoriesCosmetic.length > 0
            ) {
                listFashionOrderAndCosmeticOrder += order.total_price;
            } else if (
                intersectionCategoriesFashion?.length > 0 &&
                intersectionCategoriesCosmetic.length <= 0
            ) {
                listFashionOrder += order.total_price;
            } else if (
                intersectionCategoriesFashion?.length <= 0 &&
                intersectionCategoriesCosmetic.length > 0
            ) {
                listCosmeticOrder += order.total_price;
            }
        });

        result.fashion_order = listFashionOrder;
        result.cosmetic_order = listCosmeticOrder;
        result.fashion_cosmetic_order = listFashionOrderAndCosmeticOrder;
        return result;
    }

    public async getOrderQuantity(date_start, date_end) {
        const query = [
            {
                created_at: {
                    $gte: date_start,
                    $lte: date_end,
                },
            },
            {
                $or: [
                    {
                        deleted_at: { $exists: false },
                    },
                    {
                        deleted_at: null,
                    },
                ],
            },
        ];

        return await orderRepository.getOrdersByCondition(query);
    }

    public async getOrderExport(date_start, date_end) {
        try {
            const orders: any = await orderRepository.getOrdersExport(date_start, date_end);
            const allOrders = orders?.map((item: any) => {
                const listVoucherDiscount = item.vouchers.filter((voucher: any) => voucher?.classify !== "free_shipping");
                item.voucher_system_discount = 0;
                item.voucher_cashback_discount = 0;
                item.voucher_shop_discount = 0;
                item.total_voucher_discount = 0;

                listVoucherDiscount.forEach((voucher: any) => {
                    if (voucher?.classify === "discount") {
                        item.voucher_system_discount += (+voucher?.discount);
                    } else if (voucher?.classify === "cash_back") {
                        item.voucher_cashback_discount += (+voucher?.discount);
                    } else {
                        item.voucher_shop_discount += (+voucher?.discount);
                    }

                    item.total_voucher_discount += (+voucher?.discount);
                })

                delete item?.vouchers;

                const handleDate: any = moment(
                    new Date(item?.history?.[0]?.action_time)
                ).format('YYYY-DD-MM HH:MM:SS');

                item.date_by_shipping_status =
                    handleDate !== 'Invalid date' ? handleDate : '';

                delete item?.history;

                return item;
            });

            const ordersPending = allOrders.filter((item: any) => item.shipping_status === ShippingStatus.PENDING );

            const ordersWaitToPick = allOrders.filter((item: any) => item.shipping_status === ShippingStatus.WAIT_TO_PICK );

            const ordersShipping = allOrders.filter((item: any) => item.shipping_status === ShippingStatus.SHIPPING );

            const ordersShipped = allOrders.filter((item: any) => item.shipping_status === ShippingStatus.SHIPPED );

            const ordersCanceled = allOrders.filter((item: any) => item.shipping_status === ShippingStatus.CANCELED );

            const ordersCanceling = allOrders.filter((item: any) => item.shipping_status === ShippingStatus.CANCELING );

            const ordersReturn = allOrders.filter((item: any) => item.shipping_status === ShippingStatus.RETURN );

            return {
                allOrders, 
                ordersPending,
                ordersWaitToPick,
                ordersShipping,
                ordersShipped,
                ordersCanceled,
                ordersCanceling,
                ordersReturn,
            };
            
        } catch (error) {
            console.log(error);
        }
        
    }

    public async _columnChartTransaction(date_start, date_end) {
        const orders = await this.getOrderQuantity(date_start, date_end);

        const monthStart = date_start.getMonth() + 1;
        const monthEnd = date_end.getMonth() + 1;
        const yearStart = date_start.getFullYear();
        const yearEnd = date_end.getFullYear();
        let difference = date_end.getTime() - date_start.getTime();
        let days = Math.ceil(difference / (1000 * 3600 * 24));
        let result: any = this.init_order_quantity_chart(date_start, date_end);

        orders.forEach((order) => {
            this.fillDataForChart(
                days,
                monthStart,
                monthEnd,
                yearStart,
                yearEnd,
                order.created_at,
                1,
                result,
                {
                    transaction: false,
                    isApproved: false,
                    pauseMode: true,
                    orderTransaction: true,
                    shippingStatus: order.shipping_status,
                }
            );
        });

        return result;
    }

    public async _pieChartTransaction(date_start, date_end) {
        const orders = await this.getOrderQuantity(date_start, date_end);
        const data = {
            pending: 0,
            total: 0,
            wait_to_pick: 0,
            shipped: 0,
            canceled: 0,
            canceling: 0,
        };
        orders.forEach((order) => {
            const status = Object.keys(data).find(
                (item) => item === order.shipping_status
            );
            if (status) {
                data[status] += 1;
                data.total += 1;
            }
        });

        return data;
    }

    public async shopTransaction(date_start, date_end) {
        const query = [
            {
                created_at: {
                    $gte: date_start,
                    $lte: date_end,
                },
            },
        ];

        const monthStart = date_start.getMonth() + 1;
        const monthEnd = date_end.getMonth() + 1;
        const yearStart = date_start.getFullYear();
        const yearEnd = date_end.getFullYear();
        let difference = date_end.getTime() - date_start.getTime();
        let days = Math.ceil(difference / (1000 * 3600 * 24));
        let result: any = this.init_shop_order_quantity_chart(
            date_start,
            date_end
        );

        let orders = await orderRepository.getOrdersByShop(query);
        const settingVersion: any = await systemSetting.findOne({});

        orders.forEach((order) => {
            this.fillDataForChart(
                days,
                monthStart,
                monthEnd,
                yearStart,
                yearEnd,
                order.created_at,
                1,
                result,
                {
                    transaction: true,
                    isApproved: order.shop.is_approved,
                    pauseMode: order.shop.pause_mode,
                    orderTransaction: false,
                    shippingStatus: null,
                }
            );
        });
        return { range: result, target: this.handleTarget(date_start, date_end, settingVersion?.target_transaction) };
    }

    public async getShopSpending(date_start, date_end) {
        const query = [
            {
                shipping_status: {
                    $in: [
                        ShippingStatus.SHIPPED,
                        ShippingStatus.SHIPPING,
                        ShippingStatus.WAIT_TO_PICK,
                        ShippingStatus.PENDING,
                    ],
                },
            },
            {
                created_at: {
                    $gte: date_start,
                    $lte: date_end,
                },
            },
        ];
        let result = await orderRepository.getOrdersAndTotalPriceByCondition(
            query
        );
        const shopSpending = {
            averageSpending: 0,
            maxSpending: 0,
            minSpending: 0,
        };
        if (!result.length) return shopSpending;
        const obj = result[0]; // because array result only has one element
        shopSpending.averageSpending = Math.round(
            obj.total.sum / obj.orders.length
        );
        let numOrderPriceLTAvgSpending = 0; // number of order price which is less than average spending
        let numOrderPriceGTAvgSpending = 0; // number of order price which is greater than average spending
        let totalOrderPriceLTAvgSpending = 0;
        let totalOrderPriceGTAvgSpending = 0;
        obj.orders.forEach((order) => {
            if (order.total_price < shopSpending.averageSpending) {
                numOrderPriceLTAvgSpending++;
                totalOrderPriceLTAvgSpending += order.total_price;
            }
            if (order.total_price > shopSpending.averageSpending) {
                numOrderPriceGTAvgSpending++;
                totalOrderPriceGTAvgSpending += order.total_price;
            }
        });
        shopSpending.minSpending = Math.round(
            totalOrderPriceLTAvgSpending / numOrderPriceLTAvgSpending
        );
        shopSpending.maxSpending = Math.round(
            totalOrderPriceGTAvgSpending / numOrderPriceGTAvgSpending
        );
        return shopSpending;
    }
}

const generalAnalystService = new GeneralAnalystService();
export default generalAnalystService;
