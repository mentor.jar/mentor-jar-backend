import { VoucherDTO } from '../../DTO/VoucherDTO';
import { InMemoryVoucherStore } from '../../SocketStores/VoucherStore';
import { cloneObj } from '../../utils/helper';
import { VoucherRepository } from './../../repositories/VoucherRepository';
const voucherRepo = VoucherRepository.getInstance();
const voucherAndOrderCache = InMemoryVoucherStore.getInstance();
class VoucherService {
    private static _instance: VoucherService;

    static get _() {
        if (!this._instance) {
          this._instance = new VoucherService();
        }
        return this._instance;
      }
    

    getShopVouchers = async (shopId, date) => {
        const vouchers = await voucherRepo.getShopVouchers(shopId,date)
        return vouchers
    }

    syncVoucherSystem = async () => {
      const date = new Date()
        let vouchers:any = await voucherRepo.find({
            $or: [
                { start_time: {$lte: date}, end_time: {$gte: date} }, 
                { start_time: {$gte: date} }
            ],
            shop_id: null,
            // is_public: true,
            deleted_at: null,
            available_quantity: {$gt: 0},
            classify: {$nin: ['cash_back_discount', 'cash_back']}
        })
        vouchers = cloneObj(vouchers)
        vouchers = vouchers.map(voucher => new VoucherDTO(voucher).toVoucherSystemCacheJSON())
        voucherAndOrderCache.save("voucherSystems", vouchers)
        return vouchers
    }
}

export default VoucherService