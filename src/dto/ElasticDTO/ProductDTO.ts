export class ProductElsDTO {
    private _index: string;
    private _id: string;
    private _type: string;
    private _score: string;
    private _source: object;

    constructor({ _index, _id, _type, _score, _source }) {
        this._id = _id;
        this._index = _index;
        this._type = _type;
        this._score = _score;
        this._source = _source;
    }

    get(): any {
        return {
            _id: this._id,
            ...this._source,
        };
    }
}
