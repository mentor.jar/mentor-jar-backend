import { BaseDTO } from "./BaseDTO";

export class OrderShippingHistoryDTO extends BaseDTO {

    protected obj;
    protected fillableDB = ['partner_id', 'label_id', 'status_id', 'action_time', 'reason_code', 'reason', 'weight', 'fee', 'pick_money', 'return_part_package', 'shipping_status'];


    protected fillable = ['order_number', 'shipping_status', 'action_time'];

    static newInstance(object) {
        return new OrderShippingHistoryDTO(object);
    }

    constructor(dto) {
        super();
        this.obj = dto
        if (this.obj.shipping_status) {
            const reason = this.obj.reason_code_shipping ? this.obj.reason_code_shipping : this.obj.reason
            this.obj.shipping_status = !reason ? this.obj.shipping_status : this.obj.shipping_status + ' vì: ' + reason
        }
        this.setAttribute('order_number', this.obj.partner_id)
        this.setAttribute('shipping_status', this.obj.shipping_status )
    }

    toRequestJSON() {
        throw new Error("Method not implemented.");
    }

    toSimpleJSON = () => {
        return this.toJSON(['partner_id', 'label_id', 'status_id', 'action_time', 'reason_code', 'reason', 'weight', 'fee', 'pick_money', 'return_part_package', 'shipping_status']);
    }
    
    toCompeleteJSON = () => {
        return this.toJSON(['shipping_status', 'action_time']);
    }
}

export class OrderShippingDTO extends BaseDTO {

    protected obj;
    protected fillableDB = ['_id', 'user_confirm_shipped_time', 'assign_to_shipping_unit_time', 'history', 'shipping_info', 'order_id', 'user_id', 'shop_id'];


    protected fillable = ['_id', 'user_confirm_shipped_time', 'assign_to_shipping_unit_time', 'history', 'shipping_info', 'order_id', 'user_id', 'shop_id'];

    static newInstance(object) {
        return new OrderShippingDTO(object);
    }

    constructor(dto) {
        super();
        this.obj = dto
    }

    toRequestJSON() {
        throw new Error("Method not implemented.");
    }

    toSimpleJSON = () => {
        return this.toJSON(['history']);
    }
    
    toCompeleteJSON = (order, shipping_name, orderHistory) => {
        const data : any = OrderShippingDTO.newInstance(this.obj).toSimpleJSON();
        
        data.shipping_status = order.shipping_status;
        data.order_shipping_number = orderHistory.shipping_info ? orderHistory.shipping_info.label : null ;
        data.shipping_name = shipping_name;

        if (!this.obj.history.length && this.obj.shipping_info) {
            data.history = this.setHistoryShippingDefalt(order);
            return data;
        }

        data.history = data.history.map((item) => {
            item = new OrderShippingHistoryDTO(item).toCompeleteJSON();
            return item;
        }).sort((min, max) => {
            min = new Date(min.action_time);
            max = new Date(max.action_time);
            return max - min;
        });
        
        return data;
    }

    setHistoryShippingDefalt = (order) => {
        let shipping_status = 'Người gửi đang chuẩn bị hàng';

        let status = [{
            shipping_status: shipping_status,
            action_time: order.created_at
        }]
        return status;
    }
}

