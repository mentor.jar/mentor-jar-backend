import { BaseDTO } from "./BaseDTO";

export class ImageDTO extends BaseDTO {

    protected fillableDB = ['_id', 'accessible_type', 'accessible_id', 'created_at', 'updated_at'];
    protected _id;
    protected accessible_type;
    protected accessible_id;
    protected path;
    protected mimetype;
    protected filename;
    protected encoding;
    protected driver_type;
    protected created_at;
    protected updated_at;

    protected fillable = ['_id', 'accessible_type', 'accessible_id', 'created_at', 'updated_at'];

    constructor(dto) {
        super();
        this._id = dto._id
        this.accessible_type = dto.accessible_type
        this.accessible_id = dto.accessible_id
        this.path = dto.path
        this.mimetype = dto.mimetype
        this.filename = dto.filename
        this.encoding = dto.encoding
        this.driver_type = dto.driver_type
        this.created_at = dto.created_at
        this.updated_at = dto.updated_at
    }

    toSimpleJSON = () => ({
        _id: this._id,
        accessible_type: this.accessible_type,
        accessible_id: this.accessible_id,
        path: this.path,
        filename: this.filename
    });

    toRequestJSON() {
        throw new Error("Method not implemented.");
    }
}

