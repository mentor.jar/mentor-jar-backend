
import { BaseDTO } from "./BaseDTO"
export class CategoryDTO extends BaseDTO {


    protected obj;
    protected fillable = ['_id', 'name', 'name_localized', 'description', 'permalink', 'priority',
        'shop_id', 'parent_id', 'is_active', 'avatar', 'pdfAvatar', 'created_at', 'updated_at', 'childs', 'type'
    ];
    protected fillableDB = ['_id', 'name', 'name_localized', 'permalink', 'parent_id', 'priority', 'avatar'];
    static fieldTree = ['_id', 'name', 'name_localized', 'permalink', 'priority', 'shop_id', 'parent_id', 'childs', 'is_active', 'avatar', 'pdfAvatar', 'created_at', 'updated_at', 'type'];
    constructor(dto) {
        super();
        this.obj = dto
    }

    toSimpleJSON = () => {
        return this.toJSON(['_id', 'name', 'name_localized', 'shop_id', 'permalink', 'parent_id', 'priority', 'childs', 'is_active', 'avatar', 'pdfAvatar', 'created_at', 'updated_at'])
    };

    toJSONWithProduct = () => {
        return this.toJSON(['_id', 'name', 'shop_id', 'permalink', 'parent_id', 'products', 'priority', 'childs', 'is_active', 'avatar', 'pdfAvatar', 'created_at', 'updated_at'])
    };

    toRequestJSON() {
        let obj: any = this.toJSON(['name', 'description', 'product_id', 'option_values'])
        if (this.obj._id) {
            obj._id = this.obj._id;
        }

        return obj;
    }

    child = (arrayChild: Array<any>) => {
        return arrayChild.map(e => {
            return new CategoryDTO(e)
        });
    }

    toSimpleCategoryItem() {
        return this.toJSON(['_id', 'name', 'name_localized', 'shop_id', 'permalink', 'parent_id', 'priority', 'childs', 'is_active', 'avatar', 'pdfAvatar', 'type', 'created_at', 'updated_at'])
    }
}
