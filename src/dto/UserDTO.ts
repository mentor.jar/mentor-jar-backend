import keys from "../config/env/keys";
import { addLink } from "../utils/stringUtil";
import { BaseDTO } from "./BaseDTO";

export class UserDTO extends BaseDTO {

    protected obj;
    protected fillableDB = ['_id', 'userName', 'avatar', 'email', 'phone', 'gender', 'shop_id',
        'phone', 'isActive', 'created_at', 'updated_at', 'email_verify', 'phone_verify', 'saved_vouchers',
        'detail_size_history', 'bodyMeasurement', 'is_newbie', 'shorten_link'];


    protected fillable = ['_id', 'nameOrganizer', 'seen_last_message_index', 'userName',
        'is_muted', 'is_online', 'userName', 'avatar', 'email', 'phone', 'gender',
        'phone', 'isActive', 'follow_count', 'following_count', 'feedback', 'is_hide',
        'is_live', 'is_follow', 'isVerified', 'email_verify', 'phone_verify', 'shop_id',
        'saved_vouchers', 'created_at', 'updated_at', 'referral_code', 'detail_size_history',
        'seen_last_message_id', 'bodyMeasurement', 'is_newbie', 'shorten_link', 'phoneNumber',
        'birthday', 'createdAt', 'updatedAt', 'address', 'total_product', 'totalPosts', 'gallery_image', 'lastActivityAt'];

    constructor(dto) {
        super();
        this.obj = dto
        if (!this.obj.userName) {
            this.obj.userName = dto.nameOrganizer?.userName
        }

        this.setAttribute('follow_count', this.obj.followCount)
        this.setAttribute('following_count', this.obj.followingCount)
    }

    toRequestJSON() {
        throw new Error("Method not implemented.");
    }

    toSimpleJSON = () => {
        return this.toJSON(['_id', 'userName', 'avatar', 'email', 'gender', 'is_newbie', 'shorten_link', 'shop_id', 'gallery_image']);
    }

    toSimpleUserInfo = () => {
        let user: any = this.toJSON([
            '_id',
            'nameOrganizer',
            'avatar',
            'userName',
            'email',
            'follow_count',
            'email_verify',
            'phone_verify',
            'following_count',
            'is_follow',
            'saved_vouchers',
            'referral_code',
            'detail_size_history',
            'bodyMeasurement',
            'is_newbie',
            'shorten_link',
            'phoneNumber',
            'birthday',
            'address',
            'totalPosts',
            'total_product',
            'createdAt',
            'updatedAt',
        ]);
        user.email_verified = user.email_verify ? user.email_verify.verified : false
        user.phone_verified = user.phone_verify ? user.phone_verify.verified : false
        delete user.email_verify
        delete user.phone_verify
        return user
    }

    toCompressUserInfo = () => {
        let user: any = this.toJSON([
            '_id',
            'nameOrganizer',
            'avatar',
            'userName',
            'email',
            'follow_count',
            'email_verify',
            'phone_verify',
            'following_count',
            'is_follow',
            'shorten_link',
            'phoneNumber'
        ]);
        user.email_verified = user.email_verify ? user.email_verify.verified : false
        user.phone_verified = user.phone_verify ? user.phone_verify.verified : false
        delete user.email_verify
        delete user.phone_verify
        return user
    }

    toSimpleJSONForAdmin = () => {
        return this.toJSON([
            '_id',
            'receiveNotifications',
            'nameOrganizer',
            'avatar',
            'userName',
            'email',
            'gender',
            'follow_count',
            'email_verify',
            'phone_verify',
            'following_count',
            'is_follow',
            'saved_vouchers',
            'referral_code',
            'detail_size_history',
            'bodyMeasurement',
            'is_newbie',
            'phoneNumber',
            'birthday',
            'createdAt',
            'updatedAt',
            'gallery_image'
        ]);
    };

    toStreamDetailJSON = () => {
        return this.toJSON(['_id', 'nameOrganizer', 'avatar', 'userName', 'email', 'follow_count', 'following_count', 'is_follow', 'saved_vouchers'])
    }

    toRoomChatJSON = () => {
        return this.toJSON(['_id', 'userName', 'avatar', 'email', 'shop_id', 'gender', 'is_muted', 'is_online', 'seen_last_message_id', 'seen_last_message_index', 'is_hide']);
    }

    toDetailChatJSON = () => {
        return this.toJSON(['_id', 'userName', 'avatar', 'email', 'gender', 'lastActivityAt', 'is_online'])
    }
    toShopExploreJSON = () => {
        return this.toJSON(['_id', 'nameOrganizer', 'avatar', 'userName', 'email', 'follow_count', 'following_count', 'feedback', 'is_live', 'is_follow', 'isVerified', 'saved_vouchers'])
    }

}

