
import { BaseDTO } from "./BaseDTO";

export class GroupOrderDTO extends BaseDTO {

    protected obj;
    protected fillableDB = ['_id', 'group_order_id', 'user_id', 'orders', 'total_price', 'created_at', 'updated_at'];


    protected fillable = ['_id', 'group_order_id', 'user_id', 'orders', 'total_price', 'created_at', 'updated_at'];;

    static newInstance(object) {
        return new GroupOrderDTO(object);
    }

    constructor(dto) {
        super();
        this.obj = dto
    }

    toRequestJSON() {
        throw new Error("Method not implemented.");
    }

    toSimpleJSON = () => {
        this.obj.group_order_id = this.obj._id
        delete this.obj._id
        return this.toJSON(['group_order_id', 'user_id', 'orders', 'total_price', 'created_at', 'updated_at']);
    }
}

