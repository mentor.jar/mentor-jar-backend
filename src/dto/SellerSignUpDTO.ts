import { BaseDTO } from "./BaseDTO";

export class SellerSignUp extends BaseDTO {

    protected obj;
    protected fillableDB = ['_id', 'personal_info', 'shop_info', 'bank_info', 'status', 'approved_time', 'rejected_time', 'shop_id', 'user_mapped', 'created_at', 'updated_at'];


    protected fillable = ['_id', 'personal_info', 'shop_info', 'bank_info', 'status', 'approved_time', 'rejected_time', 'shop_id', 'user_mapped', 'created_at', 'updated_at'];

    constructor(dto) {
        super();
        this.obj = dto
    }

    toRequestJSON() {
        throw new Error("Method not implemented.");
    }

    toSimpleJSON = () => {
        return this.toJSON(['_id', 'personal_info', 'shop_info', 'bank_info', 'status', 'approved_time', 'rejected_time', 'shop_id', 'user_mapped', 'created_at', 'updated_at']);
    }
}
