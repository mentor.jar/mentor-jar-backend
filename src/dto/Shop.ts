import { BaseDTO } from "./BaseDTO";

export class ShopDTO extends BaseDTO {

    protected obj;
    protected fillableDB = ['_id', 'name', 'description', 'user_id', 'user', 'shop_type', 'banners',
        'refund_money_regulations', 'refund_money_mode', 'refund_conditions', 'pause_mode', 'is_approved', 
        'ranking_today', 'ranking_yesterday', 'avg_rating', 'country', 'allow_show_on_top', 'rank_policy', 
        'ranking_criteria', 'system_banner', 'middle_banner', 'created_at', 'updated_at', 'biggest_price', 'bank_info'];


    protected fillable = ['_id', 'name', 'description', 'user_id', 'user', 'shop_type', 'banners',
        'refund_money_regulations', 'refund_money_mode', 'refund_conditions', 'pause_mode', 'is_approved', 
        'ranking_today', 'ranking_yesterday', 'avg_rating', 'shorten_link', 'country', 'allow_show_on_top',
        'rank_policy', 'ranking_criteria', 'system_banner', 'middle_banner', 'createdAt', 'updatedAt', 'biggest_price', 'bank_info'];


    constructor(dto) {
        super();
        this.obj = dto
    }

    protected default = { shop_type: 0, refund_money_mode: false, pause_mode: false }
    toRequestJSON() {
        throw new Error("Method not implemented.");
    }

    toSimpleJSON = () => {
        return this.toJSON(['_id', 'name', 'description', 'user_id', 'user', 'shop_type', 'banners',
            'refund_money_regulations', 'refund_money_mode', 'refund_conditions', 'pause_mode', 'is_approved', 
            'ranking_today', 'ranking_yesterday', 'avg_rating', 'country', 'shorten_link', 'allow_show_on_top', 
            'rank_policy', 'ranking_criteria', 'system_banner', 'middle_banner', 'createdAt', 'updatedAt', 'biggest_price', 'bank_info']);
    }

    toTopShopJSON = () => {
        return this.toJSON(['_id', 'name', 'user_id', 'user', 'ranking_yesterday', 'ranking_today', 'avg_rating', 
        'rank_policy', 'feedback', 'system_banner']);
    }

    toShopVoucherItem() {
        return this.toJSON(['_id', 'name', 'user_id'])
    }
}

