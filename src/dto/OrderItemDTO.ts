import { BaseDTO } from "./BaseDTO";
import { getMinMax } from "../controllers/serviceHandles/product";
import { ProductDTO } from "./Product";
import { cloneObj } from "../utils/helper";

export class OrderItemDTO extends BaseDTO {

    protected obj;
    protected fillableDB = ['_id', 'variant', 'product_id', 'product', 'order_id', 'quantity', 'images', 'created_at', 'updated_at'];


    protected fillable = ['_id', 'variant', 'product_id', 'product', 'order_id', 'quantity', 'images', 'created_at', 'updated_at'];

    static newInstance(object) {
        return new OrderItemDTO(object);
    }

    constructor(dto) {
        super();
        this.obj = dto
    }

    toRequestJSON() {
        throw new Error("Method not implemented.");
    }

    toSimpleJSON = () => {
        return this.toJSON(['_id', 'variant', 'product_id', 'product', 'order_id', 'quantity', 'images', 'created_at', 'updated_at']);
    }
}

