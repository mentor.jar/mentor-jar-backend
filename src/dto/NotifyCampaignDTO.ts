import { BaseDTO } from "./BaseDTO";

export class NotifyCampaignDTO extends BaseDTO {

    protected obj;
    protected fillableDB = ['_id', 'title', 'content', 'description', 'receivers', 'topic', 'schedule', 'data', 'image', 'created_at', 'updated_at', 'timeWillPush']

    protected fillable = ['_id', 'title', 'content', 'description', 'receivers', 'topic', 'schedule', 'data', 'image', 'created_at', 'updated_at', 'timeWillPush'];

    static parse(object) {
        return new NotifyCampaignDTO(object).toSimpleJSON();
    }

    constructor(dto) {
        super();
        this.obj = dto
    }

    toRequestJSON() {
        throw new Error("Method not implemented.");
    }

    toSimpleJSON = () => {
        return this.toJSON([
            '_id', 'title', 'content', 'description',
            'receivers', 'topic', 'schedule', 'data', 'image',
            'created_at', 'updated_at', 'timeWillPush'
        ]);
    }
}

