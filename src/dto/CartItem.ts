import { ShopItemCheckout } from "../controllers/serviceHandles/cart/definition";
import { addLink } from "../utils/stringUtil";
import { BaseDTO } from "./BaseDTO";
import { ShopDTO } from "./Shop";
import { UserDTO } from "./UserDTO";
import keys from "../config/env/keys";

export class CartItemDTO extends BaseDTO {

    protected obj;
    protected fillableDB = ['_id', 'variant_id', 'product_id', 'shop_id', 'user_id', 'quantity', 'created_at', 'updated_at'];


    protected fillable = ['_id', 'items', 'voucher', 'shipping_method', 'shipping', 'shop', 'variant_id', 'product_id', 'shop_id', 'user_id', 'quantity', 'created_at', 'updated_at'];

    static newInstance(object) {
        return new CartItemDTO(object);
    }

    constructor(dto) {
        super();
        this.obj = dto
    }

    toRequestJSON() {
        throw new Error("Method not implemented.");
    }

    toSimpleJSON = () => {
        return this.toJSON(['_id', 'variant_id', 'product_id', 'shop_id', 'user_id', 'quantity', 'created_at', 'updated_at']);
    }

    toCheckoutJSON = () => {
        return this.toJSON(['_id', 'items']);
    }
    toItemSON = () => {
        return this.toJSON(['_id', 'items', 'shop', 'shipping_method', 'shipping', 'voucher']);
    }

    convertItemData() {
        const data: any = CartItemDTO.newInstance(this.obj).toItemSON();
        let item: any = {}
        item._id = data._id;
        item.shop = data.shop;
        item.items = [];
        data.items.forEach(el => {
            let itemCart: any = {};
            itemCart._id = el._id;
            itemCart.product_id = el.product_id;
            itemCart.quantity = el.quantity;
            itemCart.product = el.product;
            if (el.product.variant) {
                itemCart.variant_id = el.product.variant._id;
                if (el.product.sale_price_order_available && el.product.sale_price_order_available > 0) {
                    itemCart.price = el.product.variant.sale_price;
                } else {
                    itemCart.price = el.product.variant.before_sale_price;
                }
            } else {
                if (el.product.sale_price_order_available && el.product.sale_price_order_available > 0) {
                    itemCart.price = el.product.sale_price;
                } else {
                    itemCart.price = el.product.before_sale_price;
                }
            }
            item.items.push(itemCart);
        });
        item.shop = new ShopDTO(item.shop).toSimpleJSON();
        item.shop.user = new UserDTO(item.shop.user).toSimpleJSON();
        item.shop.user.avatar = addLink(`${keys.host_community}/`, item.shop.user.gallery_image.url)
        item.shipping_method = null;
        item.total_weight = data.items.map(item => item.product.weight*item.quantity).reduce((prev, next) => prev + next, 0)
        item.total_price = item.items.map(item => item.price).reduce((prev, next) => prev + next, 0)
        return item;
    }

    convertItemHaveGroupBuyData(group_id) {
        const data: any = CartItemDTO.newInstance(this.obj).toItemSON();
        let item: any = {}
        item._id = data._id;
        item.shop = data.shop;
        item.items = [];
        data.items.forEach(el => {
            let itemCart: any = {};
            itemCart._id = el._id;
            itemCart.product_id = el.product_id;
            itemCart.quantity = el.quantity;
            itemCart.product = el.product;
            if(group_id) {
                const group = el.product.group_buy.groups.find(g => g._id == group_id);
                if (el.product?.variant) {
                    itemCart.variant_id = el.product.variant._id;
                    const variantGroupBuy = group.variants?.find(v => v.variant_id == el.product.variant._id);
                    itemCart.price = variantGroupBuy?.group_buy_price ?? group.group_buy_price;
                } else {
                   itemCart.price = group.group_buy_price;
                }
                itemCart.number_of_member = group.number_of_member;
            } else {
                if (el.product?.variant) {
                    itemCart.variant_id = el.product.variant._id;
                    if (el.product.sale_price_order_available && el.product.sale_price_order_available > 0) {
                        itemCart.price = el.product.variant.sale_price;
                    } else {
                        itemCart.price = el.product.variant.before_sale_price;
                    }
                } else {
                    if (el.product.group_buy.quantity > 0 && el.product) {
                        itemCart.price = el.product.sale_price;
                    } else {
                        itemCart.price = el.product.before_sale_price;
                    }
                }
            }
            
            item.items.push(itemCart);
        });
        item.shop = new ShopDTO(item.shop).toSimpleJSON();
        item.shop.user = new UserDTO(item.shop.user).toSimpleJSON();
        item.shop.user.avatar = addLink(`${keys.host_community}/`, item.shop.user.gallery_image.url)
        item.shipping_method = null;
        item.total_weight = data.items.map(item => item.product.weight*item.quantity).reduce((prev, next) => prev + next, 0)
        item.total_price = item.items.map(item => item.price).reduce((prev, next) => prev + next, 0)
        return item;
    }

    CalculatePriceByShop() {
        const data: any = CartItemDTO.newInstance(this.obj).toItemSON();
        let item: any = {}
        item._id = data._id;
        item.items = [];
        data.items.forEach(el => {
            let itemCart: any = {};
            itemCart._id = el._id;
            itemCart.product_id = el.product_id;
            itemCart.quantity = el.quantity;
            if (el.product.variant) {
                itemCart.variant_id = el.product.variant._id;
                itemCart.price = el.product.variant.sale_price;
            } else {
                itemCart.price = el.product.sale_price;
            }
            item.items.push(itemCart);
        });
        item.voucher = null;
        return item;
    }

    calculateOrderTotal() {
        const data: any = CartItemDTO.newInstance(this.obj).toItemSON();
        let shop_total: number = 0;
        data.items.forEach(el => {
            shop_total += el.quantity * el.price;
        });
        data.shipping_fee = data.shipping.shipping_fee;
        data.shipping_fee_discount = 0;
        data.shop_merchandise_total = shop_total;
        data.discount_of_system_voucher = 0
        data.shop_total = shop_total + data.shipping.shipping_fee;
        return data;
    }

}

