import { BaseDTO } from './BaseDTO';
import { ShopDTO } from './Shop';
import { ProductDTO } from './Product';
import { UserDTO } from './UserDTO';
import { isMappable } from '../utils/helper';

export class TopShopBannerDTO extends BaseDTO {
    protected obj;
    protected fillable = [
        '_id',
        'name',
        'is_active',
        'images',
        'shop_ids',
        'shops',
        'createdAt',
        'updatedAt',
    ];
    protected default = {
        name: '',
        images: [],
        shop_ids: [],
        is_active: true,
        shops: [],
    };
    protected fillableDB = [
        '_id',
        'name',
        'is_active',
        'images',
        'shop_ids',
        'shops',
        'createdAt',
        'updatedAt',
    ];

    constructor(dto) {
        super();
        this.obj = dto;
    }

    static newInstance(object) {
        return new TopShopBannerDTO(object);
    }

    toSimpleJSON = () => {
        return this.toJSON([
            '_id',
            'name',
            'is_active',
            'images',
            'shop_ids',
            'shops',
        ]);
    };

    toRequestJSON = () => {
        let obj: any = this.toJSON(['name', 'is_active', 'images', 'shop_ids']);
        if (this.obj._id) {
            obj._id = this.obj._id;
        }
        return obj;
    };

    completeDataJSON = () => {
        const data: any = TopShopBannerDTO.newInstance(this.obj).toJSON([
            '_id',
            'name',
            'is_active',
            'images',
            'shop_ids',
            'shops',
            'createdAt',
            'updatedAt',
        ]);
        if (isMappable(data.shops)) {
            data.shops = data.shops?.map((item) => {
                item = new ShopDTO(item).toJSON([
                    '_id',
                    'name',
                    'description',
                    'user_id',
                    'user',
                    'shop_type',
                    'banners',
                    'refund_money_mode',
                    'pause_mode',
                    'is_approved',
                    'ranking_today',
                    'ranking_yesterday',
                    'avg_rating',
                    'country',
                    'shorten_link',
                    'allow_show_on_top',
                    'rank_policy',
                    'ranking_criteria',
                    'createdAt',
                    'updatedAt',
                ]);
                item.user = new UserDTO(item.user).toJSON([
                    '_id',
                    'nameOrganizer',
                    'avatar',
                    'userName',
                    'gender',
                    'email',
                    'follow_count',
                    'email_verify',
                    'phone_verify',
                    'following_count',
                    'is_follow',
                    'referral_code',
                    'is_newbie',
                    'shorten_link',
                    'phoneNumber',
                    'birthday',
                    'address',
                    'totalPosts',
                    'total_product',
                    'createdAt',
                    'updatedAt',
                ]);

                return item;
            });
        }

        return data;
    };
}