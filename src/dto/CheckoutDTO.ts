
import { BaseDTO } from "./BaseDTO";

export class CheckoutDTO extends BaseDTO {

    protected obj;
    protected fillableDB = ['_id', 'items'];


    protected fillable = ['_id', 'items'];

    static newInstance(object) {
        return new CheckoutDTO(object);
    }

    constructor(dto) {
        super();
        this.obj = dto
    }

    toRequestJSON() {
        throw new Error("Method not implemented.");
    }

    toSimpleJSON = () => {
        return this.toJSON(['_id', 'items']);
    }

    calculateOrderTotal() {
        const data: any = CheckoutDTO.newInstance(this.obj).toSimpleJSON();
        let item: any = {}
        let shop_total: number = 0;
        item._id = data._id;
        data.items.forEach(el => {
            shop_total += el.quantity * el.price;
        });
        item.shop_total = shop_total;
        return item;
    }

}