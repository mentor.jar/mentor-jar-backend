import { BaseDTO } from "./BaseDTO";

export class ReportStreamDTO extends BaseDTO {

    protected obj;
    protected fillableDB = ['_id', 'reasons', 'user_report_id', 'user_report' , 'stream_session_id', 'stream_session', 'owner_id', 'owner', 'time_report','is_solved', 'created_at', 'updated_at'];

    protected fillable = ['_id', 'reasons', 'user_report_id', 'user_report' , 'stream_session_id', 'stream_session', 'owner_id', 'owner', 'time_report', 'is_solved','created_at', 'updated_at'];

    constructor(dto) {
        super();
        this.obj = dto
    }

    toRequestJSON() {
        throw new Error("Method not implemented.");
    }

    toSimpleJSON = () => {
        return this.toJSON(['_id', 'reasons', 'user_report_id', 'user_report' , 'stream_session_id', 'stream_session', 'owner_id', 'owner', 'time_report', 'is_solved', 'created_at', 'updated_at']);
    }
}