import { BaseDTO } from './BaseDTO';
import { UserDTO } from './UserDTO';

export class RoomChatDTO extends BaseDTO {
    protected obj;
    protected hide_delete;
    protected fillable = [
        '_id',
        'name',
        'type',
        'file_storages',
        'users',
        'creator_id',
        'is_group',
        'messages',
        'last_message',
        'last_object',
        'red_dot',
    ];
    protected fillableDB = [
        '_id',
        'name',
        'type',
        'users',
        'creator_id',
        'is_group',
        'messages',
        'last_message',
        'last_object',
    ];
    protected default = { red_dot: 0 };
    constructor(dto, hide_delete = true) {
        super();
        this.obj = dto;
        this.hide_delete = hide_delete;
        this.setAttribute(
            'users',
            this.obj.users.map(user => {
                let userDTO = new UserDTO(user);
                return userDTO.toRoomChatJSON();
            })
        );
        if (!this.obj.last_message) {
            this.setAttribute('last_message', this.getLastMessage());
        }
        // if (!this.obj.last_object) {
        //     this.setAttribute('last_object', this.getLastObject());
        // }
        this.setRedDotForUser();
        this.setAtributeOtherForUser();
        this.setAttribute('messages', this.replaceMessages());
    }

    toRequestJSON() {
        throw new Error('Method not implemented.');
    }

    toSimpleJSON = () => {
        return this.toJSON([
            '_id',
            'name',
            'type',
            'file_storages',
            'users',
            'creator_id',
            'messages',
            'is_group',
            'last_message',
            'red_dot',
        ]);
    };

    toListJSON = () => {
        return this.toJSON([
            '_id',
            'name',
            'type',
            // 'file_storages',
            'users',
            'creator_id',
            'is_group',
            'last_message',
            'red_dot',
        ]);
    };

    getLastMessage() {
        let lengthMessage = this.obj.messages.length;

        for (let i = lengthMessage - 1; i >= 0; i--) {
            let message = this.obj.messages[i];
            if (!message.deleted_at) {
                return message;
            }
        }
        return null;
    }

    replaceMessages() {
        let messages = this.obj.messages.map((mess) => {
            if (mess.type == 'image') {
                mess.height = mess?.height || 160;
                mess.width = mess?.width || 240;
            }
            return this.addUserToMessage(mess);
        });
        if (this.hide_delete) {
            messages = messages.filter(message => {
                if (message.deleted_at) return 0;
                else return 1;
            });
        }
        return messages;
    }

    addUserToMessage(message) {
        let userFind = this.obj.users.find(user => user._id == message.user_id);
        message.user = userFind ? userFind : null;
        return message;
    }

    // getLoadMoreRecord(){

    // }

    setRedDotForUser() {
        let sizeMess = this.obj.messages.length;
        let users = this.obj.users.map(user => {
            user.red_dot =
                sizeMess -
                (user.seen_last_message_index == null
                    ? 0
                    : user.seen_last_message_index + 1);
            return user;
        });
        this.obj.users = users;
    }

    setAtributeOtherForUser() {
        let users = this.obj.users.map(user => {
            if (user.is_online == null || user.is_online == undefined)
                user.is_online = true;
            return user;
        });
        this.obj.users = users;
    }

    getEndMoreMessage(id: any = 0, limit: any = 20) {
        let lengthMessage = this.obj.messages.length;
        // init
        if (id == 0) {
            if (lengthMessage > 0) {
                return this.obj.messages.slice(
                    lengthMessage > limit ? lengthMessage - limit : 0,
                    lengthMessage
                );
            }
        }

        let index = 0;
        for (let i = 0; i < this.obj.messages.length; i++) {
            if (id == this.obj.messages[i].id) {
                index = i;
            }
        }
        limit = parseInt(limit);

        if (lengthMessage <= limit) return [];

        if (lengthMessage > 0) {
            return this.obj.messages.slice(
                index > limit ? index - limit : 0,
                index
            );
        }

        return this.obj.messages;
    }

    getNewMessage(id: any = 0, limit: any = 20) {

        let lengthMessage = this.obj.messages.length;
        let index = this.obj.messages.findIndex(ele => id == ele.id);
        if (index == -1) return [];
        limit = parseInt(limit);

        if (lengthMessage <= limit) return [];

        if (lengthMessage > 0 && index < lengthMessage) {
            return this.obj.messages.slice(
                index + 1,
                lengthMessage
            );
        }
        return [];
    }
}
