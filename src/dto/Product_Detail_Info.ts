import { BaseDTO } from "./BaseDTO";

export class ProductDetailInfoDTO extends BaseDTO {

    protected obj;
    protected fillable = ['_id', 'value', 'values', 'product_id', 'category_info_id', 'created_at', 'updated_at'];
    protected fillableDB = ['_id', 'value', 'values', 'product_id', 'category_info_id', 'created_at', 'updated_at'];

    constructor(dto) {
        super();
        this.obj = dto;
    }

    toRequestJSON() {
        throw new Error("Method not implemented.");
    }
    toSimpleJSON = () => {
        return this.toJSON(['_id', 'value', 'values', 'product_id', 'category_info_id']);
    };

}

