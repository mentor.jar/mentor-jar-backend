import { BaseDTO } from "./BaseDTO";

export class DataCityDTO extends BaseDTO {

    protected fillable = ['_id', 'Id', 'Name', 'Districts', 'Wards', 'id_vtp'];
    protected fillableDB = ['_id', 'Id', 'Name', 'Districts', 'Wards', 'id_vtp'];

    protected obj
    constructor(dto) {
        super();
        this.obj = dto
        this.obj.name = this.obj.Name
        this.setDefault('name', this.obj.Name)
        this.setDefault('district', this.obj.District)
        this.setDefault('id_vtp', this.obj.id_vtp)
    }

    toRequestJSON() {
        throw new Error("Method not implemented.");
    }

    toSimpleJSON = () => {
        return this.toJSON(['Id', 'Name', 'id_vtp']);
    }
    toJSONWithDistrict = () => {
        return this.toJSON(['Districts']);
    }
    
    toJSONWithFullDistrict = () => {
        return this.toJSON(['Id', 'Name', 'id_vtp', 'Districts']);
    }

}

