
import { BaseDTO } from "./BaseDTO";
import { ShopDTO } from "./Shop";
import { ProductDTO } from './Product';
import CastHelper from '../utils/castHelper';
import { getMinMax } from "../controllers/serviceHandles/product";
import { UserDTO } from "./UserDTO";

export class BannerDTO extends BaseDTO {

    protected obj;
    protected fillable = [
        '_id', 'name', 'image', 'promo_link', 'is_active', 'images', 'images_web',
        'products', 'start_time', 'shop_id', 'end_time', 'order', 'classify', 'position', 'vouchers', 'advance_actions', 'priority', 'shorten_link', 'description'
    ];
    protected default = {
        'name': '', 'promo_link': '', 'is_active': true,
        'products': null, 'shop_id': null, 'priority': 1
    }
    protected fillableDB = ['_id', 'name', 'description', 'product_id', 'classify', 'position', 'vouchers', 'advance_actions', 'priority', 'shorten_link'];

    protected cast = {
        'start_time': CastHelper.dateToTimestamp,
        'end_time': CastHelper.dateToTimestamp
    }

    constructor(dto) {
        super();
        this.obj = dto
    }

    static newInstance(object) {
        return new BannerDTO(object);
    }

    toSimpleJSON = () => {
        return this.toJSON(['_id', 'name', 'image', 'promo_link', 'is_active', 'images', 'images_web',
            'products', 'start_time', 'shop_id', 'end_time', 'order', 'classify', 'position', 'vouchers', 'advance_actions', 'priority', 'shorten_link', 'description'])
    };

    toRequestJSON = () => {
        let obj: any = this.toJSON(['name', 'description', 'product_id', 'option_values', 'classify', 'position', 'vouchers', 'priority', 'shorten_link'])
        if (this.obj._id) {
            obj._id = this.obj._id;
        }
        return obj;
    }

    completeDataWithVariantJSON = () => {
        let results: any = null;
        const data: any = BannerDTO.newInstance(this.obj).toSimpleJSON();

        let productDTO = data.products.map(e => new ProductDTO(e));
        results = productDTO.map(e => {
            let getProduct = e.toSimpleJSON();
            let getProductOption = e.toJSON(['variants']);
            //Handle variants field
            let variantsDTO = e.getProductVariant(getProductOption.variants);
            getProduct.variants = [];
            variantsDTO.map(e => {
                let varianJson = e.toJSON(['_id', 'option_values', 'before_sale_price', 'sale_price', 'quantity', 'is_master'])
                getProduct.variants.push(varianJson);
            });
            getProduct.price_min_max = getMinMax(getProduct.variants);
            delete getProduct.variants;
            return getProduct;
        });
        data.products = results;
        if (data.shop_id) {
            data.shop_id = new ShopDTO(data.shop_id).toSimpleJSON();
            data.shop_id.user = new UserDTO(data.shop_id.user).toSimpleUserInfo();
        }
        return data;
    }

    completeDataJSON = () => {
        const data: any = BannerDTO.newInstance(this.obj).toSimpleJSON();
        data.products = data.products.map(product => new ProductDTO(product).toSimpleJSON())
        if (data.shop_id) {
            data.shop_id = new ShopDTO(data.shop_id).toSimpleJSON();
            data.shop_id.user = new UserDTO(data.shop_id.user).toSimpleUserInfo();
        }
        return data;
    }
}
