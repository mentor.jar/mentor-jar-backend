import { BaseDTO } from "./BaseDTO";

export class ShippingMethodDTO extends BaseDTO {

    protected obj;
    protected fillableDB = ['_id', 'name', 'country', 'type', 'shipping_fee', 'option', 'created_at', 'updated_at'];


    protected fillable = ['_id', 'name', 'country', 'type', 'shipping_fee', 'option', 'created_at', 'updated_at']

    constructor(dto) {
        super();
        this.obj = dto
    }

    toRequestJSON() {
        throw new Error("Method not implemented.");
    }

    toSimpleJSON = () => {
        return this.toJSON(['_id', 'name', 'country', 'type', 'shipping_fee', 'option', 'created_at', 'updated_at']);
    }
}