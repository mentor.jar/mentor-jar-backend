import { BaseDTO } from "./BaseDTO";

export class FeedbackDTO extends BaseDTO {

    protected obj;
    protected fillableDB = ['_id', 'content', 'vote_star', 'medias', 'target_type', 'target_id', 'is_approved', 'is_public', 'is_show_body_shape', 'shop_feedback', 'buyer_feedback', 'user_id', 'shop_id', 'order_id', 'user_liked', 'created_at', 'updated_at'];


    protected fillable = ['_id', 'content', 'vote_star', 'medias', 'target_type', 'target_id', 'is_approved', 'is_public', 'is_show_body_shape', 'shop_feedback', 'buyer_feedback', 'user_id', 'shop_id', 'order_id', 'user_liked', 'created_at', 'updated_at'];

    protected default = {
        is_show_body_shape: true
    }

    constructor(dto) {
        super();
        this.obj = dto
    }

    toRequestJSON() {
        throw new Error("Method not implemented.");
    }

    toSimpleJSON = () => {
        return this.toJSON(['_id', 'content', 'vote_star', 'medias', 'target_type', 'target_id', 'is_approved', 'is_public', 'is_show_body_shape', 'shop_feedback', 'buyer_feedback', 'user_id', 'shop_id', 'order_id', 'user_liked', 'created_at', 'updated_at']);
    }
}

