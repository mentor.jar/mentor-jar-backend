import { BaseDTO } from "./BaseDTO";

export class PromotionProgramDTO extends BaseDTO {

    protected obj;
    protected fillableDB = ['_id', 'name', 'start_time', 'end_time', 'discount', 'limit_quantity', 'discount', 'shop_id', 'is_valid', 'created_at', 'updated_at'];
    protected fillable = ['_id', 'name', 'start_time', 'end_time', 'discount', 'limit_quantity', 'discount', 'shop_id', 'is_valid', 'is_reset', 'created_at', 'updated_at'];

    static newInstance(object) {
        return new PromotionProgramDTO(object);
    }

    constructor(dto) {
        super();
        this.obj = dto
    }

    toRequestJSON() {
        throw new Error("Method not implemented.");
    }

    toSimpleJSON = () => {
        return this.toJSON(['_id', 'name', 'start_time', 'end_time', 'discount', 'limit_quantity', 'discount', 'shop_id', 'is_valid', 'is_reset', 'created_at', 'updated_at']);
    }
}

