import { BaseDTO } from "./BaseDTO";
import { getMinMax } from "../controllers/serviceHandles/product";
import { ProductDTO } from "./Product";
import { cloneObj } from "../utils/helper";
import { UserDTO } from "./UserDTO";

export class VoucherDTO extends BaseDTO {

    protected obj;
    protected fillableDB = ['_id', 'name', 'code', 'target', 'user', 'start_time', 'end_time', 'save_quantity', 'use_quantity', 'available_quantity', 
    'type', 'value', 'discount_by_range_price', 'max_discount_value', 'display_mode', 'min_order_value', 'shop_id', 'products', 'shops', 'kol_user', 
    'conditions', 'approve_status', 'classify', 'is_public', 'max_budget', 'used_by', 'used_budget', 'created_at', 'updated_at', 'deleted_at', 'banner'];


    protected fillable = ['_id', 'name', 'code', 'target', 'user', 'start_time', 'end_time', 'save_quantity', 'use_quantity', 'available_quantity', 
    'type', 'value', 'discount_by_range_price', 'max_discount_value', 'display_mode', 'min_order_value', 'shop_id', 'products', 'shops', 'kol_user', 
    'conditions', 'approve_status', 'classify', 'is_public', 'max_budget', 'used', 'used_budget', 'used_by', 'created_at', 'updated_at', 'deleted_at', 'shop', 'banner'];

    static newInstance(object) {
        return new VoucherDTO(object);
    }

    constructor(dto) {
        super();
        this.obj = dto
    }

    toRequestJSON() {
        throw new Error("Method not implemented.");
    }

    toSimpleJSON = () => {
        return this.toJSON(['_id', 'name', 'code', 'target', 'user', 'start_time', 'end_time', 'save_quantity', 'use_quantity', 'available_quantity', 
        'type', 'value', 'discount_by_range_price', 'max_discount_value', 'display_mode', 'min_order_value', 'shop_id', 'products', 'shops', 'kol_user', 
        'conditions', 'approve_status', 'classify', 'is_public', 'max_budget', 'used_budget', 'created_at', 'updated_at', 'deleted_at', 'shop', 'banner']);
    }

    convertVoucher = () => {
        let voucher = cloneObj(this.obj)
        voucher.product_ids = voucher.products || []
        voucher.shop_ids = voucher.shops || []
        delete voucher.products
        delete voucher.shops
        voucher.used = voucher.use_quantity - voucher.available_quantity
        if (voucher.shop)
            voucher.shop.user = new UserDTO(voucher.shop.user).toSimpleJSON();
        return voucher
    }

    completeDataWithVariantJSON = () => {
        let results: any = null;
        const data: any = VoucherDTO.newInstance(this.obj).toSimpleJSON();
        let productDTO = data.products.map(product => new ProductDTO(product));
        results = productDTO.map(product => {
            let simpleProduct = product.toJSON(['_id', 'name', 'images', 'before_sale_price', 'sale_price', 'shop_id']);
            let productOption = product.toJSON(['variants']);
            // Don't return variants for now
            // let variantsDTO = product.getProductVariant(productOption.variants);
            // simpleProduct.variants = [];
            // variantsDTO.map(e => {
            //     let varianJson = e.toJSON(['_id', 'before_sale_price', 'sale_price', 'quantity', 'is_master'])
            //     simpleProduct.variants.push(varianJson);
            // });
            simpleProduct.price_min_max = getMinMax(productOption.variants);
            return simpleProduct;
        });
        data.products = results;
        if (data.shop)
            data.shop.user = new UserDTO(data.shop.user).toSimpleJSON();
        return data;
    }

    toVoucherSystemCacheJSON = () => {
        return this.toJSON(['_id', 'code', 'target', 'start_time', 'end_time', 'use_quantity', 
        'available_quantity', 'type', 'value', 'discount_by_range_price', 
        'max_discount_value', 'min_order_value', 'max_order_value', 'conditions', 'classify', 'max_budget']);
    }
}

