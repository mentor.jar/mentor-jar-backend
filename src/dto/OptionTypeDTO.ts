import { BaseDTO } from "./BaseDTO";
import { OptionValueDTO } from "./OptionValueDTO";

export class OptionTypeDTO extends BaseDTO {

    protected obj;
    protected fillable = ['_id', 'name', 'description', 'product_id', 'option_values'];
    protected default = { name: '', description: '', product_id: null, option_values: [] }
    protected fillableDB = ['_id', 'name', 'description', 'product_id'];

    constructor(dto) {
        super();
        this.obj = dto
    }

    toSimpleJSON = () => {
        return this.toJSON(['_id', 'name', 'description', 'product_id'])
    };

    toRequestJSON = () => {
        let obj: any = this.toJSON(['name', 'description', 'product_id', 'option_values'])
        if (this.obj._id) {
            obj._id = this.obj._id;
        }
        return obj;
    }

    getOptionValue = (option_values: Array<any>) => {
        return option_values.map(e => {
            e.option_type_id = this.obj._id
            return new OptionValueDTO(e)
        });
    }

}

