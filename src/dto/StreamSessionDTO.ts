import keys from "../config/env/keys";
import { getMinMax } from "../controllers/serviceHandles/product";
import { FollowRepo } from "../repositories/FollowRepo";
import { randomInt } from "../utils/helper";
import { addLink } from "../utils/stringUtil";
import { BaseDTO } from "./BaseDTO";
import { ProductDTO } from "./Product";
import { UserDTO } from "./UserDTO";

export class StreamSessionDTO extends BaseDTO {

    protected obj;
    protected fillableDB = ['_id', 'image', 'pin_message', 'is_approved', 'status', 'list_id_product', 'view', 'view_max', 'heart', 'time_start', 'time_end', 'active', 'archive_links', 'session_number', 'stream_id', 'shop_id', 'room_chat_id', 'name', 'type', 'time_will_start', 'type_of_viewer', 'stream', 'shop', 'user', 'created_at', 'updated_at'];

    protected fillable = ['_id', 'pin_message', 'image', 'is_approved', 'status', 'list_id_product', 'products', 'view', 'view_max', 'heart', 'time_start', 'time_end', 'active', 'archive_links', 'session_number', 'stream_id', 'shop_id', 'room_chat_id', 'name', 'type', 'time_will_start', 'type_of_viewer', 'stream', 'shop', 'user', 'created_at', 'updated_at'];

    static newInstance(object) {
        return new StreamSessionDTO(object);
    }
    constructor(dto) {
        super();
        this.obj = dto
    }

    toRequestJSON() {
        throw new Error("Method not implemented.");
    }

    toSimpleJSON = () => {
        if (!this.obj.view && !this.obj.active) this.obj.view = randomInt(2, 10);
        return this.toJSON(['_id', 'image', 'pin_message', 'is_approved', 'status', 'list_id_product', 'products', 'view_max', 'view', 'heart', 'time_start', 'time_end', 'active', 'archive_links', 'session_number', 'stream_id', 'shop_id', 'room_chat_id', 'name', 'type', 'time_will_start', 'type_of_viewer', 'stream', 'shop', 'user', 'created_at', 'updated_at']);
    }

    toUserTrendJSON = () => {
        return this.toJSON(['_id', 'image', 'is_approved', 'status', 'list_id_product', 'products', 'view', 'view_max', 'heart', 'time_start', 'time_end', 'active', 'archive_links', 'session_number', 'stream_id', 'shop_id', 'room_chat_id', 'name', 'type', 'time_will_start', 'type_of_viewer', 'created_at', 'updated_at']);
    }

    completeDataWithDTO(user: any = null) {
        let results: any = null;
        const data: any = StreamSessionDTO.newInstance(this.obj).toSimpleJSON();
        let productDTO = data.products.map(e => new ProductDTO(e));
        results = productDTO.map(e => {
            e.setUser(user);
            let getProduct = e.toJSONWithBookMark();
            let getProductOption = e.toJSON(['variants']);
            //Handle variants field
            let variantsDTO = e.getProductVariant(getProductOption.variants);
            getProduct.variants = [];
            variantsDTO.map(e => {
                let varianJson = e.toJSON(['_id', 'option_values', 'before_sale_price', 'sale_price', 'quantity', 'is_master'])
                getProduct.variants.push(varianJson);
            });
            getProduct.price_min_max = getMinMax(getProduct.variants);
            //delete getProduct.variants;
            return getProduct;
        });
        data.products = results;
        data.user.avatar = addLink(`${keys.host_community}/`, data.user.gallery_image.url)
        data.user = new UserDTO(data.user).toSimpleUserInfo();
        return data;
    }

    async completeDataStreamDetail(req) {
        let results: any = null;
        const data: any = StreamSessionDTO.newInstance(this.obj).toSimpleJSON();
        let productDTO = data.products.map(e => new ProductDTO(e));
        results = productDTO.map(e => {
            e.setUser(req.user);
            let getProduct = e.toJSONWithBookMark();
            let getProductOption = e.toJSON(['variants']);
            //Handle variants field
            let variantsDTO = e.getProductVariant(getProductOption.variants);
            getProduct.variants = [];
            variantsDTO.map(e => {
                let varianJson = e.toJSON(['_id', 'option_values', 'before_sale_price', 'sale_price', 'quantity', 'is_master'])
                getProduct.variants.push(varianJson);
            });
            getProduct.price_min_max = getMinMax(getProduct.variants);
            //delete getProduct.variants;
            return getProduct;
        });
        data.products = results;
        data.user.avatar = addLink(`${keys.host_community}/`, data.user.gallery_image.url)
        data.user.is_follow = req.user ? await FollowRepo.getInstance().checkFollow(req.user._id, data.user._id) : false;
        data.user = new UserDTO(data.user).toStreamDetailJSON();
        return data;
    }

}

