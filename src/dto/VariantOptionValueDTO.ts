import { BaseDTO } from "./BaseDTO";
import { OptionValueDTO } from "./OptionValueDTO";

export class VariantOptionValueDTO extends BaseDTO {

    protected obj;
    protected fillable = ['_id', 'option_value_id', 'variant_id'];
    protected fillableDB = ['_id', 'option_value_id', 'variant_id'];
    constructor(dto) {
        super();
        this.obj = dto
        this.setDefault('before_sale_price', this.obj.price)
        this.setDefault('sale_price', this.obj.price)
    }
    protected default = {
        sku: Date.now(),
        product_id: null,
        name: ""
    }

    toSimpleJSON = () => {
        return this.toJSON(['_id', 'option_value_id', 'variant_id']);
    };
    toRequestJSON = () => {
        let obj: any = this.toJSON(['_id', 'option_value_id', 'variant_id']);
        if (this.obj._id) {
            obj._id = this.obj._id
        }
        return obj;
    }

}

