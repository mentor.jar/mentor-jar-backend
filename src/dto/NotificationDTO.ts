import { BaseDTO } from "./BaseDTO";

export class NotificationDTO extends BaseDTO {

    protected obj;
    protected fillableDB = ['_id', 'type', 'targetedBy', 'createdBy', 'content', 'contentType', 'isRead', 'context'];

    protected cast = {
        content: (content) => content != undefined && content != null
            ? content : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry'
    }

    protected fillable = ['_id', 'type', 'targetedBy', 'createdBy', 'content', 'contentType', 'isRead', 'context'];

    static parse(object) {
        return new NotificationDTO(object).toSimpleJSON();
    }

    constructor(dto) {
        super();
        this.obj = dto
    }

    toRequestJSON() {
        throw new Error("Method not implemented.");
    }

    toSimpleJSON = () => {
        return this.toJSON(['_id', 'type', 'targetedBy', 'createdBy', 'content', 'contentType', 'isRead', 'context']);
    }
}

