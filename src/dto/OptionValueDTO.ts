import { BaseDTO } from "./BaseDTO";

export class OptionValueDTO extends BaseDTO {

    protected obj;
    protected fillable = ['_id', 'name', 'image', 'option_type_id'];
    protected fillableDB = ['_id', 'name', 'image', 'option_type_id'];

    constructor(dto) {
        super();
        this.obj = dto
    }
    protected default = { image: null, option_type_id: null, name: "" }

    toSimpleJSON = () => {
        return this.toJSON([]);
    };

    toRequestJSON = () => {
        let obj: any = this.toJSON(['_id', 'name', 'image', 'option_type_id']);
        if (this.obj._id) {
            obj._id = this.obj._id
        }
        return obj;
    }

}

