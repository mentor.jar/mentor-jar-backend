import keys from "../config/env/keys";
import { OrderShippingRepo } from "../repositories/OrderShippingRepo";
import { PaymentRepo } from "../repositories/PaymentRepo";
import { cloneObj, ObjectId } from "../utils/helper";
import { addLink } from "../utils/stringUtil";
import { BaseDTO } from "./BaseDTO";
import { UserDTO } from "./UserDTO";

export class OrderDTO extends BaseDTO {

    protected obj;
    protected fillableDB = ['_id', 'order_number', 'total_price', 'payment_status', 'shipping_status',
        'payment_method_id', 'shipping_method_id', 'user_id', 'handle_by', 'shop_id', 'vouchers', 'address',
        'pick_address', 'total_value_items', 'shipping_fee', 'shipping_discount', 'voucher_discount', 'note',
        'group_order_id', 'pick_status', 'refund_information', 'created_at', 'updated_at', 'deleted_at', 
        'is_mark_penalty', 'order_service', 'group_orders', 'payment_info', 'is_group_buy_order'];


    protected fillable = ['_id', 'order_number', 'total_price', 'payment_status', 'shipping_status',
        'payment_method_id', 'shipping_method_id', 'payment_method', 'shipping_method', 'user_id', 'handle_by',
        'shop_id', 'vouchers', 'address', 'pick_address', 'total_value_items', 'shipping_fee', 'shipping_discount',
        'voucher_discount', 'note', 'group_order_id', 'created_at', 'updated_at', 'deleted_at', 'order_items',
        'user', 'shipping_detail', 'shipping_method', 'payment_method', 'shop', 'cancel_reason', 'cancel_type',
        'cancel_by', 'cancel_time', 'seller_note', 'is_feedbacked_user', 'pick_status', 'refund_information', 
        'is_mark_penalty', 'order_service', 'group_orders', 'payment_info', 'is_group_buy_order'];

    static newInstance(object) {
        return new OrderDTO(object);
    }

    constructor(dto) {
        super();
        this.obj = dto
    }
    private orderShippingRepo = OrderShippingRepo.getInstance()
    private paymentRepo = PaymentRepo.getInstance()

    toRequestJSON() {
        throw new Error("Method not implemented.");
    }

    toSimpleJSON = () => {
        return this.toJSON(['_id', 'order_number', 'total_price', 'payment_status', 'shipping_status',
            'payment_method_id', 'shipping_method_id', 'payment_method', 'shipping_method', 'user_id',
            'handle_by', 'shop_id', 'vouchers', 'address', 'pick_address', 'total_value_items', 'shipping_fee',
            'shipping_discount', 'voucher_discount', 'note', 'group_order_id', 'created_at', 'updated_at',
            'deleted_at', 'order_items', 'user', 'shipping_method', 'payment_method', 'shop', 'cancel_reason',
            'cancel_type', 'cancel_by', 'cancel_time', 'seller_note', 'is_feedbacked_user', 'pick_status',
            'refund_information', 'is_mark_penalty', 'order_service', 'group_orders', 'is_group_buy_order']);
    }

    toSimpleJSONForBuyer = () => {
        return this.toJSON(['_id', 'order_number', 'total_price', 'payment_status', 'shipping_status',
            'payment_method_id', 'shipping_method_id', 'user_id', 'handle_by', 'shop_id', 'vouchers',
            'address', 'pick_address', 'total_value_items', 'shipping_fee', 'shipping_discount',
            'voucher_discount', 'note', 'group_order_id', 'created_at', 'updated_at', 'deleted_at',
            'order_items', 'user', 'shipping_detail', 'shipping_method', 'payment_method', 'shop',
            'cancel_reason', 'cancel_type', 'cancel_by', 'cancel_time', 'seller_note', 'is_feedbacked_user', 
            'pick_status', 'refund_information', 'is_mark_penalty', 'order_service', 'group_orders', 'is_group_buy_order']);
    }

    toSimpleJSONForAdmin = () => {
        return this.toJSON(['_id', 'order_number', 'total_price', 'payment_status', 'shipping_status',
            'payment_method_id', 'shipping_method_id', 'user_id', 'handle_by', 'shop_id', 'total_value_items', 
            'shipping_fee', 'shipping_discount', 'voucher_discount', 'note', 'group_order_id', 'created_at', 
            'updated_at', 'deleted_at', 'order_items', 'shipping_method', 'cancel_reason', 'cancel_type', 
            'cancel_by', 'cancel_time', 'seller_note', 'pick_status', 'is_mark_penalty', 'order_service', 'is_group_buy_order']);
    }

    toDetailJSON = () => {
        return this.toJSON(['_id', 'order_number', 'payment_status', 'shipping_status', 'vouchers', 'total_price',
            'payment_method_id', 'shipping_method_id', 'user_id', 'handle_by', 'shop_id', 'address', 'pick_address',
            'total_value_items', 'shipping_fee', 'shipping_discount', 'voucher_discount', 'note', 'group_order_id',
            'created_at', 'updated_at', 'user', 'payment_method', 'shipping_method', 'order_items', 'payment_info',
            'total_price_customer_pay', 'shipping_method', 'payment_method', 'shop', 'cancel_reason', 'cancel_type',
            'cancel_by', 'cancel_time', 'seller_note', 'is_feedbacked_user', 'pick_status', 'refund_information', 
            'is_mark_penalty', 'order_service', 'is_group_buy_order']);
    }

    completeDataInNotification = async () => {
        const order: any = OrderDTO.newInstance(this.obj).toSimpleJSON();
        order.image = order.order_items[0]?.images[0];
        delete order.order_items;
        return order;
    }

    completeDataPaymentAndShippingInfo = async () => {
        const order: any = OrderDTO.newInstance(this.obj).toSimpleJSON();

        const paymentOrder: any = await this.paymentRepo.findOne({ order_id: ObjectId(order._id) })

        const transaction_fee = 0;
        let payment_info: any = {};
        payment_info.total_price_product = order.total_value_items;
        payment_info.shipping_discount = order.shipping_discount;
        payment_info.shipping_fee = order.shipping_fee;
        payment_info.transaction_fee = transaction_fee;
        payment_info.total_voucher_discount = order.voucher_discount;
        payment_info.total_price = order.total_price;
        payment_info.paid_time = paymentOrder ? paymentOrder.created_at : null

        let order_shipping: any = await this.orderShippingRepo.findOne({ order_id: ObjectId(order._id) })
        order_shipping = cloneObj(order_shipping);

        order_shipping.history.sort((a, b) => {
            a = new Date(a.action_time);
            b = new Date(b.action_time);
            return b - a;
        });


        order.payment_info = payment_info;
        order.order_shipping = order_shipping;

        // order.return_order_info = null;

        // split vouchers to shop_vouchers & system_vouchers
        const system_vouchers = order.vouchers.filter(voucher => voucher.classify)
        const shop_vouchers = order.vouchers.filter(voucher => !voucher.classify)
        order.system_vouchers = system_vouchers
        order.shop_vouchers = shop_vouchers
        delete order.vouchers

        order.total_price_customer_pay = order.total_price + transaction_fee;

        order.payment_method = order.payment_method?.[0] ?? null;
        order.shipping_method = order.shipping_method?.[0] ?? null;

        // refund_status
        if(order.refund_information && order.refund_information.length) {
            order.refund_status = order.refund_information.filter(item => item.status === 'opening').length ? "processing" : "completed"
        }
        else {
            order.refund_status = "none"
        }

        order.user.avatar = addLink(`${keys.host_community}/`, order.user.gallery_image.url)
        order.user = new UserDTO(order.user).toSimpleUserInfo();

        order.shop.user.avatar = addLink(`${keys.host_community}/`, order.shop.user.gallery_image.url)
        order.shop.user = new UserDTO(order.shop.user).toSimpleUserInfo();
        return order;
    }

    orderBelongGroupOrder = () => {
        return this.toJSON(['_id', 'order_number', 'total_price', 'total_price_customer_pay', 'total_value_items', 'shipping_fee', 
        'shipping_discount', 'voucher_discount', 'order_items', 'user', 'shop', 'payment_info', 'refund_status', 'is_group_buy_order' ])
    }
}

