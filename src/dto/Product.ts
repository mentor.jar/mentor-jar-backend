import { BaseDTO } from "./BaseDTO";
import { ProductDetailInfoDTO } from "./Product_Detail_Info";
import { VariantDTO } from "./VariantDTO";
import { getMinMax } from '../controllers/serviceHandles/product';
import { ShopDTO } from "./Shop";
import { addLink } from "../utils/stringUtil";
import keys from "../config/env/keys";
import { formatDiscountPercentOrPriceProduct } from "../utils/helper";

export class ProductDTO extends BaseDTO {

    protected obj;
    protected fillableDB = ['_id', 'name', 'description', 'short_description', 'friendly_url',
        'weight', 'width', 'height', 'length', 'before_sale_price', 'sale_price', 'quantity',
        'deleted_at', 'allow_to_sell', 'is_approved', 'is_pre_order', 'shop_id', 'category_id',
        'images', 'list_category_id', 'detail_size', 'limit_sale_price_order', 'sale_price_order_available', 
        'shipping_information', 'delivery_instruction', 'exchange_information', 'delivery_information', 'allow_refund', 'duration_refund',
        'description_images', 'refund_conditions', 'sold', 'shorten_link', 'custom_images', 'popular_mark', 'is_guaranteed_item', 'is_genuine_item', 
        'top_photo_display_full_mode', 'created_at', 'updated_at', 'update_price_remaining', 'next_date_update_price', 'price_updated_dates', 'authen_images', 'is_sold_out',
        'promotion', 'stocking', 'flash_sale', 'is_suggested', 'group_buy', 'order_rooms', 'bidu_air'
    ];
    protected fillable = ['_id', 'name', 'description', 'short_description',
        'friendly_url', 'weight', 'width', 'height', 'length', 'before_sale_price',
        'sale_price', 'quantity', 'deleted_at', 'allow_to_sell', 'is_approved',
        'is_pre_order', 'shop_id', 'category_id', 'images', 'list_category_id',
        'created_at', 'updated_at', 'product_detail_infos', 'variants', 'detail_size', 'category', 
        'shop', 'order_items', 'limit_sale_price_order', 'sale_price_order_available', 'discount_percent', 
        'shipping_information', 'delivery_instruction', 'exchange_information', 'delivery_information', 'allow_refund', 'duration_refund',
        'description_images', 'refund_conditions', 'sold', 'shorten_link', 'custom_images', 'popular_mark', 'is_guaranteed_item', 'is_genuine_item', 'authen_images',
        'top_photo_display_full_mode', 'createdAt', 'updatedAt', 'is_bookmarked', 'option_types', 'order_items', 'update_price_remaining', 'next_date_update_price', 'price_updated_dates', 'is_sold_out',
        'promotion', 'stocking', 'price_min_max', 'flash_sale', 'is_suggested', 'variant', 'group_buy', 'order_rooms', 'minimum_member_group', 'bidu_air'
    ];

    static fillableList = [
        '_id', 'name', 'description', 'short_description',
        'friendly_url', 'before_sale_price',
        'sale_price', 'quantity', 'allow_to_sell', 'is_approved',
        'is_pre_order', 'shop_id', 'category_id', 'images', 'list_category_id',
        'variants', 'detail_size', 'category', 'order_items', 'shop', 'discount_percent',
        'limit_sale_price_order', 'sale_price_order_available', 'shipping_information',
        'delivery_instruction', 'exchange_information', 'delivery_information', 'allow_refund',
        'duration_refund', 'description_images', 'refund_conditions', 'sold', 'shorten_link', 
        'custom_images', 'popular_mark', 'is_guaranteed_item', 'is_genuine_item', 'deleted_at', 'createdAt',
        'top_photo_display_full_mode', 'update_price_remaining', 'next_date_update_price', 'price_updated_dates', 'authen_images', 'is_sold_out',
        'promotion', 'stocking', 'price_min_max', 'flash_sale', 'is_suggested', 'group_buy', 'order_rooms', 'minimum_member_group', 'bidu_air'
    ]
    static default = { list_category_id: [], images: [], sold: 0 }
    constructor(dto) {
        super();
        this.obj = dto
    }

    static newInstance(object) {
        return new ProductDTO(object);
    }

    toRequestJSON() {
        throw new Error("Method not implemented.");
    }

    toSimpleJSON = () => {
        this.setDefault("sold", 0)
        return this.toJSON([
            '_id', 'name', 'description', 'short_description', 'friendly_url',
            'weight', 'width', 'height', 'length', 'before_sale_price', 'sale_price', 'quantity',
            'deleted_at', 'allow_to_sell', 'is_approved', 'is_pre_order', 'shop_id', 'category_id',
            'images', 'list_category_id', 'category', 'shop', 'order_items', 'createdAt', 'updatedAt', 
            'variants', 'detail_size', 'category', 'option_types', 'limit_sale_price_order', 'sale_price_order_available',
            'shipping_information', 'delivery_instruction', 'exchange_information', 'delivery_information', 'allow_refund', 'duration_refund', 
            'description_images', 'refund_conditions', 'sold', 'shorten_link', 'discount_percent','custom_images', 'popular_mark', 
            'is_guaranteed_item', 'is_genuine_item', 'top_photo_display_full_mode', 'update_price_remaining', 'next_date_update_price', 
            'price_updated_dates', 'authen_images', 'is_sold_out', 'price_min_max', 'flash_sale', 'group_buy', 'order_rooms', 'minimum_member_group',
            'bidu_air'
        ]);
    };

    toJSONWithBookMark = () => {
        this.setDefault("sold", 0)
        this.setDefault('is_bookmarked', false);
        // if (this.checkBookmarkedProduct()) {
        // } else {
        //     this.setDefault('is_bookmarked', false);
        // }
        return this.toJSON([
            '_id', 'name', 'description', 'short_description', 'friendly_url',
            'weight', 'width', 'height', 'length', 'before_sale_price', 'sale_price', 'quantity',
            'deleted_at', 'allow_to_sell', 'is_approved', 'is_pre_order', 'shop_id', 'category_id',
            'images', 'list_category_id', 'category', 'shop', 'order_items', 'createdAt', 'updatedAt', 
            'variants', 'detail_size', 'category', 'is_bookmarked', 'option_types', 'limit_sale_price_order', 'sale_price_order_available',
            'shipping_information', 'delivery_instruction', 'exchange_information', 'delivery_information', 'allow_refund', 
            'duration_refund', 'description_images', 'refund_conditions', 'sold', 'shorten_link', 'discount_percent', 'custom_images', 'popular_mark', 
            'is_guaranteed_item', 'is_genuine_item', 'top_photo_display_full_mode', 'update_price_remaining', 'next_date_update_price', 'price_updated_dates', 
            'authen_images', 'is_sold_out', 'group_buy', 'order_rooms', 'minimum_member_group', 'bidu_air'
        ]);
    }

    toJSONForCache = () => {
        this.setDefault("sold", 0)
        this.setDefault('is_bookmarked', false);

        return this.toJSON([
            '_id', 'name', 'description', 'short_description', 'friendly_url',
            'weight', 'width', 'height', 'length', 'before_sale_price', 'sale_price', 'quantity',
            'deleted_at', 'allow_to_sell', 'is_approved', 'is_pre_order', 'shop_id', 'category_id',
            'images', 'list_category_id', 'category', 'shop', 'order_items', 'createdAt', 'updatedAt', 
            'variants', 'detail_size', 'category', 'is_bookmarked', 'option_types', 'limit_sale_price_order', 'sale_price_order_available',
            'shipping_information', 'delivery_instruction', 'exchange_information', 'delivery_information', 'allow_refund', 
            'duration_refund', 'description_images', 'refund_conditions', 'sold', 'shorten_link', 'discount_percent', 'custom_images', 'popular_mark', 
            'is_guaranteed_item', 'is_genuine_item', 'top_photo_display_full_mode', 'update_price_remaining', 'next_date_update_price', 'price_updated_dates', 
            'authen_images', 'is_sold_out', 'group_buy', 'order_rooms', 'minimum_member_group', 'bidu_air'
        ]);
    }

    toJSONForELS = () => {
        this.setDefault("sold", 0)
        this.setDefault('is_bookmarked', false);

        let product: any = this.toJSON([
            '_id', 'name', 'description', 'short_description', 'friendly_url', 'weight', 'width', 'height', 'length', 
            'before_sale_price', 'sale_price', 'quantity', 'deleted_at', 'allow_to_sell', 'is_approved', 'is_pre_order', 
            'shop_id', 'category_id', 'images', 'list_category_id', 'category', 'shop', 'order_items', 'createdAt', 
            'updatedAt', 'variants', 'detail_size', 'category', 'is_bookmarked', 'option_types', 'limit_sale_price_order', 
            'sale_price_order_available', 'shipping_information', 'delivery_instruction', 'exchange_information', 
            'delivery_information', 'allow_refund', 'duration_refund', 'description_images', 'refund_conditions', 'sold', 
            'shorten_link', 'discount_percent', 'custom_images', 'popular_mark', 'is_guaranteed_item', 'is_genuine_item', 
            'top_photo_display_full_mode', 'update_price_remaining', 'next_date_update_price', 'price_updated_dates', 
            'authen_images', 'is_sold_out', 'promotion', 'stocking', 'flash_sale', 'is_suggested', 'group_buy', 'order_rooms',
            'minimum_member_group', 'bidu_air'
        ]);

        product.description = product.description.replace(/\<img/g, "<img max-width='100%'")

        product.price_min_max = getMinMax(product?.variants);

        let user = product?.shop?.user;
        user.avatar = addLink(`${keys.host_community}/`, user?.gallery_image?.url)
        delete user?.password;
        product.shop.user = user;

        return product;
    }

    toJSONCheckoutItem = () => {
        const productItem = this.toJSON(['_id', 'name', 'images', 'before_sale_price', 'sale_price', 'quantity', 'shop_id', 
        'is_sold_out', 'variant_id', 'variant', 'group_buy', 'order_rooms', 'minimum_member_group'])
        return productItem
    }

    toJsonFlashSaleWithBookMark = () => {
        this.setDefault("sold", 0)
        if (this.checkBookmarkedProduct()) {
            this.setDefault('is_bookmarked', true);
        } else {
            this.setDefault('is_bookmarked', false);
        }
        let result: any = this.toJSON([
            '_id', 'name', 'friendly_url',
            'before_sale_price', 'sale_price', 'quantity',
            'images', 'variants', 'is_bookmarked', 'sold', 'shorten_link', 'custom_images', 'popular_mark', 
            'is_sold_out', 'flash_sale'
        ]);

        if (result.description) {
            result.description = result.description.replace(/<img/g, "<img width=\"100%\"").replace(/figure/g, 'div')
        }
        return result;
    }

    getProductDetailInfo = (product_detail_info: Array<any>) => {
        return product_detail_info.map(e => {
            e.product_id = this.obj._id
            return new ProductDetailInfoDTO(e)
        });
    }

    getProductVariant = (product_variants: Array<any>) => {
        return product_variants.map(e => {
            e.product_id = this.obj._id
            return new VariantDTO(e)
        });
    }

    checkBookmarkedProduct() {
        if (this.user) {
            if (this.user.favorite_products.includes(this.obj._id.toString())) {
                return 1;
            }
        }
        return 0;
    }

    getProductComplete(user, hideSoldLessThanTen = false) {
        //this.setUser(user);
        let getProduct: any = this.toJSONWithBookMark();
        let getProductOption: any = this.toJSON(['variants']);
        //Handle variants field
        let variantsDTO = this.getProductVariant(getProductOption.variants);
        getProduct.variants = [];
        variantsDTO.map(e => {
            let varianJson = e.toJSON(['_id', 'option_values', 'before_sale_price', 'sale_price', 'quantity', 'is_master', 'is_sold_out'])
            getProduct.variants.push(varianJson);
        });
        getProduct.price_min_max = getMinMax(getProduct.variants);
        delete getProduct.variants;
        if (hideSoldLessThanTen) {
            getProduct.sold = getProduct?.sold < 10 ? 0 : getProduct?.sold;
        }
        return getProduct;
    }

    compressAtHome() {
        this.setDefault("sold", 0);
        this.setDefault('is_bookmarked', false);
        let product:any = this.toJSON([
            '_id', 'name', 'images', 'is_guaranteed_item', 'is_genuine_item', 'is_bookmarked', 'before_sale_price',
            'sale_price', 'createdAt', 'sold', 'quantity', 'shop_id', 'discount_percent', 'group_buy', 'order_rooms', 'minimum_member_group',
            'bidu_air'
        ])

        let getProductOption: any = this.toJSON(['variants']);
        let variantsDTO = this.getProductVariant(getProductOption.variants);
        let variants = [];
        variantsDTO.map(e => {
            let varianJson = e.toJSON(['_id', 'option_values', 'before_sale_price', 'sale_price', 'quantity', 'is_master'])
            variants.push(varianJson);
        });
        product.price_min_max = getMinMax(variants);

        const getObjectShop:any  = this.toJSON(['shop']);
        product.shop = new ShopDTO(getObjectShop.shop).toJSON(['_id', 'country']);
        product.sold = product?.sold < 10 ? 0 : product.sold;

        return product;
    }

    toTopShopProductItem() {
        this.setDefault("sold", 0);
        this.setDefault('is_bookmarked', false);
        let product:any = this.toJSON([
            '_id', 'name', 'images', 'is_guaranteed_item', 'is_genuine_item', 'is_bookmarked', 'before_sale_price',
            'sale_price', 'createdAt', 'sold', 'quantity', 'shop_id', 'group_buy', 'order_rooms', 'minimum_member_group',
            'bidu_air'
        ])

        let getProductOption: any = this.toJSON(['variants']);
        let variantsDTO = this.getProductVariant(getProductOption.variants);
        let variants = [];
        variantsDTO.map(e => {
            let varianJson = e.toJSON(['_id', 'option_values', 'before_sale_price', 'sale_price', 'quantity', 'is_master'])
            variants.push(varianJson);
        });
        product.price_min_max = getMinMax(variants);

        const getObjectShop:any  = this.toJSON(['shop']);
        product.shop = new ShopDTO(getObjectShop.shop).toJSON(['_id', 'country']);
        product.sold = product?.sold < 10 ? 0 : product.sold;

        return product;
    }

    toSimpleProductItem() {
        return this.toJSON(['_id', 'name', 'images', 'quantity']);
    }
    
    toSimpleSoldProductItem() {
        return this.toJSON(['_id', 'name', 'images', 'sold'])
    }

    toSimpleGroupBuyProductItem() {
        return this.toJSON([
            '_id', 'name', 'images', 'is_guaranteed_item', 'is_genuine_item', 'is_bookmarked', 'before_sale_price',
            'sale_price', 'createdAt', 'sold', 'quantity', 'shop_id', 'group_buy', 'minimum_member_group', 'bidu_air'
        ])
    }

}

