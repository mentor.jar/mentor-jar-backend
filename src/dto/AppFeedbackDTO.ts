import { BaseDTO } from "./BaseDTO";

export class AppFeedback extends BaseDTO {

    protected obj;
    protected fillableDB = ["_id", "full_name", "email", "how_you_know_bidu", "satisfied_with_bidu_mark", "ui_mark", "shop_config_mark", "product_manage_mark", "livestream_mark", "order_manage_mark", "opinion", "user_id", "created_at", "updated_at"];


    protected fillable = ["_id", "full_name", "email", "how_you_know_bidu", "satisfied_with_bidu_mark", "ui_mark", "shop_config_mark", "product_manage_mark", "livestream_mark", "order_manage_mark", "opinion", "user_id", "created_at", "updated_at"];

    constructor(dto) {
        super();
        this.obj = dto
    }

    toRequestJSON() {
        throw new Error("Method not implemented.");
    }

    toSimpleJSON = () => {
        return this.toJSON(["_id", "full_name", "email", "how_you_know_bidu", "satisfied_with_bidu_mark", "ui_mark", "shop_config_mark", "product_manage_mark", "livestream_mark", "order_manage_mark", "opinion", "user_id", "created_at", "updated_at"]);
    }
}
