import { BaseDTO } from "./BaseDTO";
import { OptionValueDTO } from "./OptionValueDTO";

export class VariantDTO extends BaseDTO {

    protected obj;
    protected fillable = ['_id', 'sku', 'quantity', 'before_sale_price', 'sale_price', 'price', 'is_master', 'product_id', 'option_values'];
    protected fillableDB = ['_id', 'sku', 'quantity', 'option_values', 'before_sale_price', 'sale_price', 'is_master', 'product_id'];
    constructor(dto) {
        super();
        this.obj = dto
        this.setDefault('before_sale_price', this.obj.price)
        this.setDefault('sale_price', this.obj.price)
    }
    protected default = {
        sku: Date.now(),
        product_id: null,
        name: "",
    }

    toSimpleJSON = () => {
        return this.toJSON(['_id', 'sku', 'quantity', 'before_sale_price', 'sale_price', 'is_master', 'product_id']);
    };

    // 
    toRequestJSON = () => {
        let obj: any = this.toJSON(['_id', 'name', 'image', 'price', 'option_values', 'is_master', 'option_type_id']);
        if (this.obj._id) {
            obj._id = this.obj._id
        }
        return obj;
    }

    getOptionValue = (option_values: Array<any>) => {
        return option_values.map(e => {
            e.option_type_id = this.obj._id
            return new OptionValueDTO(e)
        });
    }

}

