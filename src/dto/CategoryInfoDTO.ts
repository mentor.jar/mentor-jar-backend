import { BaseDTO } from "./BaseDTO";

export class CategoryInfoDTO extends BaseDTO {

    protected fillable = ['_id', 'name', 'type', 'fashion_type', 'is_required', 'category_id', 'list_option', 'shop_id', 'is_allow_multiple_values', 'created_at', 'updated_at'];
    protected fillableDB = ['_id', 'name', 'type', 'fashion_type', 'list_option', 'is_required', 'category_id', 'shop_id', 'is_allow_multiple_values'];

    protected obj
    constructor(dto) {
        super();
        this.obj = dto
    }

    toRequestJSON() {
        throw new Error("Method not implemented.");
    }

    toSimpleJSON = () => {
        this.setDefault('is_allow_multiple_values', false)
        return this.toJSON(['_id', 'name', 'list_option', 'is_required', 'type', 'fashion_type', 'category_id', 'shop_id', 'is_allow_multiple_values']);
    }

}

