import { BaseDTO } from "./BaseDTO";

export class EmailTemplateDTO extends BaseDTO {

    protected obj;
    protected fillableDB = ['_id', 'name', 'sendgrid_template_id', 'content', 'variables', 'deleted_at', 'created_at', 'updated_at'];


    protected fillable = ['_id', 'name', 'sendgrid_template_id', 'content', 'variables', 'deleted_at', 'created_at', 'updated_at'];

    constructor(dto) {
        super();
        this.obj = dto
    }

    toRequestJSON() {
        throw new Error("Method not implemented.");
    }

    toSimpleJSON = () => {
        return this.toJSON(['_id', 'name', 'sendgrid_template_id', 'content', 'variables', 'deleted_at', 'created_at', 'updated_at']);
    }
}