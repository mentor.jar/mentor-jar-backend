import { BaseDTO } from "./BaseDTO";

export class ProductCategoryDTO extends BaseDTO {

    protected obj;
    protected fillable = ['_id', 'product_id', 'category_id', 'created_at', 'updated_at'];
    protected fillableDB = ['_id', 'product_id', 'category_id', 'created_at', 'updated_at'];

    constructor(dto) {
        super();
        this.obj = dto;
    }

    toRequestJSON() {
        throw new Error("Method not implemented.");
    }

    toSimpleJSON = () => {
        return this.toJSON(['_id', 'product_id', 'category_id']);
    };

}

