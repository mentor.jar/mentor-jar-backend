import { BaseDTO } from "./BaseDTO";

export class AddressDTO extends BaseDTO {

    protected obj;
    protected fillableDB = ['_id', 'name', 'country', 'state', 'district', 'ward',
        'street', 'phone', 'accessible_id', 'accessible_type', 'is_default', 'is_delivery_default','created_at', 'updated_at', 'is_pick_address_default', 'is_return_address_default', 'expected_delivery', 'address_type'];


    protected fillable = ['_id', 'name', 'main_address', 'country', 'state', 'district', 'ward',
        'street', 'phone', 'accessible_id', 'accessible_type', 'is_default', 'is_delivery_default', 'created_at', 'updated_at', 'is_pick_address_default', 'is_return_address_default', 'expected_delivery', 'address_type'];

    constructor(dto, isEncryptData = false) {
        super();
        this.obj = dto
        this.setAttribute('main_address', this.obj.street + ", " + this.obj.ward.name + ", " + this.obj.district.name + ", " + this.obj.state.name)
        if(isEncryptData) {
            this.setAttribute('main_address', this.obj.street + " *****, " + this.obj.state.name)
            this.setAttribute('phone', this.obj?.phone ? '*******' + this.obj.phone.slice(-3) : '**********')
            // this.setAttribute('name', this.obj.name.slice(0,3) + '*****')
        }   
    }

    toRequestJSON() {
        throw new Error("Method not implemented.");
    }

    toSimpleJSON = () => {
        return this.toJSON(['_id', 'name', 'main_address', 'country', 'state', 'district', 'ward',
            'street', 'phone', 'accessible_id', 'accessible_type', 'is_default', 'is_delivery_default', 'created_at', 'updated_at', 'is_pick_address_default', 'is_return_address_default', 'expected_delivery', 'address_type']);
    }

    toMainAddressJSON = () => {
        return this.toJSON(['_id', 'name', 'phone', 'main_address', 'country', 'state', 'district', 'ward',
        'street', 'phone'])
    }
}
