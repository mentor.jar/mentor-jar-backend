import mongoose from 'mongoose'

export interface ReportLivestreamDoc extends mongoose.Document {
    user_report_id: string,
    stream_session_id: string,
    owner_id: string,
    time_report: Date,
    reasons: Array<Object>,
    is_solved: boolean,
    updated_at: Date
}