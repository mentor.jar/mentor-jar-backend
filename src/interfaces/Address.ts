import mongoose from 'mongoose'

export interface AddressDoc extends mongoose.Document {
    name: string,
    country: Object,
    state: Object,
    district: Object,
    ward: Object,
    street?: string,
    phone: string,
    accessible_id?: string,
    accessible_type?: string,
    is_default: boolean,
    is_delivery_default: boolean,
    created_at: Date,
    updated_at: Date,
    deleted_at?: Date,
    is_pick_address_default: boolean,
    is_return_address_default: boolean,
    expected_delivery: string,
    address_type: string
}
