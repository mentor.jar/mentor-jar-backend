import mongoose from 'mongoose'

export interface ECategoryDoc extends mongoose.Document {
    name: string,
    description: String,
    permalink: String,
    priority: Number,
    is_active: Boolean,
    avatar?: String,
    pdfAvatar? :String,
    shop_id: String,
    parent_id: any,
    created_at: Date,
    updated_at: Date,
    type: String
}
