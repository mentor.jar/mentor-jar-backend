import mongoose from 'mongoose'

export interface AppFeedbackDoc extends mongoose.Document {
    full_name: string,
    email: string,
    how_you_know_bidu: Array<string>,
    satisfied_with_bidu_mark: Number,
    ui_mark: Number,
    shop_config_mark: Number,
    product_manage_mark: Number,
    livestream_mark: Number,
    order_manage_mark: Number,
    opinion: String,
    user_id?: String
}
