import mongoose from 'mongoose';

export interface SystemSettingDoc extends mongoose.Document {
    approved_order: boolean,
    sensitive_content: Array<string>,
    target_new_shop: number,
    target_transaction: number,
    version_production: string,
    version_staging: string,
    version_production_android: number,
    version_staging_android: number,
    app_version: Object,
}