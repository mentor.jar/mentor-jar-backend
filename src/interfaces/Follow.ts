import mongoose from 'mongoose'

export interface FollowDoc extends mongoose.Document {
    userId: string,
    followerId: string,
    actionFollow: string,
    followedDate: Date,
    createdAt: Date,
    updatedAt: Date
}