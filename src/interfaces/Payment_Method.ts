import mongoose from 'mongoose'

export interface PaymentMethodDoc extends mongoose.Document {
    name: string,
    country: string,
    option?: Object,
    name_query: string,
    is_active: boolean,
    created_at: Date,
    updated_at: Date,
}
