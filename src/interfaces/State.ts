import mongoose from 'mongoose'

export interface StateDoc extends mongoose.Document {
    name: string,
    country_id: string,
    zip_code: string
}
