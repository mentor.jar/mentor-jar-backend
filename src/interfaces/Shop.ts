import mongoose from 'mongoose'

export interface ShopDoc extends mongoose.Document {
    name: string,
    description: string | null,
    user_id: string,
    shop_type: string,
    return_money_mode: boolean,
    pause_mode: boolean,
    created_at: Date,
    updated_at: Date,
    country: string,
    refund_conditions?: Array<any>,
    is_approved: boolean,
    ranking_today?: Number,
    ranking_yesterday?: Number,
    avg_rating?: Number,
    shorten_link?: string,
    allow_show_on_top?: Boolean,
    rank_policy: Object,
    middle_banner: string,
    system_banner: Object,
    biggest_price: Number,
    bank_info: Object
}