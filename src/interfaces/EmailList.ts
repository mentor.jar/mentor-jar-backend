import mongoose from 'mongoose';

export interface EmailListDoc extends mongoose.Document {
    name: string,
    sendgrid_template_id: string,
    data: Object,
    status: Array<any>,
    created_at: Date,
    updated_at: Date,
    deleted_at: Date
}