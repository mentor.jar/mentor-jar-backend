import mongoose from 'mongoose'

export interface FeedbackDoc extends mongoose.Document {
    content: Text,
    vote_star: Number,
    medias: Array<any>,
    target_type: String,
    target_id: String,
    is_approved: Boolean,
    is_public: Boolean,
    is_show_body_shape: Boolean,
    shop_feedback?: Object,
    buyer_feedback?: Object,
    user_id: String,
    shop_id: String,
    order_id: String,
    user_liked: Array<any>,
    created_at: Date,
    updated_at: Date
}