import mongoose from 'mongoose'

export interface TrackingJobProcessDoc extends mongoose.Document {
    id: string,
    name: string,
    doing: Number,
    target: Number,
    data_detail: Object,
    created_at: Date,
    updated_at: Date
}

export interface ParamCreateProcess {
    name: string,
    doing: Number,
    target: Number,
    data_detail: Object | null
}
