import mongoose from 'mongoose'

export interface OrderPaymentDoc extends mongoose.Document {
    order_id: string
    user_id: string
    shop_id: string
    price: number
    type: string
    code: string
    payment_info: any
    created_at: Date
    updated_at: Date
}
