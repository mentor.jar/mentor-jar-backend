import mongoose from 'mongoose'

export interface ProductCategoryDoc extends mongoose.Document {
    category_id: string,
    product_id: string,
    created_at: Date,
    updated_at: Date
}