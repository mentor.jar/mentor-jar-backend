import mongoose from 'mongoose'

export interface ENotification extends mongoose.Document {
    type: string,
    targetedBy: string,
    createdBy: string,
    content: string,
    contentType: string,
    targetType: string,
    context: Object,
    isRead: boolean,
    shardingKey: string
}
