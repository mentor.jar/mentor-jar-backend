import mongoose from 'mongoose'

export interface ReferralUsageDoc extends mongoose.Document {
    user_id: string,
    referred_id: string,
    code: string
}