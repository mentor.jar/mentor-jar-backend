import mongoose from 'mongoose'

export interface FollowShopDoc extends mongoose.Document {
    user_id: string,
    shop_id: string,
    created_at: Date,
    updated_at: Date
}