import mongoose from 'mongoose'

export interface VariantOptionValueDoc extends mongoose.Document {
    variant_id: string,
    option_value_id: string,
    created_at: Date,
    updated_at: Date
}