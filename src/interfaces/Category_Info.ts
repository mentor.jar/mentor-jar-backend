import mongoose from 'mongoose'

export interface CategoryInfoDoc extends mongoose.Document {
    name: object,
    type: string,
    fashion_type: number,
    category_id: String | Array<String>,
    list_option: Array<object>,
    is_required: Boolean,
    is_allow_multiple_values: Boolean,
    created_at: Date,
    updated_at: Date
}