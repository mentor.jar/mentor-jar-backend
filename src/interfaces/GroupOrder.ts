import mongoose from 'mongoose'

export interface GroupOrderDoc extends mongoose.Document {
    orders: Array<any>,
    user_id: string,
    total_price: number,
    status?: String,
    created_at: Date,
    updated_at: Date,
    is_group_buy_order: Boolean,
}
