import mongoose from 'mongoose'

export interface SellerSignUpDoc extends mongoose.Document {
    personal_info: {
        full_name: string,
        phone_number: string,
        email: string,
        card_number: string,
        tax_code?: string,
        country:string
    }
    shop_info: {
        shop_name: string,
        is_has_business: boolean,
        description?: string,
        categories: Array<string>,
        address?: {
            state_id: string,
            district_id: string,
            ward_id: string,
            street: string
        } | null,
        social_business: Array<string>
    },
    status: string,
    approved_time?: Date,
    rejected_time?: Date,
    shop_id?: string,
    user_mapped?: Array<any>,
    bank_info: {
        bank_name: string,
        bank_number: string
    }
}
