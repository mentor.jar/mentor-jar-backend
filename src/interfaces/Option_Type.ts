import mongoose from 'mongoose'

export interface OptionTypeDoc extends mongoose.Document {
    name: string,
    description: string | null,
    product_id: string,
    created_at: Date,
    updated_at: Date
}