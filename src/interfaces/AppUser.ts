import mongoose from 'mongoose'

export interface EAppUser extends mongoose.Document {
    userId: string,
    deviceId: string,
    firebaseToken: string,
    os: string,
    shardingKey?: string,
    language: string
}

