import mongoose from 'mongoose'

export interface TrackingObjectDoc extends mongoose.Document {
    user_id: string,
    target_type: string,
    target_id: string,
    ad_hoc: string,
    read: boolean,
    created_at: Date,
    updated_at: Date
}
