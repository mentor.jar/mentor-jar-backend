import mongoose, { ObjectId } from 'mongoose'

export interface OrderItemDoc extends mongoose.Document {
    order_id: string,
    quantity: number,
    variant: Object,
    product_id: string,
    product: {
        name: String,
        weight: number,
    },
    images: Array<String>,
    created_at: Date,
    updated_at: Date,
    weight: Number,
}
