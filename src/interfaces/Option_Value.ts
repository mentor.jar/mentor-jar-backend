import mongoose from 'mongoose'

export interface OptionValueDoc extends mongoose.Document {
    name: string,
    option_type_id: string,
    created_at: Date,
    updated_at: Date
}