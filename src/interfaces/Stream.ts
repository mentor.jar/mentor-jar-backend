import mongoose from 'mongoose'

export interface StreamDoc extends mongoose.Document {
    name: String,
    description: String | null,
    user_id: String,
    rtmp_link: String,
    hls_link: String,
    created_at: Date,
    updated_at: Date
}