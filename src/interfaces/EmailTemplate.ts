import mongoose from 'mongoose';

export interface EmailTemplateDoc extends mongoose.Document {
    name: string,
    sendgrid_template_id: string,
    content: string,
    variables: Array<any>,
    created_at: Date,
    updated_at: Date,
    deleted_at: Date
}