import mongoose from 'mongoose'

export interface DistrictDoc extends mongoose.Document {
    name: string,
    state_id: string
}
