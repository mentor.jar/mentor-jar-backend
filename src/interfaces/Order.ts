import mongoose, { ObjectId } from 'mongoose'

export interface OrderDoc extends mongoose.Document {
    order_number: string
    total_price: number
    payment_status: string
    shipping_status: any
    payment_method_id: string
    shipping_method_id: string
    user_id: string
    handle_by?: string
    shop_id: string
    parent_id?: string
    vouchers?: Array<any>
    created_at: Date
    updated_at: Date
    deleted_at?: Date
    address: string,
    pick_address: any,
    total_value_items: number,
    shipping_fee: number,
    shipping_discount: number,
    voucher_discount: number,
    note: Object,
    group_order_id: string,
    cancel_reason: string,
    cancel_type: string,
    cancel_by: string,
    cancel_time: Date,
    seller_note: string,
    approved_time: Date,
    pick_status: Array<any>,
    refund_information: Object,
    order_service?: string,
    is_group_buy_order: Boolean
}
