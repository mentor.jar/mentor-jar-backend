import mongoose from 'mongoose'

export interface CartItemDoc extends mongoose.Document {
    user_id: string,
    variant_id?: string,
    product_id: string,
    shop_id: string,
    quantity: number
}