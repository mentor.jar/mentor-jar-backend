import mongoose from 'mongoose'

export interface ProductDetailInfoDoc extends mongoose.Document {
    value: string,
    values: Array<string>,
    category_info_id: string,
    product_id: string,
    created_at: Date,
    updated_at: Date
}