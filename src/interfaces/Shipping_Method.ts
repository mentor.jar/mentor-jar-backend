import mongoose from 'mongoose'

export interface ShippingMethodDoc extends mongoose.Document {
    name: string,
    type: string,
    country: string,
    shipping_fee: number,
    option?: Object,
    name_query: string,
    created_at: Date,
    updated_at: Date,
}
