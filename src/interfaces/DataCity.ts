import mongoose from 'mongoose'

export interface DataCityDoc extends mongoose.Document {
    Id: string,
    Name: string,
    Districts: Array<any>,
    id_vtp: number
}
