import { Timestamp } from 'mongodb';
import mongoose from 'mongoose'

export interface VariantDoc extends mongoose.Document {
    sku: string,
    quantity: number,
    before_sale_price: number,
    sale_price: number,
    is_master: boolean,
    deleted_at: Timestamp,
    product_id: string,
    created_at: Date,
    updated_at: Date
}