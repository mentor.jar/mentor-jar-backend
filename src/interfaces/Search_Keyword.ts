import mongoose from 'mongoose'

export interface SearchKeywordDoc extends mongoose.Document {
    keyword: string,
    count_number: number,
    shop_id?: string,
    created_at: Date,
    updated_at: Date,
}