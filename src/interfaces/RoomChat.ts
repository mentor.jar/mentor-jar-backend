import mongoose from 'mongoose'

export interface RoomChatDoc extends mongoose.Document {
    name: string,
    creator_id: string,
    is_group: boolean,
    messages: Array<any>,
    users: Array<any>,
    file_storages: Array<any>,
    last_message: Object
}