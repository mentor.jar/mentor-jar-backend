import mongoose from 'mongoose';

export interface ENotifyCampaign extends mongoose.Document {
    type: String;
    title: String;
    content: String;
    receivers?: Array<String>;
    topic?: Array<String>;
    description?: String;
    createdBy?: String;
    data?: Object;
    os?: String;
    image?: String;
    sound?: String;
    schedule?: String;
    timeWillPush?: Date;
}
