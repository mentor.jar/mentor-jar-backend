import mongoose from 'mongoose'

export interface RoleDoc extends mongoose.Document {
    nameRole: string,
    description: string,
    isActive: boolean,
    rights: Array<any>,
    created_at: Date,
    updated_at: Date
}
