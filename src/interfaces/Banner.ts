import mongoose from 'mongoose'

export interface EBanner extends mongoose.Document {
    name: String,
    image: String,
    images: Array<any>,
    promo_link: String,
    products: Array<String>,
    is_active: Boolean,
    shop_id: String,
    start_time: Date,
    end_time: Date,
    order: number,
    classify?: String,
    position: String,
    vouchers?: Array<String>,
    images_web?: Object,
    priority?: number,
    advance_actions?: Object,
    shorten_link?: string,
    description?: string,
}
