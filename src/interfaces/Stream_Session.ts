import mongoose from 'mongoose'

export interface StreamSessionDoc extends mongoose.Document {
    session_number: String,
    stream_id: String,
    name: String,
    image: String,
    type: String,
    type_of_viewer: String,
    status: String,
    list_id_product: Array<String>,
    view: Number,
    view_max: Number,
    heart: Number,
    time_will_start: Date,
    time_start: Date,
    time_end: Date,
    user_id: String,
    shop_id: String,
    room_chat_id: String,
    active: Boolean,
    archive_links: Array<String>,
    check_disconnect: number,
    created_at: Date,
    updated_at: Date
}