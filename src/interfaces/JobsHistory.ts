import mongoose from 'mongoose'

export interface EJobHistory extends mongoose.Document {
    jobName: String,
    objectType: String,
    objectIds: Array<String>,
    affectedType: String,
    affectedIds: Array<String>,
}
