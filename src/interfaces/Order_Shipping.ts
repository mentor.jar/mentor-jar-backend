import mongoose from 'mongoose'

export interface OrderShippingDoc extends mongoose.Document {
    user_id: string
    order_id: string
    shop_id: string
    user_confirm_shipped_tim: Date
    history: Array<Object>
    created_at: Date
    updated_at: Date
    shipping_info: Object
}
