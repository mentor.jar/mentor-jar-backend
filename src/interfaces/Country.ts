import mongoose from 'mongoose'

export interface CountryDoc extends mongoose.Document {
    name: string,
    code: string,
    zip_code: string
}
