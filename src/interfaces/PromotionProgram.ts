import mongoose from 'mongoose'

export interface PromotionProgramDoc extends mongoose.Document {
    name: string,
    start_time: Date,
    end_time: Date,
    discount: Number,
    limit_quantity: Number,
    products: Array<any>,
    is_valid: boolean,
    shop_id: string,
    is_reset: boolean,
    created_at: Date,
    updated_at: Date
}