import mongoose from 'mongoose'

export interface TrackingActivityDoc extends mongoose.Document {
    target_id: string,
    target_type: string,
    actor_id: string,
    actor_type: string,
    action_type: string,
    action_info: object,
    count_number: number,
    visited_ats: Array<Date>,
    created_at?: Date,
    updated_at?: Date,
}
