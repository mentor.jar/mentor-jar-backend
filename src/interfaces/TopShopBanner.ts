import mongoose from 'mongoose'

export interface ITopShopBanner extends mongoose.Document {
    name: String,
    images: Object,
    shop_ids: Array<any>,
    is_active: Boolean,
}