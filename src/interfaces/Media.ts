import mongoose from 'mongoose'

export interface MediaDoc extends mongoose.Document {
    accessible_type?: string,
    accessible_id?: String,
    path: String,
    full_path: String,
    mimetype: String,
    filename: String,
    encoding: String,
    driver_type?: String,
    created_at: Date,
    updated_at: Date
}