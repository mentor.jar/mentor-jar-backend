import { config } from 'dotenv'
config()
export default {
    api_ecommerce: process.env.HOST_API_ECOMMERCE,
    mongoDbUrl: process.env.MONGO_URI_DEVELOPMENT,
    mongoDbChatUrl: process.env.MONGO_URI_CHAT_DEVELOPMENT,
    elsUrl: process.env.ELS_URI_DEVELOPMENT,
    elsPassword: process.env.ELS_PASSWORD_DEVELOPMENT,
    jwtSecret: process.env.JWT_SECRET_DEVELOPMENT, //comment
    host: `localhost:${process.env.PORT}`,
    host_community: process.env.HOST_COMMUNITY,
    sendgrid_api_key: process.env.SENDGRID_KEY_DEVELOPMENT,
    firebase_sdk_credential: {
        "type": "service_account",
        "project_id": "bidu-ecommerce",
        "private_key_id": "930c47cc0f5cbf357fba9e84c2ca810fb7471997",
        "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDJ5ymp//XIVJoY\nFh+wdchS1Sj+DtxYmqzjsd1o0bKnz6pDUzYFsFbMe1YFOFkgJdjMr6Uyn3i3r+Af\nLuih3ZYMafyQWHunZLYzDxHHXJm6cK2QbCjBBqU3ERkiXrozASUtPy9/9bZe9/CZ\n9X0ce31XPldpZZW4NeDCHb0g8WG6fvpQbJ5AwPu/vhC3YC++Vmq18WCtkd7IkPbs\nEJ6ABbCPEJX8MKhP/lYcaZnfMlkZ6R6dlpNuXloYENKgXOMud4bLb6NqCf1x+/10\nGvhWDebyDfImlpGOtbe9siNxnk9J5RncrKrxpXXYk6L05/xWmpmlustmn0I0x7pd\n/ocyCFxjAgMBAAECggEAPYMCVAsp1+9ChS0UWYAA2I+drg1b/1icbMBvKrScf/jK\nMDnFPyVyxFidI/j/WkZ9jxrCMzKXpircltOFR29JfB28N0Pl5GFQCy0/puPk6ALV\nNidloS1Dn4vDgw+mHczvBSJ3d74l4jgaouVCSS+sR6y0F+W9oAI2KB3EVamEszra\nwRtDUJyyjQqEen8z35YU/ZX+pM3zGcu5fD0S5FcNqzuxkeQqUroVc+KnjVNPXhim\nYtJab/jhc+MFeqkot9xR3TR0681pd9XUkb+/XV73k2lXQ9h6n0+VswEUrdoWYiWF\nEXqlP3+6Ez0Q6jQ5j9l79t526IsNX3F5GL1WDRTPKQKBgQDmB0RE1CiRJEHGv7YW\nEdcXxREQGxS0DM5Cz6SPvrRHeLWntH5/F69jrHT+D3Q48OC2ELvIvUMqwzy997Or\nePKWXH1dW8TJawmV/K5AiDTqFZPqT2/nV0UzNkLjcJGptAUFeXKDR+lwvtCnTQl2\nnz1NUINSooZOLWNlz14/iI20qwKBgQDgsvXQgVjGYwgbXHbucuxP3JLYLLrnTv3n\nMtvih1B5c0SNrPGx8XH+AlScONeS41drxM30QuOCMxUoZs09uNo2QkJppRQjbTNk\nGzPALnC7B+q6qL86YklHBfWv77sHWKDcUBnn8D11S59gNgt28EWgLkkCCf6HfR1B\nAhCrVLFHKQKBgQCdtdu7qQ5LPOWQI77u8LpdRLLHLRoLvD/qlLzKBUFLQ1qgqeKU\ntqN8ni/0RJo1tAkNKvnkzsZvWj6aLGtWJG2GY9FcIeARDL8cw4qhGgHVgIfSfTZH\nEZGTZB+GGpb0GEXdFABMPBC0SU97dRkI6HK6QHpZPK8CiKH+mqeOvcwSUwKBgQCX\nkF86y2TEPJyFSDN419AKdnAenN1dlOyy/J1RCZEAuv2sytElBAWuHohP8A+JJwwa\nx19cuZmYgzhsTht23K9kFgGSHSWeV9RoW+jmYuzrW4W7boO+r/lZjO2SRRRU+jVX\nJjizOFtcOSxBy3KkQaHVUCMy2ubzITOQXAI+bxw9MQKBgEomu7kxXADNpxMcA7vm\njrVDuhcqDaTHKO8nXb4KcSp3GClbbnP++k1QwPFVI5iGN9ygThSWqDa0E4qlNsLr\nsgBEJWBr3oav6XKK9rWIAUrDNE5mfm/oy+WXycakTZeZrssv7dMQNOCLjOo/T5kA\ndp5ATCRfogl21XhmCusJxPtO\n-----END PRIVATE KEY-----\n",
        "client_email": "firebase-adminsdk-yjtjr@bidu-ecommerce.iam.gserviceaccount.com",
        "client_id": "113758618398403702234",
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://oauth2.googleapis.com/token",
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
        "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-yjtjr%40bidu-ecommerce.iam.gserviceaccount.com"
    },
    firebase_db_url: `https://bidu-ecommerce.firebaseio.com`,
    momo_payment: {
        partner_code: process.env.MOMO_PARTNER_CODE,
        access_key: process.env.MOMO_ACCESS_KEY,
        sercret_key: process.env.MOMO_SERCRET_KEY,
        end_point: process.env.MOMO_END_POINT_MOMO,
        return_url: process.env.MOMO_RETURN_URL,
        ipn_url: process.env.MOMO_IPN_URL,
        refund_end_point: process.env.MOMO_REFUND_END_POINT_MOMO,
        public_key: process.env.MOMO_RSA_PUBLIC_KEY
    },
    vnpay_payment: {
        tmnCodeVnPay: process.env.TMN_CODE_VNPAY,
        secretKeyVnPay: process.env.SECRET_KEY_VNPAY,
        urlVnPay: "http://sandbox.vnpayment.vn/paymentv2/vpcpay.html",
        returnUrlVnPay: process.env.VNPAY_RETURN_URL,
        returnUrlWebVnPay: process.env.VNPAY_RETURN_URL_WEB,
    },
    esms: {
        esms_api_key: process.env.ESMS_API_KEY,
        esms_secret_key: process.env.ESMS_SECRET_KEY,
        esms_url: "https://restapi.esms.vn/MainService.svc/json/SendMultipleMessage_V4_get",
        esms_brand_name: "Baotrixemay",
    },
    GHTK: {
        GHTKb2cToken: process.env.GHTK_B2C_TOKEN,
        ghtk_url: 'https://services.ghtklab.com',
    },
    speed_sms: {
        speed_sms_access_token: process.env.SPEED_SMS_ACCESS_TOKEN,
        speed_sms_url: "https://api.speedsms.vn/index.php/sms/send"
    },
    whitelist: "*",
    community_api: {
        url: "https://api-staging.bidu.com.vn"
    },
    VIETTELPOST: {
        token: process.env.VIETTELPOST_TOKEN_DEV,
        email: process.env.VIETTELPOST_EMAIL_DEV,
        password: process.env.VIETTELPOST_PASSWORD_DEV,
    }
}

