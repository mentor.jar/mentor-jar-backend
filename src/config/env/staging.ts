import { config } from 'dotenv'
config()

export default {
    api_ecommerce: process.env.HOST_API_ECOMMERCE,
    mongoDbUrl: process.env.MONGO_URI_STAGING,
    mongoDbChatUrl: process.env.MONGO_URI_CHAT_STAGING,
    elsUrl: process.env.ELS_URI_STAGING,
    elsPassword: process.env.ELS_PASSWORD_STAGING,
    jwtSecret: process.env.JWT_SECRET_STAGING,
    host: process.env.HOST_STAGING,
    host_community: process.env.HOST_COMMUNITY,
    sendgrid_api_key: process.env.SENDGRID_KEY_STAGING,
    firebase_sdk_credential: {
        type: "service_account",
        project_id: "v-fashionvideo",
        private_key_id: "12b8d0c9f44b648dd2ef38204164ed512a30a10e",
        private_key: "-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDcEjqTaQfysyHH\n1hBfgFgp54ObQCeByj5JIL5ordjJNZ2ahyj58OrDjnvKIPxsZTKMslJxGrc9o9zE\n7X6t3H3K7tq2vYQAgi9hL2jKFfVGhzEfG0PTFvvvas81qj+/0BI2+g9y39V6/jm2\nf8sf+h9Zc+y6TQPVdfqDjOOeNAAn14lPq/OqTKr38lgoKMoK5ScMft+GyCmk7rhz\nA5pkJVJRdBDWod4TcAHerrMnnozkHrOXggXYAjG3mCEpc5kdB8UUXTM0ou8cGDVH\n8KczVBL2N1XbtBuwF91UBCqlPstcrCKdiMhfJDrL3tMHdbG+sJOaEu4qZZ2o1UGQ\nB4zRKncpAgMBAAECggEADIHmtvQ/Tn5hNNCh1P1GeKwFRIB30YsTUVtDwLXjF8Og\nOPH/x+P228I2nxOTDrANS7mVl0/VStk/L1wJTDIdxIS6hN9H5/8XvtzC0y8LTFGO\n3tc3jbQ4Z/BSMkxPDwSkb5NBTh4r+mKffuB/EWRJF6LpUq9IDZlrPij2nzB4zJeU\nfxcw7+gL6kWOnSd9v6OQWaKlfh9Hml7amZaq3a5VXpsmO4NP6Zfaw2n/fwxSmQoW\nL+2Lrw2r+xEgu15b4i6N3AeInvU70Tkv/O4WgOcAoWl7k8/Wl//1nN/BYlT58j99\nToORkitRsZY/DOH9TV9EHTcT2pGCSFVWSzmMQNAShwKBgQDvyPxGvdPHrmSh/Zyh\nIJm0nwk8WIT6biRulAeG6Ba3uQ4F9vjYAR2dcaurxTY9oArPyHQuROZHUgvbCaN/\nfz2oZ44KlukyghSMI9+knQHYbQyABfnXtW24tOpkQ5hSBDk3bFgDMbGj/PRraHz6\nejLKHcCfmQteWshxoKrIal+hwwKBgQDq8/fm/oIYonOKQOs7/iEjJ24toiFHK4O2\nHI/8q27Ubd+zDzm4UssO7WqjoD/vK3PiPaHRWQVsuHTwpsP+OXmVrQzcf3BuPPsF\nQIsIHSDnzdeAHV79wTWRkE4i+BACz7pd9vpC3nhg9SXJBg63rryn2avyesw7rEqx\n+J/V9wAoowKBgQDj8u9Q2NjhdPoZvR0PWAyG1sFS6tvPrrFXpcXaC/y8v2GDbht2\nTOk4aX1Hl/1ObDya9vbrnXhEyMyYd/tEE1QsKfWwP3KFH4AlEV1kAhZlBpXhhszS\nsZkTBHnmwxOktHsoIwkwUCeNu8Un7ncRLuXlmo8pqbZTBn6S9qgkSmuLPQKBgQDn\n06wC35NMbVZlzyKMgnb98dUT78q4J6aBVXN5U0o7DuLQEjDYv9CePQKgyujqu8xP\nKpL1NspUeTjhjxsKSHnAhwyOsKwmCcOBbisA6YTjVEbToadt6GtL4sBGXs+lhoQU\nXTKYMl1DBgCvdmulMCPz1qKD0kichKb7Lh/ABkTA0wKBgQCdhl+l0Jf8uA4T+cXZ\nUuwrq0O129Wb3hF2Gydz33HuTcuVNacFYjONc2Q7GwkRa4nST6AVcVVfHnx8ZVpZ\nAdk3zn11KdY/26uD+Y3fJS81qEJE4lrDZMYbk6hO+kZhz3a+NfTg4Ab2O13vIEwj\nW07Ynw6/d8ohmQn8kui4qDlkFw==\n-----END PRIVATE KEY-----\n",
        client_email: "firebase-adminsdk-z3c7q@v-fashionvideo.iam.gserviceaccount.com",
        client_id: "101196583135477182658",
        auth_uri: "https://accounts.google.com/o/oauth2/auth",
        token_uri: "https://oauth2.googleapis.com/token",
        auth_provider_x509_cert_url: "https://www.googleapis.com/oauth2/v1/certs",
        client_x509_cert_url: "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-z3c7q%40v-fashionvideo.iam.gserviceaccount.com"
    },
    firebase_db_url: `https://v-fashionvideo.firebaseio.com`,
    momo_payment: {
        partner_code: process.env.MOMO_PARTNER_CODE,
        access_key: process.env.MOMO_ACCESS_KEY,
        sercret_key: process.env.MOMO_SERCRET_KEY,
        end_point: process.env.MOMO_END_POINT_MOMO,
        return_url: process.env.MOMO_RETURN_URL,
        ipn_url: process.env.MOMO_IPN_URL,
        refund_end_point: process.env.MOMO_REFUND_END_POINT_MOMO,
        public_key: process.env.MOMO_RSA_PUBLIC_KEY
    },
    vnpay_payment: {
        tmnCodeVnPay: process.env.TMN_CODE_VNPAY,
        secretKeyVnPay: process.env.SECRET_KEY_VNPAY,
        urlVnPay: "http://sandbox.vnpayment.vn/paymentv2/vpcpay.html",
        returnUrlVnPay: process.env.VNPAY_RETURN_URL,
        returnUrlWebVnPay: process.env.VNPAY_RETURN_URL_WEB,
    },
    esms: {
        esms_api_key: process.env.ESMS_API_KEY,
        esms_secret_key: process.env.ESMS_SECRET_KEY,
        esms_url: "https://restapi.esms.vn/MainService.svc/json/SendMultipleMessage_V4_get",
        esms_brand_name: "Baotrixemay",
    },
    GHTK: {
        GHTKb2cToken: process.env.GHTK_B2C_TOKEN,
        ghtk_url: 'https://services.ghtklab.com',
    },
    speed_sms: {
        speed_sms_access_token: process.env.SPEED_SMS_ACCESS_TOKEN,
        speed_sms_url: "https://api.speedsms.vn/index.php/sms/send"
    },
    whitelist: "*",
    community_api: {
        url: "https://api-staging.bidu.com.vn"
    },
    VIETTELPOST: {
        token: process.env.VIETTELPOST_TOKEN_DEV,
        email: process.env.VIETTELPOST_EMAIL_DEV,
        password: process.env.VIETTELPOST_PASSWORD_DEV,
    }
}
