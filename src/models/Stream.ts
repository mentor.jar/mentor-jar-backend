import mongoose from 'mongoose'
import { StreamDoc } from '../interfaces/Stream'

const Schema = mongoose.Schema;

const StreamSchema = new mongoose.Schema({
    name: {
        type: String
    },
    description: {
        type: String,
        default: null
    },
    rtmp_link: {
        type: String,
        default: null
    },
    hls_link: {
        type: String,
        default: null
    },
    application: {
        type: String
    },
    shop_id: { type: Schema.Types.ObjectId, ref: 'Shop' },
    user_id: { type: Schema.Types.ObjectId, ref: 'User' }
}, {
    versionKey: false,
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
})

const stream = mongoose.model("Stream", StreamSchema)
export default stream