import mongoose from 'mongoose'
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const Schema = mongoose.Schema;

const BBookUserSchema = new mongoose.Schema({
	type: {
		type: String
	},
	userId: {
		type: Schema.Types.ObjectId
	},
	music: {
		type: String
	},
	orientation: {
		type: String
	},
	length: {
		type: Number
	},
	autoPlay: {
		type: Boolean,
		default: false
	},
	scenes: [{
		type: Object
	}],
	thumbnail: {
		type: String
	},
	templateId: {
		type: Schema.Types.ObjectId
	},
	photosRequired: {
		type: Number
	},
	videosRequired: {
		type: Number
	},
	isPublic: {
		type: Boolean,
		default: true
	},
	dimensions: {
		width: {
			type: Number
		},
		height: {
			type: Number
		}
	},
	shorten_link: {
		type: String,
		default: null
	}
}, {
	versionKey: false,
	timestamps: true
})

BBookUserSchema.plugin(paginate)
BBookUserSchema.plugin(aggregatePaginate)

const BBookUser = mongoose.model("BBookUser", BBookUserSchema)
export default BBookUser