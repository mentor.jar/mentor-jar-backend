import mongoose from 'mongoose'
import { VoucherDoc } from '../interfaces/Voucher'
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const Schema = mongoose.Schema;

const CashBackRefSchema = new mongoose.Schema({
    user_id: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        default: null
    },
    cash_back_id: {
        type: Schema.Types.ObjectId,
        ref: 'Voucher',
        default: null
    }
})

const VoucherConditionSchema = new mongoose.Schema({
    limit_per_user: {
        type: Number
    },
    limit_per_user_by_day: {
        type: Number
    },
    shipping_method: {
        type: Array,
        default: []
    },
    payment_method: {
        type: Array,
        default: []
    }
})

const VoucherSchema = new mongoose.Schema({
    name: {
        type: String,
        require: [true, 'Vui lòng nhập tên voucher']
    },
    code: {
        type: String,
        require: [true, 'Vui lòng nhập mã voucher']
    },
    target: {
        type: String,
        require: [true, 'Vui lòng chọn target của voucher'],
        enum: ['shop', 'product', 'system', 'user', 'label', 'introduce', 'introduced', 'kol', 'newbie'],
        default: 'shop'
    },
    start_time: {
        type: Date,
        require: [true, 'Vui lòng nhập thời gian bắt đầu'],
    },
    end_time: {
        type: Date,
        require: [true, 'Vui lòng nhập thời gian bắt đầu'],
    },
    save_quantity: {
        type: Number,
        require: [true, 'Vui lòng nhập số lượng có thể lưu']
    },
    use_quantity: {
        type: Number,
        require: [true, 'Vui lòng nhập số lượng có thể sử dụng']
    },
    available_quantity: {
        type: Number,
        require: [true, 'Vui lòng nhập số lượng còn khả dụng']
    },
    type: {
        type: String,
        require: [true, 'Vui lòng chọn loại mã giảm giá'],
        enum: ['price', 'percent'],
        default: 'price'
    },
    value: {
        type: Number,
        default: 0
        // require: [true, 'Vui lòng nhập giá trị giảm giá']
    },
    discount_by_range_price: {
        type: Array,
        default: []
    },
    max_discount_value: {
        type: Number,
        default: null
    },
    display_mode: {
        type: String,
        require: [true, 'Vui lòng chọn chế độ hiển thị'],
        enum: ['all', 'none'],
        default: 'all'
    },
    min_order_value: {
        type: Number,
        require: [true, 'Vui lòng nhập giá trị đơn hàng tối thiểu']
    },
    max_order_value: {
        type: Number,
        require: false,
        default: null
    },
    shop_id: { type: Schema.Types.ObjectId, ref: 'Shop' },
    products: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Product'
        }
    ],
    shops: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Shop'
        }
    ],
    users: [
        {
            type: Schema.Types.ObjectId,
            ref: 'User'
        }
    ],
    labels: [
        {
            type: Schema.Types.ObjectId
        }
    ],
    kol_user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        default: null
    },
    cash_back_ref: {
        type: CashBackRefSchema,
        default: null
    },
    approve_status: {
        type: String,
        require: [true, 'Vui lòng chọn thái phê duyệt của voucher'],
        enum: ['pending', 'approved', 'rejected'],
        default: 'approved'
    },
    classify: {
        type: String,
        enum: ['free_shipping', 'discount', 'cash_back', 'cash_back_discount']
    },
    is_public: {
        type: Boolean,
        default: true,
    },
    max_budget: {
        type: Number,
    },
    conditions: VoucherConditionSchema,
    used_by: {
        type: Object
    },
    using_by: {
        type: Object // for cash back voucher
    },
    used_budget: {
        type: Number,
        default: 0
    },
    deleted_at: {
        type: Date
    }
}, {
    versionKey: false,
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
})

VoucherSchema.statics.relationship = function () {
    
}

VoucherSchema.plugin(paginate);
VoucherSchema.plugin(aggregatePaginate);

const Voucher = mongoose.model<VoucherDoc>("Voucher", VoucherSchema)
export default Voucher