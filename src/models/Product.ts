import { Timestamp } from 'mongodb';
import mongoose from 'mongoose'
import { ProductDoc } from '../interfaces/Product'
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const Schema = mongoose.Schema;

const SizeInfos = new mongoose.Schema({_id: false,name: String, min:Number, max: Number, unit:String});

const DetailSize = new mongoose.Schema({id: false, type_size: String, size_infos:[SizeInfos]});

const RefundObjectSchema = new mongoose.Schema({
    code: Number,
    vn_name: String,
    ko_name: String,
    en_name: String,
    active: {
        type: Boolean,
        default: false
    },
    _id: false
})

export const FlashSaleSchema = new mongoose.Schema({
    flash_sale_id: {
        type: Schema.Types.ObjectId, ref: 'FlashSale', require: true
    },
    price: {
        type: Number, default: null
    },
    quantity: {
        type: Number, default: null
    },
    discount_percent: {
        type: Number, default: null
    },
    sold: {
        type: Number, default: 0
    },
    start_time: {
        type: Date,
        require: true
    },
    end_time: {
        type: Date,
        require: true
    },
});

export const variantOfGroupBuySchema = new mongoose.Schema({ 
    variant_id: Schema.Types.ObjectId, 
    discount: Number, 
    group_buy_price: Number,
    quantity: Number,
    sold: Number
}, {
    _id: false
})

export const groupsOfGroupBuySchema = new mongoose.Schema({ 
    number_of_member: Number, 
    discount: Number, 
    group_buy_price: Number, 
    variants: [variantOfGroupBuySchema]
})

export const ComparePriceGroupBuySchema = new mongoose.Schema({
    url: {
        type: String,
        require: false,
        default: ''
    },
    price: {
        type: Number,
        require: true
    }
}, {
    _id: false
})

export const GroupBuySchema = new mongoose.Schema({
    shop_group_buy_id: Schema.Types.ObjectId,
    name: String,
    start_time: Date,
    end_time: Date,
	status: {
        type: String,
        enum: ['pending', 'reject', 'approved'],
        default: 'pending'
    },
    discount_type: {
        type: String,
        enum: ['price', 'percent']
    },
    quantity: Number,
    sold: Number,
    groups: [groupsOfGroupBuySchema],
    compare_price: {
        type: ComparePriceGroupBuySchema,
        require: false
    }
}, {
    _id: false
})

export const GroupBuyOrderInfoSchema = new mongoose.Schema({
    order_id: Schema.Types.ObjectId,
    group_order_id: Schema.Types.ObjectId,
    order_number: String
}, {
    _id: false
})

export const MemberOrderRoomShema = new mongoose.Schema({ 
    role: {
        type: String,
        enum: ['invited', 'joined', 'owner'],
        default: 'invited'
    }, 
    referal_link: String, 
    payment_status: {
        type: String,
        enum: ['paid', 'refuned'],
        default: 'paid'
    }, 
    invited_time: Date, 
    invited_by: Schema.Types.ObjectId, 
    joined_time: Date,
    avatar: String,
    order_info: GroupBuyOrderInfoSchema,
})

export const OrderRoomSchema = new mongoose.Schema({
    owner_id: Schema.Types.ObjectId,
    group_id: Schema.Types.ObjectId,
    is_valid: Boolean,
    status: {
        type: String,
        enum: ['pending', 'completed', 'canceled'],
        default: 'pending'
    },
    members: [MemberOrderRoomShema],
    variant_id: Schema.Types.ObjectId || null,
    created_at: Date, 
    updated_at: Date 
})

const ProductSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Please add a product name']
    },
    description: {
        type: String,
        require: [true, 'Please add a description']
    },
    short_description: {
        type: String
    },
    friendly_url: {
        type: String,
    },
    weight: {
        type: Number,
    },
    width: {
        type: Number
    },
    height: {
        type: Number
    },
    length: {
        type: Number
    },
    before_sale_price: {
        type: Number,
        require: [true, 'Please add a before_sale_price']
    },
    sale_price: {
        type: Number,
        default: null
        // require: [true, 'Please add a sale price'] 
    },
    quantity: {
        type: Number,
        default: 0
    },
    deleted_at: {
        type: Date || null
    },
    allow_to_sell: {
        type: Boolean,
        default: true
    },
    // pending
    // approved
    // reject
    is_approved: {
        type: String,
        default: "pending"
    },
    is_pre_order: {
        type: Boolean,
        default: true
    },
    images: [{ type: String, default: null }],
    authen_images: [{ type: String, default: null }],
    shop_id: { type: Schema.Types.ObjectId, ref: "Shop" },
    category_id: { type: Schema.Types.ObjectId, ref: 'ECategory' },
    list_category_id: {
        type: Array,
        default: []
    },
    detail_size: [
        {   _id: false, 
            type: DetailSize, 
            default: []
        }
    ],
    limit_sale_price_order: {
        type: Number,
        default: 0
    },
    sale_price_order_available: {
        type: Number,
        default: 0
    },
    shipping_information: {
        type: String,
        default: ''
    },
    delivery_instruction: {
        type: String,
        default: ''
    },
    exchange_information: {
        // Not use anymore from 08/09, but just keep this
        type: String,
        default: ''
    },
    delivery_information: {
        type: String,
        default: ''
    },
    allow_refund: {
        type: Boolean,
        default: true,
    },
    duration_refund: {
        type: Number,
        default: 0
    },
    description_images: {
        type: [ 
            {
                _id: false,
                url: {
                    type: String,
                    default: null
                },
                width: {
                    type: Number,
                    default: 0
                },
                height: {
                    type: Number,
                    default: 0
                }
            }                                   
        ],
        default: []
    },
    refund_conditions: [RefundObjectSchema],
    sold: {
        type: Number,
        default: 0,
    },
    shorten_link: {
        type: String,
        default: null
    },
    custom_images: {
        type: [
            {
                _id: false,
                url: {
                    type: String,
                    default: ""
                },
                x_percent_offset: {
                    type: Number,
                    default: null, min: 0, max: 100
                },
                y_percent_offset: {
                    type: Number,
                    default: null, min: 0, max: 100
                }
            }
        ],
        default: null
    },
    popular_mark: {
        type: Number,
        default: 0
    },
    body_shape_mark: {
        type: Object,
        default: null
    },
    is_suggested: {
        type: Object,
        default: false
    },
    is_guaranteed_item: { // Hàng được đảm bảo
        type: Boolean,
        default: false
    },
    is_genuine_item: { // Hàng chính hãng
        type: Boolean,
        default: false
    },
    top_photo_display_full_mode: {
        type: Boolean,
        default: false
    },
    update_price_remaining: {
        type: Number,
        default: 3,
    },
    next_date_update_price: {
        type: Date,
    },
    price_updated_dates: {
        type: Array,
        default: []
    },
    is_sold_out: {
        type: Boolean,
        default: false
    },
    flash_sale: [FlashSaleSchema],
    group_buy: GroupBuySchema,
    order_rooms: [OrderRoomSchema],
    bidu_air: {
        type: Boolean,
        default: false
    }
}, {
    versionKey: false,
    timestamps: true
})
ProductSchema.statics.const_is_approved = function () {
    return {
        pending: 'pending',
        approved: 'approved',
        rejected: 'rejected',
    }
}
ProductSchema.statics.relationship = function () {
    const variants = () => (
        {
            type: 'hasMany',
            table: 'variants',
            foreign_key: 'product_id',
            as: 'variants'
        }
    )

    const productDetailInfos = () => (
        {
            type: 'hasMany',
            table: 'productdetailinfos',
            foreign_key: 'product_id',
            as: 'product_detail_infos'
        }
    )
    const categories = () => (
        {
            type: 'belongTo',
            table: 'ecategories',
            foreign_key: 'category_id',
            as: 'category'
        }
    )
    const shops = () => (
        {
            type: 'belongTo',
            table: 'shops',
            foreign_key: 'shop_id',
            as: 'shop',
            child: {
                type: "lookup",
                unwind: true,
                query: {
                    from: "users",
                    let: {
                        user_id: "$user_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                '$expr': {
                                    $and: [
                                        {
                                            $eq: ["$_id", "$$user_id"]
                                        }
                                    ]
                                },
                            }
                        }
                    ],
                    as: "user"
                }
            }
        }
    )
    return { variants, productDetailInfos, categories, shops }
}
ProductSchema.plugin(paginate);
ProductSchema.plugin(aggregatePaginate);

const Product = mongoose.model<ProductDoc>("Product", ProductSchema)
export default Product