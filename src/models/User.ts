import mongoose from 'mongoose'
import validator from 'validator'
import bcrypt from 'bcryptjs'
import { object } from 'mongoose/lib/utils'
import { vietnameseStringToUnicode } from '../utils/stringUtil'
import { UserDoc } from '../interfaces/User'
import GalleryImage from './Gallery_Image'
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const BodyMeasurementSchema = new mongoose.Schema({
  height: {
    type: Number,
  },
  weight: {
    type: Number,
  },
  bustSize: {
    type: Number,
  },
  waistSize: {
    type: Number,
  },
  highHipSize: {
    type: Number,
  },
  hipSize: {
    type: Number,
  },
}, { _id: false });
const BodyMeasurement = mongoose.model('BodyMeasurement', BodyMeasurementSchema)

const UserSchema = new mongoose.Schema({
  userName: {
    type: String,
  },
  nameOrganizer: {
    userName: {
      type: String,
      index: true,
    },
    unsigneduserName: {
      type: String,
      index: true,
    },
  },
  gender: {
    type: Number,
    // enum : [ 1, 2, 3 ],
    default: 3
  },
  email: {
    type: String,
    index: true,
    // validate: {
    //   validator: validator.isEmail,
    //   message: 'Email is not valid'
    // },
    // lowercase: true,
    // unique: true,
    // required: true
  },
  password: {
    type: String,
  },
  // avatar: {
  //   type: String,
  // },
  referral_code: {
    type: String,
  },
  avatar: {
    type: mongoose.Schema.Types.ObjectId,
    ref: GalleryImage
  },
  socialAccount: {
    type: object
  },
  roleId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Role',
  },
  birthday: {
    type: String
  },
  phoneNumber: {
    type: String,
    index: true,
  },
  totalVideo: {
    type: Number
  },
  address: {
    type: String
  },
  description: {
    type: String
  },
  isActive: {
    type: Boolean
  },
  secretInfo: {
    type: Object
  },
  followCount: {
    type: Number,
    default: 0
  },
  followingCount: {
    type: Number,
    default: 1 // default follow bidu
  },
  totalPosts: {
    type: Number,
    default: 0
  },
  receiveNotifications: {
    like: {
      type: Boolean,
      default: true,
    },
    comment: {
      type: Boolean,
      default: true,
    },
    follow: {
      type: Boolean,
      default: true,
    },
    tag: {
      type: Boolean,
      default: true,
    },
    other: {
      type: Boolean,
      default: true,
    },
  },
  isVerified: {
    type: Boolean,
    default: false
  },
  isInterestShowed: {
    type: Boolean,
    default: false
  },
  verificationType: {
    type: Number,
    default: 0 // 1: is BIDU app
  },
  lastActivityAt: {
    type: Date,
    default: new Date() // 1: is BIDU app
  },
  customVerify: {
    type: String /** The badge name with a verified user (e.g. comedian, style guru, CONTENT_CREATOR) */
  },
  favorite_products: {
    type: Array,
    default: []
  },
  email_verify: {
    verified: {
      type: Boolean,
      default: false
    },
    code: {
      type: Number
    },
    expire: {
      type: Date
    }
  },
  phone_verify: {
    verified: {
      type: Boolean,
      default: false
    },
    code: {
      type: Number
    },
    expire: {
      type: Date
    }
  },
  saved_vouchers: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Vouchers'
    }
  ],
  detail_size_history: [
    {
      _id: false,
      fashion_type: {
        type: Number,
        enum: [1, 2, 3, 4, 5] 
      },
      data:[
        {
          _id: false,
          type_size: {
            type: String
          },
          size_infos: [
            {
              _id: false,
              name: {
                type: String
              },
              min: {
                type: Number
              },
              max: { 
                type: Number
              },
              unit: { 
                type: String
              }
            }
          ]
        }
      ]
    }
  ],
  shape_history: [
    {
      _id: false,
      category_id: {
        type: String,
      },
      weight: {
        type: Number,
        default: 0
      },
      height: {
        type: Number,
        default: 0
      },
      width: {
        type: Number,
        default: 0
      },
      length: {
        type: Number,
        default: 0
      },
      time: {
        type: Date
      }
    }
  ],
  category_info_history: {
    type: Object,
    default: null
  },
  bodyMeasurement: {
    type: BodyMeasurementSchema,
  },
  is_newbie: {
    type: Boolean,
    default: true
  },
  shorten_link: {
    type: String,
    default: null
  },
  type_role: {
    type: String,
    default: 'USER'
  },
  group_roles: [{
    type: mongoose.Schema.Types.ObjectId,
  }]
}, {
  versionKey: false,
  timestamps: true
})

const SocialSchema = new mongoose.Schema({
  id: String,
  // email: String,
  socialName: String,
},
  { _id: false }
);
const SocialAccount = mongoose.model('SocialAccount', SocialSchema)

const hash = (user: any, salt: any, next: any) => {
  bcrypt.hash(user.password, salt, (error, newHash) => {
    if (error) {
      return next(error)
    }
    user.password = newHash
    return next()
  })
}

const genSalt = (user: any, SALT_FACTOR: any, next: any) => {
  bcrypt.genSalt(SALT_FACTOR, (err, salt) => {
    if (err) {
      return next(err)
    }
    return hash(user, salt, next)
  })
}

// UserSchema.methods.comparePassword = function (this:UserDoc, passwordAttempt, cb) {
//   bcrypt.compare(passwordAttempt, this.password, (err, isMatch) =>
//     err ? cb(err) : cb(null, isMatch)
//   )
// }

UserSchema.virtual('clientObject').get(function (this: UserDoc) {
  const { _id, nameOrganizer = null, email = null, roleId = null, birthday = null, phoneNumber = null, totalVideo = null, isActive = null, address = null, description = null } = this
  const userName = nameOrganizer && nameOrganizer.userName
  return { _id, userName, email, roleId, birthday, phoneNumber, totalVideo, isActive, address, description }
})

UserSchema.virtual('basicInfoObject').get(function (this: UserDoc) {
  const { _id, nameOrganizer = null, email = null, avatar = null } = this
  const userName = nameOrganizer && nameOrganizer.userName
  return { _id, userName, email, avatar }
})

UserSchema.pre<UserDoc>('save', function (next) {
  const that = this
  if (that.nameOrganizer.userName === null) {
    that.nameOrganizer.userName = null
    that.nameOrganizer.unsigneduserName = null
  } else {
    that.nameOrganizer.userName = that.nameOrganizer.userName
    that.nameOrganizer.unsigneduserName = vietnameseStringToUnicode(that.nameOrganizer.userName)
  }
  next()
})
// comment because runtime error
// UserSchema.index({ 'nameOrganizer.unsigneduserName': 'text' }) // full-text search

UserSchema.plugin(paginate);
UserSchema.plugin(aggregatePaginate);

const User = mongoose.model<UserDoc>("User", UserSchema)
export default User
export { SocialAccount }
