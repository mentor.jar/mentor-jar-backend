import mongoose from 'mongoose'
import { OrderPaymentDoc } from '../interfaces/Order_Payment'
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const Schema = mongoose.Schema;

const PaymentSchema = new mongoose.Schema({
    order_id: { type: Schema.Types.ObjectId, ref: 'Order' },
    user_id: { type: Schema.Types.ObjectId, ref: 'User' },
    shop_id: { type: Schema.Types.ObjectId, ref: 'Shop' },
    price: {
        type: Number
    },
    type: {
        type: String
    },
    code: {
        type: String
    },
    payment_info: {
        type: Object
    }
}, {
    versionKey: false,
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
})

PaymentSchema.plugin(paginate);
PaymentSchema.plugin(aggregatePaginate);

const OrderPayment = mongoose.model<OrderPaymentDoc>("OrderPayment", PaymentSchema)
export default OrderPayment
