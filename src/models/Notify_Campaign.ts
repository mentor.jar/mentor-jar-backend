import mongoose from 'mongoose'
import { ENotifyCampaign } from '../interfaces/NotifyCampaign';
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const Schema = mongoose.Schema;

const NotifyCampaignSchema = new mongoose.Schema({
    type: {
        type: String,
        required: true
    },

    title: String,

    description: String,

    content: {
        type: String,
    },

    receivers: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],

    topic: [String],

    createdBy: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },

    schedule: Object,

    data: Object,

    image: String,
    /**
     * to distinguish app e-commerce/community
     */
    shardingKey: {
        type: String
    },

    timeWillPush: {
        type: Date
    },
    context: Object,
}, {
    versionKey: false,
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
})

NotifyCampaignSchema.plugin(paginate);
NotifyCampaignSchema.plugin(aggregatePaginate);

const NotifyCampaign = mongoose.model<ENotifyCampaign>("NotifyCampaign", NotifyCampaignSchema)
export default NotifyCampaign