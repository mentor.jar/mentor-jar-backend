import mongoose from 'mongoose'
import { ProductDetailInfoDoc } from '../interfaces/Product_Detail_Info'
const paginate = require('./plugins/paginate');

const Schema = mongoose.Schema;

const ProductDetailInfoSchema = new mongoose.Schema({
    value: {
        type: String,
        require: [true, 'Missing value']
    },
    values: {
        type: Array,
        require: [true, 'Missing values']
    },
    product_id: { type: Schema.Types.ObjectId, ref: 'Product' },
    category_info_id: { type: Schema.Types.ObjectId, ref: 'CategoryInfo' }
}, {
    versionKey: false,
    timestamps: true
})
ProductDetailInfoSchema.plugin(paginate);
const ProductDetailInfo = mongoose.model<ProductDetailInfoDoc>("ProductDetailInfo", ProductDetailInfoSchema)
export default ProductDetailInfo