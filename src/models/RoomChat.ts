import mongoose from 'mongoose'
import { RoomChatDoc } from '../interfaces/RoomChat'
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const Schema = mongoose.Schema;

const RoomChatSchema = new Schema({
    name: { type: String },
    creator_id: { type: Schema.Types.ObjectId, ref: 'User', default: null },
    is_group: { type: Boolean, default: false },
    messages: { type: Array, default: [] },
    /**
     * {
     *    id:timestamp
     *    content:string,
     *    type:string, text or image
     *    user_id:string,
     * }
     */
    users: { type: Array, default: [] },
    /**
     * {
     *    _id:string
     *    avatar:string,
     *    name:string,
     *    seen_last_message_index:int
     *    ailas:String
     * }
     */
    file_storages: { type: Array, default: [] },
    /**
     * {
     *    message_id:timestamp
     *    type:string,
     *    url:string,
     *    user_id:String
     * }
     */
    type: { type: String, default: "CHAT" },
    /**
     * {
     *    id:timestamp
     *    content:string,
     *    type:string, text or image
     *    user_id:string,
     * }
     */
    last_message: { type: Object, default: null }
}, {
    versionKey: false,
    timestamps: true
})
RoomChatSchema.statics.const_type = function () {
    return {
        chat: 'CHAT',
        live_stream: 'LIVE STREAM',
    }
}
RoomChatSchema.plugin(paginate);
RoomChatSchema.plugin(aggregatePaginate);
const RoomChat = mongoose.model("RoomChat", RoomChatSchema)
export default RoomChat