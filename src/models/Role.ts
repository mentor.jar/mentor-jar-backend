import mongoose from 'mongoose'
import { RoleDoc } from '../interfaces/Role'
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const RoleSchema = new mongoose.Schema({
    roleName: {
        type: String,
        require: [true, 'Vui lòng nhập tên role']
    },
    description: {
        type: String
    },
    isActive: {
        type: Boolean
    },
    rights: {
        type: Array
    },
}, {
    versionKey: false,
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
})

RoleSchema.plugin(paginate);
RoleSchema.plugin(aggregatePaginate);

const Role = mongoose.model<RoleDoc>("Role", RoleSchema)
export default Role