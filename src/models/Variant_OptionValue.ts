import mongoose from 'mongoose'
import { VariantOptionValueDoc } from '../interfaces/Variant_OptionValue'
const paginate = require('./plugins/paginate');

const Schema = mongoose.Schema;

const VariantOptionValueSchema = new mongoose.Schema({
    variant_id: { type: Schema.Types.ObjectId, ref: 'Variant' },
    option_value_id: { type: Schema.Types.ObjectId, ref: 'OptionValue' }
}, {
    versionKey: false,
    timestamps: true
})

VariantOptionValueSchema.plugin(paginate);

const VariantOptionValue = mongoose.model<VariantOptionValueDoc>("VariantOptionValue", VariantOptionValueSchema)
export default VariantOptionValue