import mongoose from 'mongoose'
import { PromotionProgramDoc } from '../interfaces/PromotionProgram'
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const Schema = mongoose.Schema;

const PromotionProgramSchema = new mongoose.Schema({
    name: {
        type: String,
        require: [true, 'Vui lòng nhập tên chương trình khuyến mãi']
    },
    start_time: {
        type: Date,
        require: [true, 'Vui lòng nhập thời gian bắt đầu'],
    },
    end_time: {
        type: Date,
        require: [true, 'Vui lòng nhập thời gian kết thúc'],
    },
    limit_quantity: {
        type: Number,
        require: [true, 'Vui lòng nhập số lượng tối đa có thể bán cho từng sản phẩm']
    },
    discount: {
        type: Number,
    },
    products: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Products'
        }
    ],
    shop_id: {
        type: Schema.Types.ObjectId,
        ref: 'Shops'
    },
    is_valid: {
        type: Boolean,
        default: true
    },
    is_reset: {
        type: Boolean,
        default: false
    }
}, {
    versionKey: false,
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
})

PromotionProgramSchema.statics.relationship = function () {

}

PromotionProgramSchema.plugin(paginate);
PromotionProgramSchema.plugin(aggregatePaginate);

const PromotionProgram = mongoose.model<PromotionProgramDoc>("PromotionProgram", PromotionProgramSchema)
export default PromotionProgram