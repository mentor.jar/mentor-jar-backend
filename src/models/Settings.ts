import mongoose from 'mongoose'
import { ShopDoc } from '../interfaces/Shop'

const SettingsSchema = new mongoose.Schema({
    key: {
        type: String
    },
    value: [{
        type: String
    }],
}, {
    versionKey: false,
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
})

const Setting = mongoose.model<ShopDoc>("Setting", SettingsSchema)
export default Setting