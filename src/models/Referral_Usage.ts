import mongoose from 'mongoose';
import { ReferralUsageDoc } from '../interfaces/ReferralUsage';
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const Schema = mongoose.Schema;

const ReferralUsageSchema = new mongoose.Schema({
    user_id: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        default: null
    },
    referred_id: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        default: null
    },
    code: {
        type: String,
    }
}, {
    versionKey: false,
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
})

ReferralUsageSchema.plugin(paginate);
ReferralUsageSchema.plugin(aggregatePaginate);
const ReferralUsage = mongoose.model<ReferralUsageDoc>("ReferralUsage", ReferralUsageSchema)
export default ReferralUsage