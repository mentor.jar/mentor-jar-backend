import mongoose from 'mongoose'
import { OrderItemDoc } from '../interfaces/Order_Item'
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const Schema = mongoose.Schema;

const OrderItemSchema = new mongoose.Schema({
    order_id: { type: Schema.Types.ObjectId, ref: 'Order', index: true },
    quantity: {
        type: Number,
        default: 1
    },
    variant: { type: Object },
    product_id: { type: Schema.Types.ObjectId, ref: 'Product', index: true},
    product: { type: Object },
    images: { type: Array },
}, {
    versionKey: false,
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
})

OrderItemSchema.plugin(paginate);
OrderItemSchema.plugin(aggregatePaginate);

const OrderItem = mongoose.model<OrderItemDoc>("OrderItem", OrderItemSchema)
export default OrderItem