import mongoose from 'mongoose'
import { CartItemDoc } from '../interfaces/CartItem'
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const Schema = mongoose.Schema;

const CartItemSchema = new mongoose.Schema({
    quantity: {
        type: Number
    },
    user_id: { type: Schema.Types.ObjectId, ref: 'User' },
    variant_id: { type: Schema.Types.ObjectId, ref: 'Variant' },
    product_id: { type: Schema.Types.ObjectId, ref: 'Product' },
    shop_id: { type: Schema.Types.ObjectId, ref: 'Shop' },
}, {
    versionKey: false,
    timestamps: true
})
CartItemSchema.plugin(paginate);
CartItemSchema.plugin(aggregatePaginate);

CartItemSchema.statics.relationship = function () {
    const shop = () => (
        {
            type: 'belongTo',
            table: 'shops',
            foreign_key: 'shop_id',
            as: 'shop'
        }
    )

    const product = () => (
        {
            type: 'belongTo',
            table: 'products',
            foreign_key: 'product_id',
            as: 'product',
            child: {
                type: "lookup",
                unwind: false,
                query: {
                    from: "variants",
                    let: {
                        product_id: "$_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        '$expr': {
                                            $and: [
                                                {
                                                    $eq: ["$product_id", "$$product_id"]
                                                }
                                            ]
                                        },
                                    }
                                ]
                            }
                        }
                    ],
                    as: "variants"
                }
            }
        }
    )

    return { shop, product }
}

const Shop = mongoose.model<CartItemDoc>("CartItem", CartItemSchema)
export default Shop