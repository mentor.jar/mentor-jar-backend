import mongoose from 'mongoose'
import { StreamSessionDoc } from '../interfaces/Stream_Session'
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const Schema = mongoose.Schema;

const StreamSessionSchema = new mongoose.Schema({
    session_number: {
        type: String,
        default: Date.now()
    },
    stream_id: {
        type: Schema.Types.ObjectId,
        ref: 'Stream'
    },
    shop_id: {
        type: Schema.Types.ObjectId,
        ref: 'Shop'
    },
    user_id: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    room_chat_id: {
        type: Schema.Types.ObjectId,
        ref: 'RoomChat'
    },
    name: {
        type: String
    },
    image: { type: String, default: null },
    type: {
        type: String
    },
    type_of_viewer: {
        type: String
    },
    // PENDING, APPROVED, REJECTED
    is_approved: {
        type: String,
        default: "approved"
    },
    status: {
        type: String,
        default: "is_coming"
    },
    list_id_product: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Product'
        }
    ],
    view: {
        type: Number,
        default: 0
    },
    heart: {
        type: Number,
        default: 0
    },
    pin_message: {
        type: Object,
        default: null
    },
    time_will_start: {
        type: Date,
        default: new Date()
    },
    time_start: {
        type: Date,
        default: null
    },
    time_end: {
        type: Date,
        default: null
    },
    active: {
        type: Boolean,
        default: false
    },
    archive_links: {
        type: Array,
        default: []
    },
    check_disconnect: {
        type: Number,
        default: 1
    },
    view_max: {
        type: Number,
        default: 1
    }
}, {
    versionKey: false,
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
})

StreamSessionSchema.statics.relationship = function () {
    const stream = () => (
        {
            type: 'belongTo',
            table: 'streams',
            foreign_key: 'stream_id',
            as: 'stream'
        }
    )
    const shop = () => (
        {
            type: 'belongTo',
            table: 'shops',
            foreign_key: 'shop_id',
            as: 'shop'
        }
    )
    return { stream, shop }
}

StreamSessionSchema.plugin(paginate);
StreamSessionSchema.plugin(aggregatePaginate);

const streamSession = mongoose.model("StreamSession", StreamSessionSchema)
export default streamSession