import mongoose from 'mongoose'
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const Schema = mongoose.Schema;

const ReportLivestreamSchema = new mongoose.Schema({
    user_report_id: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    stream_session_id: {
        type: Schema.Types.ObjectId,
        ref: 'StreamSession'
    },
    owner_id: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    time_report: {
        type: Date
    },
    reasons: {
        type: Array,
        default: null
    },
    is_solved: {
        type: Boolean,
        default: false
    }
}, {
    versionKey: false,
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
})

ReportLivestreamSchema.plugin(paginate);
ReportLivestreamSchema.plugin(aggregatePaginate);

const ReportLivestream = mongoose.model("ReportLivestream", ReportLivestreamSchema)
export default ReportLivestream