import mongoose from 'mongoose'
// import validator from 'validator'
// import { object } from 'mongoose/lib/utils'
import { ShopDoc } from '../interfaces/Shop'
import { ObjectId } from '../utils/helper';
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const Schema = mongoose.Schema;

const BankInfoSchema = new mongoose.Schema({
    bank_name: {
        type: String,
        require: [true,'Vui lòng chọn ngân hàng']
    },
    bank_number: {
        type: String,
        require: [true, 'Vui lòng nhập số tài khoản']
    },
}, {
    _id: false
})

const RefundObjectSchema = new mongoose.Schema({
    code: Number,
    vn_name: String,
    ko_name: String,
    en_name: String,
    active: {
        type: Boolean,
        default: false
    },
    _id: false
})

const RankPolicySchema = new mongoose.Schema({
    name: {
        type: String,
        require: false,
        default: null
    },
    data: {
        type: Object,
        require: false,
        default: {}
    }
}, {
    _id: false
})

const ImagesSchema = new mongoose.Schema({
    _id: false,
    vi: String,
    en: String,
    ko: String,
})

const VariantSchema = new mongoose.Schema({
    variant_id: {
        type: ObjectId
    },
    discount: {
        type: Number
    },
    group_buy_price: {
        type: Number,
    },
    quantity: {
        type: Number
    },
    _id: false
})

const GroupSchema = new mongoose.Schema({
    number_of_member: {
        type: Number,
        default: 2
    },
    discount: {
        type: Number
    },
    group_buy_price: {
        type: Number
    },
    quantity: {
        type: Number
    },
    variants: [VariantSchema]
})

const ComparePriceGroupBuySchema = new mongoose.Schema({
    url: {
        type: String,
        require: false,
        default: ''
    },
    price: {
        type: Number,
        require: true
    }
}, {
    _id: false
})

const ProductGroupBuySchema = new mongoose.Schema({
    image: {
        type: String,
        default: ''
    },
    quantity: {
        type: Number,
        default: 0
    },
    variant_quantity: {
        type: Number,
        default: 0
    },
    groups: [GroupSchema],
    compare_price: {
        type: ComparePriceGroupBuySchema,
        require: false
    }
})

const GroupBuySchema = new mongoose.Schema({
    name: {
        type: String,
        default: '',
    },
    start_time: {
        type: Date,
        require: [true, 'Vui lòng nhập thời gian bắt đầu'],
    },
    end_time: {
        type: Date,
        require: [true, 'Vui lòng nhập thời gian kết thúc'],
    },
    status: {
        type: String,
        enum: ['pending', 'reject', 'approved'],
        default: 'pending'
    },
    discount_type: {
        type: String,
        enum: ['price', 'percent'],
        default: 'price'
    },
    products: [ProductGroupBuySchema]
})

const SystemBanner = new mongoose.Schema({
    _id: false,
    images: ImagesSchema,
    name: {
        type: String,
        default: ''
    },
    is_active: {
        type: Boolean,
        default: true,
    },
    id: {
        type: String
    }
})

const ShopSchema = new mongoose.Schema({
    name: {
        type: String
    },
    description: {
        type: String
    },
    shop_type: {
        type: Number,
        default: 0
    },
    refund_money_mode: {
        type: Boolean,
        default: false
    },
    refund_money_regulations: {
        type: Object,
        default: null
    },
    pause_mode: {
        type: Boolean,
        default: false
    },
    user_id: { type: Schema.Types.ObjectId, ref: 'User', index: true },
    country: { type: String, default: null},
    shipping_methods: [{
        
        shipping_method_id: {
            type: ObjectId
        },
        name: {
            type: String
        },
        is_active: {
            type: Boolean
        },
        token: {
            type: String
        },
        pick_address: {
            type: String
        },
        code: { // id shop BIDU on ghtk
            type: String
        },
        name_query: {
            type: String
        },
    }],
    refund_conditions: [RefundObjectSchema],
    is_approved: {
        type: Boolean,
        default: false
    },
    ranking_today: {
        type: Number,
        default: 99999
    },
    ranking_yesterday: {
        type: Number,
        default: 99999
    },
    avg_rating: {
        type: Number,
        default: 5.0
    },
    shorten_link: {
        type: String,
        default: null
    },
    allow_show_on_top: {
        type: Boolean,
        default: true
    },
    rank_policy: {
        type: RankPolicySchema,
        require: false
    },
    ranking_criteria: {
        type: Object,
        require: false
    },
    system_banner: SystemBanner,
    middle_banner: {
        type: String,
        default: null
    },
    biggest_price: {
        type: Number,
        default: 0
    },
    group_buy_list: [GroupBuySchema],
    bank_info: {
        type: BankInfoSchema,
    },
}, {
    versionKey: false,
    timestamps: true
})

ShopSchema.statics.relationship = function () {
    const users = () => (
        {
            type: 'belongTo',
            table: 'users',
            foreign_key: 'user_id',
            as: 'user',
            child: {
                type: "lookup",
                unwind: true,
                query: {
                    from: "galleryimages",
                    let: {
                        user_id: "$_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                '$expr': {
                                    $and: [
                                        {
                                            $eq: ["$galleryImageAbleId", "$$user_id"]
                                        },
                                        {
                                            $eq: ["$galleryImageAbleType", "1"]
                                        }
                                    ]
                                },
                            }
                        }
                    ],
                    as: "gallery_image"
                }
            }    
        }
    )

    const banners = () => ({
        type: 'hasMany',
        table: 'ebanners',
        foreign_key: 'shop_id',
        as: 'banners',
    })
    return { users, banners }
}
ShopSchema.statics.shop_type = function () {
    return {
        0: 'Cá Nhân',
        1: 'Doanh Nghiệp',
    }
}
ShopSchema.plugin(paginate);
ShopSchema.plugin(aggregatePaginate);

const Shop = mongoose.model<ShopDoc>("Shop", ShopSchema)
export default Shop