import mongoose from 'mongoose'
import { EmailListDoc } from '../interfaces/EmailList'
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const EmailStatusSchema = new mongoose.Schema({
    target: {
        type: String
    },
    success: {
        type: Boolean,
        default: false
    },
    message: {
        type: String,
        default: ''
    }
}, {
    _id: false
})

const EmailListSchema = new mongoose.Schema({
    name: {
        type: String,
        require: [true, 'Missing email template name']
    },
    sendgrid_template_id: {
        type: String,
        require: [true, 'Missing sengrid template ID']
    },
    data: {
        type: Object,
        require: false,
        default: null
    },
    status: [EmailStatusSchema],
    deleted_at: {
        type: Date,
        require: false,
        default: null
    }
}, {
    versionKey: false,
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
})

EmailListSchema.plugin(paginate);
EmailListSchema.plugin(aggregatePaginate);

const EmailList = mongoose.model<EmailListDoc>("EmailList", EmailListSchema)
export default EmailList