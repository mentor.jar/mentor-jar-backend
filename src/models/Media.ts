import mongoose from 'mongoose'
// import validator from 'validator'
// import { object } from 'mongoose/lib/utils'
const paginate = require('./plugins/paginate');

const Schema = mongoose.Schema;

/**
 * This model has association with multy other model through accessible_id and accessible_type 
 */
const EMediaSchema = new mongoose.Schema({
    accessible_type: {
        type: String
    },
    accessible_id: {
        type: String
    },
    path: {
        type: String,
        require: [true, 'Path of image is not valid']
    },
    full_path: {
        type: String,
        require: [true, 'Path of image is not valid']
    },
    mimetype: {
        type: String,
        require: [true, 'Missing image mimetype']
    },
    file_name: {
        type: String,
        require: [true, 'Missing image file_name']
    },
    encoding: {
        type: String,
        require: [true, 'Missing image encoding']
    },
    driver_type: {
        type: String,
        default: 'Local'
    }
}, {
    versionKey: false,
    timestamps: true
})
EMediaSchema.plugin(paginate);
const EMedia = mongoose.model("EImage", EMediaSchema)
export default EMedia