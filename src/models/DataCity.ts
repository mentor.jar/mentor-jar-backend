import mongoose from 'mongoose'
// import validator from 'validator'
// import { object } from 'mongoose/lib/utils'
import { DataCityDoc } from '../interfaces/DataCity'
const paginate = require('./plugins/paginate');

const Schema = mongoose.Schema;

const DataCitySchema = new mongoose.Schema({
    Id: {
        type: String
    },
    Name: {
        type: String
    },
    Districts: { type: Array, default: [] },
    id_vtp: { type: Number }
}, {
    versionKey: false,
    timestamps: true
})

DataCitySchema.plugin(paginate);
const DataCity = mongoose.model<DataCityDoc>("DataCity", DataCitySchema)
export default DataCity