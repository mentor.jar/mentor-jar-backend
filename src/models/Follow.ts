import mongoose from 'mongoose'
import { FollowDoc } from '../interfaces/Follow'

const Schema = mongoose.Schema;

const FollowSchema = new mongoose.Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    followerId: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    actionFollow: {
        type: String
    },
    followedDate: {
        type: Date
    }
}, {
    versionKey: false,
    timestamps: true
})

const Follow = mongoose.model<FollowDoc>("Follow", FollowSchema)
export default Follow