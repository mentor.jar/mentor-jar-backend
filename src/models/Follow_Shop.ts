import mongoose from 'mongoose'
import { FollowShopDoc } from '../interfaces/Follow_Shop'

const Schema = mongoose.Schema;

const FollowShopSchema = new mongoose.Schema({
    user_id: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    shop_id: {
        type: Schema.Types.ObjectId,
        ref: 'Shop'
    },
}, {
    versionKey: false,
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
})

const Shop = mongoose.model<FollowShopDoc>("FollowShop", FollowShopSchema)
export default Shop