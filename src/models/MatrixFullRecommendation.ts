import mongoose from 'mongoose';
import { EJobHistory } from '../interfaces/JobsHistory';
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const Schema = mongoose.Schema;

const EJobsHistorySchema = new mongoose.Schema({
    jobName: {
        type: String,
        default: null
    },
    objectType: {
        type: String,
        default: null
    },
    objectIds: [{
        type: Schema.Types.ObjectId,
    }],
    affectedType: {
        type: String,
        default: null
    },
    affectedIds: [{
        type: Schema.Types.ObjectId,
    }],
}, {
    versionKey: false,
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
})

EJobsHistorySchema.plugin(paginate);
EJobsHistorySchema.plugin(aggregatePaginate);

const JobHistory = mongoose.model<EJobHistory>("EJobHistory", EJobsHistorySchema)
export default JobHistory;
