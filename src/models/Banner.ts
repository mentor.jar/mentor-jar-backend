import mongoose from 'mongoose';
import { EBanner } from '../interfaces/Banner';
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const Schema = mongoose.Schema;

const ImageSchema = new mongoose.Schema({ _id: false, top: String, middle: String, detail: String,  lang: String});
const ActionBannerSchema = new mongoose.Schema({
    shop: { // for classify = shop_view
        type: Schema.Types.ObjectId,
        ref: 'Shop'
    },
    shops: [{ // for classify = shop_list
        type: Schema.Types.ObjectId,
        ref: 'Shop'
    }],
    category: { // for classify = category_view
        type: Schema.Types.ObjectId,
        ref: 'ECategory'
    },
    _id: false
})

const EBannerSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Please add a banner name']
    },
    image: String,
    images_web: {
        type: 
            {
            vi: {
                full: {
                    type: String
                },
                responsive: {
                    type: String
                }
            }, 
            ko: {
                full: {
                    type: String
                },
                responsive: {
                    type: String
                }
            }
        }
    },
    images: [ImageSchema],
    promo_link: {
        type: String,
        default: ''
    },
    products: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Product'
        }
    ],
    order: {
        type: Number,
        default: null
    },
    is_active: {
        type: Boolean,
        default: true
    },
    start_time: Date,
    shop_id: {
        type: Schema.Types.ObjectId,
        ref: 'Shop'
    },
    end_time: Date,
    classify: {
        type: String,
        enum: ["product", "voucher", "link", "none", 'shop_view', 'list_shop', 'top_shop', 'category_view'],
        default: "none"
    },
    position: {
        type: String,
        enum: ["top", "middle", "other"],
        default: "other" 
    },
    vouchers: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Voucher'
        }
    ],
    advance_actions: {
        type: ActionBannerSchema
    },
    priority: {
        type: Number,
        default: 1
    },
    shorten_link: {
        type: String,
        default: ''
    },
    description: {
        type: String,
        default: ''
    }
}, {
    versionKey: false,
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
})

EBannerSchema.plugin(paginate);
EBannerSchema.plugin(aggregatePaginate);

const Banner = mongoose.model<EBanner>("EBanner", EBannerSchema)
export default Banner;
