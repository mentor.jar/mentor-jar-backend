import mongoose, { Schema } from 'mongoose'
import { FollowDoc } from '../interfaces/Follow'

const SearchKeywordSchema = new mongoose.Schema({
    keyword: {
        type: String,
        require: true
    },
    count_number: {
        type: Number,
        default: 1
    },
    shop_id: { type: Schema.Types.ObjectId, ref: 'Shop' },
}, {
    versionKey: false,
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
})

const SearchKeyword = mongoose.model<FollowDoc>("SearchKeyword", SearchKeywordSchema)
export default SearchKeyword