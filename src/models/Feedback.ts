import mongoose from 'mongoose'
import { FeedbackDoc } from '../interfaces/Feedback'
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const Schema = mongoose.Schema;

const shopFeedbackSchema = new mongoose.Schema({
    content: {
        type: String,
        require: [true, "Vui lòng nhập nội dung phản hồi"]
    },
    medias : [
        {
            type: String
        }
    ],
}, {
    versionKey: false,
    timestamps: true
})

const FeedbackSchema = new mongoose.Schema({
    content: {
        type: String,
        require: [true, "Vui lòng nhập nội dung phản hồi"]
    },
    vote_star: {
        type: Number,
        require: [true, 'Vui lòng chọn số sao đánh giá']
    },
    medias: [
        {
            type: String
        }
    ],
    target_type: {
        type: String,
        require: [true, 'Vui lòng chọn loại đối tượng của phản hồi'],
    },
    target_id: {
        type: Schema.Types.ObjectId,
        require: [true, "Vui lòng chọn ID đối tượng của phản hồi"]
    },
    is_approved: {
        type: Boolean,
        default: true
    },
    is_public: {
        type: Boolean,
        default: true
    },
    is_show_body_shape: {
        type: Boolean,
        default: true
    },
    shop_feedback: shopFeedbackSchema,
    buyer_feedback: {
        type: Object,
        default: {}
    },
    user_id: { type: Schema.Types.ObjectId, ref: 'User' },
    shop_id: { type: Schema.Types.ObjectId, ref: 'Shop' },
    order_id: { type: Schema.Types.ObjectId, ref: 'Order' },
    user_liked: { type: Array, default: []}
}, {
    versionKey: false,
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
})

FeedbackSchema.statics.relationship = function () {
    const shop = () => (
        {
            type: 'belongTo',
            table: 'shops',
            foreign_key: 'shop_id',
            as: 'shop'
        }
    )

    const user = () => (
        {
            type: 'belongTo',
            table: 'users',
            foreign_key: 'user_id',
            as: 'user',
            child: {
                type: "lookup",
                unwind: true,
                query: {
                    from: "galleryimages",
                    let: {
                        user_id: "$_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                '$expr': {
                                    $and: [
                                        {
                                            $eq: ["$galleryImageAbleId", "$$user_id"]
                                        },
                                        {
                                            $eq: ["$galleryImageAbleType", "1"]
                                        }
                                    ]
                                },
                            }
                        }
                    ],
                    as: "gallery_image"
                }
            }    
        }
    )

    return { shop, user }
}

FeedbackSchema.plugin(paginate);
FeedbackSchema.plugin(aggregatePaginate);

const Feedback = mongoose.model<FeedbackDoc>("Feedback", FeedbackSchema)
export default Feedback