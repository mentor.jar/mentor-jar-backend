import mongoose from 'mongoose'
import { PaymentMethodDoc } from '../interfaces/Payment_Method'
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const Schema = mongoose.Schema;

const PaymentMethodSchema = new mongoose.Schema({
    name: {
        type: String,
        require: [true, 'Vui lòng nhập tên phương thức thanh toán']
    },
    country: { type: String, default: "VN"},
    option: {
        type: Object
    },
    name_query: {
        type: String
    },
    is_active: {
        type: Boolean,
    }

})

PaymentMethodSchema.plugin(paginate);
PaymentMethodSchema.plugin(aggregatePaginate);

const PaymentMethod = mongoose.model<PaymentMethodDoc>("PaymentMethod", PaymentMethodSchema)
export default PaymentMethod