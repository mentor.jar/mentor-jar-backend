import mongoose, {Schema} from 'mongoose'
import { AppFeedbackDoc } from '../interfaces/AppFeedback'
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const AppFeedbackSchema = new mongoose.Schema({
    full_name: {
        type: String,
        require: true
    },
    email: {
        type: String,
        require: true
    },
    how_you_know_bidu: {
        type: Array,
        require: true
    },
    satisfied_with_bidu_mark: {
        type: Number,
        require: true,
        enum: [0,1,2,3,4,5]
    },
    ui_mark: {
        type: Number,
        require: true,
        enum: [0,1,2,3,4,5]
    },
    shop_config_mark: {
        type: Number,
        require: true,
        enum: [0,1,2,3,4,5]
    },
    product_manage_mark: {
        type: Number,
        require: true,
        enum: [0,1,2,3,4,5]
    },
    livestream_mark: {
        type: Number,
        require: true,
        enum: [0,1,2,3,4,5]
    },
    order_manage_mark: {
        type: Number,
        require: true,
        enum: [0,1,2,3,4,5]
    },
    opinion: {
        type: String,
        require: true,
    },
    user_id: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        require: false,
        default: null
    }
}, {
    versionKey: false,
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
})

AppFeedbackSchema.plugin(paginate);
AppFeedbackSchema.plugin(aggregatePaginate);

const AppFeedback = mongoose.model<AppFeedbackDoc>("AppFeedback", AppFeedbackSchema)
export default AppFeedback