import mongoose from 'mongoose';
import { TrackingObjectDoc } from '../interfaces/Tracking_Object';
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const Schema = mongoose.Schema;

const TrackingObjectSchema = new mongoose.Schema({
    user_id: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    target_type: {
        type: String
    },
    target_id: {
        type: Schema.Types.ObjectId
    },
    ad_hoc: {
        type: String,
        enum: ['ORDER_MANAGE', 'SETTING', 'PRODUCT_MANAGE', 'OTHER'],
        default: 'OTHER'
    },
    is_read: {
        type: Boolean,
        default: false
    }
}, {
    versionKey: false,
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
})

TrackingObjectSchema.plugin(paginate);
TrackingObjectSchema.plugin(aggregatePaginate);

const TrackingObject = mongoose.model<TrackingObjectDoc>("TrackingObject", TrackingObjectSchema)
export default TrackingObject