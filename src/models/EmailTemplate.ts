import mongoose from 'mongoose'
import { EmailTemplateDoc } from '../interfaces/EmailTemplate'
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const variableSchema = new mongoose.Schema({
    name: {
        type: String
    },
    sign: {
        type: String
    },
    type: {
        type: String,
        default: 'string',
        enum: ['string', 'number', 'list', 'image']
    }
})

const EmailTemplateSchema = new mongoose.Schema({
    name: {
        type: String,
        require: [true, 'Missing email template name']
    },
    sendgrid_template_id: {
        type: String,
        require: [true, 'Missing sengrid template ID']
    },
    content: {
        type: String,
        require: [true, 'Missing sengrid template content']
    },
    variables: [variableSchema],
    deleted_at: {
        type: Date,
        require: false,
        default: null
    }
}, {
    versionKey: false,
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
})

EmailTemplateSchema.plugin(paginate);
EmailTemplateSchema.plugin(aggregatePaginate);
EmailTemplateSchema.index({
    sendgrid_template_id: 1,
    deleted_at: 1
}, {
    unique: true
})

const EmailTemplate = mongoose.model<EmailTemplateDoc>("EmailTemplate", EmailTemplateSchema)
export default EmailTemplate