import mongoose from 'mongoose';
import { OrderDoc } from '../interfaces/Order';
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const Schema = mongoose.Schema;

const PickStatusSchema = new mongoose.Schema({ code: Number, vn_name: String, ko_name: String, en_name: String, time: Date });

const OrderSchema = new mongoose.Schema(
    {
        order_number: {
            type: String,
            default: Date.now(),
        },
        total_price: {
            type: Number,
            require: [true, 'Vui lòng tính tổng số tiền đơn hàng'],
        },
        payment_status: {
            type: String,
            require: [true, 'Vui lòng chọn trạng thái thanh toán'],
            enum: ['pending', 'paid'],
            default: 'pending',
        },
        shipping_status: {
            type: String,
            require: [true, 'Vui lòng chọn trạng thái vận chuyển'],
            enum: [
                'pending',
                'wait_to_pick',
                'shipping',
                'shipped',
                'canceled',
                'canceling',
                'return'
            ],
            default: 'pending',
        },
        payment_method_id: {
            type: Schema.Types.ObjectId,
            ref: 'PaymentMethod',
        },
        shipping_method_id: {
            type: Schema.Types.ObjectId,
            ref: 'ShippingMethod',
        },
        user_id: { type: Schema.Types.ObjectId, ref: 'User' },
        handle_by: { type: Schema.Types.ObjectId, ref: 'User' },
        shop_id: { type: Schema.Types.ObjectId, ref: 'Shop' },
        address: { type: Object },
        pick_address: { type: Object },
        vouchers: [
            {
                type: Object,
            },
        ],
        total_value_items: {
            type: Number,
            require: [true, 'Vui lòng thêm tổng tiền các mặt hàng'],
        },
        shipping_fee: {
            type: Number,
            require: [true, 'Vui lòng thêm phí vận chuyển'],
        },
        shipping_discount: {
            type: Number,
            require: [true, 'Vui lòng thêm số tiền giảm giá phí vận chuyển'],
        },
        voucher_discount: {
            type: Number,
            require: [true, 'Vui lòng thêm số tiền giảm giá của mã khuyến mãi'],
        },
        note: {
            type: Object,
        },
        group_order_id: { type: Schema.Types.ObjectId, ref: 'GroupOrder' },
        deleted_at: {
            type: Date,
        },
        cancel_reason: {
            type: String,
            default: null
        },
        cancel_type: {
            type: String,
            enum: [
                null,
                'system',
                'seller',
                'buyer',
            ],
            default: null,
        },
        cancel_by: {
            type: Schema.Types.ObjectId,
            ref: "User",
            default: null
        },
        cancel_time: {
            type: Date,
            default: null,
        },
        seller_note: {
            type: String,
            default: null,
        },
        approved_time: {
            type: Date,
            default: null,
        },
        pick_status: [PickStatusSchema],
        refund_information: [
            {
                _id: false,
                item_id: {
                    type: Schema.Types.ObjectId,
                    ref: "OrderItem",
                    default: null
                },
                refund_quantity: {
                    type: Number,
                    default: 0
                },
                status: {
                    type: String,
                    enum: ['opening', 'canceled', 'closed'],
                    default: 'opening'
                },
                opening_time: {
                    type: Date,
                    default: null,
                },
                canceled_time: {
                    type: Date,
                    default: null,
                },
                closed_time: {
                    type: Date,
                    default: null,
                },
                request_info: {
                    address: { 
                        type: Object,
                        default: null,
                    },
                    reasons: {
                        type: Array,
                        default: []
                    },
                    images: [ String ],
                    video: {
                        type: String,
                        default: null
                    },
                    note: { 
                        type: String,
                        default: null,
                    },
                    manner: {
                        type: String,
                        default: null,
                    },
                    email: {
                        type: String,
                        default: null,
                    },	
                    phone_number: { 
                        type:String,
                        default: null,
                    },
                    time: {
                        type: Date,
                        default: null,
                    }
                },
                shop_review: {
                    status: {
                        type: String,
                        enum: ['pending', 'approved', 'rejected'],
                        default: 'pending'
                    },
                    reasons: {
                        type: Array,
                        default: []
                    },
                    images: [ String ],
                    video: {
                        type: String,
                        default: null
                    },
                    note: {
                        type: String,
                        default: null,
                    },
                    email: {
                        type: String,
                        default: null,
                    },	
                    time: {
                        type: Date,
                        default: null,
                    },
                    seller_confirm_received_time:  {
                        type: Date,
                        default: null
                    }
                },
                admin_review: {
                    status: {
                        type: String,
                        enum: ['pending', 'approved', 'rejected'],
                        default: 'pending'
                    },
                    reasons: [ String ],
                    images: [ String ],
                    video: {
                        type: String,
                        default: null
                    },
                    note: {
                        type: String,
                        default: null,
                    },	
                    time: {
                        type: Date,
                        default: null,
                    }
                },
                refund_money: {
                    status: {
                        type: String,
                        enum: ['none', 'pending', 'transferring', 'completed'],
                        default: 'none'
                    },
                    value: {
                        type: Number,
                        default: 0
                     },
                    completed_time: {
                        type: Date,
                        default: null
                    }
                }
            }
        ],
        is_mark_penalty: {
            type: Boolean,
            require: false,
            default: false
        },
        order_service: { // order service name of shipment
            type: String,
            default: null
        },
        is_group_buy_order: {
            type: Boolean,
            default: false
        }
    },
    {
        versionKey: false,
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at',
        },
    }
);

OrderSchema.statics.relationship = function () {
    const shops = () => ({
        type: 'belongTo',
        table: 'users',
        foreign_key: 'user_id',
        as: 'user',
    });
    return { shops };
};

OrderSchema.plugin(paginate);
OrderSchema.plugin(aggregatePaginate);
OrderSchema.index({
    created_at: -1
})

const Order = mongoose.model<OrderDoc>('Order', OrderSchema);
export default Order;
