import mongoose from 'mongoose'
import { ShippingMethodDoc } from '../interfaces/Shipping_Method'
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const Schema = mongoose.Schema;

const ShippingMethodSchema = new mongoose.Schema({
    name: {
        type: String,
        require: [true, 'Vui lòng nhập tên phương thức thanh toán']
    },
    type: {
        type: String,
        enum: ['custom', 'third_party'],
        default: 'custom'
    },
    country: { type: String, default: "VN"},
    shipping_fee: {
        type: Number,
        default: true
    },
    option: {
        type: Object
    },
    name_query: {
        type: String
    }

})

ShippingMethodSchema.plugin(paginate);
ShippingMethodSchema.plugin(aggregatePaginate);

const ShippingMethod = mongoose.model<ShippingMethodDoc>("ShippingMethod", ShippingMethodSchema)
export default ShippingMethod