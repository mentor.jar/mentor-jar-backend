export enum TargetType {
    ORDER = 'ORDER'
}

export enum AdHoc {
    ORDER_MANAGE = 'ORDER_MANAGE',
    SETTING = 'SETTING',
    PRODUCT_MANAGE = 'PRODUCT_MANAGE',
    OTHER = 'OTHER',
}