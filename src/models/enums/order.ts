export enum PaymentStatus {
    PENDING = 'pending',
    PAID = 'paid',
}
export enum ShippingStatus {
    PENDING = 'pending',
    WAIT_TO_PICK = 'wait_to_pick',
    SHIPPING = 'shipping',
    SHIPPED = 'shipped',
    CANCELED = 'canceled',
    CANCELING = 'canceling',
    // RETURNING = 'returning',
    // RETURNED = 'returned',
    RETURN = 'return'
}
export enum ShippingMethodQuery {
    GIAOHANGTIETKIEM = "GIAOHANGTIETKIEM",
    VIETTELPOST = "VIETTELPOST",
}

export enum CancelType {
    SYSTEM = "system",
    SELLER = "seller",
    BUYER = "buyer"
}

export enum GHTKStatusAction {
    PICKED_UP = 3, // Đã lấy hàng/Đã nhập kho
    CANCELED = -1, // Hủy đơn hàng
    CAN_NOT_PICK = 7, // Không lấy được hàng
    CHECKED = 6, // Đã đối soát
    RECEIVED = 2, // Đã tiếp nhận
    ASSIGN = 12, // Đã điều phối lấy hàng/Đang lấy hàng
    DELAY_PICK_UP = 8, // Hoãn lấy hàng
    NOT_RECEIVED = 1, // Chưa tiếp nhận
    DELIVERIED = 5, // Đã giao hàng/Chưa đối soát
    DELIVERING = 4, // Đã điều phối giao hàng/Đang giao hàng
    DELAY_DELIVER = 10, // Delay giao hàng
    UNABLE_TO_DELIVER = 9, // 	Không giao được hàng
    CHECKED_AND_RETURNED = 11, // 	Đã đối soát công nợ trả hàng
    RETURN_TO_SHOP = 20, // Đang trả hàng,
    RETURNED_TO_SHOP = 21, // Đã trả hàng,
}

export const GHTK_STATUS_NOTIFY = [
    GHTKStatusAction.CANCELED, 
    GHTKStatusAction.PICKED_UP,
    GHTKStatusAction.CAN_NOT_PICK,
    GHTKStatusAction.DELIVERING,
    GHTKStatusAction.DELAY_DELIVER,
    GHTKStatusAction.UNABLE_TO_DELIVER,
    GHTKStatusAction.DELIVERIED,
]

export const GHTK_SELLER_STATUS_NOTIFY = [
    GHTKStatusAction.CANCELED, 
    GHTKStatusAction.PICKED_UP,
    GHTKStatusAction.DELIVERING,
    GHTKStatusAction.RETURN_TO_SHOP,
    GHTKStatusAction.UNABLE_TO_DELIVER,
    GHTKStatusAction.DELIVERIED,
    GHTKStatusAction.CAN_NOT_PICK,
]

export const GHTK_BUYER_STATUS_NOTIFY = [
    GHTKStatusAction.CANCELED, 
    GHTKStatusAction.DELIVERING,
    GHTKStatusAction.UNABLE_TO_DELIVER,
    GHTKStatusAction.DELIVERIED,
    GHTKStatusAction.CAN_NOT_PICK,
]

export enum PaymentMethodQuery {
    MOMO = "MOMO",
    VNPAY = "VNPAY",
    CASH = "CASH"
}

export enum RefundStatus {
    OPENING = 'opening',
    CANCELED = 'canceled',
    CLOSED = 'closed'
}

export enum ShopReviewStatus {
    PENDING = 'pending',
    APPROVED = 'approved',
    REJECTED = 'rejected',
}

export enum AdminReviewStatus {
    PENDING = 'pending',
    APPROVED = 'approved',
    REJECTED = 'rejected',
}

export enum RefundMoneyStatus {
    NONE = 'none',
    PENDING = 'pending',
    TRANSFERRING = 'transferring',
    COMPLETED = 'completed',
}

export enum BIDUStatusAction {
    PICKED_UP = 3, // Đã lấy hàng/Đã nhập kho
    CANCELED = -1, // Hủy đơn hàng
    CAN_NOT_PICK = 7, // Không lấy được hàng
    CHECKED = 6, // Đã đối soát
    RECEIVED = 2, // Đã tiếp nhận
    ASSIGN = 12, // Đã điều phối lấy hàng/Đang lấy hàng
    DELAY_PICK_UP = 8, // Hoãn lấy hàng
    NOT_RECEIVED = 1, // Chưa tiếp nhận
    DELIVERIED = 5, // Đã giao hàng/Chưa đối soát
    DELIVERING = 4, // Đã điều phối giao hàng/Đang giao hàng
    DELAY_DELIVER = 10, // Delay giao hàng
    UNABLE_TO_DELIVER = 9, // 	Không giao được hàng
    CHECKED_AND_RETURNED = 11, // 	Đã đối soát công nợ trả hàng
    RETURN_TO_SHOP = 20, // Đang trả hàng,
    RETURNED_TO_SHOP = 21, // Đã trả hàng,
    ON_PROCESSING = 31, // Đang trạng thái xử lí
    ON_SHIPMENT_PROCESSING = 32, // Đang trạng thái đơn vị vận chuyển xử lí
    DELIVER_BY_RECEIVER_POSTMAN = 33, // Đang trạng thái đơn vị vận chuyển xử lí - bưu tá nhận
    ORDER_SENT_AT_POSTOFFICE = 34, // Đơn hàng gửi tại bưu cục
    ON_DELIVERING = 51, //  Phát tiếp
    ON_DELIVERING_OTHER_POST = 52, // Chuyển tiếp bưu cục khác
    RE_SENT = 53, // Đơn Vị Yêu Cầu Phát Tiếp
}