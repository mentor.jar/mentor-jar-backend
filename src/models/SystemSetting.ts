import mongoose from 'mongoose';
import { SystemSettingDoc } from '../interfaces/SystemSetting';

const messageType = new mongoose.Schema(
    {
        title: {
            type: String,
            default: "",
        },
        content: {
            type: String,
            default: "",
        },
    },
    { _id: false }
);

const languageType = new mongoose.Schema({
    vi: {
        type: messageType,
        default: "",
    },
    ko: {
        type: messageType,
        default: "",
    },
    en: {
        type: messageType,
        default: "",
    },
},{ _id: false })

const iosType = new mongoose.Schema(
    {
        min: {
            type: String,
            default: '',
        },
        max: {
            type: String,
            default: '',
        },
        error: {
            type: Array,
            default: [],
        },
        message: {
            type: languageType,
        },
    },
    { _id: false }
);

const androidType = new mongoose.Schema(
    {
        min: {
            type: Number,
            default: 0,
        },
        max: {
            type: Number,
            default: 0,
        },
        error: {
            type: Array,
            default: [],
        },
        message: {
            type: languageType,
        },
    },
    {
        _id: false,
    }
);

const appType = new mongoose.Schema(
    {
        ios: {
            type: iosType,
        },
        android: {
            type: androidType,
        },
    },
    {
        _id: false,
    }
);

const SystemSettingSchema = new mongoose.Schema(
    {
        approved_order: {
            type: Boolean,
            default: true,
        },
        sensitive_content: {
            type: Array,
            default: [],
        },
        target_new_shop: {
            type: Number,
            default: 0,
        },
        target_transaction: {
            type: Number,
            default: 0,
        },
        version_production: {
            type: String,
            default: "1.0",
        },
        version_staging: {
            type: String,
            default: "1.0",
        },
        version_production_android: {
            type: Number,
            default: 0,
        },
        version_staging_android: {
            type: Number,
            default: 0,
        },
        app_version: {
            type: appType,
        },
        is_allow_edit_product_price: {
            type: Boolean,
            default: true
        }
    },
    {
        versionKey: false,
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at',
        },
    }
);

const SystemSetting = mongoose.model<SystemSettingDoc>("SystemSetting", SystemSettingSchema);
export default SystemSetting;