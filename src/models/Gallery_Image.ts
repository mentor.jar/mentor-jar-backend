import mongoose from 'mongoose'
const GalleryImageSchema = new mongoose.Schema({
    galleryImageAbleId: {
        type: mongoose.Schema.Types.ObjectId,
    },
    galleryImageAbleType: {
        type: String
    },
    userUpload: {
        type: mongoose.Schema.Types.ObjectId
    },
    url: {
        type: String
    },
    dimensions: {
        height: {
            type: Number,
        },
        width: {
            type: Number,
        },
    },
    mimeType: {
        type: String,
        default: 'image'
    },
    isActive: {
        type: Boolean
    },
    thumbnail: {
        type: String
    },
    order: {
        type: Number
    }
}, {
    versionKey: false,
    timestamps: true
})

const GalleryImage = mongoose.model("GalleryImage", GalleryImageSchema)
export default GalleryImage