import mongoose from 'mongoose'
import { GroupOrderDoc } from '../interfaces/GroupOrder';
import { OrderDoc } from '../interfaces/Order'
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const Schema = mongoose.Schema;

const GroupOrderSchema = new mongoose.Schema({
    total_price: {
        type: Number,
        require: [true, 'Vui lòng tính tổng số tiền đơn hàng']
    },
    user_id: { type: Schema.Types.ObjectId, ref: 'User' },
    orders: [
        {
            type: Object
        }
    ],
    status: {
        type: String,
        default: null
    },
    is_group_buy_order: {
        type: Boolean,
        default: false
    }
}, {
    versionKey: false,
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
})

GroupOrderSchema.statics.relationship = function () {
    const users = () => (
        {
            type: 'belongTo',
            table: 'users',
            foreign_key: 'user_id',
            as: 'user'
        }
    )
    return { users }
}

GroupOrderSchema.plugin(paginate);
GroupOrderSchema.plugin(aggregatePaginate);

const GroupOrder = mongoose.model<GroupOrderDoc>("GroupOrder", GroupOrderSchema)
export default GroupOrder