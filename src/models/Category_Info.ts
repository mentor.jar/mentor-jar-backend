import mongoose from 'mongoose'
// import validator from 'validator'
// import { object } from 'mongoose/lib/utils'
import { CategoryInfoDoc } from '../interfaces/Category_Info'
const paginate = require('./plugins/paginate');

const Schema = mongoose.Schema;

const CategoryInfoSchema = new mongoose.Schema({
    name: {
        type: {
            vi: {
                type: String
            },
            en: {
                type: String
            },
            ko: {
                type: String
            },
        }
    },
    type: {
        type: String
    },
    fashion_type: {
        type: Number,
        // default: 1,
        enum: [1, 2] // 1 is Áo, 2 is Quần
    },
    list_option: [
        {
            _id: false,
            vi: {
                type: String
            },
            en: {
                type: String
            },
            ko: {
                type: String
            },
        }
    ],
    is_required: {
        type: Boolean
    },
    is_allow_multiple_values: {
        type: Boolean,
        default: false
    },
    category_id: { type: Schema.Types.ObjectId, ref: 'ECategory' }
}, {
    versionKey: false,
    timestamps: true
})
CategoryInfoSchema.statics.const_type = function () {
    return {
        select: 'select',
        input: 'input',
        custom: 'custom'
    }
}
CategoryInfoSchema.plugin(paginate);
const CategoryInfo = mongoose.model<CategoryInfoDoc>("CategoryInfo", CategoryInfoSchema)
export default CategoryInfo