import mongoose from 'mongoose'
import { OptionValueDoc } from '../interfaces/Option_Value'
const paginate = require('./plugins/paginate');

const Schema = mongoose.Schema;

const OptionValueSchema = new mongoose.Schema({
    name: {
        type: String,
        require: [true, 'Please add a Option Valuename']
    },
    image: {
        type: String,
        default: null
    },
    option_type_id: { type: Schema.Types.ObjectId, ref: 'OptionType' }
}, {
    versionKey: false,
    timestamps: true
})
OptionValueSchema.plugin(paginate);
const OptionValue = mongoose.model<OptionValueDoc>("OptionValue", OptionValueSchema)
export default OptionValue