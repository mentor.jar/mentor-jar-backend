import mongoose from 'mongoose'
// import validator from 'validator'
// import { object } from 'mongoose/lib/utils'
import { ECategoryDoc } from '../interfaces/Category'
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const Schema = mongoose.Schema;

const ECategorySchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Vui lòng nhập tên danh mục']
    },
    description: {
        type: String
    },
    permalink: {
        type: String
    },
    priority: {
        type: Number
    },
    is_active: {
        type: Boolean,
        default: true
    },
    avatar: {
        type: String
    },
    pdfAvatar: {
        type: String
    },
    // nếu là admin tạo thì shop_id là null
    shop_id: { type: Schema.Types.ObjectId, ref: 'Shop' },
    // nếu là cấp 0 thì truyền null
    parent_id: { type: Schema.Types.ObjectId, ref: 'ECategory' },
    type: {
        type: String,
        default: null
    },
}, {
    versionKey: false,
    timestamps: true
})

ECategorySchema.statics.relationship = function () {
    const categories = () => (
        {
            type: 'belongTo',
            table: 'ecategories',
            foreign_key: 'parent_id',
            as: 'parents'
        }
    )
    return { categories }
}

ECategorySchema.plugin(paginate);
ECategorySchema.plugin(aggregatePaginate);

const Category = mongoose.model<ECategoryDoc>("ECategory", ECategorySchema)
export default Category;
