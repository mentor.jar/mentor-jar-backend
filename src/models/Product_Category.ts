import mongoose from 'mongoose'
import { ProductCategoryDoc } from '../interfaces/Product_Category'
const paginate = require('./plugins/paginate');

const Schema = mongoose.Schema;

const ProductCategorySchema = new mongoose.Schema({
    product_id: { type: Schema.Types.ObjectId, ref: 'Product' },
    category_id: { type: Schema.Types.ObjectId, ref: 'ECategory' }
}, {
    versionKey: false,
    timestamps: true
})
ProductCategorySchema.plugin(paginate);
const ProductCategory = mongoose.model<ProductCategoryDoc>("ProductCategory", ProductCategorySchema)
export default ProductCategory