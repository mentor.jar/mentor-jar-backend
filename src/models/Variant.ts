import { Timestamp } from 'mongodb';
import mongoose from 'mongoose'
import { VariantDoc } from '../interfaces/Variant'
const paginate = require('./plugins/paginate');

const Schema = mongoose.Schema;

const VariantSchema = new mongoose.Schema({
    sku: {
        type: String,
        required: [true, 'Please add a SKU for variant']
    },
    quantity: {
        type: Number,
        require: [true, 'Please add quantity of variant']
    },
    before_sale_price: {
        type: Number
    },
    sale_price: {
        type: Number
    },
    is_master: {
        type: Boolean,
        default: false
    },
    option_values: {
        type: Array,
        default: false
    },
    deleted_at: {
        type: Date,
        default: null
    },
    product_id: { type: Schema.Types.ObjectId, ref: "Product" }
}, {
    versionKey: false,
    timestamps: true
})

VariantSchema.plugin(paginate);

const Variant = mongoose.model<VariantDoc>("Variant", VariantSchema)
export default Variant