import mongoose from 'mongoose'
import { AddressDoc } from '../interfaces/Address'
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const Schema = mongoose.Schema;

const AddressSchema = new mongoose.Schema({
    name: {
        type: String,
        require: [true, 'Vui lòng nhập tên']
    },
    country: {
        type: String,
        default: ""
    },
    state: { type: Object },
    district: { type: Object },
    ward: { type: Object },
    street: {
        type: String
    },
    phone: {
        type: String,
        require: [true, 'Vui lòng nhập số điện thoại']
    },
    accessible_id: {
        type: String,
    },
    accessible_type: {
        type: String,
        default: 'User'
    },
    is_default: {
        type: Boolean,
        default: false
    },
    is_delivery_default: {
        type: Boolean,
        default: false
    },
    deleted_at: {
        type: Date
    },
    is_pick_address_default: {
        type: Boolean,
        default: false
    },
    is_return_address_default: {
        type: Boolean,
        default: false
    },
    expected_delivery: {
        type: String,
        enum: ["any_time","work_time"],
        default: "any_time"
    },
    address_type: {
        type: String,
        enum: ["home","company"],
        default: "home"
    }
}, {
    versionKey: false,
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
})

AddressSchema.plugin(paginate);
AddressSchema.plugin(aggregatePaginate);

const Address = mongoose.model<AddressDoc>("Address", AddressSchema)
export default Address