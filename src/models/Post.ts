import mongoose from 'mongoose'
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const PostSchema = new mongoose.Schema({
	fileVideo: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'GalleryVideo',
	},
	fileImage: [{
		type: mongoose.Schema.Types.ObjectId,
		ref: 'GalleryImage',
	}],
	postType: {
		type: String,
	},
	fileBBook: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'BBookUser',
	},
	userPostId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User'
	},
	categoryId: [{
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Category'
	}],
	contentOrganizer: {
		content: {
			type: String,
		},
		unsignedContent: {
			type: String,
		},
	},
	likeCount: {
		type: Number,
		default: 0
	},
	commentCount: {
		type: Number,
		default: 0
	},
	viewCount: {
		type: Number,
		default: 0
	},
	shareCount: {
		type: Number,
		default: 0
	},
	addressString: {
		type: String
	},
	latitude: {
		type: Number
	},
	longtitude: {
		type: Number
	},
	googlePlaceId: {
		type: String
	},
	isActive: {
		type: Boolean
	},
	hashtags: [{
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Tag'
	}],
	mentions: [{
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		default: []
	}],
	bodyTypeIds: [{
		type: mongoose.Schema.Types.ObjectId,
		ref: 'BodyShapeType',
		default: []
	}],
	isAccept: {
		type: Boolean,
	},
	shorten_link: {
		type: String,
		default: null
	}
}, {
	versionKey: false,
	timestamps: true
})


PostSchema.plugin(paginate)
PostSchema.plugin(aggregatePaginate)

PostSchema.index({ 'contentOrganizer.unsignedContent': 'text' }) // full-text search
const Post = mongoose.model("Post", PostSchema)
export default Post