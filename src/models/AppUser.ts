import mongoose from 'mongoose'
import { AddressDoc } from '../interfaces/Address'

const Schema = mongoose.Schema;

const AppUserSchema = new mongoose.Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    deviceId: {
        type: String,
        required: true,
        index: true
    },
    firebaseToken: {
        type: String,
        required: true,
        index: true
    },
    os: {
        type: String
    },
    language: {
        type: String,
        enum: [
            'vi',
            'ko',
            'en'
        ],
        default: 'vi',
    },

    /**
    * to distinguish app e-commerce/community
    */
    shardingKey: {
        type: String
    }
})


const AppUser = mongoose.model<AddressDoc>("AppUser", AppUserSchema)
export default AppUser