import mongoose from 'mongoose'
import { OptionTypeDoc } from '../interfaces/Option_Type'
const paginate = require('./plugins/paginate');

const Schema = mongoose.Schema;

const OptionTypeSchema = new mongoose.Schema({
    name: {
        type: String,
        require: [true, 'Please add a Option Type name']
    },
    description: {
        type: String
    },
    product_id: { type: Schema.Types.ObjectId, ref: 'Product' },
}, {
    versionKey: false,
    timestamps: true
})
OptionTypeSchema.plugin(paginate);
const OptionType = mongoose.model<OptionTypeDoc>("OptionType", OptionTypeSchema)
export default OptionType