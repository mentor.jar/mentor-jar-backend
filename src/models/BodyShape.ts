import mongoose from 'mongoose'
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const BodyShapeTypeSchema = new mongoose.Schema({
  bodyTypeName: {
    type: String,
    required: true
  },
  image: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'GalleryImage',
  },
  description: {
    type: String,
  },
  isActive: {
    type: Boolean
  },
  language: {
    type: Object
  },
  address: {
    type: String
  },
  priority: {
    type: Number
  }
}, {
  versionKey: false,
  timestamps: true
})

BodyShapeTypeSchema.plugin(paginate)
BodyShapeTypeSchema.plugin(aggregatePaginate)

const BodyShapeType = mongoose.model("BodyShapeType", BodyShapeTypeSchema)
export default BodyShapeType