import mongoose from 'mongoose'
import { OrderShippingDoc } from '../interfaces/Order_Shipping';
import { ObjectId } from '../utils/helper';
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const Schema = mongoose.Schema;

const OrderShippingSchema = new mongoose.Schema({
    user_id: { type: Schema.Types.ObjectId, ref: 'User' },
    order_id: { type: Schema.Types.ObjectId, ref: 'Order' },
    shop_id: { type: Schema.Types.ObjectId, ref: 'Shop' },
    user_confirm_shipped_time: {
        type: Date,
        default: null
    },
    assign_to_shipping_unit_time: {
        type: Date,
    },
    history: {
        type: [
            {
                partner_id: { type: String },
                label_id: { type: String },
                status_id: { type: Number },
                action_time: { type: Date },
                reason_code: { type: String },
                reason: { type: String },
                weight: { type: Number },
                fee: { type: Number },
                pick_money: { type: Number },
                return_part_package: { type: Number  },
                shipping_status: { type: String },
                reason_code_shipping: { type: String },
            }
        ],
        default: []
    },
    shipping_info: {
        type: {
            partner_id: { type: String },
            label: { type: String },
            area: { type: String },
            fee: { type: String },
            insurance_fee: { type: String },
            estimated_pick_time: { type: String },
            estimated_deliver_time: { type: String },
            products: { type: Array },
            tracking_id: { type: Number },
            sorting_code: { type: String  },
            status_id: { type: String },
            shipping_status: { type: String },
        },
        default: null
    }
}, {
    versionKey: false,
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
})

OrderShippingSchema.plugin(paginate);
OrderShippingSchema.plugin(aggregatePaginate);

const OrderShipping = mongoose.model<OrderShippingDoc>("OrderShipping", OrderShippingSchema)
export default OrderShipping
