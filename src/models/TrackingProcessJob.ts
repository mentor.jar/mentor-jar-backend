import mongoose from 'mongoose';
import { TrackingJobProcessDoc } from '../interfaces/TrackingProcessJob';
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const Schema = mongoose.Schema;

const TrackingProcessJobSchema = new mongoose.Schema({
    name: {
        type: String,
    },
    doing: {
        type: Number
    },
    target: {
        type: Number
    },
    status: {
        type: String
    },
    data_detail: {
        type: Object
    }
}, {
    versionKey: false,
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
})

TrackingProcessJobSchema.plugin(paginate);
TrackingProcessJobSchema.plugin(aggregatePaginate);

const TrackingProcessJob = mongoose.model("TrackingProcessJob", TrackingProcessJobSchema)
export default TrackingProcessJob