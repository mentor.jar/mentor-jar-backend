import mongoose from 'mongoose';
import { TrackingActivityDoc } from '../interfaces/Tracking_Activity';

const Schema = mongoose.Schema;
const TrackingActivitySchema = new mongoose.Schema(
    {
        target_id: Schema.Types.ObjectId,
        target_type: String,
        actor_id: Schema.Types.ObjectId,
        actor_type: String,
        action_type: String,
        action_info: Object,
        count_number: Number,
        visited_ats: Array,
        updated_at: Date,
        created_at: Date,
    }
    // {
    //     timestamps: {
    //         createdAt: 'created_at',
    //         updatedAt: 'updated_at',
    //     },
    // }
);

const TrackingActivity = mongoose.model<TrackingActivityDoc>(
    'TrackingActivity',
    TrackingActivitySchema
);
export default TrackingActivity;
