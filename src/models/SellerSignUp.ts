import mongoose, { Schema } from 'mongoose'
import { SellerSignUpDoc } from '../interfaces/SellerSignUp'
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const PersonalInfoSchema = new mongoose.Schema({
    full_name: {
        type: String,
        require: [true, 'Vui lòng nhập Họ và tên']
    },
    phone_number: {
        type: String,
        require: [true, 'Vui lòng nhập số điện thoại']
    },
    email: {
        type: String,
        require: [true, 'Vui lòng nhập email']
    },
    card_number: {
        type: String,
        require: [true, 'Vui lòng nhập số CCCD/CMND']
    },
    tax_code: {
        type: String,
        require: false,
        default: ""
    },
    country: {
        type: String,
        require: true,
        default: 'VN'
    }
}, {
    _id: false
})

const ShopInfoSchema = new mongoose.Schema({
    shop_name: {
        type: String,
        require: [true, 'Vui lòng nhập tên cửa hàng']
    },
    description: {
        type: String,
        require: false,
        default: ""
    },
    is_has_business: {
        type: String,
        require: [true, 'Vui lòng chọn doanh nghiệp hay cá nhân']
    },
    categories: {
        type: Array,
        require: [true, 'Vui lòng chọn ngành hàng']
    },
    address: {
        type: Object,
        require: false,
        default: null
    },
    social_business: {
        type: Array,
        require: [true, 'Vui lòng chọn thông tin kinh doanh trên các MXH']
    },
    social_link_info: {
        type: String,
        require: false,
        default: ""
    }
}, {
    _id: false
})

const BankInfoSchema = new mongoose.Schema({
    bank_name: {
        type: String,
        require: [true,'Vui lòng chọn ngân hàng']
    },
    bank_number: {
        type: String,
        require: [true, 'Vui lòng nhập số tài khoản']
    },
}, {
    _id: false
})

const SellerSignUpSchema = new mongoose.Schema({
    personal_info: {
        type: PersonalInfoSchema,
        require: [true, 'Vui lòng nhập đầy đủ thông tin cá nhân']
    },
    shop_info: {
        type: ShopInfoSchema,
        require: [true, 'Vui lòng nhập đầy đủ thông tin cửa hàng']
    },
    status: {
        type: String,
        require: [false, 'Vui lòng chọn trạng thái cho đơn đăng ký'],
        enum: ['pending', 'approved', 'rejected'],
        default: "pending"
    },
    approved_time: {
        type: Date,
        require: false,
        default: null
    },
    rejected_time: {
        type: Date,
        require: false,
        default: null
    },
    shop_id: {type: Schema.Types.ObjectId, ref: 'Shop', require: false, default: null},
    user_mapped: {
        type: Array,
        default: []
    },
    bank_info: {
        type: BankInfoSchema,
    }
}, {
    versionKey: false,
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
})

SellerSignUpSchema.plugin(paginate);
SellerSignUpSchema.plugin(aggregatePaginate);

const SellerSignUp = mongoose.model<SellerSignUpDoc>("SellerSignUp", SellerSignUpSchema)
export default SellerSignUp