import mongoose from 'mongoose'
import { AddressDoc } from '../interfaces/Address'
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const Schema = mongoose.Schema;

const NotificationSchema = new mongoose.Schema({
    type: {
        type: String,
    },
    targetedBy: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    createdBy: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    campaignId: {
        type: Schema.Types.ObjectId,
        ref: 'NotifyCampaign'
    },
    content: {
        type: String,
    },
    contentType: {
        type: String,
    },

    context: Object,

    targetType: {
        type: String,
    },

    isRead: {
        type: Boolean,
        default: false
    },

    /**
     * to distinguish app e-commerce/community
     */
    shardingKey: {
        type: String
    }
}, {
    versionKey: false,
    timestamps: true
})

NotificationSchema.plugin(paginate);
NotificationSchema.plugin(aggregatePaginate);

const Notification = mongoose.model<AddressDoc>("Notification", NotificationSchema)
export default Notification