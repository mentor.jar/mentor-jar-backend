import mongoose from 'mongoose';
import { ITopShopBanner } from '../interfaces/TopShopBanner';
const paginate = require('./plugins/paginate');
const aggregatePaginate = require('./plugins/aggregatePaginate');

const Schema = mongoose.Schema;

const ImagesSchema = new mongoose.Schema({
    _id: false,
    vi: String,
    en: String,
    ko: String,
})

const TopShopBannerSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Please add a top shop banner name']
    },
    images: ImagesSchema,
    shop_ids: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Shop'
        }
    ],
    is_active: {
        type: Boolean,
        default: true
    },
}, {
    versionKey: false,
    timestamps: true
})

TopShopBannerSchema.plugin(paginate);
TopShopBannerSchema.plugin(aggregatePaginate);

const TopShopBanner = mongoose.model<ITopShopBanner>("TopShopBanner", TopShopBannerSchema)
export default TopShopBanner;