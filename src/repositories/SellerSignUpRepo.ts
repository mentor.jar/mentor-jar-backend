import { BaseRepository } from "./BaseRepository";
import SellerSignUp from "../models/SellerSignUp"
import { SellerSignUpDoc } from "../interfaces/SellerSignUp";
import { cloneObj, ObjectId } from "../utils/helper";

export class SellerSignUpRepo extends BaseRepository<SellerSignUpDoc> {

    private static instance: SellerSignUpRepo;

    private constructor() {
        super();
        this.model = SellerSignUp;
    }

    public static getInstance(): SellerSignUpRepo {
        if (!SellerSignUpRepo.instance) {
            SellerSignUpRepo.instance = new SellerSignUpRepo();
        }

        return SellerSignUpRepo.instance;
    }

    public async getDetail(id) {
        let registrationUser =  await this.model.aggregate([
            {
                $match: { _id: ObjectId(id) },
            },
            {
                $lookup: {
                    from: "users",
                    let: {
                        user_ids: "$user_mapped"
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        '$expr': {
                                            $and: [
                                                {
                                                    $in: ["$_id", "$$user_ids"]
                                                }
                                            ]
                                        },
                                    }
                                ]
                            }
                        },
                        {
                            $lookup: {
                                from: "galleryimages",
                                let: {
                                    user_id: "$_id"
                                },
                                pipeline: [
                                    {
                                        $match: {
                                            '$expr': {
                                                $and: [
                                                    {
                                                        $eq: ["$galleryImageAbleId", "$$user_id"]
                                                    },
                                                    {
                                                        $eq: ["$galleryImageAbleType", "1"]
                                                    }
                                                ]
                                            },
                                        }
                                    }
                                ],
                                as: "gallery_image"
                            }
                        },
                        {
                            $unwind: {
                                path: "$gallery_image",
                                preserveNullAndEmptyArrays: true
                            }
                        }
                    ],
                    as: "user_mapped" 
                } 
            },
            {
                $project: { "user.password": 0 }
            }
        ])

        registrationUser = cloneObj(registrationUser);
        if (registrationUser) {
            return registrationUser[0];
        } else {
            return null;
        }
    }

    public async findAllRequest(status = 'all', page = 1, limit = 20) {
        const match:any = {}
        if(status !== 'all') {
            match.status = status
        }
        const query = this.model.aggregate([
            {
                $match: match
            },
            {
                $lookup: {
                    from: "users",
                    let: {
                        user_ids: "$user_mapped"
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        '$expr': {
                                            $and: [
                                                {
                                                    $in: ["$_id", "$$user_ids"]
                                                }
                                            ]
                                        },
                                    }
                                ]
                            }
                        },
                        {
                            $lookup: {
                                from: "galleryimages",
                                let: {
                                    user_id: "$_id"
                                },
                                pipeline: [
                                    {
                                        $match: {
                                            '$expr': {
                                                $and: [
                                                    {
                                                        $eq: ["$galleryImageAbleId", "$$user_id"]
                                                    },
                                                    {
                                                        $eq: ["$galleryImageAbleType", "1"]
                                                    }
                                                ]
                                            },
                                        }
                                    }
                                ],
                                as: "gallery_image"
                            }
                        },
                        {
                            $unwind: {
                                path: "$gallery_image",
                                preserveNullAndEmptyArrays: true
                            }
                        }
                    ],
                    as: "user_mapped" 
                } 
            },
            {
                $sort: {created_at: -1}
            }
        ])

        const result = await this.model
            .aggregatePaginateCustom(query, { page, limit })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        const data = result.docs;
        return { data, paginate }
    }


}