import { BaseRepository } from "./BaseRepository";
import Post from "../models/Post"

export class PostRepo extends BaseRepository<any> {

    private static instance: PostRepo;

    private constructor() {
        super();
        this.model = Post;
    }

    public static getInstance(): PostRepo {
        if (!PostRepo.instance) {
            PostRepo.instance = new PostRepo();
        }

        return PostRepo.instance;
    }

}