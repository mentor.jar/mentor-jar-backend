import { BaseRepository } from "./BaseRepository";
import OrderPayment from "../models/Order_Payment";
import { OrderPaymentDoc } from "../interfaces/Order_Payment";
export class PaymentRepo extends BaseRepository<OrderPaymentDoc>  {

    private static instance: PaymentRepo;

    constructor() {
        super();
        this.model = OrderPayment;
    }

    public static getInstance(): PaymentRepo {
        if (!PaymentRepo.instance) {
            PaymentRepo.instance = new PaymentRepo();
        }

        return PaymentRepo.instance;
    }


}
