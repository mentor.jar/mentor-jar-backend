import { SearchKeywordDoc } from '../interfaces/Search_Keyword';
import SearchKeyword from '../models/Search_Keyword';
import { ObjectId } from '../utils/helper';
import { BaseRepository } from "./BaseRepository";
import { SystemSettingRepo } from './SystemSettingRepo';


export class SearchKeywordRepo extends BaseRepository<SearchKeywordDoc> {

    private static instance: SearchKeywordRepo;
    private static systemSetting = SystemSettingRepo.getInstance();


    private constructor() {
        super();
        this.model = SearchKeyword;
    }

    public static getInstance(): SearchKeywordRepo {
        if (!SearchKeywordRepo.instance) {
            SearchKeywordRepo.instance = new SearchKeywordRepo();
        }

        return SearchKeywordRepo.instance;
    }

    public async insertOrUpdate(keyword, shop_id:any = null) {
        if (!keyword) return;
        const system: any = await SearchKeywordRepo.systemSetting.findOne({});
        const normalized = keyword.trim().toLowerCase()
        if (system?.sensitive_content?.includes(normalized)) return;
        let instance:any
        if(shop_id) {
            instance = await this.model.findOne({ keyword: { '$regex': "^" + normalized + "$", '$options': 'gi' }, shop_id: ObjectId(shop_id) });
        }
        else {
            instance = await this.model.findOne({ keyword: { '$regex': "^" + normalized + "$", '$options': 'gi' } });
        }
        
        if (instance) {
            instance.count_number = instance.count_number + 1;
            instance.save();
            return instance;
        } else {
            return this.create({
                keyword,
                shop_id
            })
        }
    }

    public async getSuggestKeyword(keyword, shop_id:any = null) {
        if (!keyword) return []
        const system: any = await SearchKeywordRepo.systemSetting.findOne({});
        const normalized = keyword.trim().toLowerCase()
        if (system?.sensitive_content?.includes(normalized)) return [];
        const keywordMatch = [
            {
                keyword: { '$regex': "^" + normalized + "\\s+.*", '$options': 'gi' }
            },
            {
                keyword: { '$regex': ".*\\s+" + normalized + "$", '$options': 'gi' }
            },
            {
                keyword: { '$regex': ".*\\s+" + normalized + "\\s+.*", '$options': 'gi' }
            },
            {
                keyword: { '$regex': "^" + normalized + "$", '$options': 'gi' }
            }
        ]

        const match = shop_id ? {
            shop_id: ObjectId(shop_id),
            $or: keywordMatch
        } : {
            $and: [
                {
                    $or: [
                        {shop_id: {$exists: false}},
                        {shop_id: null}
                    ],
                },
                {$or: keywordMatch}
            ]
        }
        
        const keywords = await this.model.aggregate([
            {
                $match: match
            },
            {
                $limit: 5
            },
            {
                $sort: { count_number: -1}
            }
        ])
        return keywords
    }

    public top(limit) {
        return this.model.find({$where:'this.keyword.length > 1'}).sort({ count_number: -1 }).limit(limit)
    }
}