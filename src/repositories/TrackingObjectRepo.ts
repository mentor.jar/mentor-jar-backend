import { BaseRepository } from "./BaseRepository";
import Role from "../models/Role"
import { RoleDoc } from "../interfaces/Role";
import { TrackingObjectDoc } from "../interfaces/Tracking_Object";
import TrackingObject from "../models/Tracking_Object";

export class TrackingObjectRepo extends BaseRepository<TrackingObjectDoc> {

    private static instance: TrackingObjectRepo;

    private constructor() {
        super();
        this.model = TrackingObject;
    }

    public static getInstance(): TrackingObjectRepo {
        if (!TrackingObjectRepo.instance) {
            TrackingObjectRepo.instance = new TrackingObjectRepo();
        }

        return TrackingObjectRepo.instance;
    }
    async createOrUpdateTrackingObject(user_id: string, target_type: string, target_id: string, ad_hoc: string) {
        const oldTracking: any = await this.findOne({ user_id, target_type, target_id });
        let newTracking;
        if (!oldTracking) {
            newTracking = await this.create({
                user_id,
                target_type,
                target_id,
                ad_hoc
            });
        } else {
            newTracking = await this.update(oldTracking._id, { is_read: false });
        }
        return newTracking;
    }


}