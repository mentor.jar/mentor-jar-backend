import mongoose from 'mongoose';
import { EJobHistory } from '../interfaces/JobsHistory';
import JobHistory from '../models/Jobs_History';
import { BaseRepository } from "./BaseRepository";
const ObjectId = mongoose.Types.ObjectId;

export default class JobHisoryRepo extends BaseRepository<EJobHistory>  {

    private static instance: JobHisoryRepo;

    private constructor() {
        super();
        this.model = JobHistory;
    }

    public static getInstance(): JobHisoryRepo {
        if (!JobHisoryRepo.instance) {
            JobHisoryRepo.instance = new JobHisoryRepo();
        }
        return JobHisoryRepo.instance;
    }

}