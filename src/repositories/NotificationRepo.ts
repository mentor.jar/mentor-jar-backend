
import mongoose from 'mongoose';
import keys from '../config/env/keys';
import { ENotification } from '../interfaces/Notification';
import Notification from '../models/Notification';
import { NotifyInterface, NotifyType } from '../services/notification';
import { NOTIFY_TYPE } from '../services/notification/constants';
import { ChangeNotificationStatusRequest, GetNotificationsRequest, PushMessageRequest } from '../services/notification/interface';
import { BaseRepository } from "./BaseRepository";
import { IBannerRepo } from './interfaces/IBannerRepo';
import IPaginateData from './interfaces/IPaginateData';
const ObjectId = mongoose.Types.ObjectId;

export default class NotificationRepo extends BaseRepository<ENotification> implements IBannerRepo, NotifyInterface.StorageServiceInt {

    private static instance: NotificationRepo;

    private constructor() {
        super();
        this.model = Notification;
    }

    public static getInstance(): NotificationRepo {
        if (!NotificationRepo.instance) {
            NotificationRepo.instance = new NotificationRepo();
        }
        return NotificationRepo.instance;
    }

    public async createNewNotification(item) {

    }

    public async getNotifications(request: GetNotificationsRequest): Promise<IPaginateData> {

        const { page, limit, userId, status, type } = request;


        const aggregate = this.model.aggregate([
            {
                $match: {
                    targetedBy: ObjectId(userId?.toString()),
                    shardingKey: NotifyType.SHARDING_KEY
                }
            },
            {
                $sort: {
                    createdAt: -1
                }
            }

        ]);

        const result = await this.model
            .aggregatePaginateCustom(aggregate, { page, limit })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }

        const data = result.docs;

        return { data, paginate }
    }

    public async getLatestNotification(userId: String, type: String) {
        const data = await this.model.find({
            targetedBy: ObjectId(userId.toString()),
            shardingKey: NotifyType.SHARDING_KEY,
            type: type
        })
            .sort({ createdAt: -1 })
            .limit(1)
        return data.length > 0 ? data[0] : null;
    }

    public async saveNotification(request: PushMessageRequest) {
        let receiverIds: any = request.receiverId;
        if (!Array.isArray(request.receiverId))
            receiverIds = [receiverIds];
        const notifySaved = await Promise.all(receiverIds.map(receiverId => {
            return this.model.create({
                isRead: false,
                type: request.type,
                createdBy: request.creatorId,
                targetedBy: receiverId,
                contentType: request.contentType,
                context: request.context,
                shardingKey: NotifyType.SHARDING_KEY,
                targetType: request.targetType
            })
        }));

        return notifySaved;
    }

    public countNotification() {
        // TODO
    }

    public unreadNotification(userId) {
        return this.find({ targetedBy: ObjectId(userId), isRead: false })
    }

    public changeReadStatus(request: ChangeNotificationStatusRequest) {
        const { userId, status, notifyIds, maskAll } = request;

        let condition: any = {
            "shardingKey": NotifyType.SHARDING_KEY,
        };

        if (userId) {
            condition = {
                ...condition,
                targetedBy: ObjectId(userId.toString())
            }
        }

        if (maskAll) {
            if (!userId) throw new Error('Require specify a user to mask read all notification');
        } else if (notifyIds) {
            condition = {
                ...condition,
                _id: { $in: notifyIds }
            }
        }

        return this.model
            .updateMany(
                condition,
                { "$set": { "isRead": status } }
            )
    }

    public removeNotification() {
        // TODO
    }

}