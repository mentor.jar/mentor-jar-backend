import { BaseRepository } from "./BaseRepository";
import EmailList from "../models/EmailList"
import { EmailListDoc } from "../interfaces/EmailList";

export class EmailListRepo extends BaseRepository<EmailListDoc> {

    private static instance: EmailListRepo;

    private constructor() {
        super();
        this.model = EmailList;
    }

    public static getInstance(): EmailListRepo {
        if (!EmailListRepo.instance) {
            EmailListRepo.instance = new EmailListRepo();
        }

        return EmailListRepo.instance;
    }

    async getEmailListByType(type, page = 1, limit = 20) {
        const query = this.model.aggregate([
            {
                $match: {
                    deleted_at: type === 'archive' ? {$ne: null} : {$eq: null}
                }
            },
            {
                $sort: {created_at: -1}
            }
        ])

        const result = await this.model
            .aggregatePaginateCustom(query, { page, limit })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        const data = result.docs;
        return { data, paginate }
    }

}