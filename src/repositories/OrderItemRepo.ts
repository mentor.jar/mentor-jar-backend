import { BaseRepository } from "./BaseRepository";
import OrderItem from "../models/Order_Item"
import { OrderItemDoc } from "../interfaces/Order_Item";
import { ObjectId } from "../utils/helper";
import { ShippingStatus } from "../models/enums/order";


export class OrderItemRepository extends BaseRepository<OrderItemDoc> {

    private static instance: OrderItemRepository;

    private constructor() {
        super();
        this.model = OrderItem;
    }

    public static getInstance(): OrderItemRepository {
        if (!OrderItemRepository.instance) {
            OrderItemRepository.instance = new OrderItemRepository();
        }

        return OrderItemRepository.instance;
    }

    getImageFormOrderId = async (order_id) => {
        const orderItem = await this.model.findOne({ order_id: order_id });
        if(!orderItem) return null;
        return orderItem.images[0];
    }

    async getDistinctProductInOrderItems(orderIds) {
        const orderItems = await this.model.find({ order_id: { $in: orderIds } }).distinct('product_id');
        return orderItems;
    }

    async getProductRanking(orderIDs) {
        orderIDs = orderIDs.map(item => ObjectId(item._id))
        return this.model.aggregate([
            {
                $match: { order_id: { $in: orderIDs } }
            },
            {
                $group: {
                    _id: "$product_id",
                    quantitySold: { $sum: "$quantity" }
                }
            },
            this.advanceProductQuery,
            {
                $unwind: {
                    path: "$product",
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $sort: { quantitySold: -1 }
            },
            {
                $limit: 10
            }
        ])
    }

    async getProductRankingForBuyer(orderIDs, page = 1, limit = 10) {
        orderIDs = orderIDs.map(item => ObjectId(item._id))
        const query = this.model.aggregate([
            {
                $match: { order_id: { $in: orderIDs } }
            },
            {
                $group: {
                    _id: "$product_id",
                    quantitySold: { $sum: "$quantity" }
                }
            },
            this.advanceProductQueryForBuyer,
            {
                $unwind: {
                    path: "$product",
                    preserveNullAndEmptyArrays: false
                }
            },
            {
                $sort: { quantitySold: -1 }
            },
            {
                $limit: 10
            }
        ])
        const result = await this.model
            .aggregatePaginateCustom(query, { page, limit })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        const data = result.docs;
        return { data, paginate }
    }

    async findOrderItemVariantByOrderAndProduct(orderID, productID) {
        const item = await this.model.findOne({
            order_id: ObjectId(orderID),
            product_id: ObjectId(productID)
        })
       
        if(item) {
            return item.variant
        }
        return null
    }

    getOrderItemUnprocessed = async (productID) => {
        return this.model.aggregate([
            {
                $match: { product_id: ObjectId(productID) }
            },
            {
                $lookup: {
                    from: "orders",
                    let: {
                        order_id: "$order_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        "shipping_status": { $in: [
                                            ShippingStatus.PENDING,
                                            ShippingStatus.WAIT_TO_PICK
                                        ]}
                                    },
                                    {
                                        $expr:{
                                            $and:
                                            [
                                                { $eq: ["$_id", "$$order_id"] },
                                            ]
                                        },
                                    }
                                ]
                            },
                        }
                    ],
                    as: "order"
                }
            }
        ])
    }

    private advanceProductQuery: any = {
        $lookup:
        {
            from: "products",
            let: {
                product_id: "$_id",
                deleted_at: "$deleted_at",
            },
            pipeline: [
                {
                    $match: {
                        $and: [
                            {
                                '$expr': {
                                    $and: [
                                        {
                                            '$eq': ['$_id', '$$product_id']
                                        }
                                    ]
                                },
                            },
                            {
                                "allow_to_sell": true
                            },
                            {
                                "is_approved": 'approved'
                            },
                            {
                                "quantity": {$gt: 0}
                            },
                            {
                                $or: [
                                    { deleted_at: { $exists: false } },
                                    { deleted_at: null }
                                ]
                            }
                        ]
                    }
                },
                {
                    $lookup:
                    {
                        from: "shops",
                        let: {
                            shop_id: "$shop_id"
                        },
                        pipeline: [
                            {
                                $match: {
                                    $and: [
                                        {
                                            '$expr': {
                                                $and: [
                                                    {
                                                        $eq: ["$_id", "$$shop_id"]
                                                    }
                                                ]
                                            },
                                        }
                                    ]
                                }
                            },
                            {
                                $lookup:
                                {
                                    from: "users",
                                    let: {
                                        user_id: "$user_id"
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $and: [
                                                    {
                                                        '$expr': {
                                                            $and: [
                                                                {
                                                                    $eq: ["$_id", "$$user_id"]
                                                                }
                                                            ]
                                                        },
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            $lookup: {
                                                from: "galleryimages",
                                                let: {
                                                    user_id: "$_id"
                                                },
                                                pipeline: [
                                                    {
                                                        $match: {
                                                            '$expr': {
                                                                $and: [
                                                                    {
                                                                        $eq: ["$galleryImageAbleId", "$$user_id"]
                                                                    },
                                                                    {
                                                                        $eq: ["$galleryImageAbleType", "1"]
                                                                    }
                                                                ]
                                                            },
                                                        }
                                                    }
                                                ],
                                                as: "gallery_image"
                                            }
                                        },
                                        {
                                            $unwind: {
                                                path: "$gallery_image",
                                                preserveNullAndEmptyArrays: true
                                            }
                                        }
                                    ],
                                    as: "user"
                                }
                            },
                            {
                                $unwind: {
                                    path: "$user",
                                    preserveNullAndEmptyArrays: true
                                }
                            }
                        ],
                        as: "shop"
                    }
                },
                {
                    $unwind: {
                        path: "$shop",
                        preserveNullAndEmptyArrays: true
                    }
                }
            ],
            as: "product"
        }
    }

    private advanceProductQueryForBuyer: any = {
        $lookup:
        {
            from: "products",
            let: {
                product_id: "$_id",
                deleted_at: "$deleted_at",
            },
            pipeline: [
                {
                    $match: {
                        $and: [
                            {
                                '$expr': {
                                    $and: [
                                        {
                                            '$eq': ['$_id', '$$product_id']
                                        }
                                    ]
                                },
                            },
                            {
                                "allow_to_sell": true
                            },
                            {
                                "is_approved": 'approved'
                            },
                            {
                                "quantity": {$gt: 0}
                            },
                            {
                                $or: [
                                    { deleted_at: { $exists: false } },
                                    { deleted_at: null }
                                ]
                            }
                        ]
                    }
                },
                {
                    $lookup: {
                        from: "variants",
                        let: {
                            product_id: "$_id"
                        },
                        pipeline: [
                            {
                                $match: {
                                    $and: [
                                        {
                                            '$expr': {
                                                $and: [
                                                    {
                                                        $eq: ["$product_id", "$$product_id"]
                                                    }
                                                ]
                                            },
                                        }
                                    ]
                                }
                            }
                        ],
                        as: "variants"
                    }
                },
                {
                    $lookup: {
                        from: "shops",
                        let: {
                            shop_id: "$shop_id"
                        },
                        pipeline: [
                            {
                                $match: {
                                    $and: [
                                        {
                                            '$expr': {
                                                $and: [
                                                    {
                                                        $eq: ["$_id", "$$shop_id"]
                                                    }
                                                ]
                                            },
                                        }
                                    ]
                                }
                            },
                        ],
                        as: "shop"
                    }
                },
                {
                    $unwind: {
                        path: "$shop",
                        preserveNullAndEmptyArrays: false
                    }
                }
            ],
            as: "product"
        }
    }

    async findOrderItemVariantByOrder(orderID) {
        const items = await this.model.find({
            order_id: ObjectId(orderID),
        })
        return items
    }
}