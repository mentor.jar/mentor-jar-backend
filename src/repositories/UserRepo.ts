import { AuthencationError, NotFoundError, ValidationError } from "../base/customError";
import { VERIFY_ACCOUNT_EMAIL_TEMPLATE } from "../base/variable";
import keys from "../config/env/keys";
import { UserDTO } from "../DTO/UserDTO";
import { UserDoc } from "../interfaces/User";
import User from "../models/User";
import { comparePassword, getAccessToken } from "../services/libs/authencation";
import { cloneObj, ObjectId } from "../utils/helper";
import { addLink } from "../utils/stringUtil";
import { BaseQueryHelper, BaseRepository } from "./BaseRepository";
import AppUserRepo from "./AppUserRepo";
import { ShopRepo } from "./ShopRepo";
import { InMemoryAuthUserStore } from "../SocketStores/AuthStore";
import { KEY } from "../constants/cache";

const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(keys.sendgrid_api_key)
export class UserRepo extends BaseRepository<UserDoc>  {

    private static instance: UserRepo;
    private storeUser = InMemoryAuthUserStore.getInstance();

    constructor() {
        super();
        this.model = User;
    }

    public static getInstance(): UserRepo {
        if (!UserRepo.instance) {
            UserRepo.instance = new UserRepo();
        }

        return UserRepo.instance;
    }

    async getUserByShopId(shopId: string) {
        const shopRepository = ShopRepo.getInstance();
        const shop = await shopRepository.findById(shopId);
        const user = await this.model.aggregate([
            {
                $match: { _id: ObjectId(shop.user_id) }
            },
            {
                $sort: { created_at: -1 }
            },
            {
                $lookup: {
                    from: "galleryimages",
                    let: {
                        user_id: "$_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        '$expr': {
                                            $and: [
                                                {
                                                    $eq: ["$galleryImageAbleId", "$$user_id"]
                                                },
                                                {
                                                    $eq: ["$galleryImageAbleType", "1"]
                                                }
                                            ]
                                        },
                                    }
                                ]
                            }
                        }
                    ],
                    as: "gallery_image"
                }
            },
            {
                $unwind: {
                    path: "$gallery_image",
                    preserveNullAndEmptyArrays: true
                }

            }
        ]);
        if (user) {
            return user[0];
        } else {
            return null;
        }
    }

    async loginDefault(email, password) {
        email = email.toLowerCase()
        let user: any = await this.findOne({ isActive: true, email: email })
        if (!user) {
            throw new NotFoundError("Địa chỉ email không tồn tại trong hệ thống")
        }
        if (!user.password && user.socialAccount) {
            return new ValidationError(`Tài khoản của bạn được đăng ký bằng tài khoản ${user.socialAccount.socialName}, vui lòng tạo mật khẩu nếu muốn tiếp tục đăng nhập bằng phương thức này.`)
        }

        const check = await comparePassword(password, user)
        if (!check)
            throw new AuthencationError("Mật khẩu không chính xác.")

        return getAccessToken(user)
    }

    async getUserbyListUserId(listUserId) {
        let users = await this.model.find({ _id: { $in: listUserId } }).populate('avatar', 'url');
        users = cloneObj(users);
        return users.map(user => {
            user.userName = user.nameOrganizer.userName;
            user.avatar = user.avatar ? addLink(`${keys.host_community}/`, user.avatar?.url) : null
            return (new UserDTO(user)).toSimpleJSON();
        })
    }

    async getUserById(userId) {
        let users = await this.getUserbyListUserId([userId]);
        if (users.length) {
            let user = users[0];
            let shop = await ShopRepo.getInstance().findShopByUserId(user._id.toString());
            user.shop_id = shop ? shop._id : null
            return users[0];
        }
        return null;
    }

    async getUserAndCacheById(id) {
        let user: any = await this.storeUser.get(id);
        if (!user) {
            user = await this.model.findById(id);
            if (!user) {
                throw new Error("User not found")
            } else {
                user = cloneObj(user);
                
                let shop:any = await ShopRepo.getInstance().findShopByUserId(user._id.toString());
                shop = cloneObj(shop);
    
                delete user.password;
                user.userName = user.nameOrganizer.userName;
                user.shop_id = shop ? shop._id : null;
                user.shop = shop;

                this.storeUser.set(KEY.STORE_USER, user)
                return user;
            }

        } else {
            return user;
        }
    }

    async getInfoUserByRoom(userId) {
        userId = ObjectId(userId.toString())
        let user: any = await this.findOne(userId);
        if (user) {
            user.is_online = await AppUserRepo.getInstance().getAppOnline(user._id) ? true : false
            user = new UserDTO(user).toDetailChatJSON()
            return user;
        }
        return null;
    }

    async getUserWithGallery(userId) {
        const user = await this.model.aggregate([
            {
                $match: { _id: ObjectId(userId) }
            },
            {
                $lookup:
                {
                    from: "galleryimages",
                    let: {
                        user_id: "$_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        '$expr': {
                                            $and: [
                                                {
                                                    $eq: ["$galleryImageAbleId", "$$user_id"]
                                                },
                                                {
                                                    $eq: ["$galleryImageAbleType", "1"]
                                                }
                                            ]
                                        },
                                    }
                                ]
                            }
                        }
                    ],
                    as: "gallery_image"
                }
            },
            {
                $unwind: {
                    path: "$gallery_image",
                    preserveNullAndEmptyArrays: true
                }
            }
        ]);
        return user;
    }

    getAllUserWithGallery(match) {
        const user = this.model.aggregate([
            {
                $match: match
            },
            {
                $lookup:
                {
                    from: "galleryimages",
                    let: {
                        user_id: "$_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        '$expr': {
                                            $and: [
                                                {
                                                    $eq: ["$galleryImageAbleId", "$$user_id"]
                                                },
                                                {
                                                    $eq: ["$galleryImageAbleType", "1"]
                                                }
                                            ]
                                        },
                                    }
                                ]
                            }
                        }
                    ],
                    as: "gallery_image"
                }
            },
            {
                $unwind: {
                    path: "$gallery_image",
                    preserveNullAndEmptyArrays: true
                }
            }
        ]);
        return user;
    }

    async sendVerifyEmail(user, email = null) {
        user = cloneObj(user)
        const userName = user?.nameOrganizer?.userName.toUpperCase() || "bạn"
        const code = Math.floor(100000 + Math.random() * 900000);

        // Save info to database
        const updateUser = { ...user }
        const currentTime = new Date().getTime()
        updateUser.email_verify.code = code
        updateUser.email_verify.expire = new Date(currentTime + 2 * 60000)

        // update user's email if it's social account
        if (email && !user.password && user.socialAccount) {
            updateUser.email = email
        }
        user = await this.model.update({ _id: ObjectId(user._id) }, updateUser)

        // Setting email info
        const userEmail = user.email || "ducphucit1202@gmail.com"
        const msg: any = {
            to: userEmail,
            from: 'biducommunity@gmail.com',
            templateId: VERIFY_ACCOUNT_EMAIL_TEMPLATE,
            dynamic_template_data: {
                user_name: userName,
                code
            }
        }

        // Send email
        let result = {
            success: true,
            message: 'Sended'
        }
        await sgMail
            .send(msg)
            .catch((error) => {
                console.error(error)
                result = {
                    success: false,
                    message: error.message
                }
            })
        return result
    }

    async getListUser(paginate: object | null, params) {
        let search = params.search;
        let match: any = { isActive: true };

        if (search) {
            let matchSearch = {
                $or: [
                    BaseQueryHelper.fieldSearch(search, 'nameOrganizer.userName'),
                    BaseQueryHelper.fieldSearch(search, 'nameOrganizer.unsigneduserName'),
                ],
            }

            match = {
                ...match,
                ...matchSearch
            }
        }

        let result = paginate
            ? await this.model.aggregatePaginateCustom(this.getAllUserWithGallery(match), paginate)
            : await this.getAllUserWithGallery(match);
        result = cloneObj(result);

        let paginateRes = {};
        if (paginate) {
            paginateRes = {
                limit: result.limit,
                total_page: result.totalPages,
                page: result.page,
                total_record: result.totalDocs,
            };
            result = result.docs;
        }
        result = result.map(e => {
            return new UserDTO(e).toSimpleJSONForAdmin();
        });
        if (paginate) return { data: result, paginate: paginateRes };

        return result;
    }

    async getValidUserToSendEmail() {
        const match: any = {
            isActive: true,
            $or: [
                { email: { $ne: null } },
                { 'socialAccount.id': { $ne: null }, 'socicalAccount.socialName': 'google' }
            ]
        }

        let result = await this.model.aggregate([
            {
                $match: match
            },
            { $sort: { "created_at": -1 } }
        ])

        result = cloneObj(result);
        result = result.map(item => {
            return {
                user_id: item._id,
                email: item?.email || item?.socialAccount?.id,
                user_name: item?.nameOrganizer?.userName
            }
        })
        // Distinct email and email black list
        result = result.filter((item) => {
            return result.filter(itemCheck => itemCheck.email === item.email).length <= 1 && !item.email.includes("@demo.com");
        })
        console.log("Total User: ", result.length);
        return result
    }

    async getValidBuyerToSendEmail() {
        const match: any = {
            isActive: true,
            $or: [
                { email: { $ne: null } },
                { 'socialAccount.id': { $ne: null }, 'socicalAccount.socialName': 'google' }
            ]
        }

        let result = await this.model.aggregate([
            {
                $match: match
            },
            {
                $lookup: {
                    from: "shops",
                    let: {
                        user_id: "$_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $eq: ["$user_id", "$$user_id"]
                                },
                            }
                        }
                    ],
                    as: "shop"
                }
            },
            {
                $unwind: {
                    path: "$shop",
                    preserveNullAndEmptyArrays: true
                }
            },
            { $sort: { "created_at": -1 } }
        ])

        result = cloneObj(result);
        result = result.map(item => {
            return {
                shop: item?.shop,
                user_id: item._id,
                email: item?.email || item?.socialAccount?.id,
                user_name: item?.nameOrganizer?.userName
            }
        })
        /** Get only:
         * Distinct by email
         * Not in black email list
         * Haven't shop or have shop without approved
         */
        result = result.filter((item) => {
            return result.filter(itemCheck => itemCheck.email === item.email).length <= 1
                && !item.email.includes("@demo.com") && (!item?.shop?._id || (item?.shop?._id && !item?.shop?.is_approved))
        })
        console.log("Total Buyer: ", result.length);
        return result
    }
    
    async userAndShops() {
        const match: any = {
            isActive: true
        }
        let result: any = await this.model.aggregate([
            {
                $match: match,
            },
            {
                $lookup: {
                    from: 'shops',
                    let: {
                        user_id: '$_id',
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $eq: ['$user_id', '$$user_id'],
                                },
                                is_approved: true,
                                pause_mode: false
                            },
                        },
                    ],
                    as: 'shop',
                },
            },
            {
                $unwind: {
                    path: '$shop',
                    preserveNullAndEmptyArrays: false,
                },
            },
            {
                $project: {
                    'shop._id': 1,
                },
            },
        ]);
        result = cloneObj(result)
        result = result.map(value => {
            return {
                user_id: value._id,
                shop_id: value.shop._id
            }
        })
        return result
    }

}
