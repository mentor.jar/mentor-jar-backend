import { BaseRepository } from "./BaseRepository";
import Feeback from "../models/Feedback";
import { FeedbackDoc } from "../interfaces/Feedback";
import { cloneObj, ObjectId } from "../utils/helper";
import { addLink } from "../utils/stringUtil";
import keys from "../config/env/keys";
import { UserDTO } from "../DTO/UserDTO";
import { OrderItemRepository } from "./OrderItemRepo";
import moment from "moment";
export class FeedbackRepo extends BaseRepository<FeedbackDoc>  {

    private static instance: FeedbackRepo;

    private orderItemRepo = OrderItemRepository.getInstance()

    constructor() {
        super();
        this.model = Feeback;
    }

    public static getInstance(): FeedbackRepo {
        if (!FeedbackRepo.instance) {
            FeedbackRepo.instance = new FeedbackRepo();
        }

        return FeedbackRepo.instance;
    }

    async getFeedbackByProductID(productID, filterAdvance, paginate, userID = null) {
        const today = new Date()
        today.setHours(0,0,0,0);
        let filter: any = {
            target_type: "Product",
            target_id: ObjectId(productID),
            created_at: {$lte: today}
        }

        let [totalFeedbacks, averageFeedbackRate, countByNumberStar, countComment, countMedia, feedbackMedias] = await Promise.all([
            (async () => {
                return this.model.aggregate([
                    { $match: filter },
                    { $count: "total" }
                ])
            })(),
            (async () => {
                return this.model.aggregate([
                    { $match: filter },
                    {
                        $group: {
                            _id: null,
                            avgRating: { $avg: "$vote_star" }
                        }
                    }
                ])
            })(),
            (async () => {
                return this.model.aggregate([
                    { $match: filter },
                    {
                        $group: {
                            _id: "$vote_star",
                            count: { $sum: 1 }
                        }
                    },
                    {
                        $project: {
                            _id: 0,
                            vote_star: "$_id",
                            total: "$count"
                        }
                    }
                ])
            })(),
            (async () => {
                return this.model.aggregate([
                    {
                        $match: {
                            ...filter,
                            content: { $ne: "" }
                        }
                    },
                    { $count: "totalComment" }
                ])
            })(),
            (async () => {
                return this.model.aggregate([
                    {
                        $match: {
                            ...filter,
                            medias: { $exists: true, $not: { $size: 0 } }
                        }
                    },
                    { $count: "totalMedia" }
                ])
            })(),
            (async () => {
                return this.model.aggregate([
                    {
                        $match: {
                            target_id: ObjectId(productID)
                        }
                    },
                    {
                        $project: {
                            medias: 1,
                            _id: 0
                        }
                    }
                ])
            })()
        ])

        const stars = [1, 2, 3, 4, 5]
        stars.forEach(star => {
            const item = countByNumberStar.filter(ele => ele.vote_star === star)[0]
            if (!item) {
                countByNumberStar.push({ vote_star: star, total: 0 })
            }
        })
        
        feedbackMedias = feedbackMedias.reduce((medias, feedback) => {
            return medias.concat(feedback?.medias)
        }, [])
        
        const goodFeedback = countByNumberStar.reduce((sum, item) => {
            return item.vote_star >= 4 ? sum += item.total : sum
        }, 0)

        const totalFeedback = totalFeedbacks.length ? totalFeedbacks[0].total : null
        const satisfactionRate = totalFeedback ? +((goodFeedback / totalFeedback * 100)).toFixed(0) : 0

        const generalFeedbackInfo = {
            averageFeedbackRate: averageFeedbackRate.length ? +(averageFeedbackRate[0].avgRating.toFixed(2)) : null,
            totalFeedback,
            totalByMedia: countMedia[0] ? countMedia[0].totalMedia : 0,
            totalByComment: countComment[0] ? countComment[0].totalComment : 0,
            totalByStar: countByNumberStar,
            feedbackMedias,
            satisfactionRate
        }

        // advance info
        if (filterAdvance.option === 'TYPE') {
            switch (filterAdvance.value) {
                case 'COMMENT':
                    filter = {
                        ...filter,
                        $and: [
                            { content: { $exists: true } },
                            { content: { $ne: null } },
                            { content: { $ne: '' } }
                        ]
                    }
                    break;
                case 'MEDIA':
                    filter = {
                        ...filter,
                        $and: [
                            { medias: { $exists: true } },
                            { medias: { $ne: [] } }
                        ]
                    }
                default:
                    break;
            }
        }
        else if (filterAdvance.option === 'STAR') {
            filter = {
                ...filter,
                vote_star: { $eq: filterAdvance.value }
            }
        }
        const relations = [
            this.model.relationship().shop(),
            this.model.relationship().user()
        ]
        const sort = { created_at: -1 }
        const promiseResult = this.getExpandValues(filter, relations, sort);
        let result = paginate
            ? await this.model.aggregatePaginateCustom(promiseResult, paginate)
            : await promiseResult;
        result = cloneObj(result);

        // paginate
        paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        let data = result.docs;
        data = cloneObj(data)
        // console.log(data);
        
        const promises = data.map(feedback => {
            return new Promise(async (resolve, reject) => {
                try {
                    const orderItemVariant = await this.orderItemRepo.findOrderItemVariantByOrderAndProduct(feedback.order_id, feedback.target_id)
                    if(orderItemVariant) {
                        feedback.variant = orderItemVariant
                    }
                    else {
                        feedback.variant = null
                    }
                    feedback.total_like = Array.isArray(feedback.user_liked) ? feedback.user_liked.length : 0
                    feedback.is_liked = userID && Array.isArray(feedback.user_liked) ? feedback.user_liked.includes(userID.toString()) : false
                    feedback.user_liked
                    resolve(feedback)
                } catch (error) {
                    resolve(feedback)
                }
            })
        })
        data = await Promise.all(promises).catch(error => {
            console.log("Error: ", error.message)
        });

        return { data, paginate, generalFeedbackInfo }
    }

    async getFeedbackAvgByShopID(shopID) {
        let filter: any = {
            shop_id: ObjectId(shopID)
        }

        let shop = await this.model.aggregate([
            { $match: filter }
        ])
        return shop?.avg_rating || 5
    }

    async getTotalFeedbackByShopID(shopID) {
        let filter: any = {
            shop_id: ObjectId(shopID)
        }

        let feedbacks = await this.model.aggregate([
            { $match: filter }
        ])
        return feedbacks
    }

    async getFeedbackListByShop(shopID, userID, filterAdvance, paginate) {
        const today = new Date()
        today.setHours(0,0,0,0);
        let filter: any = {
            target_type: "Product",
            shop_id: ObjectId(shopID),
            created_at: {$lte: today}
        }

        // general info
        let totalFeedbacks = await this.model.aggregate([
            { $match: filter },
            { $count: "total" }
        ])

        let averageFeedbackRate = await this.model.aggregate([
            { $match: filter },
            {
                $group: {
                    _id: "$shop_id",
                    avgRating: { $avg: "$vote_star" }
                }
            }
        ])

        let countByNumberStar = await this.model.aggregate([
            { $match: filter },
            {
                $group: {
                    _id: "$vote_star",
                    count: { $sum: 1 }
                }
            },
            {
                $project: {
                    _id: 0,
                    vote_star: "$_id",
                    total: "$count"
                }
            }
        ])

        const stars = [1, 2, 3, 4, 5]
        stars.forEach(star => {
            const item = countByNumberStar.filter(ele => ele.vote_star === star)[0]
            if (!item) {
                countByNumberStar.push({ vote_star: star, total: 0 })
            }
        })

        let countComment = await this.model.aggregate([
            {
                $match: {
                    ...filter,
                    content: { $ne: "" }
                }
            },
            { $count: "totalComment" }
        ])

        let countMedia = await this.model.aggregate([
            {
                $match: {
                    ...filter,
                    medias: { $exists: true, $not: { $size: 0 } }
                }
            },
            { $count: "totalMedia" }
        ])

        // all medias
        let feedbackMedias = await this.model.aggregate([
            {
                $match: {
                    shop_id: ObjectId(shopID)
                }
            },
            {
                $project: {
                    medias: 1,
                    _id: 0
                }
            }
        ])
        feedbackMedias = feedbackMedias.reduce((medias, feedback) => {
            return medias.concat(feedback?.medias)
        }, [])
        
        const goodFeedback = countByNumberStar.reduce((sum, item) => {
            return item.vote_star >= 4 ? sum += item.total : sum
        }, 0)

        const totalFeedback = totalFeedbacks.length ? totalFeedbacks[0].total : null
        const satisfactionRate = totalFeedback ? +((goodFeedback / totalFeedback * 100)).toFixed(0) : 0

        const generalFeedbackInfo = {
            averageFeedbackRate: averageFeedbackRate.length ? +(averageFeedbackRate[0].avgRating.toFixed(1)) : null,
            totalFeedback,
            totalByMedia: countMedia[0] ? countMedia[0].totalMedia : 0,
            totalByComment: countComment[0] ? countComment[0].totalComment : 0,
            totalByStar: countByNumberStar,
            feedbackMedias,
            satisfactionRate
        }

        // advance info
        if (filterAdvance.option === 'TYPE') {
            switch (filterAdvance.value) {
                case 'COMMENT':
                    filter = {
                        ...filter,
                        $and: [
                            { content: { $exists: true } },
                            { content: { $ne: null } },
                            { content: { $ne: '' } }
                        ]
                    }
                    break;
                case 'MEDIA':
                    filter = {
                        ...filter,
                        $and: [
                            { medias: { $exists: true } },
                            { medias: { $ne: [] } }
                        ]
                    }
                default:
                    break;
            }
        }
        else if (filterAdvance.option === 'STAR') {
            filter = {
                ...filter,
                vote_star: { $eq: filterAdvance.value }
            }
        }
        const relations = [
            this.model.relationship().shop(),
            this.model.relationship().user()
        ]
        const sort = { created_at: -1 }
        const promiseResult = this.getExpandValues(filter, relations, sort);
        let result = paginate
            ? await this.model.aggregatePaginateCustom(promiseResult, paginate)
            : await promiseResult;
        result = cloneObj(result);

        // paginate
        paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        let data = result.docs;
        data = cloneObj(data)
        // console.log(data);
        
        const promises = data.map(feedback => {
            return new Promise(async (resolve, reject) => {
                try {
                    const orderItemVariant = await this.orderItemRepo.findOrderItemVariantByOrderAndProduct(feedback.order_id, feedback.target_id)
                    if(orderItemVariant) {
                        feedback.variant = orderItemVariant
                    }
                    else {
                        feedback.variant = null
                    }
                    feedback.total_like = Array.isArray(feedback.user_liked) ? feedback.user_liked.length : 0
                    feedback.is_liked = userID && Array.isArray(feedback.user_liked) ? feedback.user_liked.includes(userID.toString()) : false
                    delete feedback.user_liked
                    resolve(feedback)
                } catch (error) {
                    resolve(feedback)
                }
            })
        })
        data = await Promise.all(promises).catch(error => {
            console.log("Error: ", error.message)
        });

        return { data, paginate, generalFeedbackInfo }
    }

    async getFeedbackByOrderID(orderID) {
        let filter: any = {
            order_id: ObjectId(orderID)
        }

        let feedbacks = await this.model.aggregate([
            { $match: filter },
            FeedbackQueryHelper.relations.user,
            {
                $unwind: {
                    path: "$user",
                    preserveNullAndEmptyArrays: true
                }
            },
            FeedbackQueryHelper.relations.ordereditem,
            {
                $unwind: {
                    path: "$ordered_item",
                    preserveNullAndEmptyArrays: true
                }
            },
            FeedbackQueryHelper.relations.orderitems
        ])

        feedbacks = cloneObj(feedbacks)
        feedbacks.map(feedback => {
            let user = feedback.user
            user.avatar = addLink(`${keys.host_community}/`, user.gallery_image.url)
            user = new UserDTO(user).toSimpleUserInfo();
            feedback.user = user
            return feedback
        })
        const userFeedbacks = feedbacks.filter(feedback => feedback.target_type === "Product")
        const shopFeedbacks = feedbacks.filter(feedback => feedback.target_type === "User")[0] || {}
        return {userFeedbacks, shopFeedbacks}
    }

    async getRatingAllShop(start_time: any, end_time: any) {

        const condition: Array<any> = [
            { target_type: "Product", delete: { $exists: false } },
        ];

        if (start_time) {
            condition.push({ created_at: { $gte: start_time } })
        }

        if (end_time) {
            condition.push({ created_at: { $lte: end_time } })
            
        }

        const ratingShops = await this.model.aggregate([
            {
                $facet: {
                    
                    four_five_stars: [
                        {
                            $match: { $and: [...condition, { vote_star: { $gte: 4} }, { vote_star: { $lte: 5}}] },
                        },
                        {
                            $sort: { created_at: -1 },
                        },
                        {
                            $count: 'four_five_stars',
                        },
                    ],
                    two_three_stars: [
                        {
                            $match: { $and: [...condition, { vote_star: { $gte: 2} }, { vote_star: { $lt: 4}}] },
                        },
                        {
                            $sort: { created_at: -1 },
                        },
                        {
                            $count: 'two_three_stars',
                        },
                    ],
                    less_than_two_stars: [
                        {
                            $match: { $and: [...condition, { vote_star: { $lt: 2}}] },
                        },
                        {
                            $sort: { created_at: -1 },
                        },
                        {
                            $count: 'less_than_two_stars',
                        },
                    ],
                },
            },
            {
                $project: {
                    four_five_stars: { $ifNull: [ { $arrayElemAt: ['$four_five_stars.four_five_stars', 0] }, 0] },
                    two_three_stars: { $ifNull: [ { $arrayElemAt: ['$two_three_stars.two_three_stars', 0] }, 0] },
                    less_than_two_stars: { $ifNull: [ { $arrayElemAt: ['$less_than_two_stars.less_than_two_stars', 0] }, 0] },
                },
            },
        ]);

        return ratingShops[0];
    }


    async getRatingAllShopExport(start_time: any, end_time: any) {

        const condition: Array<any> = [
            { target_type: "Product", delete: { $exists: false } },
        ];

        if (start_time) {
            condition.push({ created_at: { $gte: start_time } })
        }

        if (end_time) {
            condition.push({ created_at: { $lte: end_time } })
            
        }

        const ratingShops = await this.model.aggregate([
            {
                $facet: {
                    
                    fourToFiveStars: [
                        {
                            $match: { $and: [...condition, { vote_star: { $gte: 4} }, { vote_star: { $lte: 5}}] },
                        },
                        {
                            $sort: { created_at: 1 },
                        },
                        {
                            $project: {
                                _id: { $toString: "$_id"},
                                content: 1,
                                medias: {
                                    $reduce: {
                                        input: "$medias",
                                        initialValue: "",
                                        in: { $concat : ["$$value", "$$this", "\n"] }
                                    }
                                },
                                shop_id: { $toString: "$shop_id"},
                                user_id: { $toString: "$user_id"},
                                order_id: { $toString: "$order_id"},
                                created_at: {$dateToString: {format: "%Y-%m-%d %H:%M:%S", date: "$created_at"}}, 
                            }
                        }
                    ],
                    twoToThreeStars: [
                        {
                            $match: { $and: [...condition, { vote_star: { $gte: 2} }, { vote_star: { $lt: 4}}] },
                        },
                        {
                            $sort: { created_at: 1 },
                        },
                        {
                            $project: {
                                _id: { $toString: "$_id"},
                                content: 1,
                                medias: {
                                    $reduce: {
                                        input: "$medias",
                                        initialValue: "",
                                        in: { $concat : ["$$value", "$$this", "\n"] }
                                    }
                                },
                                shop_id: { $toString: "$shop_id"},
                                user_id: { $toString: "$user_id"},
                                order_id: { $toString: "$order_id"},
                                created_at: {$dateToString: {format: "%Y-%m-%d %H:%M:%S", date: "$created_at"}}, 
                            }
                        }
                    ],
                    lessThanTwoStars: [
                        {
                            $match: { $and: [...condition, { vote_star: { $lt: 2}}] },
                        },
                        {
                            $sort: { created_at: 1 },
                        },
                        {
                            $project: {
                                _id: { $toString: "$_id"},
                                content: 1,
                                medias: {
                                    $reduce: {
                                        input: "$medias",
                                        initialValue: "",
                                        in: { $concat : ["$$value", "$$this", "\n"] }
                                    }
                                },
                                shop_id: { $toString: "$shop_id"},
                                user_id: { $toString: "$user_id"},
                                order_id: { $toString: "$order_id"},
                                created_at: {$dateToString: {format: "%Y-%m-%d %H:%M:%S", date: "$created_at"}}, 
                            }
                        }
                    ],
                },
            },
        ]);

        return ratingShops[0];
    }

}

export class FeedbackQueryHelper {
    public static relations = {
        products: {
            $lookup: {
                from: "products",
                let: {
                    product_id: "$target_id",
                },
                pipeline: [
                    {
                        $match: {
                            $and: [
                                // Không kiểm tra các điều kiện allow_to_sell, is_approved, deleted_at ở đây
                                {
                                    '$expr': {
                                        '$eq': ['$_id', '$$product_id']
                                    }
                                }
                            ]
                        }
                    },
                    {
                        $lookup: {
                            from: "variants",
                            let: {
                                product_id: "$_id"
                            },
                            pipeline: [
                                {
                                    $match: {
                                        $and: [
                                            {
                                                '$expr': {
                                                    $and: [
                                                        {
                                                            $eq: ["$product_id", "$$product_id"]
                                                        }
                                                    ]
                                                },
                                            }
                                        ]
                                    }
                                }
                            ],
                            as: "variants"
                        }
                    }
                ],
                as: "products"
            }
        },
        user: {
            $lookup: {
                from: "users",
                let: {
                    user_id: "$user_id"
                },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$_id", "$$user_id"]
                            }
                        }
                    },
                    {
                        $lookup: {
                            from: "galleryimages",
                            let: {
                                user_id: "$_id"
                            },
                            pipeline: [
                                {
                                    $match: {
                                        '$expr': {
                                            $and: [
                                                {
                                                    $eq: ["$galleryImageAbleId", "$$user_id"]
                                                },
                                                {
                                                    $eq: ["$galleryImageAbleType", "1"]
                                                }
                                            ]
                                        },
                                    }
                                }
                            ],
                            as: "gallery_image"
                        }
                    },
                    {
                        $unwind: {
                            path: "$gallery_image",
                            preserveNullAndEmptyArrays: true
                        }
                    }
                ],
                as: "user"
            }
        },
        ordereditem: {
            $lookup: {
                from: "orderitems",
                let: {
                    product_id: "$target_id",
                    order_id: "$order_id"
                },
                pipeline: [
                    {
                        $match: {
                            $and: [
                                {
                                    '$expr': {
                                        $and: [
                                            {
                                                $eq: ["$product_id", "$$product_id"]
                                            },
                                            {
                                                $eq: ["$order_id", "$$order_id"]
                                            }
                                        ]
                                    },
                                }
                            ]
                        }
                    }
                ],
                as: "ordered_item"
            }
        },
        orderitems: {
            $lookup: {
                from: "orderitems",
                let: {
                    order_id: "$order_id"
                },
                pipeline: [
                    {
                        $match: {
                            $and: [
                                {
                                    '$expr': {
                                        $and: [
                                            {
                                                $eq: ["$order_id", "$$order_id"]
                                            }
                                        ]
                                    },
                                }
                            ]
                        }
                    }
                ],
                as: "order_items"
            }
        }
    }
}