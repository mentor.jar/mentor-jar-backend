import { BaseRepository } from "./BaseRepository";
import { ECategoryDoc } from "../interfaces/Category";
import Category from "../models/Category"
import { ICategoryRepo } from "./interfaces/ICategoryRepo";
import { IPaginateRequest } from "./interfaces/IPaginateRequest";
import { getSnakeFromTree, recursionGetListIdCategory, recursionLocalizedCategory, recursionTree, recursionTreeByLocalized, sortByPriority } from "../controllers/serviceHandles/categories";
import { CategoryDTO } from "../DTO/CategoryDTO";
import { cloneObj, ObjectId } from "../utils/helper";
import { QueryDBError } from "../base/customError";
import { ProductCategoryRepo } from "./ProductCategoryRepo";
import { ProductRepo } from "./ProductRepo";

import { Collection } from "../utils/collection";
import { executeError } from "../base/appError";
import { ProductDTO } from "../DTO/Product";
import { getMinMax } from "../controllers/serviceHandles/product";
import { CATEGORIES_LOCALIZED, VN_LANG } from "../base/variable";
// now, we have all code implementation from BaseRepository
export class CategoryRepo extends BaseRepository<ECategoryDoc> implements ICategoryRepo, IPaginateRequest {

    private static instance: CategoryRepo;

    private constructor() {
        super();
        this.model = Category;
    }

    public static getInstance(): CategoryRepo {
        if (!CategoryRepo.instance) {
            CategoryRepo.instance = new CategoryRepo();
        }

        return CategoryRepo.instance;
    }

    findBySlug(slug: string): Promise<any> {
        const result = this.model.findOne({ permalink: slug });
        return result;
    }

    async getListPagination(paginateRequest, search = '', isAdmin = false) {
        try {
            const category = await this.getTreeBySystem(isAdmin);
            if (paginateRequest.paginate) {
                let collection = new Collection(category);
                if (search) {
                    collection = collection.search('name', search);
                }
                return collection.pagination(paginateRequest.paginate.page, paginateRequest.paginate.limit);
            }

            return category;
        } catch (error) {
            throw executeError(error);
        }

    }

    async getTreeAll(language = VN_LANG, onlySystem = false, rootCategory:any = null): Promise<any> {
        let result;
        if(onlySystem) {
            const query:any = {
                shop_id: null, 
                is_active: true
            }
            if(rootCategory) {
                query.parent_id = ObjectId(rootCategory)
            }
            result = await this.model.find(query);
        }
        else {
            result = await this.model.find({});
        }
        
        let data = cloneObj(result);
        data = data.
            map((el) => (new CategoryDTO(el)).toJSON(CategoryDTO.fieldTree))
        // .sort((a, b) => sortByPriority(a, b));
        let res = { data: [] };
        recursionTreeByLocalized(data, rootCategory, res.data, language);
        return res.data
    }

    async getTreeById(id: string): Promise<any> {
        try {
            let result = await this.model.findById(id);
            result = cloneObj(result);
            if (!result)
                return null;
            result = (new CategoryDTO(result)).toSimpleJSON()
            let childs = await this.model.find({});
            let data = cloneObj(childs);

            data = data
                .map(el => (new CategoryDTO(el)).toSimpleJSON())
            // .sort((a, b) => sortByPriority(a, b));

            let res = { data: [] };
            recursionTree(data, id, res.data);

            result = cloneObj(result);
            result.childs = res.data
            return result;
        } catch (error) {
            throw (error);
        }
    }

    async getTreeByShopId(shopId: string, language?: string): Promise<any> {
        try {
            if(!language) language = VN_LANG
            let productCategoryRepo = ProductCategoryRepo.getInstance();
            let productRepo = ProductRepo.getInstance();
            const categories = await this.model.find({ shop_id: shopId });
            categories.sort((a, b) => sortByPriority(a, b))

            let listPromise: any = categories.map(cate => {
                return new Promise(async (resolve, reject) => {
                    let productCategory: any = await productCategoryRepo.find({ category_id: cate._id })
                    const productCategoryIds = productCategory.map(e => e.product_id.toString());
                    const productItems = await productRepo.find({ _id: { $in: productCategoryIds }, deleted_at: null, is_approved: "approved" });
                    cate = cloneObj(cate)
                    cate.products = productItems
                    const categoryLocalized = CATEGORIES_LOCALIZED.get(cate.name.trim().toLowerCase())
                    categoryLocalized && categoryLocalized[language] ? cate.name = categoryLocalized[language] : '';
                    resolve(cate)
                })
            });
            return Promise.all(listPromise);

        } catch (error) {
            throw (error);
        }
    }

    async getTreeVariantByShopId(shopId: string): Promise<any> {
        try {
            let productCategoryRepo = ProductCategoryRepo.getInstance();
            let productRepo = ProductRepo.getInstance();
            const categories = await this.model.find({ shop_id: shopId, is_active: true }).limit(10);
            categories.sort((a, b) => sortByPriority(a, b))
            let listPromise: any = categories.map(cate => {
                return new Promise(async (resolve, reject) => {
                    let productCategory: any = await productCategoryRepo.find({ 
                        category_id: cate._id,
                        deleted_at: null,
                    });
                    const productCategoryIds = productCategory.map(e => e.product_id.toString());
                    const productItems = await productRepo.getProductsWithVariants(productCategoryIds);
                    cate = cloneObj(cate)
                    cate.products = productItems
                    resolve(cate)
                })
            });

            let resultCategory: any = await Promise.all(listPromise);
            resultCategory = resultCategory.filter(ele => ele.products.length)
            resultCategory.sort((a, b) => sortByPriority(a, b))
            return resultCategory

        } catch (error) {
            throw (error);
        }
    }

    async getTreeBySystem(getByAdmin = true): Promise<any> {
        let filter = {};        
        if (getByAdmin == true) {
            filter = { shop_id: null };
        } else {
            filter = { shop_id: null, is_active: true };
        }
        const result = await this.model.find(filter);
        let data = cloneObj(result);

        data = data
            .map(el => (new CategoryDTO(el)).toSimpleJSON())
        // .sort((a, b) => sortByPriority(a, b))


        let res = { data: [] };
        recursionTree(data, null, res.data);

        return res.data;
    }

    async getTreeBySystemByLocalized(getByAdmin = true, language): Promise<any> {
        let filter = {};
        if (getByAdmin == true) {
            filter = { shop_id: null };
        } else {
            filter = { shop_id: null, is_active: true };
        }
        const result = await this.model.find(filter);
        let data = cloneObj(result);

        data = data
            .filter(category => category.name !== "Ưu đãi")
            .map(el => (new CategoryDTO(el)).toSimpleJSON())
        // .sort((a, b) => sortByPriority(a, b))


        let res = { data: [] };
        recursionTreeByLocalized(data, null, res.data, language);

        return res.data;
    }

    async getCategoriesShopStatistics(listIdCategory, language): Promise<any> {
        // get list category parent_id = null
        let filter = {
            shop_id: null,
            is_active: true,
        };
        let categories = await this.model.find({ ...filter, _id: { $in: listIdCategory }, parent_id: null });
        
        // get list category which have parents
        const categoryIds = listIdCategory.map(id => ObjectId(id))
        let relations = [
            this.model.relationship().categories(),
        ]
        let query = await this.getExpandValues({ ...filter, _id: { $in: categoryIds }, parent_id: { $ne: null } }, relations);

        // group by parent category
        let res: any = {};
        for (let i = 0; i < query.length; i++) {
            const clone = cloneObj(query[i])
            delete clone.parents;
            if (!res[query[i].parents._id]) {
                res[query[i].parents._id] = query[i].parents;
                res[query[i].parents._id].childs = [];
            }
            res[query[i].parents._id].childs.push(clone);
        }

        // merge array
        categories = categories.concat(Object.values(res));

        // localized
        recursionLocalizedCategory(categories, language);

        return categories
    }

    

    async searchCategoryBySystem(search): Promise<any> {

        try {
            search = search ? search.normalize("NFC") : ""
            search = search.toLowerCase();
            //Check whether list category is gotten by admin or mobile
            const getByAdmin = false;
            let dataTreeSystem = await this.getTreeBySystem(getByAdmin);
            let res: any = { data: [] };
            getSnakeFromTree(dataTreeSystem, null, res.data)
            let results: any = []
            for (let arrayCategoryContain of res.data) {
                if (!arrayCategoryContain[arrayCategoryContain.length - 1].childs) {
                    let x = arrayCategoryContain.findIndex(category => {
                        let nameCategory = category.name ? category.name.normalize("NFC") : ""
                        return nameCategory.toLowerCase().includes(search)
                    })
                    if (x > -1)
                        results.push(arrayCategoryContain);
                }
            }
            return results

        } catch (error) {
            // console.log(error);
        }
        return [];
    }

    async getAllChildCategoryId(id: string) {
        const model = await this.findById(id);

        if (!model) {
            return []
        }
        const result = await this.model.find().select('id parent_id name shop_id permalink');
        let data = JSON.parse(JSON.stringify(result));
        let res = { data: [id] };
        recursionGetListIdCategory(data, id, res.data);
        return res.data;
    }

    deleteCategories(allCategoryId: Array<String>) {
        return this.model.find({ _id: allCategoryId }).deleteMany();
    }

    async getCategory(id: String) {
        try {
            const category = await this.model.findById(id);
            if (!category)
                return null;
            let categoryDTO = new CategoryDTO(category);
            let categoryField = categoryDTO.toJSON(['name', 'priority', 'parent_id', 'permalink']);
            return categoryField;
        } catch (error) {
            throw new QueryDBError(error.message);
        }

    }

    async getCategorySystemOrderBy() {
        try {
            return this.model.aggregate([
                {
                    $match: {
                        shop_id: null,
                        avatar: { $ne: null },
                        parent_id: null,
                        createdAt: {$gte: new Date(2021, 7, 26)},
                        is_active: true,
                    },
                },
                {
                    $sort: { priority: 1 }
                }
            ]);
           
        } catch (error) {
            throw new QueryDBError(error.message);
        }

    }

    async getJSONWithMinMax(listCategory: any, user: any) {
        let data: any = [];
        listCategory.map(category => {
            let results: any = null;
            let productDTO = category.products.map(e => new ProductDTO(e));
            results = productDTO.map(e => {
                e.setUser(user);
                let getProduct = e.toJSONWithBookMark();
                let getProductOption = e.toJSON(['variants']);
                //Handle variants field
                let variantsDTO = e.getProductVariant(getProductOption.variants);
                getProduct.variants = [];
                variantsDTO.map(e => {
                    let varianJson = e.toJSON(['_id', 'option_values', 'before_sale_price', 'sale_price', 'quantity', 'is_master'])
                    getProduct.variants.push(varianJson);
                });
                getProduct.price_min_max = getMinMax(getProduct.variants);
                delete getProduct.variants;
                return getProduct;
            });
            category.products = results;
            data.push(category);
        });
        return data;
    }

    /**
     * Return category information 
     * and its active product amount
     * @param id 
     */
    async getCategoryOfShopExplore(id: String) {
        try {
            let productCategoryRepo = ProductCategoryRepo.getInstance();
            let productRepo = ProductRepo.getInstance();
            
            const categories = await this.model.find({ shop_id: id, is_active: true })
            const categoryIds = categories.map(e => e._id.toString());

            let productCategories: any = await productCategoryRepo.find({ category_id: { $in: categoryIds } })
            let productIds = productCategories.map(e => e.product_id.toString());

            let activeProducts = await productRepo.find({ 
                _id: { $in: productIds }, 
                deleted_at: null, 
                is_approved: "approved", 
                allow_to_sell: true, 
                quantity: {$gt: 0}
            });

            let activeProductIds = activeProducts.map(e => e._id.toString());
            productCategories = productCategories.filter(productCategory => {
                return activeProductIds.some(activeProductId => activeProductId === productCategory.product_id.toString());
            })

            const result: Array<any> = [];

            categories.forEach(category => {
                const jsonData = category.toJSON();
                const matchProductCategories = productCategories.filter(
                    productCategory => productCategory.category_id.toString() == category._id.toString());
                if (matchProductCategories.length > 0) {
                    // Add product number to result
                    jsonData.product_numbers = matchProductCategories.length;
                    try {
                        const productId = matchProductCategories[0].toJSON().product_id;
                        const productData: any = activeProducts.find(
                            product => product._id.toString() == productId);
                        // Add a product image to show category image
                        jsonData.image_url = productData != undefined ? productData?.images[0] : '';
                    } catch (error) { }
                    result.push(jsonData);
                }
            })

            return result;
        } catch (error) {
            throw new QueryDBError(error.message);
        }
    }

    async getShopCategoryByShopId(shopId: string, page, limit): Promise<any> {
        try {
            const query = this.model.aggregate([
                {
                    $match: {
                        shop_id: ObjectId(shopId)
                    }
                }
            ])
            const result = await this.model
                .aggregatePaginateCustom(query, { page, limit })
            const paginate = {
                limit: result.limit,
                total_page: result.totalPages,
                page: result.page,
                total_record: result.totalDocs
            }
            const data = result.docs;
            return { data, paginate }

        } catch (error) {
            throw (error);
        }
    }

    async getCategoriesInfo(listCategoryId): Promise<any> {
        const query = {
            _id: { $in: listCategoryId },
            is_active: true
        }

        const result = await this.model.find(query);

        return result;
    }
}