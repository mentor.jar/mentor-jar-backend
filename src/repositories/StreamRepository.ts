import { BaseRepository } from "./BaseRepository";
import Stream from "../models/Stream"
import { IStreamRepo } from "./interfaces/IStreamRepo";
import { StreamDoc } from "../interfaces/Stream";

export class StreamRepository extends BaseRepository<StreamDoc> implements IStreamRepo {

    private static instance: StreamRepository;

    private constructor() {
        super();
        this.model = Stream;
    }

    public static getInstance(): StreamRepository {
        if (!StreamRepository.instance) {
            StreamRepository.instance = new StreamRepository();
        }

        return StreamRepository.instance;
    }

    getStreamByUserId(id: String) {
        const stream = this.model.findOne({ user_id: id });
        return stream;
    }

}