import { OptionValueDoc } from "../../interfaces/Option_Value";
import { BaseRepository } from "../BaseRepository";
import { IOptionValueRepo } from "../interfaces/Variant/IOptionValueRepo";
import OptionValue from "../../models/Option_Value";
import { OptionTypeRepo } from "./OptionTypeRepo";
import mongoose from 'mongoose'
import { cloneObj } from "../../utils/helper";
const ObjectId = mongoose.Types.ObjectId;
export class OptionValueRepo extends BaseRepository<OptionValueDoc> implements IOptionValueRepo {

    private static instance: OptionValueRepo;

    constructor() {
        super();
        this.model = OptionValue;
    }

    public static getInstance(): OptionValueRepo {
        if (!OptionValueRepo.instance) {
            OptionValueRepo.instance = new OptionValueRepo();
        }

        return OptionValueRepo.instance;
    }

    public async getByProductIdAndValue(product_id, name) {
        let optionTypeRepo = OptionTypeRepo.getInstance();

        let optionTypes = await optionTypeRepo.getAddValue({
            product_id: ObjectId(product_id)
        }, { $eq: ["$name", name] });
        optionTypes = cloneObj(optionTypes);
        for (let optionType of optionTypes) {
            if (optionType.option_values.length > 0)
                return cloneObj(optionType.option_values[0]);
        }
        return null;
    }
}