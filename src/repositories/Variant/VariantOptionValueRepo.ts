import { BaseRepository } from "../BaseRepository";
import { IVariantOptionValueRepo } from "../interfaces/Variant/IVariantOptionValueRepo";
import { VariantOptionValueDoc } from "../../interfaces/Variant_OptionValue";
import VariantOptionValue from "../../models/Variant_OptionValue";

export class VariantOptionValueRepo extends BaseRepository<VariantOptionValueDoc> implements IVariantOptionValueRepo {

    private static instance: VariantOptionValueRepo;

    constructor() {
        super();
        this.model = VariantOptionValue;
    }

    public static getInstance(): VariantOptionValueRepo {
        if (!VariantOptionValueRepo.instance) {
            VariantOptionValueRepo.instance = new VariantOptionValueRepo();
        }

        return VariantOptionValueRepo.instance;
    }
}