import { BaseRepository } from "../BaseRepository";
import { IVariantRepo } from "../interfaces/Variant/IVariantRepo";
import { VariantOptionValueDoc } from "../../interfaces/Variant_OptionValue";
import Variant from "../../models/Variant";
import { VariantDTO } from "../../DTO/VariantDTO";
import { ObjectId } from "../../utils/helper";

export class VariantRepo extends BaseRepository<VariantOptionValueDoc> implements IVariantRepo {

    private static instance: VariantRepo;

    constructor() {
        super();
        this.model = Variant;
    }

    public static getInstance(): VariantRepo {
        if (!VariantRepo.instance) {
            VariantRepo.instance = new VariantRepo();
        }

        return VariantRepo.instance;
    }

    async getByProductId(product_id: string): Promise<any> {
        let variants: any;
        let results: any = [];
        try {
            variants = await this.model.find({ product_id: product_id });
            let variantsDTO = variants.map(e => new VariantDTO(e));

            variantsDTO.map(e => {
                let variantField = e.toJSON(['option_values', 'before_sale_price', 'sale_price', 'quantity', 'is_master']);
                results.push(variantField);
            });
            return results;
        } catch (error) {
            console.log(error.message);
        }
    }

    async findWithProduct(variant_id:any, product_id:any) {
        const query:any = [
            {
                $match: {
                    _id: ObjectId(variant_id),
                    quantity: { '$gt': 0 }
                }
            },
            {
                $lookup: {
                    from: "products",
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        _id: ObjectId(product_id)
                                    },
                                    {allow_to_sell: true},
                                    {is_approved: 'approved'},
                                    {
                                        $or: [
                                            {
                                                quantity: { '$gt': 0 }
                                            },
                                            {
                                                quantity: { '$lte': 0 },
                                                is_pre_order: true
                                            }
                                        ]
                                    },
                                    {
                                        $or: [
                                            { deleted_at: { $exists: false } },
                                            { deleted_at: null }
                                        ]
                                    }
                                ]
                            }
                        }
                    ],
                    as: "product"
                }
            },
            {
                $unwind: {
                    path: "$product",
                    preserveNullAndEmptyArrays: true
                }

            }
        ]
        return this.model.aggregate(query)
    }

}