import { OptionTypeDoc } from "../../interfaces/Option_Type";
import { BaseRepository } from "../BaseRepository";
import OptionType from "../../models/Option_Type"
import { IOptionTypeRepo } from "../interfaces/Variant/IOptionTypeRepo";
import mongoose from "mongoose";
import { OptionTypeDTO } from "../../DTO/OptionTypeDTO";
import { QueryDBError } from "../../base/customError";

const ObjectId = mongoose.Types.ObjectId;

export class OptionTypeRepo extends BaseRepository<OptionTypeDoc> implements IOptionTypeRepo {

    private static instance: OptionTypeRepo;

    constructor() {
        super();
        this.model = OptionType;
    }

    public static getInstance(): OptionTypeRepo {
        if (!OptionTypeRepo.instance) {
            OptionTypeRepo.instance = new OptionTypeRepo();
        }

        return OptionTypeRepo.instance;
    }

    public async getAddValue(item: any, childQuery) {
        let relaQuery = [
            {
                $eq: ["$option_type_id", "$$id"]
            }
        ];
        relaQuery.push(childQuery);
        let result = await this.model.aggregate([
            { $match: item }
        ])
            .lookup({
                from: "optionvalues",
                let: {
                    id: "$_id"
                },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $and: relaQuery
                            }
                        }
                    }
                ],
                as: "option_values"
            });
        return result
    }

    async getByProductId(product_id: string): Promise<any> {
        let optionType: any;
        let results: any = []
        try {
            //optionType = await this.model.find({ product_id: product_id });
            optionType = await this.getAddValue({
                product_id: ObjectId(product_id)
            }, "");

            let OpTyDTOs = optionType.map(e => new OptionTypeDTO(e));

            OpTyDTOs.map(e => {
                let getOptionType = e.toJSON(['_id', 'name', 'option_values']);
                let OpVaDTOs = e.getOptionValue(getOptionType.option_values);

                getOptionType.option_values = [];
                OpVaDTOs.map(eV => {
                    getOptionType.option_values.push(eV.toJSON(['_id', 'name', 'image']));
                })

                results.push(getOptionType);
            });
            return results;

        } catch (error) {
            throw new QueryDBError(error.message);
        }
    }

}