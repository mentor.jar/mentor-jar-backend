import { BaseRepository } from "./BaseRepository";
import BBookUser from "../models/BBookUser"

export class BBookUserRepo extends BaseRepository<any> {

    private static instance: BBookUserRepo;

    private constructor() {
        super();
        this.model = BBookUser;
    }

    public static getInstance(): BBookUserRepo {
        if (!BBookUserRepo.instance) {
            BBookUserRepo.instance = new BBookUserRepo();
        }

        return BBookUserRepo.instance;
    }

}