import mongoose from "mongoose";
import { executeError } from "../base/appError";
import { NotFoundError, QueryDBError } from "../base/customError";
import keys from "../config/env/keys";
import { detailProductFromBuyer, getMinMax } from "../controllers/serviceHandles/product";
import { ProductSort, ShopListProductRequest, ShopSearchProductRequest } from '../controllers/serviceHandles/shop/definition';
import { ProductDTO } from "../DTO/Product";
import { ProductDoc } from "../interfaces/Product";
import Product from "../models/Product";
import SimilarProductStategy from '../services/decision/similarity';
import { cloneObj, isMappable, ObjectId } from "../utils/helper";
import { BaseQueryHelper, BaseRepository } from "./BaseRepository";
import { CategoryInfoRepo } from "./CategoryInfoRepo";
import { CategoryRepo } from "./CategoryRepo";
import IPaginateData from "./interfaces/IPaginateData";
import { ProductDetailInfoRepo } from "./ProductDetailInfo";
import { StreamSessionRepository } from "./StreamSessionRepository";
import { ShopRepo } from "./ShopRepo";
import { addLink } from "../utils/stringUtil";
import { OptionTypeRepo } from "./Variant/OptionTypeRepo";
import _ from "lodash";
import { OrderRepository } from "./OrderRepo";
import { OrderItemRepository } from "./OrderItemRepo";
import { ProductRankingType } from "../controllers/serviceHandles/shopStatistic/productRanking";
import { UserRepo } from "./UserRepo";
import { UserDTO } from "../DTO/UserDTO";
import { TrackingRate, TrackingType } from "../services/tracking/definitions";
import {
    ARRAY_DETAIL_SIZE_FASHION_TYPE_1,
    ARRAY_DETAIL_SIZE_FASHION_TYPE_2,
    ARRAY_DETAIL_SIZE_FASHION_TYPE_3,
    ARRAY_DETAIL_SIZE_FASHION_TYPE_4,
    ARRAY_DETAIL_SIZE_FASHION_TYPE_5,
    ARRAY_DETAIL_SIZE_FASHION_TYPE_1_V2,
    ARRAY_DETAIL_SIZE_FASHION_TYPE_2_V2,
    ARRAY_DETAIL_SIZE_FASHION_TYPE_3_V2,
    ARRAY_DETAIL_SIZE_FASHION_TYPE_4_V2,
    ARRAY_DETAIL_SIZE_FASHION_TYPE_5_V2,
    CATEGORIES_LOCALIZED,
    KO_LANG,
    EN_LANG,
    CHAT_RESPONSE_RATE
} from "../base/variable";
import { FeedbackRepo } from "./FeedbackRepo";
import { ShippingStatus, PaymentStatus } from "../models/enums/order";
import TrackingRepo from "./TrackingRepo";
import { SCAN_MODE } from "../constants/elasticsearch";
import { Time } from "../services/cronjob/constants";

export class ProductRepo extends BaseRepository<ProductDoc>  {

    private static instance: ProductRepo;
    private static filter_type = {
        pending: 'PENDING',
        approved: 'APPROVED',
        all: 'ALL',
        stocking: 'STOCKING',
        out_stock: 'OUT_OF_STOCK',
        rejected: 'REJECTED',
        hide: 'HIDE'
    };

    private trackingRepo = TrackingRepo.getInstance();
    private shopRepo = ShopRepo.getInstance()

    constructor() {
        super();
        this.model = Product;
    }

    public static getInstance(): ProductRepo {
        if (!ProductRepo.instance) {
            ProductRepo.instance = new ProductRepo();
        }
        return ProductRepo.instance;
    }

    findBySlug(slug: string): Promise<any> {
        return this.model.findOne({ friendly_url: slug });
    }

    async getOrderedProducts(userId) {
        const orders = await OrderRepository.getInstance().find({ user_id: userId, payment_status: 'paid' });
        const orderIds: any[] = orders.map(order => {
            return order._id
        })
        const productIds = await OrderItemRepository.getInstance().getDistinctProductInOrderItems(orderIds);
        const products = await this.find({ _id: { $in: productIds } })
        return products;
    }

    async getDetail(product_id: string, user: any = null, language: string = 'vi'): Promise<any> {
        //const product = await this.model.findById(product_id);
        let results: any = null;
        try {
            let relations = [
                this.model.relationship().variants(),
                this.model.relationship().productDetailInfos(),
                this.model.relationship().shops()
            ]
            const product = await this.getExpandValues({
                _id: ObjectId(product_id), deleted_at: null
            }, relations);
            let productDTO = new ProductDTO(product[0]);
            productDTO.setUser(user);
            let getProduct: any = productDTO.toJSONWithBookMark();

            let getProductOption: any = productDTO.toJSON(['_id', 'name', 'product_detail_infos', 'variants']);
            //Handle variants field
            let variantsDTO = productDTO.getProductVariant(getProductOption.variants);
            getProduct.variants = [];
            variantsDTO.map(e => {
                let varianJson = e.toJSON(['_id', 'option_values', 'before_sale_price', 'sale_price', 'quantity', 'is_master'])
                getProduct.variants.push(varianJson);
            });
            getProduct.price_min_max = getMinMax(getProduct.variants);
            results = getProduct;        
            results = await this.getDetailAppend(results, language);
            return results;
        } catch (error) {
            throw new QueryDBError(error.message);
        }
    }

    aggregateArrayDetailSizeInfos(defaultArrayDetailSize: Array<any>, arrayDetailSizeHistory: Array<any>) {
        defaultArrayDetailSize.map(defaultEle => {
            if (Array.isArray(arrayDetailSizeHistory)) {
                const otherEle = arrayDetailSizeHistory.filter(ele => defaultEle.type_size === ele.type_size)[0]
                if (otherEle && Array.isArray(otherEle.size_infos)) {
                    defaultEle.size_infos = defaultEle.size_infos.map(size => {
                        const otherSize = otherEle.size_infos.filter(ele => size.name === ele.name)[0]
                        if (otherSize) {
                            size = { ...otherSize }
                        }
                        return size
                    })
                }
                return defaultEle;
            } else {
                return defaultEle;
            }
        })
    }

    aggregateArrayDetailSizeInfosV2(defaultArrayDetailSize: Array<any>, arrayDetailSizeHistory: Array<any>) {
        defaultArrayDetailSize.map(defaultEle => {
            if (Array.isArray(arrayDetailSizeHistory)) {
                let otherEle = arrayDetailSizeHistory.filter(ele => defaultEle.type_size === ele.type_size)[0]
                if (otherEle && Array.isArray(otherEle.size_infos)) {
                    defaultEle.size_infos = defaultEle.size_infos.map(size => {
                        let otherSize = otherEle.size_infos.filter(
                            (ele) =>
                                size.vi_name === ele.name ||
                                size.en_name === ele.name ||
                                size.ko_name === ele.name
                        )[0];
                        if (otherSize) {
                            size = {
                                ...size,
                                min: otherSize.min,
                                max: otherSize.max,
                            };
                        }
                        return size;
                    })
                }
                return defaultEle;
            } else {
                return defaultEle;
            }
        })
    }

    getDetailSize(fashionType: string, arraySize: Array<string>, detailSizeHistory: Array<any>) {
        const arrayDetailSize: Array<any> = [];
        const detailSizeHistoryByFashionType = detailSizeHistory.find(
            (el) => el.fashion_type == fashionType
        );
        let sizeInfos: any = [];
        switch (fashionType) {
            case '1':
                sizeInfos = ARRAY_DETAIL_SIZE_FASHION_TYPE_1;

                arraySize.map((typeSize) => {
                    arrayDetailSize.push({
                        type_size: typeSize,
                        size_infos: sizeInfos,
                    });
                });

                if (
                    detailSizeHistoryByFashionType &&
                    detailSizeHistoryByFashionType?.data &&
                    Array.isArray(detailSizeHistoryByFashionType?.data) &&
                    detailSizeHistoryByFashionType?.data?.length > 0
                ) {
                    this.aggregateArrayDetailSizeInfos(
                        arrayDetailSize,
                        detailSizeHistoryByFashionType.data
                    );
                }

                return arrayDetailSize;

            case '2':
                sizeInfos = ARRAY_DETAIL_SIZE_FASHION_TYPE_2;

                arraySize.map((typeSize) => {
                    arrayDetailSize.push({
                        type_size: typeSize,
                        size_infos: sizeInfos,
                    });
                });

                if (
                    detailSizeHistoryByFashionType &&
                    detailSizeHistoryByFashionType?.data &&
                    Array.isArray(detailSizeHistoryByFashionType?.data) &&
                    detailSizeHistoryByFashionType?.data?.length > 0
                ) {
                    this.aggregateArrayDetailSizeInfos(
                        arrayDetailSize,
                        detailSizeHistoryByFashionType.data
                    );
                }

                return arrayDetailSize;

            case '3':
                sizeInfos = ARRAY_DETAIL_SIZE_FASHION_TYPE_3;

                arraySize.map((typeSize) => {
                    arrayDetailSize.push({
                        type_size: typeSize,
                        size_infos: sizeInfos,
                    });
                });

                if (
                    detailSizeHistoryByFashionType &&
                    detailSizeHistoryByFashionType?.data &&
                    Array.isArray(detailSizeHistoryByFashionType?.data) &&
                    detailSizeHistoryByFashionType?.data?.length > 0
                ) {
                    this.aggregateArrayDetailSizeInfos(
                        arrayDetailSize,
                        detailSizeHistoryByFashionType.data
                    );
                }

                return arrayDetailSize;

            case '4':
                sizeInfos = ARRAY_DETAIL_SIZE_FASHION_TYPE_4;

                arraySize.map((typeSize) => {
                    arrayDetailSize.push({
                        type_size: typeSize,
                        size_infos: sizeInfos,
                    });
                });

                if (
                    detailSizeHistoryByFashionType &&
                    detailSizeHistoryByFashionType?.data &&
                    Array.isArray(detailSizeHistoryByFashionType?.data) &&
                    detailSizeHistoryByFashionType?.data?.length > 0
                ) {
                    this.aggregateArrayDetailSizeInfos(
                        arrayDetailSize,
                        detailSizeHistoryByFashionType.data
                    );
                }

                return arrayDetailSize;

            case '5':
                sizeInfos = ARRAY_DETAIL_SIZE_FASHION_TYPE_5;

                arraySize.map((typeSize) => {
                    arrayDetailSize.push({
                        type_size: typeSize,
                        size_infos: sizeInfos,
                    });
                });

                if (
                    detailSizeHistoryByFashionType &&
                    detailSizeHistoryByFashionType?.data &&
                    Array.isArray(detailSizeHistoryByFashionType?.data) &&
                    detailSizeHistoryByFashionType?.data?.length > 0
                ) {
                    this.aggregateArrayDetailSizeInfos(
                        arrayDetailSize,
                        detailSizeHistoryByFashionType.data
                    );
                }

                return arrayDetailSize;
        }
    }

    getDetailSizeV2(fashionType: string, arraySize: Array<string>, detailSizeHistory: Array<any>) {
        const arrayDetailSize: Array<any> = [];
        const detailSizeHistoryByFashionType = detailSizeHistory.find(el => el.fashion_type == fashionType)
        let sizeInfos: any = []
        
        switch (fashionType) {
            case "1":
                sizeInfos = ARRAY_DETAIL_SIZE_FASHION_TYPE_1_V2;

                arraySize.map((typeSize) => {
                    arrayDetailSize.push({
                        type_size: typeSize,
                        size_infos: sizeInfos
                    })
                })
                
                if (
                    detailSizeHistoryByFashionType &&
                    detailSizeHistoryByFashionType?.data &&
                    Array.isArray(detailSizeHistoryByFashionType?.data) &&
                    detailSizeHistoryByFashionType?.data?.length > 0
                ) {
                    this.aggregateArrayDetailSizeInfosV2(
                        arrayDetailSize,
                        detailSizeHistoryByFashionType.data
                    );
                }

                return arrayDetailSize;

            case "2":
                sizeInfos = ARRAY_DETAIL_SIZE_FASHION_TYPE_2_V2;

                arraySize.map((typeSize) => {
                    arrayDetailSize.push({
                        type_size: typeSize,
                        size_infos: sizeInfos
                    })
                })

                if (
                    detailSizeHistoryByFashionType &&
                    detailSizeHistoryByFashionType?.data &&
                    Array.isArray(detailSizeHistoryByFashionType?.data) &&
                    detailSizeHistoryByFashionType?.data?.length > 0
                ) {
                    this.aggregateArrayDetailSizeInfosV2(
                        arrayDetailSize,
                        detailSizeHistoryByFashionType.data
                    );
                }

                return arrayDetailSize;

            case "3":
                sizeInfos = ARRAY_DETAIL_SIZE_FASHION_TYPE_3_V2

                arraySize.map((typeSize) => {
                    arrayDetailSize.push({
                        type_size: typeSize,
                        size_infos: sizeInfos
                    })
                })

                if (
                    detailSizeHistoryByFashionType &&
                    detailSizeHistoryByFashionType?.data &&
                    Array.isArray(detailSizeHistoryByFashionType?.data) &&
                    detailSizeHistoryByFashionType?.data?.length > 0
                ) {
                    this.aggregateArrayDetailSizeInfosV2(
                        arrayDetailSize,
                        detailSizeHistoryByFashionType.data
                    );
                }

                return arrayDetailSize;
            case "4":
                sizeInfos = ARRAY_DETAIL_SIZE_FASHION_TYPE_4_V2

                arraySize.map((typeSize) => {
                    arrayDetailSize.push({
                        type_size: typeSize,
                        size_infos: sizeInfos
                    })
                })

                if (
                    detailSizeHistoryByFashionType &&
                    detailSizeHistoryByFashionType?.data &&
                    Array.isArray(detailSizeHistoryByFashionType?.data) &&
                    detailSizeHistoryByFashionType?.data?.length > 0
                ) {
                    this.aggregateArrayDetailSizeInfosV2(
                        arrayDetailSize,
                        detailSizeHistoryByFashionType.data
                    );
                }

                return arrayDetailSize;
            case "5":
                sizeInfos = ARRAY_DETAIL_SIZE_FASHION_TYPE_5_V2

                arraySize.map((typeSize) => {
                    arrayDetailSize.push({
                        type_size: typeSize,
                        size_infos: sizeInfos
                    })
                })

                if (
                    detailSizeHistoryByFashionType &&
                    detailSizeHistoryByFashionType.data &&
                    Array.isArray(detailSizeHistoryByFashionType?.data) &&
                    detailSizeHistoryByFashionType?.data?.length > 0
                ) {
                    this.aggregateArrayDetailSizeInfosV2(
                        arrayDetailSize,
                        detailSizeHistoryByFashionType.data
                    );
                }

                return arrayDetailSize;
        }
    }

    async getDetailAppend(product, language: string) {
        const productDetailInfoRepo = ProductDetailInfoRepo.getInstance();
        const optionTypeRepo = OptionTypeRepo.getInstance();
        const categoryRepo = CategoryRepo.getInstance();

        let optionTypes:any, category:any, productDetailInfos:any
        // @Need update performance
        [optionTypes, category, productDetailInfos] = await Promise.all([
            (async () => {
                const data = await optionTypeRepo.getByProductId(product._id);
                return data;
            })(),
            (async () => {
                const data = await categoryRepo.getCategory(product.category_id);
                return data;
            })(),
            (async () => {
                const data = await productDetailInfoRepo.getByProductId(product._id);
                return data;
            })()
        ])


        if(productDetailInfos && Array.isArray(productDetailInfos)) {
            productDetailInfos = productDetailInfos.map(element => {
                const productDetailInfo:any = cloneObj(element)
                const itemInfo = productDetailInfo.category_info
                if(itemInfo.type === "custom" || itemInfo.type === "select") {
                    // const itemValue = itemInfo.list_option.find(option => {
                    //     return option.vi === productDetailInfo.value ||
                    //         option.ko === productDetailInfo.value ||
                    //         option.en === productDetailInfo.value
                    // })
                    if(true) {
                        productDetailInfo.language = 'vi'
                        if(itemInfo.type === "custom") {
                            productDetailInfo.value = itemInfo.list_option.find(ele => ele[productDetailInfo?.language] === productDetailInfo?.value)?.[language] 
                            || productDetailInfo?.value || null
                            if(productDetailInfo.values) {
                                productDetailInfo.values = productDetailInfo.values.map(val => {
                                    val = itemInfo.list_option.find(ele => ele[productDetailInfo?.language] === val)?.[language] 
                                    || val || null
                                    return val
                                })
                                productDetailInfo.values = productDetailInfo.values.filter(ele => ele)
                                
                            }
                            else {
                                productDetailInfo.values = productDetailInfo?.value ? [ productDetailInfo?.value ] : []
                            }
                        }
                        else if(itemInfo.type === "select") {
                            productDetailInfo.value = itemInfo.list_option.find(ele => ele[productDetailInfo?.language] === productDetailInfo?.value)?.[language] || null
                            if(productDetailInfo.values) {
                                productDetailInfo.values = productDetailInfo.values.map(val => {
                                    val = itemInfo.list_option.find(ele => ele[productDetailInfo?.language] === val)?.[language] || null
                                    return val
                                })
                                productDetailInfo.values = productDetailInfo.values.filter(ele => ele)
                                
                            }
                            else {
                                productDetailInfo.values = productDetailInfo?.value ? [ productDetailInfo?.value ] : []
                            }
                        }
                        else {
                            productDetailInfo.value = productDetailInfo?.value || null
                            if(productDetailInfo.values) {
                                productDetailInfo.values = productDetailInfo.values.filter(val => val)
                            }
                            else {
                                productDetailInfo.values = productDetailInfo?.value ? [ productDetailInfo?.value ] : []
                            }
                        }
                    }
                }
                return productDetailInfo
            })
            
            productDetailInfos = productDetailInfos.map(item => {
                item = cloneObj(item)
                item.list_option = item?.category_info?.list_option || []
                item.is_required = item?.category_info?.is_required
                item.name = item?.category_info?.name
                item.type = item?.category_info?.type
                item.is_allow_multiple_values = item?.category_info?.is_allow_multiple_values
                item.list_option = item?.category_info?.list_option.map(option => {
                    option = option[language]
                    return option
                })
                return item
            })
        }

        

        const categoryLocalized = CATEGORIES_LOCALIZED.get(
            category?.name.trim().toLowerCase()
        );
        categoryLocalized && categoryLocalized[language] ? category.name = categoryLocalized[language] : '';
        
        product.option_types = optionTypes;
        product.last_category = category;
        product.product_detail_infos = productDetailInfos;
        return product;
    }

    delete(productId: string): Promise<any> {
        return this.model.findOne({ _id: productId }).updateOne({ deleted_at: new Date() });
    }

    async findProductListIncludingVariant(productIds: Array<any>, page, limit) {
        productIds = productIds.map(id => ObjectId(id))
        const match = {
            _id: { $in: productIds },
            $or: [
                { deleted_at: { $exists: false } },
                { deleted_at: null }
            ],
            allow_to_sell: true,
            is_approved: "approved",
            quantity: {$gt: 0}
        }
        const query = this.model.aggregate([
            {
                $match: match
            },
            {
                $lookup: {
                    from: "variants",
                    let: {
                        product_id: "$_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        '$expr': {
                                            $and: [
                                                {
                                                    $eq: ["$product_id", "$$product_id"]
                                                }
                                            ]
                                        },
                                    }
                                ]
                            }
                        }
                    ],
                    as: "variants"
                }
            }
        ])
        // const result = await this.model.aggregate([)
        const result = await this.model
            .aggregatePaginateCustom(query, { page, limit })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        const data = result.docs;
        return { data, paginate }
    }

    async findProductToBackPrice(productId) {
        productId = ObjectId(productId)
        const match = {
            _id: productId,
        }
        const result = await this.model.aggregate([
            {
                $match: match
            },
            {
                $lookup: {
                    from: "variants",
                    let: {
                        product_id: "$_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        '$expr': {
                                            $and: [
                                                {
                                                    $eq: ["$product_id", "$$product_id"]
                                                }
                                            ]
                                        },
                                    }
                                ]
                            }
                        }
                    ],
                    as: "variants"
                }
            }
        ])
        return result
    }

    async countProduct(shop_id: any) {
        let paginate = {
            page: 1,
            limit: 10
        }
        let data_status: any = [];
        for (const [key, value] of Object.entries(ProductRepo.filter_type)) {
            data_status.push(value);
        }
        let promises: any = []
        for (let status of data_status) {
            promises.push(new Promise((res, rej) => {
                this.findFilter({ shop_id, filter_type: status }, paginate).then(data => {
                    res({
                        _id: status,
                        count: data?.paginate?.total_record
                    })
                }).catch(error => {
                    res({
                        _id: status,
                        count: 0
                    })
                })
            }));
        }
        return await Promise.all(promises);
    }

    async findFilter(item: any, paginate: object | null, user: any = null): Promise<any> {
        let filter_type = item.filter_type;
        let shop_id = item.shop_id;
        let search = item.search
        let relations = [
            this.model.relationship().variants(),
            this.model.relationship().productDetailInfos(),
            this.model.relationship().categories(),
            this.model.relationship().shops(),
        ]
        let filter: any = { deleted_at: null }
        if (shop_id) filter.shop_id = ObjectId(shop_id);
        if (search) {
            filter = {
                $and: [
                    {
                        ...filter,
                    }, {
                        $or: [
                            {
                                name: { $regex: `${search}`, $options: 'i' },
                            }
                        ]
                    }
                ]
            }
        }
        let promiseResult = this.getExpandValues(filter, relations, { createdAt: -1 })
        switch (filter_type) {
            case ProductRepo.filter_type.pending:
                promiseResult = this.findPending(filter, relations);
                break;
            case ProductRepo.filter_type.stocking:
                promiseResult = this.findStocking(filter, relations);
                break;
            case ProductRepo.filter_type.out_stock:
                promiseResult = this.findOutStock(filter, relations);
                break;
            case ProductRepo.filter_type.rejected:
                promiseResult = this.findRejected(filter, relations);
                break;
            case ProductRepo.filter_type.hide:
                promiseResult = this.findHide(filter, relations);
                break;
            case ProductRepo.filter_type.approved:
                promiseResult = this.findApproved(filter, relations);
                break;
        }

        let result = paginate
            ? await this.model.aggregatePaginateCustom(promiseResult, paginate)
            : await promiseResult;
        result = cloneObj(result);
        let paginateRes = {}
        if (paginate) {
            paginateRes = {
                limit: result.limit,
                total_page: result.totalPages,
                page: result.page,
                total_record: result.totalDocs
            }
            result = result.docs
        }
        result = result.map(e => {
            // e = new ProductDTO(e);
            // e.setUser(user);
            // let getProduct = e.toJSON(ProductDTO.fillableList);
            let getProduct: any = new ProductDTO(e).setUser(user).toJSONWithBookMark();
            getProduct.price_min_max = getMinMax(getProduct.variants);
            getProduct.shop.user = new UserDTO(getProduct.shop.user).toSimpleJSON();
            // delete getProduct.variants;
            return getProduct;
        });
        if (paginate)
            return { data: result, paginate: paginateRes };

        return result;
    }

    findPending(filter: any | null = null, relations) {
        if (!filter) filter = {}
        filter.is_approved = this.model.const_is_approved().pending
        filter.deleted_at = null;
        let sort = { createdAt: -1 }
        return this.getExpandValues(filter, relations, sort)
    }

    findApproved(filter: any | null = null, relations) {
        if (!filter) filter = {}
        filter.is_approved = this.model.const_is_approved().approved
        filter.deleted_at = null;
        let sort = { createdAt: -1 }
        return this.getExpandValues(filter, relations, sort)
    }

    findRejected(filter: any | null = null, relations) {
        if (!filter) filter = {}
        filter.is_approved = this.model.const_is_approved().rejected
        filter.deleted_at = null;
        let sort = { createdAt: -1 }
        return this.getExpandValues(filter, relations, sort)
    }

    findHide(filter: any | null = null, relations) {
        if (!filter) filter = {}
        filter.allow_to_sell = false
        filter.deleted_at = null;
        let sort = { createdAt: -1 }
        return this.getExpandValues(filter, relations, sort)
    }

    findAll(filter: any | null = null, relations) {
        if (!filter) filter = {}
        filter.deleted_at = null;
        let sort = { createdAt: -1 }
        return this.getExpandValues(filter, relations, sort)
    }
    findStocking(filter: any | null = null, relations) {
        if (!filter) filter = {}
        filter.deleted_at = null;
        filter.quantity = { $gt: 0 }
        filter.is_approved = this.model.const_is_approved().approved
        let sort = { createdAt: -1 }
        return this.getExpandValues(filter, relations, sort);
    }
    findOutStock(filter: any | null = null, relations) {
        if (!filter) filter = {}
        filter.deleted_at = null;
        filter.is_approved = this.model.const_is_approved().approved
        filter = {
            ...filter,
            $or: [{ is_sold_out: true }, { quantity: 0 }],
        };
        let sort = { createdAt: -1 }
        return this.getExpandValues(filter, relations, sort);
    }

    async getListProductCMS() {
        const products = await this.model.find();
        return products;
    }

    async getListIdCategoryThatShopUse(shopId: String) {
        let products = await this.model.find({ shop_id: shopId });
        products = products.map(product => {
            return new ProductDTO(product).toJSON(['category_id']);
        });
        products = cloneObj(products);
        const listId = _.chain(products).map('category_id').uniq().value();
        return listId;
    }

    public findWithShop(shopId: string) {
        return this.find({
            $and: [
                {
                    shop_id: shopId,
                },
                {
                    $or: [
                        { deleted_at: { $exists: false } },
                        { deleted_at: null }
                    ]
                }
            ]
        })
    }


    public findWithShopAndIds(shopId: string, productIds: Array<string>) {
        return this.find({
            $and: [
                {
                    _id: {
                        $in: productIds
                    },
                },
                {
                    shop_id: shopId,
                },
                {
                    $or: [
                        { deleted_at: { $exists: false } },
                        { deleted_at: null }
                    ]
                },
                {
                    is_approved: "approved"
                },
                {
                    quantity: { $gt: 0 }
                },
                {
                    allow_to_sell: true
                }
            ]
        })
    }

    public findWithIds(productIds: Array<string>) {
        return this.find({
            $and: [
                {
                    _id: {
                        $in: productIds
                    },
                },
                {
                    "allow_to_sell": true
                },
                {
                    "is_approved": 'approved'
                },
                {
                    $or: [
                        { deleted_at: { $exists: false } },
                        { deleted_at: null }
                    ]
                }
            ]
        })
    }

    public async getProductsWithVariants(productCategoryIds) {
        let ids = productCategoryIds.map(function (el) { return mongoose.Types.ObjectId(el) })
        const list = await this.model.aggregate([
            {
                $match: {
                    _id: { $in: ids },
                    $or: [
                        { deleted_at: { $exists: false } },
                        { deleted_at: null }
                    ],
                    is_approved: "approved"
                }
            },
            {
                $limit: 5
            },
            {
                $sort: { updatedAt: -1 }
            },
            {
                $lookup: {
                    from: "variants",
                    let: {
                        product_id: "$_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        '$expr': {
                                            $and: [
                                                {
                                                    $eq: ["$product_id", "$$product_id"]
                                                }
                                            ]
                                        },
                                    }
                                ]
                            }
                        }
                    ],
                    as: "variants"
                }

            }
        ]);

        return list;
    }

    public async getValidProductByShopId(id, page, limit, stocking = false, includeRelation = true) {
        const conditions: any[] = [
            { shop_id: ObjectId(id) },
            ...ProductQueryHelper.condition.active,
        ]
        if(stocking) {
            conditions.push({is_sold_out: false})
            conditions.push({quantity: {$gt: 0}})
        }
        const match_condition: any = {
            "$and": conditions
        };
        const query: Array<any> = [];
        query.push({ $match: match_condition });

        if(includeRelation) {
            query.push(ProductQueryHelper.relations.variants);
            query.push(ProductQueryHelper.relations.shop)
            query.push(ProductQueryHelper.relations.unwind_shop)
        }
        
        query.push({ $sort: { createdAt: -1 } })

        const aggregate = this.model.aggregate(query);
        const result = await this.model
            .aggregatePaginateCustom(aggregate, { page, limit })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }

        const data = result.docs;
        return { data, paginate }
    }

    public async findProductForExplore(request: ShopListProductRequest, page, limit): Promise<IPaginateData> {

        const { shop_id, orderBy } = request;

        const match_condition: any = {
            "$and": [
                { shop_id: ObjectId(shop_id.toString()) },
                ...ProductQueryHelper.condition.active,
            ]
        };
        const query: Array<any> = [];
        query.push({ $match: match_condition });
        query.push(ProductQueryHelper.addField.stocking)
        query.push(ProductQueryHelper.relations.variants);
        query.push(ProductQueryHelper.relations.shop);
        query.push(ProductQueryHelper.relations.unwind_shop);
        const sort_condition: any = { };
        
        switch (orderBy) {
            case ProductSort.Price_ASC:
                query.push(ProductQueryHelper.addField.minPrice);
                query.push({ $group : { _id : "$_id", product: { $first: "$$ROOT" } } })
                sort_condition["product.stocking"] = -1;
                sort_condition["product.minPrice"] = 1;
                sort_condition["product.is_genuine_item"] = -1;
                sort_condition["product.is_guaranteed_item"] = -1;
                sort_condition["product._id"] = -1;
                break;
            case ProductSort.Price_DESC:
                query.push(ProductQueryHelper.addField.minPrice);
                query.push({ $group : { _id : "$_id", product: { $first: "$$ROOT" } } })
                sort_condition["product.stocking"] = -1;
                sort_condition["product.minPrice"] = -1;
                sort_condition["product.is_genuine_item"] = -1;
                sort_condition["product.is_guaranteed_item"] = -1;
                sort_condition["product._id"] = -1;
                break;
            case ProductSort.POPULATE:
                query.push({ $group : { _id : "$_id", product: { $first: "$$ROOT" } } })
                sort_condition["product.stocking"] = -1;
                sort_condition["product.popular_mark"] = -1;
                sort_condition["product.is_genuine_item"] = -1;
                sort_condition["product.is_guaranteed_item"] = -1;
                sort_condition["product._id"] = -1;
                break;
            case ProductSort.NEWEST:
                query.push({ $group : { _id : "$_id", product: { $first: "$$ROOT" } } })
                sort_condition["product.stocking"] = -1;
                sort_condition["product.is_genuine_item"] = -1;
                sort_condition["product.is_guaranteed_item"] = -1;
                sort_condition["product.createdAt"] = -1;
                break;
            case ProductSort.BEST_SELL:
                query.push({ $group : { _id : "$_id", product: { $first: "$$ROOT" } } })
                sort_condition["product.stocking"] = -1;
                sort_condition["product.sold"] = -1;
                sort_condition["product.is_genuine_item"] = -1;
                sort_condition["product.is_guaranteed_item"] = -1;
                sort_condition["product._id"] = -1;
                break;
            default: break;
        }

        if (Object.keys(sort_condition).length > 0)
            query.push({ $sort: sort_condition });

        const aggregate = this.model.aggregate(query)
        const result = await this.model
            .aggregatePaginateCustom(aggregate, { page, limit })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }

        let data = result.docs;
        data = data.map(item => item.product)

        return { data, paginate }
    }

    public async findProductHome(request: any, page, limit): Promise<IPaginateData> {

        const { orderBy, categoryID } = request;
        let match_condition: any = {
            "$and": [
                ...ProductQueryHelper.condition.active,
                {quantity: {$gt: 0}},
                {is_sold_out: false },
            ]
        };

        if (categoryID) {
            match_condition = {
                "$and": [
                    ...ProductQueryHelper.condition.active,
                    { category_id: ObjectId(categoryID) },
                    {quantity: {$gt: 0}},
                    { is_sold_out: false },
                ]
            }
        }
        const query: Array<any> = [];
        query.push({ $match: match_condition });
        query.push(ProductQueryHelper.addField.stocking)
        query.push(ProductQueryHelper.relations.variants);
        query.push(ProductQueryHelper.relations.shop)
        query.push(ProductQueryHelper.relations.unwind_shop)


        const sort_condition: any = {};
        switch (orderBy) {
            case ProductSort.Price_ASC:
                query.push(ProductQueryHelper.addField.minPrice);
                query.push({ $group : { _id : "$_id", product: { $first: "$$ROOT" } } })
                sort_condition["product.stocking"] = -1;
                sort_condition["product.minPrice"] = 1;
                sort_condition["product.is_genuine_item"] = -1;
                sort_condition["product.is_guaranteed_item"] = -1;
                sort_condition["product._id"] = -1;
                break;
            case ProductSort.Price_DESC:
                query.push(ProductQueryHelper.addField.minPrice);
                query.push({ $group : { _id : "$_id", product: { $first: "$$ROOT" } } })
                sort_condition["product.stocking"] = -1;
                sort_condition["product.minPrice"] = -1;
                sort_condition["product.is_genuine_item"] = -1;
                sort_condition["product.is_guaranteed_item"] = -1;
                sort_condition["product._id"] = -1;
                break;
            case ProductSort.POPULATE:
                query.push({ $group : { _id : "$_id", product: { $first: "$$ROOT" } } })
                sort_condition["product.stocking"] = -1;
                sort_condition["product.popular_mark"] = -1;
                sort_condition["product.is_genuine_item"] = -1;
                sort_condition["product.is_guaranteed_item"] = -1;
                sort_condition["product._id"] = -1;
                break;
            case ProductSort.NEWEST:
                sort_condition.createdAt = -1;
                query.push({ $group : { _id : "$_id", product: { $first: "$$ROOT" } } })
                sort_condition["product.stocking"] = -1;
                sort_condition["product.is_genuine_item"] = -1;
                sort_condition["product.is_guaranteed_item"] = -1;
                sort_condition["product.createdAt"] = -1;
                break;
            case ProductSort.BEST_SELL:
                query.push({ $group : { _id : "$_id", product: { $first: "$$ROOT" } } })
                sort_condition["product.stocking"] = -1;
                sort_condition["product.sold"] = -1;
                sort_condition["product.is_genuine_item"] = -1;
                sort_condition["product.is_guaranteed_item"] = -1;
                sort_condition["product._id"] = -1;
                break;
            default: break;
        }

        if (Object.keys(sort_condition).length > 0)
            query.push({ $sort: sort_condition });

        const aggregate = this.model.aggregate(query);
        const result = await this.model
            .aggregatePaginateCustom(aggregate, { page, limit })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }

        let data = result.docs;
        data = cloneObj(data)
        data = data.map(item => {
            item.product.discount_percent = item.product.discount_percent ? item.product.discount_percent.discount : 0
            return item.product
        })

        return { data, paginate }
    }

    public async getALlProductSyncELS(scanMode: SCAN_MODE): Promise<any> {

        try {
            let match: any = {
                deleted_at: null ,
                is_approved: "approved" ,
                //allow_to_sell: true 
            };

            let shopUpdated: any = {
                deleted_at: null,
                pause_mode: false
            };
    
            switch (scanMode) {
                case SCAN_MODE.FIVE_MINUTE:
                    const fiveMinuteAgo = new Date(Date.now() -  Time.FiveMinute);
                    match.updatedAt = { $gte: fiveMinuteAgo } 
                    shopUpdated.updatedAt = { $gte: fiveMinuteAgo } 
    
                    break;
    
                case SCAN_MODE.HOURLY:
                    const oneHourAgo = new Date(Date.now() -  Time.OneHour);
                    match.updatedAt = { $gte: oneHourAgo};
                    shopUpdated.updatedAt = { $gte: oneHourAgo};
    
                    break;
    
                case SCAN_MODE.ALL:
                    match = {
                        deleted_at: null 
                    };
                    shopUpdated = undefined;
    
                    break;
            
                default:
                    break;
            }
    
            let products = await this.model.find(match);

            let productUpdateByShop: any = [];
            let compositeProducts: any = [];
            if (shopUpdated) {
                productUpdateByShop = await this.shopRepo.getProductWhenShopUpdatedAt(shopUpdated);
                if (isMappable(productUpdateByShop)) {
                    productUpdateByShop = cloneObj(productUpdateByShop);
                    
                    for (let shop of productUpdateByShop) {
                        compositeProducts = compositeProducts.concat(shop?.products?.map((item) => item._id));
                    }
                }
            }

            if (!isMappable(products)) return [];
            products = cloneObj(products);
            products = products?.map((item) => item._id)

            products = [ ... new Set(products.concat(compositeProducts))]

            const result = await Promise.all(
                products.map(async (id) => {
                    return detailProductFromBuyer(id);
                })
            )
    
            return result;
            
        } catch (error) {
            console.log(error);
        }  
    }

    public async searchProductForExplore(request: ShopSearchProductRequest, page, limit): Promise<IPaginateData> {

        const { keyword,
            category,
            location,
            ship_provider,
            price_min,
            price_max,
            status,
            pay_option,
            rate,
            services,
            orderBy,
            shop_id } = request;

        const query: Array<any> = [];

        const condition: Array<any> = [
            ...ProductQueryHelper.condition.active,
            {quantity: {$gt: 0}},
            {is_sold_out: false},
        ]

        if (shop_id) condition.unshift({ shop_id: ObjectId(shop_id.toString()) })

        if (keyword) {

            const searchQuery = {
                $or: [
                    BaseQueryHelper.fieldSearch(keyword, 'name'),
                    BaseQueryHelper.fieldSearchStripHTML(keyword, 'description'),
                ],
            }
            condition.push(searchQuery)
        }

        if (category) {
            query.push(ProductQueryHelper.relations.category);
            condition.push({
                "$or": [
                    { category_id: ObjectId(category.toString()) },
                    {
                        product_categories: {
                            $elemMatch: { category_id: ObjectId(category.toString()) }
                        }
                    },
                    {
                        list_category_id: { $in: [category.toString()]}
                    }
                ]
            })
        }
        const match_condition: any = {
            "$and": condition
        };

        query.push({ $match: match_condition });
        query.push(ProductQueryHelper.addField.stocking)
        query.push(ProductQueryHelper.relations.variants);
        query.push(ProductQueryHelper.relations.shop);
        query.push(ProductQueryHelper.relations.unwind_shop);
        query.push(ProductQueryHelper.relations.promotion)
        query.push(ProductQueryHelper.relations.unwind_discount)
        if (price_max > 0 || price_min > 0) {
            query.push(ProductQueryHelper.addField.minPrice);
            if (price_min && !price_max)
                query.push(ProductQueryHelper.condition.minPriceMatch(price_min));
            else if (!price_min && price_max)
                query.push(ProductQueryHelper.condition.maxPriceMatch(price_max));
            else query.push(ProductQueryHelper.condition.minPriceMatchRange(price_min, price_max));
        }

        const sort_condition: any = {};
        switch (orderBy) {
            case ProductSort.Price_ASC:
                if (!(price_max || price_min)) // not added above yet
                    query.push(ProductQueryHelper.addField.minPrice);
                query.push({ $group : { _id : "$_id", product: { $first: "$$ROOT" } } })
                sort_condition["product.stocking"] = -1;
                sort_condition["product.minPrice"] = 1;
                sort_condition["product.is_genuine_item"] = -1;
                sort_condition["product.is_guaranteed_item"] = -1;
                sort_condition["product._id"] = -1;
                break;
            case ProductSort.Price_DESC:
                if (!(price_max || price_min)) // not added above yet
                    query.push(ProductQueryHelper.addField.minPrice);
                query.push({ $group : { _id : "$_id", product: { $first: "$$ROOT" } } })
                sort_condition["product.stocking"] = -1;
                sort_condition["product.minPrice"] = -1;
                sort_condition["product.is_genuine_item"] = -1;
                sort_condition["product.is_guaranteed_item"] = -1;
                sort_condition["product._id"] = -1;
                break;
            case ProductSort.POPULATE:
                query.push({ $group : { _id : "$_id", product: { $first: "$$ROOT" } } })
                sort_condition["product.stocking"] = -1;
                sort_condition["product.popular_mark"] = -1;
                sort_condition["product.is_genuine_item"] = -1;
                sort_condition["product.is_guaranteed_item"] = -1;
                sort_condition["product._id"] = -1;
                break;
            case ProductSort.NEWEST:
                sort_condition.createdAt = -1;
                query.push({ $group : { _id : "$_id", product: { $first: "$$ROOT" } } })
                sort_condition["product.stocking"] = -1;
                sort_condition["product.is_genuine_item"] = -1;
                sort_condition["product.is_guaranteed_item"] = -1;
                sort_condition["product.createdAt"] = -1;
                break;
            case ProductSort.BEST_SELL:
                query.push({ $group : { _id : "$_id", product: { $first: "$$ROOT" } } })
                sort_condition["product.stocking"] = -1;
                sort_condition["product.sold"] = -1;
                sort_condition["product.is_genuine_item"] = -1;
                sort_condition["product.is_guaranteed_item"] = -1;
                sort_condition["product._id"] = -1;
                break;
            default: break;
        }

        query.push({ $sort: sort_condition });

        const aggregate = this.model.aggregate(query);

        const result = await this.model
            .aggregatePaginateCustom(aggregate, { page, limit });

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }

        let data = result.docs;
        data = cloneObj(data)
        data = data.map(item => {
            item.product.discount_percent = item.product.discount_percent ? item.product.discount_percent.discount : 0
            return item.product
        })
        return { data, paginate }
    }

    public async findProductSimilar(
        request: { productId: String, userId?: String },
        page,
        limit
    ): Promise<IPaginateData> {
        // return;
        const productId = request.productId;
        const userId = request.userId;
        let product: any = null;
        try {
            product = await this.getDetail(productId.toString());
        } catch (error) {
            console.log(error)
            throw new Error("Sản phẩm không hợp lệ")
        }
        const systemCategory: String = product.category_id;
        const shopCategory: Array<String> = product.list_category_id;
        const similarQuery = SimilarProductStategy.byCategoryBiasQuery(systemCategory, shopCategory, userId);

        const query: any = [];
        query.push({
            $match: {
                $and: [
                    { _id: { $ne: ObjectId(productId.toString()) } },
                    { is_approved: "approved" },
                    { allow_to_sell: true },
                    {
                        $or: [
                            { deleted_at: { $exists: false } },
                            { deleted_at: null }
                        ]
                    },
                    { quantity: { $gt: 0 } }
                ]
            }
        });

        query.push(ProductQueryHelper.relations.variants);
        query.push(ProductQueryHelper.relations.shop)
        query.push(ProductQueryHelper.relations.unwind_shop)
        query.push(ProductQueryHelper.relations.promotion)
        query.push(ProductQueryHelper.relations.unwind_discount)
        query.push(...similarQuery);
        const aggregate = this.model.aggregate(query);

        const result = await this.model
            .aggregatePaginateCustom(aggregate, { page, limit });

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }

        let data = result.docs;
        data = cloneObj(data)
        data = data.map(data => {
            data.discount_percent = data.discount_percent ? data.discount_percent.discount : 0
            return data
        })
        return { data, paginate }
    }

    // async findListProductInternational(item: any, paginate: object | null, user: any = null): Promise<any> {
    //     let search = item.search
    //     let relations = [
    //         this.model.relationship().variants(),
    //         this.model.relationship().productDetailInfos(),
    //         this.model.relationship().categories(),
    //         this.model.relationship().shops(),
    //     ]
    //     let filter: any = { deleted_at: null, is_approved: "approved", allow_to_sell: true }

    //     if (search) {
    //         filter.name = { $regex: `${search}`, $options: 'i' }
    //     }
    //     let promiseResult = this.getExpandValues(filter, relations, { createdAt: -1 })
    //     promiseResult = promiseResult.match({
    //         "shop.country": { $ne: "VN" },
    //     })

    //     let result = paginate
    //         ? await this.model.aggregatePaginateCustom(promiseResult, paginate)
    //         : await promiseResult;
    //     result = cloneObj(result);
    //     let paginateRes = {}
    //     if (paginate) {
    //         paginateRes = {
    //             limit: result.limit,
    //             total_page: result.totalPages,
    //             page: result.page,
    //             total_record: result.totalDocs
    //         }
    //         result = result.docs
    //     }
    //     result = result.map(e => {
    //         let getProduct: any = new ProductDTO(e).setUser(user).toJSONWithBookMark();
    //         getProduct.price_min_max = getMinMax(getProduct.variants);
    //         getProduct.shop.user = new UserDTO(getProduct.shop.user).toSimpleJSON();
    //         delete getProduct.variants;
    //         return getProduct;
    //     });
    //     if (paginate)
    //         return { data: result, paginate: paginateRes };

    //     return result;
    // }

    async findListProductInternational(page, limit, internationalCategory, internationalShops) {
        const match = {
            $or: [
                { category_id: internationalCategory },
                { shop_id: { $in: internationalShops }}
            ],
            deleted_at: null,
            $and: ProductQueryHelper.condition.active
        }
        const query = this.model.aggregate([
            {
                $match: match
            },
            {
                $lookup: {
                    from: "variants",
                    let: {
                        product_id: "$_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        '$expr': {
                                            $and: [
                                                {
                                                    $eq: ["$product_id", "$$product_id"]
                                                }
                                            ]
                                        },
                                    }
                                ]
                            }
                        }
                    ],
                    as: "variants"
                }
            },
            ProductQueryHelper.relations.shop,
            ProductQueryHelper.relations.unwind_shop
        ])
        const result = await this.model
            .aggregatePaginateCustom(query, { page, limit })
        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        const data = result.docs;
        return { data, paginate }
    }


    async getDetailWhenUserGoToShopExplore(product_id: string, user: any = null, language: string): Promise<any> {
        //let streamSessionRepository = StreamSessionRepository.getInstance();
        let results: any = null;

        try {
            let relations = [
                this.model.relationship().variants(),
                this.model.relationship().productDetailInfos(),
                this.model.relationship().shops(),
                this.model.relationship().categories(),
            ]
            const product = await this.getExpandValues({
                _id: ObjectId(product_id), deleted_at: null
            }, relations);
            if (!product.length) {
                throw new NotFoundError("Sản phầm không tồn tại")
            }
            let productDTO = new ProductDTO(product[0]);
            let getProduct: any = productDTO.toJSONForELS();
            let getProductOption: any = productDTO.toJSON(['_id', 'name', 'product_detail_infos', 'variants', 'shop']);
            //Handle variants field
            let variantsDTO = productDTO.getProductVariant(getProductOption.variants);
            getProduct.variants = [];
            getProduct.variants = variantsDTO.map(variant => {
                return variant.toJSON(['_id', 'option_values', 'before_sale_price', 'sale_price', 'quantity', 'is_master'])
            });
            getProduct.price_min_max = getMinMax(getProduct.variants);

            getProduct.product_detail_infos = await this.getDetailCategoryInfo(getProductOption.product_detail_infos, language);
            getProduct = this.fakeProductDataInfoMore(getProduct)

            let shopRepo = ShopRepo.getInstance();
            let detail_shop:any = await shopRepo.getDetailShop(getProduct.shop_id);
            let feedbackRepo = FeedbackRepo.getInstance()
            const averageFeedbackRate = await feedbackRepo.getFeedbackAvgByShopID(getProduct.shop_id)
            const optionTypes = await OptionTypeRepo.getInstance().getByProductId(product_id);
            const listProducts = await this.getValidProductByShopId(getProduct.shop_id, 1, 5, true, false);

            // Chat Reeponse Rate
            getProduct.shop.products = listProducts.data.map(product => new ProductDTO(product).toSimpleProductItem())
            getProduct.shop.avatar = addLink(`${keys.host_community}/`, detail_shop?.user?.gallery_image?.url)
            getProduct.shop.follow_count = detail_shop?.user?.followCount || 0
            getProduct.shop.following_count = detail_shop?.user?.followingCount || 0
            getProduct.shop.rating = parseFloat(averageFeedbackRate.toFixed(1));
            getProduct.shop.is_live = false; //await streamSessionRepository.getStreamSessionLiveByShop(getProduct.shop_id);
            getProduct.shop.chat_response_rate = CHAT_RESPONSE_RATE
            // average shipping time of shop (by day) - need apply logic
            getProduct.shop.avg_shipping_time = {
                min: 3,
                max: 5,
            }
            getProduct.option_types = optionTypes;
            //getProduct.sold = getProduct?.sold < 10 ? 0 : getProduct?.sold;
            results = getProduct;

            return results;
        } catch (error) {
            throw executeError(error);
        }
    }

    async getDetailCategoryInfo(product_detail_infos, language: string = null) {
        let categoryInfoRepo = CategoryInfoRepo.getInstance();
        let promiseAddCategoryInfo: any = [];
        promiseAddCategoryInfo = product_detail_infos.map((detail_infos) => {
            return new Promise(async (resolve, reject) => {
                categoryInfoRepo.findById(detail_infos.category_info_id)
                    .then(category_info => {
                        if (category_info) {
                            resolve({
                                name: language ? category_info?.name[language] : category_info?.name,
                                value: detail_infos?.value,
                                values: detail_infos?.values || []
                            })
                        }
                        else {
                            resolve(null)
                        }
                    });
            })
        });
        let result: any = await Promise.all(promiseAddCategoryInfo);
        result = result.filter(data => data)
        return result
    }

    async getOtherProductOfShop(page, limit, shopId, productId) {
        const match = {
            $or: [
                { deleted_at: { $exists: false } },
                { deleted_at: null }
            ],
            shop_id: ObjectId(shopId),
            _id: { $ne: ObjectId(productId) }
        }
        const query = this.model.aggregate([
            {
                $match: match
            },
            {
                $lookup: {
                    from: "variants",
                    let: {
                        product_id: "$_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        '$expr': {
                                            $and: [
                                                {
                                                    $eq: ["$product_id", "$$product_id"]
                                                }
                                            ]
                                        },
                                    }
                                ]
                            }
                        }
                    ],
                    as: "variants"
                }
            }
        ])
        const result = await this.model
            .aggregatePaginateCustom(query, { page, limit })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        const data = result.docs;
        return { data, paginate }
    }

    /**
     * 
     * @param param0 
     * @returns 
     */
    async getProductOfShopForStatic({
        page, limit, shop_id,
        sort_direction, type, category_id,
        start_time, end_time
    }) {

        const aggregate: any = [
            {
                $match: {
                    $or: [
                        { deleted_at: { $exists: false } },
                        { deleted_at: null },
                        { is_approved: "approved" }
                    ],
                    shop_id: ObjectId(shop_id),
                }
            },
            ProductQueryHelper.relations.variants,
        ]

        if (category_id) {
            let categoryIds = [ObjectId(category_id)];
            const categoryRepo = CategoryRepo.getInstance();
            const categoryWithChild = await categoryRepo.getTreeById(category_id);
            if (categoryWithChild) {
                categoryIds = [...categoryIds, ...categoryWithChild.childs.map(e => ObjectId(e._id))];
            }
            aggregate.push({
                $match: {
                    category_id: { $in: categoryIds }
                }
            })
        }

        let time_condition_range_previous: any = [];
        let time_condition_range_now: any = [];
        if (start_time || end_time) {
            const previous_time = 2 * start_time - end_time;
            if (previous_time) {
                time_condition_range_previous.push({ createdAt: { $gte: new Date(previous_time) } });
            }
            if (start_time) {
                time_condition_range_previous.push({ createdAt: { $lte: start_time } });
                time_condition_range_now.push({ createdAt: { $gte: start_time } });
            }
            if (end_time) {
                time_condition_range_now.push({ createdAt: { $lte: end_time } });
            }
        }

        switch (type) {
            case ProductRankingType.REVENUE:
                aggregate.push(
                    ProductQueryHelper.relations.ordersWithTotal([...time_condition_range_previous], 'orderitems_previous'),
                    ProductQueryHelper.relations.ordersWithTotal([...time_condition_range_now], 'orderitems_now'),
                    ProductQueryHelper.addField.orderRevenue('orderitems_previous', 'order_revenue_previous'),
                    ProductQueryHelper.addField.orderRevenue('orderitems_now', 'order_revenue'),
                    { $sort: { order_revenue: sort_direction, createdAt: -1 } }
                )
                break;
            case ProductRankingType.SOLD_AMOUNT:
                aggregate.push(
                    ProductQueryHelper.relations.ordersWithTotal([...time_condition_range_previous], 'orderitems_previous'),
                    ProductQueryHelper.relations.ordersWithTotal([...time_condition_range_now], 'orderitems_now'),
                    ProductQueryHelper.addField.orderQuantity('orderitems_previous', 'order_quantity_previous'),
                    ProductQueryHelper.addField.orderQuantity('orderitems_now', 'order_quantity'),
                    { $sort: { order_quantity: sort_direction } }
                )
                break;
            case ProductRankingType.VIEW_AMOUNT:
                let time_condition_previous: any = [];
                let time_condition_now: any = [];
                if (start_time || end_time) {
                    const previous_time = 2 * start_time - end_time;
                    if (previous_time) {
                        time_condition_previous.push({ "visited_ats": { $gte: new Date(previous_time) } });
                    }
                    if (start_time) {
                        time_condition_previous.push({ "visited_ats": { $lte: start_time } });
                        time_condition_now.push({ "visited_ats": { $gte: start_time } });
                    }
                    if (end_time) {
                        time_condition_now.push({ "visited_ats": { $lte: end_time } });
                    }
                }
                let condition = {
                    target_type: 'Product',
                    actor_type: 'User',
                    action_type: TrackingType.ViewProduct,
                };
                aggregate.push(
                    ProductQueryHelper.relations.activitiesCondition([condition], "activities_previous", [...time_condition_previous]),
                    ProductQueryHelper.relations.activitiesCondition([condition], "activities_now", [...time_condition_now]),
                    ProductQueryHelper.addField.activityNumberGenerate('activities_previous', 'product_view_previous'),
                    ProductQueryHelper.addField.activityNumberGenerate('activities_now', 'product_view'),
                    { $sort: { product_view: sort_direction } }
                )
                break;
            default: break;
        }

        const query = this.model.aggregate(aggregate)

        const result = await this.model
            .aggregatePaginateCustom(query, { page, limit })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        const data = result.docs;
        return { data, paginate }
    }

    async getProductOfShopForSoldAmount({
        page, limit, shop_id,
        sort_direction = -1, category_id
    }) {
        const aggregate: any = [
            {
                $match: {
                    $and: [
                        { deleted_at: null },
                        { is_approved: 'approved' },
                        { sold: { $gt: 0 } },
                        { shop_id: ObjectId(shop_id) },
                    ],
                },
            },
            {
                $sort: { sold: sort_direction, createdAt: -1 },
            },
        ];

        if (category_id) {
            let categoryIds = [ObjectId(category_id)];
            const categoryRepo = CategoryRepo.getInstance();
            const categoryWithChild = await categoryRepo.getTreeById(category_id);
            if (categoryWithChild) {
                categoryIds = [...categoryIds, ...categoryWithChild.childs.map(e => ObjectId(e._id))];
            }
            aggregate.push({
                $match: {
                    category_id: { $in: categoryIds }
                }
            })
        }

        const query = this.model.aggregate(aggregate)

        const result = await this.model
            .aggregatePaginateCustom(query, { page, limit })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        const data = result.docs;
        return { data, paginate }
    }

    fakeProductDataInfoMore(product) {
        let info_more = {
            satisfied: 90,
            like: 190,
            view_in_month: 98
        }
        product.info_more = info_more;
        return product;
    }

    /**
     * 
     * @param product_id Id or product
     * Product must be (allowing to sell) and (approved) and [(quantity greater than 0) or [(quantity less than or equal to 0) and (allowing to pre order)]]
     */
    async findProductForAddToCart(product_id: any) {
        const query: Array<any> = [
            {
                $match: {
                    _id: ObjectId(product_id),
                    allow_to_sell: true,
                    is_approved: 'approved',
                    $or: [
                        {
                            quantity: { '$gt': 0 }
                        },
                        {
                            quantity: { '$lte': 0 },
                            is_pre_order: true
                        }
                    ]
                }
            },
            {
                $lookup: {
                    from: "variants",
                    localField: "_id",
                    foreignField: "product_id",
                    as: "variants"
                }
            }
        ]
        return this.model.aggregate(query)
    }

    async getSuggestProducts(page, limit) {
        const query: Array<any> = [];
        const condition: Array<any> = [
            ...ProductQueryHelper.condition.active,
        ]

        const match_condition: any = {
            "$and": condition
        };

        query.push({ $match: match_condition });
        query.push(ProductQueryHelper.relations.variants);
        const aggregate = this.model.aggregate(query);
        const result = await this.model
            .aggregatePaginateCustom(aggregate, { page, limit })
        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        const data = result.docs;
        return { data, paginate }
    }

    async getProductByKeyword(keyword: string, page: number, limit: number) {

        const query: Array<any> = [];

        const condition: Array<any> = [
            ...ProductQueryHelper.condition.active,
        ]
        
        const match_condition: any = {
            "$and": condition
        };

        if (keyword) {
            match_condition.$or = []
            match_condition.$or.push(BaseQueryHelper.fieldSearch(keyword, 'name'))
            match_condition.$or.push(BaseQueryHelper.fieldSearchStripHTML(keyword, 'description'))
        }

        query.push({ $match: match_condition });
        query.push(ProductQueryHelper.relations.variants);
        query.push(ProductQueryHelper.relations.shop);
        query.push(ProductQueryHelper.relations.unwind_shop);

        const sort_condition = { "name": 1 };
        query.push({ $sort: sort_condition });

        const aggregate = this.model.aggregate(query);

        const result = await this.model
            .aggregatePaginateCustom(aggregate, { page, limit });

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }

        const data = result.docs;
        return { data, paginate }
    }

    async getProductByBanner(product_ids: any, page: number, limit: number) {
        const query: Array<any> = [];

        const condition: Array<any> = [
            ...ProductQueryHelper.condition.active,
        ]

        product_ids = product_ids.map(id => ObjectId(id))

        if (product_ids) condition.push({ _id: { $in: product_ids } })

        const match_condition: any = {
            "$and": condition
        };

        query.push({ $match: match_condition });
        query.push(ProductQueryHelper.relations.variants);
        query.push(ProductQueryHelper.relations.option_types);

        const sort_condition = { "name": 1 };
        query.push({ $sort: sort_condition });

        const aggregate = this.model.aggregate(query);

        const result = await this.model
            .aggregatePaginateCustom(aggregate, { page, limit });

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }

        const data = result.docs;
        return { data, paginate }
    }

    async countProductByKeyword(keyword: string) {
        const query: Array<any> = [];

        const condition: Array<any> = [
            ...ProductQueryHelper.condition.active,
        ]

        if (keyword) {
            condition.push({
                $or: [
                    BaseQueryHelper.fieldSearch(keyword, 'name'),
                    BaseQueryHelper.fieldSearchStripHTML(keyword, 'description')
                ]
            })
        } 

        const match_condition: any = {
            "$and": condition
        };

        query.push({ $match: match_condition });
        return this.model.aggregate(query);
    }

    async getListProductOfCategorySystem(categoryId, page, limit) {
        const match = {
            $and: [
                ...ProductQueryHelper.condition.active,
                {quantity: {$gt: 0}},
                {is_sold_out: false},
            ],
            $or: [
                { deleted_at: { $exists: false } },
                { deleted_at: null }
            ],
            list_category_id: { $in: [categoryId] },
        }
        const query = this.model.aggregate([
            {
                $match: match
            },
            {
                $lookup: {
                    from: "variants",
                    let: {
                        product_id: "$_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        '$expr': {
                                            $and: [
                                                {
                                                    $eq: ["$product_id", "$$product_id"]
                                                }
                                            ]
                                        },
                                    }
                                ]
                            }
                        }
                    ],
                    as: "variants"
                }
            },
            ProductQueryHelper.relations.shop,
            ProductQueryHelper.relations.unwind_shop
        ])
        const result = await this.model
            .aggregatePaginateCustom(query, { page, limit })
        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        const data = result.docs;
        return { data, paginate }
    }

    async getListNotInTopProdctOfShop(shopIds, products) {
        const productIds = products.map(item => ObjectId(item._id));
        const match = {
            $or: [
                { deleted_at: { $exists: false } },
                { deleted_at: null }
            ],
            shop_id: ObjectId(shopIds),
            _id: { $nin: productIds }
        }
        const query = await this.model.aggregate([
            {
                $match: match
            },
            {
                $limit: 5
            },
            {
                $lookup: {
                    from: "variants",
                    let: {
                        product_id: "$_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        '$expr': {
                                            $and: [
                                                {
                                                    $eq: ["$product_id", "$$product_id"]
                                                }
                                            ]
                                        },
                                    }
                                ]
                            }
                        }
                    ],
                    as: "variants"
                }
            },
            {
                $lookup: {
                    from: "orderitems",
                    let: {
                        product_id: "$_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        '$expr': {
                                            $and: [
                                                {
                                                    $eq: ["$product_id", "$$product_id"]
                                                }
                                            ]
                                        },
                                    }
                                ]
                            }
                        },
                        {
                            $count: "sold"
                        }
                    ],
                    as: "order_items"
                }
            }
        ])

        return query;

    }

    async getProductValidForPromotion(shop_id, product_ids, page, limit) {
        const query = this.model.aggregate([
            {
                $match: {
                    $and: [
                        ...ProductQueryHelper.condition.active,
                        { quantity: { $gt: 0 } },
                        { _id: { $nin: product_ids } },
                        { shop_id: ObjectId(shop_id) }
                    ]
                }
            },
            ProductQueryHelper.relations.variants
        ])
        const result = await this.model
            .aggregatePaginateCustom(query, { page, limit })
        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        const data = result.docs;
        return { data, paginate }
    }

    async getValidBookmarkedProduct(product_ids, page, limit) {
        product_ids = product_ids.map(id => ObjectId(id))
        const query:Array<any> = [
            {
                $match: {
                    $and: [
                        ...ProductQueryHelper.condition.active,
                        { quantity: { $gt: 0 } },
                        { _id: { $in: product_ids } },
                    ]
                }
            },
            ProductQueryHelper.relations.variants
        ]
        query.push(ProductQueryHelper.relations.shop)
        query.push(ProductQueryHelper.relations.unwind_shop)
        query.push(ProductQueryHelper.relations.promotion)
        query.push(ProductQueryHelper.relations.unwind_discount)

        query.push({ $sort: { created_at: -1 } })
        const queryResult = this.model.aggregate(query)
        
        const result = await this.model
            .aggregatePaginateCustom(queryResult, { page, limit })
        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        const data = result.docs;
        return { data, paginate }
    }

    async getProductByShopCategory(product_ids, page, limit) {
        product_ids = product_ids.map(s => mongoose.Types.ObjectId(s));
        const query = this.model.aggregate([
            {
                $match: {
                    _id: { $in: product_ids },
                    deleted_at: null,
                    is_approved: "approved"
                }
            }
        ])
        const result = await this.model
            .aggregatePaginateCustom(query, { page, limit })
        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        const data = result.docs;
        return { data, paginate }
    }

    async getProductWithSoldQuantity() {
        // Get order success (paid and shipped)
        let orderSuccessIDs = await OrderRepository.getInstance().getOrderSuccess()
        orderSuccessIDs = orderSuccessIDs.map(item => ObjectId(item._id))

        // Get product sold from order success
        const match_condition: any = {
            "$and": [
                ...ProductQueryHelper.condition.active,
            ]
        };
        const query: Array<any> = [];
        query.push({ $match: match_condition });
        query.push(ProductQueryHelper.relations.order_items_in_orders(orderSuccessIDs)); // do not remove this
        query.push({ $sort: { createdAt: -1 } })

        const result = await this.model.aggregate(query);
        return result
    }

    async getProductRankingForBuyer(page = 1, limit = 10) {
        const query = this.model.aggregate([
            {
                $match: {
                    $and: [...ProductQueryHelper.condition.active, {quantity: {$gt: 0}}, {is_sold_out: false }]
                },
            },
            ProductQueryHelper.relations.variants,
            ProductQueryHelper.relations.shop,
            ProductQueryHelper.relations.unwind_shop,
            {
                $sort: {sold: -1}
            }
        ])

        const result = await this.model
            .aggregatePaginateCustom(query, { page, limit })
        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        const data = result.docs;
        
        return { data, paginate }
    }

    async updatePopularMark() {
        try {
            const products = await this.model.find({})
            const promises = products.map(product => {
                return new Promise(async (resolve, reject) => {
                    const activities = await this.trackingRepo.find({
                        target_type: 'Product',
                        target_id: ObjectId(product._id)
                    })
                    const mark = activities.reduce((sum, item) => {
                        switch (item.action_type) {
                            case TrackingType.ViewProduct:
                                sum += TrackingRate.ViewProduct * item.count_number
                                break;
                            case TrackingType.AddProductToFavorite:
                                sum += TrackingRate.AddProductToFavorite * item.count_number
                                break;
                            case TrackingType.AddProductToCart:
                                sum += TrackingRate.AddProductToCart * item.count_number
                                break;
                            case TrackingType.AddProductToCartStream:
                                sum += TrackingRate.AddProductToCartStream * item.count_number
                                break;
                            case TrackingType.BuyProduct:
                                sum += TrackingRate.BuyProduct * item.count_number
                                break;
                            case TrackingType.BuyProductInStream:
                                sum += TrackingRate.BuyProductInStream * item.count_number
                                break;
                            default:
                                sum += 0
                                break;
                        }
                        return sum;
                    }, 0)
                    // console.log(`Mark of ${product._id} is ${mark}`);
                    resolve(mark)
                    
                    const newProduct = await this.model.update({_id: product._id}, {
                        popular_mark: mark || 0
                    })
                    resolve(newProduct)
                })
            })
            await Promise.all(promises).then(data => {
                
            }).catch(error => {
                throw new Error(error)
            })
        } catch (error) {
            console.log("ERROR reCalculatePopularMark: ", error)
        }
    }

    async updateSuggestList(notIncludingIDs: Array<any>, limit: Number) {
        const products = await this.model.aggregate([
            {
                $match: { 
                    _id: {$nin: notIncludingIDs},
                    quantity: {$gt: 0},
                    is_approved: "approved",
                    deleted_at: null,
                    allow_to_sell: true
                }
            },
            {
                $sort: { "popular_mark": -1, "is_genuine_item": -1, "is_guaranteed_item": -1 }
            },
            {
                $limit: limit
            }
        ])
        const ids = products.map(product => product._id)
        await this.model.updateMany({
            _id: { $nin: notIncludingIDs }
        }, {
            is_suggested: false
        })
        return this.model.updateMany({
            _id: { $in: ids }
        }, {
            is_suggested: true
        })
    }

    async getSuggestProductList(page, limit, bodyShape) {
        let match_condition: any = {
            "$and": [
                ...ProductQueryHelper.condition.active,
                { quantity: {$gt: 0} },
                { is_sold_out: false },
                { is_suggested: true }
            ]
        };
        const sort_condition = {}
        if(bodyShape) {
            sort_condition[`body_shape_mark.${bodyShape}`] = -1
        }
        sort_condition['popular_mark'] = -1
        sort_condition['product.is_genuine_item'] = -1
        sort_condition['product.is_guaranteed_item'] = -1
        const filter = [
            {
                $match: match_condition
            },
            ProductQueryHelper.addField.stocking,
            ProductQueryHelper.relations.variants,
            ProductQueryHelper.relations.shop,
            ProductQueryHelper.relations.unwind_shop,
            // { $group : { _id : "$_id", product: { $first: "$$ROOT" } } },
            {
                $sort: sort_condition
            }
        ]

        const query = this.model.aggregate(filter)

        const result = await this.model
            .aggregatePaginateCustom(query, { page, limit })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        
        const data = result.docs;
        return { data, paginate }
    }
    
    async getSimilarProductList(page, limit, categoryID, categoryIDs) {
        categoryID = categoryID.toString()
        let match_condition: any = {
            "$and": [
                ...ProductQueryHelper.condition.active,
                { quantity: {$gt: 0} },
                { is_sold_out: false },
                {
                    list_category_id: { $in: categoryIDs}
                }
            ]
        };
        const sort_condition = {
            is_genuine_item: -1,
            is_guaranteed_item: -1,
            created_at: -1
        }

        const filter = [
            {
                $match: match_condition
            },
            ProductQueryHelper.addField.stocking,
            ProductQueryHelper.relations.variants,
            ProductQueryHelper.relations.shop,
            ProductQueryHelper.relations.unwind_shop,
            {
                $sort: sort_condition
            },
            // { $project : { _id: 1, name : 1 , sale_price : 1, variants: 1 } }
        ]

        const query = this.model.aggregate(filter)

        const result = await this.model
            .aggregatePaginateCustom(query, { page, limit })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        
        const data = result.docs;
        return { data, paginate }
    }

    async getCategoriesHaveGroupBuyFromProduct(type) {
        const filter = {
            is_approved: 'approved',
            allow_to_sell: true,
            is_sold_out: false,
            quantity: { $gt: 0 },
            $or: [
                {deleted_at: null},
                {deleted_at: {$exists: false}}
            ]
        }

        if (type === 'RUNNING') {
            filter['group_buy.start_time'] = {$lte: new Date()};
            filter['group_buy.end_time'] = {$gte: new Date()};
        } else {
            filter['group_buy.start_time'] = {$gt: new Date()};
        }

        const listCategory =  await this.model.distinct('category_id', filter);
        return listCategory;
    }
    
    async findProductByReferralLink(link) {
        const products = await this.model.aggregate([
            {
                $match: {
                    $and: [
                        ...ProductQueryHelper.condition.active,
                        {"order_rooms.members": {"$elemMatch" : {"referal_link" : link} }}
                    ]
                }
            }
        ])
        return products[0]
    }

    public async getValidGroupBuyProductByShopId(id, page, limit, listIds, stocking = false, groupBuyId = "", includeRelation = true) {
        const conditions: any[] = [
            { $or: [
                { flash_sale: { $exists: false } },
                { flash_sale: [] },
            ]
            },
            { shop_id: ObjectId(id) },
            { _id: {$nin: listIds}},
            ...ProductQueryHelper.condition.active,
        ]
        if(stocking) {
            conditions.push({is_sold_out: false})
            conditions.push({quantity: {$gt: 0}})
        }
        if (groupBuyId) {
            conditions.push({
                $or: [
                    { group_buy: { $exists: false } },
                    { group_buy: null },
                    { 'group_buy.shop_group_buy_id': ObjectId(groupBuyId) },
                ],
            })
        } else {
            conditions.push({
                $or: [{ group_buy: { $exists: false } }, { group_buy: null }],
            })
        }
        const match_condition: any = {
            "$and": conditions
        };
        const query: Array<any> = [];
        query.push({ $match: match_condition });
        if(includeRelation) {
            query.push(ProductQueryHelper.relations.variants);
            query.push(ProductQueryHelper.relations.shop)
            query.push(ProductQueryHelper.relations.unwind_shop)
        }
        
        query.push({ $sort: { createdAt: -1 } })

        const aggregate = this.model.aggregate(query);
        const result = await this.model
            .aggregatePaginateCustom(aggregate, { page, limit })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        const data = result.docs;
        return { data, paginate }
    }

    findByQueryAndProject(query: any, projection: any): Promise<any> {
        return this.model.find(query, projection);
    }

    async findProductGroupbuy(dateCompare: any) {
        const query = {
            group_buy: { $ne: null },
            order_rooms: { $elemMatch: { status: 'pending', created_at: {$lte: dateCompare} } },
        };

        return this.model.find(query);
    }
    
}

export class ProductQueryHelper {

    public static condition = {
        active: [
            { deleted_at: null },
            { is_approved: "approved" },
            { allow_to_sell: true }
        ],

        minPriceMatchRange: (price_min, price_max) => ({
            $match: {
                $and: [
                    { minPrice: { $gte: price_min } },
                    { minPrice: { $lte: price_max } }
                ]
            }
        }),

        minPriceMatch: (price_min) => ({
            $match: {
                $and: [
                    { minPrice: { $gte: price_min } },
                ]
            }
        }),

        maxPriceMatch: (price_max) => ({
            $match: {
                $and: [
                    { minPrice: { $lte: price_max } }
                ]
            }
        })
    }

    public static relations = {
        promotion: {
            $lookup: {
                from: "promotionprograms",
                let: {
                    product_id: "$_id"
                },
                pipeline: [
                    {
                        $match: {
                            $and: [
                                {
                                    '$expr': {
                                        $and: [
                                            {
                                                $in: ["$$product_id", "$products"]
                                            }
                                        ]
                                    },
                                },
                                {
                                    is_valid: true
                                },
                                {
                                    start_time: { $lte: new Date() }
                                },
                                {
                                    end_time: { $gte: new Date() }
                                }
                            ]
                        }
                    },
                ],
                as: "promotion"
            }
        },
        unwind_discount: {
            $unwind: {
                path: "$promotion",
                preserveNullAndEmptyArrays: true
            }
        },
        category: {
            $lookup: {
                from: "productcategories",
                let: {
                    product_id: "$_id"
                },
                pipeline: [
                    {
                        $match: {
                            $and: [
                                {
                                    '$expr': {
                                        $and: [
                                            {
                                                $eq: ["$product_id", "$$product_id"]
                                            }
                                        ]
                                    },
                                }
                            ]
                        }
                    },
                ],
                as: "product_categories"
            }

        },

        order_items: {
            $lookup: {
                from: "orderitems",
                let: {
                    product_id: "$_id"
                },
                pipeline: [
                    {
                        $match: {
                            $and: [
                                {
                                    '$expr': {
                                        $and: [
                                            {
                                                $eq: ["$product_id", "$$product_id"]
                                            }
                                        ]
                                    },
                                }
                            ]
                        }
                    },
                    {
                        $count: "sold"
                    }
                ],
                as: "order_items"
            }
        },

        order_items_in_orders: (orderSuccessIDs) => {  
            return {
                $lookup: {
                    from: "orderitems",
                    let: {
                        product_id: "$_id",
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        '$expr': {
                                            $and: [
                                                {
                                                    $eq: ["$product_id", "$$product_id"]
                                                },
                                            ]
                                        },
                                    },
                                    {
                                        order_id: {$in: orderSuccessIDs}
                                    }
                                ]
                            }
                        },
                        {
                            $group: {
                                _id: "$product_id",
                                sold: { $sum: "$quantity" },
                            }
                        }
                    ],
                    as: "order_items"
                }
            }
        },

        variants: {
            $lookup: {
                from: "variants",
                let: {
                    product_id: "$_id"
                },
                pipeline: [
                    {
                        $match: {
                            $and: [
                                {
                                    '$expr': {
                                        $and: [
                                            {
                                                $eq: ["$product_id", "$$product_id"]
                                            }
                                        ]
                                    },
                                }
                            ]
                        }
                    },
                ],
                as: "variants"
            }
        },

        option_types: {
            $lookup: {
                from: "optiontypes",
                let: {
                    product_id: "$_id"
                },
                pipeline: [
                    {
                        $match: {
                            $and: [
                                {
                                    '$expr': {
                                        $and: [
                                            {
                                                $eq: ["$product_id", "$$product_id"]
                                            }
                                        ]
                                    },
                                }
                            ]
                        }
                    },
                    {
                        $lookup: {
                            from: "optionvalues",
                            let: {
                                option_type_id: "$_id"
                            },
                            pipeline: [
                                {
                                    $match: {
                                        $and: [
                                            {
                                                '$expr': {
                                                    $and: [
                                                        {
                                                            $eq: ["$option_type_id", "$$option_type_id"]
                                                        }
                                                    ]
                                                },
                                            }
                                        ]
                                    },

                                },
                                {
                                    $project: {
                                        _id: 1,
                                        name: 1,
                                        image: 1
                                    }
                                }


                            ],
                            as: "option_values"
                        }
                    },
                    {
                        $project: {
                            _id: 1,
                            name: 1,
                            option_values: 1
                        }
                    }
                ],
                as: "option_types"
            }
        },

        orders: {
            $lookup: {
                from: "orderitems",
                let: {
                    product_id: "$_id"
                },
                pipeline: [
                    {
                        $match: {
                            $and: [
                                {
                                    '$expr': {
                                        $and: [
                                            {
                                                $eq: ["$product_id", "$$product_id"]
                                            }
                                        ]
                                    },
                                }
                            ]
                        }
                    },
                ],
                as: "orderitems"
            }

        },

        activities: {
            $lookup: {
                from: "trackingactivities",
                let: {
                    product_id: "$_id"
                },
                pipeline: [
                    {
                        $match: {
                            $and: [
                                {
                                    '$expr': {
                                        $and: [
                                            {
                                                $eq: ["$target_id", "$$product_id"]
                                            },
                                        ]
                                    },
                                },
                                {
                                    target_type: 'Product'
                                }
                            ]
                        }
                    },
                ],
                as: "activities"
            }

        },

        shop: {
            $lookup: {
                from: "shops",
                let: {
                    shop_id: "$shop_id"
                },
                pipeline: [
                    {
                        $match: {
                            $and: [
                                {
                                    '$expr': {
                                        $and: [
                                            {
                                                $eq: ["$_id", "$$shop_id"]
                                            }
                                        ]
                                    },
                                },
                                {
                                    is_approved: true
                                }
                            ]
                        }
                    },
                ],
                as: "shop"
            }

        },

        shop_user: {
            $lookup: {
                from: "shops",
                let: {
                    shop_id: "$shop_id"
                },
                pipeline: [
                    {
                        $match: {
                            $and: [
                                {
                                    '$expr': {
                                        $and: [
                                            {
                                                $eq: ["$_id", "$$shop_id"]
                                            }
                                        ]
                                    },
                                },
                                {
                                    is_approved: true
                                }
                            ]
                        }
                    },
                    {
                        $lookup: {
                            from: "users",
                            let: {
                                user_id: "$user_id"
                            },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $eq: ["$_id", "$$user_id"]
                                        }
                                    }
                                },
                                {
                                    $lookup: {
                                        from: "galleryimages",
                                        let: {
                                            user_id: "$_id"
                                        },
                                        pipeline: [
                                            {
                                                $match: {
                                                    '$expr': {
                                                        $and: [
                                                            {
                                                                $eq: ["$galleryImageAbleId", "$$user_id"]
                                                            },
                                                            {
                                                                $eq: ["$galleryImageAbleType", "1"]
                                                            }
                                                        ]
                                                    },
                                                }
                                            }
                                        ],
                                        as: "gallery_image"
                                    }
                                },
                                {
                                    $unwind: {
                                        path: "$gallery_image",
                                        preserveNullAndEmptyArrays: true
                                    }
                                }
                            ],
                            as: "user"
                        }
                    },
                    {
                        $unwind: {
                            path: "$user",
                            preserveNullAndEmptyArrays: false
                        }
                    }
                ],
                as: "shop"
            }

        },
        
        unwind_shop: {
            $unwind: {
                path: "$shop",
                preserveNullAndEmptyArrays: false
            }
        },

        ordersWithTotal: (condtion: any = [], as = "orderitems") => ({
            $lookup: {
                from: "orderitems",
                let: {
                    product_id: "$_id"
                },
                pipeline: [
                    {
                        $match: {
                            $and: [
                                {
                                    '$expr': {
                                        $and: [
                                            {
                                                $eq: ["$product_id", "$$product_id"]
                                            }
                                        ]
                                    },
                                },
                                ...condtion
                            ]
                        }
                    },
                    {
                        $lookup: {
                            from: "orders",
                            let: {
                                order_id: "$order_id"
                            },
                            pipeline: [
                                {
                                    $match: {
                                        $and: [
                                            {
                                                '$expr': {
                                                    $eq: ["$_id", "$$order_id"]
                                                }
                                            }
                                        ],
                                        payment_status: {$in: [PaymentStatus.PAID, PaymentStatus.PENDING]},
                                        $or: [
                                            {
                                                shipping_status: {$in: [ShippingStatus.SHIPPED, ShippingStatus.SHIPPING, ShippingStatus.WAIT_TO_PICK, ShippingStatus.RETURN]}
                                            },
                                            {
                                                shipping_status: {$in: [ShippingStatus.CANCELING, ShippingStatus.CANCELED]},
                                                approved_time: {$ne: null, $exists: true}
                                            }
                                        ] 
                                    }
                                }
                            ],
                            as: "orders"
                        }
                    },
                    {
                        $unwind: {
                            path: '$orders',
                            preserveNullAndEmptyArrays: false,
                        }
                    },
                    {
                        $addFields: {
                            total: {
                                $cond: { 
                                    if: {$ne: ['$variant', null]},
                                    then: {$multiply: ["$quantity", "$variant.sale_price"]}, 
                                    else: {$multiply: ["$quantity", "$product.sale_price"]} 
                                }   
                            }
                        }
                    }
                ],
                as
            }

        }),

        activitiesCondition: (condtion: any = [], as = "activities", pineLineCondition: any = []) => ({
            $lookup: {
                from: "trackingactivities",
                let: {
                    product_id: "$_id"
                },
                pipeline: [
                    {
                        $match: {
                            $and: [
                                {
                                    '$expr': {
                                        $and: [
                                            {
                                                $eq: ["$target_id", "$$product_id"]
                                            },
                                        ]
                                    },
                                },
                                ...condtion
                            ]
                        }
                    },
                    { $unwind: "$visited_ats" },
                    {
                        $match: {
                            $and: [
                                ...pineLineCondition
                            ]
                        }
                    },
                ],
                as,

            }

        }),
    }

    public static addField = {
        minPrice: {
            $addFields: {
                minPrice: {
                    $cond: {
                        if: { $ne: [{ $min: "$variants.sale_price" }, null] },
                        then: { $min: "$variants.sale_price" },
                        else: "$sale_price"
                    }
                },
            }
        },

        activityNumber: {
            $addFields: {
                activityNumber: { $sum: "$activities.count_number" }
            }
        },

        activityNumberGenerate: (root = 'activities', name = 'activityNumber') => ({
            $addFields: {
                [name]: { $size: `$${root}` }
            }
        }),

        orderNumber: (root = 'orderitems', name = 'orderNumber') => ({
            $addFields: {
                [name]: { $size: `$${root}` }
            }
        }),

        orderRevenue: (root = 'orderitems', name = 'orderRevenue') => ({
            $addFields: {
                [name]: {
                    $sum: `$${root}.total`
                }
            },
        }),

        orderQuantity: (root = 'orderitems', name = 'orderQuantity') => ({
            $addFields: {
                [name]: {
                    $sum: `$${root}.quantity`
                }
            },
        }),
        stocking: { // check product is stoking or out of stock
            $addFields: {
                stocking: {
                    $cond: {
                        if: { $gt: ["$quantity", 0] },
                        then: true,
                        else: false
                    }
                }
            }
        }
    }

    public static fieldSearch = (keyword, fieldName) => {
        const keywordMatch = [
            {
                [fieldName]: { '$regex': "^" + keyword + "\\s+.*", '$options': 'gi' }
            },
            {
                [fieldName]: { '$regex': ".*\\s+" + keyword + "$", '$options': 'gi' }
            },
            {
                [fieldName]: { '$regex': ".*\\s+" + keyword + "\\s+.*", '$options': 'gi' }
            },
            {
                [fieldName]: { '$regex': "^" + keyword + "$", '$options': 'gi' }
            }
        ]

        return {
            "$or": keywordMatch
        }
    }
}