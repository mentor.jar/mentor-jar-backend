import { BaseRepository } from "./BaseRepository";
import AppFeedback from "../models/AppFeedback"
import { AppFeedbackDoc } from "../interfaces/AppFeedback";
import { ObjectId } from "../utils/helper";

export class AppFeedbackRepo extends BaseRepository<AppFeedbackDoc> {

    private static instance: AppFeedbackRepo;

    private constructor() {
        super();
        this.model = AppFeedback;
    }

    public static getInstance(): AppFeedbackRepo {
        if (!AppFeedbackRepo.instance) {
            AppFeedbackRepo.instance = new AppFeedbackRepo();
        }

        return AppFeedbackRepo.instance;
    }

    async getListAppFeedback(type, filter, page = 1, limit = 20) {
        const filterMark = filter.map(filter => {
            return {
                [filter.name]: filter.value
            }
        })
        const match = filter.length ? (type === "and" ? {
            $and: filterMark
        } : {
            $or: filterMark
        }) : {}
        const query = this.model.aggregate([
            {
                $match: match
            },
            {
                $sort: {created_at: -1}
            }
        ])

        const result = await this.model
            .aggregatePaginateCustom(query, { page, limit })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        const data = result.docs;
        return { data, paginate }
    }

    async getDetail(id) {
        return this.model.aggregate([
            {
                $match: {
                    _id: ObjectId(id)
                }
            },
            {
                $lookup: {
                    from: "users",
                    let: {
                        user_id: "$user_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        '$expr': {
                                            $and: [
                                                {
                                                    $eq: ["$_id", "$$user_id"]
                                                }
                                            ]
                                        },
                                    }
                                ]
                            }
                        },
                        {
                            $lookup: {
                                from: "galleryimages",
                                let: {
                                    user_id: "$_id"
                                },
                                pipeline: [
                                    {
                                        $match: {
                                            '$expr': {
                                                $and: [
                                                    {
                                                        $eq: ["$galleryImageAbleId", "$$user_id"]
                                                    },
                                                    {
                                                        $eq: ["$galleryImageAbleType", "1"]
                                                    }
                                                ]
                                            },
                                        }
                                    }
                                ],
                                as: "gallery_image"
                            }
                        },
                        {
                            $unwind: {
                                path: "$gallery_image",
                                preserveNullAndEmptyArrays: true
                            }
                        }
                    ],
                    as: "user" 
                } 
            },
            {
                $unwind: {
                    path: "$user",
                    preserveNullAndEmptyArrays: true
                } 
            }
        ])
    }
}