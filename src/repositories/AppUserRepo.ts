import mongoose from 'mongoose';
import { EAppUser } from '../interfaces/AppUser';
import AppUser from '../models/AppUser';
import { ObjectId } from '../utils/helper';
import { BaseRepository } from "./BaseRepository";

export default class AppUserRepo extends BaseRepository<EAppUser> {

    private static instance: AppUserRepo;

    private constructor() {
        super();
        this.model = AppUser;
    }

    public static getInstance(): AppUserRepo {
        if (!AppUserRepo.instance) {
            AppUserRepo.instance = new AppUserRepo();
        }
        return AppUserRepo.instance;
    }

    async getAppOnline(userId) {
        userId = ObjectId(userId.toString())
        let response = await this.find({ userId, status: 'online' })
        if (response.length) return response[0]
        return null
    }
}
