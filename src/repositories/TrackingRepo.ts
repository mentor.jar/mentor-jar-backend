import TrackingActivity from '../models/Tracking_Activity';
import { BaseRepository } from './BaseRepository';
import { TrackingActivityDoc } from '../interfaces/Tracking_Activity';
import { ProductRepo } from './ProductRepo';
import { ObjectId } from '../utils/helper';

export default class TrackingRepo extends BaseRepository<TrackingActivityDoc> {
    private static instance: TrackingRepo;

    private constructor() {
        super();
        this.model = TrackingActivity;
    }

    public static getInstance(): TrackingRepo {
        if (!TrackingRepo.instance) {
            TrackingRepo.instance = new TrackingRepo();
        }
        return TrackingRepo.instance;
    }

    public async getUserAccessShop(shop_id, start_time, end_time) {
        const productRepository = ProductRepo.getInstance();
        let search_query = {
            shop_id,
            filter_type: 'ALL',
        };
        let products = await productRepository.findFilter(search_query, null);
        let list_product_id = products.map(product => ObjectId(product._id));
        // console.log(list_product_id);
        let trackings = await this.model.aggregate([
            {
                $match: {
                    target_type: 'Product',
                    action_type: 'view_product',
                    actor_type: 'User',
                    target_id: { $in: list_product_id },
                    visited_ats: {
                        $elemMatch: { $gt: start_time, $lt: end_time },
                    },
                },
            },
            {
                $unwind: {
                    preserveNullAndEmptyArrays: true,
                    path: '$visited_ats',
                },
            },
            {
                $sort: {
                    visited_ats: -1,
                },
            },
            {
                $group: {
                    _id: '$actor_id',
                    visited_ats: { $push: '$visited_ats' },
                },
            },
        ]);
        return trackings;
    }
}
