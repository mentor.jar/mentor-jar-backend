import { BaseRepository } from "./BaseRepository";
import ProductDetailInfo from "../models/Product_Detail_Info";
import { ProductDetailInfoDoc } from "../interfaces/Product_Detail_Info";
import { ProductDetailInfoDTO } from "../DTO/Product_Detail_Info";
import { cloneObj } from "../utils/helper";
import { CategoryInfoRepo } from "./CategoryInfoRepo";



export class ProductDetailInfoRepo extends BaseRepository<ProductDetailInfoDoc>  {

    private static instance: ProductDetailInfoRepo;

    constructor() {
        super();
        this.model = ProductDetailInfo;
    }

    public static getInstance(): ProductDetailInfoRepo {
        if (!ProductDetailInfoRepo.instance) {
            ProductDetailInfoRepo.instance = new ProductDetailInfoRepo();
        }

        return ProductDetailInfoRepo.instance;
    }
    async getByProductId(product_id: string): Promise<any> {
        let categoryInfoRepo = CategoryInfoRepo.getInstance();
        let productDetailInfo: any;
        let results: any = [];
        let arrayId: any = [];
        try {
            productDetailInfo = await this.model.find({ product_id: product_id });
            const listPromise: any = [];
            let productDetailInfoDTO = productDetailInfo.map(e => new ProductDetailInfoDTO(e));

            productDetailInfoDTO.map(e => {
                arrayId.push(e.toSimpleJSON().category_info_id);
                let productDetailInfoField = e.toJSON(['category_info_id', 'value', 'values']);
                results.push(productDetailInfoField);
            });

            // get array id of category info
            let categoryInfo = await categoryInfoRepo.find({ _id: { $in: arrayId } });
            categoryInfo = cloneObj(categoryInfo);

            //create object key:value => id:index
            let mapKeyId: any = {};
            categoryInfo.map((e, index) => {
                mapKeyId[e._id] = index;
            });
            //console.log(mapKeyId['603a44dd4f729a65792c4ad0']); => 0

            results = results.map(e => {
                //e.category_info = categoryInfo[mapKeyId[e.category_info_id]];
                let category_info_item: any = categoryInfo[mapKeyId[e.category_info_id]];
                e.category_info = {
                    name: category_info_item.name,
                    type: category_info_item.type,
                    is_required: category_info_item.is_required,
                    is_allow_multiple_values: category_info_item.is_allow_multiple_values,
                    list_option: category_info_item.list_option || []
                }
                return e;
            })
            // productDetailInfoDTO.map(e => {
            //     const promise = new Promise(async (resolve, reject) => {
            //         const categoryInfo = await categoryInfoRepo.findById(e.toSimpleJSON().category_info_id);
            //         let productDetailInfoField = e.toJSON(['category_info_id', 'value']);
            //         productDetailInfoField.name = categoryInfo.name;

            //         resolve(productDetailInfoField);
            //     })
            //     listPromise.push(promise);
            // });
            // results = await Promise.all(listPromise);

            return results;
        } catch (error) {

        }
    }


    deleteByCategoryInfo(allCategoryInfoId: Array<String>) {
        return this.model.find({ category_info_id: allCategoryInfoId }).deleteMany();
    }
    // deleteByCategoryInfo(CategoryInfoId: String) {
    //     return this.model.deleteOne({ category_info_id: CategoryInfoId });
    // }

}