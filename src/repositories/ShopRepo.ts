import { BaseQueryHelper, BaseRepository } from './BaseRepository';
import Shop from '../models/Shop';
import { ShopDoc } from '../interfaces/Shop';
import { ShopDTO } from '../DTO/Shop';
import { cloneObj, ObjectId } from '../utils/helper';
import { StreamRepository } from './StreamRepository';
import { DEFAULT_KOREA_SHOP, DEFAULT_NAME_ADDRESS, URL_SERVER, URL_SERVER_RTMP } from '../base/variable';
import { ShippingMethodRepo } from './ShippingMethodRepo';
import { AddressRepo } from './AddressRepo';
import { KOREA, VIET_NAM } from '../base/variable';
import { PaymentStatus, ShippingStatus } from '../models/enums/order';
import { makeShortenUrl } from '../utils/utils';
import { UserDTO } from '../DTO/UserDTO';
import { InMemoryAuthUserStore } from '../SocketStores/AuthStore';
export class ShopRepo extends BaseRepository<ShopDoc> {
    private static instance: ShopRepo;

    constructor() {
        super();
        this.model = Shop;
    }

    public static getInstance(): ShopRepo {
        if (!ShopRepo.instance) {
            ShopRepo.instance = new ShopRepo();
        }

        return ShopRepo.instance;
    }

    async findOrCreateByUser(user, country = VIET_NAM, isApproved = false): Promise<any> {
        let { shop } = user;
        
        if (!shop) {
            shop = await this.model.findOne({ user_id: user._id });
        }
        
        if (!shop) {
            const allShippingMethod = await ShippingMethodRepo.getInstance().getAllShippingMethod();

            let shippingMethods = allShippingMethod.map(
                method => {
                    return {
                        shipping_method_id: method._id,
                        name: method.name,
                        is_active: false,
                        is_approved: isApproved,
                        token: null,
                        pick_address: null,
                        name_query: method.name_query
                    }
                }
            )
            
            let shorten_link = makeShortenUrl(6, 's')
            while(await this.model.findOne({shorten_link}) !== null) {
                shorten_link = makeShortenUrl(6, 's')
            }

            let myShop = {
                name: user?.nameOrganizer?.userName || 'My Shop',
                description: '',
                user_id: user._id,
                shipping_methods: shippingMethods,
                country,
                shorten_link
            }
            if (country === KOREA) {
                myShop = { ...myShop, country: KOREA };
            };

            shop = await this.model.create(myShop);
            shop = cloneObj(shop);
            this.findOrCreateAddress(user._id, country);
            InMemoryAuthUserStore.getInstance().updateShop(user._id, shop);
        }
        return shop;
    }

    async findOrCreateStream(user_id: string, shop_id: string) {
        let streamRepository = StreamRepository.getInstance();
        let stream = await streamRepository.findOne({ user_id: user_id });
        if (!stream) {
            const oneStream = await streamRepository.create({
                name: user_id + '.stream',
                rtmp_link: URL_SERVER_RTMP + user_id + '.stream' || null,
                hls_link:
                    URL_SERVER + user_id + '.stream/playlist.m3u8' || null,
                user_id: user_id,
                shop_id: shop_id,
            });
            return oneStream;
        }
        return stream;
    }


    async findOrCreateAddress(user_id: string, shop_country: string) {
        let addressRepository = AddressRepo.getInstance();
        let address = await addressRepository.findOne({ accessible_id: user_id.toString() });
        if (!address && shop_country === KOREA) {
            const koreaAddress = { ...DEFAULT_KOREA_SHOP, accessible_id: user_id.toString() }
            const newAddress = await addressRepository.create(koreaAddress);
            return newAddress;
        }
        return address;
    }

    async actionAfterCreateShop(user_id: string, shop_id: string, shop_country = "") {
        const stream = await this.findOrCreateStream(user_id, shop_id);
        const address = await this.findOrCreateAddress(user_id, shop_country);
        return stream;
    }

    async getListShop(paginate: object | null, params) {
        let search = params.search;
        const filter = params.filter;
        const topShopBanner = params.topShopBanner;
        let relations = [
            this.model.relationship().users(),
        ];
        let match: any = { deleted_at: null };
        if (filter && filter !== "none") {
            match.is_approved = Boolean(+filter);
        }
   
        let promiseResult = this.getExpandValues(match, relations, {
            is_approved: -1,
            createdAt: -1,
        });

        if(topShopBanner){
            promiseResult = promiseResult.match({
                $or: [
                    { system_banner: { $eq: null } }, 
                    { "system_banner.id": topShopBanner } 
                ],
            });
        }

        if (search) {
            promiseResult = promiseResult.match({
                $or: [
                    BaseQueryHelper.fieldSearch(search, 'user.nameOrganizer.userName'),
                    BaseQueryHelper.fieldSearch(search, 'user.nameOrganizer.unsigneduserName'),
                ],
            });
        }

        let result = paginate
            ? await this.model.aggregatePaginateCustom(promiseResult, paginate)
            : await promiseResult;
        result = cloneObj(result);

        let paginateRes = {};
        if (paginate) {
            paginateRes = {
                limit: result.limit,
                total_page: result.totalPages,
                page: result.page,
                total_record: result.totalDocs,
            };
            result = result.docs;
        }
        result = result.map(e => {
            let getShop: any = new ShopDTO(e);
            getShop = getShop.toSimpleJSON()
            getShop.user = new UserDTO(getShop?.user).toSimpleJSONForAdmin();
            return getShop;
        });
        if (paginate) return { data: result, paginate: paginateRes };

        return result;
    }

    async getListShopActiveAndNewShop(start_time: any, end_time: any) {
        const conditionForAllShop: Array<any> = [];

        const condition: Array<any> = [
            { is_approved: true, pause_mode: false },
        ];

        if (start_time) {
            condition.push({ createdAt: { $gte: start_time } })
            conditionForAllShop.push({ createdAt: { $gte: start_time } })
        }

        if (end_time) {
            condition.push({ createdAt: { $lte: end_time } })
            conditionForAllShop.push({ createdAt: { $lte: end_time } })
        }

        const result = await this.model.aggregate([
            {
                $facet: {
                    all_shop: [
                        {
                            $match: { $and: conditionForAllShop },
                        },
                        {
                            $lookup: {
                                from: 'users',
                                as: 'users',
                                let: {
                                    userId: '$user_id',
                                },
                                pipeline: [
                                    {
                                        $match: {
                                            $expr: {
                                                $eq: [
                                                    '$_id',
                                                    '$$userId',
                                                ], 
                                            }
                                        },
                                    },
                                ],
                            }
                        },
                        {
                            $unwind: {
                                path: '$users',
                                preserveNullAndEmptyArrays: true,
                            },
                        },
                        {
                            $sort: { createdAt: 1 },
                        },
                        {
                            $project: {
                                _id: { $toString: "$_id"},
                                nameShop: { $ifNull: ["$users.nameOrganizer.userName", ""]},
                                country: 1,
                                email: { $ifNull : ["$users.email", { $ifNull: ["$users.socialAccount.email", ""]}] },
                                phoneNumber: { $ifNull: ["$users.phoneNumber", ""]},
                                birthday: { $ifNull: ["$users.birthday", ""]},
                                createdAt: {$dateToString: {format: "%Y-%m-%d %H:%M:%S", date: "$createdAt"}}, 
                            }
                        }
                    ],
                    approved_shop: [
                        {
                            $match: { $and: condition },
                        },
                        {
                            $sort: { createdAt: 1 },
                        },
                        {
                            $lookup: {
                                from: 'users',
                                as: 'users',
                                let: {
                                    userId: '$user_id',
                                },
                                pipeline: [
                                    {
                                        $match: {
                                            $expr: {
                                                $eq: [
                                                    '$_id',
                                                    '$$userId', 
                                                ],
                                            },  
                                        },
                                    },
                                ],
                            }
                        },
                        {
                            $unwind: {
                                path: '$users',
                                preserveNullAndEmptyArrays: true,
                            },
                        },
                        {
                            $lookup: {
                                from: 'products',
                                as: 'products',
                                let: {
                                    shopId: '$_id',
                                },
                                pipeline: [
                                    {
                                        $match: {
                                            $and: [
                                                {
                                                    $expr: {
                                                        $eq: [
                                                            '$shop_id',
                                                            '$$shopId',
                                                        ],  
                                                    },
                                                },
                                                {
                                                    allow_to_sell: true,
                                                },
                                            ],
                                        },
                                    },
                                    {
                                        $count: 'count',
                                    },
                                ],
                            },
                        },
                        {
                            $unwind: {
                                path: '$products',
                                preserveNullAndEmptyArrays: true,
                            },
                        },
                        {
                            $project: {
                                _id: { $toString: "$_id"},
                                nameShop: { $ifNull: ["$users.nameOrganizer.userName", ""]},
                                country: 1,
                                email: { $ifNull : ["$users.email", { $ifNull: ["$users.socialAccount.email", ""]}] },
                                phoneNumber: { $ifNull: ["$users.phoneNumber", ""]},
                                birthday: { $ifNull: ["$users.birthday", ""]},
                                createdAt: {$dateToString: {format: "%Y-%m-%d %H:%M:%S", date: "$createdAt"}}, 
                                products: { $ifNull : ["$products.count", 0] }
                            }
                        }
                    ],
                },
            },
            {
                $project: {
                    all_shop: 1,
                    approved_shop: 1,
                },
            },
        ]);
        return result[0]
    }

    async shopDividedByCategories (start_time: any, end_time: any) {

        const condition: Array<any> = [];

        if (start_time) {
            condition.push({ createdAt: { $gte: start_time } })
        }

        if (end_time) {
            condition.push({ createdAt: { $lte: end_time } })
        }

        return await this.model.aggregate([
            {
                $match: { $and: condition },
            },
            {
                $sort: { createdAt: 1 },
            },
            {
                $lookup: {
                    from: 'users',
                    as: 'users',
                    let: {
                        userId: '$user_id',
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $eq: [
                                        '$_id',
                                        '$$userId',
                                    ],    
                                },
                            },
                        },
                    ],
                }
            },
            {
                $unwind: {
                    path: '$users',
                    preserveNullAndEmptyArrays: true,
                },
            },
            {
                $lookup: {
                    from: 'products',
                    as: 'products',
                    let: {
                        shopId: '$_id',
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $eq: [
                                        '$shop_id',
                                        '$$shopId',
                                    ],     
                                },  
                            },
                        },
                        {
                            $lookup: {
                                from: 'ecategories',
                                as: 'categories',
                                let: {
                                    categoriesId: {
                                        $toObjectId: { $arrayElemAt: ['$list_category_id', 0] },
                                    },
                                },
                                pipeline: [
                                    {
                                        $match: {
                                            $and: [
                                                {
                                                    $expr: {
                                                        $eq: ['$_id', '$$categoriesId'],
                                                    },
                                                }, 
                                                {
                                                    is_active: true
                                                },
                                                {
                                                    parent_id: null
                                                }
                                            ]
                                        },
                                    },
                                ],
                            }, 
                        },
                        {
                            $unwind: {
                                path: '$categories',
                                preserveNullAndEmptyArrays: true,
                            },
                        },
                        {
                            $project: {
                                name: 1,
                                createdAt: 1,
                                categories: 1,
                            },
                        }
                    ],
                },
            },
            {
                $project: {
                    _id: { $toString: "$_id"},
                    nameShop: { $ifNull: ["$users.nameOrganizer.userName", ""]},
                    country: 1,
                    email: { $ifNull : ["$users.email", { $ifNull: ["$users.socialAccount.email", ""]}] },
                    phoneNumber: { $ifNull: ["$users.phoneNumber", ""]},
                    birthday: { $ifNull: ["$users.birthday", ""]},
                    createdAt: {$dateToString: {format: "%Y-%m-%d %H:%M:%S", date: "$createdAt"}}, 
                    products: { $ifNull: ['$products', []] },
                },
            },
        ]);
    }

    async getDetailShop(id) {
        let shop = await this.model.aggregate([
            {
                $match: { _id: ObjectId(id) },
            },
            {
                $sort: { created_at: -1 },
            },
            {
                $lookup: {
                    from: 'users',
                    let: {
                        user_id: '$user_id',
                        deleted_at: '$deleted_at',
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        $expr: {
                                            $and: [
                                                {
                                                    $eq: ['$_id', '$$user_id'],
                                                },
                                            ],
                                        },
                                    },
                                    {
                                        $or: [
                                            { deleted_at: { $exists: false } },
                                            { deleted_at: null },
                                        ],
                                    },
                                ],
                            },
                        },
                        {
                            $lookup: {
                                from: 'galleryimages',
                                let: {
                                    user_id: '$_id',
                                },
                                pipeline: [
                                    {
                                        $match: {
                                            $and: [
                                                {
                                                    $expr: {
                                                        $and: [
                                                            {
                                                                $eq: [
                                                                    '$galleryImageAbleId',
                                                                    '$$user_id',
                                                                ],
                                                            },
                                                            {
                                                                $eq: [
                                                                    '$galleryImageAbleType',
                                                                    '1',
                                                                ],
                                                            },
                                                        ],
                                                    },
                                                },
                                            ],
                                        },
                                    },
                                ],
                                as: 'gallery_image',
                            },
                        },
                        {
                            $unwind: {
                                path: '$gallery_image',
                                preserveNullAndEmptyArrays: true,
                            },
                        },
                        {
                            $lookup: {
                                from: 'addresses',
                                let: {
                                    user_id:{
                                        $toString: '$_id'
                                    } 
                                }, 
                                pipeline: [
                                    { 
                                        $match: { 
                                            $and: [
                                                {
                                                    is_pick_address_default: true
                                                },
                                                {
                                                    $expr: { 
                                                        $and: [
                                                            { 
                                                                $eq: [
                                                                    '$accessible_id',
                                                                    '$$user_id',
                                                                ]
                                                            }
                                                        ]
                                                    }
                                                }
                                            ]
                                        }
                                    }
                                ],
                                as: 'address'
                            },
                        },
                        {
                            $unwind: {
                                path: '$address',
                                preserveNullAndEmptyArrays: true,
                            },
                        },
                    ],
                    as: 'user',
                },
            },
            {
                $unwind: {
                    path: '$user',
                    preserveNullAndEmptyArrays: true,
                },
            },
            {
                $project: { "user.password": 0 }
            }
        ]);
        shop = cloneObj(shop);
        if (shop) {
            return shop[0];
        } else {
            return null;
        }
    }

    async settingShop(id, param) {
        await this.findOneAndUpdate({ _id: ObjectId(id) }, param);
    }

    async getShopByShippingMethod(shop_id, user_id, shipping_method_id) {
        const shippingMethod: any = await this.findOrFail({ _id: ObjectId(shop_id), user_id: ObjectId(user_id), 'shipping_methods.shipping_method_id': shipping_method_id });
        return shippingMethod;
    }

    async updateShippingMethodByShop(params, configParams) {

        const shippingMethod = await this.model.updateOne({ _id: ObjectId(params.shop_id), user_id: ObjectId(params.user_id), 'shipping_methods.shipping_method_id': params.shipping_method_id }, { $set: configParams });
        return shippingMethod;
    }

    async getShippingMethodShop(shop) {
        return shop.shipping_methods;
    }

    async findShopByShopId(id) {
        const shop: any = await this.findOrFail({ _id: ObjectId(id) });
        return shop;
    }

    async getPickAddressDefaultByShop(shop) {
        let userId = shop.user_id;
        const addressRepo = await AddressRepo.getInstance();
        const addressShop = await addressRepo.getPickAddressUser(userId)
        return addressShop;
    }

    async getPickAddressDefaultByShopKorea(shop) {
        let userId = shop.user_id;
        const addressRepo = await AddressRepo.getInstance();
        let addressShop = await addressRepo.getPickAddressUser(userId)

        if (addressShop.name && addressShop.name !== DEFAULT_NAME_ADDRESS) {
            let updateAddressShop = await addressRepo.getPickAddressUserByName(userId, DEFAULT_NAME_ADDRESS)
            if (updateAddressShop) {
                await addressRepo.updateIsPickAddressDefaultForShopKorea(updateAddressShop);
                addressShop = updateAddressShop
            }
        }

        return addressShop;
    }

    async findShopByUserId(id) {
        const shop: any = await this.findOne({ user_id: ObjectId(id) });
        return shop;
    }

    async getTokenByShop(shipping_methods, name_query) {
        let token = null;

        shipping_methods = shipping_methods.filter((item) => {
            return item.name_query = name_query
        })

        token = shipping_methods[0]?.token;
        return token;
    }

    async topShopForProductionSite(fakeIDs) {
        return this.model.aggregate([
            {
                $match: {
                    _id: {$in: fakeIDs}
                }
            },
            {
                $lookup: {
                    from: "users",
                    let: {
                        user_id: "$user_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $eq: ["$_id", "$$user_id"]
                                }
                            }
                        },
                        {
                            $lookup: {
                                from: "galleryimages",
                                let: {
                                    user_id: "$_id"
                                },
                                pipeline: [
                                    {
                                        $match: {
                                            '$expr': {
                                                $and: [
                                                    {
                                                        $eq: ["$galleryImageAbleId", "$$user_id"]
                                                    },
                                                    {
                                                        $eq: ["$galleryImageAbleType", "1"]
                                                    }
                                                ]
                                            },
                                        }
                                    }
                                ],
                                as: "gallery_image"
                            }
                        },
                        {
                            $unwind: {
                                path: "$gallery_image",
                                preserveNullAndEmptyArrays: true
                            }
                        }
                    ],
                    as: "user"
                }
            },
            {
                $unwind: {
                    path: "$user",
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup: {
                    from: "feedbacks",
                    let: {
                        shop_id: "$_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        '$expr': {
                                            $and: [
                                                {
                                                    $eq: ["$shop_id", "$$shop_id"]
                                                }
                                            ]
                                        },
                                    }
                                ]
                            }
                        },
                        {
                            $group: {
                                _id: "$shop_id",
                                averageFeedbackRate: { $avg: "$vote_star" }
                            }
                        }
                    ],
                    as: "feedback"
                },
            }
        ])
    }

    async findShopsWithUserAndFeedback(shopIds) {
        return this.model.aggregate([
            {
                $match: {
                    _id: {$in: shopIds}
                }
            },
            {
                $lookup: {
                    from: "users",
                    let: {
                        user_id: "$user_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        '$expr': {
                                            $and: [
                                                {
                                                    $eq: ["$_id", "$$user_id"]
                                                }
                                            ]
                                        },
                                    }
                                ]
                            }
                        },
                        {
                            $lookup: {
                                from: "galleryimages",
                                let: {
                                    user_id: "$_id"
                                },
                                pipeline: [
                                    {
                                        $match: {
                                            '$expr': {
                                                $and: [
                                                    {
                                                        $eq: ["$galleryImageAbleId", "$$user_id"]
                                                    },
                                                    {
                                                        $eq: ["$galleryImageAbleType", "1"]
                                                    }
                                                ]
                                            },
                                        }
                                    }
                                ],
                                as: "gallery_image"
                            }
                        },
                        {
                            $unwind: {
                                path: "$gallery_image",
                                preserveNullAndEmptyArrays: true
                            }
                        }
                    ],
                    as: "user"
                }
            },
            {
                $unwind: {
                    path: "$user",
                    preserveNullAndEmptyArrays: true
                }

            },
            {
                $lookup: {
                    from: "feedbacks",
                    let: {
                        shop_id: "$_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        '$expr': {
                                            $and: [
                                                {
                                                    $eq: ["$shop_id", "$$shop_id"]
                                                }
                                            ]
                                        },
                                    }
                                ]
                            }
                        },
                        {
                            $group: {
                                _id: "$shop_id",
                                averageFeedbackRate: { $avg: "$vote_star" }
                            }
                        }
                    ],
                    as: "feedback"
                },
            }
        ])
    }

    async findShopForRanking(page = 1, limit = 20, customQuery = null) {
        const match:any = {
            is_approved: true
        }
        if(customQuery) {
            if(customQuery.not_in && customQuery.not_in?.length) {
                match._id = {$nin: customQuery.not_in}
            }
        }
        const query = this.model.aggregate([
            {
                $match: match
            },
            {
                $lookup: {
                    from: "orders",
                    let: {
                        shop_id: "$_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $eq: ["$shop_id", "$$shop_id"]
                                },
                                payment_status: PaymentStatus.PAID,
                                shipping_status: ShippingStatus.SHIPPED
                            }
                        },
                        {
                            $group: {
                                _id: "$shop_id",
                                money: { $sum: "$total_value_items" }
                            }
                        },
                        {
                            $project : {
                                _id: false
                            }
                        }
                    ],
                    as: "success_venue"
                }
            },
            {
                $unwind: {
                    path: "$success_venue",
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup: {
                    from: "users",
                    let: {
                        user_id: "$user_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $eq: ["$_id", "$$user_id"]
                                }
                            }
                        },
                        {
                            $lookup: {
                                from: "galleryimages",
                                let: {
                                    user_id: "$_id"
                                },
                                pipeline: [
                                    {
                                        $match: {
                                            '$expr': {
                                                $and: [
                                                    {
                                                        $eq: ["$galleryImageAbleId", "$$user_id"]
                                                    },
                                                    {
                                                        $eq: ["$galleryImageAbleType", "1"]
                                                    }
                                                ]
                                            },
                                        }
                                    }
                                ],
                                as: "gallery_image"
                            }
                        },
                        {
                            $unwind: {
                                path: "$gallery_image",
                                preserveNullAndEmptyArrays: true
                            }
                        }
                    ],
                    as: "user"
                }
            },
            {
                $unwind: {
                    path: "$user",
                    preserveNullAndEmptyArrays: false
                }
            },
            {
                $lookup: {
                    from: "feedbacks",
                    let: {
                        shop_id: "$_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        '$expr': {
                                            $and: [
                                                {
                                                    $eq: ["$shop_id", "$$shop_id"]
                                                }
                                            ]
                                        },
                                    }
                                ]
                            }
                        },
                        {
                            $group: {
                                _id: "$shop_id",
                                averageFeedbackRate: { $avg: "$vote_star" }
                            }
                        }
                    ],
                    as: "feedback"
                },
            },
            {$sort: {"success_venue.money": -1, "created_at": 1}}
        ])

        let result = await this.model.aggregatePaginateCustom(query, { page, limit })
        result = cloneObj(result);
        let paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs,
        };
        result = result.docs;
        return { data: result, paginate };
    }

    async updateShopRanking(fromDate, toDate) {
        const match:any = {
            is_approved: true
        }

        let shops = await this.model.aggregate([
            {
                $match: match
            },
            {
                $lookup: {
                    from: "orders",
                    let: {
                        shop_id: "$_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $eq: ["$shop_id", "$$shop_id"]
                                },
                                payment_status: PaymentStatus.PAID,
                                shipping_status: ShippingStatus.SHIPPED,
                                created_at: { $gte: fromDate, $lte: toDate }
                            }
                        },
                        {
                            $group: {
                                _id: "$shop_id",
                                money: { $sum: "$total_value_items" },
                                count: { $sum: 1 }
                            }
                        },
                        {
                            $project : {
                                _id: false
                            }
                        }
                    ],
                    as: "success_venue"
                }
            },
            {
                $lookup: {
                    from: "feedbacks",
                    let: {
                        shop_id: "$_id",
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        '$expr': {
                                            $and: [
                                                {
                                                    $eq: ["$shop_id", "$$shop_id"]
                                                }
                                            ]
                                        },
                                    },
                                    { target_type: "Product" }
                                ]
                            }
                        },
                        {
                            $group: {
                                _id: "$shop_id",
                                averageFeedbackRate: { $avg: "$vote_star" }
                            }
                        }
                    ],
                    as: "feedback"
                },
            },
            {
                $unwind: {
                    path: "$success_venue",
                    preserveNullAndEmptyArrays: true
                }
            },
            ...ShopQueryHelper.countProduct,
            {$sort: {"success_venue.money": -1, "ranking_today": 1}}
        ])

        shops = cloneObj(shops)
        shops = shops.map((shop, index) => {
            const venueInWeek = shop?.success_venue?.money || 0
            const orderInWeek = shop?.success_venue?.count || 0
            const avgRating = +shop?.feedback[0]?.averageFeedbackRate.toFixed(1) || 5.0
            const mark = orderInWeek > 0 ? ((venueInWeek / 100_000) * 4 + orderInWeek * 4 + avgRating * 2) / 10 : 0
            const ranking_criteria = {
                totalVenue: venueInWeek,
                totalOrder: orderInWeek,
                rating: avgRating,
                mark: +mark.toFixed(2)
            }
            return {
                ...shop, ranking_criteria
            }
        })

        shops.sort((shopA, shopB) => {
            return shopB.ranking_criteria.mark - shopA.ranking_criteria.mark
        })

        const promises = shops.map((shop, index) => {
            return new Promise(async (resolve, reject) => {
                const data = {
                    ranking_yesterday: shop.ranking_today,
                    ranking_today: index + 1,
                    avg_rating: shop.ranking_criteria.rating,
                    $unset: {ranking: ""},
                    ranking_criteria: {
                        total_venue: shop.ranking_criteria.totalVenue,
                        total_order: shop.ranking_criteria.totalOrder,
                        rating: shop.ranking_criteria.rating,
                        mark: shop.ranking_criteria.mark
                    }
                }

                const newShop = await this.model.update({_id: ObjectId(shop._id)}, data)

                console.log(`Top ${index + 1}: ${shop._id} with rating ${shop?.feedback[0]?.averageFeedbackRate || 5.0}`);
                resolve(newShop)
            })
        })
        await Promise.all(promises).then(data => {
            console.log(`Update ranking of ${data.length} shops successfully`);
        }).catch(error => {
            throw new Error(error)
        })
    }

    async getShopRanking(page = 1, limit = 5) {
        const queryToday = this.model.aggregate([
            {
                $match: {
                    is_approved: true,
                    ranking_today: {$exists: true, $ne: 99999}
                }
            },
            ...ShopQueryHelper.advanceShop,
            ...ShopQueryHelper.countProduct,
            { $sort: {ranking_today: 1} }
        ])

        let resultToday = await this.model.aggregatePaginateCustom(queryToday, {limit, page})
        
        let todayShop = resultToday.docs

        todayShop = todayShop.map(shop => {
            return {
                _id: shop._id,
                shop
            }
        })
        return todayShop
    }

    async getValidShopToSendEmail() {
        const match:any = {
            is_approved: true
        }
        
        let result = await this.model.aggregate([
            {
                $match: match
            },
            {
                $lookup: {
                    from: "users",
                    let: {
                        user_id: "$user_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $eq: ["$_id", "$$user_id"]
                                },
                                isActive: true,
                                $or: [
                                    {email: {$ne: null}},
                                    {'socialAccount.id': {$ne: null}, 'socicalAccount.socialName': 'google'}
                                ]
                            }
                        }
                    ],
                    as: "user"
                }
            },
            {
                $unwind: {
                    path: "$user",
                    preserveNullAndEmptyArrays: false
                }
            },
            {$sort: { "created_at": -1}}
        ])

        result = cloneObj(result);
        result = result.map(item => {
            return {
                shop_id: item._id,
                user_id: item.user._id,
                email: item?.user?.email || item?.user?.socialAccount?.id,
                user_name: item?.user?.nameOrganizer?.userName
            }
        })
        // Distinct email and email black list
        result = result.filter((item) => {
            return result.filter(itemCheck => itemCheck.email === item.email).length <= 1 && !item.email.includes("@demo.com");
        })
        console.log("Total Seller: ", result.length);
        return result
    }

    async updateViettelPostShipment() {
        const shops = await this.model.find();
        let shippingRepo = ShippingMethodRepo.getInstance();
        let method = await shippingRepo.getAllShippingMethodById('620c942074e40ed44d99de11'); // set id shipping method
        shops.map(async (shop) => {
            if (shop.shipping_methods.length == 1) {
                shop.shipping_methods.push({
                    shipping_method_id: method._id,
                    name: method.name,
                    is_active: false,
                    token: null,
                    pick_address: null,
                    name_query: method.name_query,
                    code: null
                })
                await shop.save();
            }
        })
        return
    }

    async getProductWhenShopUpdatedAt(match) {
        return this.model.aggregate([
            {
                $match: match
            },
            {
                $lookup: {
                    from: 'products',
                    as: 'products',
                    let: {
                        shopId: '$_id',
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        $expr: {
                                            $eq: [
                                                '$shop_id',
                                                '$$shopId',
                                            ],  
                                        },
                                    },
                                    {
                                        allow_to_sell: true,
                                        is_approved: "approved",
                                        deleted_at: null,
                                    },
                                ],
                            },
                        },
                    ],
                },
            }, 
            {
                $project: {
                    _id: 0,
                    products: 1
                }
            }
        ])
    }

    async getAllShopActive() {
        return this.model.aggregate([
            {
                $match: {
                    is_approved: true,
                    pause_mode: false
                }
            }
        ])
    }
}

class ShopQueryHelper {
    public static advanceShop = [
        {
            $lookup: {
                from: "users",
                let: {
                    user_id: "$user_id"
                },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$_id", "$$user_id"]
                            }
                        }
                    },
                    {
                        $lookup: {
                            from: "galleryimages",
                            let: {
                                user_id: "$_id"
                            },
                            pipeline: [
                                {
                                    $match: {
                                        '$expr': {
                                            $and: [
                                                {
                                                    $eq: ["$galleryImageAbleId", "$$user_id"]
                                                },
                                                {
                                                    $eq: ["$galleryImageAbleType", "1"]
                                                }
                                            ]
                                        },
                                    }
                                }
                            ],
                            as: "gallery_image"
                        }
                    },
                    {
                        $unwind: {
                            path: "$gallery_image",
                            preserveNullAndEmptyArrays: true
                        }
                    }
                ],
                as: "user"
            }
        },
        {
            $unwind: {
                path: "$user",
                preserveNullAndEmptyArrays: false
            }
        }
    ] 

    public static countProduct = [
        {
            $lookup: {
                from: "products",
                let: {
                    shop_id: "$_id",
                },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$shop_id", "$$shop_id"]
                            },
                            is_approved: "approved", deleted_at: null, allow_to_sell: true, is_sold_out: false, quantity: {$gt: 0}
                        }
                    },
                    {
                        $group: {
                            _id: "$shop_id",
                            total_products: { $sum: 1 }
                        }
                    }
                ],
                as: "products"
            }
        },
        {
            $unwind: {
                path: "$products",
                preserveNullAndEmptyArrays: false
            }
        },
    ]
}
