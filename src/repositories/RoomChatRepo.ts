import { BaseRepository } from './BaseRepository';
import RoomChat from '../models/RoomChat';
import { RoomChatDoc } from '../interfaces/RoomChat';
import { RoomChatDTO } from '../DTO/RoomChatDTO';
import { cloneObj, ObjectId } from '../utils/helper';
import { ChatType } from '../services/sockets/definitions';
import { Collection } from '../utils/collection';
import { Result } from 'express-validator';
import { dateTimeToString } from '../utils/dateUtil';
import { ShopRepo } from "./ShopRepo"
import { InMemoryMessageForRoomStore } from '../SocketStores/MessageStore';
import _ from "lodash";
export class RoomChatRepo extends BaseRepository<RoomChatDoc> {
    private static instance: RoomChatRepo;

    constructor() {
        super();
        this.model = RoomChat;
    }

    public static getInstance(): RoomChatRepo {
        if (!RoomChatRepo.instance) {
            RoomChatRepo.instance = new RoomChatRepo();
        }

        return RoomChatRepo.instance;
    }
    async getRoom(search, is_all = true) {
        let matchUser: any = {};
        let paginate = {
            // limit: 10000,
            limit: search.limit,
            page: search.page,
        };
        if (search.userId)
            matchUser.users = {
                $elemMatch: { _id: search.userId + '' },
            };
        if (
            search.type == ChatType.LIVE_STREAM ||
            search.type == ChatType.CHAT
        ) {
            matchUser.type = search.type;
        }
        let resultGetId = await this.model.find(matchUser);
        // console.log(resultGetId.length);

        resultGetId = resultGetId.map(room => room._id);
        // resultGetId = [ObjectId('60a380910330280012474671')];
        matchUser = {};
        if (search.userName)
            matchUser.userName = {
                $regex: `${search.userName}`,
                $options: 'i',
            };
        let result = await this.model.aggregate([
            {
                $match: {
                    users: { $elemMatch: matchUser },
                    _id: { $in: resultGetId },
                },
            },
            {
                $project: {
                    _id: '$_id',
                    creator_id: '$creator_id',
                    is_group: '$is_group',
                    messages: '$messages',
                    size_messages: { $size: '$messages' },
                    users: '$users',
                    file_storages: '$file_storages',
                    type: '$type',
                    name: '$name',
                    createdAt: '$createdAt',
                },
            }
        ]);
        // list
        const RoomStore = InMemoryMessageForRoomStore.getInstance();
        if (!is_all && search.userId && search.type == ChatType.CHAT) {
            result = result.filter(room => {
                for (let i = 0; i < room.users.length; i++) {
                    if (
                        room.users[i]._id == search.userId + '' &&
                        room.users[i].is_hide
                    ) {
                        return 0;
                    }
                }
                return 1;
            });
        }

        for (let i = 0; i < result.length; i++) {
            let room = result[i];

            let room_redis = await RoomStore.getRoom(room._id.toString());
            if (room_redis) {
                room.last_message = room_redis.messages[room_redis.messages.length - 1]
                room.users = room_redis.users;
                room.messages = room_redis.messages
                room.last_object = room_redis.last_object
            }
            let roomChatDTO = new RoomChatDTO(room);
            if (search.userId) {
                let resultUser = room.users.find(
                    user => user._id == search.userId
                );
                let red_dot = 0
                if (room?.last_message && room?.last_message?.user_id) {

                    red_dot = room?.last_message?.user_id.toString() == search.userId ? 0 : resultUser?.red_dot
                }
                roomChatDTO.setAttribute('red_dot', red_dot)
            }
            result[i] = roomChatDTO.toListJSON();
        }

        result = new Collection(cloneObj(result));
        result = result.filter((e) => {
            return e.last_message
        })
        result.sort((a, b) => {
            return parseInt(a?.last_message?.id || 0) >=
                parseInt(b?.last_message?.id || 0)
                ? -1
                : 1;
        });
        return result.pagination(paginate.page, paginate.limit);
    }

    async getTotalRedDotOfUser(user_id) {
        let matchUser: any = {};
        matchUser = {
            users: {
                $elemMatch: { _id: user_id + '' },

            },
            type: ChatType.CHAT
        }
        let result = await this.model.aggregate([
            {
                $match: { ...matchUser },
            },
            {
                $project: {
                    _id: '$_id',
                    creator_id: '$creator_id',
                    is_group: '$is_group',
                    messages: '$messages',
                    size_messages: { $size: '$messages' },
                    users: '$users',
                    file_storages: '$file_storages',
                    type: '$type',
                    name: '$name',
                    createdAt: '$createdAt',
                },
            },
            {
                $match: {
                    size_messages: { $gt: 0 },
                },
            },
        ]);
        let total_red_dot = 0;
        // list

        // hide room of user
        const RoomStore = InMemoryMessageForRoomStore.getInstance();
        result = result.filter(room => {
            for (let i = 0; i < room.users.length; i++) {
                if (
                    room.users[i]._id == user_id + '' &&
                    room.users[i].is_hide
                ) {
                    return 0;
                }
            }
            return 1;
        });
        // console.log(result.length);
        for (let i = 0; i < result.length; i++) {
            let room = result[i];
            let room_redis = await RoomStore.getRoom(room._id.toString());

            if (room_redis) {
                room.last_message = room_redis.messages[room_redis.messages.length - 1]
                room.users = room_redis.users;
                room.messages = room_redis.messages
            }
            let roomChatDTO = new RoomChatDTO(room);

            result[i] = roomChatDTO.toListJSON();
            let resultUser = result[i].users.find(
                user => user._id == user_id
            );
            // if (resultUser?.red_dot && resultUser?.red_dot) {
            //     console.log(result[i])
            // }
            if (room?.last_message?.user_id.toString() != resultUser._id)
                total_red_dot += resultUser?.red_dot && resultUser?.red_dot > 0 ? resultUser?.red_dot : 0
        }
        // result = result.map(room => {
        //     let resultUser = room.users.find(
        //         user => user._id == user_id
        //     );
        //     total_red_dot += resultUser?.red_dot && resultUser?.red_dot > 0 ? resultUser?.red_dot : 0
        // });
        return total_red_dot;
    }

    async getRoomById(id, isAdmin = false) {
        let result: any = await this.findById(id);
        const RoomStore = InMemoryMessageForRoomStore.getInstance();
        let room_redis = await RoomStore.getRoom(result._id.toString());

        if (room_redis) {
            result.last_message = room_redis.messages[room_redis.messages.length - 1]
            result.users = room_redis.users;

            let messages = result.messages.concat(room_redis.messages)
            messages = _.uniqBy(messages, 'id');
            messages.sort((a, b) => a.id - b.id)

            result.messages = messages
        }
        let chatDTO = new RoomChatDTO(result, !isAdmin);
        return chatDTO.toSimpleJSON();
    }

    async mergeMessage(roomRedis) {
        let cloneRoomRedis = cloneObj(roomRedis)
        let roomDB: any = await this.getRoomById(cloneRoomRedis._id)
        let messages = roomDB.messages.concat(cloneRoomRedis.messages)
        messages = _.uniqBy(messages, 'id');
        messages.sort((a, b) => a.id - b.id)
        roomDB.messages = messages
        return roomDB
    }

    async createRoom(room) {
        if (room.type == ChatType.LIVE_STREAM) {
            room.users = room.users.map(user => {
                user.is_muted = false;
                return user;
            });
        }
        if (room.type == ChatType.CHAT) {
            let matchUser: any = {};
            matchUser.users = {
                $elemMatch: { _id: room.users[0]._id + '' },
            };
            matchUser.type = ChatType.CHAT;
            let resultCheck = await this.model.find(matchUser);
            // return resultCheck;
            resultCheck = cloneObj(resultCheck);
            for (let i = 0; i < resultCheck.length; i++) {
                if (
                    resultCheck[i].users.length == 2 &&
                    ((resultCheck[i].users[0]._id == room.users[0]._id &&
                        resultCheck[i].users[1]._id == room.users[1]._id) ||
                        (resultCheck[i].users[1]._id == room.users[0]._id &&
                            resultCheck[i].users[0]._id == room.users[1]._id))
                ) {
                    return resultCheck[i];
                }
            }
        }
        let result = await this.create(room);
        return result;
    }

    async userJoinRoom(roomId, userJoin) {
        let room: any = await this.getRoomById(roomId);
        if (!room) return;
        if (room.type == ChatType.LIVE_STREAM) {
            userJoin.is_muted = false;
        }
        let sizeMess = room.messages.length;
        let index = room.users.findIndex(
            user => user._id + '' == userJoin._id + ''
        );
        if (index == -1) {
            userJoin.seen_last_message_index = sizeMess - 1;
            userJoin.is_online = true;
            room.users.push(userJoin);
        } else {
            room.users[index].is_online = true;
            room.users[index].userName = userJoin.userName;
            room.users[index].avatar = userJoin.avatar;
            room.users[index].gender = userJoin.gender;
        }
        let result = await this.update(room._id, room);
        return result;
    }

    async userLeftRoom(roomId, userJoin) {
        let room: any = await this.getRoomById(roomId);
        if (!room) return;
        let index = room.users.findIndex(
            user => user._id + '' == userJoin._id + ''
        );
        if (index >= 0) {
            userJoin.is_online = false;
            room.users[index] = userJoin;
        }
        let result = await this.update(room._id, room);
        return result;
    }

    async addMessage(roomId, message, tryhard = 0, is_socket = 1) {
        let result;
        // console.log("---------------------log-chat-" + message.content + "--------------");
        // console.log(message);
        try {
            // if (!is_socket) {
            //     console.log("---------đây là gọi api-------------");
            // }
            let room: any = await this.findById(roomId);
            // console.log(room._id);
            if (!room) return;

            // check id message exist
            for (let i = 0; i < room.messages.length; i++) {
                if (room.messages[i].id == message.id) {
                    return;
                }
            }

            let messages = room.messages
            messages.push(message);
            // console.log("push Message");
            let sizeMess = room.messages.length;

            let users: any = []
            for (let user of room.users) {
                // user chat is user seen last_message
                if (!user.shop_id) {
                    let shop = await ShopRepo.getInstance().findShopByUserId(user._id.toString());
                    user.shop_id = shop ? shop._id.toString() : null
                }
                user.is_hide = false;
                user.seen_last_message_index =
                    user._id == message.user_id
                        ? sizeMess - 1
                        : user.seen_last_message_index || null;
                let red_dot = messages.length - user.seen_last_message_index - 1
                user.seen_last_message_id = sizeMess && user._id.toString() == message.user_id.toString()
                    ? room.messages[sizeMess - 1].id
                    : null;
                user.red_dot = red_dot > 0 ? red_dot : 0
                users.push(user);
            };

            if (message.type == 'image') {
                if (!room.file_storages) room.file_storages = [];
                room.file_storages.push({
                    message_id: message.id,
                    type: message.type,
                    url: message.content,
                    user_id: message.user_id,
                });
            }
            room.messages = messages;
            room.users = users;
            let room_update = {
                messages,
                users,
                last_message: message,
                last_object: !['image', 'text'].includes(message.type) ? message : room?.last_object
            }
            room.last_message = message
            room.last_object = !['image', 'text'].includes(message.type) ? message : room?.last_object
            await room.save();
            //result = await this.findById(roomId);
            result = await this.findOneAndUpdate({ _id: ObjectId(room._id) }, room_update);
            result = await this.findById(roomId);
            let index = -1;
            if (tryhard < 10) {
                let check_save_message = 0
                for (let i = 0; i < result.messages.length; i++) {
                    if (result.messages[i].id == message.id) {
                        check_save_message = 1
                        index = i;
                    }
                }
                if (!check_save_message)
                    await this.addMessage(roomId, message, tryhard + 1)
            }

            // console.log(result.messages[index]);
            // console.log(message)
            // console.log("---------------------end-log-chat-" + roomId + " try " + tryhard + ": " + message.content + "--------------");
        } catch (error) {
            console.log("error chat:")
            console.log(error)
        }
        return result;
    }

    async deleteMessage(roomId, messageId, user) {
        let room: any = await this.getRoomById(roomId);
        if (!room) return;
        let indexMessDel = -1;
        let messages = room.messages.map((message, index) => {
            if (message.id == messageId && message.user_id == user._id) {
                message.deleted_at = Math.floor(Date.now() / 1000);
                indexMessDel = index;
            }
            return message;
        });
        if (indexMessDel > 0) {
            room.users = room.users.map(user => {
                if (
                    user.seen_last_message_index &&
                    user.seen_last_message_index >= indexMessDel
                ) {
                    user.seen_last_message_index -= 1;
                }
                return user;
            });
        }
        room.messages = messages;
        let result = await this.update(room._id, room);
        return result;
    }

    async saveMute(roomId, userId) {
        let room: any = await this.getRoomById(roomId);
        if (!room) return;
        room.users = room.users.map(user => {
            if (user._id == userId) user.is_muted = true;
            return user;
        });
        let result = await this.update(room._id, room);
        return result;
    }

    async saveUnmute(roomId, userId) {
        let room: any = await this.getRoomById(roomId);
        if (!room) return;
        room.users = room.users.map(user => {
            if (user._id == userId) user.is_muted = false;
            return user;
        });
        let result = await this.update(room._id, room);
        return result;
    }

    async readLastMessage(roomId, userRead) {
        let room: any = await this.getRoomById(roomId);
        let sizeMess = room.messages.length;
        let users = room.users.map(user => {
            user.seen_last_message_index =
                user._id == userRead._id
                    ? sizeMess - 1
                    : user.seen_last_message_index || null;
            user.red_dot =
                sizeMess -
                (user.seen_last_message_index == null
                    ? 0
                    : user.seen_last_message_index + 1);
            user.seen_last_message_id = sizeMess ? room.messages[sizeMess - 1].id : null;
            return user;
        });
        room.users = users;
        let result = await this.update(room._id, room);
        // console.log(re)
        return result;
    }

    async hideRoomChatByUser(roomId, userId) {
        let room: any = await this.getRoomById(roomId);
        let users = room.users.map(user => {
            user.is_hide = user._id == userId;
            return user;
        });
        room.users = users;
        let result = await this.update(room._id, room);
        return result;
    }
}
