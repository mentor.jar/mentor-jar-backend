import { BaseRepository } from "./BaseRepository";
import Voucher from "../models/Voucher"
import { VoucherDoc } from "../interfaces/Voucher";
import { cloneObj, isMappable, ObjectId, timeOutOfPromise } from "../utils/helper";
import _ from "lodash";
import { OrderRepository } from "./OrderRepo";
import moment from "moment";
import { UserRepo } from "./UserRepo";
import { ShowType } from "../models/enums/voucher";
import { InMemoryVoucherStore } from "../SocketStores/VoucherStore";
import { handleTimeReq } from "../controllers/handleRequests/shopStatistic/userTrends";
export class VoucherRepository extends BaseRepository<VoucherDoc> {

    private static instance: VoucherRepository;
    private advanceProductQuery: any = {
        $lookup:
        {
            from: "products",
            let: {
                product_ids: "$products",
                deleted_at: "$deleted_at",
            },
            pipeline: [
                {
                    $match: {
                        $and: [
                            {
                                '$expr': {
                                    $and: [
                                        {
                                            '$in': ['$_id', '$$product_ids']
                                        }
                                    ]
                                },
                            },
                            {
                                "allow_to_sell": true
                            },
                            {
                                "is_approved": 'approved'
                            },
                            {
                                $or: [
                                    { deleted_at: { $exists: false } },
                                    { deleted_at: null }
                                ]
                            }
                        ]
                    }
                },
                {
                    $lookup: {
                        from: "variants",
                        let: {
                            product_id: "$_id"
                        },
                        pipeline: [
                            {
                                $match: {
                                    $and: [
                                        {
                                            '$expr': {
                                                $and: [
                                                    {
                                                        $eq: ["$product_id", "$$product_id"]
                                                    }
                                                ]
                                            },
                                        }
                                    ]
                                }
                            }
                        ],
                        as: "variants"
                    }
                }
            ],
            as: "products"
        }
    }

    private itemInfos = { _id: "$_id", target: "$target", type: "$type", value: "$value", max_discount_value: "$max_discount_value", 
    max_budget: "$max_budget", display_mode: "$display_mode", products: "$products", shops: "$shops", users: "$users", labels: "$labels", 
    approve_status: "$approve_status", used_by: "$used_by", name: "$name", code: "$code", start_time: "$start_time", end_time: "$end_time", 
    save_quantity: "$save_quantity", use_quantity: "$use_quantity", available_quantity: "$available_quantity", using_by: "$using_by", 
    discount_by_range_price: "$discount_by_range_price", min_order_value: "$min_order_value", max_order_value: "$max_order_value", classify: "$classify", conditions: "$conditions", 
    is_public: "$is_public", cash_back_ref: "$cash_back_ref", used_budget: '$used_budget', created_at: "$created_at", updated_at: "$updated_at" }

    private advanceShopQuery: any = {
        $lookup:
        {
            from: "shops",
            let: {
                shop_ids: "$shops"
            },
            pipeline: [
                {
                    $match: {
                        $and: [
                            {
                                '$expr': {
                                    '$in': ['$_id', '$$shop_ids']
                                }
                            },
                            {
                                $or: [
                                    { deleted_at: { $exists: false } },
                                    { deleted_at: null }
                                ]
                            }
                        ]
                    }
                },
                {
                    $lookup: {
                        from: "users",
                        let: {
                            user_id: "$user_id"
                        },
                        pipeline: [
                            {
                                $match: {
                                    $and: [
                                        {
                                            '$expr': {
                                                $and: [
                                                    {
                                                        $eq: ["$_id", "$$user_id"]
                                                    }
                                                ]
                                            },
                                        }
                                    ]
                                }
                            },
                            {
                                $lookup: {
                                    from: "galleryimages",
                                    let: {
                                        user_id: "$_id"
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                '$expr': {
                                                    $and: [
                                                        {
                                                            $eq: ["$galleryImageAbleId", "$$user_id"]
                                                        },
                                                        {
                                                            $eq: ["$galleryImageAbleType", "1"]
                                                        }
                                                    ]
                                                },
                                            }
                                        }
                                    ],
                                    as: "gallery_image"
                                }
                            },
                            {
                                $unwind: {
                                    path: "$gallery_image",
                                    preserveNullAndEmptyArrays: true
                                }
                            }

                        ],
                        as: "user"
                    }
                },
                {
                    $unwind: {
                        path: "$user",
                        preserveNullAndEmptyArrays: true
                    }

                }
            ],
            as: "shops"
        }
    }

    private advanceOwnShopQuery: any = {
        $lookup: {
            from: "shops",
            let: {
                shop_id: "$shop_id"
            },
            pipeline: [
                {
                    $match: {
                        $and: [
                            {
                                '$expr': {
                                    $and: [
                                        {
                                            $eq: ["$_id", "$$shop_id"]
                                        }
                                    ]
                                },
                            }
                        ]
                    },

                },
                {
                    $lookup: {
                        from: "users",
                        let: {
                            user_id: "$user_id"
                        },
                        pipeline: [
                            {
                                $match: {
                                    $and: [
                                        {
                                            '$expr': {
                                                $and: [
                                                    {
                                                        $eq: ["$_id", "$$user_id"]
                                                    }
                                                ]
                                            },
                                        }
                                    ]
                                }
                            },

                        ],
                        as: "user"
                    }
                },
                {
                    $unwind: {
                        path: "$user",
                        preserveNullAndEmptyArrays: true
                    }

                }

            ],
            as: "shop"
        }
    }

    private constructor() {
        super();
        this.model = Voucher;
    }

    private orderRepo = OrderRepository.getInstance()
    private userRepo = UserRepo.getInstance()
    private voucherAndOrderCache = InMemoryVoucherStore.getInstance();

    public static getInstance(): VoucherRepository {
        if (!VoucherRepository.instance) {
            VoucherRepository.instance = new VoucherRepository();
        }

        return VoucherRepository.instance;
    }

    async getDetailVoucher(id) {
        const match: any = {
            $and: [
                {_id: ObjectId(id)},
                {
                    $or: [
                        { deleted_at: { $exists: false } },
                        { deleted_at: null }
                    ]
                },
                {
                    $or: [
                        {
                            start_time: { '$lte': new Date() },
                            end_time: { '$gte': new Date() },
                        },
                        {
                            start_time: { '$gt': new Date() },
                        }
                    ]
                },
                {available_quantity: { '$gt': 0 }}
            ]
        }

        const result = await this.model.aggregate([
            {
                $match: match
            },
            this.advanceOwnShopQuery,
            {
                $unwind: {
                    path: "$shop",
                    preserveNullAndEmptyArrays: true
                }

            }
        ])
        return result[0]
    }

    async getVoucherBySystem(limit = 20, page = 1, filterType = 'ALL') {
        const match: any = {
            shop_id: null,
            $or: [
                { deleted_at: { $exists: false } },
                { deleted_at: null }
            ]
        }
        switch (filterType) {
            case 'RUNNING':
                match.start_time = { '$lte': new Date() }
                match.end_time = { '$gte': new Date() }
                break;
            case 'EXPIRED':
                match.end_time = { '$lte': new Date() }
                break;
            case 'INCOMING':
                match.start_time = { '$gte': new Date() }
                break;
            default:
                break;
        }
        const query = this.model.aggregate([
            {
                $match: match
            },
            {
                $sort: { updated_at: -1 }
            }
        ])
        const result = await this.model
            .aggregatePaginateCustom(query, { page, limit })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        const data = result.docs;
        return { data, paginate }
    }

    async getVoucherSystemCombineBanner() {
        const match: any = {
            shop_id: null,
            $or: [
                { deleted_at: { $exists: false } },
                { deleted_at: null }
            ],
            end_time: { '$gte': new Date() },
            classify: { $in: ['free_shipping', 'discount'] },
            is_public: true,
            available_quantity: { '$gt': 0 }
        }

        return this.model.aggregate([
            {
                $match: match,
            },
            {
                $lookup: {
                    from: 'ebanners',
                    as: 'banner',
                    let: {
                        voucherId: '$_id',
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        $expr: {
                                            $and: [
                                                {
                                                    $in: [
                                                        '$$voucherId',
                                                        '$vouchers',
                                                    ],
                                                },
                                            ],
                                        },
                                    },
                                    {
                                        end_time: { $gte: new Date() },
                                    },
                                    {
                                        shop_id: null
                                    }
                                ],
                            },
                        },
                    ],
                },
            },
            {
                $unwind: {
                    path: "$banner",
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $sort: { banner: -1, end_time: 1 },
            },
        ]);
    }

    async getVoucherShop(page = 1, limit = 10) {
        const match: any = {
            shop_id: { $exists: true },
            $or: [
                { deleted_at: { $exists: false } },
                { deleted_at: null }
            ],
            end_time: { '$gte': new Date() },
            is_public: true,
            available_quantity: { '$gt': 0 }
        }

        const query = this.model.aggregate([
            {
                $match: match,
            },
            {
                $lookup:
                {
                    from: "shops",
                    as: "shop",
                    let: {
                        id: "$shop_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        {
                                            $eq: ["$_id", "$$id"]
                                        },
                                    ]
                                }
                            }
                        },
                        {
                            $lookup: {
                                from: "users",
                                as: "user",
                                let: {
                                    user_id: "$user_id"
                                },
                                pipeline: [
                                    {
                                        $match: {
                                            $and: [
                                                {
                                                    '$expr': {
                                                        $and: [
                                                            {
                                                                $eq: ["$_id", "$$user_id"]
                                                            }
                                                        ]
                                                    },
                                                }
                                            ]
                                        }
                                    },
                                    {
                                        $lookup: {
                                            from: "galleryimages",
                                            let: {
                                                user_id: "$_id"
                                            },
                                            pipeline: [
                                                {
                                                    $match: {
                                                        '$expr': {
                                                            $and: [
                                                                {
                                                                    $eq: ["$galleryImageAbleId", "$$user_id"]
                                                                },
                                                                {
                                                                    $eq: ["$galleryImageAbleType", "1"]
                                                                }
                                                            ]
                                                        },
                                                    }
                                                },
                                                {
                                                    $project: {
                                                        _id: 0,
                                                        url: 1
                                                    }
                                                }
                                            ],
                                            as: "gallery_image"
                                        }
                                    },
                                    {
                                        $unwind: {
                                            path: "$gallery_image",
                                            preserveNullAndEmptyArrays: true
                                        }
                                    },
                                    {
                                        $project: {
                                            _id: 1,
                                            userName: 1,
                                            nameOrganizer: 1,
                                            gallery_image: 1,
                                        }
                                    }
                                ],
                            }
                        },
                        {
                            $unwind: {
                                path: "$user",
                                preserveNullAndEmptyArrays: true
                            }

                        }, 
                        {
                            $project: {
                                _id: 1,
                                user: 1
                            }
                        }
                    ],
                }
            },
            {
                $unwind: {
                    path: "$shop",
                    preserveNullAndEmptyArrays: true
                }

            },
            {
                $group: {
                    _id: "$shop_id",
                    vouchers: {"$push":"$$ROOT"}
                }
            },
            {
                $sort: { "vouchers.end_time": 1, "vouchers.value": -1, "vouchers.created_at": 1 },
            },
        ]);
        
        const result = await this.model
            .aggregatePaginateCustom(query, { page, limit })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        const data = result.docs;
        
        return { data, paginate};
    }

    async getSystemVoucherForPreview() {
        const currentTime = new Date();

        const match: any = {
            shop_id: null,
            $or: [
                {
                    start_time: { '$lte': currentTime },
                    end_time: { '$gte': currentTime },
                    deleted_at: null
                },
                {
                    start_time: { '$gt': currentTime },
                    deleted_at: null
                }
            ],
            classify: { $in: ['free_shipping', 'discount'] },
            available_quantity: { '$gt': 0 }
        }

        const vouchers = await this.model.aggregate([
            {
                $match: match,
            },
            {
                $sort: { value: -1 },
            },
        ]);

        // Continue checking max budget condition
        const voucherNotOutOfMaxBudget:Array<any> = await this.getValidVoucherByMaxBudget(vouchers)
        return voucherNotOutOfMaxBudget;
    }

    async getVoucherByShopForPreview(shopId, products) {
        const productList = isMappable(products) ? products.map(product_id => {
            return { products: ObjectId(product_id) }
        }) : [];

        let match: any = {
            $or: [
                { target: 'shop' },
                {
                    target: 'product',
                    $or: [
                        ...productList,
                        { products: [] }
                    ]
                }
            ],
            shop_id: ObjectId(shopId),
            start_time: { '$lte': new Date() },
            end_time: { '$gte': new Date() },
            deleted_at: null,
            available_quantity: { '$gt': 0 },
        }

        const vouchers = await this.model.aggregate([
            {
                $match: match
            },
            {
                $sort: { "value": -1 }
            },
        ]);

        // Continue checking max budget condition
        const voucherNotOutOfMaxBudget:Array<any> = await this.getValidVoucherByMaxBudget(vouchers)
        return voucherNotOutOfMaxBudget;
    }

    async getVoucherWithFilter(shopId, limit = 20, page = 1, filterType = 'ALL', isIncludeHidden = true) {
        const match: any = {
            shop_id: ObjectId(shopId),
            $or: [
                { deleted_at: { $exists: false } },
                { deleted_at: null }
            ],
        }
        if(!isIncludeHidden) {
            match.display_mode = 'all'
        }
        switch (filterType) {
            case 'RUNNING':
                match.start_time = { '$lte': new Date() }
                match.end_time = { '$gte': new Date() }
                break;
            case 'EXPIRED':
                match.end_time = { '$lte': new Date() }
                break;
            case 'INCOMING':
                match.start_time = { '$gte': new Date() }
                break;
            default:
                break;
        }
        const query = this.model.aggregate([
            {
                $match: match
            },
            {
                $sort: { updated_at: -1 }
            }
        ])
        const result = await this.model
            .aggregatePaginateCustom(query, { page, limit })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        const data = result.docs;
        return { data, paginate }
    }

    async getVoucherAllRunning() {
        const match: any = {
            $or: [
                { deleted_at: { $exists: false } },
                { deleted_at: null }
            ],
            // start_time: { '$lte': new Date() },
            // end_time:{ '$gte': new Date() }
        }

        const data = await this.model.aggregate([
            {
                $match: match
            },
            {
                $sort: { updated_at: -1 }
            },
            {
                $lookup: {
                    from: "shops",
                    let: {
                        shop_id: "$shop_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        '$expr': {
                                            $and: [
                                                {
                                                    $eq: ["$_id", "$$shop_id"]
                                                }
                                            ]
                                        },
                                    }
                                ]
                            },

                        },
                        {
                            $lookup: {
                                from: "users",
                                let: {
                                    user_id: "$user_id"
                                },
                                pipeline: [
                                    {
                                        $match: {
                                            $and: [
                                                {
                                                    '$expr': {
                                                        $and: [
                                                            {
                                                                $eq: ["$_id", "$$user_id"]
                                                            }
                                                        ]
                                                    },
                                                }
                                            ]
                                        }
                                    },

                                ],
                                as: "user"
                            }
                        },
                        {
                            $unwind: {
                                path: "$user",
                                preserveNullAndEmptyArrays: true
                            }

                        }

                    ],
                    as: "shop"
                }
            },
            {
                $unwind: {
                    path: "$shop",
                    preserveNullAndEmptyArrays: true
                }

            }
        ])
        return data
    }

    async getVoucherCMS(req) {
        let { limit = 20, page = 1, showType, shopId, filterType, filterTarget } = req.query;
        const { start_time: dateStart, end_time: dateEnd } = handleTimeReq(req);

        let match: any = {}
        if (dateStart && dateEnd) {
            match = {
                $and: [
                    { start_time: { $gte: dateStart } },
                    { start_time: { $lte: dateEnd } },
                ],
            };
        } else {
            if (dateStart) {
                match.start_time = { $gte: dateStart };
            }
            if (dateEnd) {
                match.start_time = { $lte: dateEnd };
            }
        }

        switch (showType) {
            case ShowType.SYSTEM:
                match = {
                    shop_id: null 
                }
                break;
                
            case ShowType.SHOP:
                if (shopId) {
                    match = {
                        $or: [
                            { shop_id: ObjectId(shopId) },
                            { shops: ObjectId(shopId) }
                        ]
                    }
                }
                break;
        
            default:
                break;
        }

        if (filterTarget && filterTarget !== "" && filterTarget !== "none") {
            match.target = filterTarget;
        }

        switch (filterType) {
            case 'RUNNING':
                match.start_time = { '$lte': new Date() }
                match.end_time = { '$gte': new Date() }
                break;
            case 'EXPIRED':
                match.end_time = { '$lte': new Date() }
                break;
            case 'INCOMING':
                match.start_time = { '$gte': new Date() }
                break;
            default:
                break;
        }
        const query = this.model.aggregate([
            {
                $match: match
            },
            {
                $sort: { updated_at: -1 }
            },
            {
                $lookup: {
                    from: "shops",
                    let: {
                        shop_id: "$shop_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        '$expr': {
                                            $and: [
                                                {
                                                    $eq: ["$_id", "$$shop_id"]
                                                }
                                            ]
                                        },
                                    }
                                ]
                            },

                        },
                        {
                            $lookup: {
                                from: "users",
                                let: {
                                    user_id: "$user_id"
                                },
                                pipeline: [
                                    {
                                        $match: {
                                            $and: [
                                                {
                                                    '$expr': {
                                                        $and: [
                                                            {
                                                                $eq: ["$_id", "$$user_id"]
                                                            }
                                                        ]
                                                    },
                                                }
                                            ]
                                        }
                                    },

                                ],
                                as: "user"
                            }
                        },
                        {
                            $unwind: {
                                path: "$user",
                                preserveNullAndEmptyArrays: true
                            }

                        }

                    ],
                    as: "shop"
                }
            },
            {
                $unwind: {
                    path: "$shop",
                    preserveNullAndEmptyArrays: true
                }

            }
        ])
        const result = await this.model
            .aggregatePaginateCustom(query, { page, limit })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        const data = result.docs;
        return { data, paginate }
    }

    async getDetailVoucherCMS(id) {
        const match: any = {
            _id: ObjectId(id),
        }

        const result = await this.model.aggregate([
            {
                $match: match
            },
            this.advanceProductQuery,
            this.advanceShopQuery,
            this.advanceOwnShopQuery,
            {
                $unwind: {
                    path: "$shop",
                    preserveNullAndEmptyArrays: true
                }

            }

        ])
        return result[0]
    }

    /**
     * This function use to get system voucher at pre-checkout screen. It base on some conditions as:
     * Total value of order
     * Shipping method
     * Payment method
     * Shop that includes product to buy
     * User buyer
     * Product to buy
     * Max budget
     */
    async getSystemVoucherWithCondition({ total_value, shipping_method, payment_method, shops, user, products, order_room_id }) {
        const shopList = shops.map(shop_id => {
            return { shops: ObjectId(shop_id) }
        })

        const productList = products.map(product_id => {
            return { products: ObjectId(product_id) }
        })

        const currentTime = new Date()

        let match: any = {
            $and: [
                {
                    $or: [
                        {
                            start_time: { '$lte': currentTime },
                            end_time: { '$gte': currentTime },
                            deleted_at: null
                        },
                        { // allow show voucher in the future but don't allow to use
                            start_time: { '$gt': currentTime },
                            deleted_at: null
                        }
                    ]
                },
                { available_quantity: { '$gt': 0 } },
                {
                    $or: [
                        { shop_id: { $exists: false } },
                        { shop_id: null }
                    ]
                },
                {
                    $or: [
                        {
                            target: 'shop',
                            $or: [
                                ...shopList,
                                { shops: [] }
                            ]
                        },
                        {
                            target: 'product',
                            $or: [
                                ...productList,
                                { products: [] }
                            ]
                        },
                        {
                            $and: [
                                { target: 'user' },
                                { users: ObjectId(user._id) }
                            ]
                        },
                        {
                            $and: [
                                { target: 'kol'},
                                { kol_user: ObjectId(user._id) }
                            ]
                        },
                        {
                            $or: [
                                { 
                                    $and: [
                                        { target: 'system'},
                                        { classify: { $ne: 'cash_back_discount' } }
                                    ]
                                },
                                {
                                    $and: [
                                        { classify: 'cash_back_discount' },
                                        { 'cash_back_ref.user_id': ObjectId(user._id) }
                                    ]
                                },
                                { target: { $in: ['label', 'newbie'] } },
                                {
                                    $and: [
                                        {target: { $in: ['introduce', 'introduced'] } },
                                        {users: ObjectId(user._id)},
                                    ]
                                }
                            ]
                        } 
                    ]
                }
            ]
        }

        // Get all system voucher is available to use
        let results = await this.model.aggregate([
            {
                $match: match
            },
            {
                $group: {
                    _id: "$classify",
                    items: { $push: this.itemInfos }
                }
            },
            {
                $addFields: { classify: "$_id" }
            },
            {
                $project: { _id: 0 }
            },
        ])

        results = cloneObj(results)

        // Filter by limit use quantity by user
        results = results.map(shop => {
            let vouchers = shop.items;
            
            vouchers = vouchers.filter(voucher => {
                if(voucher.target !== 'newbie') {
                    if(voucher.conditions && voucher.conditions["limit_per_user"] && voucher.used_by) {
                        return !voucher.used_by[user._id] || voucher.used_by[user._id] < +voucher.conditions["limit_per_user"]
                    }
                    else if((voucher.target === "introduced" || voucher.target === "introduce") && voucher.used_by) {
                        // Voucher cho nguoi gioi thieu & nguoi dc gioi thieu chi dung 1 lan
                        return voucher.used_by[user._id] ? +voucher.used_by[user._id] < 1 : true
                    }
                    return true
                }
                return true
            })
            shop.items = vouchers
            return shop
        })

        // Check shipping method, payment method, times to use condition for each voucher
        for(let t = 0; t < results.length; t++) {
            const ele = results[t]
            for(let i = 0; i < ele.items.length; i++) {
                const voucher = ele.items[i]
                voucher.available = false
                voucher.should_hide = false // for hide voucher if it unavailable for user in the future

                if(voucher.target === 'newbie') {
                    voucher.available = user.is_newbie
                }

                const userVoucher = await this.voucherAndOrderCache.getByKeyAndField(`voucher_${voucher._id.toString()}`, user._id.toString(), true)
                const limitPerUser = voucher.conditions ? voucher.conditions.limit_per_user : null

                // get number of used today
                const historyKey = `voucherHistoryDay_${voucher._id.toString()}`
                const fieldHistory = `${user._id.toString()}_${moment().format("DD-MM-YY")}`
                const usedToDay = await this.voucherAndOrderCache.getByKeyAndField(historyKey, fieldHistory, true)
                const limitVoucherPerDay = voucher.conditions ? voucher.conditions.limit_per_user_by_day : null

                // Check min order value
                if (voucher.min_order_value <= total_value &&
                    ((voucher.target === 'newbie' && voucher.available) || (voucher.target !== 'newbie'))) {
                    if( (voucher.max_order_value && voucher.max_order_value >= total_value) || !voucher.max_order_value ) {
                        // Check another condition
                        if (voucher.conditions && voucher.conditions != {}) {
                            const shippingMethod = voucher.conditions.shipping_method
                            const paymentMethod = voucher.conditions.payment_method
                            // const usedBy = voucher.used_by

                            // check number of times used
                            if (
                                (!limitPerUser || +userVoucher < +limitPerUser) &&
                                (!limitVoucherPerDay || +usedToDay < +limitVoucherPerDay)
                            ) {
                                // Check shipping method & payment method
                                const arrayFilter = shipping_method.filter(item => shippingMethod.includes(item))
                                if (
                                    (
                                        !shippingMethod.length ||
                                        arrayFilter.length
                                    ) &&
                                    (
                                        !paymentMethod.length ||
                                        paymentMethod.includes(payment_method)
                                    )
                                ) {
                                    // Check range value if has
                                    if (voucher.discount_by_range_price && voucher.discount_by_range_price.length) {
                                        let checkRange = false
                                        voucher.discount_by_range_price.forEach(range => {
                                            // console.log(range.from + " / " + total_value + " / " + range.to)
                                            if (_.isEmpty(range) || (range.from <= total_value && (total_value <= range.to || range.to === -1))) {
                                                checkRange = true
                                            }
                                        });
                                        voucher.available = checkRange
                                    }
                                    else {
                                        voucher.available = true
                                    }

                                }
                            }
                        }
                        else {
                            // Check range value if has
                            if (voucher.discount_by_range_price && voucher.discount_by_range_price.length) {
                                let checkRange = false
                                voucher.discount_by_range_price.forEach(range => {
                                    if (_.isEmpty(range) || (range.from <= total_value && (total_value <= range.to || range.to === -1))) {
                                        checkRange = true
                                    }
                                });
                                voucher.available = checkRange
                            }
                            else {
                                voucher.available = true
                            }
                        }
                    }
                }
                
                if(
                    limitPerUser && +userVoucher >= +limitPerUser
                ) {
                    voucher.should_hide = true
                }

                // check time
                if(voucher.available) {
                    voucher.available = !moment(currentTime).isBefore(moment(voucher.start_time))
                }

                if (order_room_id && ele.classify !== 'free_shipping') {
                    voucher.available = false;
                }

            }
        }

        // filter should hide voucher
        for(let t = 0; t < results.length; t++) {
            results[t] = cloneObj(results[t])
            results[t].items = results[t].items.filter(voucher => !voucher.should_hide)
        }

        return results
    }

    async checkValidVoucherSystemIncart({ total_value, shipping_method, payment_method, shops, user_id, products }, id_voucher) {
        const shopList = shops.map(shop_id => {
            return { shops: ObjectId(shop_id) }
        })

        const productList = products.map(product_id => {
            return { products: ObjectId(product_id) }
        })

        let match: any = {
            $and: [
                {
                    _id: ObjectId(id_voucher)
                },
                {
                    start_time: { '$lte': new Date() },
                    end_time: { '$gte': new Date() }
                },
                { available_quantity: { '$gt': 0 } },
                { min_order_value: { '$lte': total_value } },
                {
                    $or: [
                        { max_order_value: { '$gte': total_value } },
                        { max_order_value: null }
                    ]
                },
                {
                    $or: [
                        { shop_id: { $exists: false } },
                        { shop_id: null }
                    ]
                },
                {
                    $or: [
                        {
                            target: 'shop',
                            $or: [
                                ...shopList,
                                { shops: [] }
                            ]
                        },
                        {
                            target: 'product',
                            $or: [
                                ...productList,
                                { products: [] }
                            ]
                        },
                        {
                            $and: [
                                { target: 'user' },
                                { users: ObjectId(user_id) }
                            ]
                        },
                        {
                            $and: [
                                { target: 'kol'},
                                { kol_user: ObjectId(user_id) }
                            ]
                        },
                        {
                            $or: [
                                { target: { $in: ['system', 'label','newbie'] } },
                                {
                                    $and: [
                                        {target: { $in: ['introduce', 'introduced'] } },
                                        {users: ObjectId(user_id)},
                                    ]
                                }
                            ]
                        } 
                    ]
                }
            ]
        }
        let result = await this.model.aggregate([
            {
                $match: match
            }
        ]);

        // Filter by limit use quantity by user
        result = result.filter(voucher => {
            if(voucher.target !== 'newbie') {
                if(voucher.conditions && voucher.conditions["limit_per_user"] && voucher.used_by) {
                    return !voucher.used_by[user_id] || voucher.used_by[user_id] < +voucher.conditions["limit_per_user"]
                }
                else if((voucher.target === "introduced" || voucher.target === "introduce") && voucher.used_by) {
                    // Voucher cho nguoi gioi thieu & nguoi dc gioi thieu chi dung 1 lan
                    return voucher.used_by[user_id] ? +voucher.used_by[user_id] < 1 : true
                }
                return true
            }
            else if(voucher.target === 'system' && voucher.classify === 'cash_back') {
                const discountRange = voucher.discount_by_range_price(item =>
                    item.from <= total_value &&
                    (item.to >= total_value || item.to === -1)
                )
                if(!discountRange) return false
                const userRange = voucher.cash_back_users.find(item => 
                    item.value === discountRange.value && 
                    item.user_ids.find(id => id.toString() === user_id.toString())    
                )
                if(!userRange) return false
            }
            return true
        })

        // payment method
        if (result.length && payment_method && result[0].conditions) {
            if (result[0].conditions.payment_method.length > 0 && !result[0].conditions.payment_method.includes(payment_method.toString())) {
                result = [];
            }
        }

        // shipping method
        if (result.length && shipping_method && result[0].conditions) {
            const arrayFilter = shipping_method.filter(item => result[0].conditions.shipping_method.includes(item));
            if (result[0].conditions.shipping_method.length > 0 && !arrayFilter.length) {
                result = [];
            }
        }
        // Check times condition to use voucher
        if (result.length
            && result[0].target !== 'newbie'
            && result[0].used_by
            && result[0].used_by[user_id]
            && result[0].conditions
            && result[0].used_by[user_id] >= result[0].conditions.limit_per_user) {
            result = [];
        }

        // Newbie
        if(result.length && result[0].target === "newbie") {
            const user:any = await this.userRepo.findOne({_id: ObjectId(user_id)})
            if(!user?.is_newbie) {
                result = []
            }
        }
        
        return result
    }

    async checkValidCashBackVoucher({ total_value, user_id }, id_voucher) {
        const currentTime = new Date()
        let match: any = {
            $and: [
                {
                    _id: ObjectId(id_voucher)
                },
                {
                    start_time: { '$lte': currentTime },
                    end_time: { '$gte': currentTime }
                },
                { available_quantity: { '$gt': 0 } },
                { min_order_value: { '$lte': total_value } },
                {
                    $or: [
                        { max_order_value: { '$gte': total_value } },
                        { max_order_value: null }
                    ]
                },
                { classify: 'cash_back'}
            ]
        }
        let voucher = await this.model.findOne(match);
        console.log("voucher: ", voucher);
        
        const isValidForUser = !voucher?.using_by?.[user_id] ? true : false

        if(voucher && isValidForUser) {
            const subVoucherDiscount = await this.model.findOne({
                'start_time': { '$lte': currentTime },
                'end_time': { '$gte': currentTime },
                'cash_back_ref.user_id': ObjectId(user_id),
                'cash_back_ref.cash_back_id': ObjectId(voucher._id),
                'available_quantity': { '$gt': 0 }
            })
            return !subVoucherDiscount ? voucher : null
        }
        return null
    }

    async findById(id: string) {
        try {
            const res = await this.model.findOne({
                $and: [
                    { _id: ObjectId(id) },
                    { $or: [{ deleted_at: { $exists: false } }, { deleted_at: null }] }
                ]
            })
            return res
        } catch (error) {
            return null
        }
    }
    public findValidWithIds(voucherIds: Array<string>) {
        return this.find({
            $and: [
                {
                    _id: {
                        $in: voucherIds
                    },
                },
                {
                    $or: [
                        {
                            start_time: { '$lte': new Date() },
                            end_time: { '$gte': new Date() },
                        },
                        {
                            start_time: { '$gt': new Date() }
                        },
                    ]
                },
                {
                    available_quantity: { '$gt': 0 }
                },
                {
                    $or: [
                        { deleted_at: { $exists: false } },
                        { deleted_at: null }
                    ]
                }
            ]
        })
    }

    delete(voucherId: string): Promise<any> {
        return this.model.findOne({ _id: voucherId }).updateOne({ deleted_at: new Date() });
    }

    async getValidVoucherByMaxBudget(vouchers) {
        const promises = vouchers.map(voucher => {
            return new Promise(async (resolve, reject) => {
                if(+voucher.max_budget) {
                    const totalBudgetUsed = await this.orderRepo.getBudgetUsedOfVoucher(voucher._id)
                    if(totalBudgetUsed < +voucher.max_budget) {
                        resolve(voucher)
                    }
                    else {
                        resolve(null)
                    }
                }
                else {
                    resolve(voucher)
                }
            })
        })
        const promiseVouchers = await Promise.all(promises)
        return promiseVouchers.filter(voucher => voucher)
    }

    async checkValidVoucherShopInCart({ total_value, products }, shop_id, voucher_id) {
        const productList = products.map(product_id => {
            return { products: ObjectId(product_id) }
        })
        let match: any = {
            $or: [
                { target: 'shop' },
                {
                    target: 'product',
                    $or: [
                        ...productList,
                        { products: [] }
                    ]
                }
            ],
            _id: ObjectId(voucher_id),
            shop_id: ObjectId(shop_id),
            start_time: { '$lte': new Date() },
            end_time: { '$gte': new Date() },
            available_quantity: { '$gt': 0 },
            min_order_value: { '$lte': total_value }
        }
        let result = await this.model.aggregate([
            {
                $match: match
            }
        ])

        return result;

    }
    async getVoucherByShopWithCondition(shopId, products) {
        const productList = products.map(product_id => {
            return { products: ObjectId(product_id) }
        })
        let match: any = {
            $or: [
                { target: 'shop' },
                {
                    target: 'product',
                    $or: [
                        ...productList,
                        { products: [] }
                    ]
                }
            ],
            shop_id: ObjectId(shopId),
            start_time: { '$lte': new Date() },
            end_time: { '$gte': new Date() },
            deleted_at: null,
            available_quantity: { '$gt': 0 },
        }
        const vouchers = await this.model.find(match);
        // Continue checking max budget condition
        const voucherNotOutOfMaxBudget:Array<any> = await this.getValidVoucherByMaxBudget(vouchers)
        return voucherNotOutOfMaxBudget;
    }

    async checkShopVoucher(vouchers, ObjectCondition) {
        try {
            let listPromise: any = []
            listPromise = vouchers.map(async voucher => {
                return new Promise(async (resolve, reject) => {
                    let oneVoucher = await this.model.findById(voucher.voucher_id)
                    const result = await this.checkValidVoucherShopInCart(ObjectCondition, voucher.shop_id, voucher.voucher_id);
                    if (!result.length) {
                        return reject(new Error("Mã giảm giá của cửa hàng không hợp lệ"));
                    }
                    oneVoucher = cloneObj(oneVoucher)
                    oneVoucher.shipping_method_id = voucher.shipping_method_id;
                    oneVoucher.shipping_fee = voucher.shipping_fee;
                    resolve(oneVoucher);
                });
            });
            return Promise.all(listPromise)
                .then(data => {
                    return data;
                })
        } catch (error) {
            throw error;
        }
    }

    async checkShopVoucherCreateOrder(vouchers, ObjectCondition) {
        try {
            let listPromise: any = []
            listPromise = vouchers.map(async voucher => {
                return new Promise(async (resolve, reject) => {
                    const result = await this.checkValidVoucherShopInCart(ObjectCondition, voucher.shop_id, voucher.voucher_id);
                    if (!result.length) {
                        return reject(new Error("Mã giảm giá của cửa hàng không hợp lệ"));
                    }
                    resolve(result);
                });
            });
            return Promise.all(listPromise)
                .then(data => {
                    return data;
                })
        } catch (error) {
            throw error;
        }
    }


    async getVoucherByBanner(voucher_ids, page, limit) {
        voucher_ids = voucher_ids.map(_id => ObjectId(_id))
        const freeShippingQuery = this.model.aggregate([
            {
                $match: {
                    _id: { $in: voucher_ids },
                    classify: "free_shipping",
                }
            },
            { $sort: { "name": 1 } }
        ])

        const discountShippingQuery = this.model.aggregate([
            {
                $match: {
                    _id: { $in: voucher_ids },
                    classify: "discount",
                }
            },
            { $sort: { "name": 1 } }
        ])

        const freeShipping = await this.model
            .aggregatePaginateCustom(freeShippingQuery, { page, limit });

        const discount = await this.model
            .aggregatePaginateCustom(discountShippingQuery, { page, limit });

        const paginateShipping = {
            limit: freeShipping.limit,
            total_page: freeShipping.totalPages,
            page: freeShipping.page,
            total_record: freeShipping.totalDocs
        }

        const paginateDiscount = {
            limit: discount.limit,
            total_page: discount.totalPages,
            page: discount.page,
            total_record: discount.totalDocs
        }
        return [
            {
                classify: "free_shipping",
                items: freeShipping.docs,
                paginate: paginateShipping
            },
            {
                classify: "discount",
                items: discount.docs,
                paginate: paginateDiscount
            }
        ]
    }

    async getVoucherByBannerV2(voucher_ids) {
        voucher_ids = voucher_ids.map(_id => ObjectId(_id))

        const vouchers = await this.model.aggregate([
            {
                $match: {
                    _id: { $in: voucher_ids },
                    is_public: true
                }
            },
            { $sort: { "name": 1 } }
        ])

        return vouchers
    }

    async getShopVoucherInProductDetail(product_id, shop_id) {

        let match: any = {
            $and: [
                {
                    $or: [
                        { target: 'shop' },
                        {
                            target: 'product',
                            $or: [
                                { products: ObjectId(product_id) },
                                { products: [] }
                            ]
                        }
                    ]
                },
                {
                    $or: [
                        { deleted_at: { $exists: false } },
                        { deleted_at: null }
                    ]
                }
            ],
            shop_id: ObjectId(shop_id),
            start_time: { '$lte': new Date() },
            end_time: { '$gte': new Date() },
            available_quantity: { '$gt': 0 },
            display_mode: 'all'
        }
        const result = await this.model.aggregate([
            {
                $match: match
            }
        ])
        return result;
    }

    async getVoucherByTargetIntroduce(target) {
        let match: any = {
            target: target,
            end_time: { '$gte': new Date() },
        }
        const result = await this.model.aggregate([
            {
                $match: match
            }
        ])
        return result;
    }

    async findVoucherSystemOfUser(userID) {
        let results = await this.model.aggregate([
            {
                $match: {
                    start_time: { $lte: new Date() },
                    end_time: { $gte: new Date() },
                    available_quantity: { $gt: 0 },
                    deleted_at: { $exists: false },
                    approve_status: "approved",
                    $or: [
                        { users: ObjectId(userID) }, 
                        { kol_user: ObjectId(userID) }, 
                        {'cash_back_ref.user_id': ObjectId(userID)},
                        { classify: 'cash_back' }
                    ]
                }
            }
        ]);
        results = cloneObj(results)

        // Filter by limit use quantity by user
        results = results.filter(voucher => {
            if(voucher.conditions && voucher.conditions["limit_per_user"] && voucher.used_by) {
                return !voucher.used_by[userID] || voucher.used_by[userID] < +voucher.conditions["limit_per_user"]
            }
            else if((voucher.target === "introduced" || voucher.target === "introduce") && voucher.used_by) {
                // Voucher cho nguoi gioi thieu & nguoi dc gioi thieu chi dung 1 lan
                return voucher.used_by[userID] ? +voucher.used_by[userID] < 1 : true
            }
            return true
        })
        return results
    }

    async getShopVouchers(shopId,date) {
        const match :any = {
            $and: [
                {shop_id: ObjectId(shopId)},
                {end_time: { $gte: date }}
            ]
        }
        const vouchers = await this.model.aggregate([
            {
                $match: match
            },
            { $sort: { "start_time": 1 } }
        ])
        return vouchers
    }

    async checkValidVoucherOnCheckout(voucherID: any, ObjectCondition: any, classify:string = null) {
        const getOrderNumberPromises = [
            timeOutOfPromise(3000, {
                result: {
                    orderNumbers: [],
                    userVoucher: null
                }
            }),
            (async () => {
                const key = `voucher_${voucherID}`
                const [data, userVoucher, voucherByUser, voucherUsedByDay] = await Promise.all([
                    (async () => {
                        return this.voucherAndOrderCache.get("voucherSystems")
                    })(),
                    (async () => {
                        return this.voucherAndOrderCache.getByKeyAndField(key, ObjectCondition.user_id.toString(), true)
                    })(),
                    (async () => {
                        return this.voucherAndOrderCache.getAllByKey(`voucher_${voucherID}`)
                    })(),
                    (async () => {
                        const historyKey = `voucherHistoryDay_${voucherID}`
                        const fieldHistory = `${ObjectCondition.user_id.toString()}_${moment().format("DD-MM-YY")}`
                        return this.voucherAndOrderCache.getByKeyAndField(historyKey, fieldHistory, true)
                    })()
                ])
                
                if(!userVoucher) {
                    this.voucherAndOrderCache.saveUserUsingVoucher(key, ObjectCondition.user_id.toString(), 0)
                }
                return {
                    result: {
                        orderNumbers: data,
                        userVoucher,
                        voucherByUser,
                        voucherUsedByDay
                    }
                }
            })()
        ]
        const result:any = await Promise.race(getOrderNumberPromises).then((data:any) => {
            return data.result
        })
        
        const orderNumberList = result.orderNumbers
        const userVoucher = +result.userVoucher
        const voucherByUser = result.voucherByUser
        const voucherUsedByDay = +result.voucherUsedByDay
        const totalUsed = voucherByUser ? Object.values(voucherByUser).reduce((sum:number, quantity:string) => {
            return sum += +quantity
        }, 0) : 0
        
        let voucher = orderNumberList.find(item => item._id === voucherID)
        if (voucher) {
            const { payment_method, shipping_method, user_id, total_value } = ObjectCondition
            const currentDate = moment()
            const startTime = moment(voucher.start_time)
            const endTime = moment(voucher.end_time)
            const isValidVoucher = 
                (!classify || voucher.classify === classify)
                && voucher.use_quantity - +totalUsed > 0 
                && (!voucher.min_order_value || +voucher.min_order_value <= +total_value )
                && currentDate.isSameOrAfter(startTime) && currentDate.isSameOrBefore(endTime)
                && (!voucher.max_order_value || voucher.max_order_value >= total_value)
                && (!voucher.conditions?.payment_method?.length || voucher.conditions.payment_method.includes(payment_method.toString()))
                && (!voucher.conditions?.shipping_method?.length || voucher.conditions.shipping_method.includes(shipping_method.toString()))
                && (!voucher.conditions?.limit_per_user || +voucher.conditions?.limit_per_user > userVoucher)
                && (!voucher.conditions?.limit_per_user_by_day || +voucher.conditions?.limit_per_user_by_day > voucherUsedByDay)

            if(!isValidVoucher) {
                return null;
            }
            return voucher
        }
        return null
    }
}
