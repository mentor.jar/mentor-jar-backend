import { ITopShopBanner } from '../interfaces/TopShopBanner';
import TopShopBanner from '../models/TopShopBanner';
import { BaseRepository } from "./BaseRepository";
import IPaginateData from './interfaces/IPaginateData';
import mongoose from 'mongoose';

const ObjectId = mongoose.Types.ObjectId;

export default class TopShopBannerRepo extends BaseRepository<ITopShopBanner> {

    private static instance: TopShopBannerRepo;

    private constructor() {
        super();
        this.model = TopShopBanner;
    }

    public static getInstance(): TopShopBannerRepo {
        if (!TopShopBannerRepo.instance) {
            TopShopBannerRepo.instance = new TopShopBannerRepo();
        }
        return TopShopBannerRepo.instance;
    }

    public async getAllBanners(limit = 10, page = 1): Promise<IPaginateData> {
        const query = this.getQuery({})

        const result = await this.model
            .aggregatePaginateCustom(query, { page, limit })

        
        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }

        const data = result.docs;
        return { data, paginate }
    }

    async getBannerById(id: string) {
        let match = { _id: ObjectId(id) }
        const query = await this.getQuery(match);
        if (query.length > 0) {
            return query[0];
        }
        return null;
    }

    public getQuery(match: any) {
        return this.model.aggregate([
            {
                $match: match
            },
            {
                $sort: { createdAt: -1 }
            },
            {
                $lookup:
                {
                    from: "shops",
                    let: {
                        shop_ids: "$shop_ids"
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        {
                                            $in: ["$_id", "$$shop_ids"]
                                        },
                                    ]
                                }
                            }
                        },
                        {
                            $lookup: {
                                from: "users",
                                let: {
                                    user_id: "$user_id"
                                },
                                pipeline: [
                                    {
                                        $match: {
                                            $expr: {
                                                $and: [
                                                    {
                                                        $eq: ["$_id", "$$user_id"]
                                                    }
                                                ]
                                            },   
                                        }
                                    }
                                ],
                                as: "user"
                            }
                        },
                        {
                            $unwind: {
                                path: "$user",
                                preserveNullAndEmptyArrays: true
                            }
                        }
                    ],
                    as: "shops"
                }
            },
        ]);
    }
}