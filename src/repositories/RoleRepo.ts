import { BaseRepository } from "./BaseRepository";
import Role from "../models/Role"
import { RoleDoc } from "../interfaces/Role";

export class RoleRepo extends BaseRepository<RoleDoc> {

    private static instance: RoleRepo;

    private constructor() {
        super();
        this.model = Role;
    }

    public static getInstance(): RoleRepo {
        if (!RoleRepo.instance) {
            RoleRepo.instance = new RoleRepo();
        }

        return RoleRepo.instance;
    }



}