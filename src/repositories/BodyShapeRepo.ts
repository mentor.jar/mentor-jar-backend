import { BaseRepository } from "./BaseRepository";
import BodyShape from "../models/BodyShape"

export class BodyShapeRepo extends BaseRepository<any> {

    private static instance: BodyShapeRepo;

    private constructor() {
        super();
        this.model = BodyShape;
    }

    public static getInstance(): BodyShapeRepo {
        if (!BodyShapeRepo.instance) {
            BodyShapeRepo.instance = new BodyShapeRepo();
        }

        return BodyShapeRepo.instance;
    }

}