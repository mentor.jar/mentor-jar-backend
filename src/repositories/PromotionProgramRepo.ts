import { BaseRepository } from './BaseRepository';
import PromotionProgram from '../models/PromotionProgram';
import { PromotionProgramDoc } from '../interfaces/PromotionProgram';
import { ProductRepo } from './ProductRepo';
import { cloneObj, ObjectId } from '../utils/helper';
import { VariantRepo } from './Variant/VariantRepo';

const productRepo = ProductRepo.getInstance();
const variantRepo = VariantRepo.getInstance();

export class PromotionProgramRepo extends BaseRepository<PromotionProgramDoc> {
    private static instance: PromotionProgramRepo;

    constructor() {
        super();
        this.model = PromotionProgram;
    }

    public static getInstance(): PromotionProgramRepo {
        if (!PromotionProgramRepo.instance) {
            PromotionProgramRepo.instance = new PromotionProgramRepo();
        }

        return PromotionProgramRepo.instance;
    }

    async updateProductList(product_ids, promotion) {
        // create promotion
        const promises = product_ids.map(id => {
            return new Promise(async (resolve, reject) => {
                try {
                    const item = await productRepo.findProductListIncludingVariant([id], 1, 1)
                    if (item && item.data && item.data.length) {
                        let product = cloneObj(item.data[0])
                        let newPrice = product.sale_price;
                        if (!product.limit_sale_price_order || product.sale_price_order_available > 0) {
                            // Only update sale_price at the first time run promotion and sale_price_order_available greater than 0
                            newPrice = ((100 - promotion.discount) * product.before_sale_price / 100).toFixed(0)
                            // edit variant info
                            const variantPromises = product.variants.map(variant => {
                                return new Promise(async (resolve, reject) => {
                                    try {
                                        variant.sale_price = ((100 - promotion.discount) * variant.before_sale_price / 100).toFixed(0)
                                        variant = await variantRepo.update(variant._id, variant)
                                        resolve(variant)
                                    } catch (error) {
                                        reject(error.message)
                                    }
                                })
                            })
                            // update variant
                            await Promise.all(variantPromises)
                            delete product.variants
                        }
                        if (!product.limit_sale_price_order) {
                            product.sale_price_order_available = promotion.limit_quantity
                        }
                        if (!product.limit_sale_price_order) {
                            product.limit_sale_price_order = promotion.limit_quantity
                        }
                        
                        if (+newPrice !== +product.sale_price) {
                            product.sale_price = newPrice;
                            productRepo.update(product._id, product)
                        }
                        resolve(product._id)
                    }
                    else {
                        reject(`Không tìm thấy sản phẩm để cập nhật giá khuyến mãi`)
                    }
                } catch (error) {
                    reject(error.message)
                }
            })
        })

        // Update product
        let productIdUpdated: any = []
        productIdUpdated = await Promise.all(promises).then(ids => {
            return ids
        }).catch(error => {
            console.log("error: ", error);
            return []
        })
        return productIdUpdated
    }

    async backProductToOldPrice(product_ids) {
        const promises = product_ids.map(id => {
            return new Promise(async (resolve, reject) => {
                try {
                    const item = await productRepo.findProductToBackPrice(id)
                    if(item) {
                        let product = cloneObj(item[0])
                        let newProductData = {
                            sale_price: product.before_sale_price,
                            limit_sale_price_order: 0,
                            sale_price_order_available: 0
                        }
                        let variantPromises = []

                        // edit variant info
                        variantPromises = product.variants.map(variant => {
                            return new Promise(async (resolve, reject) => {
                                try {
                                    variant.sale_price = variant.before_sale_price
                                    variant = await variantRepo.update(variant._id, variant)
                                    resolve(variant)
                                } catch (error) {
                                    reject(error.message)
                                }
                            })
                        })
                        // update variant
                        await Promise.all(variantPromises)
                        delete product.variants
                        product = await productRepo.update(product._id, newProductData)
                        resolve(product._id)
                    }
                    else {
                        reject(`Không tìm thấy sản phẩm để xoá giá khuyến mãi`)
                    }
                } catch (error) {
                    reject(error.message)
                }
            })
        })

        // Update product
        let productIdUpdated: any = []
        productIdUpdated = await Promise.all(promises).then(ids => {
            return ids
        }).catch(error => {
            console.log("error: ", error);
            return []
        })
        return productIdUpdated
    }

    async checkUniqueProduct(product_ids: Array<any>, id: any = null) {
        const current = new Date()
        product_ids = product_ids.map(id => ObjectId(id))
        let match = {}
        if (id) {
            match = {
                $or: [
                    {
                        $and: [
                            { start_time: { $lte: current } },
                            { end_time: { $gte: current } }
                        ]
                    },
                    { start_time: { $gt: current } },
                ],
                products: { $in: product_ids },
                _id: { $ne: ObjectId(id) }
            }
        }
        else {
            match = {
                $or: [
                    {
                        $and: [
                            { start_time: { $lte: current } },
                            { end_time: { $gte: current } }
                        ]
                    },
                    { start_time: { $gt: current } }
                ],
                products: { $in: product_ids }
            }
        }
        const data = await this.model.findOne(match)
        return data ? true : false;
    }

    async getPromotionByProduct(productID) {
        const current = new Date()
        let match = {}
        match = {
            $or: [
                {
                    $and: [
                        { start_time: { $lte: current } },
                        { end_time: { $gte: current } }
                    ]
                }
            ],
            products: ObjectId(productID)
        }
        const data = await this.model.findOne(match)
        return data
    }

    async detailPromotionProgram(id) {
        return this.model.aggregate([
            {
                $match: {
                    _id: ObjectId(id),
                    is_valid: true,
                }
            },
            PromotionProgramQueryHelper.relations.products
        ])
    }

    async getPromotionsWithFilter(shopId, limit = 20, page = 1, filterType = 'ALL') {
        // Only get promotin after the first day of previous month
        const date = new Date()
        date.setDate(1);
        date.setMonth(date.getMonth() - 1);

        const match: any = {
            shop_id: ObjectId(shopId),
            is_valid: true,
            $or: [
                { deleted_at: { $exists: false } },
                { deleted_at: null }
            ],
            end_time: { $gt: date }
        }
        switch (filterType) {
            case 'RUNNING':
                match.start_time = { '$lte': new Date() }
                match.end_time = { '$gte': new Date() }
                break;
            case 'EXPIRED':
                match.end_time = { '$lte': new Date() }
                break;
            case 'INCOMING':
                match.start_time = { '$gte': new Date() }
                break;
            default:
                break;
        }
        const query = this.model.aggregate([
            {
                $match: match
            },
            {
                $sort: { updated_at: -1 }
            },
            PromotionProgramQueryHelper.relations.products,
            PromotionProgramQueryHelper.relations.shop,
            {
                $unwind: {
                    path: "$shop",
                    preserveNullAndEmptyArrays: true
                }
            },
        ])
        const result = await this.model
            .aggregatePaginateCustom(query, { page, limit })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        const data = result.docs;
        return { data, paginate }
    }

    async getExpirePromotion() {
        const match: any = {
            $or: [
                { is_valid: false },
                {
                    $and: [
                        { is_valid: true },
                        { end_time: { '$lte': new Date() } }
                    ]
                }
            ],
            is_reset: false,
        }
        return await this.model.aggregate([
            {
                $match: match
            }
        ])
    }

    async getStartPromotion() {
        const match: any = {
            $and: [
                { is_valid: true },
                { is_reset: false },
                { start_time: { '$lte': new Date() } },
                { end_time: { '$gte': new Date() } }
            ]
        }
        return await this.model.aggregate([
            {
                $match: match
            }
        ])
    }

}

export class PromotionProgramQueryHelper {

    public static relations = {
        products: {
            $lookup: {
                from: "products",
                let: {
                    product_ids: "$products",
                },
                pipeline: [
                    {
                        $match: {
                            $and: [
                                // Không kiểm tra các điều kiện allow_to_sell, is_approved, deleted_at ở đây
                                {
                                    '$expr': {
                                        '$in': ['$_id', '$$product_ids']
                                    }
                                }
                            ]
                        }
                    },
                    {
                        $lookup: {
                            from: "variants",
                            let: {
                                product_id: "$_id"
                            },
                            pipeline: [
                                {
                                    $match: {
                                        $and: [
                                            {
                                                '$expr': {
                                                    $and: [
                                                        {
                                                            $eq: ["$product_id", "$$product_id"]
                                                        }
                                                    ]
                                                },
                                            }
                                        ]
                                    }
                                }
                            ],
                            as: "variants"
                        }
                    }
                ],
                as: "products"
            }
        },
        shop: {
            $lookup: {
                from: "shops",
                let: {
                    shop_id: "$shop_id",
                },
                pipeline: [
                    {
                        $match: {
                            $and: [
                                {
                                    '$expr': {
                                        '$eq': ['$_id', '$$shop_id']
                                    }
                                }
                            ]
                        }
                    },

                ],
                as: "shop"
            }
        }
    }
}