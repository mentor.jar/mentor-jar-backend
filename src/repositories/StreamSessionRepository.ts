import { BaseRepository } from './BaseRepository';
import StreamSession from '../models/Stream_Session';
import { IStreamRepo } from './interfaces/IStreamRepo';
import { StreamSessionDoc } from '../interfaces/Stream_Session';
import { cloneObj, ObjectId, randomInt } from '../utils/helper';
import { StreamSessionDTO } from '../DTO/StreamSessionDTO';
import { NotFoundError } from '../base/customError';
import { moveFileStreamToFolderStreamUser } from '../controllers/serviceHandles/streams';
import { ProductRepo } from './ProductRepo';
import { Limit } from '../controllers/serviceHandles/shop/definition';
import { RoomChatRepo } from './RoomChatRepo';

export class StreamSessionRepository extends BaseRepository<StreamSessionDoc>
    implements IStreamRepo {
    private static instance: StreamSessionRepository;

    private constructor() {
        super();
        this.model = StreamSession;
    }

    public static getInstance(): StreamSessionRepository {
        if (!StreamSessionRepository.instance) {
            StreamSessionRepository.instance = new StreamSessionRepository();
        }

        return StreamSessionRepository.instance;
    }

    async getStreamSessionLiveByUser(userId) {
        const streamSession = await this.model.findOne({
            user_id: userId,
            active: true,
        });
        return streamSession ? true : false;
    }

    async getStreamSessionLiveByShop(shopId) {
        const streamSession = await this.model.findOne({
            shop_id: shopId,
            active: true,
        });
        return streamSession ? true : false;
    }

    async getStreamSession(id: any) {
        let filter: any = { _id: ObjectId(id) };
        const query = await this.getQuery(filter);
        // let promiseResult = await this.getExpandValues(filter, relations, { createdAt: -1 });
        let result = cloneObj(query);
        result = new StreamSessionDTO(result[0]);
        result = result.toSimpleJSON();
        return result;
    }

    async getStreamSessionByRoomChat(room_chat_id: any) {
        let filter: any = { room_chat_id: ObjectId(room_chat_id) };
        const query = await this.getQuery(filter);
        // let promiseResult = await this.getExpandValues(filter, relations, { createdAt: -1 });
        let result = cloneObj(query);
        if (!result.length) return null;
        result = new StreamSessionDTO(result[0]);
        result = result.toSimpleJSON();
        return result;
    }

    async getListScheduledStream(paginate: object | null) {
        let filter: any = {
            $and: [
                { time_will_start: { $gt: new Date() } },
                { time_start: { $eq: null } },
                { time_end: { $eq: null } },
            ],
        };
        const query = this.getQuery(filter);
        let result = await this.model.aggregatePaginateCustom(query, paginate);
        let paginateRes = {};
        if (paginate) {
            paginateRes = {
                limit: result.limit,
                total_page: result.totalPages,
                page: result.page,
                total_record: result.totalDocs,
            };
            result = result.docs;
        }
        result = result.map(e => {
            e = new StreamSessionDTO(e);
            let getSession = e.toSimpleJSON();
            return getSession;
        });
        if (paginate) return { data: result, paginate: paginateRes };

        return result;
    }

    async getListStreamSessionByShop(shop_id: string, paginate: object | null) {
        let filter: any = {};
        if (shop_id) {
            filter = { shop_id: ObjectId(shop_id) };
        }
        // let promiseResult = this.getExpandValues(filter, relations, { createdAt: -1 });
        // let result = paginate
        //     ? await this.model.aggregatePaginateCustom(promiseResult, paginate)
        //     : await promiseResult;
        // result = cloneObj(result);
        const query = this.getQuery(filter);
        let result = await this.model.aggregatePaginateCustom(query, paginate);
        let paginateRes = {};
        if (paginate) {
            paginateRes = {
                limit: result.limit,
                total_page: result.totalPages,
                page: result.page,
                total_record: result.totalDocs,
            };
            result = result.docs;
        }
        result = result.map(e => {
            e = new StreamSessionDTO(e);
            let getSession = e.toSimpleJSON();
            return getSession;
        });
        if (paginate) return { data: result, paginate: paginateRes };

        return result;
    }

    async getListStreamSessionByShopStatistics(
        shop_id: string,
        sort_direction = null,
        start_time = null,
        end_time = null
    ) {
        let filter: any = {
            shop_id: ObjectId(shop_id),
            active: false,
            time_end: { $ne: null },
        };

        if (start_time) {
            filter.time_start = { $gte: start_time };
        }

        if (end_time) {
            filter.time_end = { $lte: end_time };
        }

        const query = await this.model.aggregate([
            {
                $match: filter,
            },
            {
                $sort: { created_at: sort_direction || -1 },
            },
            {
                $limit: Limit.Five,
            },
        ]);
        return query;
    }

    async getListStreamSessionByViewer(paginate: object | null) {
        //let threeDaysBefore = new Date(Date.now() - 3 * 24 * 60 * 60 * 1000);
        let filter: any = {
            //created_at: { $gte: threeDaysBefore },
            $or: [{ time_end: { $ne: null } }, { active: true }],
        };
        const query = this.getQuery(filter);
        let result = await this.model.aggregatePaginateCustom(query, paginate);
        let paginateRes = {};
        if (paginate) {
            paginateRes = {
                limit: result.limit,
                total_page: result.totalPages,
                page: result.page,
                total_record: result.totalDocs,
            };
            result = result.docs;
        }
        result = result.map(e => {
            e = new StreamSessionDTO(e);
            let getSession = e.toSimpleJSON();
            return getSession;
        });
        if (paginate) return { data: result, paginate: paginateRes };
        return result;
    }

    async getRecentStreamSessions() {
        return await this.model.aggregate([
            {
                $match: { time_end: { $ne: null } },
            },
            {
                $sort: { created_at: -1 }
            },
            {
                $limit: 10
            }
        ])
    }

    async startStreamSession(id: string, user_id) {
        let update = {
            active: true,
            time_start: new Date(),
        };
        let filter = {
            _id: ObjectId(id),
            active: false,
            time_start: null,
            time_end: null,
        };

        const resultUpdate = await this.findOneAndUpdate(filter, update);
        if (!resultUpdate)
            throw new NotFoundError("Stream không tồn tại stream hoặc stream đã được bắt đầu")
        const streamSession = await this.getStreamSession(id);
        return streamSession;
    }

    async stopStreamSession(id: string) {
        let update = {
            active: false,
            time_end: new Date(),
        };
        let filter = {
            _id: ObjectId(id),
            active: true,
            time_start: { $ne: null },
            time_end: null,
        };
        const resultUpdate = await this.findOneAndUpdate(filter, update);
        if (!resultUpdate)
            throw new NotFoundError(
                'Stream không tồn tại hoặc đã được kết thúc'
            );
        let streamSession = await this.getStreamSession(id);
        streamSession = cloneObj(streamSession);
        let user_id = streamSession?.stream?.user_id;
        if (user_id) {
            let archive_links = streamSession.archive_links;
            const archive_link = await moveFileStreamToFolderStreamUser(
                user_id,
                streamSession._id
            );
            archive_links = archive_link;
            if (archive_link) {
                await this.findOneAndUpdate(
                    { _id: ObjectId(id) },
                    { archive_links }
                );
                //streamSession.archive_link = archive_link;
                streamSession.archive_links = archive_links;
            }
        }
        return streamSession;
    }

    async updateView(id: string, view) {
        let update: any = {
            view,
        };
        let filter = {
            _id: ObjectId(id),
        };
        let stream_session = await this.getStreamSession(id);
        let room_chat = await RoomChatRepo.getInstance().findById(stream_session.room_chat_id.toString());
        let view_max = room_chat ? room_chat.users.length : 1;
        update.view_max = view_max;
        await this.findOneAndUpdate(filter, update);
    }

    async updateHeart(id: string, heart) {
        let update = {
            heart,
        };
        let filter = {
            _id: ObjectId(id),
        };
        await this.findOneAndUpdate(filter, update);
    }

    async pinMessage(id: string, message) {
        let update: any = {
            pin_message: message,
        };
        let filter = {
            _id: ObjectId(id),
        };
        await this.findOneAndUpdate(filter, update);
    }

    async updateProduct(id: string, list_id_product) {
        let streamSession = await this.getStreamSession(id);
        // filter list product of shop
        let products = await ProductRepo.getInstance().findWithShopAndIds(
            streamSession.shop_id,
            list_id_product
        );
        products = cloneObj(products);
        products = products.map(product => {
            return product._id;
        });
        let update = {
            list_id_product: products,
        };
        let filter = {
            _id: ObjectId(id),
        };
        await this.findOneAndUpdate(filter, update);
        return await this.getStreamSession(id);
    }

    public getQuery(match: any) {
        return this.model.aggregate([
            {
                $match: match,
            },

            {
                $sort: { active: -1, created_at: -1 },
            },
            {
                $lookup: {
                    from: 'products',
                    let: {
                        product_ids: '$list_id_product',
                        deleted_at: '$deleted_at',
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        $expr: {
                                            $and: [
                                                {
                                                    $in: [
                                                        '$_id',
                                                        '$$product_ids',
                                                    ],
                                                },
                                            ],
                                        },
                                    },
                                    {
                                        $or: [
                                            { deleted_at: { $exists: false } },
                                            { deleted_at: null },
                                        ],
                                    },
                                ],
                            },
                        },
                        {
                            $lookup: {
                                from: 'variants',
                                let: {
                                    product_id: '$_id',
                                },
                                pipeline: [
                                    {
                                        $match: {
                                            $and: [
                                                {
                                                    $expr: {
                                                        $and: [
                                                            {
                                                                $eq: [
                                                                    '$product_id',
                                                                    '$$product_id',
                                                                ],
                                                            },
                                                        ],
                                                    },
                                                },
                                            ],
                                        },
                                    },
                                ],
                                as: 'variants',
                            },
                        },
                        {
                            $lookup: {
                                from: "optiontypes",
                                let: {
                                    product_id: "$_id"
                                },
                                pipeline: [
                                    {
                                        $match: {
                                            $and: [
                                                {
                                                    '$expr': {
                                                        $and: [
                                                            {
                                                                $eq: ["$product_id", "$$product_id"]
                                                            }
                                                        ]
                                                    },
                                                }
                                            ]
                                        }
                                    },
                                    {
                                        $lookup: {
                                            from: "optionvalues",
                                            let: {
                                                option_type_id: "$_id"
                                            },
                                            pipeline: [
                                                {
                                                    $match: {
                                                        $and: [
                                                            {
                                                                '$expr': {
                                                                    $and: [
                                                                        {
                                                                            $eq: ["$option_type_id", "$$option_type_id"]
                                                                        }
                                                                    ]
                                                                },
                                                            }
                                                        ]
                                                    },

                                                },
                                                {
                                                    $project: {
                                                        _id: 1,
                                                        name: 1,
                                                        image: 1
                                                    }
                                                }


                                            ],
                                            as: "option_values"
                                        }
                                    },
                                    {
                                        $project: {
                                            _id: 1,
                                            name: 1,
                                            option_values: 1
                                        }
                                    }
                                ],
                                as: "option_types"
                            }
                        }
                    ],
                    as: 'products',
                },
            },
            {
                $lookup: {
                    from: 'streams',
                    let: {
                        stream_id: '$stream_id',
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        {
                                            $eq: ['$_id', '$$stream_id'],
                                        },
                                    ],
                                },
                            },
                        },
                    ],
                    as: 'stream',
                },
            },
            {
                $lookup: {
                    from: 'shops',
                    let: {
                        id: '$shop_id',
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        {
                                            $eq: ['$_id', '$$id'],
                                        },
                                    ],
                                },
                            },
                        },
                    ],
                    as: 'shopData',
                },
            },
            {
                $lookup: {
                    from: 'users',
                    let: {
                        id: '$user_id',
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        {
                                            $eq: ['$_id', '$$id'],
                                        },
                                    ],
                                },
                            },
                        },
                        {
                            $lookup: {
                                from: 'galleryimages',
                                let: {
                                    user_id: '$_id',
                                },
                                pipeline: [
                                    {
                                        $match: {
                                            $and: [
                                                {
                                                    $expr: {
                                                        $and: [
                                                            {
                                                                $eq: [
                                                                    '$galleryImageAbleId',
                                                                    '$$user_id',
                                                                ],
                                                            },
                                                            {
                                                                $eq: [
                                                                    '$galleryImageAbleType',
                                                                    '1',
                                                                ],
                                                            },
                                                        ],
                                                    },
                                                },
                                            ],
                                        },
                                    },
                                ],
                                as: 'gallery_image',
                            },
                        },
                        {
                            $unwind: {
                                path: '$gallery_image',
                                preserveNullAndEmptyArrays: true,
                            },
                        },
                    ],
                    as: 'user',
                },
            },
            {
                $addFields: {
                    shop: {
                        $arrayElemAt: ['$shopData', 0],
                    },
                    user: {
                        $arrayElemAt: ['$user', 0],
                    },
                    stream: {
                        $arrayElemAt: ['$stream', 0],
                    },
                },
            },
            {
                $project: {
                    shopData: 0,
                },
            },
        ]);
    }
}
