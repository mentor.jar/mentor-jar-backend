import { ReferralUsageDoc } from '../interfaces/ReferralUsage';
import ReferralUsage from '../models/Referral_Usage';
import { BaseRepository } from "./BaseRepository";

export class ReferralUsageRepo extends BaseRepository<ReferralUsageDoc> {

    private static instance: ReferralUsageRepo;

    constructor() {
        super();
        this.model = ReferralUsage;
    }

    public static getInstance(): ReferralUsageRepo {
        if (!ReferralUsageRepo.instance) {
            ReferralUsageRepo.instance = new ReferralUsageRepo();
        }

        return ReferralUsageRepo.instance;
    }

}
