import { BaseRepository } from "./BaseRepository";
import EmailTemplate from "../models/EmailTemplate"
import { EmailTemplateDoc } from "../interfaces/EmailTemplate";

export class EmailTemplateRepo extends BaseRepository<EmailTemplateDoc> {

    private static instance: EmailTemplateRepo;

    private constructor() {
        super();
        this.model = EmailTemplate;
    }

    public static getInstance(): EmailTemplateRepo {
        if (!EmailTemplateRepo.instance) {
            EmailTemplateRepo.instance = new EmailTemplateRepo();
        }

        return EmailTemplateRepo.instance;
    }

    async getListEmailTemplate(page = 1, limit = 20) {
        const query = this.model.aggregate([
            {
                $match: {
                    deleted_at: {$eq: null}
                }
            },
            {
                $sort: {created_at: -1}
            }
        ])

        const result = await this.model
            .aggregatePaginateCustom(query, { page, limit })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        const data = result.docs;
        return { data, paginate }
    }
}