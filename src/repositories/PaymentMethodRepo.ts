import { BaseRepository } from "./BaseRepository";
import PaymentMethod from "../models/Payment_Method";
import { PaymentMethodDoc } from "../interfaces/Payment_Method";
import { ObjectId } from "../utils/helper";
export class PaymentMethodRepo extends BaseRepository<PaymentMethodDoc>  {

    private static instance: PaymentMethodRepo;

    constructor() {
        super();
        this.model = PaymentMethod;
    }

    public static getInstance(): PaymentMethodRepo {
        if (!PaymentMethodRepo.instance) {
            PaymentMethodRepo.instance = new PaymentMethodRepo();
        }

        return PaymentMethodRepo.instance;
    }

    async getPaymentMethodByIds(ids) {
        if (!ids.length) return []

        ids = ids.map(id => ObjectId(id));
        const match = {
            _id: { $in: ids }
        }

        const result = await this.model.aggregate([
            {
                $match: match
            }
        ])
        return result
    }

    async getAllPaymentMethod() {
        const paymentMethods = await this.model.find({
            is_active: true
        });
        return paymentMethods;
    }

    async getDefaultPaymentMethod() {
        const paymentMethod = await this.model.findOne({ name: 'CASH' });
        return paymentMethod;
    }

    checkPaymentMethod = async (payment_id) => {

        let result = await this.getAllPaymentMethod();

        result = result.filter((item) => {
            return item._id.toString() ===  payment_id.toString();
        })

        return result[0];
    }

}


