
import mongoose from 'mongoose';
import NotifyCampaign from '../models/Notify_Campaign';
import { NotifyInterface } from '../services/notification';
import { BaseRepository } from "./BaseRepository";
import { IBannerRepo } from './interfaces/IBannerRepo';
import NotificationRepo from './NotificationRepo';
import { cloneObj } from '../utils/helper';
import { ENotifyCampaign } from '../interfaces/NotifyCampaign';
const ObjectId = mongoose.Types.ObjectId;

export default class NotifyCampaignRepo extends BaseRepository<ENotifyCampaign> implements IBannerRepo, NotifyInterface.CampaignServiceInt {

    private static instance: NotifyCampaignRepo;

    private constructor() {
        super();
        this.model = NotifyCampaign;
    }

    public static getInstance(): NotifyCampaignRepo {
        if (!NotifyCampaignRepo.instance) {
            NotifyCampaignRepo.instance = new NotifyCampaignRepo();
        }
        return NotifyCampaignRepo.instance;
    }

    async createNotifyCampaign(request: NotifyInterface.CreateNotifyCampaignRequest, isCreate = false) {
        const model = await this.model.create(request);
        if (isCreate == true) {
            await this.saveNotificationToUser(request.receivers, {
                type: request.type,
                contentType: request.content,
                creatorId: request.createdBy,
                title: request.title,
                content: request.content,
                data: request.data,
                image: request.image,
                campaignId: model._id.toString()
            });
        }
        return model;
    };

    async saveNotificationToUser(receivers, request) {
        if (receivers && receivers.length > 0) {
            const notificationRepo = NotificationRepo.getInstance();
            return Promise.all(receivers.map(receiver => {
                notificationRepo.saveNotification({
                    ...request,
                    receiverId: receiver
                });
            }))
        }
    }

    public async getNotifyCampaigns(request) {

        const { page, limit, userId, status, type } = request;

        const aggregate = this.model.aggregate();
        const result = await this.model
            .aggregatePaginateCustom(aggregate, { page, limit })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }

        const data = result.docs;

        return { data, paginate }
    };

    public async getNotifyCampaignDetail(id) {

        let notify = await this.model.aggregate([
            {
                $match: { _id: ObjectId(id) }
            },
            {
                $sort: { created_at: -1 }
            },

            {
                $lookup:
                {
                    from: "users",
                    let: {
                        receiver_ids: "$receivers",
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        '$expr': {
                                            $and: [
                                                {
                                                    '$in': ['$_id', '$$receiver_ids']
                                                }
                                            ]
                                        },
                                    },
                                    {
                                        $or: [
                                            { deleted_at: { $exists: false } },
                                            { deleted_at: null }
                                        ]
                                    }
                                ]
                            }
                        },

                        {
                            $lookup: {
                                from: "galleryimages",
                                let: {
                                    user_id: "$_id"
                                },
                                pipeline: [
                                    {
                                        $match: {
                                            $and: [
                                                {
                                                    '$expr': {
                                                        $and: [
                                                            {
                                                                $eq: ["$galleryImageAbleId", "$$user_id"]
                                                            },
                                                            {
                                                                $eq: ["$galleryImageAbleType", "1"]
                                                            }
                                                        ]
                                                    },
                                                }
                                            ]
                                        }
                                    }
                                ],
                                as: "gallery_image"
                            }
                        },
                        {
                            $unwind: {
                                path: "$gallery_image",
                                preserveNullAndEmptyArrays: true
                            }

                        }
                    ],
                    as: "receivers"
                }
            },
        ]);
        notify = cloneObj(notify);
        if (notify) {
            return notify[0];
        } else {
            return null;
        }
    };

    removeNotifyCampaign() {

    };

}