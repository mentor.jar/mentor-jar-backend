import { EBanner } from '../interfaces/Banner';
import Banner from '../models/Banner';
import { BaseRepository } from "./BaseRepository";
import { IBannerRepo } from './interfaces/IBannerRepo';
import IPaginateData from './interfaces/IPaginateData';
import mongoose from 'mongoose';
const ObjectId = mongoose.Types.ObjectId;

export default class BannerRepo extends BaseRepository<EBanner> implements IBannerRepo {

    private static instance: BannerRepo;

    private constructor() {
        super();
        this.model = Banner;
    }

    public static getInstance(): BannerRepo {
        if (!BannerRepo.instance) {
            BannerRepo.instance = new BannerRepo();
        }
        return BannerRepo.instance;
    }

    public findHomeBanner = async (filter, sort, limit) => {
        return this.model.find(filter).sort(sort).limit(limit)
    }

    public async getBannerShopExplore(shopId): Promise<any> {
        const banners = this.model.find({ shop_id: shopId, end_time: { $gte: Date.now() } }).limit(5).sort({ 'updated_at': -1 });
        return banners;
    }

    public async getBannerByShopId(shopId, limit = 10, page = 1, filter_type): Promise<IPaginateData> {

        const match: any = {
            shop_id: ObjectId(shopId)
        }
        if (filter_type === 'RUNNING') {
            match.start_time = { '$lte': new Date() }
            match.end_time = { '$gte': new Date() }
        } else if (filter_type === 'EXPIRED') {
            match.end_time = { '$lte': new Date() }
        }

        const query = this.getQuery(match);

        const result = await this.model
            .aggregatePaginateCustom(query, { page, limit })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        const data = result.docs;
        return { data, paginate }
    }

    public getBanner(shopId, bannerId) {
        return this.findOne({
            _id: bannerId,
            shop_id: shopId
        })
    }

    public async getAllBanners(limit = 10, page = 1, filter_type, shop_id): Promise<IPaginateData> {

        const match: any = {
            shop_id: { $ne: null }
        }
        if (filter_type === 'RUNNING') {
            match.start_time = { '$lte': new Date() }
            match.end_time = { '$gte': new Date() }
        } else if (filter_type === 'EXPIRED') {
            match.end_time = { '$lte': new Date() }
        }
        if (shop_id) {
            match.shop_id = ObjectId(shop_id);
        }

        const query = this.getQuery(match);
        const result = await this.model
            .aggregatePaginateCustom(query, { page, limit })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        const data = result.docs;
        return { data, paginate }
    }

    public async getAllBannerSystem(limit = 10, page = 1, filter_type, search = null): Promise<IPaginateData> {
        const match: any = {
            shop_id: null
        }
        if (filter_type === 'RUNNING') {
            match.start_time = { '$lte': new Date() }
            match.end_time = { '$gte': new Date() }
        } else if (filter_type === 'EXPIRED') {
            match.end_time = { '$lte': new Date() }
        }

        if (search) {
            match.name = { '$regex': `${search}`, '$options': 'i' };
        } 

        const query = this.getQuery(match);
        const result = await this.model
            .aggregatePaginateCustom(query, { page, limit })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        const data = result.docs;
        return { data, paginate }
    }

    async getBannerById(id: string) {
        let match = { _id: ObjectId(id) }
        const query = await this.getQuery(match);
        if (query.length > 0) {
            return query[0];
        }
        return null;
    }

    public getQuery(match: any) {
        return this.model.aggregate([
            {
                $match: match
            },
            {
                $sort: { created_at: -1 }
            },
            {
                $lookup:
                {
                    from: "products",
                    let: {
                        product_ids: "$products",
                        deleted_at: "$deleted_at",
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        '$expr': {
                                            $and: [
                                                {
                                                    '$in': ['$_id', '$$product_ids']
                                                }
                                            ]
                                        },
                                    },
                                    {
                                        $or: [
                                            { deleted_at: { $exists: false } },
                                            { deleted_at: null }
                                        ]
                                    }
                                ]
                            }
                        },
                        {
                            $lookup: {
                                from: "variants",
                                let: {
                                    product_id: "$_id"
                                },
                                pipeline: [
                                    {
                                        $match: {
                                            $and: [
                                                {
                                                    '$expr': {
                                                        $and: [
                                                            {
                                                                $eq: ["$product_id", "$$product_id"]
                                                            }
                                                        ]
                                                    },
                                                }
                                            ]
                                        }
                                    }
                                ],
                                as: "variants"
                            }
                        }
                    ],
                    as: "products"
                }
            },
            {
                $lookup:
                {
                    from: "shops",
                    let: {
                        id: "$shop_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        {
                                            $eq: ["$_id", "$$id"]
                                        },
                                    ]
                                }
                            }
                        },
                        {
                            $lookup: {
                                from: "users",
                                let: {
                                    user_id: "$user_id"
                                },
                                pipeline: [
                                    {
                                        $match: {
                                            $and: [
                                                {
                                                    '$expr': {
                                                        $and: [
                                                            {
                                                                $eq: ["$_id", "$$user_id"]
                                                            }
                                                        ]
                                                    },
                                                }
                                            ]
                                        }
                                    }
                                ],
                                as: "user"
                            }
                        },
                        {
                            $unwind: {
                                path: "$user",
                                preserveNullAndEmptyArrays: true
                            }

                        }
                    ],
                    as: "shopData"
                }
            },
            {
                $lookup:
                {
                    from: "vouchers",
                    let: {
                        voucher_ids: "$vouchers"
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        '$expr': {
                                            $and: [
                                                {
                                                    '$in': ['$_id', { $ifNull: ['$$voucher_ids', []] }]
                                                }
                                            ]
                                        },
                                    },
                                ]
                            }
                        }
                    ],
                    as: "vouchers"
                }
            },
            {
                "$addFields": {
                    "shop_id": {
                        "$arrayElemAt": ["$shopData", 0]
                    }
                }
            },
            {
                "$project": {
                    "shopData": 0
                }
            }
        ]);
    }

    async getDetailSystemBanner(id, page, limit) {
        const match:any = {
            _id: ObjectId(id)
        }
        const query = this.model.aggregate([
            {
                $match: match
            },
            BannerQueryHelper.relations.vouchersGroupByClassify(),
            BannerQueryHelper.relations.productsGroupByClassify()
        ])
        const result = await this.model
            .aggregatePaginateCustom(query, { page, limit })
        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        const data = result.docs[0];
        return { data, paginate }
    }

    async countBannerSystem() {
        const date = new Date()
        return this.model.aggregate([
            {
                $match: {
                    shop_id: null,
                    start_time: { '$lte': date },
                    end_time: { '$gte': date },
                }
            },
            {
                $count: "total"
            }
        ])
    }

}

export class BannerQueryHelper {
    public static relations = {

        vouchersGroupByClassify: () => ({
            $lookup: {
                from: "vouchers",
                let: {
                    voucher_ids: "$vouchers"
                },
                pipeline: [
                    {
                        $match: {
                            $and: [
                                {
                                    '$expr': {
                                        $and: [
                                            {
                                                $in: ["$_id", { $ifNull: ['$$voucher_ids', []] }]
                                            }
                                        ]
                                    },
                                },
                            ]
                        }
                    },
                    {
                        $group: {
                            _id: "$classify",
                            data: {"$push":"$$ROOT"}
                        }
                    }
                ],
                as: "vouchers"
            }

        }),
        productsGroupByClassify: () => ({
            $lookup: {
                from: "products",
                let: {
                    product_ids: "$products"
                },
                pipeline: [
                    {
                        $match: {
                            $and: [
                                {
                                    '$expr': {
                                        $and: [
                                            {
                                                $in: ["$_id", { $ifNull: ['$$product_ids', []] }]
                                            }
                                        ]
                                    },
                                },
                            ]
                        }
                    },
                    {
                        $lookup: {
                            from: "variants",
                            let: {
                                product_id: "$_id"
                            },
                            pipeline: [
                                {
                                    $match: {
                                        $and: [
                                            {
                                                '$expr': {
                                                    $and: [
                                                        {
                                                            $eq: ["$product_id", "$$product_id"]
                                                        }
                                                    ]
                                                },
                                            }
                                        ]
                                    }
                                }
                            ],
                            as: "variants"
                        }
                    }
                    // {
                    //     $lookup: {
                    //         from: "ecategories",
                    //         let: {
                    //             category_id: "$category_id"
                    //         },
                    //         pipeline: [
                    //             {
                    //                 $match: {
                    //                     $and: [
                    //                         {
                    //                             '$expr': {
                    //                                 $and: [
                    //                                     {
                    //                                         $eq: ["$_id", { $ifNull: ['$$category_id', []] }]
                    //                                     }
                    //                                 ]
                    //                             },
                    //                         },
                    //                     ]
                    //                 }
                    //             }
                    //         ],
                    //         as: "category"
                    //     }
                    // },
                    // {
                    //     $group: {
                    //         _id: "$category_id",
                    //         category: { $first: "$category" },
                    //         product_data: { $push: "$$ROOT" }
                    //     }
                    // },
                    // {
                    //     $unwind: {
                    //         path: "$category",
                    //         preserveNullAndEmptyArrays: true
                    //     }
                    // },
                    // {
                    //     $project: {
                    //         _id: 0,
                    //         category_id: '$_id',
                    //         product_data: "$product_data"
                    //     }
                    // }
                ],
                as: "products"
            }

        })
    }
}