import { BaseRepository } from "./BaseRepository";
import EMedia from "../models/Media";
import { MediaDoc } from "../interfaces/Media";
export class MediaRepo extends BaseRepository<MediaDoc>  {

    private static instance: MediaRepo;

    constructor() {
        super();
        this.model = EMedia;
    }

    public static getInstance(): MediaRepo {
        if (!MediaRepo.instance) {
            MediaRepo.instance = new MediaRepo();
        }

        return MediaRepo.instance;
    }

    async findByFullPath(full_path: string) {
        try {
            const result = await this.model.findOne({ full_path: full_path });
            return result
        } catch (error) {
            return null
        }
    }

}