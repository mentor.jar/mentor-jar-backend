import { BaseRepository } from "./BaseRepository";
import ShippingMethod from "../models/Shipping_Method";
import { ShippingMethodDoc } from "../interfaces/Shipping_Method";
import { ObjectId } from "../utils/helper";
import { ShopRepo } from "./ShopRepo";
export class ShippingMethodRepo extends BaseRepository<ShippingMethodDoc>  {

    private static instance: ShippingMethodRepo;

    private shopRepo = ShopRepo.getInstance();

    constructor() {
        super();
        this.model = ShippingMethod;
    }

    public static getInstance(): ShippingMethodRepo {
        if (!ShippingMethodRepo.instance) {
            ShippingMethodRepo.instance = new ShippingMethodRepo();
        }

        return ShippingMethodRepo.instance;
    }

    async getAllShippingMethod() {
        const shippingMethod = await this.model.find();
        return shippingMethod;
    }

    async getDefaultShippingMethod() {
        const shippingMethod = await this.model.findOne();
        return shippingMethod;
    }

    async getShippingMethodByIds(ids) {
        if (!ids.length) return []

        ids = ids.map(id => ObjectId(id));
        const match = {
            _id: {$in: ids}
        }

        const result = await this.model.aggregate([
            {
                $match: match
            }
        ])
        return result
    }

    async getAllShippingMethodById(id) {
        const shippingMethod = await this.model.findOne({ _id: ObjectId(id) });
        return shippingMethod;
    }

    getShippingMethodForShop = async (shop, shipping_method_id) => {
        const shipping_methods = shop.shipping_methods.filter(method =>  
            method.shipping_method_id.toString() ===  shipping_method_id.toString()
          );
        return shipping_methods.length > 0 ? shipping_methods[0] : 0;
      }
    
    checkShopTurnOnShippingMethod = async (shop, shipping_method_id) => {
        const shipping_methods = shop.shipping_methods.filter(method =>  
            method.shipping_method_id.toString() ===  shipping_method_id.toString()
        );
        return shipping_methods[0].is_active;
    }

    async getShippingMethodActiveByShop(shopId) {
        const shop = await this.shopRepo.findShopByShopId(shopId);
        return shop.shipping_methods.filter(shippingMethod => shippingMethod.is_active == true);
    }
}