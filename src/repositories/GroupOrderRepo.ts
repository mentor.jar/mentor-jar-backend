import { BaseRepository } from "./BaseRepository";
import GroupOrder from "../models/GroupOrder";
import { GroupOrderDoc } from "../interfaces/GroupOrder";
import { cloneObj, ObjectId } from "../utils/helper";
export class GroupOrderRepo extends BaseRepository<GroupOrderDoc>  {

    private static instance: GroupOrderRepo;

    constructor() {
        super();
        this.model = GroupOrder;
    }

    public static getInstance(): GroupOrderRepo {
        if (!GroupOrderRepo.instance) {
            GroupOrderRepo.instance = new GroupOrderRepo();
        }

        return GroupOrderRepo.instance;
    }

}