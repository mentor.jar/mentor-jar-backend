import { BaseRepository } from "./BaseRepository";
import CartItem from "../models/CartItem";
import { CartItemDoc } from "../interfaces/CartItem";
import { cloneObj, ObjectId } from "../utils/helper";
import mongoose from 'mongoose';
import { ShopItemCheckout } from "../controllers/serviceHandles/cart/definition";
import { ProductRepo } from "./ProductRepo";
export class CartItemRepo extends BaseRepository<CartItemDoc>  {

    private static instance: CartItemRepo;
    private productRepo = ProductRepo.getInstance();

    constructor() {
        super();
        this.model = CartItem;
    }

    public static getInstance(): CartItemRepo {
        if (!CartItemRepo.instance) {
            CartItemRepo.instance = new CartItemRepo();
        }

        return CartItemRepo.instance;
    }

    async createOrUpdateItem(item) {
        let cartItem: any = {}
        if (item.variant_id) {
            // Find by variant
            cartItem = await this.findOne({
                user_id: ObjectId(item.user_id),
                variant_id: ObjectId(item.variant_id)
            })
        }
        else {
            // Find by product
            cartItem = await this.findOne({
                user_id: ObjectId(item.user_id),
                product_id: ObjectId(item.product_id)
            })
        }
        if (cartItem) {
            // Increase quantity
            let quantity = item.quantity;
            const exitsProduct: any = await this.productRepo.findOne({
                _id: ObjectId(item.product_id)
            })
            if (exitsProduct.group_buy) {
                quantity = quantity;
            } else {
                quantity = cartItem.quantity + quantity;
            }
            const updateInfo = {
                quantity: quantity
            }
            cartItem = await this.update(cartItem._id, updateInfo)
        }
        else {
            // Create new item
            cartItem = await this.create(item)
        }
        return cartItem
    }

    /**
     * 
     * @param items 
     * @returns 
     */

     async getItems(itemIds) {
        try {
            if (!itemIds) return [];
            let objectIdArray = itemIds.map(el => mongoose.Types.ObjectId(el));
            let cartItem = await this.model.aggregate([
                {
                    $match: {
                        _id: { $in: objectIdArray }
                    }
                },
                this.advanceProductCheckoutQuery,
                this.advanceShopQuery,
                {
                    $unwind: {
                        path: "$products",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $unwind: {
                        path: "$shop",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $sort: { "createdAt": -1 }
                },
            ])
            return cartItem
        } catch (error) {
            console.log(error)
        }
    }


    async getItemsCheckoutPage(itemIds) {
        try {
            if (!itemIds) return [];
            let objectIdArray = itemIds.map(el => mongoose.Types.ObjectId(el));
            let cartItem = await this.model.aggregate([
                {
                    $match: {
                        _id: { $in: objectIdArray }
                    }
                },
                this.advanceProductCheckoutQuery,
                this.advanceShopQuery,
                {
                    $sort: { "createdAt": -1 }
                },
                {
                    $group: {
                        _id: "$shop_id",
                        items: { $push: this.itemInfos },
                        shop: { $last: '$shop' }
                    }
                },
                {
                    $sort: { "items.createdAt": -1 }
                },

            ])
            return cartItem
        } catch (error) {
            console.log(error)
        }
    }

    async findAndGroupByShop(user_id) {
        try {
            let cartItem = await this.model.aggregate([
                {
                    $match: {
                        user_id: ObjectId(user_id)
                    }
                },
                this.advanceProductQuery,
                this.advanceShopQuery,
                {
                    $sort: { "createdAt": -1 }
                },
                {
                    $group: {
                        _id: "$shop_id",
                        items: { $push: this.itemInfos },
                        shop: { $last: '$shop' }
                    }
                },
                {
                    $sort: { "items.createdAt": -1 }
                },
            ]).then(result => {
                result = cloneObj(result)
                const inValidItems: Array<any> = []
                const validItems: Array<any> = []
                result.forEach(res => {
                    const filterValids: Array<any> = []
                    res.items.forEach(item => {
                        item = cloneObj(item)
                        item.product = item.products[0] || null
                        delete item.products

                        // handle to check valid
                        const { product } = item
                        if (!product || !product.allow_to_sell || product.is_approved != "approved" || product.quantity <= 0 || product.deleted_at) {
                            item.is_valid = false
                            if(!product || product.deleted_at) {
                                item.product_not_found = true
                            }
                            else {
                                item.product_not_found = false
                            }
                            inValidItems.push(item)
                        }
                        else if(!item.variant_id && product.variants.length) {
                            item.is_valid = false
                            item.product_not_found = false
                            inValidItems.push(item)
                        }
                        else {
                            item.is_valid = true
                            item.product_not_found = false
                            filterValids.push(item)
                        }
                    })
                    res.items = filterValids
                    res.shop = res.shop[0] || null
                    res.shop_id = res._id
                    delete res._id
                    if (filterValids.length) {
                        validItems.push(res)
                    }
                })
                return {
                    valid: validItems,
                    inValid: inValidItems
                }
            });
            return cartItem
        } catch (error) {
            console.log(error)
            throw new Error("Lỗi khi lấy thông tin giỏ hàng. Vui lòng thử lại sau")
        }
    }

    async getDetailItem(itemID) {
        const filter = {
            _id: ObjectId(itemID)
        }
        const relations = [
            this.model.relationship().shop(),
            this.model.relationship().product()
        ]
        const sort = { createdAt: -1 }
        const result = await this.getExpandValues(filter, relations, sort);
        return result
    }

    async getCartItemsByShopAndUser(shopID, userID) {
        const match = {
            shop_id: ObjectId(shopID),
            user_id: ObjectId(userID)
        }

        const result = this.model.aggregate([
            {
                $match: match
            },
            this.advanceProductQuery
        ])
        return result
    }

    async findByIDAndUserID(cartItemID, userID) {
        const filter = {
            _id: ObjectId(cartItemID),
            user_id: ObjectId(userID)
        }
        return this.model.findOne(filter)
    }

    private itemInfos = { _id: "$_id", product_id: "$product_id", quantity: "$quantity", variant_id: "$variant_id", user_id: "$user_id", products: "$products", createdAt: "$createdAt" }

    private advanceProductQuery: any = {
        $lookup:
        {
            from: "products",
            let: {
                id: "$product_id",
            },
            pipeline: [
                {
                    $match: {
                        $and: [
                            // Không kiểm tra các điều kiện allow_to_sell, is_approved, deleted_at ở đây
                            {
                                '$expr': {
                                    '$eq': ['$_id', '$$id']
                                }
                            }
                        ]
                    }
                },
                {
                    $lookup: {
                        from: "variants",
                        let: {
                            product_id: "$_id"
                        },
                        pipeline: [
                            {
                                $match: {
                                    $and: [
                                        {
                                            '$expr': {
                                                $and: [
                                                    {
                                                        $eq: ["$product_id", "$$product_id"]
                                                    }
                                                ]
                                            },
                                        }
                                    ]
                                }
                            }
                        ],
                        as: "variants"
                    }
                },
                {
                    $lookup: {
                        from: "optiontypes",
                        let: {
                            product_id: "$_id"
                        },
                        pipeline: [
                            {
                                $match: {
                                    $and: [
                                        {
                                            '$expr': {
                                                $and: [
                                                    {
                                                        $eq: ["$product_id", "$$product_id"]
                                                    }
                                                ]
                                            },
                                        }
                                    ]
                                }
                            },
                            {
                                $lookup: {
                                    from: "optionvalues",
                                    let: {
                                        option_type_id: "$_id"
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $and: [
                                                    {
                                                        '$expr': {
                                                            $and: [
                                                                {
                                                                    $eq: ["$option_type_id", "$$option_type_id"]
                                                                }
                                                            ]
                                                        },
                                                    }
                                                ]
                                            },

                                        },
                                        {
                                            $project: {
                                                _id: 1,
                                                name: 1,
                                                image: 1
                                            }
                                        }


                                    ],
                                    as: "option_values"
                                }
                            },
                            {
                                $project: {
                                    _id: 1,
                                    name: 1,
                                    option_values: 1
                                }
                            }
                        ],
                        as: "option_types"
                    }
                },
                {
                    $lookup: {
                        from: "feedbacks",
                        let: {
                            product_id: "$_id"
                        },
                        pipeline: [
                            {
                                $match: {
                                    $and: [
                                        {
                                            '$expr': {
                                                $and: [
                                                    {
                                                        $eq: ["$target_id", "$$product_id"]
                                                    }
                                                ]
                                            },
                                        }
                                    ]
                                }
                            },
                            {
                                $group: {
                                    _id: null,
                                    averageFeedbackRate: { $avg: "$vote_star" }
                                }
                            }
                        ],
                        as: "feedbacks"
                    },
                }
            ],
            as: "products"
        }
    }

    private advanceProductCheckoutQuery: any = {
        $lookup:
        {
            from: "products",
            let: {
                id: "$product_id",
                variant_id: "$variant_id"
            },
            pipeline: [
                {
                    $match: {
                        $and: [
                            {
                                '$expr': {
                                    '$eq': ['$_id', '$$id']
                                }
                            },
                            {
                                $or: [
                                    { deleted_at: { $exists: false } },
                                    { deleted_at: null }
                                ]
                            }
                        ]
                    }
                },
                {
                    $addFields: {
                        variant_id: "$$variant_id"
                    }
                },
                {
                    $lookup: {
                        from: "variants",
                        let: {
                            id: "$variant_id"
                        },
                        pipeline: [
                            {
                                $match: {
                                    $and: [
                                        {
                                            '$expr': {
                                                $and: [
                                                    {
                                                        $eq: ["$_id", "$$id"]
                                                    }
                                                ]
                                            },
                                        }
                                    ]
                                }
                            }
                        ],
                        as: "variants"
                    }
                },
                {
                    $lookup: {
                        from: "optiontypes",
                        let: {
                            product_id: "$_id"
                        },
                        pipeline: [
                            {
                                $match: {
                                    $and: [
                                        {
                                            '$expr': {
                                                $and: [
                                                    {
                                                        $eq: ["$product_id", "$$product_id"]
                                                    }
                                                ]
                                            },
                                        }
                                    ]
                                }
                            }
                        ],
                        as: "option_types"
                    }
                }
            ],
            as: "products"
        }
    }

    private advanceShopQuery: any = {
        $lookup: {
            from: "shops",
            let: {
                id: "$shop_id"
            },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            '$eq': ['$_id', '$$id']
                        },
                        is_approved: true,
                        pause_mode: false
                    }
                },
                {
                    $lookup: {
                        from: "users",
                        let: {
                            user_id: "$user_id"
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $eq: ["$_id", "$$user_id"]
                                    }
                                }
                            },
                            {
                                $lookup: {
                                    from: "galleryimages",
                                    let: {
                                        user_id: "$_id"
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                '$expr': {
                                                    $and: [
                                                        {
                                                            $eq: ["$galleryImageAbleId", "$$user_id"]
                                                        },
                                                        {
                                                            $eq: ["$galleryImageAbleType", "1"]
                                                        }
                                                    ]
                                                },
                                            }
                                        }
                                    ],
                                    as: "gallery_image"
                                }
                            },
                            {
                                $unwind: {
                                    path: "$gallery_image",
                                    preserveNullAndEmptyArrays: true
                                }
                            }
                        ],
                        as: "user"
                    }
                },
                {
                    $unwind: {
                        path: "$user",
                        preserveNullAndEmptyArrays: false
                    }
                }
            ],
            as: "shop"
        }
    }

}