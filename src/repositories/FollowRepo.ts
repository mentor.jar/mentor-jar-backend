import { BaseRepository } from "./BaseRepository";
import Follow from "../models/Follow";
import { FollowDoc } from "../interfaces/Follow";
export class FollowRepo extends BaseRepository<FollowDoc>  {

    private static instance: FollowRepo;

    constructor() {
        super();
        this.model = Follow;
    }

    public static getInstance(): FollowRepo {
        if (!FollowRepo.instance) {
            FollowRepo.instance = new FollowRepo();
        }

        return FollowRepo.instance;
    }

    async checkFollow(followerId, userId) {
        const follow = await this.model.findOne({ userId: userId, followerId: followerId });
        return follow ? true : false;
    }
}