import { BaseRepository, BaseQueryHelper } from "./BaseRepository"
import Order from "../models/Order"
import { OrderDoc } from "../interfaces/Order"
import { cloneObj, ObjectId, isMappable } from "../utils/helper"
import { convertTimestampToDate } from "../utils/dateUtil"
import { AddressDTO } from "../DTO/AddressDTO"
import { OrderDTO } from "../DTO/OrderDTO"
import { VariantRepo } from "./Variant/VariantRepo"
import { ProductRepo } from "./ProductRepo"
import { CartItemRepo } from "./CartItemRepo"
import moment from "moment";
import { REQ_DATE_TIME_FORMAT } from "../base/variable";
import mongoose from 'mongoose';
import { PaymentStatus, ShippingStatus, GHTKStatusAction } from "../models/enums/order"
import { listShippingStatus } from "../controllers/serviceHandles/order/definition"

export class OrderRepository extends BaseRepository<OrderDoc> {
    private static instance: OrderRepository

    private constructor() {
        super()
        this.model = Order
    }

    private variantRepo = VariantRepo.getInstance()
    private productRepo = ProductRepo.getInstance()
    private cartItemRepo = CartItemRepo.getInstance()

    public static getInstance(): OrderRepository {
        if (!OrderRepository.instance) {
            OrderRepository.instance = new OrderRepository()
        }
        return OrderRepository.instance
    }

    async getDetailOrderInNotification(id: string) {
        let match = { _id: ObjectId(id) }
        const query = await this.model.aggregate([
            {
                $match: match,
            },
            this.advanceOrderItemsQuery])
        if (query.length > 0) {
            let result = query[0];
            result = OrderDTO.newInstance(result).completeDataInNotification()
            return result
        }
        return null

    }

    public async getDetailOrderBuyer(id: string) {
        let match = { _id: ObjectId(id) }
        const query = await this.model.aggregate([
            {
                $match: match,
            },
            this.advanceGroupOrderQuery,
            this.advanceUserQuery,
            this.advancePaymentMethodQuery,
            this.advanceShippingMethodQuery,
            this.advanceOrderItemsQuery,
            this.advanceShopOfOrderQuery,
            {
                $unwind: {
                    path: "$shop",
                    preserveNullAndEmptyArrays: true
                }
            },
            this.advanceOrderShippingQuery,
            {
                $unwind: {
                    path: "$order_shipping",
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $unwind: {
                    path: "$group_orders",
                    preserveNullAndEmptyArrays: true
                }
            },
            this.advanceShopFeedbackQuery,
            { $addFields: { is_feedbacked_user: { $cond: { if: { $gt: [{ $size: "$feedbacks" }, 0] }, then: true, else: false } } } },
            { $unset: "feedbacks" },
            {
                $addFields: {
                    user: {
                        $arrayElemAt: ["$user", 0],
                    },
                },
            },
        ])
        if (query.length > 0) {
            let result = query[0]
            result.address = new AddressDTO(result.address).toMainAddressJSON()
            result = OrderDTO.newInstance(result).completeDataPaymentAndShippingInfo()
            return result
        }
        return null
    }

    async queryDetailOrder(match) {
        const query = await this.model.aggregate([
            {
                $match: match,
            },
            this.advanceUserQuery,
            this.advancePaymentMethodQuery,
            this.advanceShippingMethodQuery,
            this.advanceOrderItemsQuery,
            this.advanceShopOfOrderQuery,
            {
                $unwind: {
                    path: "$shop",
                    preserveNullAndEmptyArrays: true
                }
            },
            this.advanceOrderShippingQuery,
            {
                $unwind: {
                    path: "$order_shipping",
                    preserveNullAndEmptyArrays: true
                }
            },
            this.advanceShopFeedbackQuery,
            { $addFields: { is_feedbacked_user: { $cond: { if: { $gt: [{ $size: "$feedbacks" }, 0] }, then: true, else: false } } } },
            { $unset: "feedbacks" },
            {
                $addFields: {
                    user: {
                        $arrayElemAt: ["$user", 0],
                    },
                },
            },
        ])
        if (query.length > 0) {
            let result = query[0]
            result.address = new AddressDTO(result.address, true).toMainAddressJSON()
            result.pick_address = new AddressDTO(result.pick_address).toMainAddressJSON()
            result = OrderDTO.newInstance(result).completeDataPaymentAndShippingInfo()
            return result
        }
        return null
    }

    async queryDetailOrderAdmin(match) {
        const query = await this.model.aggregate([
            {
                $match: match,
            },
            this.advanceGroupOrderQuery,
            this.advanceUserQuery,
            this.advancePaymentMethodQuery,
            this.advanceShippingMethodQuery,
            this.advanceOrderItemsQuery,
            this.advanceShopOfOrderQuery,
            {
                $unwind: {
                    path: "$shop",
                    preserveNullAndEmptyArrays: true
                }
            },
            this.advanceOrderShippingQuery,
            {
                $unwind: {
                    path: "$order_shipping",
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $unwind: {
                    path: "$group_orders",
                    preserveNullAndEmptyArrays: true
                }
            },
            this.advanceShopFeedbackQuery,
            { $addFields: { is_feedbacked_user: { $cond: { if: { $gt: [{ $size: "$feedbacks" }, 0] }, then: true, else: false } } } },
            { $unset: "feedbacks" },
            {
                $addFields: {
                    user: {
                        $arrayElemAt: ["$user", 0],
                    },
                },
            },
        ])
        if (query.length > 0) {
            let result = query[0]
            result.address = new AddressDTO(result.address).toMainAddressJSON()
            result.pick_address = new AddressDTO(result.pick_address).toMainAddressJSON()
            result = OrderDTO.newInstance(result).completeDataPaymentAndShippingInfo()
            return result
        }
        return null
    }

    public async getDetailOrder(id: string) {
        let match = { _id: ObjectId(id) }
        const result = await this.queryDetailOrder(match);
        return result;
    }

    public async getDetailOrderAdmin(id: string) {
        let match = { _id: ObjectId(id) }
        const result = await this.queryDetailOrderAdmin(match);
        return result;
    }

    public async getDetailOrderByShop(id: string, shopId: string) {
        let match = {
            _id: ObjectId(id),
            shop_id: ObjectId(shopId)
        }
        const result = await this.queryDetailOrder(match);
        return result;
    }

    async getOrderWithFilterCMS(
        date_start,
        date_end,
        orderNumber,
        shopId,
        limit = 20,
        page = 1,
        filterType
    ) {
        let match: any = {
            $or: [{ deleted_at: { $exists: false } }, { deleted_at: null }],
        }
        if (date_start && date_end) {
            const start = convertTimestampToDate(date_start)
            const end = convertTimestampToDate(date_end)
            match = {
                $or: [{ deleted_at: { $exists: false } }, { deleted_at: null }],
                $and: [
                    { created_at: { $gte: start } },
                    { created_at: { $lte: end } },
                ],
            }
        } else {
            if (date_start) {
                const start = convertTimestampToDate(date_start)
                match.created_at = { $gte: start }
            }
            if (date_end) {
                const end = convertTimestampToDate(date_end)
                match.created_at = { $lte: end }
            }
        }
        if (shopId) {
            match.shop_id = ObjectId(shopId)
        }
        if (filterType) {
            if (filterType.toLowerCase() === "cancel") {
                match.shipping_status = { $in: ["canceling", "canceled"] }
            }
            else if (filterType.toLowerCase() === "return") {
                match.shipping_status = { $in: ["return"] }
            }
            else if (filterType.toLowerCase() === "returning") {
                match.shipping_status = { $in: ["return"] }
                match.refund_information = { $exists: true, $ne: [] }
                match["refund_information.status"] = { $in: ["opening"] }
            }
            else if (filterType.toLowerCase() === "returned") {
                match.shipping_status = { $in: ["return"] }
                match.refund_information = { $exists: true, $ne: [] }
                match["refund_information.status"] = { $in: ["canceled", "closed"] }
            }
            else {
                match.shipping_status = filterType.toLowerCase()
            }
        }
        if (orderNumber) {
            match.order_number = { $regex: `${orderNumber}`, $options: "i" }
        }
        const query = this.model.aggregate([
            {
                $match: match,
            },
            this.basicOrderItemsQuery,
            this.advanceShippingMethodQuery,
            {
                $unwind: {
                    path: "$shipping_method",
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $sort: { updated_at: -1 },
            },
        ])
        const result = await this.model.aggregatePaginateCustom(query, {
            page,
            limit,
        })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs,
        }
        const data = result.docs
        return { data, paginate }
    }

    async getOrderWithFilter(shopId, limit = 20, page = 1, filterType, keyword) {
        const condition: Array<any> = [
            {
                $or: [{ deleted_at: { $exists: false } }, { deleted_at: null }],
            },
        ]
        if (shopId) {
            condition.push({ shop_id: ObjectId(shopId) })
        }

        if (filterType) {
            if (filterType.toLowerCase() === "cancel") {
                condition.push({ shipping_status: { $in: ["canceling", "canceled"] } })
            }
            else if (filterType.toLowerCase() === "return") {
                condition.push({ shipping_status: { $in: ["return"] } })
            }
            else if (filterType.toLowerCase() === "returning") {
                condition.push({ shipping_status: { $in: ["return"] } })
                condition.push({ refund_information: { $exists: true, $ne: null } })
                condition.push({ "refund_information.status": { $in: ["opening"] } })
            }
            else if (filterType.toLowerCase() === "returned") {
                condition.push({ shipping_status: { $in: ["return"] } })
                condition.push({ refund_information: { $exists: true, $ne: null } })
                condition.push({ "refund_information.status": { $in: ["canceled", "closed"] } })
            }
            else {
                condition.push({ shipping_status: filterType.toLowerCase() })
            }

        }
        if (keyword) {
            const searchQuery = {
                $or: [
                    BaseQueryHelper.fieldSearch(
                        keyword,
                        "user.nameOrganizer.userName"
                    ),
                    BaseQueryHelper.fieldSearch(
                        keyword,
                        "shop.user.nameOrganizer.userName"
                    ),
                    {
                        "order_items": {
                            "$elemMatch": {
                                "$or": [
                                    BaseQueryHelper.fieldSearch(
                                        keyword,
                                        "product.name"
                                    )]
                            }
                        }
                    },
                    BaseQueryHelper.fieldSearch(keyword, "order_number"),
                ],
            }
            condition.push(searchQuery)
        }

        const query = this.model.aggregate([
            this.advanceUserQuery,
            this.advanceOrderItemsQuery,
            this.advancePaymentMethodQuery,
            this.advanceShippingMethodQuery,
            // this.advanceVoucherQuery,
            this.advanceShopOfOrderQuery,
            {
                $unwind: {
                    path: "$shop",
                    preserveNullAndEmptyArrays: true
                }
            },
            this.advanceOrderShippingQuery,
            {
                $unwind: {
                    path: "$order_shipping",
                    preserveNullAndEmptyArrays: true
                }
            },
            this.advanceShopFeedbackQuery,
            { $addFields: { is_feedbacked_user: { $cond: { if: { $gt: [{ $size: "$feedbacks" }, 0] }, then: true, else: false } } } },
            // { $unset: "feedbacks" },
            {
                $addFields: {
                    user: {
                        $arrayElemAt: ["$user", 0],
                    },
                },
            },
            {
                $sort: { created_at: -1 },
            },
            {
                $match: {
                    $and: condition,
                }
            }
        ])
        const result = await this.model.aggregatePaginateCustom(query, {
            page,
            limit,
        })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs,
        }
        const data = result.docs
        return { data, paginate }
    }

    async getOrderBuyerWithFilter(userID, limit = 20, page = 1, filterType, keyword) {
        const condition: Array<any> = [
            {
                $or: [{ deleted_at: { $exists: false } }, { deleted_at: null }],
            },
        ]
        if (userID) {
            condition.push({ user_id: ObjectId(userID) })
        }

        if (filterType) {
            if (filterType.toLowerCase() === "cancel") {
                condition.push({ shipping_status: { $in: ["canceling", "canceled"] } })
            }
            else if (filterType.toLowerCase() === "return") {
                condition.push({ shipping_status: { $in: ["return"] } })
            }
            else if (filterType.toLowerCase() === "returning") {
                condition.push({ shipping_status: { $in: ["return"] } })
                condition.push({ refund_information: { $exists: true, $ne: null } })
                condition.push({ "refund_information.status": { $in: ["opening"] } })
            }
            else if (filterType.toLowerCase() === "returned") {
                condition.push({ shipping_status: { $in: ["return"] } })
                condition.push({ refund_information: { $exists: true, $ne: null } })
                condition.push({ "refund_information.status": { $in: ["canceled", "closed"] } })
            }
            else {
                condition.push({ shipping_status: filterType.toLowerCase() })
            }

        }
        if (keyword) {
            const searchQuery = {
                $or: [
                    BaseQueryHelper.fieldSearch(
                        keyword,
                        "user.nameOrganizer.userName"
                    ),
                    BaseQueryHelper.fieldSearch(
                        keyword,
                        "shop.user.nameOrganizer.userName"
                    ),
                    {
                        "order_items": {
                            "$elemMatch": {
                                "$or": [
                                    BaseQueryHelper.fieldSearch(
                                        keyword,
                                        "product.name"
                                    )]
                            }
                        }
                    },
                    BaseQueryHelper.fieldSearch(keyword, "order_number"),
                ],
            }
            condition.push(searchQuery)
        }

        const query = this.model.aggregate([
            this.advanceUserQuery,
            this.advanceOrderItemsQuery,
            this.advancePaymentMethodQuery,
            // this.advanceShippingMethodQuery,
            // this.advanceVoucherQuery,
            this.advanceShopOfOrderQuery,
            {
                $unwind: {
                    path: "$shop",
                    preserveNullAndEmptyArrays: true
                }
            },
            // this.advanceOrderShippingQuery,
            {
                $unwind: {
                    path: "$order_shipping",
                    preserveNullAndEmptyArrays: true
                }
            },
            // this.advanceShopFeedbackQuery,
            // { $addFields: { is_feedbacked_user: { $cond: { if: { $gt: [{ $size: "$feedbacks" }, 0] }, then: true, else: false } } } },
            // { $unset: "feedbacks" },
            {
                $addFields: {
                    user: {
                        $arrayElemAt: ["$user", 0],
                    },
                },
            },
            {
                $match: {
                    $and: condition,
                }
            },
            {
                $sort: { created_at: -1 },
            },
        ])
        const result = await this.model.aggregatePaginateCustom(query, {
            page,
            limit,
        })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs,
        }
        const data = result.docs
        return { data, paginate }

    }

    async searchOrder(shopId, limit = 20, page = 1, keyword) {
        const condition: Array<any> = [
            {
                $or: [{ deleted_at: { $exists: false } }, { deleted_at: null }],
            },
        ]

        if (shopId) {
            condition.push({ shop_id: ObjectId(shopId.toString()) })
        }

        if (keyword) {
            condition.push({
                $or: [
                    BaseQueryHelper.fieldSearch(keyword, "receiverName"),
                    BaseQueryHelper.fieldSearch(
                        keyword,
                        "userOrder.nameOrganizer.userName"
                    ),
                    BaseQueryHelper.fieldSearch(
                        keyword,
                        "userOrder.nameOrganizer.userName"
                    ),
                    {
                        "order_items": {
                            "$elemMatch": {
                                "$or": [
                                    BaseQueryHelper.fieldSearch(
                                        keyword,
                                        "product.name"
                                    )]
                            }
                        }
                    },
                    BaseQueryHelper.fieldSearch(keyword, "order_number"),
                ],
            })
        }

        const aggregate = this.model.aggregate([
            this.advanceUserQuery,
            this.advanceOrderItemsQuery,
            this.advancePaymentMethodQuery,
            this.advanceShippingMethodQuery,
            this.advanceShopOfOrderQuery,
            {
                $unwind: {
                    path: "$shop",
                    preserveNullAndEmptyArrays: true
                }
            },
            this.advanceOrderShippingQuery,
            {
                $unwind: {
                    path: "$order_shipping",
                    preserveNullAndEmptyArrays: true
                }
            },
            this.advanceShopFeedbackQuery,
            { $addFields: { is_feedbacked_user: { $cond: { if: { $gt: [{ $size: "$feedbacks" }, 0] }, then: true, else: false } } } },
            { $unset: "feedbacks" },
            {
                $addFields: {
                    user: {
                        $arrayElemAt: ["$user", 0],
                    },
                },
            },
            {
                $addFields: {
                    receiverName: {
                        $concat: [
                            "$address.name",
                        ],
                    },
                },
            },
            {
                $match: {
                    $and: condition,
                },
            },
            {
                $sort: { updated_at: -1 },
            }
        ])

        const result = await this.model.aggregatePaginateCustom(aggregate, {
            page,
            limit,
        })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs,
        }
        const data = result.docs
        return { data, paginate }
    }

    async searchOrderForBuyer(userID, limit = 20, page = 1, keyword) {
        const condition: Array<any> = [
            {
                $or: [{ deleted_at: { $exists: false } }, { deleted_at: null }],
            },
        ]
        if (userID) {
            condition.push({ user_id: ObjectId(userID.toString()) })
        }

        if (keyword) {
            const searchQuery = {
                $or: [
                    BaseQueryHelper.fieldSearch(keyword, "receiverName"),
                    BaseQueryHelper.fieldSearch(
                        keyword,
                        "user.nameOrganizer.userName"
                    ),
                    BaseQueryHelper.fieldSearch(
                        keyword,
                        "shop.user.nameOrganizer.userName"
                    ),
                    {
                        "order_items": {
                            "$elemMatch": {
                                "$or": [
                                    BaseQueryHelper.fieldSearch(
                                        keyword,
                                        "product.name"
                                    )]
                            }
                        }
                    },
                    BaseQueryHelper.fieldSearch(keyword, "order_number"),
                ],
            }
            condition.push(searchQuery)
        }

        const query = this.model.aggregate([
            this.advanceUserQuery,
            this.advanceOrderItemsQuery,
            this.advancePaymentMethodQuery,
            this.advanceShippingMethodQuery,
            // this.advanceVoucherQuery,
            this.advanceShopOfOrderQuery,
            {
                $unwind: {
                    path: "$shop",
                    preserveNullAndEmptyArrays: true
                }
            },
            this.advanceOrderShippingQuery,
            {
                $unwind: {
                    path: "$order_shipping",
                    preserveNullAndEmptyArrays: true
                }
            },
            this.advanceShopFeedbackQuery,
            { $addFields: { is_feedbacked_user: { $cond: { if: { $gt: [{ $size: "$feedbacks" }, 0] }, then: true, else: false } } } },
            { $unset: "feedbacks" },
            {
                $addFields: {
                    user: {
                        $arrayElemAt: ["$user", 0],
                    },
                },
            },
            {
                $addFields: {
                    receiverName: {
                        $concat: [
                            "$address.name",
                        ],
                    },
                },
            },
            // {
            //     $addFields: {
            //         productsName: {
            //             $concat: [
            //                 "$address.name",
            //             ],
            //         },
            //     },
            // },
            {
                $match: {
                    $and: condition,
                },
            },
            {
                $sort: { updated_at: -1 },
            },
        ])
        const result = await this.model.aggregatePaginateCustom(query, {
            page,
            limit,
        })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs,
        }
        const data = result.docs
        return { data, paginate }
    }

    public async getOrderOfShop(shop_id, start_time: any, end_time: any, query_optionals: Array<any> = [], includingUser = true) {
        const condition: Array<any> = [
            {
                $or: [{ deleted_at: { $exists: false } }, { deleted_at: null }],
            },
        ]

        const query: Array<any> = []

        if (shop_id) {
            condition.push({ shop_id: ObjectId(shop_id.toString()) })
        }
        if (start_time) {
            condition.push({ created_at: { $gte: start_time } })
        }

        if (end_time) {
            condition.push({ created_at: { $lte: end_time } })
        }

        if (query_optionals?.length) {
            query_optionals.forEach(query_other => {
                condition.push(query_other)
            })
        }
        
        if(includingUser) {
            query.push(this.advanceUserQuery)
            query.push(
                {
                    $addFields: {
                        userOrder: {
                            $arrayElemAt: ["$user", 0],
                        },
                    },
                },
                {
                    $project: {
                        user: 0,
                    },
                }
            )
        }
        
        const match_condition: any = {
            $and: condition,
        }

        query.push({ $match: match_condition })
        query.push({
            $sort: { updated_at: -1 },
        })

        console.log("query: ", JSON.stringify(query));
        

        const result = await this.model.aggregate(query)

        return result
    }

    async checkCartItemBeforeCreateOrder(cartItems, userID, shopID, req, orderRoomId, groupID) {
        try {
            let listPromise: any = []
            let totalWeight = 0
            const dataItems = [] // for re-use in controller
            listPromise = cartItems.map(async item => {
                return new Promise(async (resolve, reject) => {
                    try {
                        const {
                            _id,
                            variant_id,
                            product_id,
                            user_id,
                            quantity,
                        } = item
                        const itemDB = await this.cartItemRepo.findOne({
                            _id: ObjectId(_id),
                        })
                        // if (!itemDB) {
                        //     reject(
                        //         new Error(req.__('order.product_not_in_cart'))
                        //     )
                        // }
                        // if (user_id !== userID) {
                        //     reject(
                        //         new Error(
                        //             req.__('order.not_owner_product_in_cart')
                        //         )
                        //     )
                        // }
                        if (variant_id && product_id) {
                            let variant:any, productToBuy:any
                            [variant, productToBuy] = await Promise.all([
                                (async () => {
                                    const data = await this.variantRepo.findWithProduct(
                                        variant_id,
                                        product_id
                                    )
                                    return data
                                })(),
                                (async () => {
                                    const product = await this.productRepo.findOne({ _id: ObjectId(product_id) })
                                    return product
                                })()
                            ])
                            if (orderRoomId) {
                                // group buy order must has exactly 1 quantity for the item
                                if(quantity > 1) {
                                    let message = req.__('order.invalid_quantity_for_group_buy_order')
                                    reject(
                                        new Error(
                                            message
                                        )
                                    )
                                }

                                const now = new Date().getTime();
                                const groupBuyStartTime = new Date(productToBuy.group_buy.start_time).getTime();
                                const groupBuyEndTime = new Date(productToBuy.group_buy.end_time).getTime();
                                if (!(groupBuyStartTime < now && groupBuyEndTime > now)) {
                                    let message = req.__('group_buy.invalid_group_buy', productToBuy.name)
                                    reject(
                                        new Error(
                                            message
                                        )
                                    )
                                }
                                const variantGroupBuy = productToBuy.group_buy.groups[0].variants.find(value => value.variant_id.toString() === variant_id.toString())
                                if ((variantGroupBuy.quantity - variantGroupBuy.sold) < 1) {
                                    let message = req.__('group_buy.sold_out_product', productToBuy.name)
                                    reject(
                                        new Error(
                                            message
                                        )
                                    )
                                }
                                if (orderRoomId !== "group_buy") {
                                    let room = productToBuy.order_rooms.find(value => value._id.toString() === orderRoomId.toString())
                                    if (!room) {
                                        let message = req.__('group_buy.not_exist_order_room', productToBuy.name)
                                        reject(
                                            new Error(
                                                message
                                            )
                                        )
                                    }
                                    else {
                                        if (room.status === "completed") {
                                            let message = req.__('group_buy.full_order_room', productToBuy.name)
                                            reject(
                                                new Error(
                                                    message
                                                )
                                            ) 
                                        }
                                        // check room & assign a new one if needed
                                        if(room.group_id.toString() !== groupID.toString()) {
                                            const productObj = cloneObj(productToBuy)
                                            const room_data = productObj.order_rooms.map(roomEle => {
                                                const number_of_member = productObj.group_buy.groups?.find(item => item._id.toString() === roomEle.group_id.toString())?.number_of_member || -1
                                                const percent = roomEle.members.length / number_of_member * 100
                                                const ele = {...roomEle, number_of_member, percent}
                                                return ele
                                            })
                                            room = room_data?.sort((x,y) => y.percent - x.percent)
                                                ?.filter(room => room.percent < 100 && room.status === 'pending')
                                                ?.find(room => room.group_id.toString() === groupID.toString())
                                            if(room) {
                                                req.body.order_room_id = room._id.toString()
                                            }
                                            else req.body.order_room_id = 'group_buy'
                                        }
                                    }
                                }
                                let data: any
                                if (isMappable(productToBuy.order_rooms)) {
                                    productToBuy.order_rooms.map((value) => {
                                        if (!data && (value.status === 'pending' || value.status === 'completed')) {
                                            data = value.members.find((item) => item._id.toString() === user_id.toString())
                                        }
                                    })
                                }
                                if (data) {
                                    let message = req.__('group_buy.only_one_purchase', productToBuy.name)
                                    reject(
                                        new Error(
                                            message
                                            )
                                    )
                                }
                            }
                            totalWeight += productToBuy.weight * quantity || 0
                            if (
                                !variant[0] ||
                                !variant[0].product ||
                                variant[0].quantity < quantity ||
                                variant[0].product.is_sold_out
                            ) {
                                let message = req.__('order.invalid_product_to_order', productToBuy.name)
                                reject(
                                    new Error(
                                        message
                                    )
                                )
                            }
                            else {
                                let variantObj = cloneObj(variant[0])
                                delete variantObj.product
                                dataItems.push({
                                    shop_id: shopID,
                                    variant: variantObj,
                                    product: productToBuy
                                })
                            }
                        } else if (product_id) {
                            const product = await this.productRepo.findProductForAddToCart(
                                product_id
                            )
                            const productObj = product[0]
                            if(orderRoomId) {
                                // group buy order must has exactly 1 quantity for the item
                                if(quantity > 1) {
                                    let message = req.__('order.invalid_quantity_for_group_buy_order')
                                    reject(
                                        new Error(
                                            message
                                        )
                                    )
                                }
                                const now = new Date().getTime()
                                const groupBuyStartTime = new Date(productObj.group_buy.start_time).getTime()
                                const groupBuyEndTime = new Date(productObj.group_buy.end_time).getTime()
                                if (!(groupBuyStartTime < now && groupBuyEndTime > now)) {
                                    let message = req.__('group_buy.invalid_group_buy', productObj.name)
                                    reject(
                                        new Error(
                                            message
                                        )
                                    )
                                }
                                if ((productObj.group_buy.quantity - productObj.group_buy.sold) < 1) {
                                    let message = req.__('group_buy.sold_out_product', productObj.name)
                                    reject(
                                        new Error(
                                            message
                                        )
                                    )
                                }
                                if (orderRoomId !== "group_buy") {
                                    let room = productObj.order_rooms.find(value => value._id.toString() === orderRoomId.toString())
                                    if (!room) {
                                        let message = req.__('group_buy.not_exist_order_room', productObj.name)
                                        reject(
                                            new Error(
                                                message
                                            )
                                        )
                                    }
                                    else {
                                        if (room.status === "completed") {
                                            let message = req.__('group_buy.full_order_room', productObj.name)
                                            reject(
                                                new Error(
                                                    message
                                                )
                                            ) 
                                        }

                                        // check room & assign a new one if needed
                                        if(room.group_id.toString() !== groupID.toString()) {
                                            const room_data = productObj.order_rooms.map(roomEle => {
                                                const number_of_member = productObj.group_buy.groups?.find(item => item._id.toString() === roomEle.group_id.toString())?.number_of_member || -1
                                                const percent = roomEle.members.length / number_of_member * 100
                                                const ele = {...roomEle, number_of_member, percent}
                                                return ele
                                            })
                                            room = room_data?.sort((x,y) => y.percent - x.percent)
                                                ?.filter(room => room.percent < 100 && room.status === 'pending')
                                                ?.find(room => room.group_id.toString() === groupID.toString())
                                            if(room) {
                                                req.body.order_room_id = room._id.toString()
                                            }
                                            else req.body.order_room_id = 'group_buy'
                                        }
                                    }
                                }
                                let data: any
                                if (isMappable(productObj.order_rooms)) {
                                    productObj.order_rooms.map(value => {
                                        if(!data && (value.status === "pending" || value.status === "completed")) {
                                            data = value.members.find(item => item._id.toString() === user_id.toString())
                                        }
                                    })
                                }
                                if (data) {
                                    let message = req.__('group_buy.only_one_purchase', productObj.name)
                                    reject(
                                        new Error(
                                            message
                                        )
                                    )
                                }
                            }
                            // const productToBuy: any = await this.productRepo.findOne({ _id: ObjectId(product_id) })
                            totalWeight += product[0].weight * quantity || 0
                            if (
                                !product[0] ||
                                product[0].variants.length > 0 ||
                                product[0].quantity < quantity ||
                                product[0].is_sold_out
                            ) {
                                let message = req.__('order.invalid_product_to_order', product[0].name)
                                reject(
                                    new Error(
                                        message
                                    )
                                )
                            }
                            else {
                                const productObj = cloneObj(product[0])
                                delete productObj.variants
                                dataItems.push({
                                    shop_id: shopID,
                                    variant: null,
                                    product: productObj
                                })
                            }
                        } else {
                            reject(
                                new Error(req.__('order.invalid_product_list'))
                            )
                        }
    
                        if (totalWeight > 20_000) {
                            reject(
                                new Error(req.__('order.limit_weight'))
                            )
                        }
                        resolve(item)
                    } catch (error) {
                        reject(error)
                    }
                })
            })

            return Promise.all(listPromise).then(data => {
                return {
                    success: true,
                    data,
                    dataItems
                }
            })
        } catch (error) {
            throw new Error(error.message)
        }
    }

    async getShopRanking() {
        // Only order with shipped and paid will be caculated
        return this.model.aggregate([
            {
                $match: {
                    shipping_status: "shipped",
                    payment_status: "paid"
                }
            },
            {
                $group: {
                    _id: "$shop_id",
                    totalRevenue: { $sum: "$total_value_items" }
                }
            },
            this.advanceShopQuery,
            {
                $unwind: {
                    path: "$shop",
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $sort: { totalRevenue: -1 }
            },
            {
                $limit: 10
            }
        ])
    }

    async getOrderWithOrderShipping(approvedTime) {
        let match: any = {
            shipping_status: ShippingStatus.WAIT_TO_PICK,
            approved_time: { $lte: approvedTime }
        }

        return await this.model.aggregate([
            {
                $match: match
            },
            this.advanceOrderShippingQuery,
            {
                $unwind: {
                    path: "$order_shipping",
                    preserveNullAndEmptyArrays: true
                }
            }
        ])
    }

    async getPreOrderWithOrderShipping() {
        let match: any = {
            shipping_status: ShippingStatus.WAIT_TO_PICK,
            assign_to_shipping_unit_time: null,
        }

        return await this.model.aggregate([
            this.advanceOrderShippingQuery,
            {
                $lookup: {
                    from: "orderitems",
                    let: {
                        order_id: "$_id",
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        {
                                            $eq: ["$order_id", "$$order_id"],
                                        },
                                        {
                                            $eq: ["$product.is_pre_order", true]
                                        }
                                    ],
                                },
                            },
                        }              
                    ],
                    as: "order_items"
                },
            },
            {
                $unwind: {
                    path: "$order_shipping",
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $addFields: {
                    assign_to_shipping_unit_time: "$order_shipping.assign_to_shipping_unit_time"
                }
            },
            {
                $match: match
            }

        ])
    }

    async getShopRankingForBuyer(page = 1, limit = 10) {
        // Only order with shipped and paid will be caculated
        const today = new Date(moment().format(REQ_DATE_TIME_FORMAT));
        const yesterday = new Date(moment().add(-1, "days").format(REQ_DATE_TIME_FORMAT));
        const yesterdayShopQuery = this.model.aggregate([
            {
                $match: {
                    shipping_status: "shipped",
                    payment_status: "paid",
                    updated_at: { $lt: yesterday }
                }
            },
            {
                $group: {
                    _id: "$shop_id",
                    totalRevenue: { $sum: "$total_value_items" }
                }
            },
            this.advanceShopQuery,
            {
                $unwind: {
                    path: "$shop",
                    preserveNullAndEmptyArrays: false
                }
            },
            {
                $sort: { totalRevenue: -1 }
            }
        ])
        const todayShopQuery = this.model.aggregate([
            {
                $match: {
                    shipping_status: "shipped",
                    payment_status: "paid",
                    updated_at: { $lt: today }
                }
            },
            {
                $group: {
                    _id: "$shop_id",
                    totalRevenue: { $sum: "$total_value_items" }
                }
            },
            this.advanceShopQuery,
            {
                $unwind: {
                    path: "$shop",
                    preserveNullAndEmptyArrays: false
                }
            },
            {
                $sort: { totalRevenue: -1 }
            }
        ])

        const resultYesterday = await this.model
            .aggregatePaginateCustom(yesterdayShopQuery, { page, limit })
        const resultToday = await this.model
            .aggregatePaginateCustom(todayShopQuery, { page, limit })

        const paginateYesterday = {
            limit: resultYesterday.limit,
            total_page: resultYesterday.totalPages,
            page: resultYesterday.page,
            total_record: resultYesterday.totalDocs
        }
        const paginateToday = {
            limit: resultToday.limit,
            total_page: resultToday.totalPages,
            page: resultToday.page,
            total_record: resultToday.totalDocs
        }

        const dataYesterday = resultYesterday.docs;
        const dataToday = resultToday.docs
        const data = {
            yesterdayShop: {shops: dataYesterday, paginateYesterday},
            todayShop: {shops: dataToday, paginateToday}
        }
        return data
    }

    async getShopRankingForBuyerPaginate(page: Number, limit: Number) {
        // Only order with shipped and paid will be calculated
        const today = new Date(moment().format(REQ_DATE_TIME_FORMAT));
        const yesterday = new Date(moment().add(-1, "days").format(REQ_DATE_TIME_FORMAT));
        const yesterdayShopQuery = this.model.aggregate([
            {
                $match: {
                    shipping_status: "shipped",
                    payment_status: "paid",
                    updated_at: { $lt: yesterday }
                }
            },
            {
                $group: {
                    _id: "$shop_id",
                    totalRevenue: { $sum: "$total_value_items" }
                }
            },
            this.advanceShopQuery,
            {
                $unwind: {
                    path: "$shop",
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $sort: { totalRevenue: -1 }
            }
        ])

        let yesterdayShop = await this.model.aggregatePaginateCustom(yesterdayShopQuery, {page, limit});
        yesterdayShop = cloneObj(yesterdayShop);

        const todayShopQuery = this.model.aggregate([
            {
                $match: {
                    shipping_status: "shipped",
                    payment_status: "paid",
                    updated_at: { $lt: today }
                }
            },
            {
                $group: {
                    _id: "$shop_id",
                    totalRevenue: { $sum: "$total_value_items" }
                }
            },
            this.advanceShopQuery,
            {
                $unwind: {
                    path: "$shop",
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $sort: { totalRevenue: -1 }
            }
        ])

        let todayShop = await this.model.aggregatePaginateCustom(todayShopQuery, {page, limit});
        todayShop = cloneObj(todayShop)

        const paginate = {
            limit: todayShop.limit ?? 10,
            total_page: todayShop.totalPages ?? 1,
            page: todayShop.page ?? 1,
            total_record: todayShop.totalDocs ?? 5
        }

        return [yesterdayShop.docs, todayShop.docs, paginate]
    }

    async getOrderSuccess(shopId = undefined) {
        let match: any = {
            shipping_status: "shipped",
            payment_status: "paid"
        }
        if (shopId) {
            match.shop_id = ObjectId(shopId);
        }
        return this.model.aggregate([
            {
                $match: match
            },
            { $project: { "_id": "$_id" } }
        ])
    }

    async getOrderByArrayId(Ids) {
        let objectIdArray = Ids.map(s => mongoose.Types.ObjectId(s));
        let match: any = {
            _id: { $in: objectIdArray }
        };
        return await this.model.aggregate([
            {
                $match: match
            },
            this.advanceOrderItemsQuery
        ]);
    }

    async countOrder(userId: string) {
        return await this.model.aggregate([
            {
                $match: {
                    user_id: ObjectId(userId)
                }
            },
            {
                $group: {
                    _id: "$shipping_status",
                    count: { $sum: 1 }
                }
            },
            // add more field have value 0
            {
                $group: {
                    _id: null,
                    data: { $push: "$$ROOT" }
                }
            },
            {
                $project: {
                    data: {
                        $map: {
                            input: listShippingStatus,
                            as: "shippingStatus",
                            in: {
                                $let: {
                                    vars: { index: { "$indexOfArray": ["$data._id", "$$shippingStatus"] } },
                                    in: {
                                        $cond: {
                                            if: { $ne: ["$$index", -1] },
                                            then: { $arrayElemAt: ["$data", "$$index"] },
                                            else: { _id: "$$shippingStatus", count: 0 }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            {
                $unwind: "$data"
            },
            {
                $replaceRoot: {
                    newRoot: "$data"
                }
            }
        ])
    }

    async getTimePrepareOrder(shop_id) {
        return this.model.aggregate([
            {
                $match: {
                    shop_id: ObjectId(shop_id),
                    shipping_status: {$in: [ShippingStatus.SHIPPING, ShippingStatus.SHIPPED, ShippingStatus.RETURN]}
                }
            },
            {
                $lookup: {
                    from: "ordershippings",
                    let: {
                        order_id: "$_id"
                    },
                    pipeline: [
                        {
                            $match: {
                                $and: [
                                    {
                                        '$expr': {
                                            '$eq': ['$order_id', '$$order_id']
                                        }
                                    }
                                ]
                            }
                        }
                    ],
                    as: "orders_shippings"
                }
            },
            {
                $unwind: {
                    path: "$orders_shippings",
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $project: {
                    day_prepare: {
                        $cond: {
                            if: {
                                $gt: [{
                                    $divide: [ { $subtract: ["$orders_shippings.assign_to_shipping_unit_time", "$created_at"] }, 1000*60*60*24]
                                } , 0]
                            },
                            then: {
                                $round: [ { $divide: [ { $subtract: ["$orders_shippings.assign_to_shipping_unit_time", "$created_at"] }, 1000*60*60*24] }, 0 ]
                            },
                            else: 0
                        }
                    }
                },
            },
            {
                $group: {
                    _id: "$day_prepare",
                    count: {$sum: 1}
                }
            },
            { 
                $project: {  
                    _id: 0,
                    day: "$_id",
                    count: "$count"
                }
            },
        ])
    }

    async getBudgetUsedOfVoucher(voucherID) {
        let orders = await this.model.aggregate([
            {
                $match: {
                    "vouchers": { $elemMatch: { "_id": voucherID.toString() } }
                }
            },
            {
                $project: {
                    vouchers: 1
                }
            }   
        ])
        orders = cloneObj(orders)
        const usedBudget = orders.reduce((total, item) => {
            const voucher = item.vouchers.find(item => item._id === voucherID.toString())
            return total += voucher.discount || 0
        }, 0)
        return usedBudget
    }

    async getOrderNotCanceledByUser(userID) {
        try {
            return this.model.aggregate([
                {
                    $match: {
                        user_id: ObjectId(userID),
                        shipping_status: {
                            $nin: [ShippingStatus.CANCELED]
                        }
                    }
                }
            ])
        } catch (error) {
            throw new Error(error)
        }
    }

    async findNumberOfBuyerByShop(shopID) {
        return this.model.aggregate([
            {
                $match: {
                    shipping_status: ShippingStatus.SHIPPED,
                    payment_status: PaymentStatus.PAID,
                    shop_id: shopID
                }
            },
            {
                $group: {
                    _id: "$user_id"
                }
            }
        ])
    }

    private advanceShopQuery: any = {
        $lookup:
        {
            from: "shops",
            let: {
                shop_id: "$_id"
            },
            pipeline: [
                {
                    $match: {
                        $and: [
                            { is_approved: true },
                            {
                                '$expr': {
                                    '$eq': ['$_id', '$$shop_id']
                                }
                            },
                            {
                                $or: [
                                    { deleted_at: { $exists: false } },
                                    { deleted_at: null }
                                ]
                            }
                        ]
                    }
                },
                {
                    $lookup: {
                        from: "users",
                        let: {
                            user_id: "$user_id"
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $eq: ["$_id", "$$user_id"]
                                    }
                                }
                            },
                            {
                                $lookup: {
                                    from: "galleryimages",
                                    let: {
                                        user_id: "$_id"
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                '$expr': {
                                                    $and: [
                                                        {
                                                            $eq: ["$galleryImageAbleId", "$$user_id"]
                                                        },
                                                        {
                                                            $eq: ["$galleryImageAbleType", "1"]
                                                        }
                                                    ]
                                                },
                                            }
                                        }
                                    ],
                                    as: "gallery_image"
                                }
                            },
                            {
                                $unwind: {
                                    path: "$gallery_image",
                                    preserveNullAndEmptyArrays: true
                                }
                            }
                        ],
                        as: "user"
                    }
                },
                {
                    $unwind: {
                        path: "$user",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $lookup: {
                        from: "feedbacks",
                        let: {
                            shop_id: "$_id"
                        },
                        pipeline: [
                            {
                                $match: {
                                    $and: [
                                        {
                                            '$expr': {
                                                $and: [
                                                    {
                                                        $eq: ["$shop_id", "$$shop_id"]
                                                    }
                                                ]
                                            },
                                        }
                                    ]
                                }
                            },
                            {
                                $group: {
                                    _id: "$shop_id",
                                    averageFeedbackRate: { $avg: "$vote_star" }
                                }
                            }
                        ],
                        as: "feedback"
                    },
                }
            ],
            as: "shop"
        }
    }

    private advanceShopOfOrderQuery: any = {
        $lookup:
        {
            from: "shops",
            let: {
                shop_id: "$shop_id"
            },
            pipeline: [
                {
                    $match: {
                        $and: [
                            {
                                '$expr': {
                                    '$eq': ['$_id', '$$shop_id']
                                }
                            },
                            {
                                $or: [
                                    { deleted_at: { $exists: false } },
                                    { deleted_at: null }
                                ]
                            }
                        ]
                    }
                },
                {
                    $lookup: {
                        from: "users",
                        let: {
                            user_id: "$user_id"
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $eq: ["$_id", "$$user_id"]
                                    }
                                }
                            },
                            {
                                $lookup: {
                                    from: "galleryimages",
                                    let: {
                                        user_id: "$_id"
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                '$expr': {
                                                    $and: [
                                                        {
                                                            $eq: ["$galleryImageAbleId", "$$user_id"]
                                                        },
                                                        {
                                                            $eq: ["$galleryImageAbleType", "1"]
                                                        }
                                                    ]
                                                },
                                            }
                                        }
                                    ],
                                    as: "gallery_image"
                                }
                            },
                            {
                                $unwind: {
                                    path: "$gallery_image",
                                    preserveNullAndEmptyArrays: true
                                }
                            }
                        ],
                        as: "user"
                    }
                },
                {
                    $unwind: {
                        path: "$user",
                        preserveNullAndEmptyArrays: true
                    }
                }
            ],
            as: "shop"
        }
    }

    private advanceUserQuery: any = {
        $lookup: {
            from: "users",
            let: {
                user_id: "$user_id",
                deleted_at: "$deleted_at",
            },
            pipeline: [
                {
                    $match: {
                        $and: [
                            {
                                $expr: {
                                    $and: [
                                        {
                                            $eq: ["$_id", "$$user_id"],
                                        },
                                    ],
                                },
                            },
                            {
                                $or: [
                                    { deleted_at: { $exists: false } },
                                    { deleted_at: null },
                                ],
                            },
                        ],
                    },
                },
                {
                    $lookup: {
                        from: "galleryimages",
                        let: {
                            user_id: "$_id",
                        },
                        pipeline: [
                            {
                                $match: {
                                    $and: [
                                        {
                                            $expr: {
                                                $and: [
                                                    {
                                                        $eq: [
                                                            "$galleryImageAbleId",
                                                            "$$user_id",
                                                        ],
                                                    },
                                                    {
                                                        $eq: [
                                                            "$galleryImageAbleType",
                                                            "1",
                                                        ],
                                                    },
                                                ],
                                            },
                                        },
                                    ],
                                },
                            },
                        ],
                        as: "gallery_image",
                    },
                },
                {
                    $unwind: {
                        path: "$gallery_image",
                        preserveNullAndEmptyArrays: true,
                    },
                },
            ],
            as: "user",
        },
    }

    private advanceShopOrderQuery: any = {
        $lookup:
        {
            from: "shops",
            let: {
                shop_id: "$shop_id"
            },
            pipeline: [
                {
                    $match: {
                        $and: [
                            {
                                '$expr': {
                                    '$eq': ['$_id', '$$shop_id']
                                }
                            },
                            {
                                $or: [
                                    { deleted_at: { $exists: false } },
                                    { deleted_at: null }
                                ]
                            }
                        ]
                    }
                },

            ],
            as: "shop"
        }
    }

    private advancePaymentMethodQuery: any = {
        $lookup: {
            from: "paymentmethods",
            let: {
                payment_method_id: "$payment_method_id",
            },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $and: [
                                {
                                    $eq: ["$_id", "$$payment_method_id"],
                                },
                            ],
                        },
                    },
                },
            ],
            as: "payment_method",
        },
    }

    private advanceShippingMethodQuery: any = {
        $lookup: {
            from: "shippingmethods",
            let: {
                shipping_method_id: "$shipping_method_id",
            },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $and: [
                                {
                                    $eq: ["$_id", "$$shipping_method_id"],
                                },
                            ],
                        },
                    },
                },
            ],
            as: "shipping_method",
        },
    }

    private advanceVoucherQuery: any = {
        $lookup: {
            from: "vouchers",
            let: {
                voucher_ids: "$vouchers",
            },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $and: [
                                {
                                    $in: ["$_id", "$$voucher_ids"],
                                },
                            ],
                        },
                    },
                },
            ],
            as: "vouchers",
        },
    }

    private basicOrderItemsQuery: any = {
        $lookup: {
            from: "orderitems",
            localField: '_id',
            foreignField: 'order_id',
            as: "order_items"
        },
    }

    private advanceOrderItemsQuery: any = {
        $lookup: {
            from: "orderitems",
            let: {
                order_id: "$_id",
            },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $and: [
                                {
                                    $eq: ["$order_id", "$$order_id"],
                                },
                            ],
                        },
                    },
                },
                {
                    $lookup: {
                        from: "feedbacks",
                        let: {
                            order_id: "$order_id",
                            product_id: "$product_id"
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            {
                                                $eq: ["$order_id", "$$order_id"],
                                            },
                                            {
                                                $eq: ["$target_id", "$$product_id"]
                                            }
                                        ],
                                    },
                                }
                            }
                        ],
                        as: "feedbacks"
                    }
                },
                { $addFields: { is_feedback: { $cond: { if: { $gt: [{ $size: "$feedbacks" }, 0] }, then: true, else: false } } } },
                {
                    $addFields:
                    {
                        feedback: { $ifNull: [{ $arrayElemAt: ["$feedbacks", 0] }, null] }
                    }
                },
                { $unset: "feedbacks" }
            ],
            as: "order_items"
        },
    }

    findOrderByOrderNumber = async (item) => {
        const result = await this.model.findOne(item);
        return result;
    }

    private advanceOrderShippingQuery: any = {
        $lookup: {
            from: "ordershippings",
            let: {
                order_id: "$_id",
            },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $and: [
                                {
                                    $eq: ["$order_id", "$$order_id"],
                                },
                            ],
                        },
                    },
                },
            ],
            as: "order_shipping"
        }
    }

    private advanceShopFeedbackQuery: any = {
        $lookup: {
            from: "feedbacks",
            let: {
                order_id: "$_id",
                target_id: "$user_id"
            },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $and: [
                                {
                                    $eq: ["$order_id", "$$order_id"],
                                },
                                {
                                    $eq: ["$target_id", "$$target_id"]
                                },
                                {
                                    $eq: ["$target_type", "User"]
                                }
                            ],
                        },
                    }
                }
            ],
            as: "feedbacks"
        }
    }

    private advanceGroupOrderQuery: any = {
        $lookup: {
            from: "grouporders",
            let: {
                group_order_id: "$group_order_id",
                order_id: "$_id"
            },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $and: [
                                {
                                    $eq: ["$_id", "$$group_order_id"],
                                },
                            ],
                        },
                    },
                },
                {
                    $project: {
                        _id: 0,
                        orders: "$orders"
                    }
                }
            ],
            as: "group_orders"
        },
    }

    public async getOrdersByCondition(query: Array<any> = []) {
        const result = await this.model.find({ $and: query })
        return result;
    }

    public async getOrdersExport(start_time: any, end_time: any, shippingStatus: any = null) {
        const query = [
            {
                created_at: {
                    $gte: start_time,
                    $lte: end_time,
                },
            },
            {
                $or: [
                    {
                        deleted_at: { $exists: false },
                    },
                    {
                        deleted_at: null,
                    },
                ],
            },
        ];

        if (shippingStatus) {
            query.push(shippingStatus);
        }

        return this.model.aggregate([
            {
                $match: {
                    $and: query,
                },
            },
            {
                $lookup: {
                    from: 'shops',
                    as: 'shop',
                    let: { shopId: '$shop_id' },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $eq: ['$_id', '$$shopId'],
                                },
                            },
                        },
                        {
                            $lookup: {
                                from: 'users',
                                as: 'user',
                                let: { userId: '$user_id' },
                                pipeline: [
                                    {
                                        $match: {
                                            $expr: {
                                                $eq: ['$_id', '$$userId'],
                                            },
                                        },
                                    },
                                ],
                            },
                        },
                        {
                            $unwind: {
                                path: '$user',
                                preserveNullAndEmptyArrays: true,
                            },
                        },
                    ],
                },
            },
            {
                $unwind: {
                    path: '$shop',
                    preserveNullAndEmptyArrays: true,
                },
            },
            {
                $lookup: {
                    from: 'ordershippings',
                    as: 'order_shipping',
                    let: { orderId: '$_id' },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $eq: ['$order_id', '$$orderId'],
                                },
                            },
                        },
                    ],
                },
            },
            {
                $unwind: {
                    path: '$order_shipping',
                    preserveNullAndEmptyArrays: true,
                },
            },
            {
                $lookup: {
                    from: 'users',
                    as: 'user',
                    let: { userId: '$user_id' },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $eq: ['$_id', '$$userId'],
                                },
                            },
                        },
                    ],
                },
            },
            {
                $unwind: {
                    path: '$user',
                    preserveNullAndEmptyArrays: true,
                },
            },
            {
                $project: {
                    _id: { $toString: '$_id' },
                    shop_id: { $toString: '$shop_id' },
                    shop_name: {
                        $ifNull: ['$shop.user.nameOrganizer.userName', ''],
                    },
                    order_number: 1,
                    code_shipping: {
                        $ifNull: ['$order_shipping.shipping_info.label', ''],
                    },
                    created_at: {
                        $dateToString: {
                            format: '%Y-%m-%d %H:%M:%S',
                            date: '$created_at',
                        },
                    },
                    user_id: { $toString: '$user_id' },
                    user_name: {
                        $ifNull: ['$user.nameOrganizer.userName', ''],
                    },
                    total_value_items: 1,
                    shipping_fee: 1,
                    shipping_discount: 1,
                    total_price: 1,
                    vouchers: 1,
                    updated_at: {
                        $ifNull: [
                            {
                                $dateToString: {
                                    format: '%Y-%m-%d %H:%M:%S',
                                    date: '$updated_at',
                                },
                            },
                            ""
                        ]
                    },
                    shipping_status: 1,
                    history: {
                        $filter: {
                            input: "$order_shipping.history",
                            as: "item",
                            cond: { $eq: ["$$item.status_id", 5] }      
                        }   
                    },
                },
            },
        ]);
    }

    public async getOrdersByCategory(query: Array<any> = []) {
        try {
            const result = await this.model.aggregate([
                {
                    $match: {
                        $and: query
                    }
                },
                {
                    $lookup: {
                        from: "orderitems",
                        let: { orderId: "$_id" },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $eq: ["$order_id", "$$orderId"]
                                    }
                                }
                            },
                            {
                                $lookup: {
                                    from: "products",
                                    let: { productId: "$product_id" },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr: {
                                                    $eq: ["$_id", "$$productId"]
                                                }
                                            }
                                        },
                                        {
                                            $lookup: {
                                                from: "ecategories",
                                                let: {
                                                    categoriesId: {
                                                        $toObjectId: { $arrayElemAt: ['$list_category_id', 0] },
                                                    },
                                                },
                                                pipeline: [
                                                    {
                                                        $match: {
                                                            $expr: {
                                                                $and: [
                                                                    {
                                                                        $eq: ["$_id", "$$categoriesId"]
                                                                    },
                                                                    {
                                                                        $eq: ["$is_active", true]
                                                                    },
                                                                    {
                                                                        $eq: ["$parent_id", null]
                                                                    }
                                                                ]
                                                            }
                                                        }
                                                    },
                                                ],
                                                as: "category"
                                            },
                                        },
                                        {
                                            $unwind: "$category"
                                        }
                                    ],
                                    as: "product"
                                },
                            },
                            {
                                $unwind: "$product"
                            }
                        ],
                        as: "order_items"
                    }
                },
                {
                    $project: {
                        _id: { $toString: '$_id' },
                        shop_id: { $toString: '$shop_id' },
                        order_number: 1,
                        order_items: 1,
                        created_at: {
                            $dateToString: {
                                format: '%Y-%m-%d %H:%M:%S',
                                date: '$created_at',
                            },
                        },
                        user_id: { $toString: '$user_id' },
                        user_name: {
                            $ifNull: ['$user.nameOrganizer.userName', ''],
                        },
                        total_value_items: 1,
                        shipping_fee: 1,
                        shipping_discount: 1,
                        total_price: 1,
                        vouchers: 1,
                        updated_at: {
                            $ifNull: [
                                {
                                    $dateToString: {
                                        format: '%Y-%m-%d %H:%M:%S',
                                        date: '$updated_at',
                                    },
                                },
                                ""
                            ]
                        },
                        shipping_status: 1,
                    },
                },
            ]);
            return result;
        } catch (error) {
            console.log(error);
        }
    }

    public async getOrdersByShop(query: Array<any> = []) {
        const result = await this.model.aggregate([
            {
                $match: {
                    $and: query
                }
            },
            {
                $lookup: {
                    from: "shops",
                    let: { shopId: "$shop_id" },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $eq: ["$_id", "$$shopId"]
                                }
                            }
                        },
                    ],
                    as: "shop"
                }
            },
            {
                $unwind: "$shop"
            },
        ])
        return result;
    }

    public async getOrdersAndTotalPriceByCondition(query: Array<any> = []) {
        const result = await this.model.aggregate([
            {
                $match: {
                    $and: query
                }
            },
            {
                $facet: {
                    orders: [],
                    total: [
                        {
                            $group: {
                                _id: '',
                                "sum": { $sum: '$total_price' }
                            }
                        },
                        {
                            $project: {
                                _id: 0,
                                "sum": '$sum'
                            }
                        }
                    ]
                },
            },
            {
                $unwind: '$total'
            }
        ])
        return result;
    }

    async getTransactionForAccountant({
        dateStart,
        dateEnd,
        orderNumber,
        shopId,
        filterType
    }) {
        let orders = await this._getOrderWithFilterForAccountant({
            dateStart,
            dateEnd,
            orderNumber,
            shopId,
            filterType
        });
        const promises = orders.map(order => {
            return new Promise(async (resolve, reject) => {
                order = cloneObj(order)
                order = await OrderDTO.newInstance(order).completeDataPaymentAndShippingInfo()
                resolve(order)
            })
        })

        const result = await Promise.all(promises);
        return result;
    }

    async _getOrderWithFilterForAccountant({
        dateStart,
        dateEnd,
        orderNumber,
        shopId,
        filterType
    }) {
        let match: any = {
            $or: [{ deleted_at: { $exists: false } }, { deleted_at: null }],
        }
        if (dateStart && dateEnd) {
            match = {
                $or: [{ deleted_at: { $exists: false } }, { deleted_at: null }],
                $and: [
                    { created_at: { $gte: dateStart } },
                    { created_at: { $lte: dateEnd } },
                ],
            }
        } else {
            if (dateStart) {
                match.created_at = { $gte: dateStart }
            }
            if (dateEnd) {
                match.created_at = { $lte: dateEnd }
            }
        }
        if (shopId) {
            match.shop_id = ObjectId(shopId)
        }
        
        if (filterType) {
            if (filterType.toLowerCase() === "cancel") {
                match.shipping_status = { $in: ["canceling", "canceled"] }
            }
            else if (filterType.toLowerCase() === "return") {
                match.shipping_status = { $in: ["return"] }
            }
            else if (filterType.toLowerCase() === "returning") {
                match.shipping_status = { $in: ["return"] }
                match.refund_information = { $exists: true, $ne: [] }
                match["refund_information.status"] = { $in: ["opening"] }
            }
            else if (filterType.toLowerCase() === "returned") {
                match.shipping_status = { $in: ["return"] }
                match.refund_information = { $exists: true, $ne: [] }
                match["refund_information.status"] = { $in: ["canceled", "closed"] }
            }
            else {
                match.shipping_status = filterType.toLowerCase()
            }
        }

        if (orderNumber) {
            match.order_number = { $regex: `${orderNumber}`, $options: "i" }
        }
        const matchShippingStatus = [
            GHTKStatusAction.DELIVERIED,
            GHTKStatusAction.CHECKED,
            GHTKStatusAction.CANCELED,
            GHTKStatusAction.CHECKED_AND_RETURNED,
            GHTKStatusAction.RETURNED_TO_SHOP,
            GHTKStatusAction.RETURN_TO_SHOP,
        ]
        const aggregate = this.model.aggregate([
            this.advanceOrderShippingQuery,
            {
                $unwind: {
                    path: "$order_shipping",
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $match: {
                    $or: [
                        {
                            'order_shipping.history': { 
                                $elemMatch: { 
                                    $or: [
                                        {
                                            status_id: {
                                                $in: matchShippingStatus,
                                            },
                                            action_time: {
                                                $gte: dateStart,
                                                $lt: dateEnd   
                                            }
                                        },
                                    ]
                                }
                            },
                        }
                    ]
                }
            },
            {
                $addFields: {
                    payment_method: {
                        $arrayElemAt: ["$payment_method", 0]
                    },
                    shipping_method: {
                        $arrayElemAt: ["$shipping_method", 0]
                    },
                },
            },
            {
                $sort: { created_at: -1 },
            },
            this.advanceUserQuery,
            {
                $addFields: {
                    user: {
                        $arrayElemAt: ["$user", 0]
                    },
                },
            },
            this.advancePaymentMethodQuery,
            this.advanceShippingMethodQuery,
            // this.advanceVoucherQuery,
            this.advanceOrderItemsQuery,
            this.advanceShopOfOrderQuery,
            {
                $unwind: {
                    path: "$shop",
                    preserveNullAndEmptyArrays: true
                }
            },
        ])
        return aggregate;
    }
}