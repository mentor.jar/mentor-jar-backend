import { BaseRepository } from "./BaseRepository";
import { DataCityDoc } from "../interfaces/DataCity";
import DataCity from "../models/DataCity"
import { DataCityDTO } from "../DTO/DataCityDTO";
import { QueryDBError } from "../base/customError";

// now, we have all code implementation from BaseRepository
export class DataCityRepo extends BaseRepository<DataCityDoc>  {

    private static instance: DataCityRepo;

    private constructor() {
        super();
        this.model = DataCity;
    }

    public static getInstance(): DataCityRepo {
        if (!DataCityRepo.instance) {
            DataCityRepo.instance = new DataCityRepo();
        }
        return DataCityRepo.instance;
    }

    async findGetByName() {
        let result = await this.find({});
        return result.map(element => {
            return (new DataCityDTO(element)).toSimpleJSON();
        })
    }

    async getAddressData(stateId, districtId) {
        let match: any = {}
        let result;
        // if stateId exist, get list district by stateId otherwise get list state
        if (stateId) {
            match.Id = stateId
            result = await this.findOne(match)
            // if districtId exist, get list ward by districtId otherwise get list district
            if (districtId) {
                let district = result.Districts.find(element => element.Id == districtId)
                if (!district)
                    throw new QueryDBError('Không tồn tại quận/huyện này')
                return district.Wards.map(element => {
                    return (new DataCityDTO(element)).toSimpleJSON()
                })
            } else {
                if (!result)
                    throw new QueryDBError('Không tồn tại tỉnh/Thành phố này')
                let listDistrict = result.Districts.map(element => {
                    return (new DataCityDTO(element)).toJSON(['Id', 'Name'])
                })
                return listDistrict
            }
        }
        result = await this.find(match)
        return result.map(element => {
            return (new DataCityDTO(element)).toSimpleJSON()
        })
    }

    async getAddressByStateId({
        stateId,
        districtId,
        wardsId,
        isGetState = false,
        isGetDistrict = false,
        isGetWards = false,
    }: any) {
        let match: any = {
            Id: stateId,
        };
        let result = null,
            district: any = null,
            wards: any = null;
        let state: any = await this.findOne(match);

        if (isGetState && stateId) {
            return new DataCityDTO(state).toJSONWithFullDistrict();
        }

        district = state.Districts.find((element) => element.Id == districtId);
        if (isGetDistrict && districtId) {
            return new DataCityDTO(district).toSimpleJSON();
        }

        wards = district.Wards.find((element) => element.Id == wardsId);

        if (isGetWards && wardsId) {
            return new DataCityDTO(wards).toSimpleJSON();
        }

        result = new DataCityDTO(result).toSimpleJSON();
        return;
    }
}