import { BaseRepository } from "./BaseRepository";
import Role from "../models/Role"
import { RoleDoc } from "../interfaces/Role";
import { ParamCreateProcess, TrackingJobProcessDoc } from "../interfaces/TrackingProcessJob";
import TrackingJobProcess from "../models/TrackingProcessJob";
import { RuleError } from "../base/customError";
import { ObjectId } from "mongodb";

export class TrackingJobProcessRepo extends BaseRepository<TrackingJobProcessDoc> {

    private static instance: TrackingJobProcessRepo;

    private constructor() {
        super();
        this.model = TrackingJobProcess;
    }

    public static getInstance(): TrackingJobProcessRepo {
        if (!TrackingJobProcessRepo.instance) {
            TrackingJobProcessRepo.instance = new TrackingJobProcessRepo();
        }

        return TrackingJobProcessRepo.instance;
    }

    async createProcess(params: ParamCreateProcess, only = true) {
        let flagProcessDoing = await this.checkProcessDoing(params.name);
        if (flagProcessDoing) throw new RuleError("The process is not finished yet");
        if (only) {
            let process: any = await this.findOne({ name: params.name })
            if (process) {
                return await this.update(process._id.toString(), {
                    name: params.name,
                    doing: params.doing,
                    target: params.target,
                    data_detail: params.data_detail,
                    status: 'DOING'
                })
            }
        }
        return await this.create({
            name: params.name,
            doing: params.doing,
            target: params.target,
            data_detail: params.data_detail,
            status: 'DOING'
        })
    }

    async updateProcessIncrease(name, data_detail) {
        let flagProcessDoing = await this.checkProcessDoing(name);
        if (!flagProcessDoing) return
        let process: any = await this.findOne({ name, status: 'DOING' })
        process.doing++
        process.status = process.doing < process.target ? 'DOING' : 'DONE';
        let data_update: any = {
            doing: process.doing,
            status: process.status,
        }
        if (data_detail) {
            data_update.data_detail = data_detail
        }
        return this.update(process._id.toString(), data_update)
    }

    async updateProcess(name, doing, data_detail) {
        let flagProcessDoing = await this.checkProcessDoing(name);
        if (!flagProcessDoing) return
        let process: any = await this.findOne({ name, status: 'DOING' })
        process.status = doing < process.target ? 'DOING' : 'DONE';
        let data_update: any = {
            doing: doing,
            status: process.status,
        }
        if (data_detail) {
            data_update.data_detail = data_detail
        }
        return this.update(process._id.toString(), data_update)
    }

    async checkProcessDoing(name) {
        let dataFlag = await this.findOne({ name, status: 'DOING' });
        return dataFlag ? true : false
    }
}