import { BaseRepository } from "./BaseRepository";
import FollowShop from "../models/Follow_Shop";
import { FollowShopDoc } from "../interfaces/Follow_Shop";
export class FollowShopRepository extends BaseRepository<FollowShopDoc>  {

    private static instance: FollowShopRepository;

    constructor() {
        super();
        this.model = FollowShop;
    }

    public static getInstance(): FollowShopRepository {
        if (!FollowShopRepository.instance) {
            FollowShopRepository.instance = new FollowShopRepository();
        }

        return FollowShopRepository.instance;
    }

    async checkFollow(shopId, userId) {
        const follow = await this.model.findOne({ user_id: userId, shop_id: shopId });
        return follow ? true : false;
    }
}