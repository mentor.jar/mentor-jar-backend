import { BaseRepository } from "./BaseRepository";
import OrderShipping from "../models/Order_Shipping";
import { OrderShippingDoc } from "../interfaces/Order_Shipping";
import { NotFoundError } from "../base/customError";
import GHTKService from "../services/3rd/shippings/GHTK";
import { ShippingStatus, GHTKStatusAction, PaymentStatus } from "../models/enums/order";
import { OrderRepository } from "./OrderRepo";
import moment from "moment";
import { updateOrderLogChannel } from "../base/log";
export class OrderShippingRepo extends BaseRepository<OrderShippingDoc>  {

    private static instance: OrderShippingRepo;

    constructor() {
        super();
        this.model = OrderShipping;
    }

    public static getInstance(): OrderShippingRepo {
        if (!OrderShippingRepo.instance) {
            OrderShippingRepo.instance = new OrderShippingRepo();
        }

        return OrderShippingRepo.instance;
    }

    updateOrderShipping = async (orderId, item) => {
        let data = await this.model.findOne({ order_id: orderId });
        if (!data) {
            throw new NotFoundError('Không tìm thấy OrderShipping.');
        }
        data.shipping_info = item;
        data.assign_to_shipping_unit_time = new Date();
        await data.save();
        return data;
    }

    findOrderShipping = async (query) => {
        const result = await this.model.findOne(query);
        // if (!result) throw new NotFoundError('Không tìm thấy OrderShipping.');
        return result;
    }

    getShippingHistoryByOrderId = async (orderId) => {
        const orderShipping = await this.findOrderShipping({ order_id: orderId });
        return orderShipping;
    }

    updateTimeUserConfirmShipping = async (orderId) => {
        const orderRepo = await OrderRepository.getInstance();
        let order = await orderRepo.findById(orderId);
        if (order.shipping_status === ShippingStatus.SHIPPING) {
            order.shipping_status = ShippingStatus.SHIPPED;
            await order.save();
            this.orderLogResponseInfo(order);
        }
    }

    updatePaymentSatatusShipping = async (orderId) => {
        const orderRepo = await OrderRepository.getInstance();
        let order = await orderRepo.findById(orderId);
        if (order.payment_status === PaymentStatus.PENDING) {
            order.payment_status = PaymentStatus.PAID;
            await order.save();
            this.orderLogResponseInfo(order);
        }
    }

    getOrderShippedInHistory = async () => {
        const dayAgo = moment().subtract(3, 'days') 
        let query = { 
            shipping_info: { $ne: null },
            $or: [ 
                {
                    $and: [
                        { 'history.status_id': 5 },
                        { 'history.action_time': { $lte: dayAgo.toDate() }},
                    ]
                },
                {
                    $and: [
                        { 'history.status_id': 6 },
                        { 'history.action_time': { $lte: dayAgo.toDate() }}
                    ]
                },
            ],
            user_confirm_shipped_time: { $eq: null }

        }
        const orderShipping = await this.model.find(query);
        return orderShipping;
    }

    getOrderShippedAndPaidInHistory = async () => {
        const dayAgo = moment().subtract(3, 'days') 
        let query = { 
            shipping_info: { $ne: null },
            $and: [
                { 'history.status_id': 6 },
                { 'history.action_time': { $lte: dayAgo.toDate() }}
            ],
            user_confirm_shipped_time: { $eq: null }
        }
        const orderShipping = await this.model.find(query);
        return orderShipping;
    }

    updateOrderAndOrderShippedHistory = async () => {
        const ordersShipping = await this.getOrderShippedInHistory();
        const ordersPaid = await this.getOrderShippedAndPaidInHistory();

        let orderIds = ordersShipping.map(item => item.order_id);
        let orderIdPaids = ordersPaid.map(item => item.order_id);

        // update confirm shipped time
        ordersShipping.map(item => {
            item.user_confirm_shipped_time = new Date();
            item.save();
        })
        // update order status
        orderIds.map(async (id) => {
            await this.updateTimeUserConfirmShipping(id);
            return;
        })
        // update payment status
        orderIdPaids.map(async (id) => {
            await this.updatePaymentSatatusShipping(id);
            return; 
        })
    }

    getOrderGHTKCannotPickUp = async () => {
        let arrReceive = [ GHTKStatusAction.ASSIGN, GHTKStatusAction.PICKED_UP, GHTKStatusAction.DELAY_PICK_UP ];
        let arrNotReceive = [ GHTKStatusAction.ASSIGN, GHTKStatusAction.RECEIVED ];
        const dayAgo = moment().subtract(1, 'days') 
        let query =  {
            $or: [
                { 
                    shipping_info: { $ne: null },
                    $and: [
                        { 'history.status_id': { $nin: arrReceive }},
                    ],
                    user_confirm_shipped_time: { $eq: null },
                    assign_to_shipping_unit_time: { $lte: dayAgo.toDate() },
                    'shipping_info.status_id': { $eq: GHTKStatusAction.RECEIVED }
                },
                { 
                    shipping_info: { $ne: null },
                    $and: [
                        { 'history.status_id': { $nin: arrNotReceive }},
                    ],
                    user_confirm_shipped_time: { $eq: null },
                    assign_to_shipping_unit_time: { $lte: dayAgo.toDate() },
                    'shipping_info.status_id': { $eq: GHTKStatusAction.NOT_RECEIVED }
                }
            ]
        }
        const orderShipping = await this.model.find(query);
        return orderShipping;
    }


    orderLogResponseInfo = (response) => {
        updateOrderLogChannel.info("-----*----")
        updateOrderLogChannel.info("Response: ", response)
        updateOrderLogChannel.info("-----*----")
    }
}


