import { CategoryInfoDoc } from "../interfaces/Category_Info";
import { BaseRepository } from "./BaseRepository";
import { ICategoryInfoRepo } from "./interfaces/ICategoryInfoRepo";
import Category_Info from "../models/Category_Info"
import { CategoryInfoDTO } from "../DTO/CategoryInfoDTO";
import { cloneObj } from "../utils/helper";
import { ProductDetailInfoRepo } from "../repositories/ProductDetailInfo";
import { QueryDBError } from "../base/customError";
import { ObjectId } from '../utils/helper';
import { FASHION_TYPE_OTHER } from "../base/variable";

export class CategoryInfoRepo extends BaseRepository<CategoryInfoDoc> implements ICategoryInfoRepo {

    private static instance: CategoryInfoRepo;

    private constructor() {
        super();
        this.model = Category_Info;
    }

    public static getInstance(): CategoryInfoRepo {
        if (!CategoryInfoRepo.instance) {
            CategoryInfoRepo.instance = new CategoryInfoRepo();
        }

        return CategoryInfoRepo.instance;
    }

    async getByCategoryId(categoryId: string): Promise<any> {
        const result = await this.model.find({ category_id: categoryId });
        let data = cloneObj(result)
        return data.map(el => (new CategoryInfoDTO(el)).toSimpleJSON());
    }

    async getByCategoryIdAndName(categoryId: string, name: string): Promise<any> {
        let result = await this.model.findOne({ category_id: ObjectId(categoryId), "name.vi": name });
        if(!result) {
            result = await this.model.findOne({ category_id: ObjectId(categoryId), name: name });
        }
        return result == null ? FASHION_TYPE_OTHER : result.fashion_type;
    }

    async deleteCategoriesInfo(allCategoryId: Array<String>) {
        let productDetailInfoRepo = ProductDetailInfoRepo.getInstance();
        //return this.model.find({ category_id: allCategoryId }).deleteMany();
        try {
            const categoryInfoId: any = [];
            await this.model.find({ category_id: allCategoryId }).then((result) => {
                result.forEach((e) => {
                    categoryInfoId.push(e._id);
                    e.delete();
                });
            });
            await productDetailInfoRepo.deleteByCategoryInfo(categoryInfoId);
        } catch (error) {
            throw new QueryDBError(error.message);
        }
    }
}