import { BaseRepository } from "./BaseRepository";
import ReportLivestream from "../models/Report_Livestream";
import { ReportLivestreamDoc } from "../interfaces/Report_Livestream";

export class ReportLivestreamRepository extends BaseRepository<ReportLivestreamDoc> {

    private static instance: ReportLivestreamRepository;

    private constructor() {
        super();
        this.model = ReportLivestream;
    }

    public static getInstance(): ReportLivestreamRepository {
        if (!ReportLivestreamRepository.instance) {
            ReportLivestreamRepository.instance = new ReportLivestreamRepository();
        }

        return ReportLivestreamRepository.instance;
    }

    async getListReport(limit = 10, page = 1) {
        const match: any = {
        }

        const query = this.model.aggregate([
            {
                $match: match
            },
            {
                $sort: { created_at: -1 }
            },
            this.advanceUserQuery,
            this.advanceOwnerQuery,
            this.advanceStreamQuery,
            {
                $unwind: {
                    path: "$user_report",
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $unwind: {
                    path: "$owner",
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $unwind: {
                    path: "$stream_session",
                    preserveNullAndEmptyArrays: true
                }
            }
        ]);
        const result = await this.model
            .aggregatePaginateCustom(query, { page, limit })

        const paginate = {
            limit: result.limit,
            total_page: result.totalPages,
            page: result.page,
            total_record: result.totalDocs
        }
        const data = result.docs;
        return { data, paginate }
    }

    private advanceUserQuery: any = {
        $lookup:
        {
            from: "users",
            let: {
                user_id: "$user_report_id",
            },
            pipeline: [
                {
                    $match: {
                        $and: [
                            {
                                '$expr': {
                                    $and: [
                                        {
                                            '$eq': ['$_id', '$$user_id']
                                        }
                                    ]
                                },
                            }
                        ]
                    }
                },
                {
                    $lookup: {
                        from: "galleryimages",
                        let: {
                            user_id: "$_id"
                        },
                        pipeline: [
                            {
                                $match: {
                                    '$expr': {
                                        $and: [
                                            {
                                                $eq: ["$galleryImageAbleId", "$$user_id"]
                                            },
                                            {
                                                $eq: ["$galleryImageAbleType", "1"]
                                            }
                                        ]
                                    },
                                }
                            }
                        ],
                        as: "gallery_image"
                    }
                },
                {
                    $unwind: {
                        path: "$gallery_image",
                        preserveNullAndEmptyArrays: true
                    }
                }
            ],
            as: "user_report"
        }
    }

    private advanceOwnerQuery: any = {
        $lookup:
        {
            from: "users",
            let: {
                user_id: "$owner_id",
            },
            pipeline: [
                {
                    $match: {
                        $and: [
                            {
                                '$expr': {
                                    $and: [
                                        {
                                            '$eq': ['$_id', '$$user_id']
                                        }
                                    ]
                                },
                            }
                        ]
                    }
                },
                {
                    $lookup: {
                        from: "galleryimages",
                        let: {
                            user_id: "$_id"
                        },
                        pipeline: [
                            {
                                $match: {
                                    '$expr': {
                                        $and: [
                                            {
                                                $eq: ["$galleryImageAbleId", "$$user_id"]
                                            },
                                            {
                                                $eq: ["$galleryImageAbleType", "1"]
                                            }
                                        ]
                                    },
                                }
                            }
                        ],
                        as: "gallery_image"
                    }
                },
                {
                    $unwind: {
                        path: "$gallery_image",
                        preserveNullAndEmptyArrays: true
                    }
                }
            ],
            as: "owner"
        }
    }

    private advanceStreamQuery: any = {
        $lookup:
        {
            from: "streamsessions",
            let: {
                stream_session_id: "$stream_session_id",
            },
            pipeline: [
                {
                    $match: {
                        $and: [
                            {
                                '$expr': {
                                    $and: [
                                        {
                                            '$eq': ['$_id', '$$stream_session_id']
                                        }
                                    ]
                                },
                            }
                        ]
                    }
                }
            ],
            as: "stream_session"
        }
    }

}