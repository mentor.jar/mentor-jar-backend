import { BaseRepository } from "./BaseRepository";
import ProductCategory from "../models/Product_Category";
import { ProductCategoryDoc } from "../interfaces/Product_Category";

export class ProductCategoryRepo extends BaseRepository<ProductCategoryDoc>  {

    private static instance: ProductCategoryRepo;

    constructor() {
        super();
        this.model = ProductCategory;
    }

    public static getInstance(): ProductCategoryRepo {
        if (!ProductCategoryRepo.instance) {
            ProductCategoryRepo.instance = new ProductCategoryRepo();
        }

        return ProductCategoryRepo.instance;
    }

    deleteByCategoryId(categoryId: String) {
        return this.model.find({ category_id: categoryId }).deleteMany();
    }

    async createProductCategoryRecord(categoryId: string, product_ids: Array<string>) {
        let promiseRes: any = product_ids.map(productId => {
            return new Promise((resolve, reject) => {
                const item = this.model.create({
                    category_id: categoryId,
                    product_id: productId
                })
                resolve(item)
            })
        });

        return Promise.all(promiseRes)
    }

    async updateProductCategoryRecord(categoryId: string, newProductList: Array<string>) {
        let oldProductList = await this.model.find({ category_id: categoryId }, { product_id: 1 })
        oldProductList = oldProductList.map(e => e.product_id.toString())

        const deleteIds = oldProductList.filter(e => newProductList.indexOf(e) < 0)
        const createIds = newProductList.filter(e => oldProductList.indexOf(e) < 0)

        if (deleteIds.length) {
            const deleteItems = await this.model.find({ product_id: { $in: deleteIds }, category_id: categoryId }).deleteMany();
        }

        if (createIds.length) {
            const createItem = await this.createProductCategoryRecord(categoryId, createIds)
        }
    }

    async getProductIdList(categoryId: string) {
        const listItems = await this.model.find(
            { 
                category_id: categoryId, 
                deleted_at: null
            }, 
            { product_id: 1 })
        return listItems.map(e => e.product_id.toString())
    }

}