import { BaseRepository } from "./BaseRepository";
import Address from "../models/Address"
import { AddressDoc } from "../interfaces/Address";
import { ObjectId } from "../utils/helper";
import { DataCityRepo } from "./DataCityRepo";


export class AddressRepo extends BaseRepository<AddressDoc> {

    private static instance: AddressRepo;

    private dataCityRepo = DataCityRepo.getInstance();

    private constructor() {
        super();
        this.model = Address;
    }

    public static getInstance(): AddressRepo {
        if (!AddressRepo.instance) {
            AddressRepo.instance = new AddressRepo();
        }

        return AddressRepo.instance;
    }

    async getDefaultAddress(userId: string) {
        const address = await this.model.findOne({ accessible_id: userId, accessible_type: "User", is_default: true });
        return address;
    }

    async getAddressByUser(userId: string) {
        const address = await this.model.find({ accessible_id: userId, accessible_type: "User" });
        return address;
    }

    async updateIsDefault(address) {
        const updateAdd = await this.model
            .findOne({ _id: { $ne: address._id }, accessible_id: address.accessible_id, accessible_type: "User", is_default: true })
            .updateOne({ is_default: false });
        return updateAdd
    }

    async updateIsPickAddressDefault(address) {
        const updateAdd = await this.model
            .findOne({ _id: { $ne: address._id }, accessible_id: address.accessible_id, accessible_type: "User", is_pick_address_default: true })
            .updateOne({ is_pick_address_default: false });
        return updateAdd
    }

    async updateIsPickAddressDefaultForShopKorea(address) {
        await this.model
            .findOne({ _id: { $ne: address._id }, accessible_id: address.accessible_id, accessible_type: "User", is_pick_address_default: true })
            .updateOne({ is_pick_address_default: false });
        await this.model
            .findOne({ _id: ObjectId(address._id) , accessible_id: address.accessible_id, accessible_type: "User"})
            .updateOne({ is_pick_address_default: true });
    }

    async updateIsReturnAddressDefault(address) {
        const updateAdd = await this.model
            .findOne({ _id: { $ne: address._id }, accessible_id: address.accessible_id, accessible_type: "User", is_return_address_default: true })
            .updateOne({ is_return_address_default: false });
        return updateAdd
    }

    async createAddress(request: any) {
        const listAddress = await this.model.find({ accessible_id: request.accessible_id });
        if (!listAddress.length) {
            request.is_default = true;
            request.is_pick_address_default = true;
            request.is_return_address_default = true;
        }
        // set id_vtp in request when: state, ward, district not exist
        request = await this.setDataAddress(request);
        const address = await this.model.create(request);
        if (address.is_default) {
            await this.updateIsDefault(address);
        }
        if (address.is_pick_address_default) {
            await this.updateIsPickAddressDefault(address);
        }
        if (address.is_return_address_default) {
            await this.updateIsReturnAddressDefault(address);
        }
        return address;
    }

    async updateAddress(id, request: any) {
        request = await this.setDataAddress(request);
        const address = await this.update(id, request);
        if (address.is_default) {
            await this.updateIsDefault(address);
        }
        if (address.is_pick_address_default) {
            await this.updateIsPickAddressDefault(address);
        }
        if (address.is_return_address_default) {
            await this.updateIsReturnAddressDefault(address);
        }
        return address;
    }

    async deleteAddress(idAddress) {
        const result = await this.delete(idAddress);
        return result;
    }

    async findWithUser(addressID, userID) {
        return this.model.findOne({
            accessible_id: ObjectId(userID),
            accessible_type: "User",
            _id: ObjectId(addressID)
        })
    }

    async getPickAddressUserByName(userId: string, name: string) {
        const address = await this.model.findOne({ 
            name: name,
            accessible_id: userId, 
            accessible_type: "User", 
        });
        return address;
    }

    async getPickAddressUser(userId: string) {
        const address = await this.model.findOne({ accessible_id: userId, accessible_type: "User", is_pick_address_default: true });
        return address;
    }

    async checkAddressUer(userId: string) {

        const addresses = await this.model.find({ accessible_id: userId, accessible_type: "User" });
        let result = {
            address_default: null,
            pick_address_default: null,
            return_address_default: null
        }
        if (!addresses.length) {
            return result;
        }
        addresses.map((address) => {
            if (address.is_default) {
                result.address_default = address;
            }
            if (address.is_pick_address_default) {
                result.pick_address_default = address;
            }
            if (address.is_return_address_default) {
                result.return_address_default = address;
            }
            return result;
        })
        return result;
    }

    async setDataAddress(request: any) {
        if (!request) return;
        if (!request.state?.id_vtp) {
            request.state.id_vtp = await this.findIdViettelPost({
                stateId: request.state.id,
                isGetState: true
            })
        }
        
        if (!request.district?.id_vtp) {
            request.district.id_vtp = await this.findIdViettelPost({
                stateId: request.state.id,
                districtId: request.district.id,
                isGetDistrict: true
            })
        }

        if (!request.ward?.id_vtp) {
            request.ward.id_vtp = await this.findIdViettelPost({
                stateId: request.state.id,
                districtId: request.district.id,
                wardsId: request.ward.id,
                isGetWards: true
            })
        }
        return request;
    } 


    async findIdViettelPost({ 
        stateId,
        districtId,
        wardsId,
        isGetState = false,
        isGetDistrict = false,
        isGetWards = false,
    }: any) {
        let id: any = await this.dataCityRepo.getAddressByStateId({
            stateId,
            districtId,
            wardsId,
            isGetState,
            isGetDistrict,
            isGetWards,
        })
        return id.id_vtp;
    }

}