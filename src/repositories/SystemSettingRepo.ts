import { SystemSettingDoc } from "../interfaces/SystemSetting";
import SystemSetting from "../models/SystemSetting";
import { BaseRepository } from "./BaseRepository";

export class SystemSettingRepo extends BaseRepository<SystemSettingDoc> {
    private static instance: SystemSettingRepo;

    private constructor() {
        super();
        this.model = SystemSetting;
    }

    public static getInstance(): SystemSettingRepo {
        if (!SystemSettingRepo.instance) {
            SystemSettingRepo.instance = new SystemSettingRepo();
        }

        return SystemSettingRepo.instance;
    }

    async getOrCreateSystemSetting() {
        const systemSettingExists: any = await this.findOne({});
        if (!systemSettingExists) {
            return this.create({});
        } else {
            return systemSettingExists;
        }
    }
}