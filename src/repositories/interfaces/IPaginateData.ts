export default interface IPaginateData {
  data: Array<any>,
  paginate: Object
}