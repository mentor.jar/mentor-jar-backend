export interface IOrderGHTKRepo {
    id: string,
}

export interface IPickAddresGHTKRepo {
    pick_name: string,
    pick_money: Number,
    pick_address: string,
    pick_province: string,
    pick_district: string,
    pick_ward: string,
    pick_tel: string,
}

export interface IDeliveryAddressGHTKRepo {
    name: string,
    address: string,
    province: string,
    district: string,
    ward: string,
    hamlet?: string,
    tel: string,
    note?: string,
    email: string,
}

export interface IOtherInfoGHTKRepo {
    is_freeship?: Number
    value: Number,
    transport?: String,
    deliver_option?: String,
    pick_session?: String,
    pick_date?: String,
    pick_option?: String,
}
export interface IProductGHTKRepo {
    name: String,
    weight: Number,
    quantity: Number,
    product_code?: Number,
    price: Number,
}

