export interface ICategoryRepo {
    findBySlug(slug: string): Promise<any>;
    getTreeAll(language: string): Promise<any>;
    getTreeById(id: string): Promise<any>
    getTreeByShopId(shopId: string, language: string): Promise<any>
    getTreeBySystem(getByAdmin): Promise<any>
}