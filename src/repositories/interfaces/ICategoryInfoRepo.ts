export interface ICategoryInfoRepo {
    getByCategoryId(categoryId: string): Promise<any>
}