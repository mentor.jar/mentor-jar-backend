// interface write and Read DB
import { NotFoundError, QueryDBError, SyntaxError } from '../base/customError';
import { IWrite, IRead } from './interfaces/interfaceGlobalRepository';
import { RELATIVE_SEARCH_FIELD } from '../base/variable';
import mongoose from 'mongoose';
const ObjectId = mongoose.Types.ObjectId;
export abstract class BaseRepository<T> implements IWrite<T>, IRead<T> {
    protected model;
    //we created constructor with arguments to manipulate mongodb operations
    private static relation = {
        BELONG_TO: 'belongTo',
        HAS_MANY: 'hasMany',
        HAS_ONE: 'hasOne',
        BELONG_TO_MANY: 'belongToMany',
    };
    setModel(model) {
        this.model = model;
    }

    getModel() {
        return this.model;
    }

    find(item: any): Promise<T[]> {
        const result = this.model.find(item);
        return result;
    }

    populate(relat: string): Promise<T[]> {
        const result = this.model.populate(relat);
        return result;
    }

    findOne(item: any): Promise<T[]> {
        const result = this.model.findOne(item);
        return result;
    }

    findOneAndUpdate(filter: any, item: any): Promise<T[]> {
        const result = this.model.findOneAndUpdate(filter, item, {
            new: true,
        });
        return result;
    }

    async findById(id: string) {
        try {
            const result = await this.model.findById(ObjectId(id));
            return result;
        } catch (error) {
            return null;
        }
    }

    async findOrFail(item: any) {
        try {
            const result = await this.findOne(item);
            if (!result) {
                throw new NotFoundError('Không tìm thấy');
            }
            return result;
        } catch (error) {
            if (error instanceof NotFoundError) {
                throw error;
            } else {
                throw new QueryDBError(error.message);
            }
        }
    }

    // we add to method, the async keyword to manipulate the insert result
    // of method.
    async create(item: any): Promise<T> {
        const result = await this.model.create(item);
        // after the insert operations, we returns only ok property (that haves a 1 or 0 results)
        // and we convert to boolean result (0 false, 1 true)
        return result;
    }

    /**
     *
     * @param item to write DB
     * @param findItem  to find DB from filter findItem
     * if findItem = null then find by _id in item
     */
    async createOrUpdate(item: any, findItem: any = null): Promise<T> {
        let checkExist = await this.findById(item._id);
        if (findItem) {
            checkExist = await this.findOne(findItem);
        }
        let objSchema: any = null;
        if (checkExist) {
            objSchema = await this.update(checkExist._id, item);
        } else {
            try {
                if (!item._id) delete item._id;
                objSchema = await this.create(item);
            } catch (error) {
                delete item._id;
                objSchema = await this.create(item);
            }
        }
        return objSchema;
    }

    async update(id: string, item: any): Promise<T> {
        const model = await this.findById(id);
        if (!model) {
            throw new NotFoundError('Không tìm thấy');
        }
        const objSchema = await this.model.findByIdAndUpdate(id, item, {
            new: true
        });
        return objSchema;
    }

    async delete(id: string): Promise<boolean> {
        const model = await this.findById(id);
        if (!model) {
            throw new NotFoundError('Không tìm thấy');
        }
        const objSchema = await this.model.findByIdAndDelete(id);
        return objSchema;
    }

    deleteMany(item: any) {
        return this.model.find(item).deleteMany();
    }

    deleteManyByID(ids) {
        return this.model.deleteMany({
            _id: { $in: ids },
        });
    }

    async count(filter = {}) {
        return this.model.find(filter).count();
    }

    hasOne(schema, relation: any, queryChild: any = null) {
        let relaQuery = [
            {
                $eq: ['$' + relation.foreign_key, '$$id'],
            },
        ];
        let fieldTable = relation.as ? relation.as : relation.table;
        if (queryChild) {
            relaQuery.push(queryChild);
        }
        schema = schema
            .lookup({
                from: relation.table,
                let: {
                    id: '$_id',
                },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $and: relaQuery,
                            },
                        },
                    },
                ],
                as: relation.as ? relation.as : relation.table,
            })
            .addFields({
                [fieldTable]: {
                    $slice: ['$' + fieldTable, 1],
                },
            })
            .unwind({
                path: '$' + fieldTable,
                preserveNullAndEmptyArrays: true,
            });
        return schema;
    }

    hasMany(schema, relation: any, queryChild: any = null) {
        let relaQuery = [
            {
                $eq: ['$' + relation.foreign_key, '$$id'],
            },
        ];
        let fieldTable = relation.as ? relation.as : relation.table;
        if (queryChild) {
            relaQuery.push(queryChild);
        }
        schema = schema.lookup({
            from: relation.table,
            let: {
                id: '$_id',
            },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $and: relaQuery,
                        },
                    },
                },
            ],
            as: relation.as ? relation.as : relation.table,
        });
        return schema;
    }

    belongTo(schema, relation: any, queryChild: any = null) {
        let relaQuery = [
            {
                $eq: ['$_id', '$$id'],
            },
        ];
        let fieldTable = relation.as ? relation.as : relation.table;
        let pipeline: Array<any> = [
            {
                $match: {
                    $expr: {
                        $and: relaQuery,
                    },
                },
            },
        ];
        if (queryChild) {
            if (queryChild.type == 'lookup') {
                const lookupQuery = {
                    $lookup: queryChild.query,
                };
                pipeline.push(lookupQuery);
            }
            if (queryChild.unwind) {
                pipeline.push({
                    $unwind: {
                        path: '$' + queryChild.query.as,
                        preserveNullAndEmptyArrays: true,
                    },
                });
            }
        }
        schema = schema
            .lookup({
                from: relation.table,
                let: {
                    id: '$' + relation.foreign_key,
                },
                pipeline: pipeline,
                as: fieldTable,
            })
            .unwind({
                path: '$' + fieldTable,
                preserveNullAndEmptyArrays: true,
            });
        return schema;
    }

    withJoin(schema, relations: Array<any>) {
        relations.map(relation => {
            switch (relation.type) {
                case BaseRepository.relation.HAS_MANY:
                    schema = this.hasMany(schema, relation);
                    break;
                case BaseRepository.relation.HAS_ONE:
                    schema = this.hasOne(schema, relation);
                case BaseRepository.relation.BELONG_TO:
                    schema = this.belongTo(schema, relation, relation.child);
                    break;
            }
        });
        return schema;
    }

    getExpandValues(item: any, relations: any = [], sort: any = null) {
        let queryAggre: any = [{ $match: item }];
        if (sort) queryAggre.push({ $sort: sort });
        let result = this.model.aggregate(queryAggre);
        result = this.withJoin(result, relations);
        return result;
    }

    async insertMany(items: any): Promise<T> {
        const result = await this.model.insertMany(items);
        // after the insert operations, we returns only ok property (that haves a 1 or 0 results)
        // and we convert to boolean result (0 false, 1 true)
        return result;
    }

}

export class BaseQueryHelper {
    public static fieldSearch = (keyword, fieldName) => {
        const keywordMatch = [
            {
                [fieldName]: { $regex: `${keyword}`, $options: 'gi' }
            }
        ]
        return {
            $or: keywordMatch,
        };
    };
    public static fieldSearchExtract = (keyword, fieldName) => {
        let keywordMatch;
        // Relatively search
        if (RELATIVE_SEARCH_FIELD.includes(fieldName)) {
            keywordMatch = [
                {
                    [fieldName]: { $regex: `${keyword}`, $options: 'i' }
                }
            ]
        } else {
            keywordMatch = [
                {
                    [fieldName]: {
                        $regex: '^' + keyword + '\\s+.*',
                        $options: 'gi',
                    },
                },
                {
                    [fieldName]: {
                        $regex: '.*\\s+' + keyword + '$',
                        $options: 'gi',
                    },
                },
                {
                    [fieldName]: {
                        $regex: '.*\\s+' + keyword + '\\s+.*',
                        $options: 'gi',
                    },
                },
                {
                    [fieldName]: { $regex: '^' + keyword + '$', $options: 'gi' },
                },
            ];
        }

        return {
            $or: keywordMatch,
        };
    };

    public static fieldSearchStripHTML = (keyword, fieldName) => {
        // TODO: Escape case for '&nbsp;',...
        const keywordMatch = [
            {
                [fieldName]: { $regex: `[^<]${keyword}[^\/>]`, $options: 'gi' }
            },
        ]
        return {
            $or: keywordMatch,
        };
    };
}
