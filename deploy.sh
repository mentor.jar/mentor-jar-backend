#!/bin/bash
echo "*** Select environment to deploy"
PS3='Please enter your choice (0 to quit): '
options=("Staging" "Production (please careful)")
select opt in "${options[@]}"; do
    case $REPLY in
    "1")
        echo "*** 1. Starting..."
        docker network create --driver bridge localhost || true
        docker-compose -f docker-compose.staging.yml up -d --build
        yes y | docker image prune
        echo "*** 2 .Done"
        break
        ;;
    "2")
        echo "*** 1. Starting..."
        docker network create --driver bridge localhost || true
        docker-compose -f docker-compose.prod.yml up -d --build
        yes y | docker image prune
        echo "*** 2 .Done"
        break
        ;;
    "0")
        break
        ;;
    "exit")
        break
        ;;
    *) echo "invalid option $REPLY" ;;
    esac
done
