import * as Sentry from '@sentry/node';
import * as Tracing from '@sentry/tracing';
import bodyParser from 'body-parser';
import { config } from 'dotenv';
import express from 'express';
import morganBody from 'morgan-body';
import path from 'path';
import corsOptions from './src/config/env/cors';
import { connectMongoDb } from './src/base/connection/mongo';
import { connectElasticSearch } from './src/base/connection/elastic';
import './src/base/connection/firebase';
import { handleError } from './src/base/handleError';
import injector from './src/base/injector';
import logs_config from './src/base/log';
import { authChat } from './src/middleware/socket/chat';
import { socketChat } from './src/services/sockets/chat';
const cors = require('cors');
const rootRoute = require('./src/routes/index');
import i18n from "i18n"
import { socketLivestream } from './src/services/sockets/livestream';
import './src/services/cronjob';
import { InMemoryMessageForRoomStore } from './src/SocketStores/MessageStore';
import { socketPublic } from './src/services/sockets/public';

// Don't remove
import { VN_LANG } from './src/base/variable';

const app = express();

const setLocale = async (req, res, next) => {
    const language = req.headers["accept-language"] || VN_LANG
    i18n.setLocale(language)
    next()
}

config();
connectMongoDb();
connectElasticSearch();

app.use(i18n.init);
i18n.configure({
    locales: ['vi', 'en', 'ko'],
    directory: __dirname + '/src/locales',
    objectNotation: true
});
app.use(setLocale)

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(bodyParser.json());
app.use(injector);
app.use(cors(corsOptions));
morganBody(app, logs_config);
app.use(express.static(path.join(__dirname, 'public')));

if (process.env.NODE_ENV === 'production') {
    Sentry.init({
        dsn:
            'https://183c7ed0ed2f46ceb7397f18e9277515@o526890.ingest.sentry.io/5642495',
        integrations: [
            new Sentry.Integrations.Http({ tracing: true }),
            new Tracing.Integrations.Express({ app }),
        ],
        tracesSampleRate: 1.0,
    });

    app.use(Sentry.Handlers.requestHandler());
    app.use(Sentry.Handlers.tracingHandler());
}

app.use('/', rootRoute);


if (process.env.NODE_ENV === 'production')
    app.use(Sentry.Handlers.errorHandler());

app.use(handleError);

const port = process.env.PORT || 8080;
app.listen(port, () => {
    console.log(`* ### Start with port: ${process.env.PORT || 8080} *`);
});

// ------------------ Service socket --------------------------
const httpServer = require('http').createServer();
httpServer.timeout = 300000;
const io = require('socket.io')(httpServer, {
    cors: {
        origin: '*',
    },
});
io.use(authChat);
const RoomStore = InMemoryMessageForRoomStore.getInstance();
io.on('connection', socket => {
    // emit user details
    socket.emit('user', {
        userID: socket.user._id,
        user: socket.user,
    });

    socket.join(socket.user._id);
    // service chat
    socketChat(socket);
    // service stream
    socketLivestream(socket);

    socketPublic(socket);
});

const PORT = process.env.PORT_SOCKET || 3003;

httpServer.listen(PORT, () =>
    console.log(`server listening at http://localhost:${PORT}`)
);
