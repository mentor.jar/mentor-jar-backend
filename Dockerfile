# Builder
FROM node:10-alpine AS builder
ENV NODE_OPTIONS=--max_old_space_size=4096
# Workdir
WORKDIR /app/src

COPY ./src/package* ./
COPY ./src/.env ./
RUN npm install --production

COPY ./src .
RUN npm run build
RUN cp -R /app/build/src/* /app/src

CMD npm start
